// ----------------------------------------------------------------------------
// SPDX-License-Identifier: BSD-2-Clause
// Copyright © 2014-2024 Konstantin Shmelkov <mrcashe@gmail.com>.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
// THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
// PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
// CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
// EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
// PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
// OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
// WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
// OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
// EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// ----------------------------------------------------------------------------

#include <tau/color.hh>
#include <tau/display.hh>
#include <tau/exception.hh>
#include <tau/sys.hh>
#include <tau/painter.hh>
#include <pixmap-impl.hh>
#include <iostream>

namespace tau {

Pixmap::Pixmap() {}

Pixmap::Pixmap(int depth, const Size & size):
    impl(Pixmap_impl::create(depth, size))
{
}

Pixmap::Pixmap(int depth, unsigned width, unsigned height):
    impl(Pixmap_impl::create(depth, Size(width, height)))
{
}

Pixmap::Pixmap(Pixmap_ptr pp):
    impl(pp)
{
}

Pixmap::Pixmap(Pixmap_cptr cpp):
    cimpl(cpp)
{
}

Pixmap::operator bool() const noexcept {
    return nullptr != impl || nullptr != cimpl;
}

void Pixmap::reset() {
    impl.reset();
    cimpl.reset();
}

Pixmap_ptr Pixmap::ptr() {
    return impl;
}

Pixmap_cptr Pixmap::ptr() const {
    return cimpl ? cimpl : impl;
}

Pixmap_cptr Pixmap::cptr() const {
    return cimpl ? cimpl : impl;
}

bool Pixmap::empty() const noexcept {
    return (!impl || impl->empty()) && (!cimpl || cimpl->empty());
}

Size Pixmap::size() const noexcept {
    return impl ? impl->size() : (cimpl ? cimpl->size() : Size());
}

int Pixmap::depth() const noexcept {
    return impl ? impl->depth() : (cimpl ? cimpl->depth() : 0);
}

std::size_t Pixmap::bytes() const noexcept {
    return impl ? impl->bytes() : (cimpl ? cimpl->bytes() : 0);
}

const uint8_t * Pixmap::raw() const noexcept {
    return impl ? impl->raw() : (cimpl ? cimpl->raw() : nullptr);
}

Vector Pixmap::ppi() const noexcept {
    return impl ? impl->ppi() : (cimpl ? cimpl->ppi() : Vector());
}

void Pixmap::set_ppi(const Vector & value) {
    if (impl) { impl->set_ppi(value); }
}

void Pixmap::resize(unsigned width, unsigned height) {
    if (impl) { impl->resize(Size(width, height)); }
}

void Pixmap::resize(const Size & sz) {
    if (impl) { impl->resize(sz); }
}

void Pixmap::put_pixel(int x, int y, const Color & c) {
    if (impl) { impl->put_pixel({ x, y }, c); }
}

void Pixmap::put_pixel(const Point & pt, const Color & c) {
    if (impl) { impl->put_pixel(pt, c); }
}

signal<void()> & Pixmap::signal_changed() {
    if (impl) { return impl->signal_changed(); }
    throw user_error("Pixmap::signal_changed(): unable to return reference to the signal_changed_ due to pure Pixmap");
}

Color Pixmap::get_pixel(int x, int y) const noexcept {
    return impl ? impl->get_pixel(Point(x, y)) : (cimpl ? cimpl->get_pixel(Point(x, y)): Color());
}

Color Pixmap::get_pixel(const Point & pt) const noexcept {
    return impl ? impl->get_pixel(pt) : (cimpl ? cimpl->get_pixel(pt) : Color());
}

void Pixmap::copy(const Pixmap other) {
    if (impl) { impl->copy(other.impl.get()); }
}

Pixmap Pixmap::dup() const {
    return impl ? impl->dup() : (cimpl ? cimpl->dup() : Pixmap());
}

void Pixmap::set_argb32(int x, int y, const uint8_t * buffer, std::size_t nbytes) {
    if (impl) { impl->set_argb32(Point(x, y), buffer, nbytes); }
}

void Pixmap::set_argb32(const Point & pt, const uint8_t * buffer, std::size_t nbytes) {
    if (impl) { impl->set_argb32(pt, buffer, nbytes); }
}

Painter Pixmap::painter() {
    return impl ? impl->painter() : Painter();
}

// static
Pixmap Pixmap::load_from_file(const ustring & path) {
    return Pixmap(Pixmap_impl::load_from_file(path));
}

// static
Pixmap Pixmap::create(const uint8_t * raw, std::size_t nbytes) {
    return Pixmap(Pixmap_impl::create(raw, nbytes));
}

} // namespace tau

//END

// ----------------------------------------------------------------------------
// SPDX-License-Identifier: BSD-2-Clause
// Copyright © 2014-2024 Konstantin Shmelkov <mrcashe@gmail.com>.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
// THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
// PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
// CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
// EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
// PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
// OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
// WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
// OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
// EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// ----------------------------------------------------------------------------

#include <tau/geometry.hh>
#include <cmath>
#include <iostream>

namespace tau {

Matrix::Matrix(double xx, double yy, double xy, double yx, double x0, double y0):
    xx_(xx),
    yy_(yy),
    xy_(xy),
    yx_(yx),
    x0_(x0),
    y0_(y0)
{
}

bool Matrix::operator==(const Matrix & other) const noexcept {
    return xx_ == other.xx_ && yy_ == other.yy_ && xy_ == other.xy_ && yx_ == other.yx_ && x0_ == other.x0_ && y0_ == other.y0_;
}

bool Matrix::operator!=(const Matrix & other) const noexcept {
    return xx_ != other.xx_ || yy_ != other.yy_ || xy_ != other.xy_ || yx_ != other.yx_ || x0_ != other.x0_ || y0_ != other.y0_;
}

void Matrix::reset() {
    xx_ = yy_ = 1.0;
    xy_ = yx_ = x0_ = y0_ = 0.0;
}

// r.x0 = a->x0 * b->xx + a->y0 * b->xy + b->x0;
// r.y0 = a->x0 * b->yx + a->y0 * b->yy + b->y0;

void Matrix::translate(double dx, double dy) {
    x0_ += dx*xx_+dy*xy_;
    y0_ += dy*yy_+dx*yx_;
}

void Matrix::translate(const Vector & v) {
    translate(v.x(), v.y());
}

void Matrix::rotate(double radians) {
    double s = std::sin(radians);
    double c = std::cos(radians);

    double xx = c*xx_+s*xy_;
    double yx = c*yx_+s*yy_;
    double xy = c*xy_-s*xx_;
    double yy = c*yy_-s*yx_;

    xx_ = xx;
    yx_ = yx;
    xy_ = xy;
    yy_ = yy;
}

void Matrix::skew(double horiz, double vert) {
    double xx = yx_*vert;
    double xy = yy_*vert;
    double yx = xx_*horiz;
    double yy = xy_*horiz;

    xx_ += xx;
    xy_ += xy;
    yx_ += yx;
    yy_ += yy;
}

// FIXME x0, y0?
void Matrix::scale(double sx, double sy) {
    xx_ *= sx;
    xy_ *= sx;

    yy_ *= sy;
    yx_ *= sy;
}

// FIXME x0, y0?
void Matrix::scale(double s) {
    xx_ *= s;
    xy_ *= s;
    yy_ *= s;
    yx_ *= s;
}

Vector Matrix::distance(double dx, double dy) const {
    return { xx_*dx+yx_*dy, yy_*dy+xy_*dx };
}

Vector Matrix::distance(const Vector & vec) const {
    return distance(vec.x(), vec.y());
}

Vector Matrix::mul(const Vector & vec) const {
    return { xx_*vec.x()+yx_*vec.y()+x0_, yy_*vec.y()+xy_*vec.x()+y0_ };
}

double Matrix::det() const {
    return xx_*yy_-xy_*yx_;
}

Matrix Matrix::inverted() const {
    double d = det();

    if (d) {
        double idet = 1.0/d;
        double xx = yy_*idet;
        double xy = xy_*idet;
        double yx = yx_*idet;
        double yy = xx_*idet;
        double x0 = idet*(yx_*y0_-yy_*x0_);
        double y0 = idet*(xy_*x0_-xx_*y0_);
        return Matrix(xx, xy, yx, yy, x0, y0);
    }

    return Matrix();
}

Matrix Matrix::translated(const Vector & v) const {
    Matrix mat(*this);
    mat.translate(v);
    return mat;
}

Matrix Matrix::translated(double dx, double dy) const {
    Matrix mat(*this);
    mat.translate(dx, dy);
    return mat;
}

bool Matrix::identity() const noexcept {
    return 1.0 == xx_ && 1.0 == yy_ && 0.0 == xy_ && 0.0 == yx_ && 0.0 == x0_ && 0.0 == y0_;
}

double Matrix::angle() const noexcept {
    return std::atan2(xy_, xx_);
}

bool Matrix::unity() const noexcept {
    if (0.0 == xy_ && 0.0 == yx_) {
        if (!(1.0 == xx_ || -1.0 == xx_)) {
            return false;
        }

        if (!(1.0 == yy_ || -1.0 == yy_)) {
            return false;
        }
    }

    else if (0.0 == xx_ && 0.0 == yy_) {
        if (!(1.0 == xy_ || -1.0 == xy_)) {
            return false;
        }

        if (!(1.0 == yx_ || -1.0 == yx_)) {
            return false;
        }
    }

    else {
        return false;
    }

    return true;
}

double Matrix::xx() const noexcept {
    return xx_;
}

double Matrix::xy() const noexcept {
    return xy_;
}

double Matrix::yx() const noexcept {
    return yx_;
}

double Matrix::yy() const noexcept {
    return yy_;
}

double Matrix::x0() const noexcept {
    return x0_;
}

double Matrix::y0() const noexcept {
    return y0_;
}

double Matrix::max_factor() const noexcept {
    double mf = std::max(std::fabs(xx_), std::max(std::fabs(yy_), std::max(std::fabs(xy_), std::fabs(yx_))));
    return 0.0 != mf ? 1.0/mf : 0.0;
}

Vector operator*(const Vector & vec, const Matrix & mat) {
    return mat.mul(vec);
}

Vector operator*(const Matrix & mat, const Vector & vec) {
    return mat.mul(vec);
}

Vector & operator*=(Vector & vec, const Matrix & mat) {
    vec = mat.mul(vec);
    return vec;
}

} // namespace tau

//END

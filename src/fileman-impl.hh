// ----------------------------------------------------------------------------
// SPDX-License-Identifier: BSD-2-Clause
// Copyright © 2014-2024 Konstantin Shmelkov <mrcashe@gmail.com>.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
// THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
// PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
// CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
// EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
// PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
// OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
// WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
// OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
// EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// ----------------------------------------------------------------------------

#ifndef __TAU_FILEMAN_IMPL_HH__
#define __TAU_FILEMAN_IMPL_HH__

#include <gettext-impl.hh>
#include <navigator-impl.hh>
#include <observer-impl.hh>
#include <table-impl.hh>
#include <twins-impl.hh>
#include <tau/fileman.hh>
#include <tau/icon.hh>
#include <tau/timer.hh>

namespace tau {

class Fileman_impl: public Twins_impl {
public:
    using Mode = Fileman::Mode;

    Fileman_impl(Mode fm_mode, const ustring & path=ustring());
    ~Fileman_impl() { destroy(); }

    Mode mode() const noexcept { return mode_; }
    ustring uri() const;
    void set_uri(const ustring & dirpath);
    std::vector<ustring> selection() const;
    ustring entry() const;
    Navigator_ptr navigator() noexcept { return navi_; }

    void add_filter(const ustring & patterns, const ustring & title=ustring());
    ustring filter() const { return navi_->filter(); }

    void set_option(std::string_view opt);
    void reset_option(std::string_view opt);
    bool has_option(std::string_view opt) const noexcept;
    std::string options(char32_t sep=U':') const;

    void allow_overwrite() { overwrite_allowed_ = true; }
    void disallow_overwrite() { overwrite_allowed_ = false; }
    bool overwrite_allowed() const noexcept { return overwrite_allowed_; }

    void load_state(Key_file & kf, Key_section & sect);
    void save_state(Key_file & kf, Key_section & sect);

    Action & action_apply() noexcept { return user_apply_ax_; }
    Action & action_cancel() noexcept { return action_cancel_; }

    // TODO 0.8 Public it at facade!
    Action & action_zoom_in() noexcept { return zoom_in_ax_; }

    // TODO 0.8 Public it at facade!
    Action & action_zoom_out() noexcept { return zoom_out_ax_; }

private:

    using History           = std::list<ustring>;
    using History_iter      = History::iterator;
    using Selection         = std::set<ustring>;

    Table_ptr               table_;
    Box_ptr                 pathbox_;           // Path selection buttons here.
    Navigator_ptr           navi_;
    Entry_ptr               entry_;
    Button_ptr              up_button_;
    Button_ptr              mkdir_ok_button_;

    Mode                    mode_;
    Observer_ptr            observer_;
    Cycle_ptr               filters_;
    bool                    observer_visible_       = true;     // Observer visible by default.
    bool                    dir_creation_allowed_;              // Directory creation allowed.
    bool                    overwrite_allowed_      = false;    // Allow overwrite without a prompt.
    Selection               selection_;         // Currently selected filenames.
    History                 history_;
    History_iter            ihistory_           { history_.begin() };
    Timer                   apply_timer_;
    connection              observer_cx_        { true };

    Action                  user_apply_ax_      { KC_NONE, KM_NONE };
    Action                  action_apply_       { KC_NONE, KM_NONE, lgettext("Apply"), "dialog-ok", fun(this, &Fileman_impl::on_apply) };
    Action                  action_cancel_      { "Escape Cancel", lgettext("Cancel"), "dialog-cancel", fun(this, &Fileman_impl::on_cancel) };
    Action                  action_next_        { KC_RIGHT, KM_ALT, lgettext("Next Folder"), "go-next", lgettext("Go to the next folder"), fun(this, &Fileman_impl::next) };
    Action                  action_previous_    { KC_LEFT, KM_ALT, lgettext("Previous Folder"), "go-previous", lgettext("Go to the previous folder"), fun(this, &Fileman_impl::prev) };
    Action                  action_updir_       { "<Alt>Up BackSpace", lgettext("Up Folder"), "go-up", lgettext("Go to the parent folder"), fun(this, &Fileman_impl::updir) };
    Action                  action_refresh_     { "F5", lgettext("Refresh"), "view-refresh", lgettext("Reload current folder") };
    Action                  action_mkdir_       { "F7", lgettext("Create Directory"), "folder-new", lgettext("Create a new folder"), fun(this, &Fileman_impl::on_mkdir) };
    Action                  zoom_in_ax_         { U'=', KM_CONTROL, lgettext("Zoom In"), "zoom-in", lgettext("Increase icon size"), fun(this, &Fileman_impl::zoom_in) }; // FIXME Ctrl+= -> Ctrl++?
    Action                  zoom_out_ax_        { U'-', KM_CONTROL, lgettext("Zoom Out"), "zoom-out", lgettext("Decrease icon size"), fun(this, &Fileman_impl::zoom_out) };
    Toggle_action           action_hidden_      { "<Alt>.", lgettext("Show Hidden Files"), "show-hidden", lgettext("Show Hidden Files"), fun(this, &Fileman_impl::on_show_hidden) };

private:

    void entry_from_selection();
    void mkdir(const ustring & path);
    void apply();
    bool next_avail() const;
    bool prev_avail() const;
    void add_to_history(const ustring & path);
    void refresh_history();
    void enable_apply(bool yes);
    void next();
    void prev();
    void updir();
    void show_observer();
    void hide_observer();
    void popup_configure();
    void zoom_in();
    void zoom_out();

    void on_file_select(const ustring & filename, bool yes);
    void on_file_activate(const ustring & path);
    void on_dir_changed(const ustring & path);
    void on_entry_activate(const ustring & s);
    void on_entry_changed();
    void on_display();
    void on_apply();
    void on_cancel();
    void on_mkdir();
    void on_mkdir_apply(Entry_impl * entry);
    void on_mkdir_activate(const ustring & dirname, Entry_impl * entry);
    void on_mkdir_changed(Entry_impl & entry);
    void on_show_hidden(bool show);
    bool on_entry_mouse_down(int mbt, int mm, const Point & pt);
    bool on_navi_mouse_down(int mbt, int mm, const Point & pt);
    void on_observer_uri(const ustring & uri);
    void on_filter_select(Entry_impl * ep);
};

} // namespace tau

#endif //__TAU_FILEMAN_IMPL_HH__

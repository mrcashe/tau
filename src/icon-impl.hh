// ----------------------------------------------------------------------------
// SPDX-License-Identifier: BSD-2-Clause
// Copyright © 2014-2024 Konstantin Shmelkov <mrcashe@gmail.com>.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
// THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
// PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
// CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
// EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
// PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
// OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
// WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
// OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
// EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// ----------------------------------------------------------------------------

#ifndef __TAU_ICON_IMPL_HH__
#define __TAU_ICON_IMPL_HH__

#include <tau/enums.hh>
#include <tau/icon.hh>
#include <image-impl.hh>

namespace tau {

class Icon_impl: public Image_impl {
public:

    Icon_impl();
    Icon_impl(int icon_size);
    Icon_impl(const ustring & icon_name, int icon_size);
    Icon_impl(Pixmap_cptr pix, int icon_size=Icon::DEFAULT);
    Icon_impl(Action_base & action, int icon_size, Action::Flags items=Action::ALL);
    ~Icon_impl();

    void assign(const ustring & icon_name, int icon_size);
    void assign(const ustring & icon_name);
    void assign(Pixmap_cptr pix);
    ustring icon_name() const noexcept { return icon_name_; }
    void resize(int icon_size);
    int icon_size() const noexcept;
    Pixmap_ptr pixmap() { return !ani_.empty() ? ani_.front().pix : nullptr; }
    Pixmap_cptr pixmap() const { return !cani_.empty() ? cani_.front().pix : (!ani_.empty() ? ani_.front().pix : nullptr); }
    signal<void()> & signal_click();

protected:

    // Overrides Image_impl.
    bool update_requisition() override;

private:

    ustring             icon_name_;
    int                 icon_size_      { Icon::DEFAULT };
    connection          icon_size_cx_;
    connection          theme_cx_       { true };
    signal<void()>  *   signal_click_   { nullptr };

private:

    void init();
    void on_display_in();
    void on_display_out();
    void update_pixmap();
    bool on_mouse_down(int mbt, int mm, const Point & pt);
};

} // namespace tau

#endif // __TAU_ICON_IMPL_HH__

// ----------------------------------------------------------------------------
// SPDX-License-Identifier: BSD-2-Clause
// Copyright © 2014-2024 Konstantin Shmelkov <mrcashe@gmail.com>.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
// THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
// PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
// CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
// EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
// PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
// OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
// WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
// OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
// EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// ----------------------------------------------------------------------------

#ifndef __TAU_FILE_IMPL_HH__
#define __TAU_FILE_IMPL_HH__

#include <defs-impl.hh>
#include <tau/file.hh>
#include <tau/timeval.hh>
#include <tau/signal.hh>

namespace tau {

class File_impl {
public:

    File_impl(const File_impl & other) = delete;
    File_impl(File_impl && other) = delete;
    File_impl & operator=(const File_impl & other) = delete;
    File_impl & operator=(File_impl && other) = delete;
    virtual ~File_impl() = default;

    static File_ptr create(const ustring & uri=ustring());

    enum {
        IS_DIR      = 1 << 0,
        IS_LNK      = 1 << 1,
        IS_REG      = 1 << 2,
        IS_CHR      = 1 << 3,
        IS_BLK      = 1 << 4,
        IS_FIFO     = 1 << 5,
        IS_SOCK     = 1 << 6
    };

    bool exists() const noexcept { return exists_; }
    uintmax_t bytes() const noexcept { return bytes_; }
    bool is_dir() const noexcept { return IS_DIR & flags_; }
    bool is_link() const noexcept { return IS_LNK & flags_; }
    bool is_regular() const noexcept { return IS_REG & flags_; }
    bool is_char() const noexcept { return IS_CHR & flags_; }
    bool is_block() const noexcept { return IS_BLK & flags_; }
    bool is_fifo() const noexcept { return IS_FIFO & flags_; }
    bool is_socket() const noexcept { return IS_SOCK & flags_; }

    Timeval atime() const noexcept { return atime_; }
    Timeval ctime() const noexcept { return ctime_; }
    Timeval mtime() const noexcept { return mtime_; }

    virtual signal<void(int, const ustring &)> & signal_watch(int event_mask) = 0;
    virtual bool is_exec() const noexcept = 0;
    virtual bool is_hidden() const noexcept = 0;
    virtual bool is_removable() const noexcept = 0;
    virtual void rm(int opts=0, slot<void(int)> slot_async=slot<void(int)>()) = 0;

    std::optional<ustring> read_link() const;
    std::string mime() const;
    ustring type(const Language & lang=Language()) const;
    Pixmap_cptr icon(int icon_size) const;

    void cp(const ustring & dest, int opts=0);

protected:

    ustring             uri_;
    bool                exists_     = false;
    uintmax_t           bytes_      = 0;
    uint32_t            flags_      = 0;

    Timeval             atime_      { 0 };
    Timeval             ctime_      { 0 };
    Timeval             mtime_      { 0 };

    mutable std::string mime_;
    mutable ustring     type_;

protected:

    File_impl() = default;

    // Overriden by File_unix.
    // Overriden by File_win.
    virtual std::string_view user_icon() const = 0;

};

} // namespace tau

#endif // __TAU_FILE_IMPL_HH__

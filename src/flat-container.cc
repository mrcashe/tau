// ----------------------------------------------------------------------------
// SPDX-License-Identifier: BSD-2-Clause
// Copyright © 2014-2024 Konstantin Shmelkov <mrcashe@gmail.com>.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
// THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
// PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
// CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
// EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
// PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
// OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
// WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
// OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
// EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// ----------------------------------------------------------------------------

#include <tau/painter.hh>
#include <flat-container.hh>
#include <algorithm>

namespace tau {

Flat_container::Flat_container() {
    signal_display_out_.connect(fun(this, &Flat_container::on_display_out));
    signal_child_.connect(fun(this, &Flat_container::on_child));
}

// Overrides Widget_impl.
void Flat_container::handle_paint(Painter pr, const Rect & inval) {
    Widget_impl::handle_paint(pr, inval);
    paint_children(pr, inval, viewables_.data(), viewables_.size(), &Widget_impl::handle_paint);
}

// Overrides Widget_impl.
void Flat_container::handle_backpaint(Painter pr, const Rect & inval) {
    Widget_impl::handle_backpaint(pr, inval);
    paint_children(pr, inval, viewables_.data(), viewables_.size(), &Widget_impl::handle_backpaint);
}

// Overrides pure Container_impl.
void Flat_container::on_child_viewable(Widget_impl * wi, bool yes) {
    if (!in_shutdown()) {
        if (yes) {
            viewables_.push_back(wi);
        }

        else {
            auto i = std::find(viewables_.begin(), viewables_.end(), wi);
            if (i != viewables_.end()) { viewables_.erase(i); }
        }

        update_mouse_owner();
    }
}

// Overrides pure Container_impl.
Widget_impl * Flat_container::mouse_target(const Point & pt) {
    if (!in_shutdown()) {
        if (mouse_grabber_) { return mouse_grabber_; }

        for (auto wp: viewables_) {
            if (Rect(wp->origin(), wp->size()).contains(pt)) {
                return wp->enabled() ? wp : nullptr;
            }
        }
    }

    return nullptr;
}

int Flat_container::level(const Widget_impl * wp) const noexcept {
    return container_ ? container_->level(this) : 0;
}

void Flat_container::on_display_out() {
    viewables_.clear();
}

void Flat_container::on_child(Widget_ptr wp, int op) {
    if (children_.empty()) {
        viewables_.clear();
    }
}

} // namespace tau

//END

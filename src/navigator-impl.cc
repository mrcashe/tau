// ----------------------------------------------------------------------------
// SPDX-License-Identifier: BSD-2-Clause
// Copyright © 2014-2024 Konstantin Shmelkov <mrcashe@gmail.com>.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
// THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
// PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
// CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
// EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
// PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
// OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
// WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
// OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
// EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// ----------------------------------------------------------------------------

#include <display-impl.hh>
#include <box-impl.hh>
#include <button-impl.hh>
#include <counter-impl.hh>
#include <header-impl.hh>
#include <icon-impl.hh>
#include <label-impl.hh>
#include <list-impl.hh>
#include <navigator-impl.hh>
#include <pixmap-impl.hh>
#include <progress-impl.hh>
#include <roller-impl.hh>
#include <spinner-impl.hh>
#include <sys-impl.hh>
#include <theme-impl.hh>
#include <window-impl.hh>
#include <tau/locale.hh>
#include <tau/string.hh>
#include <algorithm>
#include <iomanip>
#include <iostream>

namespace tau {

Navigator_impl::Navigator_impl(const ustring & uri):
    theme_(Theme_impl::root())
{
    // Variant A - Label for empty dir.
    empty_ = std::make_shared<Label_impl>(lgettext("Empty Folder"));
    empty_->conf().redirect(Conf::FOREGROUND, Conf::WEAK_FOREGROUND);
    empty_->conf().font().enlarge(4);
    empty_->conf().font().make_bold();
    insert(empty_);

    // Variant B - Box with file list and status bar.
    box_ = std::make_shared<Box_impl>(Orientation::SOUTH);
    Roller_ptr roller = std::make_shared<Roller_impl>();
    box_->append(roller);

    // View as table (list?).
    list_ = std::make_shared<List_impl>();
    list_->set_column_spacing(3);
    list_->set_min_column_width(0, 80);
    list_->signal_activate_row().connect(fun(this, &Navigator_impl::on_list_activate));
    list_->signal_mark_validate().connect(fun(this, &Navigator_impl::on_list_mark_validate));
    list_->signal_size_changed().connect(fun(this, &Navigator_impl::limit_name_column));
    list_->signal_move_row().connect(fun(this, &Navigator_impl::on_list_move_row));
    list_->action_cancel().disable();
    roller->insert(list_);

    // Header for list.
    hdr_ = std::make_shared<Header_impl>(Separator::SOLID, list_);
    hdr_->show_column(0, lgettext("Name"));
    hdr_->show_column(1, lgettext("Size"), Align::END);
    hdr_->show_column(2, lgettext("Date"), Align::END);
    hdr_->show_column(3, lgettext("Type"));
    hdr_->show_sort_marker(0);
    hdr_->signal_click().connect(fun(this, &Navigator_impl::on_header_click));
    hdr_->signal_width_changed().connect(fun(this, &Navigator_impl::on_header_width_changed));
    box_->prepend(hdr_, true);

    // Status bar outer box.
    ostatus_ = std::make_shared<Box_impl>(Orientation::WEST, 2);
    ostatus_->conf().boolean(Conf::BUTTON_LABEL) = false;
    ostatus_->conf().integer(Conf::ICON_SIZE) = Icon::SMALL;
    box_->append(ostatus_, true);

    // Cancel button inside of outer status box.
    connect_action(cancel_ax_);
    cancel_ax_.signal_enable().connect(fun(cancel_ax_, &Action::show));
    cancel_ax_.signal_disable().connect(fun(cancel_ax_, &Action::hide));
    cancel_ax_.disable();
    auto button = std::make_shared<Button_impl>(cancel_ax_);
    button->hide_relief();
    button->hint_margin(2, 2, 1, 0);
    button->set_cursor("hand");
    ostatus_->append(button, true);

    // File processing Progress indicator inside of outer status box.
    progress_ = std::make_shared<Progress_impl>();
    progress_->hint_margin_top(1);
    ostatus_->append(progress_, true);
    cancel_ax_.signal_enable().connect(fun(progress_, &Widget_impl::show));
    cancel_ax_.signal_enable().connect(fun(hide_cx_, &connection::drop));
    cancel_ax_.signal_disable().connect(fun(this, &Navigator_impl::on_cancel_disable));

    // Inner status box.
    status_ = std::make_shared<Box_impl>(2);
    status_->hint_margin(2, 0, 1, 0);
    ostatus_->append(status_);

    // Directory counter inside of inner status box.
    dir_counter_ = std::make_shared<Counter_impl>();
    dir_counter_->hide_buttons(), dir_counter_->disallow_edit(), dir_counter_->hide();
    status_->append(dir_counter_, true);
    dir_label_ = std::make_shared<Label_impl>(lgettext("folders"));
    dir_label_->hint_margin(2, 2, 0, 0);
    dir_counter_->append(dir_label_, true);

    // File counter inside of inner status box.
    file_counter_ = std::make_shared<Counter_impl>();
    file_counter_->hide_buttons(), file_counter_->disallow_edit(), file_counter_->hide();
    status_->append(file_counter_, true);
    file_label_ = std::make_shared<Label_impl>(lgettext("files"));
    file_label_->hint_margin(2, 2, 0, 0);
    file_counter_->append(file_label_, true);

    // Selection counter inside of inner status box.
    sel_counter_ = std::make_shared<Counter_impl>();
    sel_counter_->hide_buttons(), sel_counter_->disallow_edit(), sel_counter_->hide();
    status_->append(sel_counter_, true);
    sel_label_ = std::make_shared<Label_impl>(lgettext("files"));
    sel_label_->hint_margin(2, 4, 0, 0);
    sel_counter_->append(sel_label_, true);
    all_value_ = std::make_shared<Label_impl>("0");
    all_value_->hint_margin(2, 2, 0, 0);
    sel_counter_->append(all_value_, true);

    // Spinner indicating processing inside of inner status box.
    spinner_ = std::make_shared<Spinner_impl>();
    spinner_->hint_margin(2, 2, 0, 0);

    connect_action(zoom_in_ax_);
    connect_action(zoom_out_ax_);
    signal_display_in_.connect(bind_back(fun(this, &Navigator_impl::set_uri), file_exists(uri) || file_is_dir(path_dirname(uri)) ? path_real(uri) : path_cwd()));
    signal_take_focus_.connect(fun(this, &Navigator_impl::grab_focus));
    conf().signal_changed(Conf::ICON_SIZE).connect(fun(this, &Navigator_impl::on_icon_size));
    signal_mouse_wheel().connect(fun(this, &Navigator_impl::on_mouse_wheel), true);
}

int Navigator_impl::find_row(const ustring & name) {
    if (!holders_.empty()) {
        auto & hol = holders_.front();
        auto i = std::find_if(hol.recs_.begin(), hol.recs_.end(), [name](auto & rec) { return name == rec.name_; });
        if (i != hol.recs_.end()) { return i->y_; }
        i = std::find_if(hol.dirs_.begin(), hol.dirs_.end(), [name](auto & rec) { return name == rec.name_; });
        if (i != hol.dirs_.end()) { return i->y_; }
    }

    return INT_MIN;
}

ustring Navigator_impl::name_from_row(int row) {
    if (!holders_.empty()) {
        auto & hol = holders_.front();
        auto i = std::find_if(hol.recs_.begin(), hol.recs_.end(), [row](auto & rec) { return row == rec.y_; });
        if (i != hol.recs_.end()) { return i->name_; }
        i = std::find_if(hol.dirs_.begin(), hol.dirs_.end(), [row](auto & rec) { return row == rec.y_; });
        if (i != hol.dirs_.end()) { return i->name_; }
    }

    return ustring();
}

bool Navigator_impl::compare(const Rec & r1, const Rec & r2) const noexcept {
    if ("name" == sort_) { return has_option("backward") ? r2.name_ < r1.name_ : r1.name_ < r2.name_; }
    if ("date" == sort_) { return has_option("backward") ? r2.mtime_ < r1.mtime_ : r1.mtime_ < r2.mtime_; }

    if (!r1.dir_) {
        if ("bytes" == sort_) { return has_option("backward") ? r2.size_ < r1.size_ : r1.size_ < r2.size_; }
        if ("file" == sort_) { return has_option("backward") ? r2.type_ < r1.type_ : r1.type_ < r2.type_; }
    }

    return false;
}

void Navigator_impl::limit_name_column() {
    if (list_ && 0 == list_->column_width(0)) {
        unsigned w = 0;
        if (has_option("bytes") || has_option("date")) { w = (7*list_->size().width())/10; }
        list_->set_max_column_width(0, w);
    }
}

void Navigator_impl::update_sel_counter() {
    std::size_t n = list_ ? list_->sel_count() : 0;
    sel_counter_->assign(n ? n+1 : 0);
    sel_counter_->par_show(0 != n);
    sel_label_->assign(lngettext("object", "objects", n)+' '+lgettext("selected of"));
    update_counters();
}

void Navigator_impl::update_counters() {
    update_file_counter();
    update_dir_counter();
}

void Navigator_impl::update_file_counter() {
    if (!holders_.empty()) {
        Holder & hol = holders_.front();
        std::size_t n = has_option("hidden") ? hol.recs_.size() : std::count_if(hol.recs_.begin(), hol.recs_.end(), [](auto & rec) { return !rec.hidden_; });
        file_counter_->assign(n);
        file_label_->assign(lngettext("file", "files", n));
        file_counter_->par_show(sel_counter_->hidden() && n);
    }
}

void Navigator_impl::update_dir_counter() {
    if (!holders_.empty()) {
        Holder & hol = holders_.front();
        std::size_t n = has_option("hidden") ? hol.dirs_.size() : std::count_if(hol.dirs_.begin(), hol.dirs_.end(), [](auto & rec) { return !rec.hidden_; });
        dir_counter_->assign(n);
        dir_label_->assign(lngettext("folder", "folders", n));
        dir_counter_->par_show(sel_counter_->hidden() && n);
    }
}

ustring Navigator_impl::format_file_time(Timeval & mtime) const {
    Timeval now = Timeval::now();
    tm tm_file = mtime.localtime(), tm_now = now.localtime();
    return mtime.str((tm_file.tm_year == tm_now.tm_year ? "%e %b %R" : "%x"));
}

std::vector<ustring> Navigator_impl::list_dir(const ustring & uri) const {
    std::vector<ustring> v;
    try { v = file_list(uri); std::erase(v, "."), std::erase(v, ".."); }
    catch (...) {}
    return v;
}

Navigator_impl::Rec & Navigator_impl::make_record(const ustring & filename) {
    Holder & hol = holders_.front();
    auto path = path_build(hol.path_, filename);
    File file(path);

    Rec rec;
    rec.dir_ = file.is_dir();
    rec.name_ = filename;
    rec.hidden_ = file.is_hidden();
    rec.link_ = file.is_link();
    rec.type_ = file.type();
    rec.size_ = file.bytes();
    rec.mtime_ = file.mtime();

    if (rec.link_) {
        File rfile(path_real(path));
        rec.dir_ = rfile.is_dir();
        rec.type_ = rfile.type();
        rec.mtime_ = rfile.mtime();
    }

    if (rec.dir_) {
        update_dir_counter();
        return hol.dirs_.emplace_back(rec);
    }

    rec.mime_ = file.mime();
    return hol.recs_.emplace_back(rec);
}

// NOTE Called only when list_ is not pure.
void Navigator_impl::list_record(Rec & rec, int y) {
    bool visible = !rec.filtered_ && (!rec.hidden_ || has_option("hidden"));

    // Show file/dir name.
    auto label = std::make_shared<Label_impl>(rec.name_, Align::START);
    label->par_show(visible);
    label->wrap(Wrap::ELLIPSIZE_CENTER);
    label->signal_select().connect(bind_back(fun(this, &Navigator_impl::on_file_select), rec.name_));
    label->signal_unselect().connect(bind_back(fun(this, &Navigator_impl::on_file_unselect), rec.name_));
    rec.y_ = list_->insert_row(label, y, Align::START);

    // Do not show file size for directory.
    if (has_option("bytes") && !rec.dir_) {
        label = std::make_shared<Label_impl>(str_bytes(rec.size_));
        label->par_show(visible);
        list_->insert(rec.y_, label, 0, Align::END, true);
    }

    // Show date/time.
    if (has_option("date")) {
        label = std::make_shared<Label_impl>(format_file_time(rec.mtime_));
        label->par_show(visible);
        list_->insert(rec.y_, label, 1, Align::END, true);
    }

    // Show file type.
    if (has_option("type")) {
        label = std::make_shared<Label_impl>(rec.type_);
        label->par_show(visible);
        list_->insert(rec.y_, label, 2, Align::START, true);
    }

    File file(path_build(uri_, rec.name_));
    int px = conf().integer(Conf::ICON_SIZE);

    if (!rec.icon_) {
        if (!rec.link_) {
            rec.icon_ = file.icon(px).ptr();
        }

        else if (auto lnk = file.read_link()) {
            auto rlink = path_real(*lnk);
            rec.tooltip_ = "->"_tu+rlink;
            File rfile(rlink);

            if (rfile.exists()) {
                if (auto cpix = (rec.dir_ ? file : rfile).icon(px).ptr()) {
                    if (auto lpix = theme_->find_icon("link", Icon::NANO)) {
                        auto pix = cpix->dup();

                        if (auto pr = pix->painter()) {
                            pr.move_to(pix->size().iwidth()-lpix->size().iwidth(), pix->size().iheight()-lpix->size().iheight());
                            pr.pixmap(lpix, { 0, 0 }, lpix->size(), true);
                            pr.fill();
                            rec.icon_ = pix;
                        }
                    }
                }
            }

            else {
                rec.icon_ = theme_->find_icon("link-broken", px);
            }
        }
    }

    if (rec.icon_) {
        auto img = std::make_shared<Icon_impl>(rec.icon_);
        img->par_show(visible);
        img->set_tooltip(rec.tooltip_);
        list_->insert(rec.y_, img, -1, Align::START, true);
    }
}

void Navigator_impl::begin_any() {
    if (!spinner_->has_parent()) {
        if (list_) { list_->set_cursor("wait:watch"); }
        status_->prepend(spinner_);
        spinner_->start();
    }

    if (run_cx_.empty()) {
        if (list_) { list_->clear(); }
        progress_->set_value(0);
        progress_->set_format("0 s");
        cancel_ax_.enable();
        t1_ = Timeval::now();
        tick_cx_ = Loop_impl::this_ptr()->alarm(fun(this, &Navigator_impl::on_tick), 772, true);
    }
}

void Navigator_impl::begin_read() {
    begin_any();
    if (list_) { list_->clear(); }
    insert(empty_);
    Holder & hol = holders_.front();
    hol.dirs_.clear();
    hol.recs_.clear();
    hol.files_ = list_dir(hol.path_);
    fi_ = hol.files_.begin();
    update_counters();
    run_cx_ = connect_run(fun(this, &Navigator_impl::run_read));
}

void Navigator_impl::run_read() {
    Holder & hol = holders_.front();

    if (fi_ != hol.files_.end()) {
        make_record(*fi_); insert(box_);
        ++fi_;
    }

    else {
        begin_filter();
    }
}

void Navigator_impl::begin_filter() {
    begin_any();
    Holder & hol = holders_.front();
    update_counters();

    if (!filters_.empty()) {
        ri_ = hol.recs_.begin();
        run_cx_ = connect_run(fun(this, &Navigator_impl::run_filter));
    }

    else {
        if (!hol.recs_.empty()) { insert(box_); }
        update_counters();
        begin_sort();
    }
}

void Navigator_impl::run_filter() {
    Holder & hol = holders_.front();

    if (ri_ != hol.recs_.end()) {
        ri_->filtered_ = !filters_.empty() && !std::any_of(filters_.begin(), filters_.end(), [this](auto & pat) { return path_match(pat, ri_->name_); });
        update_file_counter();
        insert(box_);
        ++ri_;
    }

    else {
        begin_sort();
    }
}

void Navigator_impl::begin_sort() {
    Holder & hol = holders_.front();

    if (!hol.dirs_.empty()) {
        ri_ = hol.dirs_.begin();
        run_cx_ = connect_run(fun(this, &Navigator_impl::run_sort_dirs));
    }

    else {
        ri_ = hol.recs_.begin();
        run_cx_ = connect_run(fun(this, &Navigator_impl::run_sort));
    }
}

void Navigator_impl::run_sort_dirs() {
    Holder & hol = holders_.front();

    if (hol.dirs_.size() > 15) {
        if (ri_ != hol.dirs_.end()) {
            std::swap(*ri_, *std::min_element(ri_, hol.dirs_.end(), [this](auto & r1, auto & r2) { return compare(r1, r2); }));
            ++ri_;
        }

        else {
            begin_sort_files();
        }
    }

    else {
        std::sort(hol.dirs_.begin(), hol.dirs_.end(), [this](auto & r1, auto & r2) { return compare(r1, r2); });
        begin_sort_files();
    }
}

void Navigator_impl::begin_sort_files() {
    Holder & hol = holders_.front();

    if (!hol.recs_.empty()) {
        ri_ = hol.recs_.begin();
        run_cx_ = connect_run(fun(this, &Navigator_impl::run_sort));
    }

    else {
        begin_show();
    }
}

void Navigator_impl::run_sort() {
    Holder & hol = holders_.front();

    if (hol.recs_.size() > 15) {
        if (ri_ != hol.recs_.end()) {
            std::swap(*ri_, *std::min_element(ri_, hol.recs_.end(), [this](auto & r1, auto & r2) { return compare(r1, r2); }));
            ++ri_;
        }

        else {
            begin_show();
        }
    }

    else {
        std::sort(hol.recs_.begin(), hol.recs_.end(), [this](auto & r1, auto & r2) { return compare(r1, r2); });
        begin_show();
    }
}

void Navigator_impl::begin_show() {
    begin_any();
    Holder & hol = holders_.front();
    ri_ = hol.dirs_.begin();
    progress_->set_max_value(has_option("hidden") ? hol.recs_.size()+hol.dirs_.size() : std::count_if(hol.dirs_.begin(), hol.dirs_.end(), [](auto & rec) { return !rec.hidden_; })+std::count_if(hol.recs_.begin(), hol.recs_.end(), [](auto & rec) { return !rec.hidden_; }));
    run_cx_ = connect_run(fun(this, &Navigator_impl::show_dirs));
}

void Navigator_impl::show_dirs() {
    Holder & hol = holders_.front();

    if (ri_ != hol.dirs_.end()) {
        if (list_) {
            list_record(*ri_);
            progress_->set_value(list_->count());
            if (!list_->has_selection() && path_same(hol.name_, ri_->name_)) { list_->select(ri_->y_); }
        }

        ++ri_;
    }

    else if (!has_option("dirs_only")) {
        ri_ = hol.recs_.begin();
        run_cx_ = connect_run(fun(this, &Navigator_impl::show_files));
    }

    else {
        end_show();
    }
}

void Navigator_impl::show_files() {
    Holder & hol = holders_.front();

    if (ri_ != holders_.front().recs_.end()) {
        if (list_) {
            list_record(*ri_);
            progress_->set_value(list_->count());
            if (!list_->has_selection() && path_same(hol.name_, ri_->name_)) { list_->select(ri_->y_); }
        }

        ++ri_;
    }

    else {
        end_show();
    }
}

void Navigator_impl::end_show() {
    cancel_ax_.disable();
    tick_cx_.drop();
    on_tick();
    status_->remove_front();
    all_value_->assign(str_format(std::locale(), dir_counter_->value()+file_counter_->value()));
    if (list_) { list_->unset_cursor(); }
    if (focused()) { grab_focus(); }
    Holder & hol = holders_.front();

    if (list_) {
        if (!list_->has_selection()) {
            if (INT_MIN == list_->select(find_row(hol.name_.empty() ? path_notdir(uri_) : hol.name_))) { list_->select_front(); }
            if (focused()) { list_->take_focus(); }
        }
    }

    if (!hol.dirs_.empty()) {
        ri_ = hol.dirs_.begin();
        run_cx_ = connect_run(fun(this, &Navigator_impl::show_objects));
    }

    else {
        run_cx_.drop();
    }
}

void Navigator_impl::list_objects(Rec & rec) {
    if (list_) {
        auto lp = std::dynamic_pointer_cast<Label_impl>(list_->widget_at(rec.y_, 1));
        if (!lp) { lp = std::make_shared<Label_impl>(); lp->par_show(!rec.hidden_ || has_option("hidden")); list_->insert(rec.y_, lp, 0, Align::END, true); }
        lp->assign(str_format(std::locale(), rec.size_, ' ', lngettext("object", "objects", rec.size_)));
    }
}

// Shows number of objects within nested directories.
void Navigator_impl::show_objects() {
    Holder & hol = holders_.front();

    if (ri_ != hol.dirs_.end()) {
        ri_->size_ = list_dir(path_build(uri_, ri_->name_)).size();
        list_objects(*ri_);
        ++ri_;
    }

    else {
        run_cx_.drop();
    }
}

void Navigator_impl::refresh() {
    if (list_) {
        int current = list_->current();
        ustring filename;
        if (INT_MIN != current) { filename = name_from_row(current); }
        auto path = path_build(uri_, filename);
        set_uri(file_is_dir(path) ? uri_ : path);
    }
}

void Navigator_impl::set_uri(const ustring & uri) {
    auto was = uri_;
    ustring name, path = path_is_absolute(uri) ? uri : path_build(path_cwd(), uri);

    if (!file_is_dir(path)) {
        name = path_notdir(path);
        ustring p = path_dirname(path);
        if (file_is_dir(p)) { uri_ = p; }
    }

    else {
        uri_ = path;
    }

    if (!file_is_dir(uri_)) { uri_ = path_cwd(); }
    auto i = std::find_if(holders_.begin(), holders_.end(), [this](auto & h) { return path_same(uri_, h.path_); });

    if (holders_.end() == i) {
        if (holders_.size() >= NAVI_CACHE_SIZE) { holders_.pop_back(); } // defs-impl.hh
        auto & hol = holders_.emplace_front();
        hol.path_ = uri_;
        hol.name_ = name;
        hol.file_ = File(uri_);
        hol.wcx_ = hol.file_.signal_watch(File::CREATED|File::CREATED|File::DELETED|File::MOVED_IN|File::MOVED_OUT).connect(fun(this, &Navigator_impl::on_watch));
        begin_read();
    }

    else {
        if (i != holders_.begin()) {
            i->wcx_.unset_autodrop();
            i->tcx_.unset_autodrop();
            auto hol = *i;
            holders_.erase(i);
            holders_.push_front(hol);
            holders_.front().wcx_.set_autodrop();
            holders_.front().tcx_.set_autodrop();
            begin_filter();
        }

        else if (run_cx_.empty()) {
            begin_filter();
        }
    }

    if (!path_same(was, uri_)) { signal_dir_changed_(uri_); }
}

void Navigator_impl::on_list_activate(int row) {
    if (!holders_.empty()) {
        auto & hol = holders_.front();
        auto i = std::find_if(hol.recs_.begin(), hol.recs_.end(), [row](auto & rec) { return row == rec.y_; });
        if (i != hol.recs_.end()) { signal_file_activate_(path_build(uri_, i->name_)); return; }
        i = std::find_if(hol.dirs_.begin(), hol.dirs_.end(), [row](auto & rec) { return row == rec.y_; });
        if (i != hol.dirs_.end()) { set_uri(path_build(uri_, i->name_)); }
    }
}

bool Navigator_impl::on_list_mark_validate(int y) {
    return has_option("dir_select") || has_option("dirs_only") || holders_.empty() || !std::any_of(holders_.front().dirs_.begin(), holders_.front().dirs_.end(), [y](auto & rec) { return rec.y_ == y; });
}

void Navigator_impl::on_header_click(int column) {
    switch (column) {
        case 0:
            if ("name" != sort_) { sort_by("name"); }
            else if (has_option("backward")) { reset_option("backward"); }
            else { set_option("backward"); }
            break;

        case 1:
            if ("bytes" != sort_) { sort_by("bytes"); }
            else if (has_option("backward")) { reset_option("backward"); }
            else { set_option("backward"); }
            break;

        case 2:
            if ("date" != sort_) { sort_by("date"); }
            else if (has_option("backward")) { reset_option("backward"); }
            else { set_option("backward"); }
            break;

        case 3:
            if ("type" != sort_) { sort_by("type"); }
            else if (has_option("backward")) { reset_option("backward"); }
            else { set_option("backward"); }
            break;
    }
}

void Navigator_impl::on_header_width_changed(int column) {
    if (list_) {
        if (0 == column) {
            list_->set_max_column_width(0, 0);
        }
    }
}

void Navigator_impl::set_filter(const ustring & filters) {
    filters_.clear();

    if ("*" != filters) {
        for (auto & s: str_explode(filters, ":;,")) {
            filters_.push_back(str_trim(s));
        }
    }

    refresh();
}

void Navigator_impl::update_sort_marker() {
    int list_col = -1;
    if ("name" == sort_) { list_col = 0; }
    else if ("bytes" == sort_) { list_col = 1; }
    else if ("date" == sort_) { list_col = 2; }
    else if ("type" == sort_) { list_col = 3; }
    hdr_->show_sort_marker(list_col, has_option("backward"));
}

void Navigator_impl::sort_by(const ustring & col) {
    sort_ = col;
    update_sort_marker();
    refresh();
}

void Navigator_impl::set_option(std::string_view opt) {
    auto lopt = str_tolower(opt);

    if (options_.insert(lopt).second) {
        if ("multiple" == lopt) {
            if (list_) {
                list_->allow_multiple_select();
            }
        }

        else if ("backward" == lopt) {
            update_sort_marker();
            refresh();
        }

        else if ("hidden" == lopt) {
            if (list_ && !holders_.empty()) {
                for (auto & rec: holders_.front().dirs_) {
                    if (rec.hidden_) {
                        for (auto wp: list_->widgets(rec.y_)) {
                            wp->show();
                        }
                    }
                }

                for (auto & rec: holders_.front().recs_) {
                    if (rec.hidden_) {
                        for (auto wp: list_->widgets(rec.y_)) {
                            wp->show();
                        }
                    }
                }
            }
        }

        else {
            refresh();
        }

        limit_name_column();
    }
}

void Navigator_impl::reset_option(std::string_view opt) {
    auto lopt = str_tolower(opt);

    if (0 != options_.erase(lopt)) {
        if ("multiple" == lopt) {
            if (list_) {
                list_->disallow_multiple_select();
            }
        }

        else if ("backward" == lopt) {
            update_sort_marker();
            refresh();
        }

        else if ("hidden" == lopt) {
            if (list_ && !holders_.empty()) {
                for (auto & rec: holders_.front().dirs_) {
                    if (rec.hidden_) {
                        for (auto wp: list_->widgets(rec.y_)) {
                            wp->hide();
                        }
                    }
                }

                for (auto & rec: holders_.front().recs_) {
                    if (rec.hidden_) {
                        for (auto wp: list_->widgets(rec.y_)) {
                            wp->hide();
                        }
                    }
                }
            }
        }

        else {
            refresh();
        }

        limit_name_column();
    }
}

bool Navigator_impl::has_option(std::string_view opt) const {
    return options_.contains(str_tolower(opt));
}

ustring Navigator_impl::filter() const {
    return str_implode(filters_, ',');
}

std::string Navigator_impl::options(char32_t sep) const {
    std::string res = "name";
    for (auto & s: options_) { res += sep; res += s; }
    return res;
}

void Navigator_impl::on_icon_size() {
    // TODO Loop over images and reassign pixmaps.
    begin_read();
}

void Navigator_impl::zoom_in() {
    int px = conf().integer(Conf::ICON_SIZE);
    if (0 == px) { px = Icon::MEDIUM; }

    switch (px) {
        case Icon::SMALL: px = Icon::MEDIUM; break;
        case Icon::MEDIUM: px = Icon::LARGE; break;
        case Icon::LARGE: px = Icon::HEAVY; break;
    }

    conf().integer(Conf::ICON_SIZE) = px;
}

void Navigator_impl::zoom_out() {
    int px = conf().integer(Conf::ICON_SIZE);
    if (0 == px) { px = theme_->icon_pixels(px); }

    switch (px) {
        case Icon::HEAVY: px = Icon::LARGE; break;
        case Icon::LARGE: px = Icon::MEDIUM; break;
        case Icon::MEDIUM: px = Icon::SMALL; break;
    }

    conf().integer(Conf::ICON_SIZE) = px;
}

bool Navigator_impl::on_mouse_wheel(int delta, int mm, const Point & pt) {
    if (MM_CONTROL & mm) {
        if (delta > 0) { zoom_in(); }
        else { zoom_out(); }
        return true;
    }

    return false;
}

void Navigator_impl::on_tick() {
    progress_->set_format(str_format(std::locale(), (cancel_ax_.enabled() ? "%$%% - " : ""), std::setprecision(2), std::fixed, double(Timeval::now()-t1_)/1000000, " s"));
}

void Navigator_impl::on_cancel_disable() {
    hide_cx_ = Loop_impl::this_ptr()->alarm(fun(progress_, &Widget_impl::hide), 5511);
}

void Navigator_impl::on_file_select(const ustring & filename) {
    if (!holders_.empty()) { holders_.front().name_ = filename; }
    signal_file_select_(filename);
    update_sel_counter();
}

void Navigator_impl::on_file_unselect(const ustring & filename) {
    if (display()) { signal_file_unselect_(filename); }
    update_sel_counter();
}

void Navigator_impl::on_list_move_row(int o, int n) {
    if (!holders_.empty()) {
        auto & hol = holders_.front();
        auto i = std::find_if(hol.dirs_.begin(), hol.dirs_.end(), [o](auto & rec) { return o == rec.y_; });
        if (i != hol.dirs_.end()) { i->y_ = n; return; }
        i = std::find_if(hol.recs_.begin(), hol.recs_.end(), [o](auto & rec) { return o == rec.y_; });
        if (i != hol.recs_.end()) { i->y_ = n; }
    }
}

void Navigator_impl::on_watch(unsigned event, const ustring & path) {
    auto dir = path_dirname(path);
    auto i = std::find_if(holders_.begin(), holders_.end(), [&dir](auto & hol) { return path_same(hol.path_, dir); });

    if (i != holders_.end()) {
        i->mask_ |= event;
        if (i->tcx_.empty()) { i->tcx_ = Loop_impl::this_ptr()->alarm(bind_back(fun(this, &Navigator_impl::on_watch_timer), path), WATCH_DELAY); }
    }
}

void Navigator_impl::on_watch_timer(const ustring & path) {
    auto dir = path_dirname(path), name = path_notdir(path);
    auto h = std::find_if(holders_.begin(), holders_.end(), [&dir](auto & hol) { return path_same(hol.path_, dir); });

    if (h != holders_.end()) {
        auto & hol = *h;

        if (hol.mask_ & (File::DELETED|File::MOVED_OUT)) {
            std::erase(hol.files_, name);
            auto i = std::find_if(hol.recs_.begin(), hol.recs_.end(), [name](auto & rec) { return name == rec.name_; });

            if (i != hol.recs_.end()) {
                if (!holders_.empty() && &hol == &holders_.front() && list_) { list_->remove(i->y_); }
                hol.recs_.erase(i);
                return;
            }

            i = std::find_if(hol.dirs_.begin(), hol.dirs_.end(), [name](auto & rec) { return name == rec.name_; });

            if (i != hol.dirs_.end()) {
                if (!holders_.empty() && &hol == &holders_.front() && list_) { list_->remove(i->y_); }
                hol.dirs_.erase(i);
                return;
            }
        }

        else if (hol.mask_ & (File::MOVED_IN|File::CREATED)) {
            hol.files_.push_back(name);
            Rec & r2 = make_record(name);

            if (r2.dir_) {
                r2.size_ = list_dir(path).size();
                auto i = std::find_if(hol.dirs_.begin(), hol.dirs_.end(), [&r2, this](auto & r1) { return &r1 != &r2 && !compare(r1, r2); });
                if (i != hol.dirs_.end() && !holders_.empty() && &hol == &holders_.front() && list_) { list_record(r2, i->y_); list_objects(r2); }
            }

            else {
                auto i = std::find_if(hol.recs_.begin(), hol.recs_.end(), [&r2, this](auto & r1) { return &r1 != &r2 && !compare(r1, r2); });
                if (i != hol.recs_.end() && !holders_.empty() && &hol == &holders_.front() && list_) { list_record(r2, i->y_); }
            }
        }

        hol.mask_ = File::NOTHING;
    }
}

} // namespace tau

//END

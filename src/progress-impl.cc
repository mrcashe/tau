// ----------------------------------------------------------------------------
// SPDX-License-Identifier: BSD-2-Clause
// Copyright © 2014-2024 Konstantin Shmelkov <mrcashe@gmail.com>.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
// THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
// PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
// CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
// EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
// PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
// OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
// WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
// OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
// EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// ----------------------------------------------------------------------------

#include <tau/brush.hh>
#include <tau/font.hh>
#include <tau/painter.hh>
#include <tau/string.hh>
#include <progress-impl.hh>
#include <cmath>
#include <iomanip>
#include <iostream>

namespace tau {

Progress_impl::Progress_impl(Border bs, bool vertical):
    Frame_impl(bs),
    vertical_(vertical)
{
    area_ = std::make_shared<Widget_impl>();
    insert(area_);

    area_->conf().signal_changed(Conf::PROGRESS_FOREGROUND).connect(fun(this, &Progress_impl::paint_now));
    area_->conf().signal_changed(Conf::BACKGROUND).connect(fun(this, &Progress_impl::paint_now));

    signal_paint().connect(fun(this, &Progress_impl::on_paint));
    signal_display_in_.connect(fun(this, &Progress_impl::on_display_in));
}

void Progress_impl::set_value(double value) {
    double cval = std::max(std::min(value, max_value_), min_value_);

    if (value_ != cval) {
        value_ = cval;
        format_str();
        calc_hints();
        paint_now();
    }
}

void Progress_impl::set_min_value(double min_value) {
    if (min_value < max_value_) {
        if (min_value_ != min_value) {
            min_value_ = min_value;

            if (value_ < min_value_) {
                value_ = min_value_;
                format_str();
                calc_hints();
                paint_now();
            }
        }
    }
}

void Progress_impl::set_max_value(double max_value) {
    if (max_value > min_value_) {
        if (max_value_ != max_value) {
            max_value_ = max_value;

            if (value_ > max_value_) {
                value_ = max_value_;
                format_str();
                calc_hints();
                paint_now();
            }
        }
    }
}

void Progress_impl::set_precision(int prec) {
    if (precision_ != prec) {
        precision_ = prec;

        if (!fmt_.empty()) {
            format_str();
            calc_hints();
            paint_now();
        }
    }
}

void Progress_impl::set_format(const ustring & fmt) {
    if (fmt_ != fmt) {
        fmt_ = fmt;
        format_str();
        calc_hints();
        paint_now();
    }
}

void Progress_impl::text_align(Align align) {
    if (align != text_align_) {
        text_align_ = align;
        paint_now();
    }
}

void Progress_impl::paint_now() {
    if (Painter pr = painter()) {
        redraw(pr);
    }
}

void Progress_impl::redraw(Painter pr) {
    Rect r(size());
    if (border_visible()) { r.grow(-2, -2), r.translate(1, 1); }

    if (r) {
        int radius = 71*border_top_left_radius()/100;
        double range = max_value_-min_value_;
        pr.rectangle(r.left(), r.top(), r.right()+1, r.bottom()+1, radius);
        pr.set_brush(conf().brush(Conf::BACKGROUND));
        pr.fill();

        if (range > 0) {
            double units_per_pixel, value = std::max(std::min(max_value_, value_), min_value_);
            int x1, y1, x2, y2;

            if (vertical_) {
                units_per_pixel = range/r.height();
                double dy2 = (value-min_value_)/units_per_pixel;
                x1 = r.left(), y2 = r.bottom(), x2 = r.right()+1, y1 = y2-dy2;
            }

            else {
                units_per_pixel = range/r.width();
                double dx2 = (value-min_value_)/units_per_pixel;
                x1 = r.left(), y1 = r.top(), x2 = x1+dx2, y2 = r.bottom()+1;
            }

            if (x1 < x2 && y1 < y2) {
                pr.rectangle(x1, y1, x2, y2, radius);
                pr.set_brush(conf().brush(Conf::PROGRESS_BACKGROUND));
                pr.fill();
            }

            if (!msg_.empty()) {
                if (auto font = pr.select_font(conf().font().spec())) {
                    int ascent = font ? std::ceil(font.ascent()) : text_size_.height();
                    int x = r.left(), y = ascent+((r.height()-text_size_.height())/2);

                    if (r.width() > text_size_.width()) {
                        if (Align::END == text_align_) { x = r.width()-text_size_.width(); }
                        else if (Align::CENTER == text_align_) { x = (r.width()-text_size_.width())/2; }
                    }

                    pr.move_to(x, y);
                    pr.text(msg_, conf().color(Conf::PROGRESS_FOREGROUND));
                    pr.stroke();
                }
            }
        }
    }
}

void Progress_impl::format_str() {
    msg_.clear();
    ustring::size_type i = 0, n, len = fmt_.size();

    if (0 != len) {
        for (;;) {
            n = fmt_.find('%', i);

            if (ustring::npos == n) {
                msg_ += fmt_.substr(i);
                break;
            }

            msg_ += fmt_.substr(i, n-i);

            if (n < len-1) {
                char32_t wc = fmt_[n+1];

                switch (wc) {
                    case '%':
                        msg_ += '%';
                        i = n+2;
                        break;

                    case '$':
                        msg_ += str_format(std::setprecision(precision_), std::fixed, 100.0*std::fabs(value_-min_value_)/std::fabs(max_value_-min_value_));
                        i = n+2;
                        break;

                    case '_':
                        msg_ += str_format(std::setprecision(precision_), std::fixed, min_value_);
                        i = n+2;
                        break;

                    case '^':
                        msg_ += str_format(std::setprecision(precision_), std::fixed, max_value_);
                        i = n+2;
                        break;

                    default:
                        i = n+1;
                }
            }
        }
    }
}

void Progress_impl::calc_hints() {
    Size sz;

    if (!msg_.empty()) {
        if (Painter pr = painter()) {
            pr.select_font(conf().font().spec());
            Vector v = pr.text_size(msg_);
            text_size_.set(std::ceil(v.x()), std::ceil(v.y()));
            sz.set(text_size_.width()+2, text_size_.height()+2);
        }
    }

    else {
        if (vertical_) { sz.set(4, 0); }
        else { sz.set(0, 4); }
    }

    area_->hint_size(sz);
}

void Progress_impl::on_display_in() {
    set_border_radius(conf().integer(Conf::RADIUS));
    format_str();
    calc_hints();
}

bool Progress_impl::on_paint(Painter pr, const Rect & inval) {
    redraw(pr);
    return false;
}

} // namespace tau

//END

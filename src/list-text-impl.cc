// ----------------------------------------------------------------------------
// SPDX-License-Identifier: BSD-2-Clause
// Copyright © 2014-2024 Konstantin Shmelkov <mrcashe@gmail.com>.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
// THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
// PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
// CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
// EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
// PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
// OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
// WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
// OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
// EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// ----------------------------------------------------------------------------

#include <tau/exception.hh>
#include <tau/string.hh>
#include <list-text-impl.hh>
#include <label-impl.hh>
#include <iostream>

namespace tau {

List_text_impl::List_text_impl(Align align):
    List_impl(),
    align_(align)
{
    init();
}

List_text_impl::List_text_impl(unsigned spacing, Align align):
    List_impl(spacing),
    align_(align)
{
    init();
}

List_text_impl::List_text_impl(unsigned xspacing, unsigned yspacing, Align align):
    List_impl(xspacing, yspacing),
    align_(align)
{
    init();
}

List_text_impl::List_text_impl(const std::vector<ustring> & sv, Align align):
    List_impl(),
    align_(align)
{
    init();
    append(sv);
}

List_text_impl::List_text_impl(const std::vector<ustring> & sv, unsigned spacing, Align align):
    List_impl(spacing),
    align_(align)
{
    init();
    append(sv);
}

List_text_impl::List_text_impl(const std::vector<ustring> & sv, unsigned xspacing, unsigned yspacing, Align align):
    List_impl(xspacing, yspacing),
    align_(align)
{
    init();
    append(sv);
}

void List_text_impl::init() {
    signal_select_row().connect(fun(this, &List_text_impl::on_row_selected));
    signal_activate_row().connect(fun(this, &List_text_impl::on_row_activated));
    signal_remove_row().connect(fun(this, &List_text_impl::on_row_removed));
    signal_move_row().connect(fun(this, &List_text_impl::on_row_moved));
    signal_children_changed_.connect(fun(this, &List_text_impl::on_children_changed));
    action_edit_.disable();
    connect_action(action_edit_);
}

int List_text_impl::append(const ustring & str, bool shrink) {
    int y = INT_MIN;
    std::u32string ws(str);

    if (signal_approve_(ws) && signal_validate_(ws)) {
        auto tp = new_holder(ws);
        y = append_row(tp, shrink);
        holders_[y] = tp.get();
    }

    return y;
}

int List_text_impl::append(const ustring & str, Align align, bool shrink) {
    int y = INT_MIN;
    std::u32string ws(str);

    if (signal_approve_(ws) && signal_validate_(ws)) {
        auto tp = new_holder(ws);
        y = append_row(tp, align, shrink);
        holders_[y] = tp.get();
    }

    return y;
}

int List_text_impl::append(const std::vector<ustring> & sv, bool shrink) {
    int first = INT_MIN;

    for (auto & s: sv) {
        int y = append(s, shrink);
        if (INT_MIN == y) { break; }
        if (INT_MIN == first) { first = y; }
    }

    return first;
}

int List_text_impl::append(const std::vector<ustring> & sv, Align align, bool shrink) {
    int first = INT_MIN;

    for (auto & s: sv) {
        int y = append(s, align, shrink);
        if (INT_MIN == y) { break; }
        if (INT_MIN == first) { first = y; }
    }

    return first;
}

int List_text_impl::prepend(const ustring & str, bool shrink) {
    int y = INT_MIN;
    std::u32string ws(str);

    if (signal_approve_(ws) && signal_validate_(ws)) {
        auto tp = new_holder(ws);
        y = prepend_row(tp, shrink);
        holders_[y] = tp.get();
    }

    return y;
}

int List_text_impl::prepend(const ustring & str, Align align, bool shrink) {
    int y = INT_MIN;
    std::u32string ws(str);

    if (signal_approve_(ws) && signal_validate_(ws)) {
        auto tp = new_holder(ws);
        y = prepend_row(tp, align, shrink);
        holders_[y] = tp.get();
    }

    return y;
}

int List_text_impl::prepend(const std::vector<ustring> & sv, bool shrink) {
    int first = INT_MIN;

    for (auto & s: sv) {
        int y = prepend(s, shrink);
        if (INT_MIN == y) { break; }
        if (INT_MIN == first) { first = y; }
    }

    return first;
}

int List_text_impl::prepend(const std::vector<ustring> & sv, Align align, bool shrink) {
    int first = INT_MIN;

    for (auto & s: sv) {
        int y = prepend(s, align, shrink);
        if (INT_MIN == y) { break; }
        if (INT_MIN == first) { first = y; }
    }

    return first;
}

int List_text_impl::insert(const ustring & str, int row, bool shrink) {
    int y = INT_MIN;
    std::u32string ws(str);

    if (signal_approve_(ws) && signal_validate_(ws)) {
        auto tp = new_holder(ws);
        y = insert_row(tp, row, shrink);
        holders_[y] = tp.get();
    }

    return y;
}

int List_text_impl::insert(const ustring & str, int row, Align align, bool shrink) {
    int y = INT_MIN;
    std::u32string ws(str);

    if (signal_approve_(ws) && signal_validate_(ws)) {
        auto tp = new_holder(ws);
        y = insert_row(tp, row, align, shrink);
        holders_[y] = tp.get();
    }

    return y;
}

int List_text_impl::insert(const std::vector<ustring> & sv, int row, bool shrink) {
    int first = INT_MIN;

    for (auto & s: sv) {
        int y = insert(s, row++, shrink);
        if (INT_MIN == y) { break; }
        if (INT_MIN == first) { first = y; }
    }

    return first;
}

int List_text_impl::insert(const std::vector<ustring> & sv, int row, Align align, bool shrink) {
    int first = INT_MIN;

    for (auto & s: sv) {
        int y = insert(s, row++, align, shrink);
        if (INT_MIN == y) { break; }
        if (INT_MIN == first) { first = y; }
    }

    return first;
}

int List_text_impl::insert_before(const ustring & str, const ustring & other, bool shrink) {
    auto i = std::find_if(holders_.begin(), holders_.end(), [other](auto & p) { return p.second->str() == other; });
    return i != holders_.end() ? insert(str, i->first-1, shrink) : prepend(str, shrink);
}

int List_text_impl::insert_before(const ustring & str, const ustring & other, Align align, bool shrink) {
    auto i = std::find_if(holders_.begin(), holders_.end(), [other](auto & p) { return p.second->str() == other; });
    return i != holders_.end() ? insert(str, i->first-1, align, shrink) : prepend(str, align, shrink);
}

int List_text_impl::insert_before(const std::vector<ustring> & sv, const ustring & other, bool shrink) {
    auto i = std::find_if(holders_.begin(), holders_.end(), [other](auto & p) { return p.second->str() == other; });
    return i != holders_.end() ? insert(sv, i->first-1, shrink) : prepend(sv, shrink);
}

int List_text_impl::insert_before(const std::vector<ustring> & sv, const ustring & other, Align align, bool shrink) {
    auto i = std::find_if(holders_.begin(), holders_.end(), [other](auto & p) { return p.second->str() == other; });
    return i != holders_.end() ? insert(sv, i->first-1, align, shrink) : prepend(sv, align, shrink);
}

int List_text_impl::insert_after(const ustring & str, const ustring & other, bool shrink) {
    auto i = std::find_if(holders_.begin(), holders_.end(), [other](auto & p) { return p.second->str() == other; });
    return i != holders_.end() ? insert(str, 1+i->first, shrink) : append(str, shrink);
}

int List_text_impl::insert_after(const ustring & str, const ustring & other, Align align, bool shrink) {
    auto i = std::find_if(holders_.begin(), holders_.end(), [other](auto & p) { return p.second->str() == other; });
    return i != holders_.end() ? insert(str, 1+i->first, align, shrink) : append(str, align, shrink);
}

int List_text_impl::insert_after(const std::vector<ustring> & sv, const ustring & other, bool shrink) {
    auto i = std::find_if(holders_.begin(), holders_.end(), [other](auto & p) { return p.second->str() == other; });
    return i != holders_.end() ? insert(sv, 1+i->first, shrink) : append(sv, shrink);
}

int List_text_impl::insert_after(const std::vector<ustring> & sv, const ustring & other, Align align, bool shrink) {
    auto i = std::find_if(holders_.begin(), holders_.end(), [other](auto & p) { return p.second->str() == other; });
    return i != holders_.end() ? insert(sv, 1+i->first, align, shrink) : append(sv, align, shrink);
}

int List_text_impl::insert(int y, const ustring & s, int x, bool shrink) {
    return List_impl::insert(y, std::make_shared<Label_impl>(s), x, shrink);
}

int List_text_impl::insert(int y, const ustring & s, int x, Align align, bool shrink) {
    return List_impl::insert(y, std::make_shared<Label_impl>(s), x, align, shrink);
}

int List_text_impl::select(const ustring & str, bool fix) {
    std::u32string wstr(str);
    auto i = std::find_if(holders_.begin(), holders_.end(), [&wstr](auto & p) { return wstr == p.second->wstr(); } );
    return i != holders_.end() ? List_impl::select(i->first, fix) : INT_MIN;
}

int List_text_impl::select_similar(const ustring & str, bool fix) {
    std::u32string wstr(str);
    auto i = std::find_if(holders_.begin(), holders_.end(), [&wstr](auto & p) { return str_similar(wstr, p.second->wstr()); } );
    return i != holders_.end() ? List_impl::select(i->first, fix) : INT_MIN;
}

ustring List_text_impl::str(int y) const {
    auto i = holders_.find(y);
    return i != holders_.end() ? i->second->str() : ustring();
}

std::u32string List_text_impl::wstr(int y) const {
    auto i = holders_.find(y);
    return i != holders_.end() ? i->second->wstr() : std::u32string();
}

ustring List_text_impl::str() const {
    auto i = holders_.find(current());
    return i != holders_.end() ? i->second->str() : ustring();
}

// TODO replace str_ by std::u32string.
std::u32string List_text_impl::wstr() const {
    auto i = holders_.find(current());
    return i != holders_.end() ? i->second->wstr() : std::u32string();
}

std::vector<ustring> List_text_impl::strings() const {
    std::vector<ustring> v;
    for (int row: selection()) { v.emplace_back(str(row)); }
    return v;
}

std::vector<std::u32string> List_text_impl::wstrings() const {
    std::vector<std::u32string> v;
    for (int row: selection()) { v.emplace_back(wstr(row)); }
    return v;
}

void List_text_impl::text_align(Align align) {
    if (align_ != align) {
        align_ = align;
        for (auto & p: holders_) { p.second->text_align(align_); }
    }
}

void List_text_impl::on_row_selected(int y) {
    if (!signal_select_text_.empty()) {
        std::u32string ws;
        auto i = holders_.find(y);
        if (i != holders_.end()) { ws = i->second->wstr(); }
        signal_select_text_(ws);
    }
}

void List_text_impl::on_row_activated(int y) {
    if (!signal_activate_text_.empty()) {
        auto i = holders_.find(y);
        if (i != holders_.end() && !i->second->empty()) { signal_activate_text_(i->second->str()); }
    }
}

void List_text_impl::on_row_removed(int y) {
    auto i = holders_.find(y);

    if (i != holders_.end()) {
        if (!signal_remove_text_.empty()) {
            auto ws = i->second->wstr();
            signal_remove_text_(y, ws);
        }

        holders_.erase(i);
    }
}

void List_text_impl::on_row_moved(int y, int ynew) {
    auto i = holders_.find(y);

    if (i != holders_.end()) {
        auto tp = i->second;
        holders_.erase(i);
        holders_[ynew] = tp;
    }
}

std::size_t List_text_impl::remove(const ustring & s, bool similar) {
    std::size_t n = 0;

    for (;;) {
        Holders::iterator i;

        if (similar) {
            i = std::find_if(holders_.begin(), holders_.end(), [s](auto & p) { return str_similar(s, p.second->str()); });
        }

        else {
            std::u32string ws(s);
            i = std::find_if(holders_.begin(), holders_.end(), [&ws](auto & p) { return ws == p.second->wstr(); });
        }

        if (i == holders_.end()) { break; }
        n += List_impl::remove(i->first);
    }

    return n;
}

int List_text_impl::find(const ustring & s, int ymin, int ymax) const {
    std::u32string ws(s);
    auto i = std::find_if(holders_.begin(), holders_.end(), [&ws, ymin, ymax](auto & p) { return p.first >= ymin && p.first < ymax && ws == p.second->wstr(); });
    return i != holders_.end() ? i->first : INT_MIN;
}

int List_text_impl::similar(const ustring & s, int ymin, int ymax) const {
    std::u32string ws(s);
    auto i = std::find_if(holders_.begin(), holders_.end(), [&ws, ymin, ymax](auto & p) { return p.first >= ymin && p.first < ymax && str_similar(ws, p.second->wstr()); });
    return i != holders_.end() ? i->first : INT_MIN;
}

int List_text_impl::like(const ustring & s, int ymin, int ymax) const {
    std::u32string ws(s);
    auto i = std::find_if(holders_.begin(), holders_.end(), [&ws, ymin, ymax](auto & p) { return p.first >= ymin && p.first < ymax && std::u32string::npos != str_like(ws, p.second->wstr()); });
    return i != holders_.end() ? i->first : INT_MIN;
}

bool List_text_impl::contains(const ustring & s, bool similar) const {
    return INT_MIN != (similar ? this->similar(s) : find(s));
}

void List_text_impl::wrap(Wrap wrap) {
    if (wrap_ != wrap) {
        wrap_ = wrap;
        for (auto & p: holders_) { p.second->wrap(wrap_); }
    }
}

Entry_ptr List_text_impl::new_holder(const std::u32string & ws) {
    auto tp = std::make_shared<Entry_impl>(ws, align_, Border::NONE);
    tp->wrap(wrap_);
    if (!editable_) { tp->disallow_edit(); }
    tp->signal_validate().connect(fun(signal_validate_));
    tp->signal_approve().connect(fun(signal_approve_));
    tp->signal_mouse_down().connect(fun(this, &List_text_impl::on_fake_mouse), true);
    tp->signal_mouse_up().connect(fun(this, &List_text_impl::on_fake_mouse), true);
    tp->signal_mouse_double_click().connect(fun(this, &List_text_impl::on_fake_mouse), true);
    tp->signal_changed().connect(bind_back(fun(this, &List_text_impl::on_entry_changed), tp.get()));
    tp->signal_parent().connect(tau::bind_front(fun(this, &List_text_impl::on_parent), tp.get()));
    return tp;
}

void List_text_impl::allow_edit() {
    editable_ = true;
    action_edit_.enable();
    for (auto & p: holders_) { p.second->allow_edit(); }
}

void List_text_impl::disallow_edit() {
    editable_ = false;
    action_edit_.disable();
    for (auto & p: holders_) { p.second->disallow_edit(); }
}

void List_text_impl::allow_edit(int y) {
    auto i = holders_.find(y);
    if (i != holders_.end()) { i->second->allow_edit(); }
}

void List_text_impl::disallow_edit(int y) {
    auto i = holders_.find(y);
    if (i != holders_.end()) { i->second->disallow_edit(); }
}

bool List_text_impl::editable(int y) const noexcept {
    auto i = holders_.find(y);
    return i != holders_.end() ? i->second->editable() : false;
}

void List_text_impl::allow_edit(const ustring & str) {
    auto i = std::find_if(holders_.begin(), holders_.end(), [str](auto & hol) { return hol.second->str() == str; });
    if (i != holders_.end()) { i->second->allow_edit(); }
}

void List_text_impl::disallow_edit(const ustring & str) {
    auto i = std::find_if(holders_.begin(), holders_.end(), [str](auto & hol) { return hol.second->str() == str; });
    if (i != holders_.end()) { i->second->disallow_edit(); }
}

bool List_text_impl::editable(const ustring & str) const noexcept {
    auto i = std::find_if(holders_.begin(), holders_.end(), [str](auto & hol) { return hol.second->str() == str; });
    return i != holders_.end() ? i->second->editable() : false;
}

// From action.
void List_text_impl::on_edit() {
    auto i = holders_.find(current());
    if (i != holders_.end() && i->second->editable()) { edit(i->second, i->first); }
}

// From programmer.
Widget_ptr List_text_impl::edit(int y) {
    auto i = holders_.find(y);
    return i != holders_.end() ? edit(i->second, i->first) : nullptr;
}

Widget_ptr List_text_impl::editor() {
    auto i = std::find_if(holders_.begin(), holders_.end(), [](auto & p) { return p.second->focused(); });
    return i != holders_.end() ? chptr(i->second) : nullptr;
}

Widget_cptr List_text_impl::editor() const {
    auto i = std::find_if(holders_.begin(), holders_.end(), [](auto & p) { return p.second->focused(); });
    return i != holders_.end() ? chptr(i->second) : nullptr;
}

// private.
Widget_ptr List_text_impl::edit(Entry_impl * tp, int y) {
    bool editable = tp->editable();
    tp->allow_edit();

    // Focus successful.
    if (tp->take_focus()) {
        unselect();
        offset(tp->to_parent(this));

        action_previous().disable();
        action_next().disable();
        action_previous_page().disable();
        action_next_page().disable();
        action_home().disable();
        action_end().disable();
        action_select_previous().disable();
        action_select_next().disable();
        action_select_previous_page().disable();
        action_select_next_page().disable();
        action_select_home().disable();
        action_select_end().disable();

        if (!editable) {
            edit_disallow_cx_ = tp->signal_focus_out().connect(fun(tp, &Entry_impl::disallow_edit));
            tp->signal_focus_out().connect(fun(edit_disallow_cx_, &connection::drop));
        }

        edit_activate_cx_ = tp->signal_activate().connect(tau::bind_front(fun(signal_activate_edit_), y));
        edit_cancel_cx_ = tp->action_cancel().connect(bind_back(fun(this, &List_text_impl::on_entry_cancel), y), true);
        edit_focus_out_cx_ = tp->signal_focus_out().connect(fun(this, &List_text_impl::end_edit));
        return chptr(tp);
    }

    // Failed to gain focus.
    else if (!editable) {
        tp->disallow_edit();
    }

    return nullptr;
}

void List_text_impl::on_entry_cancel(int row) {
    signal_cancel_edit_(row);
    end_edit();
}

void List_text_impl::on_entry_changed(Entry_impl * tp) {
    if (!signal_text_changed_.empty()) {
        auto i = std::find_if(holders_.begin(), holders_.end(), [tp](auto & p) { return tp == p.second; });
        if (i != holders_.end()) { signal_text_changed_(i->first, tp->wstr()); }
    }
}

void List_text_impl::end_edit() {
    action_previous().enable();
    action_next().enable();
    action_previous_page().enable();
    action_next_page().enable();
    action_home().enable();
    action_end().enable();
    action_select_previous().enable();
    action_select_next().enable();
    action_select_previous_page().enable();
    action_select_next_page().enable();
    action_select_home().enable();
    action_select_end().enable();
    edit_activate_cx_.drop();
    edit_cancel_cx_.drop();
    edit_focus_out_cx_.drop();
}

void List_text_impl::on_parent(Entry_impl * tp, Object * op) {
    if (!in_destroy() && !op) {
        auto i = std::find_if(holders_.begin(), holders_.end(), [tp](auto & p) { return p.second == tp; });
        if (i != holders_.end()) { signal_remove_text_(i->first, tp->wstr()); holders_.erase(i); }
    }
}

void List_text_impl::on_children_changed() {
    if (empty()) {
        holders_.clear();
    }
}

} // namespace tau

//END

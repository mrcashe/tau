// ----------------------------------------------------------------------------
// SPDX-License-Identifier: BSD-2-Clause
// Copyright © 2014-2024 Konstantin Shmelkov <mrcashe@gmail.com>.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
// THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
// PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
// CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
// EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
// PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
// OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
// WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
// OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
// EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// ----------------------------------------------------------------------------

#ifndef __TAU_LAYERED_CONTAINER_HH__
#define __TAU_LAYERED_CONTAINER_HH__

#include <container-impl.hh>
#include <map>

namespace tau {

class Layered_container: public Container_impl {
    friend Container;

public:

    // Overrides Widget_impl.
    void handle_paint(Painter pr, const Rect & inval) override;

    // Overrides Widget_impl.
    void handle_backpaint(Painter pr, const Rect & inval) override;

    // Overrides pure Container_impl.
    void on_child_viewable(Widget_impl * wp, bool yes) override;

    // Overrides pure Container_impl.
    void rise(Widget_impl * wp) override;

    // Overrides pure Container_impl.
    void lower(Widget_impl * wp) override;

    // Overrides pure Container_impl.
    int  level(const Widget_impl * wp) const noexcept override;

protected:

    Layered_container();

    // Overrides pure Container_impl.
    Widget_impl * mouse_target(const Point & pt) override;

private:

    using Viewables = std::map<int, std::vector<Widget_impl *>>;
    using Layers = std::map<Widget_impl *, int>;

    Viewables       viewables_;
    Layers          layers_;

private:

    void on_display_out();
    void on_child(Widget_ptr wp, int op);
};

} // namespace tau

#endif // __TAU_LAYERED_CONTAINER_HH__

// ----------------------------------------------------------------------------
// SPDX-License-Identifier: BSD-2-Clause
// Copyright © 2014-2024 Konstantin Shmelkov <mrcashe@gmail.com>.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
// THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
// PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
// CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
// EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
// PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
// OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
// WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
// OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
// EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// ----------------------------------------------------------------------------

#include <tau/exception.hh>
#include <display-impl.hh>
#include <loop-impl.hh>
#include <menubox-impl.hh>
#include <menu-item-impl.hh>
#include <scroller-impl.hh>
#include <algorithm>
#include <iostream>

namespace tau {

Menu_impl::Menu_impl(Orientation orient):
    Roller_impl(orient),
    orient_(orient)
{
    freeze();

    conf().boolean(Conf::BUTTON_LABEL) = false;
    conf().integer(Conf::ICON_SIZE) = Icon::SMALL;
    conf().redirect(Conf::BACKGROUND, Conf::MENU_BACKGROUND);
    conf().redirect(Conf::BUTTON_BACKGROUND, Conf::MENU_BACKGROUND);
    conf().redirect(Conf::FOREGROUND, tau::Conf::MENU_FOREGROUND);
    conf().redirect(Conf::FONT, tau::Conf::MENU_FONT);

    signal_focus_in_.connect(fun(this, &Menu_impl::mark));
    signal_display_in_.connect(fun(this, &Menu_impl::on_display_in));
    signal_mouse_down().connect(fun(this, &Menu_impl::on_mouse_down));
    signal_mouse_leave().connect(fun(this, &Menu_impl::drop_open));

    connect_action(action_enter_);
    connect_action(action_cancel_);
    connect_action(action_home_);
    connect_action(action_end_);
}

void Menu_impl::on_display_in() {
    if (has_enabled_items()) { thaw(); }
}

// Overrides Box_impl.
// Overriden by Menubar_impl.
// Overriden by Menubox_impl.
void Menu_impl::remove(Widget_impl * wp) {
    if (current_item_.get() == wp) { current_item_.reset(); }
    if (marked_item_ == wp) { marked_item_ = nullptr; }
}

// FIXME Redefines Roller_impl.
// Overriden by Menubar_impl.
// Overriden by Menubox_impl.
void Menu_impl::clear() {
    current_item_.reset();
    marked_item_ = nullptr;
}

Menu_item_ptr Menu_impl::current_item() {
    if (current_item_) { return current_item_; }
    int min_pos = INT_MAX;
    Menu_item_ptr min_item;

    for (auto wp: compound()->children()) {
        if (auto ip = std::dynamic_pointer_cast<Menu_item_impl>(wp)) {
            if (!ip->disabled() && !ip->hidden()) {
                int pos = horizontal() ? ip->origin().x() : ip->origin().y();
                if (pos < min_pos) { min_pos = pos, min_item = ip; }
            }
        }
    }

    return min_item;
}

Menu_item_ptr Menu_impl::next_item() {
    if (auto cur = current_item()) {
        int min_plus = INT_MAX, max_minus = INT_MAX;
        Menu_item_ptr ip_min_plus, ip_max_minus;

        for (auto wp: compound()->children()) {
            if (auto ip = std::dynamic_pointer_cast<Menu_item_impl>(wp)) {
                if (ip != cur && ip->enabled() && !ip->hidden()) {
                    int d = horizontal() ? ip->origin().x()-cur->origin().x() : ip->origin().y()-cur->origin().y();
                    if (d >= 0 && d < min_plus) { min_plus = d, ip_min_plus = ip; }
                    else if (d < max_minus) { max_minus = d, ip_max_minus = ip; }
                }
            }
        }

        if (ip_min_plus) {
            return ip_min_plus;
        }

        if (ip_max_minus) {
            return ip_max_minus;
        }
    }

    return nullptr;
}

Menu_item_ptr Menu_impl::previous_item() {
    if (auto cur = current_item()) {
        int max_plus = INT_MIN, min_minus = INT_MIN;
        Menu_item_ptr ip_max_plus, ip_min_minus;

        for (auto wp: compound()->children()) {
            if (auto ip = std::dynamic_pointer_cast<Menu_item_impl>(wp)) {
                if (ip != cur && ip->enabled() && !ip->hidden()) {
                    int d = horizontal() ? ip->origin().x()-cur->origin().x() : ip->origin().y()-cur->origin().y();
                    if (d >= 0 && d > max_plus) { max_plus = d, ip_max_plus = ip; }
                    else if (d > min_minus) { min_minus = d, ip_min_minus = ip; }
                }
            }
        }

        if (ip_min_minus) {
            return ip_min_minus;
        }

        if (ip_max_plus) {
            return ip_max_plus;
        }
    }

    return nullptr;
}

void Menu_impl::select_current() {
    select_item(current_item());
}

void Menu_impl::select_next() {
    if (auto ip = next_item()) {
        select_item(ip);
    }
}

void Menu_impl::select_prev() {
    if (auto ip = previous_item()) {
        select_item(ip);
    }
}

bool Menu_impl::emit_current() {
    if (auto item = current_item()) {
        if (item->enabled()) {
            if (auto check_impl = std::dynamic_pointer_cast<Check_menu_impl>(item)) {
                end_modal();
                check_impl->toggle();
                pass_quit();
                return true;
            }

            end_modal();
            item->activate();
            return true;
        }
    }

    return false;
}

bool Menu_impl::open_current() {
    if (enabled() && current_item_ && !submenu_) {
        if (auto sub = std::dynamic_pointer_cast<Submenu_impl>(current_item_)) {
            if (auto mp = std::dynamic_pointer_cast<Menubox_impl>(sub->menu())) {
                if (!mp->disabled() && !mp->has_parent() && mp->has_enabled_items()) {
                    if (auto wip = toplevel()) {
                        Point pos;
                        Gravity gravity;

                        if (Orientation::RIGHT == orient_) {
                            pos = current_item_->to_parent(wip, Point(0, current_item_->size().height()));
                            gravity = Gravity::TOP_LEFT;
                        }

                        else {
                            int y = current_item_->origin().y();
                            Point p1 = to_parent(wip, Point(size().width()+margin_hint().right, y));
                            Point p2 = to_parent(wip, Point(-margin_hint().left, y));
                            if (wip->size().iwidth()-p1.x() >= p2.x()) { pos = p1; gravity = Gravity::TOP_LEFT; }
                            else { pos = p2; gravity = Gravity::TOP_RIGHT; }
                        }

                        submenu_ = mp;
                        end_modal();
                        ungrab_mouse();

                        try {
                            mp->popup(wip, mp, pos, gravity, this);
                            return true;
                        }

                        catch (exception & x) {
                            pass_quit();
                            std::cerr << __func__ << ": " << x.what() << std::endl;
                        }
                    }
                }
            }
        }
    }

    return false;
}

void Menu_impl::activate_current() {
    if (current_item_) {
        if (!open_current()) {
            emit_current();
        }
    }
}

void Menu_impl::unselect_current() {
    close_submenu();
    unmark();
    current_item_ = nullptr;
}

Menu_item_ptr Menu_impl::select_item(Menu_item_ptr ip) {
    if (ip && ip->enabled() && !ip->hidden()) {
        unselect_current();
        scroller()->pan(ip.get());
        current_item_ = ip;
        mark();
    }

    return current_item_;
}

Menu_item_ptr Menu_impl::item_ptr(Widget_impl * wp) {
    return std::dynamic_pointer_cast<Menu_item_impl>(compound()->chptr(wp));
}

void Menu_impl::on_item_enable(Menu_item_impl * ip, bool yes) {
    if (yes) {
        thaw();
    }

    else if (!has_enabled_items()) {
        freeze();
    }

    if (!yes && ip == current_item_.get()) { unselect_current(); }
}

void Menu_impl::on_open_timer() {
    open_current();
}

void Menu_impl::on_item_enter(const Point & pt, Widget_impl * wp) {
    if (window() && window()->focused()) {
        if (auto ip = item_ptr(wp)) {
            select_item(ip);

            if (auto sip = std::dynamic_pointer_cast<Submenu_impl>(ip)) {
                if (sip->enabled()) {
                    if (auto dp = display()) {
                        if (auto loop = dp->loop()) {
                            open_cx_ = loop->alarm(fun(this, &Menu_impl::on_open_timer), 938);
                        }
                    }
                }
            }
        }
    }
}

bool Menu_impl::on_item_mouse_down(int mbt, int mm, const Point &, Widget_impl * wp) {
    if (MBT_LEFT == mbt && MM_NONE == mm) {
        if (auto ip = item_ptr(wp)) {
            select_item(ip);
            activate_current();
            return true;
        }
    }

    return false;
}

bool Menu_impl::on_mouse_down(int mbt, int mm, const Point & pt) {
    if (auto wip = window()) {
        if (!Rect(wip->size()).contains(pt)) {
            if (auto m = hover_menu(pt)) {
                end_modal();
                ungrab_mouse();
                m->close_submenu();
                m->grab_modal();
                m->grab_mouse();
                return m->signal_mouse_down()(mbt, mm, wip->to_screen(pt)-m->to_screen());
            }

            pass_quit();
        }

        else {
            if (auto ip = hover_item(pt)) {
                select_item(ip);
                activate_current();
            }
        }

        return true;
    }

    return false;
}

void Menu_impl::add(Widget_ptr wp) {
    if (auto ip = std::dynamic_pointer_cast<Menu_item_impl>(wp)) {
        ip->signal_enable().connect(bind_back(fun(this, &Menu_impl::on_item_enable), ip.get(), true));
        ip->signal_disable().connect(bind_back(fun(this, &Menu_impl::on_item_enable), ip.get(), false));
        ip->signal_mouse_down().connect(bind_back(fun(this, &Menu_impl::on_item_mouse_down), ip.get()));
        ip->signal_mouse_enter().connect(bind_back(fun(this, &Menu_impl::on_item_enter), ip.get()));
        ip->signal_select().connect(fun(this, &Menu_impl::drop_open));
        ip->set_string(MENU_ITEM_TAG);
        ip->signal_activate().connect(fun(this, &Menu_impl::pass_quit));
        signal_enabled_items_.connect(fun(ip, &Widget_impl::disabled));
        if (!ip->disabled()) { on_item_enable(ip.get(), true); }
    }
}

void Menu_impl::mark() {
    unmark();

    if (current_item_) {
        marked_item_ = current_item_.get();
        mark_item(marked_item_, true);
    }
}

void Menu_impl::unmark() {
    if (marked_item_) {
        mark_item(marked_item_, false);
        marked_item_ = nullptr;
    }
}

bool Menu_impl::has_enabled_items() const noexcept {
    return !signal_enabled_items_();
}

void Menu_impl::cancel() {
    auto pmenu = unset_parent_menu();
    end_modal();
    ungrab_mouse();
    quit_menu();
    if (pmenu) { pmenu->child_menu_cancel(); }
}

// Overridden by Menubox_impl.
void Menu_impl::quit_menu() {
    close_submenu();
    unselect_current();
    unset_parent_menu();
    signal_quit_();
}

void Menu_impl::close_submenu() {
    if (auto sm = submenu_) {
        submenu_.reset();
        sm->quit_menu();
    }
}

void Menu_impl::reset_submenu() {
    if (auto sm = submenu_) {
        submenu_.reset();
        sm->unset_parent_menu();
    }
}

Menu_impl * Menu_impl::unset_parent_menu() {
    auto p = pmenu_;
    pmenu_ = nullptr;
    return p;
}

void Menu_impl::pass_quit() {
    auto pmenu = unset_parent_menu();
    end_modal();
    ungrab_mouse();
    quit_menu();
    if (pmenu) { pmenu->pass_quit(); }
}

void Menu_impl::on_home() {
    int min = INT_MAX;
    Menu_item_ptr first;

    for (auto wp: compound()->children()) {
        if (auto ip = std::dynamic_pointer_cast<Menu_item_impl>(wp)) {
            if (ip->enabled()) {
                if (horizontal()) {
                    int x = ip->origin().x();
                    if (x < min) { first = ip; min = x; }
                }

                else {
                    int y = ip->origin().y();
                    if (y < min) { first = ip; min = y; }
                }
            }
        }
    }

    if (first) { select_item(first); }
}

void Menu_impl::on_end() {
    int max = INT_MIN;
    Menu_item_ptr last;

    for (auto wp: compound()->children()) {
        if (auto ip = std::dynamic_pointer_cast<Menu_item_impl>(wp)) {
            if (ip->enabled()) {
                if (horizontal()) {
                    int x = ip->origin().x();
                    if (x > max) { last = ip; max = x; }
                }

                else {
                    int y = ip->origin().y();
                    if (y > max) { last = ip; max = y; }
                }
            }
        }
    }

    if (last) { select_item(last); }
}

Menu_impl * Menu_impl::hover_menu(const Point & pt) noexcept {
    if (auto wip = window()) {
        if (!Rect(wip->size()).contains(pt)) {
            Point spt = wip->to_screen(pt);

            for (auto m = parent_menu(); m; m = m->parent_menu()) {
                if (Rect(m->to_screen(), m->size()).contains(spt)) {
                    return m;
                }
            }
        }
    }

    return nullptr;
}

Menu_item_ptr Menu_impl::hover_item(const Point & pt) noexcept {
    for (auto wp: compound()->children()) {
        if (auto ip = std::dynamic_pointer_cast<Menu_item_impl>(wp)) {
            if (Rect(ip->origin(), ip->size()).contains(pt)) {
                return ip;
            }
        }
    }

    return nullptr;
}

const char * MENU_ITEM_TAG = "Menu_item";

} // namespace tau

//END

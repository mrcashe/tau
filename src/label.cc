// ----------------------------------------------------------------------------
// SPDX-License-Identifier: BSD-2-Clause
// Copyright © 2014-2024 Konstantin Shmelkov <mrcashe@gmail.com>.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
// THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
// PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
// CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
// EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
// PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
// OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
// WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
// OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
// EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// ----------------------------------------------------------------------------

#include <tau/label.hh>
#include <label-impl.hh>

namespace tau {

#define LABEL_IMPL (std::static_pointer_cast<Label_impl>(impl))

Label::Label():
    Widget(std::make_shared<Label_impl>())
{
}

Label::Label(const Label & other):
    Widget(other.impl)
{
}

Label & Label::operator=(const Label & other) {
    Widget::operator=(other);
    return *this;
}

Label::Label(Label && other):
    Widget(other.impl)
{
}

Label & Label::operator=(Label && other) {
    Widget::operator=(other);
    return *this;
}

Label::Label(Align halign, Align valign):
    Widget(std::make_shared<Label_impl>(halign, valign))
{
}

Label::Label(const ustring & s, Align halign, Align valign):
    Widget(std::make_shared<Label_impl>(s, halign, valign))
{
}

Label::Label(const std::u32string & ws, Align halign, Align valign):
    Widget(std::make_shared<Label_impl>(ws, halign, valign))
{
}

Label::Label(Buffer buf, Align halign, Align valign):
    Widget(std::make_shared<Label_impl>(buf, halign, valign))
{
}

Label::Label(Widget_ptr wp):
    Widget(std::dynamic_pointer_cast<Label_impl>(wp))
{
}

Label & Label::operator=(Widget_ptr wp) {
    Widget::operator=(std::dynamic_pointer_cast<Label_impl>(wp));
    return *this;
}

void Label::set_buffer(Buffer buf) {
    LABEL_IMPL->set_buffer(buf);
}

Buffer Label::buffer() {
    return LABEL_IMPL->buffer();
}

const Buffer Label::buffer() const {
    return LABEL_IMPL->buffer();
}

void Label::assign(const ustring & s) {
    LABEL_IMPL->assign(s);
}

void Label::assign(const std::u32string & ws) {
    LABEL_IMPL->assign(ws);
}

bool Label::empty() const noexcept {
    return LABEL_IMPL->empty();
}

std::size_t Label::rows() const noexcept {
    return LABEL_IMPL->rows();
}

ustring Label::str() const {
    return LABEL_IMPL->str();
}

std::u32string Label::wstr() const {
    return LABEL_IMPL->wstr();
}

void Label::clear() {
    LABEL_IMPL->clear();
}

Buffer_citer Label::iter(std::size_t row, std::size_t col) const {
    return LABEL_IMPL->iter(row, col);
}

Buffer_citer Label::iter(const Point & pt) const {
    return LABEL_IMPL->iter(pt);
}

Size Label::text_size(const ustring & s) {
    return LABEL_IMPL->text_size(s);
}

Size Label::text_size(const std::u32string & s) {
    return LABEL_IMPL->text_size(s);
}

void Label::set_spacing(unsigned spc) {
    LABEL_IMPL->set_spacing(spc);
}

unsigned Label::spacing() const noexcept {
    return LABEL_IMPL->spacing();
}

void Label::text_align(Align xalign, Align yalign) {
    LABEL_IMPL->text_align(xalign, yalign);
}

std::pair<Align, Align> Label::text_align() const {
    return LABEL_IMPL->text_align();
}

void Label::wrap(Wrap wrap) {
    LABEL_IMPL->wrap(wrap);
}

Wrap Label::wrap() const noexcept {
    return LABEL_IMPL->wrap();
}

int Label::x_at_col(std::size_t ri, std::size_t col) const noexcept {
    return LABEL_IMPL->x_at_col(ri, col);
}

std::size_t Label::col_at_x(std::size_t ri, int x) const noexcept {
    return LABEL_IMPL->col_at_x(ri, x);
}

std::size_t Label::row_at_y(int y) const noexcept {
    return LABEL_IMPL->row_at_y(y);
}

int Label::baseline(std::size_t ri) const noexcept {
    return LABEL_IMPL->baseline(ri);
}

Rect Label::row_bounds(std::size_t ri) const noexcept {
    return LABEL_IMPL->row_bounds(ri);
}

} // namespace tau

//END

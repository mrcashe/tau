// ----------------------------------------------------------------------------
// SPDX-License-Identifier: BSD-2-Clause
// Copyright © 2014-2024 Konstantin Shmelkov <mrcashe@gmail.com>.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
// THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
// PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
// CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
// EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
// PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
// OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
// WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
// OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
// EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// ----------------------------------------------------------------------------

#ifndef __TAU_ROLLER_IMPL_HH__
#define __TAU_ROLLER_IMPL_HH__

#include <bin-impl.hh>
#include <box-impl.hh>
#include <scroller-impl.hh>
#include <table-impl.hh>

namespace tau {

class Roller_impl: public Bin_impl {
public:

    Roller_impl(bool autohide=true);
    Roller_impl(Orientation orient, bool autohide=true);

    void allow_autohide();
    void disallow_autohide();
    bool autohide_allowed() const noexcept { return autohide_; }

    void insert(Widget_ptr wp);

    // FIXME Redefines Bin_impl.
    // FIXME Redefined by Menu_impl.
    void clear();

    bool empty() const noexcept;

    Scroller_impl * scroller() { return scroller_; }
    const Scroller_impl * scroller() const { return scroller_; }

    Widget_ptr scroller_ptr() noexcept { return box_ ? box_->chptr(scroller_) : table_->chptr(scroller_); }
    Widget_cptr scroller_ptr() const noexcept { return box_ ? box_->chptr(scroller_) : table_->chptr(scroller_); }

    void pan(int pos);
    void pan(Widget_impl * wp);
    int  pan() const noexcept;

    void set_step(int step);
    int  step() const noexcept;

    Container_impl * compound() noexcept override;
    const Container_impl * compound() const noexcept override;

private:

    Button_impl *   start_      = nullptr;
    Button_impl *   end_        = nullptr;
    Scroller_impl * scroller_;
    Box_impl *      box_        = nullptr;
    Table_impl *    table_      = nullptr;
    Slider_impl *   vslider_    = nullptr;
    Slider_impl *   hslider_    = nullptr;
    bool            autohide_;
    bool            horz_;
    connection      timer_cx_   { true };

private:

    void update_buttons();
    void on_start_button_click();
    void on_end_button_click();
    bool on_mouse_wheel(int d, unsigned mods, const Point & pt);
    void on_timer();
};

} // namespace tau

#endif // __TAU_ROLLER_IMPL_HH__

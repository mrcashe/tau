// ----------------------------------------------------------------------------
// SPDX-License-Identifier: BSD-2-Clause
// Copyright © 2014-2024 Konstantin Shmelkov <mrcashe@gmail.com>.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
// THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
// PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
// CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
// EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
// PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
// OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
// WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
// OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
// EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// ----------------------------------------------------------------------------

#include <tau/edit.hh>
#include <edit-impl.hh>

namespace tau {

#define EDIT_IMPL (std::static_pointer_cast<Edit_impl>(impl))

Edit::Edit():
    Text(std::make_shared<Edit_impl>())
{
}

Edit::Edit(const Edit & other):
    Text(other.impl)
{
}

Edit & Edit::operator=(const Edit & other) {
    Text::operator=(other);
    return *this;
}

Edit::Edit(Edit && other):
    Text(other.impl)
{
}

Edit & Edit::operator=(Edit && other) {
    Text::operator=(other);
    return *this;
}

Edit::Edit(Widget_ptr wp):
    Text(std::dynamic_pointer_cast<Edit_impl>(wp))
{
}

Edit & Edit::operator=(Widget_ptr wp) {
    Text::operator=(std::dynamic_pointer_cast<Edit_impl>(wp));
    return *this;
}

Edit::Edit(Align halign, Align valign):
    Text(std::static_pointer_cast<Widget_impl>(std::make_shared<Edit_impl>(halign, valign)))
{
}

Edit::Edit(const ustring & s, Align halign, Align valign):
    Text(std::static_pointer_cast<Widget_impl>(std::make_shared<Edit_impl>(s, halign, valign)))
{
}

Edit::Edit(Buffer buf, Align halign, Align valign):
    Text(std::static_pointer_cast<Widget_impl>(std::make_shared<Edit_impl>(buf, halign, valign)))
{
}

void Edit::allow_edit() {
    EDIT_IMPL->allow_edit();
}

void Edit::disallow_edit() {
    EDIT_IMPL->disallow_edit();
}

bool Edit::editable() const noexcept {
    return EDIT_IMPL->editable();
}

void Edit::enter_text(const ustring & s) {
    EDIT_IMPL->enter_text(s);
}

bool Edit::modified() const noexcept {
    return EDIT_IMPL->modified();
}

Action & Edit::action_cut() {
    return EDIT_IMPL->action_cut();
}

Action & Edit::action_enter() {
    return EDIT_IMPL->action_enter();
}

Action & Edit::action_delete() {
    return EDIT_IMPL->action_delete();
}

Action & Edit::action_backspace() {
    return EDIT_IMPL->action_backspace();
}

Action & Edit::action_paste() {
    return EDIT_IMPL->action_paste();
}

Action & Edit::action_undo() {
    return EDIT_IMPL->action_undo();
}

Action & Edit::action_redo() {
    return EDIT_IMPL->action_redo();
}

Action & Edit::action_tab() {
    return EDIT_IMPL->action_tab();
}

Toggle_action & Edit::action_insert() {
    return EDIT_IMPL->action_insert();
}

signal<void(bool)> & Edit::signal_modified() {
        return EDIT_IMPL->signal_modified();
}

signal<void()> & Edit::signal_changed() {
        return EDIT_IMPL->signal_changed();
}

} // namespace tau

//END

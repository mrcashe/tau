// ----------------------------------------------------------------------------
// SPDX-License-Identifier: BSD-2-Clause
// Copyright © 2014-2024 Konstantin Shmelkov <mrcashe@gmail.com>.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
// THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
// PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
// CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
// EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
// PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
// OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
// WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
// OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
// EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// ----------------------------------------------------------------------------

/// @file cycle-impl.hh The Cycle_impl class declaration.
/// Implementation is in cycle-impl.cc file.

#ifndef __TAU_CYCLE_IMPL_HH__
#define __TAU_CYCLE_IMPL_HH__

#include <tau/action.hh>
#include <card-impl.hh>
#include <frame-impl.hh>
#include <map>

namespace tau {

class Cycle_impl: public Frame_impl {
public:

    Cycle_impl(Border bs=Border::INSET);

    // Overrides Frame_impl.
    void set_border_style(Border bs) override;
    Border border_style() const noexcept { return user_style_; }

    int add(Widget_ptr wp);
    std::size_t remove(Widget_impl * wp);
    std::size_t remove(int id);
    virtual void clear();       // Overriden by Cycle_text_impl.
    bool empty() const noexcept { return card_->empty(); }
    std::size_t count() const noexcept { return card_->count(); }

    Widget_impl * widget(int id);
    Widget_impl * widget(int id) const;

    int select(Widget_impl * wp, bool fix=false);
    int select(int id, bool fix=false);
    int setup(Widget_impl * wp, bool fix=false);
    int setup(int id, bool fix=false);

    int current() const noexcept;
    bool fixed() const noexcept { return fixed_ >= 0; }
    void unfix();

    void append(Widget_ptr wp, bool shrink=false);
    Widget_ptr append(const ustring & text, unsigned margin_left=0, unsigned margin_right=0);
    Widget_ptr append(const ustring & text, const Color & color, unsigned margin_left=0, unsigned margin_right=0);
    void prepend(Widget_ptr wp, bool shrink=false);
    Widget_ptr prepend(const ustring & text, unsigned margin_left=0, unsigned margin_right=0);
    Widget_ptr prepend(const ustring & text, const Color & color, unsigned margin_left=0, unsigned margin_right=0);

    Action & action_cancel() { return action_cancel_; }
    Action & action_activate() { return action_activate_; }

    Container_impl * compound() noexcept override { return card_; };
    const Container_impl * compound() const noexcept override { return card_; };

    signal<void(int)> & signal_new_id() { return signal_new_id_; }
    signal<void(int)> & signal_remove_id() { return signal_remove_id_; }
    signal<void(int)> & signal_select_id() { return signal_select_id_; }

protected:

    Card_impl *         card_;
    Action              action_cancel_      { "Escape Cancel"_tu    };
    Action              action_activate_    { "Enter"_tu            };

private:

    Border              user_style_;
    Box_impl *          box_;               // Outer box.
    Box_impl *          bbox_;              // Button box.
    Table_impl *        itable_;
    int                 id_                 = 0;
    int                 fixed_              = INT_MIN;

    struct Holder {
        Widget_impl *   wp;
        connection      cx  { true };
    };

    // id -> Widget ptr.
    using Map = std::map<int, Holder>;
    Map                 map_;

    Action              action_up_          { KC_UP,        KM_NONE, "", "picto-inc"_tu };
    Action              action_down_        { KC_DOWN,      KM_NONE, "", "picto-dec"_tu };

    signal<void(int)>   signal_new_id_;
    signal<void(int)>   signal_remove_id_;
    signal<void(int)>   signal_select_id_;

private:

    void update_enable();

    void on_select(int id);
    bool on_mouse_wheel(int delta, int mm, const Point & where);
    bool on_mouse_down(int mbt, int mm, const Point & where);
    bool on_take_focus();
    void on_focus_in();
    void on_focus_out();
    void on_conf_radius();
};

} // namespace tau

#endif // __TAU_CYCLE_IMPL_HH__

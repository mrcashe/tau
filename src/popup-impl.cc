// ----------------------------------------------------------------------------
// SPDX-License-Identifier: BSD-2-Clause
// Copyright © 2014-2024 Konstantin Shmelkov <mrcashe@gmail.com>.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
// THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
// PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
// CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
// EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
// PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
// OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
// WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
// OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
// EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// ----------------------------------------------------------------------------

#include <tau/exception.hh>
#include <display-impl.hh>
#include <popup-impl.hh>
#include <theme-impl.hh>
#include <iostream>

namespace tau {

Popup_impl::Popup_impl(Winface_ptr winface, const Point & upos, Window_ptr wpp, Gravity gravity):
    upos_(upos),
    gravity_(gravity),
    wpp_(wpp)
{
    wpp->signal_close_tooltip()();
    hidden_ = true;
    winface_ = winface;
    winface_->init(this);
    position_ = upos;
    sanitize_.reserve(16);
    signal_show_.connect(fun(this, &Popup_impl::on_show));
    signal_hints_changed_.connect(fun(this, &Popup_impl::adjust));
    signal_parent().connect(fun(this, &Popup_impl::on_parent), true);
    conf().set_parent(wpp->conf());
}

// Overrides Widget_impl.
Window_impl * Popup_impl::toplevel() noexcept {
    return parent_window() ? parent_window()->toplevel() : this;
}

// Overrides Widget_impl.
const Window_impl * Popup_impl::toplevel() const noexcept {
    return parent_window() ? parent_window()->toplevel() : this;
}

// Overrides Widget_impl.
// Overrides Window_impl.
Point Popup_impl::to_screen(const Point & pt) const noexcept {
    if (auto wip = parent_window()) { return wip->to_screen(pt+position()); }
    return Window_impl::to_screen(pt);
}

// Overrides Widget_impl.
Point Popup_impl::to_parent(const Container_impl * ci, const Point & pt) const noexcept {
    if (auto wip = parent_window()) {
        if (ci == wip.get() || ci->has_parent(wip.get())) {
            return wip->to_parent(ci, pt+position());
        }
    }

    return Window_impl::to_parent(ci, pt);
}

// Overrides Widget_impl.
// Overrides Window_impl.
Point Popup_impl::where_mouse() const noexcept {
    if (auto pw = parent_window()) {
        return pw->where_mouse()-position()-client_area().top_left();
    }

    return Window_impl::where_mouse();
}

void Popup_impl::gravity(Gravity gravity) {
    if (gravity_ != gravity) {
        gravity_ = gravity;
        adjust(Hints::SIZE);
    }
}

void Popup_impl::on_show() {
    resize(required_size());
}

void Popup_impl::adjust(Hints) {
    Size rs = required_size(), ps = rs;
    auto wpp = parent_window();
    if (wpp) { ps = wpp->size(); }
    Rect req(rs), pb(ps);

    if (req) {
        switch (gravity_) {
            case Gravity::TOP_LEFT:
                req.translate(upos_);
                break;

            case Gravity::LEFT:
                req.translate(upos_.x(), upos_.y()-req.iheight()/2);
                break;

            case Gravity::BOTTOM_LEFT:
                req.translate(upos_.x(), upos_.y()-req.iheight());
                break;

            case Gravity::TOP_RIGHT:
                req.translate(upos_.x()-req.iwidth(), upos_.y());
                break;

            case Gravity::RIGHT:
                req.translate(upos_.x()-req.iwidth(), upos_.y()-req.iheight()/2);
                break;

            case Gravity::BOTTOM_RIGHT:
                req.translate(upos_.x()-req.iwidth(), upos_.y()-req.iheight());
                break;

            case Gravity::TOP:
                req.translate(upos_.x()-req.iwidth()/2, upos_.y());
                break;

            case Gravity::CENTER:
                req.translate(upos_.x()-req.iwidth()/2, upos_.y()-req.iheight()/2);
                break;

            case Gravity::BOTTOM:
                req.translate(upos_.x()-req.iwidth()/2, upos_.y()-req.iheight());
                break;

            default:
                return;
        }

        Rect united(pb|req);

        if (united != pb) {
            Size ds(united.size()-ps), pps(ps);
            pps.update_min(req.size());

            if (ds.width()) {
                req.update_width(pps.width());
                req.update_left((ps-req.size()).width());
            }

            if (ds.height()) {
                req.update_height(pps.height());
                req.update_top((ps-req.size()).height());
            }
        }

        move(req);
    }
}

// Only Display_impl allowed to be a parent!
void Popup_impl::on_parent(Object * parent) {
    if (parent && !dynamic_cast<Display_impl *>(parent)) {
        throw user_error("Popup: only Display allowed to be a parent");
    }
}

} // namespace tau

//END

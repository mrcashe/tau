// ----------------------------------------------------------------------------
// SPDX-License-Identifier: BSD-2-Clause
// Copyright © 2014-2024 Konstantin Shmelkov <mrcashe@gmail.com>.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
// THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
// PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
// CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
// EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
// PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
// OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
// WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
// OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
// EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// ----------------------------------------------------------------------------

#include <tau/color.hh>
#include <tau/exception.hh>
#include <tau/locale.hh>
#include <tau/sys.hh>
#include <pixmap-impl.hh>
#include <cstdio>
#include <cstring>
#include <iostream>
#include <png.h>

namespace {

bool on_file_supported(const tau::ustring & path) {
    return tau::str_similar("png", tau::path_suffix(path));
}

tau::Pixmap_ptr load_png(png_structp png_ptr, png_infop info_ptr) {
    png_read_info(png_ptr, info_ptr);
    unsigned ctype = png_get_color_type(png_ptr, info_ptr);
    unsigned bit_depth = png_get_bit_depth(png_ptr, info_ptr);

    if (PNG_COLOR_TYPE_PALETTE == ctype) { png_set_palette_to_rgb(png_ptr); }
    if (16 == bit_depth) { png_set_strip_16(png_ptr); }
    // if (PNG_COLOR_TYPE_RGB_ALPHA == ctype || PNG_COLOR_TYPE_RGB == ctype) { png_set_bgr(png_ptr); }

    png_read_update_info(png_ptr, info_ptr);
    ctype = png_get_color_type(png_ptr, info_ptr);
    bit_depth = png_get_bit_depth(png_ptr, info_ptr);
    if (8 != bit_depth) { throw tau::bad_pixmap("PNG: failed to set 8 bit depth"); }
    int width = png_get_image_width(png_ptr, info_ptr);
    int height = png_get_image_height(png_ptr, info_ptr);

    png_bytep * row_pointers = reinterpret_cast<png_bytep *>(png_malloc(png_ptr, sizeof(png_bytep)*height));
    for (int y = 0; y < height; ++y) { row_pointers[y] = reinterpret_cast<png_byte *>(png_malloc(png_ptr, png_get_rowbytes(png_ptr, info_ptr))); }
    png_read_image(png_ptr, row_pointers);

    int bpp = 24;
    if (PNG_COLOR_TYPE_GRAY == ctype) { bpp = 8; }
    else if (PNG_COLOR_TYPE_RGB_ALPHA == ctype || PNG_COLOR_TYPE_GRAY_ALPHA == ctype) { bpp = 32; }
    if (32 == bpp) { png_set_swap(png_ptr); }
    tau::Pixmap_ptr pix = tau::Pixmap_impl::create(bpp, width, height);
    pix->set_ppi(tau::Vector(png_get_x_pixels_per_inch(png_ptr, info_ptr), png_get_y_pixels_per_inch(png_ptr, info_ptr)));

    for (int y = 0; y < height; ++y) {
        png_byte * row = row_pointers[y];

        if (PNG_COLOR_TYPE_RGB_ALPHA == ctype) {
            for (int x = 0; x < width; ++x) {
                png_byte * p = row+(x*4);
                uint32_t r = p[0], g = p[1], b = p[2], a = p[3];
                pix->put_pixel({ x, y }, tau::Color::from_argb32((a << 24)|(r << 16)|(g << 8)|b));
            }
        }

        else if (PNG_COLOR_TYPE_RGB == ctype) {
            for (int x = 0; x < width; ++x) {
                png_byte * p = row+(x*3);
                uint32_t b = p[0], g = p[1], r = p[2];
                pix->put_pixel({ x, y }, tau::Color::from_rgb24((r << 16)|(g << 8)|b));
            }
        }

        else if (PNG_COLOR_TYPE_GRAY == ctype) {
            for (int x = 0; x < width; ++x) {
                png_byte * p = row+x;
                pix->put_pixel({ x, y }, tau::Color::from_gray8(*p));
            }
        }

        else if (PNG_COLOR_TYPE_GRAY_ALPHA == ctype) {
            for (int x = 0; x < width; ++x) {
                png_byte * p = row+(x*2);
                pix->put_pixel({ x, y }, tau::Color::from_gray8(p[0], p[1]/255.0));
            }
        }

        else {
            throw tau::bad_pixmap(tau::str_format("PNG: color type of ", ctype, " unsupported"));
        }
    }

    for (int y = 0; y < height; ++y) { png_free(png_ptr, row_pointers[y]); }
    png_free(png_ptr, row_pointers);
    png_read_end(png_ptr, info_ptr);
    return pix;
}

tau::Pixmap_ptr load_png_from_file(const tau::ustring & path) {
    auto io = tau::Locale().iocharset();
    std::string lfp = io.is_utf8() ? std::string(path) : io.encode(path);
    FILE * is = std::fopen(lfp.c_str(), "rb");
    if (!is) { return nullptr; }
    uint8_t header[8];
    if (8 != std::fread(header, 1, 8, is)) { std::fclose(is); return nullptr; }
    if (png_sig_cmp(header, 0, 8)) { std::fclose(is); return nullptr; }

    png_structp png_ptr = png_create_read_struct(PNG_LIBPNG_VER_STRING, NULL, NULL, NULL);
    if (!png_ptr) { std::fclose(is); throw tau::bad_pixmap(tau::str_format(path, ": PNG: png_create_read_struct failed")); }

    png_infop info_ptr = png_create_info_struct(png_ptr);
    if (!info_ptr) { std::fclose(is); throw tau::bad_pixmap(tau::str_format(path, ": PNG: png_create_info_struct failed")); }

    if (setjmp(png_jmpbuf(png_ptr))) { std::fclose(is); throw tau::bad_pixmap(tau::str_format(path, ": PNG: error during init_io")); }
    png_init_io(png_ptr, is);
    png_set_sig_bytes(png_ptr, 8);

    tau::Pixmap_ptr pix;
    try { pix = load_png(png_ptr, info_ptr); }
    catch (tau::exception & x) { std::fclose(is); throw; }
    std::fclose(is);
    return pix;
}

tau::Pixmap_ptr load_png_from_memory(const uint8_t * buf, std::size_t bytes) {
    if (8 < bytes && png_check_sig(buf, 8)) {
        // Get PNG file info struct (memory is allocated by libpng).
        if (png_structp png_ptr = png_create_read_struct(PNG_LIBPNG_VER_STRING, NULL, NULL, NULL)) {
            // Get PNG image data info struct (memory is allocated by libpng).
            png_infop info_ptr = png_create_info_struct(png_ptr);

            if (!info_ptr) {
                // libpng must free file info struct memory before we bail.
                png_destroy_read_struct(&png_ptr, NULL, NULL);
                return nullptr;
            }

            struct read_t {
                const uint8_t *     buf;
                std::size_t         ofs;
                std::size_t         bytes;
            };

            auto fn = [](png_structp png_ptr, png_bytep out, png_size_t nbytes) -> void {
                png_voidp io_ptr = png_get_io_ptr(png_ptr);

                if (io_ptr) {
                    auto * rt = reinterpret_cast<read_t *>(io_ptr);
                    std::size_t n = std::min(rt->bytes-rt->ofs, nbytes);
                    if (n) { std::memcpy(out, rt->buf+rt->ofs, n); rt->ofs += n; }
                }
            };

            read_t rt { buf, 8, bytes };
            png_set_read_fn(png_ptr, &rt, fn);
            png_set_sig_bytes(png_ptr, 8);
            tau::Pixmap_ptr pix;
            try { pix = load_png(png_ptr, info_ptr); }
            catch (...) {}
            png_destroy_read_struct(&png_ptr, &info_ptr, NULL);
            return pix;
        }
    }

    return nullptr;
}

} // anonymous namespace

namespace tau {

void pixmap_png_init() {
    Pixmap_impl::signal_file_supported().connect(fun(on_file_supported));
    Pixmap_impl::signal_load_from_file().connect(fun(load_png_from_file));
    Pixmap_impl::signal_load_from_memory().connect(fun(load_png_from_memory));
}

} // namespace tau

//END

// ----------------------------------------------------------------------------
// SPDX-License-Identifier: BSD-2-Clause
// Copyright © 2014-2024 Konstantin Shmelkov <mrcashe@gmail.com>.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
// THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
// PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
// CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
// EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
// PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
// OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
// WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
// OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
// EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// ----------------------------------------------------------------------------

#include <sys-impl.hh>
#include <tau/gettext.hh>

namespace tau {

void gettext_update() {}
ustring gettext(const ustring & msgid) { return msgid; }
ustring lgettext(const ustring & msgid) { return msgid; }
ustring dgettext(std::string_view domain, const ustring & msgid) { return msgid; }
ustring dcgettext(std::string_view domain, const ustring & msgid, int category) { return msgid; }
ustring ngettext(const ustring & msgid1, const ustring & msgid2, unsigned long n) { return msgid1; }
ustring lngettext(const ustring & msgid1, const ustring & msgid2, unsigned long n) { return msgid1; }
ustring dngettext(std::string_view domain, const ustring & msgid1, const ustring & msgid2, unsigned long n) { return msgid1; }
ustring dcngettext(std::string_view domain, const ustring & msgid1, const ustring & msgid2, unsigned long n, int category) { return msgid1; }
ustring gettext_open(std::string_view domain, const ustring & dirname) { return ustring(); }
ustring gettext_open(std::string_view domain) { return ustring(); }

} // namespace tau

//END

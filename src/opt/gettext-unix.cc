// ----------------------------------------------------------------------------
// SPDX-License-Identifier: BSD-2-Clause
// Copyright © 2014-2024 Konstantin Shmelkov <mrcashe@gmail.com>.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
// THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
// PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
// CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
// EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
// PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
// OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
// WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
// OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
// EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// ----------------------------------------------------------------------------

#include <gettext-impl.hh>
#include <tau/gettext.hh>
#include <tau/language.hh>
#include <tau/string.hh>
#include <sys-impl.hh>
#include <libintl.h>
#include <iostream>

namespace tau {

void gettext_update() {
    auto lang = Language().code();

    ustring w = path_which(std::string(gettext_domain_)+".mo", { path_build(path_share(), { "locale", lang, "LC_MESSAGES" }),
        path_build(path_dirname(path_self()), { "locale", lang, "LC_MESSAGES" }),
        path_build(sysinfo_.lib_prefix, { "share", "locale", lang, "LC_MESSAGES" }) });

    if (!w.empty()) {
        bindtextdomain(gettext_domain_, path_dirname(path_dirname(path_dirname(w))).data());
        bind_textdomain_codeset(gettext_domain_, "UTF8");
    }

    w = path_which(program_name()+".mo", {  path_build(path_prefix(), { "share", "locale", lang, "LC_MESSAGES" }),
                                            path_build(path_share(), { "locale", lang, "LC_MESSAGES" }),
                                            path_build(path_dirname(path_self()), { "locale", lang, "LC_MESSAGES" }) });

    if (!w.empty()) {
        bindtextdomain(program_name().data(), path_dirname(path_dirname(path_dirname(w))).data());
        bind_textdomain_codeset(program_name().data(), "UTF8");
        textdomain(program_name().data());
    }
}

/// Wraps bindtextdomain().
ustring gettext_open(std::string_view domain) {
    ustring w = path_which(ustring(is_tau(domain))+".mo", { path_build(path_prefix(), { "share", "locale", Language().code(), "LC_MESSAGES" }), path_build(path_share(), { "locale", Language().code(), "LC_MESSAGES" }) });

    if (!w.empty()) {
        ustring wd = bindtextdomain(is_tau(domain), path_dirname(path_dirname(path_dirname(w))).data());
        bind_textdomain_codeset(is_tau(domain), "UTF8");
        return wd;
    }

    return ustring();
}

} // namespace tau

//END

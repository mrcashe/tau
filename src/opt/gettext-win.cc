// ----------------------------------------------------------------------------
// SPDX-License-Identifier: BSD-2-Clause
// Copyright © 2014-2024 Konstantin Shmelkov <mrcashe@gmail.com>.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
// THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
// PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
// CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
// EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
// PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
// OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
// WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
// OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
// EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// ----------------------------------------------------------------------------

#include <gettext-impl.hh>
#include <Windows/sys-win.hh>
#include <tau/file.hh>
#include <tau/gettext.hh>
#include <tau/language.hh>
#include <tau/string.hh>
#include <libintl.h>
#include <iostream>
#include <filesystem>
#include <fstream>
#include <set>

namespace {

std::set<tau::ustring>  mos_;       // Loaded *.mo file names.

struct Deleter {
    ~Deleter() {
        for (auto file: mos_) {
            tau::File(file).rm();
        }
    }
};

Deleter del_;

tau::ustring rc_load(const char * domain) {
    if (HRSRC hr = FindResourceA(GetModuleHandle(NULL), tau::str_format("mo-", tau::Language().code(), '-', domain).data(), RT_RCDATA)) {
        if (HGLOBAL hg = LoadResource(GetModuleHandle(NULL), hr)) {
            if (DWORD bytes = SizeofResource(GetModuleHandle(NULL), hr)) {
                if (LPVOID p = LockResource(hg)) {
                    try {
                        const tau::ustring locpath = tau::path_build(tau::path_tmp(), "locale");
                        const tau::ustring dirpath = tau::path_build(locpath, tau::Language().code(), "LC_MESSAGES");
                        tau::file_mkdir(dirpath);
                        const tau::ustring filepath = tau::path_build(dirpath, tau::ustring(domain)+".mo");
                        mos_.insert(filepath);
                        std::ofstream os(std::filesystem::path(std::wstring(filepath)), std::ios::binary);
                        os.write(reinterpret_cast<const char *>(p), bytes);
                        const tau::ustring res = bindtextdomain(domain, locpath.data());
                        bind_textdomain_codeset(domain, "UTF8");
                        return res;
                    }

                    catch (...) {
                        std::cerr << "rc_load(): failed" << std::endl;
                    }
                }
            }
        }
    }

    return tau::ustring();
}

} // anonymous namespace

namespace tau {

void gettext_update() {
    ustring dom = rc_load("tau");
    auto lang = Language().code();

    if (dom.empty()) {
        ustring w = path_which(std::string(gettext_domain_)+".mo", { path_build(path_share(), { "locale", lang, "LC_MESSAGES" }),
                               path_build(path_dirname(path_self()), { "locale", lang, "LC_MESSAGES" }),
                               path_build(sysinfo_.lib_prefix, { "share", "locale", lang, "LC_MESSAGES" }) });

        if (!w.empty()) {
            bindtextdomain(gettext_domain_, path_dirname(path_dirname(path_dirname(w))).data());
            bind_textdomain_codeset(gettext_domain_, "UTF8");
        }
    }

    ustring prg = program_name();
    dom = rc_load(prg.data());

    if (dom.empty()) {
        ustring w = path_which(prg+".mo", { path_build(path_prefix(), { "share", "locale", lang, "LC_MESSAGES" }),
                                            path_build(path_share(), { "locale", lang, "LC_MESSAGES" }),
                                            path_build(path_dirname(path_self()), { "locale", lang, "LC_MESSAGES" }) });

        if (!w.empty()) {
            bindtextdomain(prg.data(), path_dirname(path_dirname(path_dirname(w))).data());
            bind_textdomain_codeset(prg.data(), "UTF8");
            textdomain(prg.data());
        }
    }

    else {
        textdomain(prg.data());
    }
}

/// Wraps bindtextdomain().
ustring gettext_open(std::string_view domain) {
    if (rc_load(is_tau(domain)).empty()) {
        ustring w = path_which(domain+".mo", { path_build(path_prefix(), "share", path_build("locale", Language().code(), "LC_MESSAGES")), path_build(path_share(), "locale", path_build(Language().code(), "LC_MESSAGES")) });

        if (!w.empty()) {
            ustring wd = bindtextdomain(is_tau(domain), path_dirname(path_dirname(path_dirname(w))).data());
            bind_textdomain_codeset(is_tau(domain), "UTF8");
            return wd;
        }
    }

    return ustring();
}

} // namespace tau

//END

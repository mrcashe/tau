// ----------------------------------------------------------------------------
// SPDX-License-Identifier: BSD-2-Clause
// Copyright © 2014-2024 Konstantin Shmelkov <mrcashe@gmail.com>.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
// THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
// PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
// CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
// EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
// PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
// OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
// WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
// OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
// EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// ----------------------------------------------------------------------------

#include <tau/key-file.hh>
#include <tau/locale.hh>
#include <tau/string.hh>
#include <Unix/sys-unix.hh>
#include <Unix/loop-unix.hh>
#include <iostream>

namespace tau {

void boot_linkage() {
    sysinfo_.shared = true;
    auto self = path_self();
    auto & sect = kache_.section(uri_escape(self));
    uint64_t mtime = File(path_self()).mtime(), mlast = kache_.get_integer(sect, "mtime");
    sysinfo_.sopath = uri_unescape(kache_.get_string(sect, "sopath"));

    if (sysinfo_.sopath.empty() || mtime > mlast) {
        kache_.set_integer(sect, "mtime", mtime);
        auto v = str_explode(str_getenv("LD_LIBRARY_PATH"), ':');
        v.insert(v.begin(), path_dirname(path_self()));

        for (auto & s: std::vector<ustring> { "/usr/lib", "/usr/local/lib" }) {
            v.emplace_back(s);
            v.emplace_back(path_build(s, sysinfo_.host));
            v.emplace_back(str_format(s, sysinfo_.abits));
        }

        v.emplace_back(path_build("/opt", program_name(), "lib"));
        v.emplace_back(path_build(path_prefix(), "lib"));
        auto soname = str_format("libtau.so.", sysinfo_.Major, '.', sysinfo_.Minor, '.', sysinfo_.Micro);
        sysinfo_.sopath = path_which(soname, v);

        if (sysinfo_.sopath.empty()) {
            auto v = str_explode(str_trim(pipe_in(str_format("ldd ", self))));
            auto i = std::find_if(v.begin(), v.end(), [&soname](auto & s) { return str_like(s, soname) < s.size(); });
            if (i != v.end()) { sysinfo_.sopath = *i; };
        }
    }

    if (!sysinfo_.sopath.empty()) {
        kache_.set_string(sect, "sopath", uri_escape(sysinfo_.sopath));
        sysinfo_.lib_prefix = path_dirname(path_dirname(sysinfo_.sopath));
    }

    else {
        sysinfo_.lib_prefix = path_prefix();
    }
}

} // namespace tau

//END

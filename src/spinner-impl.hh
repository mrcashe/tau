// ----------------------------------------------------------------------------
// SPDX-License-Identifier: BSD-2-Clause
// Copyright © 2014-2024 Konstantin Shmelkov <mrcashe@gmail.com>.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
// THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
// PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
// CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
// EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
// PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
// OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
// WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
// OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
// EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// ----------------------------------------------------------------------------

#ifndef __TAU_SPINNER_IMPL_HH__
#define __TAU_SPINNER_IMPL_HH__

#include <tau/spinner.hh>
#include "widget-impl.hh"

namespace tau {

class Spinner_impl: public Widget_impl {
public:

    using Style = Spinner::Style;

    Spinner_impl();
    Spinner_impl(Style spinner_style);
    void start();
    void stop();
    bool active() const noexcept { return !cx_.empty(); }
    void spinner_style(Style spinner_style);
    Style spinner_style() const noexcept { return sstyle_; }

private:

    int             step_ = 0;      // Rotation steps (0...7).
    double          vstep_ = 0.1;   // Color value step.
    double          ivalue_ = 0.0;  // Initial color value.
    Color           color_;
    Style           sstyle_ = Spinner::BALL;
    connection      cx_ { true };

private:

    void init();
    void on_display();
    void on_timer();
    bool on_paint(Painter pr, const Rect & inval);
    void on_foreground_changed();
};

} // namespace tau

#endif // __TAU_SPINNER_IMPL_HH__

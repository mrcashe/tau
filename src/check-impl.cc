// ----------------------------------------------------------------------------
// SPDX-License-Identifier: BSD-2-Clause
// Copyright © 2014-2024 Konstantin Shmelkov <mrcashe@gmail.com>.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
// THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
// PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
// CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
// EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
// PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
// OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
// WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
// OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
// EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// ----------------------------------------------------------------------------

#include <tau/brush.hh>
#include <tau/font.hh>
#include <tau/painter.hh>
#include <tau/pen.hh>
#include <check-impl.hh>
#include <theme-impl.hh>
#include <cmath>
#include <iostream>

namespace tau {

Check_impl::Check_impl(bool checked):
    Frame_impl(),
    checked_(checked),
    check_style_(Check::VSTYLE),
    user_border_style_(Border::INSET)
{
    init();
}

Check_impl::Check_impl(Style cs, bool checked):
    Frame_impl(),
    checked_(checked),
    check_style_(cs),
    user_border_style_(Border::INSET)
{
    init();
}

Check_impl::Check_impl(Border bs, bool checked):
    Frame_impl(),
    checked_(checked),
    check_style_(Check::VSTYLE),
    user_border_style_(bs)
{
    init();
}

Check_impl::Check_impl(Style cs, Border bs, bool checked):
    Frame_impl(),
    checked_(checked),
    check_style_(cs),
    user_border_style_(bs)
{
    init();
}

void Check_impl::init() {
    disallow_focus();
    require_aspect_ratio(1.0);
    auto wp = std::make_shared<Widget_impl>();
    insert(wp);
    cp_->conf().redirect(Conf::BACKGROUND, Conf::WHITESPACE_BACKGROUND);
    radio_signal_ = std::make_shared<Radio_signal>();
    set_style();
    radio_cx_ = radio_signal_->connect(fun(this, &Check_impl::on_radio_signal));
    connect_action(action_toggle_);
    connect_action(action_cancel_);

    wp->signal_paint().connect(fun(this, &Check_impl::on_paint));
    wp->signal_display_in().connect(fun(this, &Check_impl::on_font_changed));
    conf().signal_changed(Conf::FONT).connect(fun(this, &Check_impl::on_font_changed));
    wp->signal_mouse_down().connect(fun(this, &Check_impl::on_mouse_down));
    signal_focus_in_.connect(fun(this, &Check_impl::redraw));
    signal_focus_out_.connect(fun(this, &Check_impl::redraw));
    signal_take_focus_.connect(fun(this, &Check_impl::grab_focus), true);
    signal_enable_.connect(fun(this, &Check_impl::on_enable));
    signal_disable_.connect(fun(this, &Check_impl::on_disable));
}

void Check_impl::set_style() {
    if (Check::RSTYLE != check_style_) {
        set_border(user_border_width_, user_border_style_);
        cp_->hint_margin(1);
    }

    else {
        set_border(0, Border::NONE);
        cp_->hint_margin(0);
    }
}

void Check_impl::set_check_style(Style cs) {
    if (check_style_ != cs) {
        check_style_ = cs;
        set_style();
        redraw();
    }
}

void Check_impl::check() {
    if (!checked_) {
        checked_ = true;
        radio_cx_.block();
        radio_signal_->operator()(RADIO_CHECK);
        radio_cx_.unblock();
        redraw();
        signal_check_();
    }
}

void Check_impl::setup(bool state) {
    if (checked_ != state) {
        if (joined()) {
            if (state) {
                checked_ = true;
                radio_cx_.block();
                radio_signal_->operator()(RADIO_SETUP);
                radio_cx_.unblock();
                redraw();
            }
        }

        else {
            checked_ = state;
            redraw();
        }
    }
}

void Check_impl::uncheck() {
    if (checked_ && !joined()) {
        checked_ = false;
        redraw();
        signal_uncheck_();
    }
}

void Check_impl::set(bool state) {
    if (state) { check(); }
    else { uncheck(); }
}

void Check_impl::join(Check_ptr other) {
    if (this != other.get()) {
        if (other->disabled()) freeze(); else thaw();
        radio_signal_ = other->radio_signal_;
        if (checked_) { radio_signal_->operator()(RADIO_CHECK); }
        radio_cx_ = radio_signal_->connect(fun(this, &Check_impl::on_radio_signal));
    }
}

void Check_impl::on_radio_signal(int code) {
    switch (code) {
        case RADIO_CHECK: checked_ = false; redraw(); signal_uncheck_(); break;
        case RADIO_SETUP: checked_ = false; redraw(); break;
        case RADIO_ENABLE: thaw(); break;
        case RADIO_DISABLE: freeze(); break;
    }
}

void Check_impl::on_enable() {
    cp_->conf().redirect(Conf::BACKGROUND, Conf::WHITESPACE_BACKGROUND);

    if (!disabled() && joined()) {
        radio_cx_.block();
        radio_signal_->operator()(RADIO_ENABLE);
        radio_cx_.unblock();
    }
}

void Check_impl::on_disable() {
    cp_->conf().unredirect(Conf::BACKGROUND);

    if (disabled() && joined()) {
        radio_cx_.block();
        radio_signal_->operator()(RADIO_DISABLE);
        radio_cx_.unblock();
    }
}

void Check_impl::set_border_width(unsigned npx) {
    user_border_width_ = npx;
    set_border(npx);
}

void Check_impl::toggle() {
    if (edit_allowed_) {
        if (checked() && !joined()) { uncheck(); }
        else { check(); }
    }
}

void Check_impl::on_cancel() {
    if (focused()) {
        drop_focus();
    }
}

bool Check_impl::on_mouse_down(int mbt, int mm, const Point & pt) {
    if (MBT_LEFT == mbt && edit_allowed_) {
        grab_focus();
        toggle();
        return true;
    }

    return false;
}

void Check_impl::on_font_changed() {
    if (Painter pr = cp_->painter()) {
        Size rs;

        if (Font font = pr.select_font(conf().font().spec())) {
            unsigned s = 3*font.height()/4;
            rs.update_max_height(s);
            rs.update_max_width(s);
            cp_->hint_min_size(rs);
        }
    }
}

bool Check_impl::on_paint(Painter pr, const Rect & inval) {
    draw_check(pr);
    return true;
}

void Check_impl::redraw() {
    if (size()) {
        if (Painter pr = cp_->painter()) { draw_check(pr); }
        else { invalidate(); }
    }
}

void Check_impl::draw_check(Painter pr) {
    Rect r(cp_->size());
    unsigned s = r.size().min();
    Point rc = r.center();
    pr.matrix().translate(rc.x(), rc.y());
    const int MAX = s/2-2;
    Brush back = conf().brush(Conf::BACKGROUND);
    Brush wsp  = Theme_impl::root()->conf().brush(Conf::WHITESPACE_BACKGROUND);
    Color fore = enabled() ? cp_->conf().color(Conf::FOREGROUND) : back.color().faded();

    switch (check_style_) {
        case Check::RSTYLE:
            pr.set_brush(back);
            pr.paint();
            pr.circle(0, 0, s/2-1);
            pr.set_brush(wsp);
            pr.fill_preserve();
            pr.set_pen(fore);
            pr.stroke();

            if (checked_) {
                pr.circle(0, 0, MAX/2);
                pr.set_brush(fore);
                pr.fill();
            }

            break;

        case Check::QSTYLE:
            pr.set_brush(wsp);
            pr.paint();

            if (checked_) {
                pr.rectangle(-MAX/2, -MAX/2, MAX, MAX);
                pr.set_brush(fore);
                pr.fill();
            }

            break;

        case Check::XSTYLE:
            pr.set_brush(wsp);
            pr.paint();

            if (checked_) {
                pr.move_to(-MAX, -MAX);
                pr.line_to(MAX, MAX);
                pr.move_to(-MAX, MAX);
                pr.line_to(MAX, -MAX);
                pr.set_pen(Pen(fore, 2, Line::SOLID, Cap::FLAT));
                pr.stroke();
            }

            break;

        // Assume CHECK_VSTYLE.
        default:
            pr.set_brush(wsp);
            pr.paint();

            if (checked_) {
                pr.move_to(-MAX, 0);
                pr.line_to(-MAX/4, MAX);
                pr.line_to(MAX, -MAX);
                pr.set_pen(Pen(fore, 2, Line::SOLID, Cap::ROUND));
                pr.stroke();
            }
    }
}

} // namespace tau

//END

// ----------------------------------------------------------------------------
// SPDX-License-Identifier: BSD-2-Clause
// Copyright © 2014-2024 Konstantin Shmelkov <mrcashe@gmail.com>.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
// THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
// PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
// CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
// EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
// PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
// OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
// WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
// OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
// EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// ----------------------------------------------------------------------------

/// @file action.cc Various action implementatios.

#include <tau/action.hh>
#include <tau/string.hh>
#include <theme-impl.hh>
#include <gettext-impl.hh>
#include <algorithm>
#include <iostream>

namespace tau {

struct Master_action::Data {
    ustring             label_;
    ustring             icon_name_;
    ustring             tooltip_;
    bool                visible_ : 1 = true;
    bool                enabled_ : 1 = true;
    std::list<Accel>    accels_;

    signal<void()>      signal_disable_;
    signal<void()>      signal_enable_;
    signal<void()>      signal_show_;
    signal<void()>      signal_hide_;

    signal<void(const Accel &)>     signal_accel_added_;
    signal<void(const Accel &)>     signal_accel_removed_;
    signal<void(const ustring &)>   signal_label_changed_;
    signal<void(const ustring &)>   signal_icon_changed_;
    signal<void(const ustring &)>   signal_tooltip_changed_;
};

Master_action::Master_action():
    data(new Data)
{
}

Master_action::Master_action(const ustring & accels):
    data(new Data)
{
    add_accels(accels);
}

Master_action::Master_action(const Master_action & other):
    data(new Data(*other.data))
{
}

Master_action::Master_action(Master_action && other):
    data(new Data(std::move(*other.data)))
{
}

Master_action::Master_action(char32_t kc, int km):
    data(new Data)
{
    add_accel(kc, km);
}

Master_action::Master_action(const ustring & accels, const ustring & label):
    data(new Data)
{
    add_accels(accels);
    set_label(label);
}

Master_action::Master_action(char32_t kc, int km, const ustring & label):
    data(new Data)
{
    add_accel(kc, km);
    set_label(label);
}

Master_action::Master_action(const ustring & accels, const ustring & label, const ustring & icon_name):
    data(new Data)
{
    add_accels(accels);
    set_label(label);
    set_icon(icon_name);
}

Master_action::Master_action(char32_t kc, int km, const ustring & label, const ustring & icon_name):
    data(new Data)
{
    add_accel(kc, km);
    set_label(label);
    set_icon(icon_name);
}

Master_action::Master_action(const ustring & accels, const ustring & label, const ustring & icon_name, const ustring & tooltip):
    data(new Data)
{
    add_accels(accels);
    set_label(label);
    set_icon(icon_name);
    set_tooltip(tooltip);
}

Master_action::Master_action(char32_t kc, int km, const ustring & label, const ustring & icon_name, const ustring & tooltip):
    data(new Data)
{
    add_accel(kc, km);
    set_label(label);
    set_icon(icon_name);
    set_tooltip(tooltip);
}

Master_action::~Master_action() {
    delete data;
}

Master_action & Master_action::operator=(const Master_action & other) {
    if (this != &other) { *data = *other.data; }
    return *this;
}

Master_action & Master_action::operator=(Master_action && other) {
    *data = std::move(*other.data);
    return *this;
}

void Master_action::set_label(const ustring & label) {
    if (data->label_ != label) {
        data->label_ = label;
        data->signal_label_changed_(data->label_);
    }
}

ustring Master_action::label() const {
    return lgettext(data->label_);
}

void Master_action::enable() {
    if (!data->enabled_) {
        data->enabled_ = true;
        data->signal_enable_();
    }
}

void Master_action::disable() {
    if (data->enabled_) {
        data->enabled_ = false;
        data->signal_disable_();
    }
}

void Master_action::par_enable(bool yes) {
    if (yes) { enable(); }
    else { disable(); }
}

bool Master_action::enabled() const noexcept {
    return data->enabled_;
}

void Master_action::show() {
    if (!data->visible_) {
        data->visible_ = true;
        data->signal_show_();
    }
}

void Master_action::hide() {
    if (data->visible_) {
        data->visible_ = false;
        data->signal_hide_();
    }
}

bool Master_action::visible() const noexcept {
    return data->visible_;
}

void Master_action::set_icon(const ustring & icon_name) {
    if (data->icon_name_ != icon_name) {
        data->icon_name_ = icon_name;
        data->signal_icon_changed_(data->icon_name_);
    }
}

ustring Master_action::icon_name() const {
    return data->icon_name_;
}

void Master_action::set_tooltip(const ustring & tooltip) {
    if (data->tooltip_ != tooltip) {
        data->tooltip_ = tooltip;
        data->signal_tooltip_changed_(data->tooltip_);
    }
}

void Master_action::unset_tooltip() {
    if (!data->tooltip_.empty()) {
        data->tooltip_.clear();
        data->signal_tooltip_changed_(data->tooltip_);
    }
}

ustring Master_action::tooltip() const {
    return lgettext(data->tooltip_);
}

void Master_action::add_accel(char32_t kc, int km) {
    auto i = std::find_if(data->accels_.begin(), data->accels_.end(), [kc, km](auto & acc) { return acc.equal(kc, km); });
    if (i == data->accels_.end()) { data->signal_accel_added_(data->accels_.emplace_back(kc, km)); }
}

void Master_action::add_accels(const ustring & key_specs) {
    auto blanks = Locale().blanks();

    for (const ustring & spec: str_explode(key_specs, blanks)) {
        char32_t kc; int km;
        key_spec_from_string(spec, kc, km);
        add_accel(kc, km);
    }
}

void Master_action::remove_accel(char32_t kc, int km) {
    Accel accel(kc, km);
    auto i = std::find(data->accels_.begin(), data->accels_.end(), accel);
    if (i != data->accels_.end()) { data->accels_.erase(i), data->signal_accel_removed_(accel); }
}

void Master_action::remove_accels(const ustring & key_specs) {
    auto blanks = Locale().blanks();

    for (const ustring & spec: str_explode(key_specs, blanks)) {
        char32_t kc; int km;
        key_spec_from_string(spec, kc, km);
        remove_accel(kc, km);
    }
}

void Master_action::clear_accels() {
    auto accels = data->accels_;
    for (auto & accel: accels) { remove_accel(accel.key_code(), accel.key_modifier()); }
    data->accels_.clear();
}

std::vector<Accel *> Master_action::accels() {
    std::vector<Accel *> v(data->accels_.size());
    auto i = v.begin();
    for (auto & acc: data->accels_) { *i++ = &acc; }
    return v;
}

std::vector<const Accel *> Master_action::accels() const {
    std::vector<const Accel *> v(data->accels_.size());
    auto i = v.begin();
    for (auto & acc: data->accels_) { *i++ = &acc; }
    return v;
}

signal<void()> & Master_action::signal_disable() {
    return data->signal_disable_;
}

signal<void()> & Master_action::signal_enable() {
    return data->signal_enable_;
}

signal<void()> & Master_action::signal_hide() {
    return data->signal_hide_;
}

signal<void()> & Master_action::signal_show() {
    return data->signal_show_;
}

signal<void(const Accel & accel)> & Master_action::signal_accel_added() {
    return data->signal_accel_added_;
}

signal<void(const Accel & accel)> & Master_action::signal_accel_removed() {
    return data->signal_accel_removed_;
}

signal<void(const ustring &)> & Master_action::signal_label_changed() {
    return data->signal_label_changed_;
}

signal<void(const ustring &)> & Master_action::signal_icon_changed() {
    return data->signal_icon_changed_;
}

signal<void(const ustring &)> & Master_action::signal_tooltip_changed() {
    return data->signal_tooltip_changed_;
}

// ----------------------------------------------------------------------------
// ----------------------------------------------------------------------------

struct Action_base::Data: public trackable {
    struct Acc {
        Accel               acc_;
        connection          cx_ { true };
        Acc(char32_t kc, int km): acc_(kc, km) {}
    };

    std::list<Acc>          accels_;
    bool                    disabled_ : 1       = false;
    bool                    frozen_ : 1         = false;
    bool                    hidden_ : 1         = false;
    bool                    disappeared_ : 1    = false;
    bool                    selected_ : 1       = false;
    ustring                 label_;
    ustring                 icon_name_;
    ustring                 tooltip_;

    signal<void()> *                                signal_disable_             = nullptr;
    signal<void()> *                                signal_enable_              = nullptr;
    signal<void()> *                                signal_show_                = nullptr;
    signal<void()> *                                signal_hide_                = nullptr;
    signal<void(const Accel &)> *                   signal_accel_added_         = nullptr;
    signal<void(const Accel &, const Accel &)> *    signal_accel_changed_       = nullptr;
    signal<void(const Accel &)> *                   signal_accel_removed_       = nullptr;
    signal<void(const ustring &)> *                 signal_label_changed_       = nullptr;
    signal<void(const ustring &)> *                 signal_icon_changed_        = nullptr;
    signal<void(const ustring &)> *                 signal_tooltip_changed_     = nullptr;
    signal<void()>                                  signal_destroy_;
    signal<void()>                                  signal_select_;
    signal<void()>                                  signal_unselect_;

    connection              accel_added_cx_ { true };
    connection              accel_changed_cx_ { true };
    connection              accel_removed_cx_ { true };
    connection              enable_cx_ { true };
    connection              disable_cx_ { true };
    connection              show_cx_ { true };
    connection              hide_cx_ { true };
    connection              label_changed_cx_ { true };
    connection              icon_changed_cx_ { true };
    connection              tooltip_changed_cx_ { true };

    Data() {
        signal_select_.connect(bind_back(fun(this, &Data::on_select), true));
        signal_unselect_.connect(bind_back(fun(this, &Data::on_select), false));
    }

    virtual ~Data() {
        if (signal_disable_) { delete signal_disable_; }
        if (signal_enable_) { delete signal_enable_; }
        if (signal_show_) { delete signal_show_; }
        if (signal_hide_) { delete signal_hide_; }
        if (signal_accel_added_) { delete signal_accel_added_; }
        if (signal_accel_changed_) { delete signal_accel_changed_; }
        if (signal_accel_removed_) { delete signal_accel_removed_; }
        if (signal_label_changed_) { delete signal_label_changed_; }
        if (signal_icon_changed_) { delete signal_icon_changed_; }
        if (signal_tooltip_changed_) { delete signal_tooltip_changed_; }
    }

    Data(const Data & other):
        disabled_(other.disabled_),
        frozen_(other.frozen_),
        hidden_(other.hidden_),
        disappeared_(other.disappeared_),
        selected_(other.selected_),
        label_(other.label_),
        icon_name_(other.icon_name_),
        tooltip_(other.tooltip_)
    {
        for (auto & acc: other.accels_) { add_accel(acc.acc_.key_code(), acc.acc_.key_modifier()); }
    }

    Data(Data && other):
        disabled_(other.disabled_),
        frozen_(other.frozen_),
        hidden_(other.hidden_),
        disappeared_(other.disappeared_),
        selected_(other.selected_),
        label_(other.label_),
        icon_name_(other.icon_name_),
        tooltip_(other.tooltip_)
    {
        for (auto & acc: other.accels_) { add_accel(acc.acc_.key_code(), acc.acc_.key_modifier()); }
    }

    Data & operator=(const Data & other) {
        if (this != &other) {
            accels_.clear();
            for (auto & acc: other.accels_) { add_accel(acc.acc_.key_code(), acc.acc_.key_modifier()); }
            disabled_ = other.disabled_;
            frozen_ = other.frozen_;
            hidden_ = other.hidden_;
            disappeared_ = other.disappeared_;
            selected_ = other.selected_;
            label_ = other.label_;
            icon_name_ = other.icon_name_;
            tooltip_ = other.tooltip_;
        }

        return *this;
    }

    Data & operator=(Data && other) {
        accels_.clear();
        for (auto & acc: other.accels_) { add_accel(acc.acc_.key_code(), acc.acc_.key_modifier()); }
        disabled_ = other.disabled_;
        frozen_ = other.frozen_;
        hidden_ = other.hidden_;
        disappeared_ = other.disappeared_;
        selected_ = other.selected_;
        label_ = other.label_;
        icon_name_ = other.icon_name_;
        tooltip_ = other.tooltip_;
        return *this;
    }

    virtual bool exec() = 0;

    void on_accel_changed(Acc & acc, char32_t kc, int km) {
        acc.cx_ = acc.acc_.signal_changed().connect(bind_back(fun(this, &Data::on_accel_changed), std::ref(acc), acc.acc_.key_code(), acc.acc_.key_modifier()));

        if (signal_accel_changed_) {
            Accel old(kc, km);
            (*signal_accel_changed_)(acc.acc_, old);
        }
    }

    void add_accel(char32_t kc, int km) {
        if (KC_NONE != kc) {
            auto i = std::find_if(accels_.begin(), accels_.end(), [kc, km](auto & acc) { return acc.acc_.equal(kc, km); } );

            if (i == accels_.end()) {
                auto & acc = accels_.emplace_back(kc, km);
                acc.cx_ = acc.acc_.signal_changed().connect(bind_back(fun(this, &Data::on_accel_changed), std::ref(acc), acc.acc_.key_code(), acc.acc_.key_modifier()));
                acc.acc_.connect(fun(this, &Data::exec));
                if (signal_accel_added_) { (*signal_accel_added_)(acc.acc_); }
            }
        }
    }

    void on_accel_added(const Accel & accel) {
        auto [ kc, km ] = accel.keys();
        add_accel(kc, km);
    }

    void remove_accel(char32_t kc, int km) {
        Accel accel(kc, km);
        auto i = std::find_if(accels_.begin(), accels_.end(), [&accel](auto & acc) { return accel == acc.acc_; } );

        if (i != accels_.end()) {
            accels_.erase(i);
            if (signal_accel_removed_) { (*signal_accel_removed_)(accel); }
        }
    }

    void on_accel_removed(const Accel & accel) {
        auto [ kc, km ] = accel.keys();
        remove_accel(kc, km);
    }

    void freeze() {
        if (!frozen_) {
            frozen_ = true;
            if (!disabled_) { on_disable(); }
        }
    }

    void thaw() {
        if (frozen_) {
            frozen_ = false;
            if (!disabled_) { on_enable(); }
        }
    }

    void appear() {
        if (disappeared_) {
            disappeared_ = false;
            if (!hidden_ && signal_show_) { (*signal_show_)(); }
        }
    }

    void disappear() {
        if (!disappeared_) {
            disappeared_ = true;
            if (!hidden_ && signal_hide_) { (*signal_hide_)(); }
        }
    }

    void on_enable() {
        for (auto & accel: accels_) { accel.acc_.enable(); }
        if (signal_enable_) (*signal_enable_)();
    }

    void on_disable() {
        for (auto & accel: accels_) { accel.acc_.disable(); }
        if (signal_disable_) (*signal_disable_)();
    }

    bool enabled() const noexcept {
        return !disabled_ && !frozen_;
    }

    signal<void()> & signal_disable() {
        if (!signal_disable_) { signal_disable_ = new signal<void()>; }
        return *signal_disable_;
    }

    signal<void()> & signal_enable() {
        if (!signal_enable_) { signal_enable_ = new signal<void()>; }
        return *signal_enable_;
    }

    signal<void()> & signal_show() {
        if (!signal_show_) { signal_show_ = new signal<void()>; }
        return *signal_show_;
    }

    signal<void()> & signal_hide() {
        if (!signal_hide_) { signal_hide_ = new signal<void()>; }
        return *signal_hide_;
    }

    signal<void(const Accel &)> & signal_accel_added() {
        if (!signal_accel_added_) { signal_accel_added_ = new signal<void(const Accel &)>; }
        return *signal_accel_added_;
    }

    signal<void(const Accel &, const Accel &)> & signal_accel_changed() {
        if (!signal_accel_changed_) { signal_accel_changed_ = new signal<void(const Accel &, const Accel &)>; }
        return *signal_accel_changed_;
    }

    signal<void(const Accel &)> & signal_accel_removed() {
        if (!signal_accel_removed_) { signal_accel_removed_ = new signal<void(const Accel &)>; }
        return *signal_accel_removed_;
    }

    signal<void(const ustring &)> & signal_label_changed() {
        if (!signal_label_changed_) { signal_label_changed_ = new signal<void(const ustring &)>; }
        return *signal_label_changed_;
    }

    signal<void(const ustring &)> & signal_icon_changed() {
        if (!signal_icon_changed_) { signal_icon_changed_ = new signal<void(const ustring &)>; }
        return *signal_icon_changed_;
    }

    signal<void(const ustring &)> & signal_tooltip_changed() {
        if (!signal_tooltip_changed_) { signal_tooltip_changed_ = new signal<void(const ustring &)>; }
        return *signal_tooltip_changed_;
    }

private:

    void on_select(bool yes) {
        selected_ = yes;
    }
};

struct Action::Action_data: Action_base::Data {
    signal<void()>    signal_void_;

    Action_data() = default;

    Action_data(const Action_data & other):
        Data(other)
    {
    }

    Action_data(Action_data && other):
        Data(other)
    {
    }

    Action_data & operator=(const Action_data & other) {
        Data::operator=(other);
        return *this;
    }

    Action_data & operator=(Action_data && other) {
        Data::operator=(other);
        return *this;
    }

   ~Action_data() {
        if (signal_before_) { delete signal_before_; }
        if (signal_after_) { delete signal_after_; }
    }

    bool exec() override {
        if (enabled()) {
            if (signal_before_ && !signal_before_->operator()()) {
                return true;
            }

            bool res = !signal_void_.empty();

            if (res) {
                signal_void_();
            }

            if (signal_after_ && !signal_after_->empty()) {
                signal_after_->operator()();
                return true;
            }

            return res;
        }

        return false;
    }

    signal_all<> & signal_before() {
        if (!signal_before_) { signal_before_ = new signal_all<>; }
        return *signal_before_;
    }

    signal_all<> & signal_after() {
        if (!signal_after_) { signal_after_ = new signal_all<>; }
        return *signal_after_;
    }

private:

    signal_all<>    * signal_before_    = nullptr;
    signal_all<>    * signal_after_     = nullptr;
};

#define ACTION_DATA static_cast<Action::Action_data *>(data)

// ----------------------------------------------------------------------------
// ----------------------------------------------------------------------------

// TODO 0.8 make_unique() & remove.
Action_base::~Action_base() {
    delete data;
}

void Action_base::set_label(const ustring & label) {
    if (data->label_ != label) {
        data->label_ = label;
        if (data->signal_label_changed_) { (*data->signal_label_changed_)(data->label_); }
    }
}

ustring Action_base::label() const {
    return data->label_;
}

void Action_base::enable() {
    if (data->disabled_) {
        data->disabled_ = false;
        if (!data->frozen_) { data->on_enable(); }
    }
}

void Action_base::disable() {
    if (!data->disabled_) {
        data->disabled_ = true;
        if (!data->frozen_) { data->on_disable(); }
    }
}

void Action_base::par_enable(bool yes) {
    if (yes) { enable(); }
    else { disable(); }
}

bool Action_base::enabled() const noexcept {
    return data->enabled();
}

void Action_base::show() {
    if (data->hidden_) {
        data->hidden_ = false;
        if (!data->disappeared_ && data->signal_show_) { (*data->signal_show_)(); }
    }
}

void Action_base::hide() {
    if (!data->hidden_) {
        data->hidden_ = true;
        if (!data->disappeared_ && data->signal_hide_) { (*data->signal_hide_)(); }
    }
}

void Action_base::par_show(bool yes) {
    if (yes) { show(); }
    else { hide(); }
}

bool Action_base::visible() const noexcept {
    return !data->hidden_ && !data->disappeared_;
}

bool Action_base::selected() const noexcept {
    return data->selected_;
}

void Action_base::set_icon(const ustring & icon_name) {
    if (data->icon_name_ != icon_name) {
        data->icon_name_ = icon_name;
        if (data->signal_icon_changed_) { (*data->signal_icon_changed_)(data->icon_name_); }
    }
}

ustring Action_base::icon_name() const {
    return data->icon_name_;
}

void Action_base::set_tooltip(const ustring & tooltip) {
    if (data->tooltip_ != tooltip) {
        data->tooltip_ = tooltip;
        if (data->signal_tooltip_changed_) { (*data->signal_tooltip_changed_)(data->tooltip_); }
    }
}

void Action_base::unset_tooltip() {
    if (!data->tooltip_.empty()) {
        data->tooltip_.clear();
        if (data->signal_tooltip_changed_) { (*data->signal_tooltip_changed_)(data->tooltip_); }
    }
}

ustring Action_base::tooltip() const {
    return data->tooltip_;
}

void Action_base::add_accel(char32_t kc, int km) {
    data->add_accel(kc, km);
}

void Action_base::add_accels(const ustring & key_specs) {
    for (const ustring & spec: str_explode(key_specs, Locale().blanks())) {
        if (!spec.empty()) {
            char32_t kc; int km;
            key_spec_from_string(spec, kc, km);
            if (0 == kc) { std::cerr << "** Action_base::add_accels(): failed to resolve specification " << spec << std::endl; }
            else { add_accel(kc, km); }
        }
    }
}

void Action_base::remove_accel(char32_t kc, int km) {
    data->remove_accel(kc, km);
}

void Action_base::remove_accels(const ustring & key_specs) {
    for (const ustring & spec: str_explode(key_specs, Locale().blanks())) {
        char32_t kc; int km;
        key_spec_from_string(spec, kc, km);
        remove_accel(kc, km);
    }
}

void Action_base::clear_accels() {
    if (data->signal_accel_removed_) {
        for (auto & acc: data->accels_) {
            (*data->signal_accel_removed_)(acc.acc_);
        }
    }

    data->accels_.clear();
}

void Action_base::set_master_action(Master_action & master_action) {
    data->accel_added_cx_ = master_action.signal_accel_added().connect(fun(data, &Data::on_accel_added));
    data->accel_removed_cx_ = master_action.signal_accel_removed().connect(fun(data, &Data::on_accel_removed));
    data->enable_cx_ = master_action.signal_enable().connect(fun(data, &Data::thaw));
    data->disable_cx_ = master_action.signal_disable().connect(fun(data, &Data::freeze));
    data->show_cx_ = master_action.signal_show().connect(fun(data, &Data::appear));
    data->hide_cx_ = master_action.signal_hide().connect(fun(data, &Data::disappear));
    data->label_changed_cx_ = master_action.signal_label_changed().connect(fun(this, &Action_base::set_label));
    data->icon_changed_cx_ = master_action.signal_icon_changed().connect(fun(this, &Action_base::set_icon));
    data->tooltip_changed_cx_ = master_action.signal_tooltip_changed().connect(fun(this, &Action_base::set_tooltip));

    clear_accels();

    for (Accel * accel: master_action.accels()) {
        auto [ kc, km ] = accel->keys();
        add_accel(kc, km);
    }

    if (!master_action.enabled()) { data->freeze(); }
    if (!master_action.visible()) { data->disappear(); }
    set_label(master_action.label());
    set_icon(master_action.icon_name());
    set_tooltip(master_action.tooltip());
}

void Action_base::set_master_action(Master_action * master_action) {
    if (master_action) {
        set_master_action(*master_action);
    }
}

void Action_base::set_master_action(const ustring & name) {
    set_master_action(Theme_impl::root()->find_action(name));
}

Action_base * Action_base::lookup(char32_t kc, int km) {
    for (auto & accel: data->accels_) {
        if (accel.acc_.equal(kc, km)) {
            return this;
        }
    }

    return nullptr;
}

std::vector<Accel *> Action_base::accels() {
    std::vector<Accel *> v(data->accels_.size());
    auto i = v.begin();
    for (auto & acc: data->accels_) { *i++ = &acc.acc_; }
    return v;
}

std::vector<const Accel *> Action_base::accels() const {
    std::vector<const Accel *> v(data->accels_.size());
    auto i = v.begin();
    for (auto & acc: data->accels_) { *i++ = &acc.acc_; }
    return v;
}

signal<void()> & Action_base::signal_disable() {
    return data->signal_disable();
}

signal<void()> & Action_base::signal_enable() {
    return data->signal_enable();
}

signal<void()> & Action_base::signal_hide() {
    return data->signal_hide();
}

signal<void()> & Action_base::signal_show() {
    return data->signal_show();
}

signal<void(const Accel &)> & Action_base::signal_accel_added() {
    return data->signal_accel_added();
}

signal<void(const Accel &, const Accel &)> & Action_base::signal_accel_changed() {
    return data->signal_accel_changed();
}

signal<void(const Accel &)> & Action_base::signal_accel_removed() {
    return data->signal_accel_removed();
}

signal<void(const ustring &)> & Action_base::signal_label_changed() {
    return data->signal_label_changed();
}

signal<void(const ustring &)> & Action_base::signal_icon_changed() {
    return data->signal_icon_changed();
}

signal<void(const ustring &)> & Action_base::signal_tooltip_changed() {
    return data->signal_tooltip_changed();
}

signal<void()> & Action_base::signal_select() {
    return data->signal_select_;
}

signal<void()> & Action_base::signal_unselect() {
    return data->signal_unselect_;
}

// TODO 0.8 remove.
signal<void()> & Action_base::signal_destroy() {
    return data->signal_destroy_;
}

// ----------------------------------------------------------------------------
// ----------------------------------------------------------------------------

Action::Action() {
    data = new Action_data;
}

Action::Action(slot<void()> slot_activate) {
    data = new Action_data;
    ACTION_DATA->signal_void_.connect(slot_activate);
}

Action::Action(const ustring & accels, slot<void()> slot_activate) {
    data = new Action_data;
    add_accels(accels);
    if (slot_activate) { ACTION_DATA->signal_void_.connect(slot_activate); }
}

Action::Action(char32_t kc, int km, slot<void()> slot_activate) {
    data = new Action_data;
    add_accel(kc, km);
    if (slot_activate) { ACTION_DATA->signal_void_.connect(slot_activate); }
}

Action::Action(const ustring & accels, const ustring & label, slot<void()> slot_activate) {
    data = new Action_data;
    add_accels(accels);
    set_label(label);
    if (slot_activate) { ACTION_DATA->signal_void_.connect(slot_activate); }
}

Action::Action(char32_t kc, int km, const ustring & label, slot<void()> slot_activate) {
    data = new Action_data;
    add_accel(kc, km);
    set_label(label);
    if (slot_activate) { ACTION_DATA->signal_void_.connect(slot_activate); }
}

Action::Action(const ustring & accels, const ustring & label, const ustring & icon_name, slot<void()> slot_activate) {
    data = new Action_data;
    add_accels(accels);
    set_label(label);
    set_icon(icon_name);
    if (slot_activate) { ACTION_DATA->signal_void_.connect(slot_activate); }
}

Action::Action(char32_t kc, int km, const ustring & label, const ustring & icon_name, slot<void()> slot_activate) {
    data = new Action_data;
    add_accel(kc, km);
    set_label(label);
    set_icon(icon_name);
    if (slot_activate) { ACTION_DATA->signal_void_.connect(slot_activate); }
}

Action::Action(const ustring & accels, const ustring & label, const ustring & icon_name, const ustring & tooltip, slot<void()> slot_activate) {
    data = new Action_data;
    add_accels(accels);
    set_label(label);
    set_icon(icon_name);
    set_tooltip(tooltip);
    if (slot_activate) { ACTION_DATA->signal_void_.connect(slot_activate); }
}

Action::Action(char32_t kc, int km, const ustring & label, const ustring & icon_name, const ustring & tooltip, slot<void()> slot_activate) {
    data = new Action_data;
    add_accel(kc, km);
    set_label(label);
    set_icon(icon_name);
    set_tooltip(tooltip);
    if (slot_activate) { ACTION_DATA->signal_void_.connect(slot_activate); }
}

Action::Action(Master_action & master_action, slot<void()> slot_activate) {
    data = new Action_data;
    set_master_action(master_action);
    if (slot_activate) { ACTION_DATA->signal_void_.connect(slot_activate); }
}

Action::Action(Master_action * master_action, slot<void()> slot_activate) {
    data = new Action_data;
    set_master_action(master_action);
    if (slot_activate) { ACTION_DATA->signal_void_.connect(slot_activate); }
}

Action::Action(const Action & other) {
    data = new Action_data(*(static_cast<Action_data *>(other.data)));
}

Action::Action(Action && other) {
    data = new Action_data(*(static_cast<Action_data *>(other.data)));
}

Action::~Action() {
}

Action & Action::operator=(const Action & other) {
    if (this != &other) { *data = *other.data; }
    return *this;
}

Action & Action::operator=(Action && other) {
    *data = *other.data;
    return *this;
}

void Action::exec() const {
    ACTION_DATA->exec();
}

void Action::operator()() const {
    exec();
}

connection Action::connect_before(slot<bool()> slot, bool prepend) {
    return ACTION_DATA->signal_before().connect(slot, prepend);
}

connection Action::connect(slot<void()> slot, bool prepend) {
    return ACTION_DATA->signal_void_.connect(slot, prepend);
}

connection Action::connect_after(slot<bool()> slot, bool prepend) {
    return ACTION_DATA->signal_after().connect(slot, prepend);
}

// ----------------------------------------------------------------------------
// ----------------------------------------------------------------------------

// TODO signal_before_, signal_after_.
struct Toggle_action::Toggle_data: Action_base::Data {
    bool                state_ = false;
    signal<void(bool)>  signal_toggle_;
    signal<void(bool)>  signal_setup_;


    Toggle_data() {
        signal_select_.connect(fun(this, &Toggle_data::emit_setup));
    }

    ~Toggle_data() {
        if (signal_before_) { delete signal_before_; }
        if (signal_after_) { delete signal_after_; }
    }

    Toggle_data(const Toggle_data & other):
        Data(other)
    {
    }

    Toggle_data(Toggle_data && other):
        Data(other)
    {
    }

    Toggle_data & operator=(const Toggle_data & other) {
        Data::operator=(other);
        return *this;
    }

    Toggle_data & operator=(Toggle_data && other) {
        Data::operator=(other);
        return *this;
    }

    bool exec() override {
        set(!state_);
        return enabled();
    }

    void setup(bool state) {
        if (state_ != state) {
            state_ = state;
            emit_setup();
        }
    }

    void set(bool state) {
        if (enabled() && state_ != state) {
            if (signal_before_ && !signal_before_->operator()(state)) { return; }
            setup(state);
            signal_toggle_(state_);
            if (signal_after_) { signal_after_->operator()(state); }
        }
    }


    signal_all<bool> & signal_before() {
        if (!signal_before_) { signal_before_ = new signal_all<bool>; }
        return *signal_before_;
    }

    signal_all<bool> & signal_after() {
        if (!signal_after_) { signal_after_ = new signal_all<bool>; }
        return *signal_after_;
    }

private:

    signal_all<bool>  * signal_before_  = nullptr;
    signal_all<bool>  * signal_after_   = nullptr;

    void emit_setup() {
        signal_setup_(state_);
    }
};

#define TOGGLE_DATA static_cast<Toggle_action::Toggle_data *>(data)

Toggle_action::Toggle_action():
    Action_base()
{
    data = new Toggle_data;
}

Toggle_action::Toggle_action(slot<void(bool)> slot_toggle):
    Action_base()
{
    data = new Toggle_data;
    TOGGLE_DATA->signal_toggle_.connect(slot_toggle);
}

Toggle_action::Toggle_action(const ustring & accels, slot<void(bool)> slot_toggle) {
    data = new Toggle_data;
    add_accels(accels);
    if (slot_toggle) { TOGGLE_DATA->signal_toggle_.connect(slot_toggle); }
}

Toggle_action::Toggle_action(char32_t kc, int km, slot<void(bool)> slot_toggle) {
    data = new Toggle_data;
    add_accel(kc, km);
    if (slot_toggle) { TOGGLE_DATA->signal_toggle_.connect(slot_toggle); }
}

Toggle_action::Toggle_action(const ustring & accels, const ustring & label, slot<void(bool)> slot_toggle) {
    data = new Toggle_data;
    add_accels(accels);
    set_label(label);
    if (slot_toggle) { TOGGLE_DATA->signal_toggle_.connect(slot_toggle); }
}

Toggle_action::Toggle_action(char32_t kc, int km, const ustring & label, slot<void(bool)> slot_toggle) {
    data = new Toggle_data;
    add_accel(kc, km);
    set_label(label);
    if (slot_toggle) { TOGGLE_DATA->signal_toggle_.connect(slot_toggle); }
}

Toggle_action::Toggle_action(const ustring & accels, const ustring & label, const ustring & icon_name, slot<void(bool)> slot_toggle) {
    data = new Toggle_data;
    add_accels(accels);
    set_label(label);
    set_icon(icon_name);
    if (slot_toggle) { TOGGLE_DATA->signal_toggle_.connect(slot_toggle); }
}

Toggle_action::Toggle_action(char32_t kc, int km, const ustring & label, const ustring & icon_name, slot<void(bool)> slot_toggle) {
    data = new Toggle_data;
    add_accel(kc, km);
    set_label(label);
    set_icon(icon_name);
    if (slot_toggle) { TOGGLE_DATA->signal_toggle_.connect(slot_toggle); }
}

Toggle_action::Toggle_action(const ustring & accels, const ustring & label, const ustring & icon_name, const ustring & tooltip, slot<void(bool)> slot_toggle) {
    data = new Toggle_data;
    add_accels(accels);
    set_label(label);
    set_icon(icon_name);
    set_tooltip(tooltip);
    if (slot_toggle) { TOGGLE_DATA->signal_toggle_.connect(slot_toggle); }
}

Toggle_action::Toggle_action(char32_t kc, int km, const ustring & label, const ustring & icon_name, const ustring & tooltip, slot<void(bool)> slot_toggle) {
    data = new Toggle_data;
    add_accel(kc, km);
    set_label(label);
    set_icon(icon_name);
    set_tooltip(tooltip);
    if (slot_toggle) { TOGGLE_DATA->signal_toggle_.connect(slot_toggle); }
}

Toggle_action::Toggle_action(Master_action & master_action, slot<void(bool)> slot_toggle) {
    data = new Toggle_data;
    set_master_action(master_action);
    if (slot_toggle) { TOGGLE_DATA->signal_toggle_.connect(slot_toggle); }
}

Toggle_action::Toggle_action(Master_action * master_action, slot<void(bool)> slot_toggle) {
    data = new Toggle_data;
    set_master_action(master_action);
    if (slot_toggle) { TOGGLE_DATA->signal_toggle_.connect(slot_toggle); }
}

Toggle_action::Toggle_action(const Toggle_action & other) {
    data = new Toggle_data(*(static_cast<Toggle_data *>(other.data)));
}

Toggle_action::Toggle_action(Toggle_action && other) {
    data = new Toggle_data(*(static_cast<Toggle_data *>(other.data)));
}

Toggle_action & Toggle_action::operator=(const Toggle_action & other) {
    if (this != &other) { *data = *other.data; }
    return *this;
}

Toggle_action & Toggle_action::operator=(Toggle_action && other) {
    *data = *other.data;
    return *this;
}

Toggle_action::~Toggle_action() {}

void Toggle_action::toggle() {
    TOGGLE_DATA->exec();
}

void Toggle_action::operator()() {
    toggle();
}

bool Toggle_action::get() const noexcept {
    return TOGGLE_DATA->state_;
}

void Toggle_action::setup(bool state) {
    TOGGLE_DATA->setup(state);
}

void Toggle_action::set(bool state) {
    TOGGLE_DATA->set(state);
}

void Toggle_action::operator()(bool state) {
    set(state);
}

connection Toggle_action::connect_before(slot<bool(bool)> slot, bool prepend) {
    return TOGGLE_DATA->signal_before().connect(slot, prepend);
}

connection Toggle_action::connect_after(slot<bool(bool)> slot, bool prepend) {
    return TOGGLE_DATA->signal_after().connect(slot, prepend);
}

connection Toggle_action::connect(slot<void(bool)> slot_toggle, bool prepend) {
    return TOGGLE_DATA->signal_toggle_.connect(slot_toggle, prepend);
}

signal<void(bool state)> & Toggle_action::signal_setup() {
    return TOGGLE_DATA->signal_setup_;
}

} // namespace tau

//END

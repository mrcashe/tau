// ----------------------------------------------------------------------------
// SPDX-License-Identifier: BSD-2-Clause
// Copyright © 2014-2024 Konstantin Shmelkov <mrcashe@gmail.com>.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
// THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
// PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
// CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
// EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
// PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
// OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
// WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
// OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
// EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// ----------------------------------------------------------------------------

#ifndef __TAU_MENU_IMPL_HH__
#define __TAU_MENU_IMPL_HH__

#include <roller-impl.hh>
#include <separator-impl.hh>

namespace tau {

extern const char * MENU_ITEM_TAG;

class Menu_impl: public Roller_impl {
public:

    // Overriden by Menubar_impl.
    // Overriden by Menubox_impl.
    virtual void remove(Widget_impl * wp);

    // Redefines Roller_impl.
    // Overriden by Menubar_impl.
    // Overriden by Menubox_impl.
    virtual void clear();

    // Overriden by Menubar_impl.
    // Overriden by Menubox_impl.
    virtual void append(Widget_ptr wp, bool shrink=false) = 0;

    // Overriden by Menubar_impl.
    // Overriden by Menubox_impl.
    virtual void prepend(Widget_ptr wp, bool shrink=false) = 0;

    // Overriden by Menubar_impl.
    // Overriden by Menubox_impl.
    virtual void insert_before(Widget_ptr wp, const Widget_impl * other, bool shrink=false) = 0;

    // Overriden by Menubar_impl.
    // Overriden by Menubox_impl.
    virtual void insert_after(Widget_ptr wp, const Widget_impl * other, bool shrink=false) = 0;

    // Overriden by Menubar_impl.
    // Overriden by Menubox_impl.
    virtual bool empty() const noexcept = 0;

    virtual void child_menu_cancel() = 0;
    virtual void child_menu_left() = 0;
    virtual void child_menu_right() = 0;

    bool has_enabled_items() const noexcept;
    void quit_menu();
    void pass_quit();
    bool horizontal() const noexcept { return Orientation::RIGHT == orient_ || Orientation::LEFT == orient_; }

    Widget_ptr append_separator(Separator::Style separator_style=Separator::GROOVE) {
        Separator_ptr sp = std::make_shared<Separator_impl>(separator_style);
        append(sp);
        return sp;
    }

    Widget_ptr prepend_separator(Separator::Style separator_style=Separator::GROOVE) {
        Separator_ptr sp = std::make_shared<Separator_impl>(separator_style);
        prepend(sp);
        return sp;
    }

    Widget_ptr insert_separator_before(const Widget_impl * other, Separator::Style separator_style=Separator::GROOVE) {
        Separator_ptr sp = std::make_shared<Separator_impl>(separator_style);
        insert_before(sp, other);
        return sp;
    }

    Widget_ptr insert_separator_after(const Widget_impl * other, Separator::Style separator_style=Separator::GROOVE) {
        Separator_ptr sp = std::make_shared<Separator_impl>(separator_style);
        insert_after(sp, other);
        return sp;
    }

    Menu_impl * parent_menu() { return pmenu_; }
    const Menu_impl * parent_menu() const { return pmenu_; }
    Menu_impl * unset_parent_menu();
    void close_submenu();

    signal<void()> & signal_quit() { return signal_quit_; }

protected:

    Orientation         orient_;
    Orientation         submenu_or_     = Orientation::RIGHT;
    Menu_impl *         pmenu_          = nullptr;
    Menu_ptr            submenu_;
    Menu_item_ptr       current_item_;
    Menu_item_impl *    marked_item_    = nullptr;
    Action              action_enter_   { "Enter Space", fun(this, &Menu_impl::activate_current) };
    Action              action_cancel_  { "Escape Cancel", fun(this, &Menu_impl::cancel) };
    Action              action_home_    { KC_HOME, KM_NONE, fun(this, &Menu_impl::on_home) };
    Action              action_end_     { KC_END, KM_NONE, fun(this, &Menu_impl::on_end) };
    signal<void()>      signal_quit_;

protected:

    Menu_impl(Orientation orient);

    // Overriden by Menubar_impl.
    // Overriden by Menubox_impl.
    virtual void mark_item(Widget_impl * wp, bool select) = 0;

    bool emit_current();
    void add(Widget_ptr wp);
    void cancel();
    Menu_item_ptr current_item();
    Menu_item_ptr item_ptr(Widget_impl * item);
    Menu_item_ptr next_item();
    Menu_item_ptr previous_item();
    Menu_impl * hover_menu(const Point & pt) noexcept;

    Menu_item_ptr select_item(Menu_item_ptr item);
    Menu_item_ptr hover_item(const Point & pt) noexcept;
    bool open_current();

    void unselect_current();
    void select_current();
    void select_next();
    void select_prev();

    void activate_current();
    void reset_submenu();
    void unmark();

private:

    signal_all<>        signal_enabled_items_;
    connection          open_cx_        { true };

private:

    void mark();
    void drop_open() { open_cx_.drop(); }
    void step_scroller(bool increase);

    void on_home();
    void on_end();
    void on_item_enable(Menu_item_impl * ip, bool yes);
    void on_item_enter(const Point & pt, Widget_impl * wp);
    bool on_item_mouse_down(int mbt, int mm, const Point & pt, Widget_impl * wp);
    bool on_mouse_down(int mbt, int mm, const Point & pt);
    void on_open_timer();
    void on_display_in();
};

} // namespace tau

#endif // __TAU_MENU_IMPL_HH__

// ----------------------------------------------------------------------------
// SPDX-License-Identifier: BSD-2-Clause
// Copyright © 2014-2024 Konstantin Shmelkov <mrcashe@gmail.com>.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
// THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
// PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
// CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
// EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
// PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
// OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
// WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
// OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
// EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// ----------------------------------------------------------------------------

/// @file cycle-impl.cc The Cycle_impl class implementation.
/// The header is cycle-impl.hh.

#include <tau/brush.hh>
#include <tau/icon.hh>
#include <box-impl.hh>
#include <button-impl.hh>
#include <cycle-impl.hh>
#include <icon-impl.hh>
#include <label-impl.hh>
#include <table-impl.hh>

namespace tau {

Cycle_impl::Cycle_impl(Border bs):
    Frame_impl(bs)
{
    user_style_ = border_left_style();

    auto box = std::make_shared<Box_impl>(Orientation::LEFT, 1);
    box_ = box.get(); box_->hint_margin(1); insert(box);

    box = std::make_shared<Box_impl>(Orientation::DOWN, Align::FILL, 1); box_->append(box, true);
    bbox_ = box.get();

    auto table = std::make_shared<Table_impl>(tau::Align::CENTER, tau::Align::FILL, 1, 0); box_->append(table);
    itable_ = table.get();
    itable_->conf().redirect(Conf::BACKGROUND, Conf::WHITESPACE_BACKGROUND);
    itable_->align_column(0, tau::Align::FILL);

    auto card = std::make_shared<Card_impl>(); itable_->put(card, 0, 0, 1, 1, false, true);
    card_ = card.get();

    auto up = std::make_shared<Button_impl>(action_up_, Icon::NANO, Action::ICON);
    up->conf().integer(Conf::RADIUS) = 0; up->enable_repeat(); up->hint_size(12, 0);
    bbox_->append(up, true);

    auto down = std::make_shared<Button_impl>(action_down_, Icon::NANO, Action::ICON);
    down->conf().integer(Conf::RADIUS) = 0; down->enable_repeat(); down->hint_size(12, 0);
    bbox_->append(down, true);

    conf().signal_changed(Conf::RADIUS).connect(fun(this, &Cycle_impl::on_conf_radius));
    action_cancel_.connect_after(bind_return(fun(this, &Cycle_impl::drop_focus), true));
    action_up_.connect(fun(this, &Cycle_impl::hide_tooltip));
    action_up_.connect(fun(card_, &Card_impl::show_previous));
    action_down_.connect(fun(this, &Cycle_impl::hide_tooltip));
    action_down_.connect(fun(card_, &Card_impl::show_next));
    action_up_.disable();
    action_down_.disable();

    connect_action(action_up_);
    connect_action(action_down_);
    connect_action(action_cancel_);
    connect_action(action_activate_);

    signal_mouse_wheel().connect(fun(this, &Cycle_impl::on_mouse_wheel), true);
    signal_mouse_down().connect(fun(this, &Cycle_impl::on_mouse_down));
    signal_display_in_.connect(fun(this, &Cycle_impl::on_conf_radius));
    signal_focus_in_.connect(fun(this, &Cycle_impl::on_focus_in));
    signal_focus_out_.connect(fun(this, &Cycle_impl::on_focus_out));
    signal_take_focus_.connect(fun(this, &Cycle_impl::on_take_focus), true);
}

void Cycle_impl::set_border_style(Border bs) {
    user_style_ = bs;
    if (Border::NONE == bs) { box_->hint_margin(0); }
    else { box_->hint_margin(2); }
    Frame_impl::set_border_style(bs);
}

int Cycle_impl::add(Widget_ptr wp) {
    wp->signal_show().connect(fun(wp->signal_select()));
    wp->signal_hide().connect(fun(wp->signal_unselect()));
    card_->insert(wp);
    auto [ i, succeed ] = map_.emplace(id_, Holder());
    i->second.wp = wp.get();
    i->second.cx = wp->signal_select().connect(bind_back(fun(this, &Cycle_impl::on_select), id_));
    update_enable();
    signal_new_id_(id_);
    return id_++;
}

std::size_t Cycle_impl::remove(Widget_impl * wp) {
    std::size_t n = card_->remove(wp);

    if (n) {
        auto i = std::find_if(map_.begin(), map_.end(), [wp](auto & p) { return p.second.wp == wp; });

        if (i != map_.end()) {
            int id = i->first;
            map_.erase(i);
            signal_remove_id_(id);
            if (id == fixed_) { unfix(); }
        }

        update_enable();
        return n;
    }

    return itable_->remove(wp);
}

std::size_t Cycle_impl::remove(int id) {
    std::size_t n = 0;
    auto i = map_.find(id);
    if (i != map_.end()) { n = remove(i->second.wp); }
    return n;
}

// Overriden bt Cycle_text_impl.
void Cycle_impl::clear() {
    for (auto & p: map_) { signal_remove_id_(p.first); }
    card_->clear();
    action_up_.disable();
    action_down_.disable();
    fixed_ = INT_MIN;
}

void Cycle_impl::update_enable() {
    bool en = fixed_ < 0 && 1 < card_->count();
    action_up_.par_enable(en);
    action_down_.par_enable(en);
}

int Cycle_impl::select(Widget_impl * wp, bool fix) {
    if (!fixed()) {
        auto i = std::find_if(map_.begin(), map_.end(), [wp](auto & p) { return wp == p.second.wp; });

        if (i != map_.end()) {
            wp->show();
            if (fix) { fixed_ = i->first; }
            return i->first;
        }
    }

    return INT_MIN;
}

int Cycle_impl::select(int id, bool fix) {
    if (!fixed()) {
        auto i = map_.find(id);

        if (i != map_.end()) {
            i->second.wp->show();
            if (fix) { fixed_ = id; }
            return i->first;
        }
    }

    return INT_MIN;
}

int Cycle_impl::setup(Widget_impl * wp, bool fix) {
    if (!fixed()) {
        auto i = std::find_if(map_.begin(), map_.end(), [wp](auto & p) { return wp == p.second.wp; });

        if (i != map_.end()) {
            i->second.cx.block();
            i->second.wp->show();
            i->second.cx.unblock();
            if (fix) { fixed_ = i->first; }
            return i->first;
        }
    }

    return INT_MIN;
}

int Cycle_impl::setup(int id, bool fix) {
    if (!fixed()) {
        auto i = map_.find(id);

        if (i != map_.end()) {
            i->second.cx.block();
            i->second.wp->show();
            i->second.cx.unblock();
            if (fix) { fixed_ = id; }
            return i->first;
        }
    }

    return INT_MIN;
}

void Cycle_impl::unfix() {
    fixed_ = INT_MIN;
    update_enable();
}

int Cycle_impl::current() const noexcept {
    auto i = std::find_if(map_.begin(), map_.end(), [](auto & p) { return !p.second.wp->hidden(); });
    return i != map_.end() ? i->first : INT_MIN;
}

void Cycle_impl::append(Widget_ptr wp, bool shrink) {
    Span rng = itable_->span();
    if (rng.xmax < rng.xmin) { rng.xmax = 0; }
    itable_->put(wp, rng.xmax, 0, 1, 1, shrink, false);
}

Widget_impl * Cycle_impl::widget(int id) {
    auto i = map_.find(id);
    return i != map_.end() ? i->second.wp : nullptr;
}

Widget_impl * Cycle_impl::widget(int id) const {
    auto i = map_.find(id);
    return i != map_.end() ? i->second.wp : nullptr;
}

Widget_ptr Cycle_impl::append(const ustring & text, unsigned margin_left, unsigned margin_right) {
    auto tp = std::make_shared<Label_impl>(text);
    tp->hint_margin_left(margin_left);
    tp->hint_margin_right(margin_right);
    append(tp, true);
    return tp;
}

Widget_ptr Cycle_impl::append(const ustring & str, const Color & color, unsigned margin_left, unsigned margin_right) {
    auto tp = append(str, margin_left, margin_right);
    tp->conf().color(Conf::FOREGROUND) = color;
    return tp;
}

void Cycle_impl::prepend(Widget_ptr wp, bool shrink) {
    Span rng = itable_->span();
    if (rng.xmax < rng.xmin) { rng.xmin = 0; }
    itable_->put(wp, rng.xmin-1, 0, 1, 1, shrink, false);
}

Widget_ptr Cycle_impl::prepend(const ustring & text, unsigned margin_left, unsigned margin_right) {
    auto tp = std::make_shared<Label_impl>(text);
    tp->hint_margin_left(margin_left);
    tp->hint_margin_right(margin_right);
    prepend(tp, true);
    return tp;
}

Widget_ptr Cycle_impl::prepend(const ustring & str, const Color & color, unsigned margin_left, unsigned margin_right) {
    auto tp = prepend(str, margin_left, margin_right);
    tp->conf().color(Conf::FOREGROUND) = color;
    return tp;
}

bool Cycle_impl::on_mouse_wheel(int delta, int mm, const Point & where) {
    bool yes = false;
    if (delta > 0) { yes = action_down_.enabled(); action_down_(); }
    else { yes = action_up_.enabled(); action_up_(); }
    return yes;
}

bool Cycle_impl::on_mouse_down(int mbt, int mm, const Point & where) {
    return MBT_LEFT == mbt && take_focus();
}

void Cycle_impl::on_focus_in() {
    disallow_tooltip();
    set_border_color(conf().brush(Conf::SELECT_BACKGROUND).value().color());
    Frame_impl::set_border_style(Border::SOLID);
}

bool Cycle_impl::on_take_focus() {
    return card_->take_focus() ? true : grab_focus();
}

void Cycle_impl::on_focus_out() {
    allow_tooltip();
    unset_border_color();
    Frame_impl::set_border_style(user_style_);
}

void Cycle_impl::on_select(int id) {
    signal_select_id_(id);
}

void Cycle_impl::on_conf_radius() {
    set_border_radius(conf().integer(Conf::RADIUS));
}

} // namespace tau

//END

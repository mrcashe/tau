// ----------------------------------------------------------------------------
// SPDX-License-Identifier: BSD-2-Clause
// Copyright © 2014-2024 Konstantin Shmelkov <mrcashe@gmail.com>.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
// THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
// PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
// CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
// EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
// PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
// OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
// WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
// OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
// EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// ----------------------------------------------------------------------------

#include <tau/exception.hh>
#include <display-impl.hh>
#include <menubox-impl.hh>
#include <menu-item-impl.hh>
#include <popup-impl.hh>
#include <iostream>

namespace tau {

Menubox_impl::Menubox_impl():
    Menu_impl(Orientation::DOWN)
{
    hint_margin(3);
    auto list = std::make_shared<List_impl>();
    list->conf().unredirect(Conf::BACKGROUND);
    list->set_columns_margin(2, 2);
    insert(list);
    list_ = list.get();
    connect_action(action_previous_);
    connect_action(action_next_);
    connect_action(action_left_);
    connect_action(action_right_);
}

void Menubox_impl::on_left() {
    bool q = true;

    if (Gravity::RIGHT == gravity_ || Gravity::TOP_RIGHT == gravity_ || Gravity::BOTTOM_RIGHT == gravity_) {
        q = !open_current();
    }

    if (q) {
        if (auto pmenu = unset_parent_menu()) {
            quit_menu();
            pmenu->child_menu_left();
        }
    }
}

void Menubox_impl::on_right() {
    bool q = true;

    if (Gravity::LEFT == gravity_ || Gravity::TOP_LEFT == gravity_ || Gravity::BOTTOM_LEFT == gravity_) {
        q = !open_current();
    }

    if (q) {
        if (auto pmenu = unset_parent_menu()) {
            quit_menu();
            pmenu->child_menu_right();
        }
    }
}

void Menubox_impl::child_menu_cancel() {
    close_submenu();
    grab_modal();
    grab_mouse();
}

void Menubox_impl::child_menu_left() {
    close_submenu();
    grab_modal();
    grab_mouse();
}

void Menubox_impl::child_menu_right() {
    close_submenu();
    grab_modal();
    grab_mouse();
}

// Overrides pure Menu_impl.
void Menubox_impl::mark_item(Widget_impl * wp, bool select) {
    if (select) { list_->select(list_->span(wp).ymin); }
    else { list_->unselect(); }
}

Window_ptr Menubox_impl::popup(Widget_ptr self, Menu_impl * pmenu) {
    if (auto dp = Display_impl::this_ptr()) {
        if (auto wip = dp->mouse_owner()) {
            return popup(wip, self, pmenu);
        }
    }

    throw internal_error(str_format(__func__, ": can't obtain mouse owning window"));
}

Window_ptr Menubox_impl::popup(Window_impl * wip, Widget_ptr self, Menu_impl * pmenu) {
    return popup(wip, self, wip->where_mouse(), pmenu);
}

Window_ptr Menubox_impl::popup(Window_impl * wip, Widget_ptr self, const Point & origin, Menu_impl * pmenu) {
    Point pos;
    Gravity gravity;

    if (Orientation::RIGHT == orient_) {
        pos = to_parent(wip, origin);
        gravity = Gravity::TOP_LEFT;
    }

    else {
        int y = origin.y()-margin_hint().top-2;  // FIXME from where this '2'?
        Point p1 = to_parent(wip, Point(size().width()+margin_hint().right, y));
        Point p2 = to_parent(wip, Point(-margin_hint().left, y));

        if (wip->size().iwidth()-p1.x() >= p2.x()) {
            pos = p1;
            gravity = Gravity::TOP_LEFT;
        }

        else {
            pos = p2;
            gravity = Gravity::TOP_RIGHT;
        }
    }

    return popup(wip, self, origin, gravity, pmenu);
}

Window_ptr Menubox_impl::popup(Window_impl * wip, Widget_ptr self, Gravity gravity, Menu_impl * pmenu) {
    return popup(wip, self, wip->where_mouse(), gravity, pmenu);
}

Window_ptr Menubox_impl::popup(Window_impl * wip, Widget_ptr self, const Point & origin, Gravity gravity, Menu_impl * pmenu) {
    if (!wip) { throw internal_error("got pure window"); }
    auto dp = wip->display();
    if (!dp) { throw internal_error("unable to obtain display"); }
    auto pip = dp->create_popup(wip, origin, gravity);
    auto frame = std::make_shared<Frame_impl>(Border::OUTSET);
    pip->insert(frame);
    frame->insert(self);
    gravity_ = gravity;
    pmenu_ = pmenu;
    pip->conf().redirect(Conf::BACKGROUND, Conf::MENU_BACKGROUND);
    signal_quit_.connect(fun(pip, &Window_impl::close));
    pip->signal_display_in().connect(fun(this, &Menubox_impl::select_current));
    pip->show();
    if (!grab_modal()) { frame->clear(); pip->close(); throw internal_error("failed to gain modal focus"); }
    if (!grab_mouse()) { frame->clear(); pip->close(); throw internal_error("failed to grab mouse"); }
    return pip;
}

void Menubox_impl::put_widget(Widget_ptr wp, int y) {
    wp->hint_margin(2, 2, 0, 0);

    if (auto ip = std::dynamic_pointer_cast<Menu_item_impl>(wp)) {
        Widget_ptr aux1, aux2, aux3, aux4;

        if (auto accel = std::dynamic_pointer_cast<Menu_accel>(ip)) {
            accel->accel_label_->hint_margin(2, 0, 0, 0);
            list_->insert(y, accel->accel_label_, 2, Align::END, true);
            aux1 = aux3 = accel->accel_label_;
        }

        if (auto img = std::dynamic_pointer_cast<Menu_image>(ip)) {
            if (Widget_ptr image = img->img_) {
                image->hint_margin(2, 2, 0, 0);
                list_->insert(y, image, -1, true);
                aux2 = aux4 = image;
            }
        }

        if (auto submenu = std::dynamic_pointer_cast<Submenu_impl>(ip)) {
            Widget_ptr arrow = submenu->arrow();
            arrow->hint_margin(2, 0, 0, 0);
            list_->insert(y, arrow, 2, Align::END, true);
            aux1 = aux3 = arrow;
        }

        else if (auto check_item = std::dynamic_pointer_cast<Check_menu_impl>(ip)) {
            if (Check_ptr check = check_item->check_ptr()) {
                list_->insert(y, check, -1, true);
                check->set_string(MENU_ITEM_TAG);
                aux3 = check;
            }
        }

        if (aux1) {
            aux1->set_string(MENU_ITEM_TAG);
            aux1->par_enable(!ip->disabled());
            ip->signal_enable().connect(fun(aux1, &Widget_impl::enable));
            ip->signal_disable().connect(fun(aux1, &Widget_impl::disable));
        }

        if (aux2) {
            aux2->set_string(MENU_ITEM_TAG);
            aux2->par_enable(!ip->disabled());
            ip->signal_enable().connect(fun(aux2, &Widget_impl::enable));
            ip->signal_disable().connect(fun(aux2, &Widget_impl::disable));
        }

        if (aux3) {
            aux3->par_show(!ip->hidden());
            ip->signal_show().connect(fun(aux3, &Widget_impl::show));
            ip->signal_hide().connect(fun(aux3, &Widget_impl::hide));
        }

        if (aux4) {
            aux4->par_show(!ip->hidden());
            ip->signal_show().connect(fun(aux4, &Widget_impl::show));
            ip->signal_hide().connect(fun(aux4, &Widget_impl::hide));
        }
    }

    add(wp);
    arrange();
}

// Overrides pure Menu_impl.
void Menubox_impl::append(Widget_ptr wp, bool) {
    int y;
    if (std::dynamic_pointer_cast<Menu_item_impl>(wp)) { y = list_->append_row(wp, Align::START, true); }
    else { y = list_->append(wp, Align::FILL, true); }
    put_widget(wp, y);
}

// Overrides pure Menu_impl.
void Menubox_impl::prepend(Widget_ptr wp, bool) {
    int row;
    if (std::dynamic_pointer_cast<Menu_item_impl>(wp)) { row = list_->prepend_row(wp, Align::START, true); }
    else { row = list_->prepend(wp, Align::FILL, true); }
    put_widget(wp, row);
}

// Overrides pure Menu_impl.
void Menubox_impl::insert_before(Widget_ptr wp, const Widget_impl * other, bool) {
    Span rng = list_->span(other);
    int row;
    if (std::dynamic_pointer_cast<Menu_item_impl>(wp)) { row = list_->insert_row(wp, rng.ymin, Align::START, true); }
    else { row = list_->insert(wp, rng.ymin, Align::FILL, true); }

    put_widget(wp, row);
}

// Overrides pure Menu_impl.
void Menubox_impl::insert_after(Widget_ptr wp, const Widget_impl * other, bool) {
    Span rng = list_->span(other);
    int row;
    if (std::dynamic_pointer_cast<Menu_item_impl>(wp)) { row = list_->insert_row(wp, 1+rng.ymin, Align::START, true); }
    else { row = list_->insert(wp, 1+rng.ymin, Align::FILL, true); }
    put_widget(wp, row);
}

// Overrides Menu_impl.
void Menubox_impl::remove(Widget_impl * wp) {
    list_->remove(list_->span(wp).ymin);
    Menu_impl::remove(wp);
    arrange();
}

// Overrides Menu_impl.
void Menubox_impl::clear() {
    list_->clear();
    Menu_impl::clear();
}

void Menubox_impl::arrange() {
    Span trng = list_->span();

    for (auto wp: compound()->children()) {
        if (wp->get_string() != MENU_ITEM_TAG) {
            Span spn = list_->span(wp.get());
            list_->respan(wp.get(), { std::dynamic_pointer_cast<Separator_impl>(wp) ? trng.xmin : 0, spn.ymin, trng.xmax, spn.ymin+1 });
        }
    }

    trng = list_->span();
    bool fsep = true;

    for (int yy = trng.ymin; yy < trng.ymax; ++yy) {
        auto v = list_->widgets(yy);
        Separator_ptr sep;
        if (1 == v.size()) { sep = std::dynamic_pointer_cast<Separator_impl>(v.front()); }
        if (sep) { sep->par_show(!fsep); }
        fsep = nullptr != sep;
    }

    fsep = false;

    for (int yy = trng.ymax-1; yy >= trng.ymin; --yy) {
        auto v = list_->widgets(yy);

        if (1 == v.size()) {
            if (auto sep = std::dynamic_pointer_cast<Separator_impl>(v.front())) {
                if (fsep) { sep->hide(); }
                fsep = true;
            }

            else {
                break;
            }
        }

        else {
            break;
        }
    }
}

} // namespace tau

//END

// ----------------------------------------------------------------------------
// Copyright © 2014-2024 Konstantin Shmelkov <mrcashe@gmail.com>.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
// THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
// PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
// CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
// EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
// PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
// OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
// WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
// OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
// EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// ----------------------------------------------------------------------------

#include <tau/script.hh>
#include <tau/string.hh>

namespace tau {

struct Script_data {
    const char * code;
    const char * name;
    int          id;
};

static const Script_data datas[] = {
    { // 0
        .code   = "Zzzz",
        .name   = "Unknown",
        .id     = 999,
    },
    { // 1
        .code   = "Zyyy",
        .name   = "Common",
        .id     = 998,
    },
    { // 2
        .code   = "Zinh",
        .name   = "Inherited",
        .id     = 994,
    },
    { // 3
        .code   = "Arab",
        .name   = "Arabic",
        .id     = 160,
    },
    { // 4
        .code   = "Armn",
        .name   = "Armenian",
        .id     = 230,
    },
    { // 5
        .code   = "Beng",
        .name   = "Bengali",
        .id     = 325,
    },
    { // 6
        .code   = "Bopo",
        .name   = "Bopomofo",
        .id     = 285,
    },
    { // 7
        .code   = "Cher",
        .name   = "Cherokee",
        .id     = 445,
    },
    { // 8
        .code   = "Copt",
        .name   = "Coptic",
        .id     = 204,
    },
    { // 9
        .code   = "Cyrl",
        .name   = "Cyrillic",
        .id     = 220,
    },
    { // 10
        .code   = "Dsrt",
        .name   = "Deseret",
        .id     = 250,
    },
    { // 11
        .code   = "Deva",
        .name   = "Devanagari",
        .id     = 315,
    },
    { // 12
        .code   = "Ethi",
        .name   = "Ethiopic",
        .id     = 430,
    },
    { // 13
        .code   = "Geor",
        .name   = "Georgian",
        .id     = 240,
    },
    { // 14
        .code   = "Goth",
        .name   = "Gothic",
        .id     = 206,
    },
    { // 15
        .code   = "Grek",
        .name   = "Greek",
        .id     = 200,
    },
    { // 16
        .code   = "Gujr",
        .name   = "Gujarati",
        .id     = 320,
    },
    { // 17
        .code   = "Guru",
        .name   = "Gurmukhi",
        .id     = 310,
    },
    { // 18
        .code   = "Hani",
        .name   = "Han",
        .id     = 500,
    },
    { // 19
        .code   = "Hang",
        .name   = "Hangul",
        .id     = 286,
    },
    { // 20
        .code   = "Hebr",
        .name   = "Hebrew",
        .id     = 125,
    },
    { // 21
        .code   = "Hira",
        .name   = "Hiragana",
        .id     = 410,
    },
    { // 22
        .code   = "Knda",
        .name   = "Kannada",
        .id     = 345,
    },
    { // 23
        .code   = "Kana",
        .name   = "Katakana",
        .id     = 411,
    },
    { // 24
        .code   = "Khmr",
        .name   = "Khmer",
        .id     = 355,
    },
    { // 25
        .code   = "Laoo",
        .name   = "Lao",
        .id     = 356,
    },
    { // 26
        .code   = "Latn",
        .name   = "Latin",
        .id     = 215,
    },
    { // 27
        .code   = "Mlym",
        .name   = "Malayalam",
        .id     = 347,
    },
    { // 28
        .code   = "Mong",
        .name   = "Mongolian",
        .id     = 145,
    },
    { // 29
        .code   = "Mymr",
        .name   = "Myanmar",
        .id     = 350,
    },
    { // 30
        .code   = "Ogam",
        .name   = "Ogham",
        .id     = 212,
    },
    { // 31
        .code   = "Ital",
        .name   = "Old Italic",
        .id     = 212,
    },
    { // 32
        .code   = "Orya",
        .name   = "Oriya",
        .id     = 327,
    },
    { // 33
        .code   = "Runr",
        .name   = "Runic",
        .id     = 211,
    },
    { // 34
        .code   = "Sinh",
        .name   = "Sinhala",
        .id     = 348,
    },
    { // 35
        .code   = "Syrc",
        .name   = "Syriac",
        .id     = 135,
    },
    { // 36
        .code   = "Taml",
        .name   = "Tamil",
        .id     = 346,
    },
    { // 37
        .code   = "Telu",
        .name   = "Telugu",
        .id     = 340,
    },
    { // 38
        .code   = "Thaa",
        .name   = "Thaana",
        .id     = 170,
    },
    { // 39
        .code   = "Thai",
        .name   = "Thai",
        .id     = 352,
    },
    { // 40
        .code   = "Tibt",
        .name   = "Tibetian",
        .id     = 330,
    },
    { // 41
        .code   = "Cans",
        .name   = "Canadian Aboriginal",
        .id     = 440,
    },
    { // 42
        .code   = "Yiii",
        .name   = "Yi",
        .id     = 460,
    },
    { // 43
        .code   = "Tglg",
        .name   = "Tagalog",
        .id     = 370,
    },
    { // 44
        .code   = "Hano",
        .name   = "Hanundo",
        .id     = 371,
    },
    { // 45
        .code   = "Buhd",
        .name   = "Buhid",
        .id     = 372,
    },
    { // 46
        .code   = "Tagb",
        .name   = "Tagbanwa",
        .id     = 373,
    },
    { // 47
        .code   = "Brai",
        .name   = "Braille",
        .id     = 570,
    },
    { // 48
        .code   = "Cprt",
        .name   = "Cypriot",
        .id     = 403,
    },
    { // 49
        .code   = "Limb",
        .name   = "Limbu",
        .id     = 336,
    },
    { // 50
        .code   = "Osma",
        .name   = "Osmanya",
        .id     = 260,
    },
    { // 51
        .code   = "Shaw",
        .name   = "Shavian",
        .id     = 281,
    },
    { // 52
        .code   = "Linb",
        .name   = "Linear B",
        .id     = 401,
    },
    { // 53
        .code   = "Tale",
        .name   = "Thai Le",
        .id     = 353,
    },
    { // 54
        .code   = "Ugar",
        .name   = "Ugaritic",
        .id     = 040,
    },
    { // 55
        .code   = "Talu",
        .name   = "New Tai Lue",
        .id     = 354,
    },
    { // 56
        .code   = "Bugi",
        .name   = "Buginese",
        .id     = 367,
    },
    { // 57
        .code   = "Glag",
        .name   = "Glagolitic",
        .id     = 225,
    },
    { // 58
        .code   = "Tfng",
        .name   = "Tifinagh",
        .id     = 120,
    },
    { // 59
        .code   = "Sylo",
        .name   = "Syloti Nagry",
        .id     = 316,
    },
    { // 60
        .code   = "Xpeo",
        .name   = "Old Persian",
        .id     = 030,
    },
    { // 61
        .code   = "Khar",
        .name   = "Kharoshthi",
        .id     = 305,
    },
    { // 62
        .code   = "Bali",
        .name   = "Balinese",
        .id     = 360,
    },
    { // 63
        .code   = "Xsux",
        .name   = "Cuneiform",
        .id     = 020,
    },
    { // 64
        .code   = "Phnx",
        .name   = "Phoenitian",
        .id     = 115,
    },
    { // 65
        .code   = "Phag",
        .name   = "Phags-pa",
        .id     = 331,
    },
    { // 66
        .code   = "Nkoo",
        .name   = "N'Ko",
        .id     = 165,
    },
    { // 67
        .code   = "Kali",
        .name   = "Kayah Li",
        .id     = 357,
    },
    { // 68
        .code   = "Lepc",
        .name   = "LepchaLi",
        .id     = 335,
    },
    { // 69
        .code   = "Rjng",
        .name   = "Rejang",
        .id     = 363,
    },
    { // 70
        .code   = "Sund",
        .name   = "Sundanese",
        .id     = 362,
    },
    { // 71
        .code   = "Saur",
        .name   = "Saurashtra",
        .id     = 344,
    },
    { // 72
        .code   = "Cham",
        .name   = "Cham",
        .id     = 358,
    },
    { // 73
        .code   = "Olck",
        .name   = "Ol Chiki",
        .id     = 261,
    },
    { // 74
        .code   = "Vaii",
        .name   = "Vai",
        .id     = 470,
    },
    { // 75
        .code   = "Cari",
        .name   = "Carian",
        .id     = 201,
    },
    { // 76
        .code   = "Lyci",
        .name   = "Lucian",
        .id     = 202,
    },
    { // 77
        .code   = "Lydi",
        .name   = "Ludian",
        .id     = 116,
    },
    { // 78
        .code   = "Batk",
        .name   = "Batak",
        .id     = 365,
    },
    { // 79
        .code   = "Brah",
        .name   = "Brahmi",
        .id     = 300,
    },
    { // 80
        .code   = "Mand",
        .name   = "Mandaic",
        .id     = 140,
    },
    { // 81
        .code   = "Cakm",
        .name   = "Chakma",
        .id     = 349,
    },
    { // 82
        .code   = "Merc",
        .name   = "Meroitic Cursive",
        .id     = 101,
    },
    { // 83
        .code   = "Mero",
        .name   = "Meroitic Hieroglyphs",
        .id     = 100,
    },
    { // 84
        .code   = "Plrd",
        .name   = "Miao",
        .id     = 282,
    },
    { // 85
        .code   = "Shrd",
        .name   = "Sharada",
        .id     = 319,
    },
    { // 86
        .code   = "Sora",
        .name   = "Sora Sompeng",
        .id     = 398,
    },
    { // 87
        .code   = "Takr",
        .name   = "Takri",
        .id     = 321,
    },
    {
        .code   = nullptr,
        .name   = nullptr,
        .id     = -1
    }
};

Script::Script():
    data(&datas[0])
{
}

Script::Script(const Script_data * datap):
    data(datap)
{
}

Script::Script(int id) {
    for (const Script_data * datap = datas; datap->name; ++datap) {
        if (datap->id == id) {
            data = datap;
            return;
        }
    }

    data = datas;
}

std::string Script::name() const {
    return data->name;
}

std::string Script::code() const {
    return data->code;
}

int Script::id() const {
    return data->id;
}

Script Script::from_name(std::string_view name) {
    std::string uname = str_toupper(str_trim(name));

    for (const Script_data * datap = datas; datap->name; ++datap) {
        if (str_toupper(datap->name) == uname) {
            return Script(datap);
        }
    }

    return Script(0);
}

Script Script::from_code(std::string_view code) {
    std::string ucode = str_toupper(str_trim(code));

    for (const Script_data * datap = datas; datap->name; ++datap) {
        if (str_toupper(datap->code) == ucode) {
            return Script(datap);
        }
    }

    return Script(0);
}

bool Script::operator==(const Script & other) const {
    return data == other.data;
}

bool Script::operator!=(const Script & other) const {
    return data != other.data;
}

} // namespace tau

//END

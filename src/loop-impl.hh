// ----------------------------------------------------------------------------
// SPDX-License-Identifier: BSD-2-Clause
// Copyright © 2014-2024 Konstantin Shmelkov <mrcashe@gmail.com>.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
// THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
// PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
// CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
// EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
// PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
// OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
// WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
// OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
// EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// ----------------------------------------------------------------------------

/// @file loop-impl.hh The Loop_impl class declaration.

#ifndef __TAU_LOOP_IMPL_HH__
#define __TAU_LOOP_IMPL_HH__

#include <sys-impl.hh>
#include <tau/loop.hh>
#include <map>
#include <mutex>
#include <thread>

namespace tau {

class Loop_impl: public trackable {
public:

    /// Make plymorphic.
    virtual ~Loop_impl() = default;

    using Slot_iterate = Loop::Slot_iterate;
    using Signal_iterate = Loop::Signal_iterate;

    /// TODO Make thread-local variable for current.
    static Loop_ptr this_ptr();
    static Loop_ptr that_ptr(std::thread::id tid);
    static std::size_t count() noexcept;
    static std::vector<Loop_ptr> list();

    void run();
    void quit();
    int id() const { return id_; }
    bool running() const noexcept { return runlevel_ > 0; }
    bool alive() const { return runlevel_ >= 0; }
    Timer_ptr make_timer(int time_ms=0, bool periodical=false);
    void take_timer(Timer_ptr tp);
    void start_timer(Timer_ptr tp);
    void stop_timer(Timer_ptr tp);
    Sysinfo sysinfo() noexcept { return sysinfo_; }
    std::vector<ustring> mounts() { return list_mount_points(); }
    virtual Event_ptr event() = 0;
    connection alarm(slot<void()> slot_alarm, int timeout_ms, bool periodical=false);

    signal<void()> & signal_start() { return signal_start_; }
    signal<void()> & signal_idle() { return signal_idle_; }
    signal<void()> & signal_run() { return signal_run_; }
    signal<void()> & signal_quit() { return signal_quit_; }
    signal<void(int, const ustring &)> & signal_watch();
    Signal_iterate & signal_iterate() { return signal_iterate_; }

protected:

    signal<void()>  signal_start_;
    signal<void()>  signal_idle_;
    signal<void()>  signal_run_;
    signal<void()>  signal_quit_;
    signal<void(int, const ustring &)> signal_watch_;

protected:

    Loop_impl();
    static void new_loop(Loop_ptr loop);

    /// Do one iteration of run cycle.
    /// @param timeout_ms the timeout in milliseconds.
    /// @return @b true if had events, otherwise a timeout expired.
    virtual bool iterate(int timeout_ms) = 0;

    // Overridden by Loop_unix.
    virtual void init_mounts() {}

    // Overridden by Loop_unix.
    virtual void done_mounts() {}

    // Overridden by Loop_unix.
    // Overridden by Loop_win.
    virtual void on_quit() {}

private:

    using Timers    = std::multimap<uint64_t, Timer_ptr>;
    using Taken     = std::vector<Timer_ptr>;

    int                     id_             = -1;
    int                     runlevel_       = 0;
    uint64_t                uidle_          = 200000;       // Idle timeout in microseconds.
    uint64_t                idle_           = 0;
    uint64_t                run_            = UINT64_MAX;
    uint64_t                earliest_       = UINT64_MAX;
    Timers                  timers_;
    Timer_ptr               top_;           // Currently processing timer.
    Taken                   taken_;
    connection              kache_cx_       { true };
    connection              idle_cx_        { true };
    Event_ptr               kache_event_;
    std::recursive_mutex    mmx_;
    Signal_iterate          signal_iterate_;

private:

    // Outer iterate.
    void outer_iterate();

    // Check kache_ Key file.
    void chk_kache();

    // Called when kache_ changed.
    void on_kache_changed();

    // Done with kache_.
    void done_kache();

    // Default handler for signal_iterate_.
    bool on_iterate(const Slot_iterate & slot);

    void on_idle();
};

} // namespace tau

#endif // __TAU_LOOP_IMPL_HH__

// ----------------------------------------------------------------------------
// SPDX-License-Identifier: BSD-2-Clause
// Copyright © 2014-2024 Konstantin Shmelkov <mrcashe@gmail.com>.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
// THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
// PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
// CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
// EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
// PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
// OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
// WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
// OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
// EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// ----------------------------------------------------------------------------

#include <tau/timer.hh>
#include <tau/timeval.hh>
#include <loop-impl.hh>
#include <timer-impl.hh>

namespace tau {

Timer::Timer():
    impl(Loop_impl::this_ptr()->make_timer())
{
    impl->timer_ = true;
}

Timer::Timer(slot<void()> slot_alarm, int time_ms, bool periodical):
    impl(Loop_impl::this_ptr()->make_timer(time_ms, periodical))
{
    impl->timer_ = true;
    connect(slot_alarm);
    restart(time_ms, periodical);
}

Timer::~Timer() {
    impl->timer_ = false;
    if (impl->loop_) { impl->loop_->take_timer(impl); }
}

connection Timer::connect(slot<void()> slot_alarm, bool prepend) {
    impl->cx_ = impl->signal_alarm_.connect(slot_alarm, prepend);
    return impl->cx_;
}

bool Timer::empty() const noexcept {
    return impl->signal_alarm_.empty();
}

void Timer::start(int time_ms, bool periodical) {
    if (!impl->running_) {
        restart(time_ms, periodical);
    }
}

void Timer::restart(int time_ms, bool periodical) {
    if (time_ms > 0 && impl->loop_) {
        if (impl->running_) { impl->loop_->stop_timer(impl); }
        impl->time_ms_ = time_ms;
        impl->periodical_ = periodical;
        impl->loop_->start_timer(impl);
    }

    else { impl->pause_ = false; }
}

void Timer::restart() {
    if (impl->loop_) {
        if (impl->running_) { impl->loop_->stop_timer(impl); }
        impl->loop_->start_timer(impl);
    }

    else { impl->pause_ = false; }
}

void Timer::stop() {
    if (impl->running_ && impl->loop_) { impl->loop_->stop_timer(impl); }
    impl->pause_ = false;
}

void Timer::pause() {
    if (impl->running_ && impl->loop_) {
        impl->loop_->stop_timer(impl);
        uint64_t now = Timeval::now();
        impl->rem_ = impl->time_point_ > now ? (impl->time_point_-now)/1000 : 0;
        impl->pause_ = true;
    }
}

bool Timer::running() const noexcept {
    return impl->running_ || impl->pause_;
}

void Timer::resume() {
    if (impl->pause_ && impl->loop_) {
        impl->loop_->start_timer(impl);
    }
}

int Timer::timeout() const noexcept {
    return impl->time_ms_;
}

int Timer::remain() const noexcept {
    if (impl->pause_) { return impl->rem_; }
    uint64_t now = Timeval::now();
    return impl->time_point_ > now ? (impl->time_point_-now)/1000 : 0;
}

bool Timer::paused() const noexcept {
    return impl->pause_;
}

void Timer::flush() {
    if (impl->running_) {
        stop();
        impl->signal_alarm_();
        if (impl->periodical_) { restart(); }
    }
}

} // namespace tau

//END

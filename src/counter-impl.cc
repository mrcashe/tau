// ----------------------------------------------------------------------------
// SPDX-License-Identifier: BSD-2-Clause
// Copyright © 2014-2024 Konstantin Shmelkov <mrcashe@gmail.com>.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
// THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
// PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
// CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
// EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
// PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
// OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
// WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
// OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
// EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// ----------------------------------------------------------------------------

#include <tau/brush.hh>
#include <tau/icon.hh>
#include <tau/locale.hh>
#include <tau/string.hh>
#include <box-impl.hh>
#include <button-impl.hh>
#include <counter-impl.hh>
#include <entry-impl.hh>
#include <icon-impl.hh>
#include <cmath>
#include <iomanip>
#include <iostream>         // cerr

namespace {

const int MAX_PRECISION = 12;

} // anonymous namespace

namespace tau {

Counter_impl::Counter_impl(double value, double max_value, double min_value):
    Frame_impl(Border::INSET)
{
    init(value, max_value, min_value);
}

Counter_impl::Counter_impl(Border bs, double value, double max_value, double min_value):
    Frame_impl(bs)
{
    init(value, max_value, min_value);
}

void Counter_impl::init(double value, double max_value, double min_value) {
    auto box = std::make_shared<Box_impl>(Orientation::LEFT);
    box->signal_mouse_wheel().connect(fun(this, &Counter_impl::on_mouse_wheel), true);
    box->action_focus_next().disable();
    box->action_focus_previous().disable();
    box_ = box.get();
    box_->conf().integer(Conf::RADIUS) = 0;
    insert(box);

    box = std::make_shared<Box_impl>(Orientation::DOWN, Align::FILL);
    box->disallow_focus();
    bbox_ = box.get();
    box_->append(box, true);

    box = std::make_shared<Box_impl>();
    box->action_focus_next().disable();
    box->action_focus_previous().disable();
    ibox_ = box.get();
    box_->append(box);

    auto entry = std::make_shared<Entry_impl>(Align::CENTER, Border::NONE);
    ibox_->append(entry);
    entry_ = entry.get();
    entry_->signal_validate().connect(fun(this, &Counter_impl::on_validate));
    entry_->signal_approve().connect(fun(this, &Counter_impl::on_approve));
    entry_->action_cancel().connect(fun(this, &Counter_impl::draw_value));
    entry_->action_cancel().connect(fun(this, &Counter_impl::drop_focus));
    entry_->action_activate().connect(fun(this, &Counter_impl::on_entry_activate));
    entry_->conf().integer(Conf::RADIUS) = 0;

    auto up = std::make_shared<Button_impl>(action_up_, Icon::NANO, Action::ICON);
    up->enable_repeat();
    up->hint_size(12, 0);
    bbox_->append(up, true);

    auto down = std::make_shared<Button_impl>(action_down_, Icon::NANO, Action::ICON);
    down->enable_repeat();
    down->hint_size(12, 0);
    bbox_->append(down, true);

    set_max_value(max_value);
    set_min_value(min_value);
    adjust_precision(value);
    update_value(value, true);

    conf().signal_changed(Conf::RADIUS).connect(fun(this, &Counter_impl::on_conf_radius));
    signal_display_in_.connect(fun(this, &Counter_impl::on_conf_radius));

    action_up_.connect(bind_back(fun(this, &Counter_impl::do_step), true));
    action_down_.connect(bind_back(fun(this, &Counter_impl::do_step), false));
    action_up_.connect(fun(entry_, &Entry_impl::unselect));
    action_down_.connect(fun(entry_, &Entry_impl::unselect));
    action_previous_page_.connect(fun(entry_, &Entry_impl::unselect));
    action_previous_page_.connect(fun(this, &Counter_impl::increase_page));
    action_next_page_.connect(fun(entry_, &Entry_impl::unselect));
    action_next_page_.connect(fun(this, &Counter_impl::decrease_page));

    connect_action(action_up_);
    connect_action(action_down_);
    connect_action(action_previous_page_);
    connect_action(action_next_page_);

    conf().signal_changed(Conf::FONT).connect(fun(this, &Counter_impl::adjust_width));
    conf().signal_changed(Conf::EDIT_FONT).connect(fun(this, &Counter_impl::adjust_width));
    signal_focus_in_.connect(fun(this, &Counter_impl::on_focus_in));
    signal_focus_out_.connect(fun(this, &Counter_impl::on_focus_out));
    signal_take_focus_.connect(fun(entry_, &Widget_impl::take_focus), true);
    signal_display_in_.connect(fun(this, &Counter_impl::adjust_width));

    entry_->conf().unredirect(Conf::BACKGROUND);
    signal_border_changed().connect(fun(this, &Counter_impl::on_border_changed));
    allow_edit();
    on_border_changed();
}

void Counter_impl::allow_edit() {
    ibox_->conf().redirect(Conf::BACKGROUND, Conf::WHITESPACE_BACKGROUND);
    entry_->allow_edit();
    action_up_.enable();
    action_down_.enable();
    action_next_page_.enable();
    action_previous_page_.enable();
}

void Counter_impl::disallow_edit() {
    ibox_->conf().unredirect(Conf::BACKGROUND);
    drop_entry_focus();
    update_value(value_);
    entry_->disallow_edit();
    action_up_.disable();
    action_down_.disable();
    action_next_page_.disable();
    action_previous_page_.disable();
}

void Counter_impl::show_buttons() {
    bbox_->show();
}

void Counter_impl::hide_buttons() {
    bbox_->hide();
}

bool Counter_impl::buttons_visible() const noexcept {
    return !bbox_->hidden();
}

void Counter_impl::draw_value() {
    auto s = format_value(value_);
    entry_->assign(s);
}

bool Counter_impl::update_value(double value, bool force) {
    if (value < min_value_) {
        value = min_value_;
    }

    else if (max_value_ > min_value_ && value >= max_value_) {
        value = max_value_;
    }

    if (force || (value_ != value) || empty()) {
        value_ = value;
        draw_value();
        return true;
    }

    return false;
}

void Counter_impl::assign(double value) {
    update_value(value);
}

void Counter_impl::assign() {
    update_value(value_);
}

void Counter_impl::set_min_value(double min_value) {
    if (min_value_ != min_value) {
        min_value_ = min_value;

        if (value_ < min_value_) {
            if (update_value(min_value_)) {
                signal_value_changed_(value_);
            }
        }
    }
}

void Counter_impl::set_max_value(double max_value) {
    if (max_value_ != max_value) {
        max_value_ = max_value;

        if (value_ > max_value_) {
            if (update_value(max_value_)) {
                signal_value_changed_(value_);
            }
        }
    }
}

void Counter_impl::adjust_precision(double example) {
    double fract = std::fabs(example)-std::floor(std::fabs(example));
    ustring s = str_format(fract);
    ustring::size_type dot_pos = s.find('.');
    int prec = ustring::npos != dot_pos ? s.size()-dot_pos-1 : 0;

    if (prec > precision_) {
        precision_ = std::min(prec, MAX_PRECISION);
        draw_value();
    }

    adjust_width();
}

void Counter_impl::set_step_value(double step_value) {
    step_value = std::fabs(step_value);

    if (step_value > 0) {
        if (step_value_ != step_value) {
            if (max_value_ > min_value_) { step_value_ = std::min(step_value, max_value_-min_value_); }
            else { step_value_ = step_value; }
            adjust_precision(step_value_);
        }
    }
}

void Counter_impl::set_precision(int prec) {
    if (precision_ != prec) {
        precision_ = std::min(prec, MAX_PRECISION);
        adjust_width();
        draw_value();
    }
}

void Counter_impl::set_fixed() {
    if (!fixed_) {
        fixed_ = true;
        adjust_width();
        draw_value();
    }
}

void Counter_impl::unset_fixed() {
    if (fixed_) {
        fixed_ = false;
        adjust_width();
        draw_value();
    }
}

void Counter_impl::set_width(int width) {
    int wabs = abs(width);

    if (width_ != wabs) {
        width_ = wabs;
        adjust_width();
        draw_value();
    }
}

void Counter_impl::set_fill(char c) {
    if (fill_ != c) {
        fill_ = c;
        adjust_width();
        draw_value();
    }
}

void Counter_impl::set_caps() {
    if (!caps_) {
        caps_ = true;
        adjust_width();
        draw_value();
    }
}

void Counter_impl::unset_caps() {
    if (caps_) {
        caps_ = false;
        adjust_width();
        draw_value();
    }
}

void Counter_impl::set_base(int base) {
    base = std::clamp(base, 2, 35);

    if (base_ != base) {
        base_ = base;
        adjust_width();
        draw_value();
    }
}

void Counter_impl::do_step(bool inc) {
    if (enabled()) {
        double val;

        if (inc) {
            val = value_+step_value_;

            if (step_value_ < 1) {
                double av = std::fabs(value_), prc = std::pow(10, -precision_);
                int n = std::floor(av/prc);
                double modulus = av-n*prc;
                // std::cout << modulus << std::endl;
                if (modulus < step_value_ && modulus >= prc) { val -= modulus; }
            }
        }

        else {
            val = value_-step_value_;

            if (step_value_ < 1) {
                double av = std::fabs(value_), prc = std::pow(10, -precision_);
                int n = std::floor(av/prc);
                double modulus = av-n*prc;
                // std::cout << modulus << std::endl;
                if (modulus < step_value_ && modulus >= prc) { val += step_value_-modulus; }
            }
        }

        if (update_value(val)) {
            signal_value_changed_(value_);
        }
    }
}

void Counter_impl::increase_page() {
    if (enabled()) {
        if (update_value(value_+base_*step_value_)) {
            signal_value_changed_(value_);
        }
    }
}

void Counter_impl::decrease_page() {
    if (enabled()) {
        if (update_value(value_-base_*step_value_)) {
            signal_value_changed_(value_);
        }
    }
}

void Counter_impl::drop_entry_focus() {
    entry_->unselect();
    if (entry_->focused()) { entry_->move_to(0); grab_focus(); }
}

ustring Counter_impl::format_value(double v) {
    ustring s;

    if (16 == base_) {
        uintmax_t iv = v;
        s = str_format(std::hex, std::setfill(fill_), std::setw(width_), iv);
    }

    else if (8 == base_) {
        uintmax_t iv = v;
        s = str_format(std::oct, std::setfill(fill_), std::setw(width_), iv);
    }

    else if (2 == base_) {
        uintmax_t iv = v;

        while (0 != iv) {
            s.insert(0, 1, (1 & iv ? '1' : '0'));
            iv >>= 1;
        }

        while (int(s.size()) < width_) { s.insert(0, 1, fill_); }
        if (s.empty()) { s += '0'; }
    }

    else {
        s = fixed_ ? str_format(std::locale(), std::setw(width_), std::setfill(fill_), std::setprecision(precision_), std::fixed, v):
                     str_format(std::locale(), std::setw(width_), std::setfill(fill_), std::setprecision(precision_), v);
        auto dec = Locale().decimal_point();
        if (!dec.empty() && U'.' != dec.front()) { s = str_replace(s, '.', dec.front()); }
    }

    return caps_ ? str_toupper(s) : s;
}

double Counter_impl::get_value() {
    std::string s = str_replace(entry_->str(), ' ', "");
    double value;

    if (10 == base_) {
        std::istringstream is(str_replace(s, ',', '.'));
        is >> value;
    }

    else {
        try { value = std::stoull(s, nullptr, base_); }
        catch (...) { std::cerr << "** Counter_impl: stoull(): an exception thrown, values is " << s << std::endl; }
    }

    value = std::max(value, min_value_);
    if (0.0 != max_value_) { value = std::min(value, max_value_); }
    return value;
}

void Counter_impl::update_from_entry() {
    if (on_approve(entry_->str()) && update_value(get_value())) { signal_value_changed_(value_); }
    else { draw_value(); }
}

void Counter_impl::adjust_width() {
    std::size_t len = std::max(format_value(min_value_).size(), format_value(max_value_).size());
    unsigned w = entry_->text_size(ustring(std::max(std::size_t(3), 2+len), '8')).width();
    entry_->hint_min_size(w, 0);
    entry_->hint_max_size(2*w, 0);
}

void Counter_impl::append(Widget_ptr wp, bool shrink) {
    ibox_->append(wp, shrink);
}

Widget_ptr Counter_impl::append(const ustring & text, unsigned margin_left, unsigned margin_right) {
    Label_ptr wp = std::make_shared<Label_impl>(text);
    wp->hint_margin(margin_left, margin_right, 0, 0);
    append(wp, true);
    return wp;
}

Widget_ptr Counter_impl::append(const ustring & str, const Color & color, unsigned margin_left, unsigned margin_right) {
    auto tp = append(str, margin_left, margin_right);
    tp->conf().color(Conf::FOREGROUND) = color;
    return tp;
}

void Counter_impl::prepend(Widget_ptr wp, bool shrink) {
    ibox_->prepend(wp, shrink);
}

Widget_ptr Counter_impl::prepend(const ustring & text, unsigned margin_left, unsigned margin_right) {
    Label_ptr wp = std::make_shared<Label_impl>(text);
    wp->hint_margin(margin_left, margin_right, 0, 0);
    prepend(wp, true);
    return wp;
}

Widget_ptr Counter_impl::prepend(const ustring & str, const Color & color, unsigned margin_left, unsigned margin_right) {
    auto tp = prepend(str, margin_left, margin_right);
    tp->conf().color(Conf::FOREGROUND) = color;
    return tp;
}

void Counter_impl::on_border_changed() {
    box_->hint_margin(border_visible() ? 1 : 0);
}

bool Counter_impl::on_mouse_wheel(int delta, int mm, const Point & where) {
    if (editable()) {
        drop_entry_focus();

        if (delta > 0) {
            if (MM_CONTROL & mm) { decrease_page(); }
            else { action_down_(); }
        }

        else {
            if (MM_CONTROL & mm) { increase_page(); }
            else { action_up_(); }
        }

        return true;
    }

    return false;
}

void Counter_impl::on_entry_activate() {
    update_from_entry();
}

void Counter_impl::on_activate() {
    signal_activate_(value_);
}

bool Counter_impl::on_approve(const ustring & s) {
    if (!s.empty() && on_validate(s)) {
        std::string ss(str_replace(str_replace(s, ' ', ""), ',', '.'));
        double value;

        if (10 != base_) {
            try {
                value = std::stoull(ss, nullptr, base_);
                return value >= min_value_ && (0.0 == max_value_ || value <= max_value_);
            }

            catch (...) { return false; }
        }

        else {
            std::istringstream is(ss);
            is >> value;
            return value >= min_value_ && (0.0 == max_value_ || value <= max_value_);
        }
    }

    return false;
}

bool Counter_impl::on_validate(const ustring & s) {
    if (!s.empty()) {
        std::string ss(str_replace(str_replace(s, ' ', ""), ',', '.'));

        if (10 != base_) {
            try { auto i = std::stoull(ss, nullptr, base_); (void)i; return true; }
            catch (...) { return false; }
        }

        else {
            return ss.npos == ss.find_first_not_of("0123456789+-eE.");
        }
    }

    return true;
}

void Counter_impl::on_focus_in() {
    if (border_visible()) {
        set_border_color(conf().brush(Conf::SELECT_BACKGROUND).value().color());
    }
}

void Counter_impl::on_focus_out() {
    unset_border_color();
    update_from_entry();
}

void Counter_impl::on_conf_radius() {
    set_border_radius(conf().integer(Conf::RADIUS));
}

bool Counter_impl::editable() const noexcept {
    return entry_->editable();
}

void Counter_impl::clear() {
    entry_->clear();
}

bool Counter_impl::empty() const noexcept {
    return entry_->empty();
}

ustring Counter_impl::str() const {
    return entry_->str();
}

std::u32string Counter_impl::wstr() const {
    return entry_->wstr();
}

Action & Counter_impl::action_cancel() {
    return entry_->action_cancel();
}

Action & Counter_impl::action_activate() {
    return entry_->action_activate();
}

} // namespace tau

//END

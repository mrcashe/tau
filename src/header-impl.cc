// ----------------------------------------------------------------------------
// SPDX-License-Identifier: BSD-2-Clause
// Copyright © 2014-2024 Konstantin Shmelkov <mrcashe@gmail.com>.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
// THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
// PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
// CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
// EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
// PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
// OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
// WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
// OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
// EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// ----------------------------------------------------------------------------

#include <tau/string.hh>
#include <absolute-impl.hh>
#include <box-impl.hh>
#include <header-impl.hh>
#include <icon-impl.hh>
#include <label-impl.hh>
#include <list-impl.hh>
#include <scroller-impl.hh>
#include <separator-impl.hh>
#include <iostream>

namespace tau {

Header_impl::Header_impl(List_ptr list, bool resize_allowed):
    resize_allowed_(resize_allowed)
{
    init(list);
}

Header_impl::Header_impl(Separator::Style sep_style, List_ptr list, bool resize_allowed):
    resize_allowed_(resize_allowed)
{
    init(list);
    set_separator_style(sep_style);
}

void Header_impl::init(List_ptr list) {
    abs_ = std::make_shared<Absolute_impl>();
    abs_->hide();
    scroller_ = std::make_shared<Scroller_impl>();
    scroller_->insert(abs_);
    scroller_->disallow_focus();
    insert(scroller_);

    list_ = list;
    list_->signal_column_bounds_changed().connect(fun(this, &Header_impl::on_column_bounds_changed));
    list_->signal_size_changed().connect(fun(this, &Header_impl::arrange_list));
    list_->signal_hints_changed().connect(fun(this, &Header_impl::sync_scrollers));
    list_->signal_offset_changed().connect(bind_back(fun(this, &Header_impl::sync_scrollers), Hints::SIZE));
}

void Header_impl::show_column(int column) {
    show_column(column, str_format("Column ", column));
}

void Header_impl::show_column(int column, const ustring & title, Align align) {
    show_column(column, std::make_shared<Label_impl>(title, align, Align::CENTER));
}

void Header_impl::show_column(int column, Widget_ptr title) {
    if (list_) {
        auto i = std::find_if(headers_.begin(), headers_.end(), [column](auto & hdr) { return hdr.column_ == column; } );

        if (i == headers_.end()) {
            if (title->margin_hint().left < 2) { title->hint_margin_left(2); }
            auto & hdr = headers_.emplace_back();
            hdr.min_ = list_->min_column_width(column);
            hdr.column_ = column;
            hdr.title_ = title;
            hdr.box_ = std::make_shared<Box_impl>();
            hdr.box_->signal_mouse_down().connect(bind_back(fun(this, &Header_impl::on_header_mouse_down), column));
            hdr.box_->signal_hints_changed().connect(bind_back(fun(this, &Header_impl::on_requisition), column));
            hdr.box_->append(hdr.title_);
            hdr.sep_ = std::make_shared<Separator_impl>(sep_style_);

            if (resize_allowed_) {
                hdr.up_cx_ = hdr.sep_->signal_mouse_down().connect(bind_back(fun(this, &Header_impl::on_sep_mouse_down), std::ref(hdr)));
                hdr.down_cx_ = hdr.sep_->signal_mouse_up().connect(bind_back(fun(this, &Header_impl::on_sep_mouse_up), std::ref(hdr)));
                hdr.motion_cx_ = hdr.sep_->signal_mouse_motion().connect(bind_back(fun(this, &Header_impl::on_sep_mouse_motion), std::ref(hdr)));
                hdr.sep_->set_cursor("size_hor");
            }

            hdr.box_->append(hdr.sep_, true);
            abs_->put(hdr.box_, 0, 0);
            abs_->show();
            arrange_list();
        }
    }
}

void Header_impl::on_requisition(Hints, int y) {
    auto i = std::find_if(headers_.begin(), headers_.end(), [y](auto & hdr) { return hdr.column_ == y; } );
    if (list_ && i != headers_.end()) { list_->set_min_column_width(y, std::max(i->min_, i->box_->required_size().width())); }
}

void Header_impl::hide_column(int y) {
    auto i = std::find_if(headers_.begin(), headers_.end(), [y](auto & hdr) { return hdr.column_ == y; } );

    if (list_ && i != headers_.end()) {
        abs_->remove(i->box_.get());
        list_->set_min_column_width(i->column_, i->min_);
        headers_.erase(i);
        if (abs_->empty()) { abs_->hide(); }
        else { arrange_list(); }
    }
}

void Header_impl::show_sort_marker(int y, bool descend) {
    hide_sort_marker();
    auto i = std::find_if(headers_.begin(), headers_.end(), [y](auto & hdr) { return hdr.column_ == y; } );

    if (i != headers_.end()) {
        ustring picto_name = descend ? "go-down" : "go-up";

        if (!i->marker_) {
            i->marker_ = std::make_shared<Icon_impl>(picto_name, Icon::TINY);
            i->marker_->hint_margin(2, 4, 0, 0);
            i->box_->insert_after(i->marker_, i->title_.get(), true);
        }

        else {
            std::static_pointer_cast<Icon_impl>(i->marker_)->assign(picto_name, Icon::TINY);
        }

        return;
    }
}

void Header_impl::hide_sort_marker() {
    for (auto & hdr: headers_) {
        if (hdr.marker_) {
            hdr.box_->remove(hdr.marker_.get());
            hdr.marker_.reset();
            return;
        }
    }
}

void Header_impl::sync_scrollers(Hints) {
    if (list_) {
        Point ofs = list_->offset();
        abs_->hint_min_size(list_->logical_size().width(), 0);
        scroller_->pan(ofs.x(), 0);
    }
}

void Header_impl::on_column_bounds_changed(int) {
    tmr_.start(638);
}

void Header_impl::arrange_list() {
    if (list_ && !abs_->hidden()) {
        unsigned req_size = 0;

        // Hide last separator.
        auto i = headers_.rbegin();
        if (i != headers_.rend()) { i->sep_->hide(); }
        while (++i != headers_.rend()) { i->sep_->show(); i->sep_->hint_margin_left(list_->spacing().first); }

        // Calculate requisitions.
        for (auto & hdr: headers_) {
            if (list_->column_bounds(hdr.column_)) {
                req_size = std::max(req_size, hdr.box_->required_size().height());
            }
        }

        // Allocate headers.
        for (auto & hdr: headers_) {
            if (Rect r = list_->column_bounds(hdr.column_)) {
                hdr.box_->show();
                abs_->move(hdr.box_.get(), r.left(), 0);
                abs_->resize(hdr.box_.get(), r.width(), req_size);
            }

            else {
                hdr.box_->hide();
            }
        }
    }
}

void Header_impl::set_separator_style(Separator::Style sep_style) {
    if (sep_style_ != sep_style) {
        sep_style_ = sep_style;
        for (auto & hdr: headers_) { hdr.sep_->set_separator_style(sep_style); }
    }
}

void Header_impl::allow_resize() {
    if (!resize_allowed_) {
        resize_allowed_ = true;

        for (auto & hdr: headers_) {
            hdr.up_cx_ = hdr.sep_->signal_mouse_down().connect(bind_back(fun(this, &Header_impl::on_sep_mouse_down), std::ref(hdr)));
            hdr.down_cx_ = hdr.sep_->signal_mouse_up().connect(bind_back(fun(this, &Header_impl::on_sep_mouse_up), std::ref(hdr)));
            hdr.motion_cx_ = hdr.sep_->signal_mouse_motion().connect(bind_back(fun(this, &Header_impl::on_sep_mouse_motion), std::ref(hdr)));
            hdr.sep_->set_cursor("size_hor");
        }
    }
}

void Header_impl::disallow_resize() {
    if (resize_allowed_) {
        resize_allowed_ = false;

        for (auto & hdr: headers_) {
            hdr.up_cx_.drop();
            hdr.down_cx_.drop();
            hdr.motion_cx_.drop();
            hdr.sep_->unset_cursor();
        }
    }
}

bool Header_impl::on_sep_mouse_down(int mbt, int mm, const Point & pt, Header & hdr) {
    if (list_ && MBT_LEFT == mbt) {
        if (Rect r = list_->column_bounds(hdr.column_)) {
            hdr.cw_ = r.width();
            hdr.lx_ = hdr.sep_->to_parent(this, pt).x();
            hdr.fix_ = true;
            hdr.sep_->grab_mouse();
            return true;
        }
    }

    return false;
}

bool Header_impl::on_sep_mouse_up(int mbt, int mm, const Point & pt, Header & hdr) {
    if (MBT_LEFT == mbt) {
        hdr.sep_->ungrab_mouse();
        hdr.fix_ = false;
        return true;
    }

    return false;
}

void Header_impl::on_sep_mouse_motion(int mm, const Point & pt, Header & hdr) {
    if (list_ && hdr.fix_) {
        int dx = hdr.sep_->to_parent(this, pt).x()-hdr.lx_, w = hdr.cw_+dx, wmin = hdr.box_->required_size().width();

        if (w >= wmin) {
            list_->set_column_width(hdr.column_, w);
            signal_width_changed_(hdr.column_);
        }
    }
}

bool Header_impl::on_header_mouse_down(int mbt, int mm, const Point & pt, int column) {
    if (MBT_LEFT == mbt) {
        signal_click_(column);
        return true;
    }

    return false;
}

} // namespace tau

// ----------------------------------------------------------------------------
// SPDX-License-Identifier: BSD-2-Clause
// Copyright © 2014-2024 Konstantin Shmelkov <mrcashe@gmail.com>.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
// THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
// PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
// CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
// EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
// PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
// OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
// WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
// OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
// EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// ----------------------------------------------------------------------------

/// @file box-impl.hh The Box_impl class declaration.
/// Box_impl is a Box implementation class.
/// Implementation is in box-impl.cc file.

#ifndef __TAU_BOX_IMPL_HH__
#define __TAU_BOX_IMPL_HH__

#include <tau/enums.hh>
#include <flat-container.hh>

namespace tau {

class Box_impl: public Flat_container {
public:

    Box_impl(unsigned spacing=0);
    Box_impl(Orientation orient, unsigned spacing=0);
    Box_impl(Align align, unsigned spacing=0);
    Box_impl(Orientation orient, Align align, unsigned spacing=0);
   ~Box_impl();

    Align align() const noexcept { return align_; }
    void align(Align align);
    void set_spacing(unsigned spacing);
    unsigned spacing() const noexcept { return spacing_; }

    void append(Widget_ptr wp, bool shrink=false);
    void prepend(Widget_ptr wp, bool shrink=false);
    void insert_before(Widget_ptr wp, const Widget_impl * other, bool shrink=false);
    void insert_after(Widget_ptr wp, const Widget_impl * other, bool shrink=false);

    void remove(Widget_impl * wp);
    void clear();

    void remove_before(const Widget_impl * wp);
    void remove_after(const Widget_impl * wp);
    void remove_front();
    void remove_back();

    bool empty() const noexcept { return holders_.empty(); }
    bool shrunk(const Widget_impl * wp) const noexcept;
    void shrink(Widget_impl * wp);
    void expand(Widget_impl * wp);
    void shrink_all();
    void expand_all();
    void orientation(Orientation orient);
    Orientation orientation() const noexcept { return orient_; }

    // Dynamically allocated signal.
    signal<void()> & signal_orientation_changed();

    Container_impl * compound() noexcept override { return this; };
    const Container_impl * compound() const noexcept override { return this; };
    bool horizontal() const noexcept { return Orientation::RIGHT == orient_ || Orientation::LEFT == orient_; }

private:

    struct Holder {
        Widget_impl *   wp_             = nullptr;
        bool            sh_: 1          = false;
        Size            req_;           // Last requisition.
        Size            mg_;            // Last margins.
        connection      cx_             { true };
    };

    using Holders = std::list<Holder>;
    using Hiter = Holders::iterator;

    Holders             holders_;
    unsigned            spacing_ = 0;
    Align               align_ = Align::START;
    Orientation         orient_;

    // -----------------------------
    // Cached by update_requisiton()
    // -----------------------------

    std::size_t         nvisible_ = 0;  // Visible widgets count.
    std::size_t         nsh_ = 0;       // Shrunk widget count.
    std::size_t         req_ = 0;       // Sum of spaces, shrunk requisitions and margins.

    signal<void()> *    signal_orientation_changed_ = nullptr;

private:

    void init();
    void update_requisition();
    void arrange();
    void new_child(Hiter hi, Widget_ptr wp, bool shrink);

    void on_hints(Hiter hi, Hints op);
    bool on_focus_next();
    bool on_focus_previous();
    bool on_take_focus();
    bool on_forward();
    bool on_reverse();
};

} // namespace tau

#endif // __TAU_BOX_IMPL_HH__

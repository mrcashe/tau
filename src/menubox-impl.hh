// ----------------------------------------------------------------------------
// SPDX-License-Identifier: BSD-2-Clause
// Copyright © 2014-2024 Konstantin Shmelkov <mrcashe@gmail.com>.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
// THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
// PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
// CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
// EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
// PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
// OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
// WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
// OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
// EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// ----------------------------------------------------------------------------

#ifndef __TAU_MENUBOX_IMPL_HH__
#define __TAU_MENUBOX_IMPL_HH__

#include <menu-impl.hh>
#include <list-impl.hh>

namespace tau {

class Menubox_impl: public Menu_impl {
public:

    Menubox_impl();

    // Overrides pure Menu_impl.
    void append(Widget_ptr wp, bool shrink=false) override;

    // Overrides pure Menu_impl.
    void prepend(Widget_ptr wp, bool shrink=false) override;

    // Overrides pure Menu_impl.
    void insert_before(Widget_ptr wp, const Widget_impl * other, bool shrink=false) override;

    // Overrides pure Menu_impl.
    void insert_after(Widget_ptr wp, const Widget_impl * other, bool shrink=false) override;

    // Overrides pure Menu_impl.
    bool empty() const noexcept override { return list_->empty(); }

    // Overrides Menu_impl.
    void remove(Widget_impl * wp) override;

    // Overrides Menu_impl.
    void clear() override;

    // Overrides pure Menu_impl.
    void child_menu_cancel() override;

    // Overrides pure Menu_impl.
    void child_menu_left() override;

    // Overrides pure Menu_impl.
    void child_menu_right() override;

    Window_ptr popup(Widget_ptr self, Menu_impl * pmenu=nullptr);
    Window_ptr popup(Window_impl * parent_window, Widget_ptr self, Menu_impl * pmenu=nullptr);
    Window_ptr popup(Window_impl * parent_window, Widget_ptr self, Gravity gravity, Menu_impl * pmenu=nullptr);
    Window_ptr popup(Window_impl * parent_window, Widget_ptr self, const Point & position, Menu_impl * pmenu=nullptr);
    Window_ptr popup(Window_impl * parent_window, Widget_ptr self, const Point & position, Gravity gravity, Menu_impl * pmenu=nullptr);

    Container_impl * compound() noexcept override { return list_; };
    const Container_impl * compound() const noexcept override { return list_; };

protected:

    // Overrides pure Menu_impl.
    void mark_item(Widget_impl * ip, bool select) override;

private:

    List_impl * list_;
    Gravity     gravity_ = Gravity::NONE;
    Action      action_previous_ { "Up", fun(this, &Menubox_impl::select_prev) };
    Action      action_next_ { "Down", fun(this, &Menubox_impl::select_next) };
    Action      action_left_ { "Left", fun(this, &Menubox_impl::on_left) };
    Action      action_right_ { "Right", fun(this, &Menubox_impl::on_right) };

private:

    void arrange();
    void put_widget(Widget_ptr wp, int row);
    void on_left();
    void on_right();
};

} // namespace tau

#endif // __TAU_MENUBOX_IMPL_HH__

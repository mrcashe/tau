// ----------------------------------------------------------------------------
// SPDX-License-Identifier: BSD-2-Clause
// Copyright © 2014-2024 Konstantin Shmelkov <mrcashe@gmail.com>.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
// THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
// PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
// CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
// EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
// PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
// OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
// WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
// OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
// EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// ----------------------------------------------------------------------------

#ifndef __TAU_PIXMAP_IMPL_HH__
#define __TAU_PIXMAP_IMPL_HH__

#include <tau/painter.hh>
#include <tau/pixmap.hh>
#include <tau/string.hh>

namespace tau {

void pixmap_xpm_init();
void pixmap_ico_init();
void pixmap_bmp_init();
void pixmap_png_init();
Pixmap_ptr load_bmp_from_memory(const char * mem, std::size_t bytes);

class Pixmap_impl {
protected:

    Pixmap_impl() = default;

public:

    Pixmap_impl(const Pixmap_impl & other) = delete;
    Pixmap_impl & operator=(const Pixmap_impl & other) = delete;
    virtual ~Pixmap_impl() { signal_destroy_(); }

    Vector ppi() const { return ppi_; }
    void set_ppi(const Vector & value) { ppi_ = value; }

    void copy(const Pixmap_impl * other);

    // This method is platform depended.
    static Pixmap_ptr create(int depth, const Size & size=Size());

    static Pixmap_ptr create(int depth, unsigned width, unsigned height) {
        return create(depth, Size(width, height));
    }

    static Pixmap_ptr create(int depth, Pixmap_cptr other) {
        Pixmap_ptr pix = create(depth, other->size());
        pix->copy(other.get());
        return pix;
    }

    static Pixmap_ptr create(const uint8_t * raw, std::size_t nbytes);
    static Pixmap_ptr load_from_file(const ustring & path);

    Pixmap_ptr dup() const;

    // Overridden by Pixmap_xcb.
    // Overridden by Pixmap_win.
    virtual void fill_rectangles(const Rect * rs, std::size_t nrs, const Color & c) = 0;

    // Overridden by Pixmap_xcb.
    // Overridden by Pixmap_win.
    virtual Painter painter() = 0;

    // Overridden by Pixmap_xcb.
    // Overridden by Pixmap_win.
    virtual Size size() const = 0;

    // Test if empty.
    bool empty() const noexcept { return size().empty(); }

    // Overridden by Pixmap_xcb.
    // Overridden by Pixmap_win.
    virtual int depth() const = 0;

    // Overridden by Pixmap_xcb.
    // Overridden by Pixmap_win.
    virtual std::size_t bytes() const = 0;

    // Overridden by Pixmap_xcb.
    // Overridden by Pixmap_win.
    virtual const uint8_t * raw() const = 0;

    // Overridden by Pixmap_xcb.
    // Overridden by Pixmap_win.
    virtual void resize(const Size & sz) = 0;

    // Overridden by Pixmap_xcb.
    // Overridden by Pixmap_win.
    virtual void put_pixel(const Point & pt, const Color & c) = 0;

    // Overridden by Pixmap_xcb.
    // Overridden by Pixmap_win.
    virtual Color get_pixel(const Point & pt) const = 0;

    // Overridden by Pixmap_xcb.
    // Overridden by Pixmap_win.
    virtual void set_argb32(const Point & pt, const uint8_t * buffer, std::size_t nbytes) = 0;

    signal<void()> & signal_destroy() { return signal_destroy_; }
    signal<void()> & signal_changed() { return signal_changed_; }

    static signal<bool(const ustring & path)> & signal_file_supported();
    static signal<Pixmap_ptr (const uint8_t * raw, std::size_t nbytes)> & signal_load_from_memory();
    static signal<Pixmap_ptr (const ustring & path)> & signal_load_from_file();

protected:

    Vector       ppi_ { 72.0, 72.0 };
    signal<void()> signal_changed_;
    signal<void()> signal_destroy_;
};

} // namespace tau

#endif // __TAU_PIXMAP_IMPL_HH__

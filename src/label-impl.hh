// ----------------------------------------------------------------------------
// SPDX-License-Identifier: BSD-2-Clause
// Copyright © 2014-2024 Konstantin Shmelkov <mrcashe@gmail.com>.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
// THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
// PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
// CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
// EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
// PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
// OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
// WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
// OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
// EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// ----------------------------------------------------------------------------

/// @file label-impl.hh The Label class declaration (Label_impl).

#ifndef __TAU_LABEL_IMPL_HH__
#define __TAU_LABEL_IMPL_HH__

#include <tau/buffer.hh>
#include <tau/font.hh>
#include <tau/painter.hh>
#include <widget-impl.hh>

namespace tau {

class Label_impl: public Widget_impl {
public:

    Label_impl();
    explicit Label_impl(Align xalign, Align yalign=Align::CENTER);
    explicit Label_impl(const ustring & s, Align xalign=Align::CENTER, Align yalign=Align::CENTER);
    explicit Label_impl(const std::u32string & ws, Align xalign=Align::CENTER, Align yalign=Align::CENTER);
    explicit Label_impl(Buffer buf, Align xalign=Align::CENTER, Align yalign=Align::CENTER);

    void assign(const std::u32string & ws);
    void assign(const ustring & s) { assign(std::u32string(s)); }

    // Overridden by Edit_impl.
    virtual void clear();

    bool empty() const noexcept { return buffer_.empty(); }
    ustring str() const { return buffer_.str(); }
    std::u32string wstr() const { return buffer_.wstr(); }
    Rect row_bounds(std::size_t ri) const noexcept;
    void set_buffer(Buffer buf);
    Buffer buffer() { return buffer_; }
    const Buffer & buffer() const { return buffer_; }
    std::size_t rows() const noexcept { return buffer_.rows(); }
    void wrap(Wrap wrap);
    Wrap wrap() const noexcept { return wrap_; }
    void set_spacing(unsigned spc);
    unsigned spacing() const noexcept { return spacing_; }
    void text_align(Align xalign, Align yalign);
    std::pair<Align, Align> text_align() const { return { xalign_, yalign_ }; }
    Size text_size(const ustring & s);
    Size text_size(const std::u32string & s);
    int x_at_col(std::size_t ri, std::size_t col) const noexcept { return ri < rows_.size() ? x_at_col(rows_[ri], col) : 0; }
    std::size_t col_at_x(std::size_t ri, int x) const noexcept { return ri < rows_.size() ? col_at_x(rows_[ri], x) :0; }
    std::size_t row_at_y(int y) const noexcept;
    int baseline(std::size_t ri) const noexcept { return ri < rows_.size() ? rows_[ri].ybase_ : 0; }
    Buffer_citer iter(std::size_t row, std::size_t col) const { return buffer_.citer(row, col); }
    Buffer_citer iter(const Point & pt) const;

protected:

    // Character offsets (in pixels).
    using Offsets  = std::vector<int>;

    // Character indice.
    using Indice   = std::vector<std::size_t>;

    // Row structure.
    struct Row {
        std::size_t     cmax_       = 0;            // Number of columns (characters).
        unsigned        width_      = 0;            // Width in pixels.
        int             ascent_     = 0;            // Ascent in pixels.
        int             descent_    = 0;            // Descent in pixels.
        int             ybase_      = 0;            // Baseline within entire widget area.
        int             ox_         = 0;            // Offset within X coordinate.
        unsigned        e1_         = 0;            // Ellipsized first part size in chars.
        unsigned        e2_         = 0;            // Ellipsized last part position in chars.
        Offsets         ofs_;                       // Character offsets.
        Indice          blanks_;                    // Where blanks.
    };

    using Rows = std::vector<Row>;
    using Iter = Rows::iterator;
    using Citer = Rows::const_iterator;

    Rows                rows_;
    Buffer              buffer_;
    Buffer_citer        sel_;           // Selection start.
    Buffer_citer        esel_;          // Selection end.
    Align               xalign_         = Align::CENTER;
    Align               yalign_         = Align::CENTER;
    Wrap                wrap_           = Wrap::NONE;
    Size                xz_;            // Extra requisition.
    unsigned            text_width_     = 0;
    unsigned            text_height_    = 0;
    unsigned            font_height_    = 0;
    int                 oy_ = 0;                    // Vertical offset.
    Rect                vport_;                     // Paper area.
    bool                wrapped_: 1     = false;    // Has wrapped rows.
    Conf::Item          font_item_      { Conf::FONT };
    connection          insert2_cx_     { true };

protected:

    void update_requisition();

    // Overridden by Text_impl.
    // Overridden by Edit_impl.
    virtual void init_buffer();

    // Overridden by Text_impl.
    virtual void on_wrap();

    // Overridden by Text_impl.
    virtual void render(Painter pr, const Rect & r) { render_pro(pr, r); }

    // Overridden by Text_impl.
    virtual void on_buffer_insert(Buffer_citer b, Buffer_citer e);

    // Overridden by Text_impl.
    virtual void on_buffer_replace(Buffer_citer b, Buffer_citer e, const std::u32string & replaced);

    // Overridden by Text_impl.
    virtual void on_buffer_erase(Buffer_citer b, Buffer_citer e, const std::u32string & erased);

    // Overridden by Edit_impl.
    virtual Painter rnd_painter() { return painter(); }

    void change_font_style(Conf::Item fi);
    void update_range(Buffer_citer begin, Buffer_citer end);
    int x_at_col(const Row & row, std::size_t col) const;
    std::size_t col_at_x(const Row & row, int x) const;
    Iter at_y(int y) noexcept;
    Citer at_y(int y) const noexcept;
    void render_pro(Painter pr, const Rect & r);

private:

    unsigned            spacing_        { 0 };
    std::vector<Font>   fonts_          { 1 };
    int                 wspace_         { 0 };

    int                 tab_width_      { 8 };
    std::u32string      ellipsis_       { U"…" };

    connection          insert_cx_      { true };
    connection          replace_cx_     { true };
    connection          erase_cx_       { true };

private:

    void init();
    Font select_font(Painter pr);
    void update_font();
    void update_vport();
    int  calc_height(Citer first, Citer last);
    int  calc_width(Citer first, Citer last);
    void calc_fill(Iter i);
    void calc_ellipsis(Iter i, Painter pr);
    void calc_single_run(Iter i, Painter pr);
    void calc(Iter i, Painter pr);
    void calc_all();
    bool align(Iter first, Iter last);
    void align_all();
    void translate_rows(Iter first, Iter last, int dy);
    void render(Painter pr, Citer ri, std::size_t pos);
    void render_ellipsized(Painter pr, Citer ri);
    Painter wipe_area(int x1, int y1, int x2, int y2, Painter pr=Painter());
    void wipe_all();
    void clear_i();

    void on_font_changed();
    void on_size_changed();
    bool on_paint(Painter pr, const Rect & inval);
    void on_display_in();
    void on_focus_in();
    void on_focus_out();
    void on_enable();
    void on_disable();
};

} // namespace tau

#endif // __TAU_LABEL_IMPL_HH__

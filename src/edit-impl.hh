// ----------------------------------------------------------------------------
// SPDX-License-Identifier: BSD-2-Clause
// Copyright © 2014-2024 Konstantin Shmelkov <mrcashe@gmail.com>.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
// THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
// PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
// CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
// EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
// PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
// OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
// WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
// OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
// EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// ----------------------------------------------------------------------------

#ifndef __TAU_EDIT_IMPL_HH__
#define __TAU_EDIT_IMPL_HH__

#include <text-impl.hh>

namespace tau {

class Edit_impl: public Text_impl {
public:

    Edit_impl();
    explicit Edit_impl(Align halign, Align valign=Align::START);
    explicit Edit_impl(const ustring & s, Align halign=Align::START, Align valign=Align::START);
    explicit Edit_impl(Buffer buf, Align halign=Align::START, Align valign=Align::START);

    // Overrides Text_impl.
    void clear() override;

    void allow_edit();
    void disallow_edit();
    bool editable() const { return editable_; }
    void enter_text(const ustring & str);
    bool modified() const { return undo_index_ != flush_index_; }

    Action & action_enter() { return action_enter_; }
    Action & action_cut() { return action_cut_; }
    Action & action_paste() { return action_paste_; }
    Action & action_delete() { return action_delete_; }
    Action & action_backspace() { return action_backspace_; }
    Action & action_undo() { return action_undo_; }
    Action & action_redo() { return action_redo_; }
    Action & action_tab() { return action_tab_; }
    Toggle_action & action_insert() { return action_insert_; }

    signal<void(bool)> & signal_modified() { return signal_modified_; }
    signal<void()> & signal_changed() { return buffer_.signal_changed(); }

protected:

    // Overrides Text_impl.
    void init_buffer() override;

    // Overriddes pure Text_renderer_impl.
    // Overrides Label_impl.
    Painter rnd_painter() override;

private:

    enum Undo_type { UNDO_INSERT, UNDO_REPLACE, UNDO_ERASE };

    struct Undo {
        Undo_type       type;
        std::size_t     row1;
        std::size_t     col1;
        std::size_t     row2;
        std::size_t     col2;
        std::u32string  str1;
        std::u32string  str2;
    };

    using Undoes = std::vector<Undo>;

    Undoes              undo_;
    std::size_t         undo_index_         = 0;
    std::size_t         flush_index_        = 0;
    ustring             newline_            = "\u000a";
    bool                editable_           = true;
    bool                split_undo_         = false;
    Painter             ppr_;               // Private painter.

    connection          edit_replace_cx_    { true };
    connection          edit_erase_cx_      { true };
    connection          flush_cx_           { true };
    connection          clipboard_cx_       { true };
    signal<void(bool)>  signal_modified_;

    Action              action_enter_       { KC_ENTER, KM_NONE, fun(this, &Edit_impl::enter) };
    Action              action_backspace_   { KC_BACKSPACE, KM_NONE, fun(this, &Edit_impl::backspace) };
    Action              action_cut_         { "<Ctrl>X <Shift>Delete", fun(this, &Edit_impl::cut) };
    Action              action_paste_       { "<Ctrl>V <Shift>Insert", fun(this, &Edit_impl::paste) };
    Action              action_delete_      { KC_DELETE, KM_NONE, fun(this, &Edit_impl::del) };
    Action              action_undo_        { "<Alt>BackSpace <Ctrl>Z", fun(this, &Edit_impl::undo) };
    Action              action_redo_        { "<Alt>Enter", fun(this, &Edit_impl::redo) };
    Action              action_tab_         { KC_TAB, KM_NONE, fun(this, &Edit_impl::tab) };
    Toggle_action       action_insert_      { KC_INSERT, KM_NONE, fun(this, &Edit_impl::on_insert) };

private:

    void init();
    void cutoff_redo();
    void clear_undo();

    void backspace();
    void enter();
    void tab();
    void cut();
    void paste();
    void undo();
    void redo();

    void del();
    void del_char();
    void del_selection();
    void del_range(Buffer_citer b, Buffer_citer e);

    bool on_key_down(char32_t kc, uint32_t km);
    bool on_input(const ustring & s, int src);
    void on_insert(bool replace);
    void on_edit_insert(Buffer_citer b, Buffer_citer e);
    void on_edit_replace(Buffer_citer b, Buffer_citer e, const std::u32string & replaced);
    void on_edit_erase(Buffer_citer b, Buffer_citer e, const std::u32string & erased);
    void on_flush();
    void on_selection_changed();
    void on_display_in();
    void on_clipboard_changed();
};

} // namespace tau

#endif // __TAU_EDIT_IMPL_HH__

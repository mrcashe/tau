// ----------------------------------------------------------------------------
// SPDX-License-Identifier: BSD-2-Clause
// Copyright © 2014-2024 Konstantin Shmelkov <mrcashe@gmail.com>.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
// THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
// PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
// CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
// EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
// PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
// OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
// WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
// OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
// EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// ----------------------------------------------------------------------------

#include <tau/list.hh>
#include <list-impl.hh>

#define LIST_IMPL (std::static_pointer_cast<List_impl>(impl))

namespace tau {

List::List():
    List_base(std::make_shared<List_impl>())
{
}

List::List(unsigned spacing):
    List_base(std::make_shared<List_impl>(spacing))
{
}

List::List(unsigned xspacing, unsigned yspacing):
    List_base(std::make_shared<List_impl>(xspacing, yspacing))
{
}

List::List(const List & other):
    List_base(other.impl)
{
}

List & List::operator=(const List & other) {
    List_base::operator=(other);
    return *this;
}

List::List(List && other):
    List_base(other.impl)
{
}

List & List::operator=(List && other) {
    List_base::operator=(other);
    return *this;
}

List::List(Widget_ptr wp):
    List_base(std::dynamic_pointer_cast<List_impl>(wp))
{
}

List & List::operator=(Widget_ptr wp) {
    List_base::operator=(std::dynamic_pointer_cast<List_impl>(wp));
    return *this;
}

int List::prepend_row(Widget & w, bool shrink) {
    return LIST_IMPL->prepend_row(w.ptr(), shrink);
}

int List::prepend_row(Widget & w, Align align, bool shrink) {
    return LIST_IMPL->prepend_row(w.ptr(), align, shrink);
}

int List::insert_row(Widget & w, int position, bool shrink) {
    return LIST_IMPL->insert_row(w.ptr(), position, shrink);
}

int List::insert_row(Widget & w, int position, Align align, bool shrink) {
    return LIST_IMPL->insert_row(w.ptr(), position, align, shrink);
}

int List::append_row(Widget & w, bool shrink) {
    return LIST_IMPL->append_row(w.ptr(), shrink);
}

int List::append_row(Widget & w, Align align, bool shrink) {
    return LIST_IMPL->append_row(w.ptr(), align, shrink);
}

int List::prepend(Widget & w, bool shrink) {
    return LIST_IMPL->prepend(w.ptr(), shrink);
}

int List::prepend(Widget & w, Align align, bool shrink) {
    return LIST_IMPL->prepend(w.ptr(), align, shrink);
}

int List::insert(Widget & w, int position, bool shrink) {
    return LIST_IMPL->insert(w.ptr(), position, shrink);
}

int List::insert(Widget & w, int position, Align align, bool shrink) {
    return LIST_IMPL->insert(w.ptr(), position, align, shrink);
}

int List::append(Widget & w, bool shrink) {
    return LIST_IMPL->append(w.ptr(), shrink);
}

int List::append(Widget & w, Align align, bool shrink) {
    return LIST_IMPL->append(w.ptr(), align, shrink);
}

int List::prepend(int branch, Widget & w, bool shrink) {
    return LIST_IMPL->prepend(branch, w.ptr(), shrink);
}

int List::prepend(int branch, Widget & w, Align align, bool shrink) {
    return LIST_IMPL->prepend(branch, w.ptr(), align, shrink);
}

int List::insert(int branch, Widget & w, int position, bool shrink) {
    return LIST_IMPL->insert(branch, w.ptr(), position, shrink);
}

int List::insert(int branch, Widget & w, int position, Align align, bool shrink) {
    return LIST_IMPL->insert(branch, w.ptr(), position, align, shrink);
}

int List::append(int branch, Widget & w, bool shrink) {
    return LIST_IMPL->append(branch, w.ptr(), shrink);
}

int List::append(int branch, Widget & w, Align align, bool shrink) {
    return LIST_IMPL->append(branch, w.ptr(), align, shrink);
}

std::size_t List::remove(int row) {
    return LIST_IMPL->remove(row);
}

std::size_t List::remove(Widget & w) {
    return LIST_IMPL->remove(w.ptr().get());
}

} // namespace tau

//END

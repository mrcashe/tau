// ----------------------------------------------------------------------------
// SPDX-License-Identifier: BSD-2-Clause
// Copyright © 2014-2024 Konstantin Shmelkov <mrcashe@gmail.com>.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
// THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
// PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
// CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
// EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
// PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
// OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
// WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
// OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
// EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// ----------------------------------------------------------------------------

#include <tau/language.hh>
#include <tau/string.hh>
#include <locale-impl.hh>
#include <gettext-impl.hh>
#include <cstring>

namespace tau {

struct Language_data {
    const char * code;
    const char * ename;
    const char * scr;
    const char * name;
    const char * sample;
};

Language_data datas[] = {
    {
        .code       = "C",
        .ename      = "C",
        .scr        = "Latn",
        .name       = "C",
        .sample     = "The quick brown fox jumps over the lazy dog."
    },
    {
        .code       = "aa:aar:aars",
        .ename      = "Afar",
        .scr        = "Latn",
        .name       = gettext_noop("Afaraf")
    },
    {
        .code       = "ab:abk:abks",
        .ename      = "Abkhaz",
        .scr        = "Cyrl",
        .name       = gettext_noop("аҧсуа бызшәа, аҧсшәа")
    },
    {
        .code       = "af:afr:afrs",
        .ename      = "Afrikaans",
        .scr        = "Latn",
        .name       = gettext_noop("Afrikaans"),
        .sample     = "Ek kan glas eet, maar dit doen my nie skade nie."
    },
    {
        .code       = "ak:aka",
        .ename      = "Akan",
        .scr        = "Latn",
        .name       = gettext_noop("Akan")
    },
    {
        .code       = "am:amh",
        .ename      = "Amharic",
        .scr        = "Ethi",
        .name       = gettext_noop("አማርኛ")
    },
    {
        .code       = "an:arg",
        .ename      = "Aragonese",
        .scr        = "Latn",
        .name       = gettext_noop("aragonés")
    },
    {
        .code       = "ar:ara",
        .ename      = "Arabic",
        .scr        = "Arab",
        .name       = gettext_noop("العربية"),
        .sample     = "نص حكيم له سر قاطع وذو شأن عظيم مكتوب على ثوب أخضر ومغلف بجلد أزرق."
    },
    {
        .code       = "as:asm",
        .ename      = "Assamese",
        .scr        = "Beng",
        .name       = gettext_noop("অসমীয়া")
    },
    {
        .code       = "av:ava",
        .ename      = "Avaric",
        .scr        = "Cyrl",
        .name       = gettext_noop("авар мацӀ, магӀарул мацӀ")
    },
    {
        .code       = "ay:aym",
        .ename      = "Aymara",
        .scr        = "Latn",
        .name       = gettext_noop("aymar aru")
    },
    {
        .code       = "azj:aze:az:az-az",
        .ename      = "North Azerbaijani",
        .scr        = "Latn",
        .name       = gettext_noop("azərbaycan dili"),
        .sample     = "Zəfər, jaketini də papağını da götür, bu axşam hava çox soyuq olacaq."
    },
    {
        .code       = "azb:az-ir",
        .ename      = "South Azerbaijani",
        .scr        = "Arab"
    },
    {
        .code       = "ba:bak",
        .ename      = "Bashkir",
        .scr        = "Cyrl",
        .name       = gettext_noop("башҡорт теле")
    },
    {
        .code       = "be:bel",
        .ename      = "Belarusian",
        .scr        = "Cyrl",
        .name       = gettext_noop("беларуская мова")
    },
    {
        .code       = "bg:bul:buls",
        .ename      = "Bulgarian",
        .scr        = "Cyrl",
        .name       = gettext_noop("български език"),
        .sample     = "За миг бях в чужд плюшен скърцащ фотьойл."
    },
    {
        .code       = "bh:bih",
        .ename      = "Bihari",
        .scr        = "Deva",
        .name       = gettext_noop("भोजपुरी")
    },
    {
        .code       = "bho",
        .ename      = "Bhojpuri",
        .scr        = "Deva",
        .name       = gettext_noop("भोजपुरी")
    },
    {
        .code       = "bi:bis",
        .ename      = "Bislama",
        .scr        = "Latn",
        .name       = gettext_noop("Bislama")
    },
    {
        .code       = "bin",
        .ename      = "Edo, Bini",
        .scr        = "Latn"
    },
    {
        .code       = "bm:bam",
        .ename      = "Bambara",
        .scr        = "Latn",
        .name       = gettext_noop("bamanankan")
    },
    {
        .code       = "bn:ben",
        .ename      = "Bengali, Bangla",
        .scr        = "Beng",
        .name       = gettext_noop("বাংলা")
    },
    {
        .code       = "bo:bod:tib",
        .ename      = "Tibetian Standard",
        .scr        = "Tibt",
        .name       = gettext_noop("བོད་ཡིག")
    },
    {
        .code       = "br:bre",
        .ename      = "Breton",
        .scr        = "Latn",
        .name       = gettext_noop("brezhoneg")
    },
    {
        .code       = "brx:sit",
        .ename      = "Bodo",
        .scr        = "Deva",
        .name       = gettext_noop("बोड़ो")
    },
    {
        .code       = "bs:bos",
        .ename      = "Bosnian",
        .scr        = "Latn",
        .name       = gettext_noop("bosanski jezik"),
        .sample     = "Fin džip, gluh jež i čvrst konjić dođoše bez moljca."
    },
    {
        .code       = "bua",
        .ename      = "Buryat",
        .scr        = "Cyrl",
        .name       = gettext_noop("Буряад хэлэн")
    },
    {
        .code       = "byn",
        .ename      = "Bilen",
        .scr        = "Ethi",
        .name       = gettext_noop("ብሊና")
    },
    {
        .code       = "ca:cat",
        .ename      = "Catalan",
        .scr        = "Latn",
        .name       = gettext_noop("català"),
        .sample     = "Jove xef, porti whisky amb quinze glaçons d'hidrogen, coi!"
    },
    {
        .code       = "ce:che",
        .ename      = "Chechen",
        .scr        = "Cyrl",
        .name       = gettext_noop("Нохчийн мотт")
    },
    {
        .code       = "ch:cha",
        .ename      = "Chamorro",
        .scr        = "Latn",
        .name       = gettext_noop("Finu' Chamorro")
    },
    {
        .code       = "chm",
        .ename      = "Mari",
        .scr        = "Cyrl",
        .name       = gettext_noop("Марий йылме")
    },
    {
        .code       = "chr",
        .ename      = "Cherokee",
        .scr        = "Cher",
        .name       = gettext_noop(" ᏣᎳᎩ")
    },
    {
        .code       = "ckb",
        .ename      = "Central Kurdish",
        .scr        = "Arab",
        .name       = gettext_noop("سۆرانی، کوردیی ناوەندی")
    },
    {
        .code       = "co:cos",
        .ename      = "Corsican",
        .scr        = "Latn",
        .name       = gettext_noop("Corsu")
    },
    {
        .code       = "crh",
        .ename      = "Crimean Tatar",
        .scr        = "Latn",
        .name       = gettext_noop("Qırımtatar")
    },
    {
        .code       = "cs:ces:cze",
        .ename      = "Czech",
        .scr        = "Latn",
        .name       = gettext_noop("čeština"),
        .sample     = "Příliš žluťoučký kůň úpěl ďábelské ódy."
    },
    {
        .code       = "csb",
        .ename      = "Kashubian",
        .scr        = "Latn",
        .name       = gettext_noop("Kaszëbsczi")
    },
    {
        .code       = "cu:chu",
        .ename      = "Church Slavonic",
        .scr        = "Cyrl",
        .name       = gettext_noop("ѩзыкъ словѣньскъ")
    },
    {
        .code       = "cv:chv",
        .ename      = "Chuvash",
        .scr        = "Cyrl:Latn",
        .name       = gettext_noop("чӑваш чӗлхи")
    },
    {
        .code       = "cy:cym:wel",
        .ename      = "Welsh",
        .scr        = "Latn",
        .name       = gettext_noop("Cymraeg")
    },
    {
        .code       = "da:dan",
        .ename      = "Danish",
        .scr        = "Latn",
        .name       = gettext_noop("dansk"),
        .sample     = "Quizdeltagerne spiste jordbær med fløde, mens cirkusklovnen Walther spillede på xylofon."
    },
    {
        .code       = "de:deu:ger",
        .ename      = "German",
        .scr        = "Latn",
        .name       = gettext_noop("Deutsch"),
        .sample     = "Zwölf Boxkämpfer jagen Viktor quer über den großen Sylter Deich."
    },
    {
        .code       = "doi",
        .ename      = "Dogri",
        .scr        = "Deva",
        .name       = gettext_noop("डोगरी")
    },
    {
        .code       = "dv:div",
        .ename      = "Divehi",
        .scr        = "Thaa",
        .name       = gettext_noop("ދިވެހި")
    },
    {
        .code       = "dz:dzo",
        .ename      = "Dzongkha",
        .scr        = "Tibt",
        .name       = gettext_noop("རྫོང་ཁ")
    },
    {
        .code       = "ee:ewe",
        .ename      = "Ewe",
        .scr        = "Latn",
        .name       = gettext_noop("Eʋegbe")
    },
    {
        .code       = "el:ell:gre",
        .ename      = "Greek",
        .scr        = "Grek",
        .name       = gettext_noop("ελληνικά"),
        .sample     = "Θέλει αρετή και τόλμη η ελευθερία. (Ανδρέας Κάλβος)"
    },
    {
        .code       = "en:eng",
        .ename      = "English",
        .scr        = "Latn",
        .name       = gettext_noop("English"),
        .sample     = "The quick brown fox jumps over the lazy dog."
    },
    {
        .code       = "eo:epo",
        .ename      = "Esperanto",
        .scr        = "Latn",
        .name       = gettext_noop("Esperanto"),
        .sample     = "Eĥoŝanĝo ĉiuĵaŭde."
    },
    {
        .code       = "es:spa",
        .ename      = "Spanish",
        .scr        = "Latn",
        .name       = gettext_noop("Español"),
        .sample     = "Jovencillo emponzoñado de whisky: ¡qué figurota exhibe!"
    },
    {
        .code       = "et:est",
        .ename      = "Estonian",
        .scr        = "Latn",
        .name       = gettext_noop("Eesti"),
        .sample     = "See väike mölder jõuab rongile hüpata."
    },
    {
        .code       = "eu:eus:baq",
        .ename      = "Basque",
        .scr        = "Latn",
        .name       = gettext_noop("euskara"),
        .sample     = "Kristala jan dezaket, ez dit minik ematen."
    },
    {
        .code       = "fa:fas:per",
        .ename      = "Persian",
        .scr        = "Arab",
        .name       = gettext_noop("فارسی"),
        .sample     = "«الا یا اَیُّها السّاقی! اَدِرْ کَأساً وَ ناوِلْها!» که عشق آسان نمود اوّل، ولی افتاد مشکل‌ها!"
    },
    {
        .code       = "fat",
        .ename      = "Fante",
        .scr        = "Latn",
        .name       = gettext_noop("Fante")
    },
    {
        .code       = "ff:ful",
        .ename      = "Fulah",
        .scr        = "Latn",
        .name       = gettext_noop("Fulfulde")
    },
    {
        .code       = "fi:fin",
        .ename      = "Finnish",
        .scr        = "Latn",
        .name       = gettext_noop("Suomi"),
        .sample     = "Viekas kettu punaturkki laiskan koiran takaa kurkki."
    },
    {
        .code       = "fil",
        .ename      = "Filipino",
        .scr        = "Latn",
        .name       = gettext_noop("Wikang Filipino")
    },
    {
        .code       = "fj:fij",
        .ename      = "Fijian",
        .scr        = "Latn",
        .name       = gettext_noop("vosa Vakaviti")
    },
    {
        .code       = "fo:fao",
        .ename      = "Faroese",
        .scr        = "Latn",
        .name       = gettext_noop("føroyskt")
    },
    {
        .code       = "fr:fra:fre",
        .ename      = "French",
        .scr        = "Latn",
        .name       = gettext_noop("Français"),
        .sample     = "Voix ambiguë d'un cœur qui, au zéphyr, préfère les jattes de kiwis."
    },
    {
        .code       = "fur",
        .ename      = "Friulian",
        .scr        = "Latn",
        .name       = gettext_noop("Furlane")
    },
    {
        .code       = "fy:fry",
        .ename      = "Western Frisian",
        .scr        = "Latn",
        .name       = gettext_noop("Frysk")
    },
    {
        .code       = "ga:gle",
        .ename      = "Irish",
        .scr        = "Latn",
        .name       = gettext_noop("Gaeilge"),
        .sample     = "D'ḟuascail Íosa Úrṁac na hÓiġe Beannaiṫe pór Éaḃa agus Áḋaiṁ."
    },
    {
        .code       = "gd:gla",
        .ename      = "Gaelic",
        .scr        = "Latn",
        .name       = gettext_noop("Gàidhlig"),
        .sample     = "S urrainn dhomh gloinne ithe; cha ghoirtich i mi."
    },
    {
        .code       = "gez",
        .ename      = "Ge'ez",
        .scr        = "Ethi",
        .name       = gettext_noop("ግዕዝ")
    },
    {
        .code       = "gl:glg",
        .ename      = "Galician",
        .scr        = "Latn",
        .name       = gettext_noop("Gallego"),
        .sample     = "Eu podo xantar cristais e non cortarme."
    },
    {
        .code       = "gn:grn",
        .ename      = "Guaraní",
        .scr        = "Latn",
        .name       = gettext_noop("Avañe'ẽ")
    },
    {
        .code       = "gu:guj",
        .ename      = "Gujarati",
        .scr        = "Gujr",
        .name       = gettext_noop("ગુજરાતી"),
        .sample     = "હું કાચ ખાઇ શકુ છુ અને તેનાથી મને દર્દ નથી થતુ."
    },
    {
        .code       = "gv:glv",
        .ename      = "Manx",
        .scr        = "Latn",
        .name       = gettext_noop("Gaelg"),
        .sample     = "Foddym gee glonney agh cha jean eh gortaghey mee."
    },
    {
        .code       = "ha:hau",
        .ename      = "Hausa",
        .scr        = "Latn",
        .name       = gettext_noop("هَوُسَ")
    },
    {
        .code       = "haw",
        .ename      = "Hawaiian",
        .scr        = "Latn",
        .name       = gettext_noop("Ōlelo Hawaiʻi"),
        .sample     = "Hiki iaʻu ke ʻai i ke aniani; ʻaʻole nō lā au e ʻeha."
    },
    {
        .code       = "he:heb",
        .ename      = "Hebrew",
        .scr        = "Hebr",
        .name       = gettext_noop("עברית"),
        .sample     = "דג סקרן שט לו בים זך אך לפתע פגש חבורה נחמדה שצצה כך."
    },
    {
        .code       = "hi:hin",
        .ename      = "Hindi",
        .scr        = "Deva",
        .name       = gettext_noop("हिन्दी"),
        .sample     = "नहीं नजर किसी की बुरी नहीं किसी का मुँह काला जो करे सो उपर वाला"
    },
    {
        .code       = "hne",
        .ename      = "Chhattisgarhi",
        .scr        = "Deva",
        .name       = gettext_noop("छत्तीसगढ़ी")
    },
    {
        .code       = "ho:hmo",
        .ename      = "Hiri Motu",
        .scr        = "Latn",
        .name       = gettext_noop("Hiri Motu")
    },
    {
        .code       = "hr:hrv",
        .ename      = "Croatian",
        .scr        = "Latn",
        .name       = gettext_noop("Hrvatski"),
        .sample     = "Ja mogu jesti staklo i ne boli me."
    },
    {
        .code       = "hsb",
        .ename      = "Upper Sorbian",
        .scr        = "Latn",
        .name       = gettext_noop("Hornjoserbšćina")
    },
    {
        .code       = "ht:hat",
        .ename      = "Haitian",
        .scr        = "Latn",
        .name       = gettext_noop("Kreyòl ayisyen")
    },
    {
        .code       = "hu:hun",
        .ename      = "Hungarian",
        .scr        = "Latn",
        .name       = gettext_noop("Magyar"),
        .sample     = "Egy hűtlen vejét fülöncsípő, dühös mexikói úr Wesselényinél mázol Quitóban."
    },
    {
        .code       = "hy:hye",
        .ename      = "Armenian",
        .scr        = "Armn",
        .name       = gettext_noop("Հայերեն"),
        .sample     = "Կրնամ ապակի ուտել և ինծի անհանգիստ չըներ։"
    },
    {
        .code       = "hz:her",
        .ename      = "Herero",
        .scr        = "Latn",
        .name       = gettext_noop("Otjiherero")
    },
    {
        .code       = "ia:ina",
        .ename      = "Interlingua",
        .scr        = "Latn",
        .name       = gettext_noop("Interlingua")
    },
    {
        .code       = "id:ind",
        .ename      = "Indonesian",
        .scr        = "Latn",
        .name       = gettext_noop("Bahasa Indonesia")
    },
    {
        .code       = "ie:ile",
        .ename      = "Interlingue",
        .scr        = "Latn",
        .name       = gettext_noop("Interlingue")
    },
    {
        .code       = "ig:ibo",
        .ename      = "Igbo",
        .scr        = "Latn",
        .name       = gettext_noop("Asụsụ Igbo")
    },
    {
        .code       = "ii:iii",
        .ename      = "Sichuan Yi",
        .scr        = "Yiii",
        .name       = gettext_noop("ꆈꌠꉙ")
    },
    {
        .code       = "ik:ipk",
        .ename      = "Inupiaq",
        .scr        = "Cyrl",
        .name       = gettext_noop("Iñupiaq")
    },
    {
        .code       = "io:ido",
        .ename      = "Ido",
        .scr        = "Latn",
        .name       = gettext_noop("Ido")
    },
    {
        .code       = "is:isl:ice",
        .ename      = "Icelandic",
        .scr        = "Latn",
        .name       = gettext_noop("Íslenska"),
        .sample     = "Kæmi ný öxi hér ykist þjófum nú bæði víl og ádrepa"
    },
    {
        .code       = "it:ita",
        .ename      = "Italian",
        .scr        = "Latn",
        .name       = gettext_noop("Italiano"),
        .sample     = "Ma la volpe, col suo balzo, ha raggiunto il quieto Fido."
    },
    {
        .code       = "iu:iku",
        .ename      = "Inuktitut",
        .scr        = "Cans",
        .name       = gettext_noop("ᐃᓄᒃᑎᑐᑦ")
    },
    {
        .code       = "ja:jpn",
        .ename      = "Japanese",
        .scr        = "Hani:Kana:Hira",
        .name       = gettext_noop("日本語"),
        .sample     = "いろはにほへと ちりぬるを 色は匂へど 散りぬるを"
    },
    {
        .code       = "jv:jav",
        .ename      = "Javanese",
        .scr        = "Latn",
        .name       = gettext_noop("Basa Jawa"),
        .sample     = "Aku isa mangan beling tanpa lara."
    },
    {
        .code       = "ka:kat:geo",
        .ename      = "Georgian",
        .scr        = "Geor",
        .name       = gettext_noop("ქართული"),
        .sample     = "მინას ვჭამ და არა მტკივა."
    },
    {
        .code       = "kaa",
        .ename      = "Kara-Kalpak",
        .scr        = "Cyrl",
        .name       = gettext_noop("Қарақалпақ")
    },
    {
        .code       = "kab",
        .ename      = "Kabyle",
        .scr        = "Latn",
        .name       = gettext_noop("Taqbaylit")
    },
    {
        .code       = "ki:kik",
        .ename      = "Kikuyu",
        .scr        = "Latn",
        .name       = gettext_noop("Gĩkũyũ")
    },
    {
        .code       = "kj:kua",
        .ename      = "Kuanyama",
        .scr        = "Latn",
        .name       = gettext_noop("Kuanyama")
    },
    {
        .code       = "kk:kaz",
        .ename      = "Kazakh",
        .scr        = "Cyrl",
        .name       = gettext_noop("қазақ тілі")
    },
    {
        .code       = "kl:kal",
        .ename      = "Kalaallisut",
        .scr        = "Latn",
        .name       = gettext_noop("Kalaallisut")
    },
    {
        .code       = "km:khm",
        .ename      = "Central Khmer",
        .scr        = "Khmr",
        .name       = gettext_noop("ខ្មែរ")
    },
    {
        .code       = "kl:kal",
        .ename      = "Kalaallisut",
        .scr        = "Latn",
        .name       = gettext_noop("Kalaallisut")
    },
    {
        .code       = "kmr",
        .ename      = "Nothern Kurdish",
        .scr        = "Latn",
        .name       = gettext_noop("Kurmancî")
    },
    {
        .code       = "ko:kor",
        .ename      = "Korean",
        .scr        = "Hang",
        .name       = gettext_noop("한국어"),
        .sample     = "다람쥐 헌 쳇바퀴에 타고파"
    },
    {
        .code       = "kok",
        .ename      = "Konkani",
        .scr        = "Deva",
        .name       = gettext_noop("कोंकणी")
    },
    {
        .code       = "kr:kau",
        .ename      = "Kanuri",
        .scr        = "Latn",
        .name       = gettext_noop("Kanuri")
    },
    {
        .code       = "ks:kas",
        .ename      = "Kashmiri",
        .scr        = "Arab",
        .name       = gettext_noop("कश्मीरी")
    },
    {
        .code       = "kum",
        .ename      = "Kumyk",
        .scr        = "Cyrl",
        .name       = gettext_noop("къумукъ тил")
    },
    {
        .code       = "kv:kom",
        .ename      = "Komi",
        .scr        = "Cyrl",
        .name       = gettext_noop("коми кыв")
    },
    {
        .code       = "kw:cor",
        .ename      = "Cornish",
        .scr        = "Latn",
        .name       = gettext_noop("Kernewek"),
        .sample     = "Mý a yl dybry gwéder hag éf ny wra ow ankenya."
    },
    {
        .code       = "kwm",
        .ename      = "Kwambi",
        .scr        = "Latn",
        .name       = gettext_noop("Kwambi")
    },
    {
        .code       = "ky:kir",
        .ename      = "Kirghiz",
        .scr        = "Cyrl",
        .name       = gettext_noop("Кыргызча")
    },
    {
        .code       = "la:lat",
        .ename      = "Latin",
        .scr        = "Latn",
        .name       = gettext_noop("Latine"),
        .sample     = "Sic surgens, dux, zelotypos quam karus haberis"
    },
    {
        .code       = "lb:ltz",
        .ename      = "Luxembourgish",
        .scr        = "Latn",
        .name       = gettext_noop("Lëtzebuergesch")
    },
    {
        .code       = "lez",
        .ename      = "Lezgian",
        .scr        = "Cyrl",
        .name       = gettext_noop("Лезги чӏал")
    },
    {
        .code       = "lg:lug",
        .ename      = "Ganda",
        .scr        = "Latn",
        .name       = gettext_noop("Luganda")
    },
    {
        .code       = "li:lim",
        .ename      = "Limburgish",
        .scr        = "Latn",
        .name       = gettext_noop("Limburgs")
    },
    {
        .code       = "ln:lin",
        .ename      = "Lingala",
        .scr        = "Latn",
        .name       = gettext_noop("Lingála")
    },
    {
        .code       = "lo:lao",
        .ename      = "Lao",
        .scr        = "Laoo",
        .name       = gettext_noop("ພາສາລາວ")
    },
    {
        .code       = "lt:lit",
        .ename      = "Lithuanian",
        .scr        = "Latn",
        .name       = gettext_noop("Lietuvių kalba"),
        .sample     = "Įlinkdama fechtuotojo špaga sublykčiojusi pragręžė apvalų arbūzą."
    },
    {
        .code       = "lv:lav",
        .ename      = "Latvian",
        .scr        = "Latn",
        .name       = gettext_noop("Latviešu Valoda"),
        .sample     = "Sarkanās jūrascūciņas peld pa jūru."
    },
    {
        .code       = "mai",
        .ename      = "Maithili",
        .scr        = "Deva",
        .name       = gettext_noop("মৈথিলী/मैथिली")
    },
    {
        .code       = "mg:mlg",
        .ename      = "Malagasy",
        .scr        = "Latn",
        .name       = gettext_noop("Fiteny malagasy")
    },
    {
        .code       = "mh:mah",
        .ename      = "Marshallese",
        .scr        = "Latn",
        .name       = gettext_noop("Kajin M̧ajeļ")
    },
    {
        .code       = "mi:mri:mao",
        .ename      = "Maori",
        .scr        = "Latn",
        .name       = gettext_noop("te reo Māori")
    },
    {
        .code       = "mk:mkd:mac",
        .ename      = "Macedonian",
        .scr        = "Cyrl",
        .name       = gettext_noop("Македонски јазик"),
        .sample     = "Можам да јадам стакло, а не ме штета."
    },
    {
        .code       = "ml:mal",
        .ename      = "Malayalam",
        .scr        = "Mlym",
        .name       = gettext_noop("മലയാളം"),
        .sample     = "വേദനയില്ലാതെ കുപ്പിചില്ലു് എനിയ്ക്കു് കഴിയ്ക്കാം."
    },
    {
        .code       = "mn:mon",
        .ename      = "Mongolian",
        .scr        = "Cyrl",
        .name       = gettext_noop("Монгол хэл")
    },
    {
        .code       = "mni",
        .ename      = "Meitei",
        .scr        = "Beng",
        .name       = gettext_noop("মনিপুরি")
    },
    {
        .code       = "mr:mar",
        .ename      = "Marathi",
        .scr        = "Deva",
        .name       = gettext_noop("मराठी"),
        .sample     = "मी काच खाऊ शकतो, मला ते दुखत नाही."

    },
    {
        .code       = "ms:msa:may",
        .ename      = "Malay",
        .scr        = "Latn",
        .name       = gettext_noop("Bahasa Melayu"),
        .sample     = "Saya boleh makan kaca dan ia tidak mencederakan saya."
    },
    {
        .code       = "mt:mlt",
        .ename      = "Maltese",
        .scr        = "Latn",
        .name       = gettext_noop("Malti")
    },
    {
        .code       = "my:mya:bur",
        .ename      = "Burmese",
        .scr        = "Mymr",
        .name       = gettext_noop("ဗမာစာ  ")
    },
    {
        .code       = "na:nau",
        .ename      = "Nauru",
        .scr        = "Latn",
        .name       = gettext_noop("Dorerin Naoero")
    },
    {
        .code       = "nb:nob",
        .ename      = "Norwegian Bokmål",
        .scr        = "Latn",
        .name       = gettext_noop("Norsk Bokmål"),
        .sample     = "Vår sære Zulu fra badeøya spilte jo whist og quickstep i min taxi."
    },
    {
        .code       = "nds",
        .ename      = "Low German",
        .scr        = "Latn",
        .name       = gettext_noop("Plattdeutsch")
    },
    {
        .code       = "ne:nep",
        .ename      = "Nepali",
        .scr        = "Deva",
        .name       = gettext_noop("नेपाली")
    },
    {
        .code       = "ng:ndo",
        .ename      = "Ndonga",
        .scr        = "Latn",
        .name       = gettext_noop("Owambo")
    },
    {
        .code       = "nl:nld:dut",
        .ename      = "Dutch",
        .scr        = "Latn",
        .name       = gettext_noop("Nederlands"),
        .sample     = "Pa's wijze lynx bezag vroom het fikse aquaduct."
    },
    {
        .code       = "nn:nno",
        .ename      = "Norwegian Nynorsk",
        .scr        = "Latn",
        .name       = gettext_noop("Norsk Nynorsk"),
        .sample     = "Eg kan eta glas utan å skada meg."
    },
    {
        .code       = "no:nor",
        .ename      = "Norwegian",
        .scr        = "Latn",
        .name       = gettext_noop("Norsk"),
        .sample     = "Vår sære Zulu fra badeøya spilte jo whist og quickstep i min taxi."
    },
    {
        .code       = "nr:nbl",
        .ename      = "South Ndebele",
        .scr        = "Latn",
        .name       = gettext_noop("isiNdebele")
    },
    {
        .code       = "nv:nav",
        .ename      = "Navajo",
        .scr        = "Latn",
        .name       = gettext_noop("Diné bizaad"),
        .sample     = "Tsésǫʼ yishą́ągo bííníshghah dóó doo shił neezgai da."
    },
    {
        .code       = "ny:nya",
        .ename      = "Chichewa",
        .scr        = "Latn",
        .name       = gettext_noop("chiCheŵa")
    },
    {
        .code       = "oc:oci",
        .ename      = "Occitan",
        .scr        = "Latn",
        .name       = gettext_noop("Occitan"),
        .sample     = "Pòdi manjar de veire, me nafrariá pas."
    },
    {
        .code       = "om:orm",
        .ename      = "Oromo",
        .scr        = "Latn",
        .name       = gettext_noop("Afaan Oromoo")
    },
    {
        .code       = "or:ori",
        .ename      = "Oriya",
        .scr        = "Orya",
        .name       = gettext_noop("ଓଡ଼ିଆ"),
        .sample     = "ମୁଁ କାଚ ଖାଇପାରେ ଏବଂ ତାହା ମୋର କ୍ଷତି କରିନଥାଏ।."
    },
    {
        .code       = "os:oss",
        .ename      = "Ossetian",
        .scr        = "Cyrl",
        .name       = gettext_noop("ирон æвзаг")
    },
    {
        .code       = "ota",
        .ename      = "Turkish, Ottoman",
        .scr        = "Arab",
        .name       = gettext_noop("لسان عثمانى")
    },
    {
        .code       = "pa:pan",
        .ename      = "Panjabi",
        .scr        = "Guru",
        .name       = gettext_noop("ਪੰਜਾਬੀ"),
        .sample     = "ਮੈਂ ਗਲਾਸ ਖਾ ਸਕਦਾ ਹਾਂ ਅਤੇ ਇਸ ਨਾਲ ਮੈਨੂੰ ਕੋਈ ਤਕਲੀਫ ਨਹੀਂ."
    },
    {
        .code       = "pl:pol",
        .ename      = "Polish",
        .scr        = "Latn",
        .name       = gettext_noop("Język Polski"),
        .sample     = "Pchnąć w tę łódź jeża lub ośm skrzyń fig."
    },
    {
        .code       = "ps:pus",
        .ename      = "Pashto",
        .scr        = "Arab",
        .name       = gettext_noop("پښتو")
    },
    {
        .code       = "pt:por",
        .ename      = "Portuguese",
        .scr        = "Latn",
        .name       = gettext_noop("Português"),
        .sample     = "Vejam a bruxa da raposa Salta-Pocinhas e o cão feliz que dorme regalado."
    },
    {
        .code       = "qu:que",
        .ename      = "Quechua",
        .scr        = "Latn",
        .name       = gettext_noop("Runa Simi")
    },
    {
        .code       = "rm:roh",
        .ename      = "Romansh",
        .scr        = "Latn",
        .name       = gettext_noop("Rumantsch Grischun")
    },
    {
        .code       = "rn:run",
        .ename      = "Rundi",
        .scr        = "Latn",
        .name       = gettext_noop("Ikirundi")
    },
    {
        .code       = "ro:ron:rum",
        .ename      = "Romanian",
        .scr        = "Latn",
        .name       = gettext_noop("Română"),
        .sample     = "Fumegând hipnotic sașiul azvârle mreje în bălți."

    },
    {
        .code       = "ru:rus",
        .ename      = "Russian",
        .scr        = "Cyrl",
        .name       = gettext_noop("Русский"),
        .sample     = "В чащах юга жил бы цитрус? Да, но фальшивый экземпляр!"
    },
    {
        .code       = "rw:kin",
        .ename      = "Kinyarwanda",
        .scr        = "Latn",
        .name       = gettext_noop("Ikinyarwanda")

    },
    {
        .code       = "sa:san",
        .ename      = "Sanskrit",
        .scr        = "Deva",
        .name       = gettext_noop("संस्कृतम्"),
        .sample     = "काचं शक्नोम्यत्तुम् । नोपहिनस्ति माम् ॥"

    },
    {
        .code       = "sah",
        .ename      = "Yakut",
        .scr        = "Cyrl",
        .name       = gettext_noop("Саха тыла")

    },
    {
        .code       = "sat",
        .ename      = "Santali",
        .scr        = "Deva",
        .name       = gettext_noop("ᱥᱟᱱᱛᱟᱲᱤ")

    },
    {
        .code       = "sc:srd",
        .ename      = "Sardinian",
        .scr        = "Latn",
        .name       = gettext_noop("Sardu")

    },
    {
        .code       = "sco",
        .ename      = "Scots",
        .scr        = "Latn",
        .name       = gettext_noop("Scots")

    },
    {
        .code       = "sd:snd",
        .ename      = "Sindhi",
        .scr        = "Arab",
        .name       = gettext_noop("सिन्धी")

    },
    {
        .code       = "sdh",
        .ename      = "Southern Kurdish",
        .scr        = "Arab",
        .name       = gettext_noop("کوردی خوارگ")
    },
    {
        .code       = "se:sme",
        .ename      = "Northern Sami",
        .scr        = "Latn",
        .name       = gettext_noop("Davvisámegiella")

    },
    {
        .code       = "sg:sag",
        .ename      = "Sango",
        .scr        = "Latn",
        .name       = gettext_noop("Yângâ tî sängö")

    },
    {
        .code       = "si:sin",
        .ename      = "Sinhala",
        .scr        = "Sinh",
        .name       = gettext_noop("සිංහල")

    },
    {
        .code       = "sk:slk:slo",
        .ename      = "Slovak",
        .scr        = "Latn",
        .name       = gettext_noop("Slovenčina"),
        .sample     = "Starý kôň na hŕbe kníh žuje tíško povädnuté ruže, na stĺpe sa ďateľ učí kvákať novú ódu o živote."
    },
    {
        .code       = "sl:slv",
        .ename      = "Slovenian",
        .scr        = "Latn",
        .name       = gettext_noop("Slovenski Jezik"),
        .sample     = "Šerif bo za vajo spet kuhal domače žgance."
    },
    {
        .code       = "sm:smo",
        .ename      = "Samoan",
        .scr        = "Latn",
        .name       = gettext_noop("Gagana fa'a Samoa")
    },
    {
        .code       = "sma",
        .ename      = "Southern Sami",
        .scr        = "Latn",
        .name       = gettext_noop("Åarjelsaemien gïele")
    },
    {
        .code       = "smj",
        .ename      = "Lule Sami",
        .scr        = "Latn",
        .name       = gettext_noop("Julevsámegiella")
    },
    {
        .code       = "smn",
        .ename      = "Inari Sami",
        .scr        = "Latn",
        .name       = gettext_noop("Anarâškielâ")
    },
    {
        .code       = "sms",
        .ename      = "Skolt Sami",
        .scr        = "Latn",
        .name       = gettext_noop("Sääʹmǩiõll")
    },
    {
        .code       = "sn:sna",
        .ename      = "Shona",
        .scr        = "Latn",
        .name       = gettext_noop("chiShona")
    },
    {
        .code       = "so:som",
        .ename      = "Somali",
        .scr        = "Latn",
        .name       = gettext_noop("Soomaaliga")
    },
    {
        .code       = "sq:sqi:alb",
        .ename      = "Albanian",
        .scr        = "Latn",
        .name       = gettext_noop("Shqip"),
        .sample     = "Unë mund të ha qelq dhe nuk më gjen gjë."
    },
    {
        .code       = "sr:srp",
        .ename      = "Serbian",
        .scr        = "Cyrl",
        .name       = gettext_noop("Српски језик"),
        .sample     = "Чешће цeђење мрeжастим џаком побољшава фертилизацију генских хибрида."
    },
    {
        .code       = "ss:ssw",
        .ename      = "Swati",
        .scr        = "Latn",
        .name       = gettext_noop("SiSwati")
    },
    {
        .code       = "st:sot",
        .ename      = "Southern Sotho",
        .scr        = "Latn",
        .name       = gettext_noop("Sesotho")
    },
    {
        .code       = "su:sun",
        .ename      = "Sundanese",
        .scr        = "Latn",
        .name       = gettext_noop("Basa Sunda")
    },
    {
        .code       = "sv:swe",
        .ename      = "Swedish",
        .scr        = "Latn",
        .name       = gettext_noop("Svenska"),
        .sample     = "Flygande bäckasiner söka strax hwila på mjuka tuvor."
    },
    {
        .code       = "sw:swa",
        .ename      = "Swahili",
        .scr        = "Latn",
        .name       = gettext_noop("Kiswahili")
    },
    {
        .code       = "syr",
        .ename      = "Syriac",
        .scr        = "Syrc",
        .name       = gettext_noop("ܠܶܫܳܢܳܐ ܣܽܘܪܝܳܝܳܐ")
    },
    {
        .code       = "ta:tam",
        .ename      = "Tamil",
        .scr        = "Taml",
        .name       = gettext_noop("தமிழ்"),
        .sample     = "நான் கண்ணாடி சாப்பிடுவேன், அதனால் எனக்கு ஒரு கேடும் வராது."
    },
    {
        .code       = "te:tel",
        .ename      = "Telugu",
        .scr        = "Telu",
        .name       = gettext_noop("తెలుగు"),
        .sample     = "నేను గాజు తినగలను అయినా నాకు యేమీ కాదు."
    },
    {
        .code       = "tg:tgk",
        .ename      = "Tajik",
        .scr        = "Cyrl",
        .name       = gettext_noop("тоҷикӣ")
    },
    {
        .code       = "th:tha",
        .ename      = "Thai",
        .scr        = "Thai",
        .name       = gettext_noop("ไทย"),
        .sample     = "เป็นมนุษย์สุดประเสริฐเลิศคุณค่า - กว่าบรรดาฝูงสัตว์เดรัจฉาน - จงฝ่าฟันพัฒนาวิชาการ อย่าล้างผลาญฤๅเข่นฆ่าบีฑาใคร - ไม่ถือโทษโกรธแช่งซัดฮึดฮัดด่า - หัดอภัยเหมือนกีฬาอัชฌาสัย - ปฏิบัติประพฤติกฎกำหนดใจ - พูดจาให้จ๊ะ ๆ จ๋า ๆ น่าฟังเอยฯ"
    },
    {
        .code       = "ti:tir",
        .ename      = "Tigrinya",
        .scr        = "Ethi",
        .name       = gettext_noop("ትግርኛ")
    },
    {
        .code       = "tig",
        .ename      = "Tigre",
        .scr        = "Ethi",
        .name       = gettext_noop("ትግረ")
    },
    {
        .code       = "tk:tuk",
        .ename      = "Turkmen",
        .scr        = "Latn",
        .name       = gettext_noop("Türkmen")
    },
    {
        .code       = "tl:tgl",
        .ename      = "Tagalog",
        .scr        = "Latn",
        .name       = gettext_noop("Wikang Tagalog"),
        .sample     = "Kaya kong kumain nang bubog at hindi ako masaktan."
    },
    {
        .code       = "tn:tsn",
        .ename      = "Tswana",
        .scr        = "Latn",
        .name       = gettext_noop("Setswana")
    },
    {
        .code       = "to:ton",
        .ename      = "Tonga",
        .scr        = "Latn",
        .name       = gettext_noop("Faka Tonga")
    },
    {
        .code       = "tr:tur",
        .ename      = "Turkish",
        .scr        = "Latn",
        .name       = gettext_noop("Türkçe"),
        .sample     = "Pijamalı hasta yağız şoföre çabucak güvendi."
    },
    {
        .code       = "ts:tso",
        .ename      = "Tsonga",
        .scr        = "Latn",
        .name       = gettext_noop("Xitsonga")
    },
    {
        .code       = "tt:tat",
        .ename      = "Tatar",
        .scr        = "Cyrl",
        .name       = gettext_noop("Татар теле")
    },
    {
        .code       = "tw:twi",
        .ename      = "Twi",
        .scr        = "Latn",
        .name       = gettext_noop("Twi"),
        .sample     = "Metumi awe tumpan, ɜnyɜ me hwee."
    },
    {
        .code       = "ti:tah",
        .ename      = "Tahitian",
        .scr        = "Latn",
        .name       = gettext_noop("Reo Tahiti")
    },
    {
        .code       = "tyv",
        .ename      = "Tuvan",
        .scr        = "Cyrl",
        .name       = gettext_noop("Тыва дыл")
    },
    {
        .code       = "ug:uig",
        .ename      = "Uyghur",
        .scr        = "Arab",
        .name       = gettext_noop("ئۇيغۇرچە")
    },
    {
        .code       = "uk:ukr",
        .ename      = "Ukrainian",
        .scr        = "Cyrl",
        .name       = gettext_noop("Українська"),
        .sample     = "Чуєш їх, доцю, га? Кумедна ж ти, прощайся без ґольфів!"
    },
    {
        .code       = "ur:urd",
        .ename      = "Urdu",
        .scr        = "Arab",
        .name       = gettext_noop("اردو"),
        .sample     = "میں کانچ کھا سکتا ہوں اور مجھے تکلیف نہیں ہوتی ۔"
    },
    {
        .code       = "uz:uzb",
        .ename      = "Uzbek",
        .scr        = "Latn",
        .name       = gettext_noop("Oʻzbek")
    },
    {
        .code       = "ve:ven",
        .ename      = "Venda",
        .scr        = "Latn",
        .name       = gettext_noop("Tshivenḓa")
    },
    {
        .code       = "vi:vie",
        .ename      = "Vietnamese",
        .scr        = "Latn",
        .name       = gettext_noop("Tiếng Việt"),
        .sample     = "Con sói nâu nhảy qua con chó lười."
    },
    {
        .code       = "vo:vol",
        .ename      = "Volapük",
        .scr        = "Latn",
        .name       = gettext_noop("Volapük")
    },
    {
        .code       = "vot",
        .ename      = "Votic",
        .scr        = "Latn",
        .name       = gettext_noop("Vađđa ceeli")
    },
    {
        .code       = "wa:wln",
        .ename      = "Walloon",
        .scr        = "Latn",
        .name       = gettext_noop("Walon"),
        .sample     = "Dji pou magnî do vêre, çoula m' freut nén må."
    },
    {
        .code       = "wo:wol",
        .ename      = "Wolof",
        .scr        = "Latn",
        .name       = gettext_noop("Wollof")
    },
    {
        .code       = "xh:xho",
        .ename      = "Xhosa",
        .scr        = "Latn",
        .name       = gettext_noop("isiXhosa")
    },
    {
        .code       = "yi:yid",
        .ename      = "Yiddish",
        .scr        = "Hebr",
        .name       = gettext_noop("ייִדיש"),
        .sample     = "איך קען עסן גלאָז און עס טוט מיר נישט װײ."
    },
    {
        .code       = "yo:yor",
        .ename      = "Yoruba",
        .scr        = "Latn",
        .name       = gettext_noop("Yorùbá"),
        .sample     = "Mo lè je̩ dígí, kò ní pa mí lára."
    },
    {
        .code       = "za:zha",
        .ename      = "Zhuang",
        .scr        = "Latn",
        .name       = gettext_noop("Saɯ cueŋƅ")
    },
    {
        .code       = "zh:zho:chi",
        .ename      = "Chinese",
        .scr        = "Hani",
        .name       = gettext_noop("中文"),
        .sample     = "我能吞下玻璃而不傷身體。"
    },
    {
        .code       = "zu:zul",
        .ename      = "Zulu",
        .scr        = "Latn",
        .name       = gettext_noop("isiZulu")
    },
    {
        .code   = nullptr
    }
};

Language::Language(std::string_view code):
    data(datas)
{
    if (code.empty()) {
        if (locale_ptr_) {
            data = locale_ptr_->lang.data;
        }
    }

    else {
        // First try find by ISO code.
        for (Language_data * datap = datas; datap->code; ++datap) {
            for (const char * p = datap->code; *p; ) {
                const char * e = std::strchr(p, ':');
                if (!e) { e = p+std::strlen(p); }
                if (std::string(p, e-p) == code) { data = datap; return; }
                p = e;
                if (*p) { ++p; }
            }
        }

        // Not found, now try find by name.
        for (Language_data * datap = datas; datap->code; ++datap) {
            if (datap->ename && code == std::string(datap->ename)) { data = datap; return; }
            if (datap->name && (code == std::string(datap->name) || code == lgettext(datap->name).raw())) { data = datap; return; }
        }

        data = datas;
    }
}

Language::Language(const Language & other):
    data(other.data)
{
}

Language & Language::operator=(const Language & other) {
    if (this != &other) {
        data = other.data;
    }

    return *this;
}

Language::~Language() {}

// static
Language Language::system() {
    return sys_locale_ptr_ ?  sys_locale_ptr_->lang : Language();
}

std::string Language::ename() const {
    return data->ename;
}

std::string Language::code() const {
    std::vector<ustring> v;
    if (data->code) { v = str_explode(data->code, ':'); }
    unsigned pos, len, min = UINT_MAX; std::string sh;

    for (ustring s: v) {
        pos = s.find_first_of("_-"); if (pos < s.size()) { s.erase(pos); }
        len = s.size();
        if (len < min) { min = len, sh = s; }
    }

    return sh;
}

std::string Language::code2() const {
    const std::vector<ustring> v = str_explode(data->code, ':');

    for (std::size_t i = 0; i < v.size(); ++i) {
        if (2 == v[i].size()) {
            return v[i];
        }
    }

    return v.empty() ? "" : v[0];
}

std::string Language::code3() const {
    const std::vector<ustring> v = str_explode(data->code, ':');

    for (std::size_t i = 0; i < v.size(); ++i) {
        if (3 == v[i].size()) {
            return v[i];
        }
    }

    return v.empty() ? "" : v[0];
}

ustring Language::name() const {
    return data->name ? lgettext(data->name) : data->ename;
}

bool Language::operator==(const Language & other) const noexcept {
    return other.data == data;
}

bool Language::operator!=(const Language & other) const noexcept {
    return other.data != data;
}

bool Language::operator<(const Language & other) const noexcept {
    return std::strcmp(str_explode(data->code, ':').front().data(), str_explode(other.data->code, ':').front().data()) < 0;
}

ustring Language::sample() const {
    return data->sample ? data->sample : "The quick brown fox jumps over the lazy dog.";
}

std::vector<Script> Language::scripts() const {
    std::vector<Script> v;
    for (const ustring & s: str_explode(data->scr, Locale().blanks())) { v.push_back(Script::from_code(s)); }
    return v;
}

} // namespace tau

//END

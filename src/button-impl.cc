// ----------------------------------------------------------------------------
// SPDX-License-Identifier: BSD-2-Clause
// Copyright © 2014-2024 Konstantin Shmelkov <mrcashe@gmail.com>.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
// THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
// PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
// CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
// EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
// PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
// OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
// WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
// OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
// EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// ----------------------------------------------------------------------------

#include <tau/brush.hh>
#include <tau/painter.hh>
#include <box-impl.hh>
#include <button-impl.hh>
#include <icon-impl.hh>
#include <text-impl.hh>
#include <table-impl.hh>
#include <iostream>

namespace tau {

Button_base_impl::Button_base_impl() {
    init(-1, -1, -1, -1);
}

Button_base_impl::Button_base_impl(unsigned r) {
    init(r, r, r, r);
}

Button_base_impl::Button_base_impl(unsigned rtop_left, unsigned rtop_right, unsigned rbottom_right, unsigned rbottom_left) {
    init(rtop_left, rtop_right, rbottom_right, rbottom_left);
}

Button_base_impl::Button_base_impl(const ustring & label) {
    init(-1, -1, -1, -1);
    set_label(label);
}

Button_base_impl::Button_base_impl(const ustring & label, unsigned r) {
    init(r, r, r, r);
    set_label(label);
}

Button_base_impl::Button_base_impl(const ustring & label, unsigned rtop_left, unsigned rtop_right, unsigned rbottom_right, unsigned rbottom_left) {
    init(rtop_left, rtop_right, rbottom_right, rbottom_left);
    set_label(label);
}

Button_base_impl::Button_base_impl(Widget_ptr img) {
    init(-1, -1, -1, -1);
    set_image(img);
}

Button_base_impl::Button_base_impl(Widget_ptr img, unsigned r) {
    init(r, r, r, r);
    set_image(img);
}

Button_base_impl::Button_base_impl(Widget_ptr img, unsigned rtop_left, unsigned rtop_right, unsigned rbottom_right, unsigned rbottom_left) {
    init(rtop_left, rtop_right, rbottom_right, rbottom_left);
    set_image(img);
}

Button_base_impl::Button_base_impl(Widget_ptr img, const ustring & label) {
    init(-1, -1, -1, -1);
    set_image(img);
    set_label(label);
}

Button_base_impl::Button_base_impl(Widget_ptr img, const ustring & label, unsigned r) {
    init(r, r, r, r);
    set_image(img);
    set_label(label);
}

Button_base_impl::Button_base_impl(Widget_ptr img, const ustring & label, unsigned rtop_left, unsigned rtop_right, unsigned rbottom_right, unsigned rbottom_left) {
    init(rtop_left, rtop_right, rbottom_right, rbottom_left);
    set_image(img);
    set_label(label);
}

Button_base_impl::Button_base_impl(int icon_size, const ustring & icon_name) {
    init(-1, -1, -1, -1);
    set_icon(icon_name, icon_size);
}

Button_base_impl::Button_base_impl(int icon_size, const ustring & icon_name, unsigned r) {
    init(r, r, r, r);
    set_icon(icon_name, icon_size);
}

Button_base_impl::Button_base_impl(int icon_size, const ustring & icon_name, unsigned rtop_left, unsigned rtop_right, unsigned rbottom_right, unsigned rbottom_left) {
    init(rtop_left, rtop_right, rbottom_right, rbottom_left);
    set_icon(icon_name, icon_size);
}

Button_base_impl::Button_base_impl(const ustring & label, int icon_size, const ustring & icon_name) {
    init(-1, -1, -1, -1);
    set_label(label);
    set_icon(icon_name, icon_size);
}

Button_base_impl::Button_base_impl(const ustring & label, int icon_size, const ustring & icon_name, unsigned r) {
    init(r, r, r, r);
    set_label(label);
    set_icon(icon_name, icon_size);
}

Button_base_impl::Button_base_impl(const ustring & label, int icon_size, const ustring & icon_name, unsigned rtop_left, unsigned rtop_right, unsigned rbottom_right, unsigned rbottom_left) {
    init(rtop_left, rtop_right, rbottom_right, rbottom_left);
    set_label(label);
    set_icon(icon_name, icon_size);
}

Button_base_impl::Button_base_impl(Action_base & action, Action::Flags items) {
    init(-1, -1, -1, -1);
    init(action, Icon::DEFAULT, items);
}

Button_base_impl::Button_base_impl(Action_base & action, unsigned r, Action::Flags items) {
    init(r, r, r, r);
    init(action, Icon::DEFAULT, items);
}

Button_base_impl::Button_base_impl(Action_base & action, unsigned rtop_left, unsigned rtop_right, unsigned rbottom_right, unsigned rbottom_left, Action::Flags items) {
    init(rtop_left, rtop_right, rbottom_right, rbottom_left);
    init(action, Icon::DEFAULT, items);
}

Button_base_impl::Button_base_impl(Action_base & action, int icon_size, Action::Flags items) {
    init(-1, -1, -1, -1);
    init(action, icon_size, items);
}

Button_base_impl::Button_base_impl(Action_base & action, int icon_size, unsigned r, Action::Flags items) {
    init(r, r, r, r);
    init(action, icon_size, items);
}

Button_base_impl::Button_base_impl(Action_base & action, int icon_size, unsigned rtop_left, unsigned rtop_right, unsigned rbottom_right, unsigned rbottom_left, Action::Flags items) {
    init(rtop_left, rtop_right, rbottom_right, rbottom_left);
    init(action, icon_size, items);
}

Button_base_impl::Button_base_impl(Master_action & action, Action::Flags items) {
    init(-1, -1, -1, -1);
    init(action, Icon::DEFAULT, items);
}

Button_base_impl::Button_base_impl(Master_action & action, unsigned r, Action::Flags items) {
    init(r, r, r, r);
    init(action, Icon::DEFAULT, items);
}

Button_base_impl::Button_base_impl(Master_action & action, unsigned rtop_left, unsigned rtop_right, unsigned rbottom_right, unsigned rbottom_left, Action::Flags items) {
    init(rtop_left, rtop_right, rbottom_right, rbottom_left);
    init(action, Icon::DEFAULT, items);
}

Button_base_impl::Button_base_impl(Master_action & action, int icon_size, Action::Flags items) {
    init(-1, -1, -1, -1);
    init(action, icon_size, items);
}

Button_base_impl::Button_base_impl(Master_action & action, int icon_size, unsigned r, Action::Flags items) {
    init(r, r, r, r);
    init(action, icon_size, items);
}

Button_base_impl::Button_base_impl(Master_action & action, int icon_size, unsigned rtop_left, unsigned rtop_right, unsigned rbottom_right, unsigned rbottom_left, Action::Flags items) {
    init(rtop_left, rtop_right, rbottom_right, rbottom_left);
    init(action, icon_size, items);
}

void Button_base_impl::init(int rtop_left, int rtop_right, int rbottom_right, int rbottom_left) {
    disallow_focus();
    radius_top_left_ = rtop_left;
    radius_top_right_ = rtop_right;
    radius_bottom_right_ = rbottom_right;
    radius_bottom_left_ = rbottom_left;
    table_ = std::make_shared<Table_impl>(Align::FILL, Align::FILL, 4, 0);
    table_->set_row_margin(0, 1, 1);
    table_->set_column_margin(0, 1, 1);
    table_->set_column_margin(1, 1, 4);
    insert(table_);
    set_border(1, Border::OUTSET, -1 != rtop_left ? rtop_left : 0, -1 != rtop_right ? rtop_right : 0, -1 != rbottom_right ? rbottom_right : 0, -1 != rbottom_left ? rbottom_left :0);

    conf().signal_changed(Conf::RADIUS).connect(fun(this, &Button_impl::on_conf_radius));
    conf().signal_changed(Conf::BACKGROUND).connect(fun(this, &Button_base_impl::redraw));
    conf().signal_changed(Conf::BUTTON_BACKGROUND).connect(fun(this, &Button_base_impl::on_background_changed));
    on_background_changed();

    signal_display_in_.connect(fun(this, &Button_base_impl::redraw));
    signal_focus_in_.connect(fun(this, &Button_base_impl::redraw));
    signal_focus_out_.connect(fun(this, &Button_base_impl::redraw));
    signal_take_focus_.connect(fun(this, &Button_base_impl::grab_focus));
    signal_mouse_enter().connect(fun(this, &Button_base_impl::on_mouse_enter));
    signal_mouse_leave().connect(fun(this, &Button_base_impl::on_mouse_leave));
    signal_mouse_down().connect(fun(this, &Button_base_impl::on_mouse_down), true);
    signal_mouse_up().connect(fun(this, &Button_base_impl::on_mouse_up), true);
}

void Button_base_impl::init(Action_base & action, int icon_size, Action::Flags items) {
    enable_cx_ = action.signal_enable().connect(fun(this, &Button_base_impl::thaw));
    disable_cx_ = action.signal_disable().connect(fun(this, &Button_base_impl::freeze));
    show_cx_ = action.signal_show().connect(fun(this, &Button_base_impl::appear));
    hide_cx_ = action.signal_hide().connect(fun(this, &Button_base_impl::disappear));
    conf().signal_changed(Conf::BUTTON_LABEL).connect(fun(this, &Button_base_impl::on_button_label));

    if (!action.enabled()) { freeze(); }
    if (!action.visible()) { disappear(); }

    text_ = action.label();
    action.signal_label_changed().connect(fun(this, &Button_base_impl::on_action_label_changed));
    if (conf().boolean(Conf::BUTTON_LABEL) && !text_.empty()) { set_label(text_); }

    if (items & Action::TOOLTIP) {
        action.signal_tooltip_changed().connect(bind_back(fun(this, &Button_base_impl::on_action_tooltip_changed), std::cref(action)));
        tooltip_ = action.tooltip();
        set_action_tooltip(action);
    }

    if (items & Action::ACCEL) {
        action.signal_accel_added().connect(bind_back(fun(this, &Button_base_impl::on_action_accel_changed), std::cref(action)));
        action.signal_accel_removed().connect(bind_back(fun(this, &Button_base_impl::on_action_accel_changed), std::cref(action)));
    }

    if (items & Action::ICON) {
        action.signal_icon_changed().connect(bind_back(fun(this, &Button_base_impl::set_icon), icon_size));
        if (!action.icon_name().empty()) { set_icon(action.icon_name(), icon_size); }
    }
}

void Button_base_impl::init(Master_action & action, int icon_size, Action::Flags items) {
    action.signal_enable().connect(fun(this, &Button_base_impl::thaw));
    action.signal_disable().connect(fun(this, &Button_base_impl::freeze));
    action.signal_show().connect(fun(this, &Button_base_impl::appear));
    action.signal_hide().connect(fun(this, &Button_base_impl::disappear));
    conf().signal_changed(Conf::BUTTON_LABEL).connect(fun(this, &Button_base_impl::on_button_label));

    freeze();
    if (!action.visible()) { disappear(); }

    text_ = action.label();
    action.signal_label_changed().connect(fun(this, &Button_base_impl::on_action_label_changed));
    if (conf().boolean(Conf::BUTTON_LABEL) && !text_.empty()) { set_label(text_); }

    if (items & Action::TOOLTIP) {
        action.signal_tooltip_changed().connect(bind_back(fun(this, &Button_base_impl::on_master_tooltip_changed), std::cref(action)));
        tooltip_ = action.tooltip();
        set_action_tooltip(action);
    }

    if (items & Action::ACCEL) {
        action.signal_accel_added().connect(bind_back(fun(this, &Button_base_impl::on_master_accel_changed), std::cref(action)));
        action.signal_accel_removed().connect(bind_back(fun(this, &Button_base_impl::on_master_accel_changed), std::cref(action)));
    }

    if (items & Action::ICON) {
        action.signal_icon_changed().connect(bind_back(fun(this, &Button_base_impl::set_icon), icon_size));
        if (!action.icon_name().empty()) { set_icon(action.icon_name(), icon_size); }
    }
}

void Button_base_impl::set_label(const ustring & s) {
    table_->remove(label_.get());

    if (!s.empty()) {
        auto lp = std::make_shared<Label_impl>(s);
        lp->wrap(Wrap::ELLIPSIZE_END);
        label_ = lp;
        table_->put(label_, 1, 0, 1, 1, false, true);
    }
}

void Button_base_impl::set_image(Widget_ptr wp) {
    table_->remove(image_.get());
    image_ = wp; table_->put(image_, 0, 0, 1, 1, true, true);
}

void Button_base_impl::set_icon(const ustring & icon_name, int icon_size) {
    set_image(std::make_shared<Icon_impl>(icon_name, icon_size));
}

int Button_base_impl::icon_size() const noexcept {
    if (auto icon = std::dynamic_pointer_cast<Icon_impl>(image_)) {
        return icon->icon_size();
    }

    return -1;
}

void Button_base_impl::resize_icon(int icon_size) {
    if (auto icon = std::dynamic_pointer_cast<Icon_impl>(image_)) {
        icon->resize(icon_size);
    }
}

void Button_base_impl::on_action_label_changed(const ustring & label) {
    text_ = label;
    if (label_) { set_label(text_); }
}

void Button_base_impl::on_button_label() {
    set_label(conf().boolean(Conf::BUTTON_LABEL) ? text_ : "");
}

void Button_base_impl::on_conf_radius() {
    int r = conf().integer(Conf::RADIUS);
    int ltop = radius_top_left_, rtop = radius_top_right_, rbot = radius_bottom_right_, lbot = radius_bottom_left_;
    if (-1 == ltop) { ltop = r; }
    if (-1 == rtop) { rtop = r; }
    if (-1 == rbot) { rbot = r; }
    if (-1 == lbot) { lbot = r; }
    set_border_radius(ltop, rtop, rbot, lbot);
}

void Button_base_impl::set_action_tooltip(const Action_base & action) {
    set_action_tooltip(action.accels());
}

void Button_base_impl::set_action_tooltip(const Master_action & action) {
    set_action_tooltip(action.accels());
}

void Button_base_impl::set_action_tooltip(const std::vector<const Accel *> & accels) {
    if (!accels.empty()) {
        auto box = std::make_shared<Box_impl>(Orientation::RIGHT, 8);
        if (!tooltip_.empty()) { box->append(std::make_shared<Label_impl>(tooltip_), true); }
        auto tp = std::make_shared<Label_impl>(accels.front()->label());
        tp->conf().redirect(Conf::FOREGROUND, Conf::ACCEL_FOREGROUND);
        box->append(tp, true);
        box->hint_margin(2);
        box->conf().redirect(Conf::FONT, Conf::TOOLTIP_FONT);
        set_tooltip(box);
    }

    else if (!tooltip_.empty()) {
        set_tooltip(tooltip_);
    }

    else {
        unset_tooltip();
    }
}

void Button_base_impl::on_action_accel_changed(const Accel & accel, const Action_base & action) {
    set_action_tooltip(action);
}

void Button_base_impl::on_master_accel_changed(const Accel & accel, const Master_action & action) {
    set_action_tooltip(action);
}

void Button_base_impl::on_action_tooltip_changed(const ustring & tooltip, const Action_base & action) {
    tooltip_ = tooltip;
    set_action_tooltip(action);
}

void Button_base_impl::on_master_tooltip_changed(const ustring & tooltip, const Master_action & action) {
    tooltip_ = tooltip;
    set_action_tooltip(action);
}

void Button_base_impl::on_mouse_enter(const Point & pt) {
    if (!pressed_) {
        redraw();
    }
}

void Button_base_impl::on_background_changed() {
    if (relief_visible_) {
        Brush b = conf().brush(Conf::BUTTON_BACKGROUND);
        conf().brush(Conf::BACKGROUND) = b;
    }
}

void Button_base_impl::on_mouse_leave() {
    fix_press_ = false;
    timer_.stop();
    timer_cx_.drop();
    redraw();
}

bool Button_base_impl::on_mouse_up(int mbt, int mm, const Point & position) {
    if (MBT_LEFT == mbt) {
        if (fix_press_) {
            fix_press_ = false;
            timer_.stop();
            timer_cx_.drop();
            on_release();
        }

        return true;
    }

    return false;
}

bool Button_base_impl::on_mouse_down(int mbt, int mm, const Point & position) {
    if (MBT_LEFT == mbt) {
        hide_tooltip();
        fix_press_ = true;
        grab_focus();
        press();
        on_press();
        return true;
    }

    return false;
}

void Button_base_impl::show_relief() {
    if (!relief_visible_) {
        relief_visible_ = true;
        on_background_changed();
        redraw();
    }
}

void Button_base_impl::hide_relief() {
    if (relief_visible_) {
        relief_visible_ = false;
        conf().unset(Conf::BACKGROUND);
        redraw();
    }
}

bool Button_base_impl::relief_visible() const noexcept {
    return relief_visible_;
}

void Button_base_impl::redraw() {
    Brush b = conf().brush(relief_visible_ ? Conf::BUTTON_BACKGROUND : Conf::BACKGROUND);

    if (relief_visible_) {
        if (!pressed_ && enabled() && (focused() || hover())) {
            set_border_style(Border::SOLID);
            set_border_color(conf().color(Conf::SELECT_BACKGROUND));
        }

        else {
            unset_border_color();
            set_border_style(pressed_ ? Border::INSET : Border::OUTSET);
        }
    }

    else {
        set_border_style(pressed_ ? Border::INSET : Border::NONE);
    }

    if (enabled()) {
        if (pressed_) { b.darker(0.1); }
        else if (hover()) { b.lighter(0.07); }
    }

    else {
        unset_border_color();
    }

    table_->conf().brush(Conf::BACKGROUND) = b;
}

void Button_base_impl::press() {
    if (enabled()) {
        if (!pressed_) {
            pressed_ = true;
            redraw();
        }
    }
}

// Overrides Frame_impl.
void Button_base_impl::set_border_top_left_radius(unsigned radius) {
    if (radius_top_left_ != int(radius)) {
        radius_top_left_ = radius;
        Frame_impl::set_border_top_left_radius(radius);
    }
}

// Overrides Frame_impl.
void Button_base_impl::set_border_top_right_radius(unsigned radius) {
    if (radius_top_right_ != int(radius)) {
        radius_top_right_ = radius;
        Frame_impl::set_border_top_right_radius(radius);
    }
}

// Overrides Frame_impl.
void Button_base_impl::set_border_bottom_right_radius(unsigned radius) {
    if (radius_bottom_right_ != int(radius)) {
        radius_bottom_right_ = radius;
        Frame_impl::set_border_bottom_right_radius(radius);
    }
}

// Overrides Frame_impl.
void Button_base_impl::set_border_bottom_left_radius(unsigned radius) {
    if (radius_bottom_left_ != int(radius)) {
        radius_bottom_left_ = radius;
        Frame_impl::set_border_bottom_left_radius(radius);
    }
}

// Overrides Frame_impl.
void Button_base_impl::set_border_radius(unsigned radius) {
    set_border_radius(radius, radius, radius, radius);
}

// Overrides Frame_impl.
void Button_base_impl::set_border_radius(unsigned top_left, unsigned top_right, unsigned bottom_right, unsigned bottom_left) {
    set_border_top_left_radius(top_left);
    set_border_top_right_radius(top_right);
    set_border_bottom_right_radius(bottom_right);
    set_border_bottom_left_radius(bottom_left);
}

void Button_base_impl::unset_border_top_left_radius() {
    radius_top_left_ = -1;
    Frame_impl::set_border_top_left_radius(conf().integer(Conf::RADIUS));

}

void Button_base_impl::unset_border_top_right_radius() {
    radius_top_right_ = -1;
    Frame_impl::set_border_top_right_radius(conf().integer(Conf::RADIUS));
}

void Button_base_impl::unset_border_bottom_right_radius() {
    radius_bottom_right_ = -1;
    Frame_impl::set_border_bottom_right_radius(conf().integer(Conf::RADIUS));
}

void Button_base_impl::unset_border_bottom_left_radius() {
    radius_bottom_left_ = -1;
    Frame_impl::set_border_bottom_left_radius(conf().integer(Conf::RADIUS));
}

void Button_base_impl::unset_border_radius() {
    radius_top_left_ = radius_top_right_ = radius_bottom_right_ = radius_bottom_left_ = -1;
    Frame_impl::set_border_radius(conf().integer(Conf::RADIUS));
}

// ----------------------------------------------------------------------------
// ----------------------------------------------------------------------------

Button_impl::Button_impl() {
    init();
}

Button_impl::Button_impl(unsigned radius):
    Button_base_impl(radius)
{
    init();
}

Button_impl::Button_impl(unsigned rtop_left, unsigned rtop_right, unsigned rbottom_right, unsigned rbottom_left):
    Button_base_impl(rtop_left, rtop_right, rbottom_right, rbottom_left)
{
    init();
}

Button_impl::Button_impl(const ustring & label):
    Button_base_impl(label)
{
    init();
}

Button_impl::Button_impl(const ustring & label, unsigned radius):
    Button_base_impl(label, radius)
{
    init();
}

Button_impl::Button_impl(const ustring & label, unsigned rtop_left, unsigned rtop_right, unsigned rbottom_right, unsigned rbottom_left):
    Button_base_impl(label, rtop_left, rtop_right, rbottom_right, rbottom_left)
{
    init();
}

Button_impl::Button_impl(Widget_ptr img):
    Button_base_impl(img)
{
    init();
}

Button_impl::Button_impl(Widget_ptr img, unsigned radius):
    Button_base_impl(img, radius)
{
    init();
}

Button_impl::Button_impl(Widget_ptr img, unsigned rtop_left, unsigned rtop_right, unsigned rbottom_right, unsigned rbottom_left):
    Button_base_impl(img, rtop_left, rtop_right, rbottom_right, rbottom_left)
{
    init();
}

Button_impl::Button_impl(Widget_ptr img, const ustring & label):
    Button_base_impl(img, label)
{
    init();
}

Button_impl::Button_impl(Widget_ptr img, const ustring & label, unsigned radius):
    Button_base_impl(img, label, radius)
{
    init();
}

Button_impl::Button_impl(Widget_ptr img, const ustring & label, unsigned rtop_left, unsigned rtop_right, unsigned rbottom_right, unsigned rbottom_left):
    Button_base_impl(img, label, rtop_left, rtop_right, rbottom_right, rbottom_left)
{
    init();
}

Button_impl::Button_impl(int icon_size, const ustring & icon_name):
    Button_base_impl(icon_size, icon_name)
{
    init();
}

Button_impl::Button_impl(int icon_size, const ustring & icon_name, unsigned radius):
    Button_base_impl(icon_size, icon_name, radius)
{
    init();
}

Button_impl::Button_impl(int icon_size, const ustring & icon_name, unsigned rtop_left, unsigned rtop_right, unsigned rbottom_right, unsigned rbottom_left):
    Button_base_impl(icon_size, icon_name, rtop_left, rtop_right, rbottom_right, rbottom_left)
{
    init();
}

Button_impl::Button_impl(const ustring & label, int icon_size, const ustring & icon_name):
    Button_base_impl(label, icon_size, icon_name)
{
    init();
}

Button_impl::Button_impl(const ustring & label, int icon_size, const ustring & icon_name, unsigned radius):
    Button_base_impl(label, icon_size, icon_name, radius)
{
    init();
}

Button_impl::Button_impl(const ustring & label, int icon_size, const ustring & icon_name, unsigned rtop_left, unsigned rtop_right, unsigned rbottom_right, unsigned rbottom_left):
    Button_base_impl(label, icon_size, icon_name, rtop_left, rtop_right, rbottom_right, rbottom_left)
{
    init();
}

Button_impl::Button_impl(Action & action, Action::Flags items):
    Button_base_impl(action, items)
{
    init();
    signal_click_.connect(fun(action, &Action::exec));
}

Button_impl::Button_impl(Action & action, unsigned radius, Action::Flags items):
    Button_base_impl(action, radius, items)
{
    init();
    signal_click_.connect(fun(action, &Action::exec));
}

Button_impl::Button_impl(Action & action, unsigned rtop_left, unsigned rtop_right, unsigned rbottom_right, unsigned rbottom_left, Action::Flags items):
    Button_base_impl(action, rtop_left, rtop_right, rbottom_right, rbottom_left, items)
{
    init();
    signal_click_.connect(fun(action, &Action::exec));
}

Button_impl::Button_impl(Action & action, int icon_size, Action::Flags items):
    Button_base_impl(action, icon_size, items)
{
    init();
    signal_click_.connect(fun(action, &Action::exec));
}

Button_impl::Button_impl(Action & action, int icon_size, unsigned radius, Action::Flags items):
    Button_base_impl(action, icon_size, radius, items)
{
    init();
    signal_click_.connect(fun(action, &Action::exec));
}

Button_impl::Button_impl(Action & action, int icon_size, unsigned rtop_left, unsigned rtop_right, unsigned rbottom_right, unsigned rbottom_left, Action::Flags items):
    Button_base_impl(action, icon_size, rtop_left, rtop_right, rbottom_right, rbottom_left, items)
{
    init();
    signal_click_.connect(fun(action, &Action::exec));
}

Button_impl::Button_impl(Master_action & action, Action::Flags items):
    Button_base_impl(action, items)
{
    init();
}

Button_impl::Button_impl(Master_action & action, unsigned radius, Action::Flags items):
    Button_base_impl(action, radius, items)
{
    init();
}

Button_impl::Button_impl(Master_action & action, unsigned rtop_left, unsigned rtop_right, unsigned rbottom_right, unsigned rbottom_left, Action::Flags items):
    Button_base_impl(action, rtop_left, rtop_right, rbottom_right, rbottom_left, items)
{
    init();
}

Button_impl::Button_impl(Master_action & action, int icon_size, Action::Flags items):
    Button_base_impl(action, icon_size, items)
{
    init();
}

Button_impl::Button_impl(Master_action & action, int icon_size, unsigned radius, Action::Flags items):
    Button_base_impl(action, icon_size, radius, items)
{
    init();
}

Button_impl::Button_impl(Master_action & action, int icon_size, unsigned rtop_left, unsigned rtop_right, unsigned rbottom_right, unsigned rbottom_left, Action::Flags items):
    Button_base_impl(action, icon_size, rtop_left, rtop_right, rbottom_right, rbottom_left, items)
{
    init();
}

void Button_impl::init() {
    connect_accel(space_accel_);
    connect_accel(enter_accel_);
    signal_mouse_leave().connect(fun(this, &Button_impl::on_mouse_leave), true);
    signal_disable_.connect(fun(this, &Button_impl::on_disable));
}

void Button_impl::click() {
    if (enabled()) {
        signal_click_();
    }
}

void Button_impl::set_repeat_delay(unsigned first, unsigned next) {
    if (0 != first) {
        repeat_delay_ = first;
        repeat_interval_ = 0 != next ? next : first;
    }
}

void Button_impl::set_action(Action & action, Action::Flags items) {
    Button_base_impl::init(action, Icon::DEFAULT, items);
}

void Button_impl::set_action(Action & action, int icon_size, Action::Flags items) {
    Button_base_impl::init(action, icon_size, items);
}

void Button_impl::select(Action & action) {
    action.signal_select().connect(bind_back(fun(this, &Button_impl::on_select), std::ref(action)));
    action.signal_unselect().connect(bind_back(fun(this, &Button_impl::on_unselect), std::ref(action)));
}

void Button_impl::on_select(Action & action) {
    if (action.enabled()) { thaw(); } else { freeze(); }
    if (!action.visible()) { disappear(); } else { appear(); }
    enable_cx_ = action.signal_enable().connect(fun(this, &Button_impl::thaw));
    disable_cx_ = action.signal_disable().connect(fun(this, &Button_impl::freeze));
    show_cx_ = action.signal_show().connect(fun(this, &Button_impl::appear));
    hide_cx_ = action.signal_hide().connect(fun(this, &Button_impl::disappear));
    exec_cx_ = signal_click_.connect(fun(action, &Action::exec));
}

void Button_impl::on_unselect(Action & action) {
    enable_cx_.drop(), disable_cx_.drop(), show_cx_.drop(), hide_cx_.drop(), exec_cx_.drop();
    freeze();
}

bool Button_impl::on_keyboard_activate() {
    hide_tooltip();
    press();
    timer_cx_ = timer_.connect(fun(this, &Button_impl::on_release_timeout));
    timer_.start(140);
    return true;
}

void Button_impl::on_release_timeout() {
    timer_.stop();
    timer_cx_.drop();
    on_release();
}

void Button_impl::on_repeat_timeout() {
    if (repeat_) {
        if (hover()) { timer_.restart(repeat_interval_); }
        click();
    }

    else {
        timer_.stop();
        timer_cx_.drop();
    }
}

void Button_impl::on_mouse_leave() {
    pressed_ = false;
}

// Overrides Button_base_impl.
void Button_impl::on_press() {
    if (repeat_) {
        timer_cx_.drop();
        timer_cx_ = timer_.connect(fun(this, &Button_impl::on_repeat_timeout));
        timer_.restart(repeat_delay_);
        click();
    }
}

void Button_impl::on_disable() {
    if (!enabled()) {
        timer_.stop();
        timer_cx_.drop();
        if (pressed_) { pressed_ = false; }
        redraw();
    }
}

void Button_impl::on_release() {
    pressed_ = false;
    redraw();
    if (!repeat_) { click(); }
}

// ----------------------------------------------------------------------------
// ----------------------------------------------------------------------------

Toggle_impl::Toggle_impl() {}

Toggle_impl::Toggle_impl(unsigned radius):
    Button_base_impl(radius)
{
}

Toggle_impl::Toggle_impl(unsigned rtop_left, unsigned rtop_right, unsigned rbottom_right, unsigned rbottom_left):
    Button_base_impl(rtop_left, rtop_right, rbottom_right, rbottom_left)
{
}

Toggle_impl::Toggle_impl(const ustring & label):
    Button_base_impl(label)
{
}

Toggle_impl::Toggle_impl(const ustring & label, unsigned radius):
    Button_base_impl(label, radius)
{
}

Toggle_impl::Toggle_impl(const ustring & label, unsigned rtop_left, unsigned rtop_right, unsigned rbottom_right, unsigned rbottom_left):
    Button_base_impl(label, rtop_left, rtop_right, rbottom_right, rbottom_left)
{
}

Toggle_impl::Toggle_impl(Widget_ptr img):
    Button_base_impl(img)
{
}

Toggle_impl::Toggle_impl(Widget_ptr img, unsigned radius):
    Button_base_impl(img, radius)
{
}

Toggle_impl::Toggle_impl(Widget_ptr img, unsigned rtop_left, unsigned rtop_right, unsigned rbottom_right, unsigned rbottom_left):
    Button_base_impl(img, rtop_left, rtop_right, rbottom_right, rbottom_left)
{
}

Toggle_impl::Toggle_impl(Widget_ptr img, const ustring & label):
    Button_base_impl(img, label)
{
}

Toggle_impl::Toggle_impl(Widget_ptr img, const ustring & label, unsigned radius):
    Button_base_impl(img, label, radius)
{
}

Toggle_impl::Toggle_impl(Widget_ptr img, const ustring & label, unsigned rtop_left, unsigned rtop_right, unsigned rbottom_right, unsigned rbottom_left):
    Button_base_impl(img, label, rtop_left, rtop_right, rbottom_right, rbottom_left)
{
}

Toggle_impl::Toggle_impl(int icon_size, const ustring & icon_name):
    Button_base_impl(icon_size, icon_name)
{
}

Toggle_impl::Toggle_impl(int icon_size, const ustring & icon_name, unsigned radius):
    Button_base_impl(icon_size, icon_name, radius)
{
}

Toggle_impl::Toggle_impl(int icon_size, const ustring & icon_name, unsigned rtop_left, unsigned rtop_right, unsigned rbottom_right, unsigned rbottom_left):
    Button_base_impl(icon_size, icon_name, rtop_left, rtop_right, rbottom_right, rbottom_left)
{
}

Toggle_impl::Toggle_impl(const ustring & label, int icon_size, const ustring & icon_name):
    Button_base_impl(label, icon_size, icon_name)
{
}

Toggle_impl::Toggle_impl(const ustring & label, int icon_size, const ustring & icon_name, unsigned radius):
    Button_base_impl(label, icon_size, icon_name, radius)
{
}

Toggle_impl::Toggle_impl(const ustring & label, int icon_size, const ustring & icon_name, unsigned rtop_left, unsigned rtop_right, unsigned rbottom_right, unsigned rbottom_left):
    Button_base_impl(label, icon_size, icon_name, rtop_left, rtop_right, rbottom_right, rbottom_left)
{
}

Toggle_impl::Toggle_impl(Toggle_action & action, Action::Flags items):
    Button_base_impl(action, items)
{
    signal_toggle_.connect(fun(action, &Toggle_action::set));
    setup(action.get());
    action.signal_setup().connect(fun(this, &Toggle_impl::setup));
}

Toggle_impl::Toggle_impl(Toggle_action & action, unsigned radius, Action::Flags items):
    Button_base_impl(action, radius, items)
{
    signal_toggle_.connect(fun(action, &Toggle_action::set));
    setup(action.get());
    action.signal_setup().connect(fun(this, &Toggle_impl::setup));
}

Toggle_impl::Toggle_impl(Toggle_action & action, unsigned rtop_left, unsigned rtop_right, unsigned rbottom_right, unsigned rbottom_left, Action::Flags items):
    Button_base_impl(action, rtop_left, rtop_right, rbottom_right, rbottom_left, items)
{
    signal_toggle_.connect(fun(action, &Toggle_action::set));
    setup(action.get());
    action.signal_setup().connect(fun(this, &Toggle_impl::setup));
}

Toggle_impl::Toggle_impl(Toggle_action & action, int icon_size, Action::Flags items):
    Button_base_impl(action, icon_size, items)
{
    signal_toggle_.connect(fun(action, &Toggle_action::set));
    setup(action.get());
    action.signal_setup().connect(fun(this, &Toggle_impl::setup));
}

Toggle_impl::Toggle_impl(Toggle_action & action, int icon_size, unsigned radius, Action::Flags items):
    Button_base_impl(action, icon_size, radius, items)
{
    signal_toggle_.connect(fun(action, &Toggle_action::set));
    setup(action.get());
    action.signal_setup().connect(fun(this, &Toggle_impl::setup));
}

Toggle_impl::Toggle_impl(Toggle_action & action, int icon_size, unsigned rtop_left, unsigned rtop_right, unsigned rbottom_right, unsigned rbottom_left, Action::Flags items):
    Button_base_impl(action, icon_size, rtop_left, rtop_right, rbottom_right, rbottom_left, items)
{
    signal_toggle_.connect(fun(action, &Toggle_action::set));
    setup(action.get());
    action.signal_setup().connect(fun(this, &Toggle_impl::setup));
}

Toggle_impl::Toggle_impl(Master_action & action, Action::Flags items):
    Button_base_impl(action, items)
{
}

Toggle_impl::Toggle_impl(Master_action & action, unsigned radius, Action::Flags items):
    Button_base_impl(action, radius, items)
{
}

Toggle_impl::Toggle_impl(Master_action & action, unsigned rtop_left, unsigned rtop_right, unsigned rbottom_right, unsigned rbottom_left, Action::Flags items):
    Button_base_impl(action, rtop_left, rtop_right, rbottom_right, rbottom_left, items)
{
}

Toggle_impl::Toggle_impl(Master_action & action, int icon_size, Action::Flags items):
    Button_base_impl(action, icon_size, items)
{
}

Toggle_impl::Toggle_impl(Master_action & action, int icon_size, unsigned radius, Action::Flags items):
    Button_base_impl(action, icon_size, radius, items)
{
}

Toggle_impl::Toggle_impl(Master_action & action, int icon_size, unsigned rtop_left, unsigned rtop_right, unsigned rbottom_right, unsigned rbottom_left, Action::Flags items):
    Button_base_impl(action, icon_size, rtop_left, rtop_right, rbottom_right, rbottom_left, items)
{
}

void Toggle_impl::setup(bool state) {
    if (state_ != state) {
        state_ = pressed_ = state;
        redraw();
    }
}

void Toggle_impl::set(bool state) {
    if (state_ != state) {
        setup(state);
        signal_toggle_(state);
    }
}

void Toggle_impl::toggle() {
    set(!state_);
}

void Toggle_impl::set_action(Toggle_action & action, Action::Flags items) {
    init(action, Icon::DEFAULT, items);
}

void Toggle_impl::set_action(Toggle_action & action, int icon_size, Action::Flags items) {
    init(action, icon_size, items);
}

void Toggle_impl::select(Toggle_action & action) {
    action.signal_select().connect(bind_back(fun(this, &Toggle_impl::on_select), std::ref(action)));
    action.signal_unselect().connect(bind_back(fun(this, &Toggle_impl::on_unselect), std::ref(action)));
}

void Toggle_impl::on_select(Toggle_action & action) {
    if (action.enabled()) { thaw(); } else { freeze(); }
    if (!action.visible()) { disappear(); } else { appear(); }
    enable_cx_ = action.signal_enable().connect(fun(this, &Toggle_impl::thaw));
    disable_cx_ = action.signal_disable().connect(fun(this, &Toggle_impl::freeze));
    show_cx_ = action.signal_show().connect(fun(this, &Toggle_impl::appear));
    hide_cx_ = action.signal_hide().connect(fun(this, &Toggle_impl::disappear));
    toggle_cx_ = signal_toggle_.connect(fun(action, &Toggle_action::set));
}

void Toggle_impl::on_unselect(Toggle_action & action) {
    enable_cx_.drop(), disable_cx_.drop(), show_cx_.drop(), hide_cx_.drop(), toggle_cx_.drop();
    freeze();
}

void Toggle_impl::on_release() {
    pressed_ = !state_;
    state_ = pressed_;
    redraw();
    signal_toggle_(state_);
}

} // namespace tau

//END

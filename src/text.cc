// ----------------------------------------------------------------------------
// SPDX-License-Identifier: BSD-2-Clause
// Copyright © 2014-2024 Konstantin Shmelkov <mrcashe@gmail.com>.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
// THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
// PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
// CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
// EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
// PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
// OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
// WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
// OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
// EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// ----------------------------------------------------------------------------

#include <tau/text.hh>
#include <text-impl.hh>

namespace tau {

#define TEXT_IMPL (std::static_pointer_cast<Text_impl>(impl))

Text::Text():
    Label(std::make_shared<Text_impl>())
{
}

Text::Text(const Text & other):
    Label(other.impl)
{
}

Text & Text::operator=(const Text & other) {
    Label::operator=(other);
    return *this;
}

Text::Text(Text && other):
    Label(other.impl)
{
}

Text & Text::operator=(Text && other) {
    Label::operator=(other);
    return *this;
}

Text::Text(Align halign, Align valign):
    Label(std::make_shared<Text_impl>(halign, valign))
{
}

Text::Text(const ustring & s, Align halign, Align valign):
    Label(std::make_shared<Text_impl>(s, halign, valign))
{
}

Text::Text(const std::u32string & ws, Align halign, Align valign):
    Label(std::make_shared<Text_impl>(ws, halign, valign))
{
}

Text::Text(Buffer buf, Align halign, Align valign):
    Label(std::make_shared<Text_impl>(buf, halign, valign))
{
}

Text::Text(Widget_ptr wp):
    Label(std::dynamic_pointer_cast<Text_impl>(wp))
{
}

Text & Text::operator=(Widget_ptr wp) {
    Label::operator=(std::dynamic_pointer_cast<Text_impl>(wp));
    return *this;
}

void Text::allow_select() {
    TEXT_IMPL->allow_select();
}

void Text::disallow_select() {
    TEXT_IMPL->disallow_select();
}

bool Text::select_allowed() const noexcept {
    return TEXT_IMPL->select_allowed();
}

bool Text::has_selection() const noexcept {
    return TEXT_IMPL->has_selection();
}

std::pair<Buffer_citer, Buffer_citer> Text::current() const {
    return TEXT_IMPL->current();
}

ustring Text::selection() const {
    return TEXT_IMPL->selection();
}

std::u32string Text::wselection() const {
    return TEXT_IMPL->wselection();
}

void Text::select(Buffer_citer b, Buffer_citer e) {
    TEXT_IMPL->select(b, e);
}

void Text::select(std::size_t row1, std::size_t col1, std::size_t row2, std::size_t col2) {
    TEXT_IMPL->select(row1, col1, row2, col2);
}

void Text::select_all() {
    TEXT_IMPL->select_all();
}

void Text::unselect() {
    TEXT_IMPL->unselect();
}

void Text::move_to(const Buffer_citer pos) {
    TEXT_IMPL->move_to(pos);
}

void Text::move_to(std::size_t ln, std::size_t pos) {
    TEXT_IMPL->move_to(ln, pos);
}

Buffer_citer Text::caret() const {
    return TEXT_IMPL->caret();
}

void Text::enable_caret() {
    TEXT_IMPL->enable_caret();
}

void Text::disable_caret() {
    TEXT_IMPL->disable_caret();
}

bool Text::caret_enabled() const noexcept {
    return TEXT_IMPL->caret_enabled();
}

Action & Text::action_previous() {
    return TEXT_IMPL->action_previous();
}

Action & Text::action_select_previous() {
    return TEXT_IMPL->action_select_previous();
}

Action & Text::action_next() {
    return TEXT_IMPL->action_next();
}

Action & Text::action_select_next() {
    return TEXT_IMPL->action_select_next();
}

Action & Text::action_previous_line() {
    return TEXT_IMPL->action_previous_line();
}

Action & Text::action_select_previous_line() {
    return TEXT_IMPL->action_select_previous_line();
}

Action & Text::action_next_line() {
    return TEXT_IMPL->action_next_line();
}

Action & Text::action_select_next_line() {
    return TEXT_IMPL->action_select_next_line();
}

Action & Text::action_previous_word() {
    return TEXT_IMPL->action_previous_word();
}

Action & Text::action_select_previous_word() {
    return TEXT_IMPL->action_select_previous_word();
}

Action & Text::action_next_word() {
    return TEXT_IMPL->action_next_word();
}

Action & Text::action_select_next_word() {
    return TEXT_IMPL->action_select_next_word();
}

Action & Text::action_home() {
    return TEXT_IMPL->action_home();
}

Action & Text::action_select_home() {
    return TEXT_IMPL->action_select_home();
}

Action & Text::action_end() {
    return TEXT_IMPL->action_end();
}

Action & Text::action_select_to_eol() {
    return TEXT_IMPL->action_select_to_eol();
}

Action & Text::action_sof() {
    return TEXT_IMPL->action_sof();
}

Action & Text::action_select_to_sof() {
    return TEXT_IMPL->action_select_to_sof();
}

Action & Text::action_eof() {
    return TEXT_IMPL->action_eof();
}

Action & Text::action_select_to_eof() {
    return TEXT_IMPL->action_select_to_eof();
}

Action & Text::action_previous_page() {
    return TEXT_IMPL->action_previous_page();
}

Action & Text::action_next_page() {
    return TEXT_IMPL->action_next_page();
}

Action & Text::action_select_previous_page() {
    return TEXT_IMPL->action_select_previous_page();
}

Action & Text::action_select_next_page() {
    return TEXT_IMPL->action_select_next_page();
}

Action & Text::action_select_all() {
    return TEXT_IMPL->action_select_all();
}

Action & Text::action_copy() {
    return TEXT_IMPL->action_copy();
}

Action & Text::action_cancel() {
    return TEXT_IMPL->action_cancel();
}

signal<void()> & Text::signal_caret_motion() {
    return TEXT_IMPL->signal_caret_motion();
}

signal<void()> & Text::signal_selection_changed() {
    return TEXT_IMPL->signal_selection_changed();
}

signal<void()> & Text::signal_click() {
    return TEXT_IMPL->signal_click();
}

} // namespace tau

//END

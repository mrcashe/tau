// ----------------------------------------------------------------------------
// SPDX-License-Identifier: BSD-2-Clause
// Copyright © 2014-2024 Konstantin Shmelkov <mrcashe@gmail.com>.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
// THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
// PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
// CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
// EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
// PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
// OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
// WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
// OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
// EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// ----------------------------------------------------------------------------

#include <tau/entry.hh>
#include <entry-impl.hh>

namespace tau {

#define ENTRY_IMPL (std::static_pointer_cast<Entry_impl>(impl))

Entry::Entry(Border border_style):
    Widget(std::make_shared<Entry_impl>(border_style))
{
}

Entry::Entry(const Entry & other):
    Widget(other.impl)
{
}

Entry & Entry::operator=(const Entry & other) {
    Widget::operator=(other);
    return *this;
}

Entry::Entry(Entry && other):
    Widget(other.impl)
{
}

Entry & Entry::operator=(Entry && other) {
    Widget::operator=(other);
    return *this;
}

Entry::Entry(Widget_ptr wp):
    Widget(std::dynamic_pointer_cast<Entry_impl>(wp))
{
}

Entry & Entry::operator=(Widget_ptr wp) {
    Widget::operator=(std::dynamic_pointer_cast<Entry_impl>(wp));
    return *this;
}

Entry::Entry(Align text_align, Border border_style):
    Widget(std::static_pointer_cast<Widget_impl>(std::make_shared<Entry_impl>(text_align, border_style)))
{
}

Entry::Entry(const ustring & s, Border border_style):
    Widget(std::static_pointer_cast<Widget_impl>(std::make_shared<Entry_impl>(s, border_style)))
{
}

Entry::Entry(const std::u32string & ws, Border border_style):
    Widget(std::static_pointer_cast<Widget_impl>(std::make_shared<Entry_impl>(ws, border_style)))
{
}

Entry::Entry(const ustring & s, Align text_align, Border border_style):
    Widget(std::static_pointer_cast<Widget_impl>(std::make_shared<Entry_impl>(s, text_align, border_style)))
{
}

Entry::Entry(const std::u32string & ws, Align text_align, Border border_style):
    Widget(std::static_pointer_cast<Widget_impl>(std::make_shared<Entry_impl>(ws, text_align, border_style)))
{
}

void Entry::allow_edit() {
    ENTRY_IMPL->allow_edit();
}

void Entry::disallow_edit() {
    ENTRY_IMPL->disallow_edit();
}

bool Entry::editable() const noexcept {
    return ENTRY_IMPL->editable();
}

void Entry::set_border_style(Border bs) {
    ENTRY_IMPL->set_border_style(bs);
}

Border Entry::border_style() const noexcept {
    return ENTRY_IMPL->border_style();
}

void Entry::text_align(Align align) {
    ENTRY_IMPL->text_align(align);
}

Align Entry::text_align() const noexcept {
    return ENTRY_IMPL->text_align();
}

void Entry::wrap(Wrap wmode) {
    ENTRY_IMPL->wrap(wmode);
}

Wrap Entry::wrap() const noexcept {
    return ENTRY_IMPL->wrap();
}

void Entry::assign(const ustring & s) {
    ENTRY_IMPL->assign(s);
}

void Entry::assign(const std::u32string & ws) {
    ENTRY_IMPL->assign(ws);
}

ustring Entry::str() const {
    return ENTRY_IMPL->str();
}

std::u32string Entry::wstr() const {
    return ENTRY_IMPL->wstr();
}

Size Entry::text_size(const ustring & s) {
    return ENTRY_IMPL->text_size(s);
}

bool Entry::empty() const noexcept {
    return ENTRY_IMPL->empty();
}

void Entry::clear() {
    ENTRY_IMPL->clear();
}

void Entry::select_all() {
    ENTRY_IMPL->select_all();
}

void Entry::select(std::size_t begin, std::size_t end) {
    ENTRY_IMPL->select(begin, end);
}

bool Entry::has_selection() const noexcept {
    return ENTRY_IMPL->has_selection();
}

void Entry::unselect() {
    ENTRY_IMPL->unselect();
}

void Entry::move_to(std::size_t col) {
    ENTRY_IMPL->move_to(col);
}

std::size_t Entry::caret() const {
    return ENTRY_IMPL->caret();
}

void Entry::append(Widget & w, bool shrink) {
    ENTRY_IMPL->append(w.ptr(), shrink);
}

Widget_ptr Entry::append(const ustring & text, unsigned margin_left, unsigned margin_right) {
    return ENTRY_IMPL->append(text, margin_left, margin_right);
}

Widget_ptr Entry::append(const ustring & text, const Color & color, unsigned margin_left, unsigned margin_right) {
    return ENTRY_IMPL->append(text, color, margin_left, margin_right);
}

void Entry::prepend(Widget & w, bool shrink) {
    ENTRY_IMPL->prepend(w.ptr(), shrink);
}

Widget_ptr Entry::prepend(const ustring & text, unsigned margin_left, unsigned margin_right) {
    return ENTRY_IMPL->prepend(text, margin_left, margin_right);
}

Widget_ptr Entry::prepend(const ustring & text, const Color & color, unsigned margin_left, unsigned margin_right) {
    return ENTRY_IMPL->prepend(text, color, margin_left, margin_right);
}

Action & Entry::action_cancel() {
    return ENTRY_IMPL->action_cancel();
}

Action & Entry::action_activate() {
    return ENTRY_IMPL->action_activate();
}

signal<void()> & Entry::signal_changed() {
    return ENTRY_IMPL->signal_changed();
}

signal<void(const std::u32string &)> & Entry::signal_activate() {
    return ENTRY_IMPL->signal_activate();
}

signal_all<const std::u32string &> & Entry::signal_validate() {
    return ENTRY_IMPL->signal_validate();
}

signal_all<const std::u32string &> & Entry::signal_approve() {
    return ENTRY_IMPL->signal_approve();
}

} // namespace tau

//END

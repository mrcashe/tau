// ----------------------------------------------------------------------------
// SPDX-License-Identifier: BSD-2-Clause
// Copyright © 2014-2024 Konstantin Shmelkov <mrcashe@gmail.com>.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
// THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
// PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
// CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
// EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
// PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
// OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
// WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
// OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
// EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// ----------------------------------------------------------------------------

#ifndef __TAU_FONT_SELECTOR_IMPL_HH__
#define __TAU_FONT_SELECTOR_IMPL_HH__

#include <tau/action.hh>
#include <tau/icon.hh>
#include <tau/timer.hh>
#include <gettext-impl.hh>
#include <table-impl.hh>

namespace tau {

class Fontsel_impl: public Table_impl {
public:

    Fontsel_impl();
    Fontsel_impl(std::string_view spec, const ustring & sample=ustring());

    void set_show_monospace_only(bool yes);
    bool monospace_only_visible() const;

    void select(std::string_view spec);
    ustring spec() const { return spec_; }

    void set_sample(const ustring & sample);
    ustring sample() const { return sample_; }

    Action & action_apply() { return apply_; }
    Action & action_cancel() { return cancel_; }

    signal<void(std::string_view)> & signal_selection_changed() { return signal_selection_changed_; }
    signal<void(std::string_view)> & signal_font_activated() { return signal_font_activated_; }

private:

    List_text_impl *        families_;
    List_text_impl *        faces_;
    Counter_impl *          counter_;
    Label_impl *            fontspec_;
    Label_impl *            psname_;
    Label_impl *            normal_;
    Label_impl *            mono_;
    Label_impl *            famlabel_;      // Family label widget.
    Entry_impl *            entry_;
    Check_impl *            monocheck_;
    Spinner_impl *          spinner_;
    unsigned                hsample_ = 0;
    ustring                 sample_;        // User defined sample string.
    ustring                 family_;
    ustring                 face_;
    ustring                 aspec_;         // Spec which was applied.
    ustring                 uspec_;         // Font specification defined by user.
    ustring                 uface_;         // Font face defined by user.
    ustring                 spec_;          // Currenttly selected specification.
    ustring                 ifamilies_;     // i18n'ed "Families" title.
    Timer                   apply_timer_;

    Action                  zin_            { "<Ctrl>= <Ctrl>+"_tu, "", "zoom-in", lgettext("Increase Font Size") };
    Action                  zout_           { "<Ctrl>-"_tu, "", "zoom-out", lgettext("Decrease Font Size") };
    Action                  cancel_         { "Escape Cancel", lgettext("Cancel"), "dialog-cancel"_tu };
    Action                  apply_          { KC_NONE, KM_NONE, lgettext("Apply"), "dialog-ok"_tu };

    signal<void(std::string_view)> signal_selection_changed_;
    signal<void(std::string_view)> signal_font_activated_;

private:

    void init();
    void update_families();
    void update_faces();
    void update_font();
    void update_entry();
    void update_tooltips();
    void enable_apply(bool yes);
    void reset_famlabel();

    void on_display();
    void on_family_selected(const ustring & str);
    void on_family_activated(const ustring & str);
    void on_face_selected(const ustring & str);
    void on_face_activated(const ustring & str);
    void on_counter_value_changed(double value);
    void on_sample_requisition_changed(Hints);
    void on_zin();
    void on_zout();
    void on_entry_changed();
    void on_entry_activate(const ustring & s);
    void on_apply();
    void on_monospace(bool yes);
    void on_next_family(const ustring & fam);
    void on_list_focus(Widget_impl * wp, bool in);
    void on_size_changed();

    void focus_next();
    void focus_previous();
};

} // namespace tau

#endif // __TAU_FONT_SELECTOR_IMPL_HH__

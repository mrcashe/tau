// ----------------------------------------------------------------------------
// SPDX-License-Identifier: BSD-2-Clause
// Copyright © 2014-2024 Konstantin Shmelkov <mrcashe@gmail.com>.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
// THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
// PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
// CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
// EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
// PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
// OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
// WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
// OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
// EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// ----------------------------------------------------------------------------

#ifndef __TAU_ABSOLUTE_IMPL_HH__
#define __TAU_ABSOLUTE_IMPL_HH__

#include <layered-container.hh>

namespace tau {

class Absolute_impl: public Layered_container {
public:

    Absolute_impl();
   ~Absolute_impl() { destroy(); }

   void put(Widget_ptr wp, const Point & pos, const Size & size=Size());
   void put(Widget_ptr wp, const Point & pos, unsigned width, unsigned height) { put(wp, pos, { width, height }); }
   void put(Widget_ptr wp, int x, int y, const Size & size=Size()) { put(wp, { x, y }, size); }
   void put(Widget_ptr wp, int x, int y, unsigned width, unsigned height) { put(wp, { x, y }, { width, height }); }

   void move(Widget_impl * wp, const Point & move_to);
   void move(Widget_impl * wp, int x, int y) { move(wp, { x, y }); }

   void move_rel(Widget_impl * wp, const Point & dpt);
   void move_rel(Widget_impl * wp, int dx, int dy) { move_rel(wp, { dx, dy }); }

   void resize(Widget_impl * wp, const Size & sz);
   void resize(Widget_impl * wp, unsigned width, unsigned height) { resize(wp, { width, height }); }

   void remove(Widget_impl * wp);
   void clear();
   bool empty() const noexcept { return holders_.empty(); }

   Container_impl * compound() noexcept override { return this; };
   const Container_impl * compound() const noexcept override { return this; };

private:

    struct Holder {
        Widget_impl *   wp;
        Point           pos;
        Size            size;
        connection      cx      { true };
    };

    using Holders = std::list<Holder>;
    using Hiter = Holders::iterator;

    Holders             holders_;

private:

    void arrange();
    void update_requisition();
    Size child_requisition(const Holder & hol);

    void on_hints(Hiter hi, Hints op);
    void on_hide(Widget_impl * wp);
    void on_show(Widget_impl * wp);
    bool on_take_focus();
};

} // namespace tau

#endif // __TAU_ABSOLUTE_IMPL_HH__

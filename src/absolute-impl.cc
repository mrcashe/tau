// ----------------------------------------------------------------------------
// SPDX-License-Identifier: BSD-2-Clause
// Copyright © 2014-2024 Konstantin Shmelkov <mrcashe@gmail.com>.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
// THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
// PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
// CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
// EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
// PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
// OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
// WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
// OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
// EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// ----------------------------------------------------------------------------

#include <absolute-impl.hh>
#include <algorithm>
#include <iostream>

namespace tau {

Absolute_impl::Absolute_impl() {
    signal_arrange_.connect(fun(this, &Absolute_impl::arrange));
    signal_display_in_.connect(fun(this, &Absolute_impl::update_requisition));
    signal_size_changed_.connect(fun(this, &Absolute_impl::arrange));
    signal_take_focus_.connect(fun(this, &Absolute_impl::on_take_focus));
}

void Absolute_impl::put(Widget_ptr wp, const Point & pos, const Size & size) {
    chk_parent(wp);
    update_child_bounds(wp.get(), INT_MIN, INT_MIN);
    auto hi = holders_.insert(holders_.end(), Holder());
    hi->wp = wp.get();
    hi->pos = pos;
    hi->size = size;
    hi->cx = wp->signal_hints_changed().connect(tau::bind_front(fun(this, &Absolute_impl::on_hints), hi));
    make_child(wp);
    update_requisition();
    queue_arrange();
}

void Absolute_impl::remove(Widget_impl * wp) {
    auto i = std::find_if(holders_.begin(), holders_.end(), [wp](auto & hol) { return wp == hol.wp; });

    if (i != holders_.end()) {
        std::list<Widget_impl *> v { wp };
        invalidate_children(v);
        holders_.erase(i);
        unparent(v);
        update_requisition();
        queue_arrange();
    }
}

void Absolute_impl::clear() {
    holders_.clear();
    unparent_all();
    update_requisition();
    invalidate();
}

void Absolute_impl::move(Widget_impl * wp, const Point & pos) {
    auto i = std::find_if(holders_.begin(), holders_.end(), [wp](auto & hol) { return wp == hol.wp; });

    if (i != holders_.end()) {
        i->pos = pos;
        Rect was(wp->origin(), wp->size());

        if (wp->update_origin(pos)) {
            Rect now(pos, wp->size());
            invalidate(was|now);
            update_requisition();
            queue_arrange();
        }
    }
}

void Absolute_impl::move_rel(Widget_impl * wp, const Point & offset) {
    move(wp, wp->origin()+offset);
}

void Absolute_impl::resize(Widget_impl * wp, const Size & size) {
    auto i = std::find_if(holders_.begin(), holders_.end(), [wp](auto & hol) { return wp == hol.wp; });

    if (i != holders_.end()) {
        i->size = size;
        Rect was(wp->origin(), wp->size());

        if (wp->update_size(size)) {
            Rect now(i->pos, wp->size());
            invalidate(was|now);
            update_requisition();
            queue_arrange();
        }
    }
}

void Absolute_impl::arrange() {
    Rect inval;

    for (auto & hol: holders_) {
        if (!hol.wp->hidden()) {
            Rect was(hol.wp->origin(), hol.wp->size()); was &= Rect(size());
            Size req = child_requisition(hol);
            bool changed = false;
            if (hol.wp->update_origin(hol.pos)) { changed = true; }
            if (hol.wp->update_size(req)) { changed = true; }
            if (changed) { inval |= (was|Rect(hol.pos, req)); }
        }
    }

    invalidate(inval);
}

Size Absolute_impl::child_requisition(const Holder & hol) {
    unsigned width = 0, height = 0;
    Size min = hol.wp->min_size_hint(), max = hol.wp->max_size_hint();

    if (0 != hol.size.width()) {
        width = hol.size.width();
    }

    else {
        width = hol.wp->size_hint().width();
        if (0 == width) { width = hol.wp->required_size().width(); }

        if (0 != width) {
            if (0 != min.width()) { width = std::max(width, min.width()); }
            if (0 != max.width()) { width = std::min(width, max.width()); }
        }
    }

    if (0 != hol.size.height()) {
        height = hol.size.height();
    }

    else {
        height = hol.wp->size_hint().height();
        if (0 == height) { height = hol.wp->required_size().height(); }

        if (0 != height) {
            if (0 != min.height()) { height = std::max(height, min.height()); }
            if (0 != max.height()) { height = std::min(height, max.height()); }
        }
    }

    return { width, height };
}

void Absolute_impl::update_requisition() {
    int xmax = 0, ymax = 0;

    for (auto & hol: holders_) {
        if (!hol.wp->hidden()) {
            Point bottom_right = hol.pos;
            Size req = child_requisition(hol);
            bottom_right.translate(req.iwidth(), req.iheight());
            xmax = std::max(xmax, bottom_right.x());
            ymax = std::max(ymax, bottom_right.y());
        }
    }

    require_size(xmax, ymax);
}

void Absolute_impl::on_hints(Hiter hi, Hints op) {
    if (!in_shutdown()) {
        if (Hints::SHOW == op) {
            update_requisition();
            queue_arrange();
        }

        else if (Hints::HIDE == op) {
            update_child_bounds(hi->wp, INT_MIN, INT_MIN);
            update_requisition();
            queue_arrange();
        }

        else {
            if (!hi->size) {
                if (child_requisition(*hi) != hi->wp->size()) {
                    update_requisition();
                }
            }
        }
    }
}

bool Absolute_impl::on_take_focus() {
    if (focused_child_ && focused_child_->take_focus()) { return true; }
    return std::any_of(holders_.begin(), holders_.end(), [](auto & hol) { return hol.wp->take_focus(); });
}

} // namespace tau

//END

// ----------------------------------------------------------------------------
// SPDX-License-Identifier: BSD-2-Clause
// Copyright © 2014-2024 Konstantin Shmelkov <mrcashe@gmail.com>.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
// THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
// PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
// CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
// EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
// PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
// OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
// WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
// OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
// EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// ----------------------------------------------------------------------------

/// @file container-impl.hh The Container_impl class declaration.
/// The implementation is in container-impl.cc file.

#ifndef __TAU_CONTAINER_IMPL_HH__
#define __TAU_CONTAINER_IMPL_HH__

#include <tau/action.hh>
#include <widget-impl.hh>
#include <algorithm>
#include <map>

namespace tau {

class Container_impl: public Widget_impl {
    friend Container;

public:

    ~Container_impl();

    // Overrides Widget_impl.
    bool handle_accel(char32_t kc, int km) override;

    // Overrides Widget_impl.
    bool handle_key_down(char32_t kc, int km) override;

    // Overrides Widget_impl.
    bool handle_key_up(char32_t kc, int km) override;

    // Overrides Widget_impl.
    bool handle_input(const ustring & s, int src) override;

    // Overrides Widget_impl.
    bool handle_mouse_down(int mbt, int mm, const Point & pt) override;

    // Overrides Widget_impl.
    bool handle_mouse_up(int mbt, int mm, const Point & pt) override;

    // Overrides Widget_impl.
    bool handle_mouse_double_click(int mbt, int mm, const Point & pt) override;

    // Overrides Widget_impl.
    void handle_mouse_motion(int mm, const Point & pt) override;

    // Overrides Widget_impl.
    void handle_mouse_enter(const Point & pt) override;

    // Overrides Widget_impl.
    void handle_mouse_leave() override;

    // Overrides Widget_impl.
    bool handle_mouse_wheel(int delta, int mm, const Point & pt) override;

    // Overridden by Popup_impl.
    // Overrides Widget_impl.
    Point to_parent(const Container_impl * ci, const Point & pt=Point()) const noexcept override;

    // Overridden by Window_impl.
    // Overrides Widget_impl.
    void set_cursor_up(Cursor_ptr cursor, Widget_impl * wip) override;

    // Overridden by Window_impl.
    // Overrides Widget_impl.
    void unset_cursor_up(Widget_impl * wip) override;

    // Overridden by Window_impl.
    // Overrides Widget_impl.
    void show_cursor_up() override;

    // Overridden by Window_impl.
    // Overrides Widget_impl.
    bool grab_modal_up(Widget_impl * caller) override;

    // Overridden by Window_impl.
    // Overrides Widget_impl.
    bool end_modal_up(Widget_impl * caller) override;

    // Overridden by Window_impl.
    // Overrides Widget_impl.
    int  grab_focus_up(Widget_impl * caller) override;

    // Overridden by Window_impl.
    // Overrides Widget_impl.
    void drop_focus_up(Widget_impl * caller) override;

    // Overrides Widget_impl.
    // Overridden by Window_impl.
    Window_impl * window() noexcept override;

    // Overrides Widget_impl.
    // Overridden by Window_impl.
    const Window_impl * window() const noexcept override;

    // Overrides Widget_impl.
    void resume_focus() override;

    // Overrides Widget_impl.
    void suspend_focus() override;

    // Overrides Widget_impl.
    void clear_focus() override;

    // Overrides Widget_impl.
    void clear_modal() override;

    // Overridden by Window_impl.
    bool grab_mouse_up(Widget_impl * wi) override;

    // Overridden by Window_impl.
    // Overrides Widget_impl.
    bool ungrab_mouse_up(Widget_impl * caller) override;

    // Overrides Widget_impl.
    // Overridden by Window_impl.
    bool grabs_mouse() const noexcept override;

    // Overrides Widget_impl.
    bool grabs_modal() const noexcept override;

    // Overrides Widget_impl.
    bool running() const noexcept override {
        return Widget_impl::running() || std::any_of(children_.begin(), children_.end(), [](auto wp) { return wp->running(); } );
    }

    // Overrides Widget_impl.
    signal<bool(int, int, Point)> & signal_mouse_down() override;

    // Overrides Widget_impl.
    signal<bool(int, int, Point)> & signal_mouse_double_click() override;

    // Overrides Widget_impl.
    signal<bool(int, int, Point)> & signal_mouse_up() override;

    // Overrides Widget_impl.
    signal<void(int, Point)> & signal_mouse_motion() override;

    // Overrides Widget_impl.
    signal<void(Point)> & signal_mouse_enter() override;

    // Overrides Widget_impl.
    signal<void()> & signal_mouse_leave() override;

    // Overrides Widget_impl.
    signal<bool(int, int, Point)> & signal_mouse_wheel() override;

    // Overrides Widget_impl.
    signal<bool(char32_t, int)> & signal_key_down() override;

    // Overrides Widget_impl.
    signal<bool(char32_t, int)> & signal_key_up() override;

    // Overrides Widget_impl.
    signal<bool(const ustring &, int)> & signal_input() override;

    void focus_after(Widget_impl * wp, Widget_impl * after_this) { set_focus_chain(wp, after_this, false); }
    void focus_before(Widget_impl * wp, Widget_impl * before_this) { set_focus_chain(wp, before_this, true); }
    void focus_after(Widget_ptr wp, Widget_impl * after_this) { set_focus_chain(wp.get(), after_this, false); }
    void focus_before(Widget_ptr wp, Widget_impl * before_this) { set_focus_chain(wp.get(), before_this, true); }
    void focus_after(Widget_impl * wp, Widget_ptr after_this) { set_focus_chain(wp, after_this.get(), false); }
    void focus_before(Widget_impl * wp, Widget_ptr before_this) { set_focus_chain(wp, before_this.get(), true); }
    void focus_after(Widget_ptr wp, Widget_ptr after_this) { set_focus_chain(wp.get(), after_this.get(), false); }
    void focus_before(Widget_ptr wp, Widget_ptr before_this) { set_focus_chain(wp.get(), before_this.get(), true); }
    void unchain_focus(Widget_impl * wp);

    void queue_arrange();
    void sync_arrange();
    void ungrab_mouse_down();

    // Overriden by all containers.
    virtual Container_impl * compound() noexcept = 0;

    // Overriden by all containers.
    virtual const Container_impl * compound() const noexcept = 0;

    // Overriden by Flat_container.
    // Overriden by Layered_container.
    virtual void on_child_viewable(Widget_impl * wp, bool yes) = 0;

    // Overriden by Flat_container.
    // Overriden by Layered_container.
    virtual void rise(Widget_impl * wp) = 0;

    // Overriden by Flat_container.
    // Overriden by Layered_container.
    virtual void lower(Widget_impl * wp) = 0;

    // Overriden by Flat_container.
    // Overriden by Layered_container.
    virtual int  level(const Widget_impl * wp) const noexcept = 0;

    const Widget_impl * focused_child() const noexcept { return focused_child_; }
    const Widget_impl * modal_child() const noexcept { return modal_child_; }
    const Widget_impl * mouse_grabber() const noexcept { return mouse_grabber_; }
    const Widget_impl * mouse_owner() const noexcept { return mouse_owner_; }

    // Overridden by Window_impl.
    virtual Widget_ptr focus_endpoint() noexcept;

    // Overridden by Window_impl.
    virtual Widget_cptr focus_endpoint() const noexcept;

    std::list<Widget_ptr> children() const;
    Widget_ptr chptr(Widget_impl * wp) noexcept;
    Widget_cptr chptr(const Widget_impl * wp) const noexcept;
    Widget & manage(Widget * w);

    Action & action_focus_next() { return action_focus_next_; }
    Action & action_focus_previous() { return action_focus_previous_; }

    signal<void()> & signal_arrange() { return signal_arrange_; }
    // TODO 0.8 public in facade.
    signal<void(Widget_ptr, int)> & signal_child() { return signal_child_; }
    // TODO 0.8 remove.
    signal<void()> & signal_children_changed() { return signal_children_changed_; }

protected:

    // Holder for sanitized widgets.
    struct San {
        Widget_ptr  wp;
        connection  cx  { true };        // connection to signal_parent().
    };

    using Widgets   = std::vector<Widget_ptr>;
    using Sans      = std::vector<San>;

    bool            arrange_: 1     = false;    // Arrange queued.
    bool            in_arrange_: 1  = false;    // In sync_arrange() now.
    Widget_impl *   focused_child_  = nullptr;
    Widget_impl *   modal_child_    = nullptr;
    Widget_impl *   mouse_grabber_  = nullptr;
    Widget_impl *   mouse_owner_    = nullptr;
    Widgets         children_;
    Sans            sanitize_;

    Action          action_focus_next_      { "Tab"_tu };
    Action          action_focus_previous_  { "<Shift>Tab <Shift>TabL"_tu };

    signal<void()>  signal_arrange_;
    signal<void()>  signal_children_changed_;
    signal<void(Widget_ptr, int)>   signal_child_;

protected:

    Container_impl();

    // Overrides Widget_impl.
    void on_tooltip_timer() override;

    // Overriden by Flat_container.
    // Overriden by Layered_container.
    virtual Widget_impl * mouse_target(const Point & pt) = 0;

    // Overridden by Window_impl.
    virtual void queue_arrange_up();

    void make_child(Widget_ptr wp);
    void unparent_all();
    std::size_t unparent(const std::list<Widget_impl *> & wps);
    void invalidate_children(const std::list<Widget_impl *> & wps);
    void chk_parent(Widget_ptr wp);
    void set_modal_child(Widget_impl * wp);
    bool update_child_bounds(Widget_impl * wp, const Rect & bounds);
    bool update_child_bounds(Widget_impl * wp, const Point & origin, const Size & sz=Size());
    bool update_child_bounds(Widget_impl * wp, int x, int y, const Size & sz=Size());
    bool update_child_bounds(Widget_impl * wp, int x, int y, unsigned w, unsigned h);
    void update_mouse_owner();
    void paint_children(Painter pr, const Rect & inval, Widget_impl ** wps, std::size_t nwps, void (Widget_impl::*method)(Painter, const Rect &));
    void sanitize(Widget_ptr wp);
    void sanitize(Widget * w);

private:

    // Managed facade holder.
    struct Facade {
        Widget *    w;
        connection  cx      { true };       // connection to signal_parent().
    };

    // Structure to hold focus chain data.
    struct Chain {
        Widget_impl *       wp;
        connection          in_cx           { true };
        connection          out_cx          { true };
        connection          unparent_cx     { true };
        connection          unchain_cx      { true };
    };

    using Chains            = std::map<Widget_impl *, Chain>;
    using Containers        = std::vector<Container_impl *>;
    using Facades           = std::vector<Facade>;

    Containers              containers_;    // Owning containers.
    Chains                  before_;
    Chains                  after_;
    Facades                 mans_;          // Managed facades.
    Facades                 facades_;       // Sanitizing facades.
    Widget_impl *           chain_          { nullptr };
    mutable Window_impl *   window_         { nullptr };
    connection              sanitize_cx_    { true };

private:

    Widget_impl * mouse_target_update(const Point & pt);
    void set_mouse_owner(Widget_impl * wi, const Point & pt=Point());

    void trigger_sanitize();
    void trigger_sanitize_if();

    bool on_mouse_down(int mbt, int mm, const Point & pt);
    bool on_mouse_up(int mbt, int mm, const Point & pt);
    bool on_mouse_double_click(int mbt, int mm, const Point & pt);
    void on_mouse_motion(int mm, const Point & pt);
    void on_mouse_enter(const Point & pt);
    void on_mouse_leave();
    bool on_mouse_wheel(int delta, int mm, const Point & pt);
    bool on_key_down(char32_t kc, int km);
    bool on_key_up(char32_t kc, int km);
    bool on_input(const ustring & s, int src);

    void on_show();
    void on_hide();
    void on_enable();
    void on_disable();
    void on_parent(Object * o);
    void on_display(bool in);
    void on_pdata();
    void on_sanitize();
    void on_sanitize_run();
    void on_managed_parent(Widget * w, Object * o);
    void on_san_parent(Widget_impl * wp, Object *o );

    // Focus chain management.
    void set_focus_chain(Widget_impl * wp, Widget_impl * other, bool before);
    void on_focus_chain(Widget_impl * wp, bool in, bool before);
    bool on_focus_next();
    bool on_focus_previous();
};

} // namespace tau

#endif // __TAU_CONTAINER_IMPL_HH__

// ----------------------------------------------------------------------------
// SPDX-License-Identifier: BSD-2-Clause
// Copyright © 2014-2024 Konstantin Shmelkov <mrcashe@gmail.com>.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
// THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
// PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
// CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
// EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
// PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
// OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
// WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
// OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
// EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// ----------------------------------------------------------------------------

/// @file entry-impl.cc The Entry_impl (wrapped Entry) class implementation.
/// Header is entry-impl.hh.

#include <tau/locale.hh>
#include <tau/string.hh>
#include <box-impl.hh>
#include <display-impl.hh>
#include <entry-impl.hh>
#include <loop-impl.hh>
#include <iostream>

namespace tau {

Entry_impl::Entry_impl(Border border_style):
    Frame_impl(border_style)
{
    init(Align::START);
}

Entry_impl::Entry_impl(Align text_align, Border border_style):
    Frame_impl(border_style)
{
    init(text_align);
}

Entry_impl::Entry_impl(const ustring & s, Border border_style):
    Frame_impl(border_style)
{
    init(Align::START);
    assign(s);
}

Entry_impl::Entry_impl(const std::u32string & ws, Border border_style):
    Frame_impl(border_style)
{
    init(Align::START);
    assign(ws);
}

Entry_impl::Entry_impl(const ustring & s, Align text_align, Border border_style):
    Frame_impl(border_style)
{
    init(text_align);
    assign(s);
}

Entry_impl::Entry_impl(const std::u32string & ws, Align text_align, Border border_style):
    Frame_impl(border_style)
{
    init(text_align);
    assign(ws);
}

void Entry_impl::init(Align text_align) {
    scroller_ = std::make_shared<Scroller_impl>();
    insert(scroller_);
    scroller_->action_pan_left().disable();
    scroller_->action_pan_right().disable();
    scroller_->action_pan_up().disable();
    scroller_->action_pan_down().disable();
    scroller_->action_previous_page().disable();
    scroller_->action_next_page().disable();
    scroller_->action_home().disable();
    scroller_->action_end().disable();

    edit_ = std::make_shared<Edit_impl>(buffer_, text_align, Align::CENTER);
    scroller_->insert(edit_);
    edit_->action_enter().disable();
    edit_->action_tab().disable();
    edit_->action_next_page().disable();
    edit_->action_previous_page().disable();
    edit_->action_next_line().disable();
    edit_->action_previous_line().disable();
    changed_cx_ = buffer_.signal_changed().connect(fun(this, &Entry_impl::on_buffer_changed));
    edit_->signal_focus_out().connect(fun(edit_, &Edit_impl::unselect));
    edit_->signal_mouse_down().connect(fun(this, &Entry_impl::on_edit_mouse_down), true);

    signal_take_focus().connect(fun(edit_, &Widget_impl::grab_focus), true);
    signal_focus_in().connect(fun(this, &Entry_impl::on_focus_in));
    signal_focus_out().connect(fun(this, &Entry_impl::on_focus_out));
    signal_mouse_double_click().connect(fun(this, &Entry_impl::on_mouse_double_click), true);
    signal_border_changed().connect(fun(this, &Entry_impl::on_border_changed));

    action_enter_.connect_before(fun(this, &Entry_impl::on_approve));
    action_enter_.connect_after(bind_return(fun(this, &Entry_impl::drop_focus), true));
    connect_action(action_enter_);
    action_cancel().connect_after(bind_return(fun(this, &Entry_impl::on_cancel), true));

    conf().signal_changed(Conf::RADIUS).connect(fun(this, &Entry_impl::on_conf_radius));
    signal_display_in_.connect(fun(this, &Entry_impl::on_conf_radius));

    edit_->conf().unredirect(Conf::BACKGROUND);
    scroller_->conf().redirect(Conf::BACKGROUND, Conf::WHITESPACE_BACKGROUND);
    on_border_changed();
}

Entry_impl::~Entry_impl() {
    if (signal_validate_) { delete signal_validate_; }
    if (signal_approve_) { delete signal_approve_; }
}

// Overrides Frame_impl.
void Entry_impl::set_border_style(Border bs) {
    if (Border::NONE != bs) { Frame_impl::set_border_style(bs); }
    else { set_border(0, bs, conf().integer(Conf::RADIUS)); }
}

void Entry_impl::assign(const ustring & s) {
    assign(std::u32string(s));
}

void Entry_impl::assign(const std::u32string & ws) {
    auto was = wstr();
    std::size_t eol = ws.find_first_of(Locale().newlines());
    auto sh = ws.npos != eol ? ws.substr(0, eol) : ws;

    if (approve(ws)) {
        changed_cx_.block();
        buffer_.assign(sh);
        shadow_ = sh;
        changed_cx_.unblock();
        if (focused()) { select_all(); }
        if (wstr() != was) { signal_changed_(); }    // FIXME about emission of this signal?
    }
}

void Entry_impl::allow_edit() {
    allow_focus();
    edit_->allow_edit();
}

void Entry_impl::disallow_edit() {
    edit_->disallow_edit();
    disallow_focus();
}

bool Entry_impl::editable() const noexcept {
    return edit_->editable();
}

void Entry_impl::text_align(Align align) {
    edit_->text_align(align, Align::CENTER);
}

Align Entry_impl::text_align() const noexcept {
    return edit_->text_align().first;
}

void Entry_impl::select(std::size_t begin, std::size_t end) {
    Buffer_citer b = buffer_.citer(0, begin), e = buffer_.citer(0, end);
    edit_->select(b, e);
}

void Entry_impl::unselect() {
    edit_->unselect();
}

void Entry_impl::move_to(std::size_t col) {
    edit_->move_to(0, col);
}

std::size_t Entry_impl::caret() const {
    return edit_->caret().col();
}

void Entry_impl::append(Widget_ptr wp, bool shrink) {
    init_box();
    box_->append(wp, shrink);
    wp->disallow_focus();
}

Widget_ptr Entry_impl::append(const ustring & text, unsigned margin_left, unsigned margin_right) {
    auto tp = std::make_shared<Label_impl>(text);
    tp->hint_margin_left(margin_left);
    tp->hint_margin_right(margin_right);
    append(tp, true);
    return tp;
}

Widget_ptr Entry_impl::append(const ustring & str, const Color & color, unsigned margin_left, unsigned margin_right) {
    auto tp = append(str, margin_left, margin_right);
    tp->conf().color(Conf::FOREGROUND) = color;
    return tp;
}

void Entry_impl::prepend(Widget_ptr wp, bool shrink) {
    init_box();
    box_->prepend(wp, shrink);
    wp->disallow_focus();
}

Widget_ptr Entry_impl::prepend(const ustring & str, unsigned margin_left, unsigned margin_right) {
    auto tp = std::make_shared<Label_impl>(str);
    tp->hint_margin_left(margin_left);
    tp->hint_margin_right(margin_right);
    prepend(tp, true);
    return tp;
}

Widget_ptr Entry_impl::prepend(const ustring & str, const Color & color, unsigned margin_left, unsigned margin_right) {
    auto tp = prepend(str, margin_left, margin_right);
    tp->conf().color(Conf::FOREGROUND) = color;
    return tp;
}

// FIXME blocked while assign()?
void Entry_impl::on_buffer_changed() {
    auto ws = buffer_.wstr();

    // Prevent newline insertion from the clipboard.
    auto nl = ws.find_first_of(Locale().wnewlines());
    bool cut = nl != ws.npos;
    if (cut) { ws.erase(nl); }

    if (signal_validate_ && !signal_validate_->operator()(ws)) {
        changed_cx_.block();
        std::size_t x = caret();
        buffer_.assign(shadow_);
        move_to(x);
        changed_cx_.unblock();
    }

    else {
        shadow_ = ws;

        if (cut) {
            changed_cx_.block();
            buffer_.assign(ws);
            changed_cx_.unblock();
        }

        signal_changed_();
    }
}

// Validate given string using probably installed validators.
bool Entry_impl::approve(const std::u32string & ws) {
    auto sig = signal_approve_ && !signal_approve_->empty() ? signal_approve_ : signal_validate_;
    if (sig) { return sig->operator()(ws); }
    return true;
}

// Connected using action_enter_.connect_before().
bool Entry_impl::on_approve() {
    return approve(buffer_.wstr());
}

// Connected using action_enter_.connect().
void Entry_impl::on_activate() {
    if (!signal_activate_.empty()) { signal_activate_(buffer_.wstr()); }
    buffer_.flush();
}

void Entry_impl::on_focus_in() {
    backup_ = buffer_.wstr();
    buffer_.flush();
    if (!has_selection()) { select_all(); }

    // In case focus was gained by this->grab_focus() edit is not focused.
    // Also possible we gained the focus via entry_->grab_focus() call, so
    // wait some time and ensure the edit is focused by calling grab_focus() on it.
    if (auto dp = display()) {
        if (auto lp = dp->loop()) {
            lp->alarm(bind_void(fun(edit_, &Edit_impl::grab_focus)), 3);
        }
    }

    disallow_tooltip();
}

void Entry_impl::on_focus_out() {
    // if (buffer_.changed()) { action_cancel()(); }
    allow_tooltip();
}

void Entry_impl::on_cancel() {
    buffer_.assign(backup_);
    buffer_.flush();
    drop_focus();
}

bool Entry_impl::on_edit_mouse_down(int mbt, int mm, const Point & where) {
    if (MBT_LEFT == mbt) {
        if (has_selection()) {
            unselect();
            return true;
        }

        if (edit_->focusable() && !edit_->focused()) {
            return edit_->grab_focus();
        }

        if (edit_->select_allowed()) {
            edit_->select_all();
            return true;
        }
    }

    return false;
}

bool Entry_impl::on_mouse_double_click(int mbt, int mm, const Point & where) {
    unselect();
    action_enter_.exec();
    return true;
}

void Entry_impl::on_border_changed() {
    Widget_ptr wp = box_;
    if (!wp) { wp = scroller_; }
    wp->hint_margin(border_visible() ? 1 : 0);
}

void Entry_impl::on_conf_radius() {
    set_border_radius(conf().integer(Conf::RADIUS));
}

void Entry_impl::init_box() {
    if (!box_) {
        box_ = std::make_shared<Box_impl>(Orientation::RIGHT);
        box_->action_focus_next().disable();
        box_->action_focus_previous().disable();
        scroller_->conf().unredirect(Conf::BACKGROUND);
        box_->conf().redirect(Conf::BACKGROUND, Conf::WHITESPACE_BACKGROUND);
        insert(box_);
        scroller_->hint_margin(0);
        box_->append(scroller_);
    }
}

signal_all<const std::u32string &> & Entry_impl::signal_validate() {
    if (!signal_validate_) { signal_validate_ = new signal_all<const std::u32string &>; }
    return *signal_validate_;
}

signal_all<const std::u32string &> & Entry_impl::signal_approve() {
    if (!signal_approve_) { signal_approve_ = new signal_all<const std::u32string &>; }
    return *signal_approve_;
}

} // namespace tau

//END

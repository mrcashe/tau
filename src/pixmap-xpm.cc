// ----------------------------------------------------------------------------
// SPDX-License-Identifier: BSD-2-Clause
// Copyright © 2014-2024 Konstantin Shmelkov <mrcashe@gmail.com>.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
// THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
// PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
// CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
// EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
// PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
// OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
// WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
// OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
// EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// ----------------------------------------------------------------------------

#include <tau/buffer.hh>
#include <tau/color.hh>
#include <tau/sys.hh>
#include <tau/locale.hh>
#include <pixmap-impl.hh>
#include <cstring>
#include <fstream>
#include <iomanip>
#include <iostream>
#include <unordered_map>

namespace {

bool on_file_supported(const tau::ustring & path) {
    return tau::str_similar("xpm", tau::path_suffix(path));
}

tau::Pixmap_ptr load_xpm_from_buffer(tau::Buffer buf) {
    std::unordered_map<std::string, uint32_t> colors;
    std::string varname;                // variable name.
    std::size_t width = 0;              // image width.
    std::size_t height = 0;             // image height.
    std::size_t ncolors = 0;            // number of colors.
    std::size_t nchars = 0;             // number of characters per color.
    std::vector<uint32_t> pixels;       // actual pixels.
    std::size_t pindex = 0;             // pixel addition index.
    bool has_alpha = false;             // alpha channel found.
    tau::Buffer_citer cur = buf.cbegin();

    if (!cur.equals("/* XPM */")) { return nullptr; }

    for (; cur != buf.cend(); cur.forward_line()) {
        tau::Buffer_citer end(cur);
        end.move_to_eol();
        tau::ustring s = cur.str(end);

        if (0 == width && 0 == height) {
            tau::ustring::size_type asterisk = s.find('*'), left_square = s.find('['), right_square = s.find(']');

            if (tau::ustring::npos != asterisk && tau::ustring::npos != left_square && tau::ustring::npos != right_square) {
                if (asterisk < left_square && left_square < right_square) {
                    varname = tau::str_trim(s.substr(1+asterisk, left_square-asterisk-1));
                }
            }

            else {
                tau::ustring::size_type open = s.find('"');

                if (tau::ustring::npos != open) {
                    tau::ustring::size_type close = s.find('"', 1+open);

                    if (tau::ustring::npos != close) {
                        auto v = tau::str_explode(tau::str_trim(s.substr(1+open, close-open-1)), ' ');

                        if (4 == v.size()) {
                            try {
                                width = std::stoi(v[0].raw(), nullptr, 0);
                                height = std::stoi(v[1].raw(), nullptr, 0);
                                ncolors = std::stoi(v[2].raw(), nullptr, 0);
                                nchars = std::stoi(v[3].raw(), nullptr, 0);
                                pixels.assign(width*height, 0);
                            }

                            catch (std::exception & x) {
                                std::cerr << "** load_xpm_from_buffer(): std::exception caught: " << x.what() << std::endl;
                                return nullptr;
                            }
                        }
                    }
                }
            }
        }

        else if (colors.size() < ncolors) {
            tau::ustring::size_type open = s.find('"');

            if (tau::ustring::npos != open) {
                tau::ustring::size_type close = s.find('"', 1+open);

                if (tau::ustring::npos != close) {
                    tau::ustring ss = s.substr(1+open, close-open-1);
                    auto v = tau::str_explode(tau::str_trim(ss), tau::Locale().blanks());

                    if (tau::char32_isblank(*ss.begin())) {
                        v.insert(v.begin(), tau::ustring(nchars, ' '));
                    }

                    if (3 == v.size()) {
                        if (v[0].size() == nchars && "c" == v[1]) {
                            uint32_t w = 0;

                            if ('#' == v[2][0]) {
                                try {
                                    w = std::stoi(v[2].raw().substr(1), nullptr, 16);
                                }

                                catch (std::exception & x) {
                                    std::cerr << "** load_xpm_from_buffer(): std::exception caught: " << x.what() << std::endl;
                                }

                                w |= 0xff000000;
                            }

                            else if (str_similar(v[2], "none")) {
                                has_alpha = true;
                            }

                            colors[v[0]] = w;
                        }
                    }
                }
            }
        }

        else if (0 != pixels.size()) {
            tau::ustring::size_type open = s.find('"');

            if (tau::ustring::npos != open) {
                tau::ustring::size_type close = s.find('"', 1+open);

                if (tau::ustring::npos != close) {
                    if (close-open-1 == width*nchars) {
                        ++open;

                        for (std::size_t n = 0; n < width; ++n, open += nchars) {
                            tau::ustring val(s.substr(open, nchars));
                            uint32_t px = colors[val];
                            pixels[pindex++] = px;
                        }
                    }
                }
            }
        }
    }

    if (0 != width && 0 != height && pindex == width*height) {
        tau::Pixmap_ptr pix;
        tau::Size sz(width, height);
        int depth = has_alpha ? 32 : (2 == ncolors ? 1 : 24);
        pix = tau::Pixmap_impl::create(depth, sz);
        pindex = 0;

        for (std::size_t y = 0; y < height; ++y) {
            for (std::size_t x = 0; x < width; ++x) {
                uint32_t w = pixels[pindex++];
                pix->put_pixel(tau::Point(x, y), tau::Color::from_argb32(w));
            }
        }

        return pix;
    }

    return nullptr;
}

tau::Pixmap_ptr load_xpm_from_memory(const char * mem, std::size_t nbytes) {
    if (nbytes >= 9 && 0 == std::memcmp(mem, "/* XPM */", 9)) {
        return load_xpm_from_buffer(tau::Buffer(mem));
    }

    return nullptr;
}

tau::Pixmap_ptr load_xpm_from_file(const tau::ustring & path) {
    tau::Buffer buf = tau::Buffer::load_from_file(path);
    return load_xpm_from_buffer(buf);
}

tau::Pixmap_ptr load_xpm_from_memory_u(const uint8_t * mem, std::size_t nbytes) {
    return load_xpm_from_memory((const char *)mem, nbytes);
}

} // anonymous namespace

namespace tau {

void pixmap_xpm_init() {
    Pixmap_impl::signal_file_supported().connect(fun(on_file_supported));
    Pixmap_impl::signal_load_from_file().connect(fun(load_xpm_from_file));
    Pixmap_impl::signal_load_from_memory().connect(fun(load_xpm_from_memory_u));
}

} // namespace tau

//END

// ----------------------------------------------------------------------------
// SPDX-License-Identifier: BSD-2-Clause
// Copyright © 2014-2024 Konstantin Shmelkov <mrcashe@gmail.com>.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
// THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
// PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
// CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
// EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
// PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
// OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
// WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
// OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
// EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// ----------------------------------------------------------------------------

#include <tau/encoding.hh>
#include <tau/exception.hh>
#include <tau/locale.hh>
#include <tau/string.hh>
#include <gettext-impl.hh>
#include <algorithm>
#include <bit>
#include <cstring>
#include <iomanip>
#include <memory>
#include <map>
#include <iostream>

namespace tau {

bool char32_is_delimiter(char32_t uc) {
    return std::u32string::npos != Locale().wdelimiters().find(uc);
}

bool char32_is_newline(char32_t uc) {
    return std::u32string::npos != Locale().wnewlines().find(uc);
}

bool char32_isblank(char32_t uc) {
    return std::u32string::npos != Locale().wblanks().find(uc);
}

bool char32_is_zerowidth(char32_t uc) {
    static const std::u32string s = U"\u00AD\u034F\u200B\u200C\u200D\u200E\u200F"
                                    "\u2028\u202A\u202B\u202C\u202D\u202E\u2060"
                                    "\u2061\u2062\u2063\uFEFF";
    return s.npos != s.find(uc);
}

bool str_has_prefix(const ustring & str, const ustring & prefix, bool similar) {
    if (prefix.size() > str.size()) { return false; }
    return similar ? str_similar(prefix, str.substr(0, prefix.size())) : 0 == str.compare(0, prefix.size(), prefix);
}

bool str_has_suffix(const ustring & str, const ustring & suffix, bool similar) {
    if (suffix.size() > str.size()) { return false; }
    return similar ? str_similar(suffix, str.substr(str.size()-suffix.size())) : 0 == str.compare(str.size()-suffix.size(), suffix.size(), suffix);
}

char32_t char32_toupper(char32_t uc) {
    if (0x0061 <= uc && uc <= 0x007a) return uc-0x0020;
    if (0x00e0 <= uc && uc <= 0x00f6) return uc-0x0020;
    if (0x00f8 <= uc && uc <= 0x00fe) return uc-0x0020;
    if (0x0101 <= uc && uc <= 0x0147 && 0 != (uc & 1)) return --uc;
    if (0x014b <= uc && uc <= 0x0177 && 0 != (uc & 1)) return --uc;
    if (0x017a <= uc && uc <= 0x017e && 0 == (uc & 1)) return --uc;
    if (0x0450 <= uc && uc <= 0x045f) return uc-0x0050;
    if (0x0430 <= uc && uc <= 0x044f) return uc-0x0020;
    if (0x0461 <= uc && uc <= 0x0481 && 0 != (uc & 1)) return --uc;
    if (0x048b <= uc && uc <= 0x04bf && 0 != (uc & 1)) return --uc;
    if (0x04c2 <= uc && uc <= 0x04ce && 0 == (uc & 1)) return --uc;
    return uc;
}

char32_t char32_tolower(char32_t uc) {
    if (0x0041 <= uc && uc <= 0x005a) return uc+0x0020;
    if (0x00c0 <= uc && uc <= 0x00d6) return uc+0x0020;
    if (0x00d8 <= uc && uc <= 0x00de) return uc+0x0020;
    if (0x0100 <= uc && uc <= 0x0146 && 0 == (uc & 1)) return ++uc;
    if (0x014a <= uc && uc <= 0x0176 && 0 == (uc & 1)) return ++uc;
    if (0x0179 <= uc && uc <= 0x017d && 0 != (uc & 1)) return ++uc;
    if (0x0400 <= uc && uc <= 0x040f) return uc+0x0050;
    if (0x0410 <= uc && uc <= 0x042f) return uc+0x0020;
    if (0x0460 <= uc && uc <= 0x0480 && 0 == (uc & 1)) return ++uc;
    if (0x048a <= uc && uc <= 0x04be && 0 == (uc & 1)) return ++uc;
    if (0x04c1 <= uc && uc <= 0x04cd && 0 != (uc & 1)) return ++uc;
    return uc;
}

/// Check is key code is a control code, not alpha-numeric.
bool char32_is_control(char32_t uc) {
    if (uc <  0x00000020) return true;
    if (uc == 0x0000007f) return true;
    if (uc <  0x00000080) return false;
    if (uc <  0x000000a0) return true;

    static const std::u32string ctrls = U"\u034f\u200b\u200c\u200d\u200e\u200f\u2028\u2029\u202a\202b\u202c\u202d\u202e\u2060";
    if (ctrls.npos != ctrls.find(uc)) { return true; }

    if (uc <  0x0000fe01) return false;
    if (uc <  0x0000fe10) return true;
    if (uc == 0x0000feff) return true;
    if (uc == 0x0000fffd) return true;
    if (uc <  0x000e0100) return false;
    if (uc <  0x000e01f0) return true;
    if (uc <  0x00110000) return false;
    return true;
}

bool char32_is_unicode(char32_t uc) {
    if (char32_is_control(uc)) return false;
    if (uc < 0x00110000) return true;
    return false;
}

bool char32_is_modifier(char32_t uc) {
    if (uc < 0x02b0) return false;
    if (uc < 0x0370) return true;
    if (uc < 0x0483) return false;
    if (uc < 0x048a) return true;
    if (uc < 0x1dc0) return false;
    if (uc < 0x1de7) return true;
    if (uc < 0x1dfc) return false;
    if (uc < 0x1e00) return true;
    if (uc < 0x20d0) return false;
    if (uc < 0x20f1) return true;

    return false;
}

bool char16_is_surrogate(char16_t uc) {
    return (uc >= u'\xdc00' && uc <= u'\xdfff') || (uc >= u'\xd800' && uc <= u'\xdbff');
}

void char32_to_surrogate(char32_t c32, char16_t & c1, char16_t & c2) {
    if (c32 <= U'\x0000ffff') {
        c1 = c32;
        c2 = 0;
    }

    else if (c32 <= U'\x10ffff') {
        c32 -= U'\x10000';
        c1 = u'\xd800'+(c32 >> 10);
        c2 = u'\xdc00'+(c32 & 0x3ff);
    }

    else {
        c1 = c2 = u'\0';
    }
}

char32_t char32_from_surrogate(char16_t c1, char16_t c2) {
    char32_t cc = U'\x000000';

    if (char16_is_surrogate(c1) && char16_is_surrogate(c2)) {
        if (c1 >= u'\xdc00') { std::swap(c1, c2); }
        cc = c1-u'\xd800';
        cc <<= 10;
        cc += c2-U'\xdc00'+U'\x10000';
    }

    return cc;
}

// Legal utf-8 byte sequence
// http://www.unicode.org/versions/Unicode6.0.0/ch03.pdf - page 94
//
//  Code Points         1st       2s       3s       4s
// U+000000..U+00007F   00..7F
// U+000080..U+0007FF   C2..DF   80..BF
// U+000800..U+000FFF   E0       A0..BF   80..BF
// U+001000..U+00CFFF   E1..EC   80..BF   80..BF
// U+00D000..U+00D7FF   ED       80..9F   80..BF
// U+00E000..U+00FFFF   EE..EF   80..BF   80..BF
// U+010000..U+03FFFF   F0       90..BF   80..BF   80..BF
// U+040000..U+0FFFFF   F1..F3   80..BF   80..BF   80..BF
// U+100000..U+10FFFF   F4       80..8F   80..BF   80..BF
//
// 1  00000000 00000000 00000000 0xxxxxxx
// 2  00000000 00000000 00000xxx xxxxxxxx
// 3  00000000 00000000 xxxxxxxx xxxxxxxx
// 4  00000000 000xxxxx xxxxxxxx xxxxxxxx
std::size_t char32_len(char32_t wc) {
    if (0 == (wc & 0xffffff80)) { return 1; }
    if (0 == (wc & 0xfffff800)) { return 2; }
    if (0 == (wc & 0xffff0000)) { return 3; }
    return 4;
}

// (0) 00000000
// (1) 0aaaaaaa                                 0xxx xxxx
// (2) 110xxxxx 10xxxxxx                        1100 0000 - 1101 1111   -64     -95
// (3) 1110xxxx 10xxxxxx 10xxxxxx               1110 0000 - 1110 1111   -96     -111
// (4) 11110xxx 10xxxxxx 10xxxxxx 10xxxxxx      1111 0000 - 1111 0111   -112    -119
std::size_t utf8_charlen(char lead) {
    return std::clamp(std::countl_one(uint8_t(lead)), 1, 4);
}

const char * utf8_next(const char * p) {
    return p+utf8_charlen(*p);
}

ssize_t utf8_strlen(const char * s, ssize_t max_len) {
    ssize_t n = 0;
    bool unicode = false;

    if (max_len < 0) {
        while (*s) {
            std::size_t bytes = utf8_charlen(*s);
            if (bytes > 1) { unicode = true; }
            s = utf8_next(s);
            ++n;
        }
    }

    else {
        const char * p = s+max_len;

        while (*s && s < p) {
            std::size_t bytes = utf8_charlen(*s);
            if (bytes > 1) { unicode = true; }
            s = utf8_next(s);
            ++n;
        }
    }

    return unicode ? n : -n;
}

std::u32string utf8_to_utf32(const char * s, ssize_t len) {
    ssize_t nchars = utf8_strlen(s, len);
    std::u32string result;

    if (0 != nchars) {
        result.resize(std::abs(nchars));

        for (std::size_t i = 0; i < result.size(); ++i) {
            if (nchars > 0) {
                result[i] = char32_from_pointer(s);
                s = utf8_next(s);
            }

            else {
                result[i] = uint8_t(*s++);
            }
        }
    }

    return result;
}

char32_t char32_from_pointer(const char * u) {
    unsigned n = utf8_charlen(*u);
    if (n < 2) { return *u; }
    char32_t wc = static_cast<uint8_t>(*u++ & ('\x7f' >> n));
    while (--n) { wc <<= 6; wc |= static_cast<uint8_t>('\x3f' & *u++); }
    return wc;
}

// Declared at ustring.hh.
char32_t char32_from_iterator(std::string::const_iterator pos) {
    char b[4];
    std::size_t len = utf8_charlen(*pos);
    for (std::size_t i = 0; i < len; ++i) { b[i] = *pos++; }
    return char32_from_pointer(b);
}

ustring str_toupper(const ustring & s) {
    ustring result;
    for (auto uc: s) { result += char32_toupper(uc); }
    return result;
}

std::u32string str_toupper(const std::u32string & str) {
    std::u32string ws(str);
    for (std::size_t i = 0; i < str.size(); ++i) { ws[i] = char32_toupper(ws[i]); }
    return ws;
}

ustring str_tolower(const ustring & s) {
    ustring result;
    for (auto uc: s) { result += char32_tolower(uc); }
    return result;
}

std::u32string str_tolower(const std::u32string & str) {
    std::u32string ws(str);
    for (std::size_t i = 0; i < str.size(); ++i) { ws[i] = char32_tolower(ws[i]); }
    return ws;
}

ustring str_trim(const ustring & s, const ustring & match) {
    ustring t = str_trimright(str_trimleft(s)), res;
    bool skip = true;

    for (char32_t wc: t) {
        if (s.npos == match.find(wc)) {
            skip = false;
            res += wc;
        }

        else {
            if (!skip) { res += U' '; }
            skip = true;
        }
    }

    return res;
}

ustring str_trim(const ustring & s) {
    return str_trim(s, Locale().blanks()+Locale().newlines());
}

std::u32string str_trim(const std::u32string & s, const std::u32string & match) {
    std::u32string t = str_trimright(str_trimleft(s, match), match), res;
    bool skip = true;

    for (auto wc: t) {
        if (s.npos == match.find(wc)) {
            skip = false;
            res += wc;
        }

        else {
            if (!skip) { res += U' '; }
            skip = true;
        }
    }

    return res;
}

std::u32string str_trim(const std::u32string & s) {
    return str_trim(s, Locale().wblanks()+Locale().wnewlines());
}

ustring str_trimright(const ustring & s, const ustring & match) {
    std::size_t pos = s.find_last_not_of(match);
    return s.npos == pos ? s : s.substr(0, pos+1);
}

ustring str_trimright(const ustring & s) {
    return str_trimright(s, Locale().blanks()+Locale().newlines());
}

std::u32string str_trimright(const std::u32string & s, const std::u32string & match) {
    std::size_t pos = s.find_last_not_of(match);
    return s.npos == pos ? s : s.substr(0, pos+1);
}

std::u32string str_trimright(const std::u32string & s) {
    return str_trimright(s, Locale().wblanks()+Locale().wnewlines());
}

ustring str_trimleft(const ustring & s, const ustring & match) {
    std::size_t pos = s.find_first_not_of(match);
    return s.npos == pos ? s : s.substr(pos);
}

ustring str_trimleft(const ustring & s) {
    return str_trimleft(s, Locale().blanks()+Locale().newlines());
}

std::u32string str_trimleft(const std::u32string & s, const std::u32string & match) {
    std::size_t pos = s.find_first_not_of(match);
    return s.npos == pos ? s : s.substr(pos);
}

std::u32string str_trimleft(const std::u32string & s) {
    return str_trimleft(s, Locale().wblanks()+Locale().wnewlines());
}

ustring str_trimboth(const ustring & s) {
    return str_trimleft(str_trimright(s));
}

ustring str_trimboth(const ustring & s, const ustring & match) {
    return str_trimleft(str_trimright(s, match), match);
}

std::u32string str_trimboth(const std::u32string  & s) {
    return str_trimleft(str_trimright(s));
}

std::u32string str_trimboth(const std::u32string  & s, const std::u32string & match) {
    return str_trimleft(str_trimright(s, match));
}

std::vector<ustring> str_explode(const ustring & str, const ustring & delimiters) {
    std::vector<ustring> v;
    std::size_t pos = 0, len = str.size(), del;

    while (pos < len) {
        while (pos < len && str.npos != delimiters.find(str[pos])) { ++pos; }
        del = str.find_first_of(delimiters, pos);

        if (str.npos != del) {
            if (pos < del) {
                v.emplace_back(str, pos, del-pos);
                pos = del;
            }
        }

        else {
            if (pos < len) {
                v.emplace_back(str, pos);
                pos = len;
            }
        }
    }

    return v;
}

std::vector<ustring> str_explode(const ustring & str, char32_t wc) {
    return str_explode(str, ustring(1, wc));
}

std::vector<ustring> str_explode(const ustring & str) {
    return str_explode(str, Locale().newlines()+Locale().blanks());
}

std::vector<std::u32string> str_explode(const std::u32string & str, const std::u32string & delimiters) {
    std::vector<std::u32string> v;
    std::size_t pos = 0, len = str.size(), del;

    while (pos < len) {
        while (pos < len && str.npos != delimiters.find(str[pos])) { ++pos; }
        del = str.find_first_of(delimiters, pos);

        if (str.npos != del) {
            if (pos < del) {
                v.emplace_back(str, pos, del-pos);
                pos = del;
            }
        }

        else {
            if (pos < len) {
                v.emplace_back(str, pos);
                pos = len;
            }
        }
    }

    return v;
}

std::vector<std::u32string> str_explode(const std::u32string & str, char32_t wc) {
    return str_explode(str, std::u32string(1, wc));
}

std::vector<std::u32string> str_explode(const std::u32string & str) {
    return str_explode(str, Locale().wnewlines()+Locale().wblanks());
}

ustring str_implode(const std::vector<ustring> & pieces, char32_t glue) {
    return str_implode(pieces, ustring(1, glue));
}

ustring str_implode(const std::vector<ustring> & pieces, const ustring & glue) {
    ustring result;

    for (std::size_t n = 0; n < pieces.size(); ) {
        result += pieces[n];
        if (++n != pieces.size()) { result += glue; }
    }

    return result;
}

bool str_similar(const ustring & s1, const ustring & s2) {
    auto blanks = Locale().blanks()+Locale().newlines()+"_-";
    auto t1 = str_trim(s1, blanks), t2 = str_trim(s2, blanks);
    return t1.size() == t2.size() && 0 == str_like(t1, t2);
}

bool str_similar(const ustring & s, const std::vector<ustring> & vars) {
    auto blanks = Locale().blanks()+Locale().newlines()+"_-";
    auto t = str_trim(s, blanks);

    return std::any_of(vars.begin(), vars.end(), [&t, &blanks](auto & u) {
        auto t2 = str_trim(u, blanks);
        return t.size() == t2.size() && 0 == str_like(t, t2);
    });
}

bool str_similar(const ustring & test, const ustring * vars, std::size_t array_size) {
    for (std::size_t i = 0; i < array_size; ++i) {
        if (!vars[i].empty()) {
            if (str_similar(test, vars[i])) {
                return true;
            }
        }
    }

    return false;
}

bool str_similar(const ustring & test, const ustring & vars, char32_t delimiter) {
    return U'\0' == delimiter ? str_similar(test, vars) : str_similar(test, str_explode(vars, delimiter));
}

bool str_similar(const ustring & test, const ustring & vars, const ustring & delimiters) {
    return str_similar(test, str_explode(vars, delimiters));
}

ustring str_bytes(uintmax_t nbytes, bool si) {
    return str_bytes(nbytes, std::locale(), si);
}

ustring str_bytes(uintmax_t nbytes, const std::locale & lc, bool si) {
    static const char * prefixes_si[5]  { gettext_noop("bytes"), gettext_noop("kB"),    gettext_noop("MB"),     gettext_noop("GB"),     gettext_noop("TB")  };
    static const char * prefixes[5]     { gettext_noop("bytes"), gettext_noop("KiB"),   gettext_noop("MiB"),    gettext_noop("GiB"),    gettext_noop("TiB") };
    const char ** p = si ? prefixes_si : prefixes;
    uintmax_t m = si ? 1000 : 1024, m1 = m, m2 = 1;
    if (nbytes < m1) { return str_format(lc, nbytes, ' ', lgettext(p[0])); }
    m1 *= m, m2 *= m; if (nbytes < m1) { return str_format(lc, std::fixed, std::setprecision(1), double(nbytes)/m2, ' ', lgettext(p[1])); }
    m1 *= m, m2 *= m; if (nbytes < m1) { return str_format(lc, std::fixed, std::setprecision(1), double(nbytes)/m2, ' ', lgettext(p[2])); }
    m1 *= m, m2 *= m; if (nbytes < m1) { return str_format(lc, std::fixed, std::setprecision(1), double(nbytes)/m2, ' ', lgettext(p[3])); }
    m2 *= m; return str_format(lc, std::fixed, std::setprecision(1), double(nbytes)/m2, ' ', lgettext(p[4]));
}

ustring str_unicode(char32_t wc) {
    static const char * u = "U+", * x = "0x";
    return str_format((char32_is_unicode(wc) ? u : x), std::setw(4), std::setfill('0'), std::hex, unsigned(wc));
}

char utf8_leader(std::size_t nbytes) {
    return (nbytes > 1 && nbytes < 7) ? '\xfe' << (7-nbytes) : 0;
}

bool str_is_numeric(const ustring & s, int base) {
    if (s.empty()) { return false; }
    constexpr const char * digits = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    ustring tmp(digits, std::max(2, std::min(36, base)));
    tmp += "+-"; if (10 == base) { tmp += '.'; tmp += Locale().mon_decimal_point(); }

    for (char32_t wc: str_toupper(s)) {
        if (ustring::npos == tmp.find(wc)) {
            return false;
        }
    }

    return true;
}

bool str_is_numeric(const std::u32string & ws, int base) {
    if (ws.empty()) { return false; }
    constexpr const char32_t * digits = U"0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    std::u32string tmp(digits, std::max(2, std::min(36, base)));
    tmp += U"+-"; if (10 == base) { tmp += U'.'; tmp += Locale().mon_decimal_point(); }

    for (char32_t wc: str_toupper(ws)) {
        if (std::u32string::npos == tmp.find(wc)) {
            return false;
        }
    }

    return true;
}

std::u32string str_replace(const std::u32string & str, char32_t match, char32_t replacement) {
    std::u32string res(str);

    for (std::size_t i = 0; i < res.size(); ++i) {
        if (match == res[i]) {
            res[i] = replacement;
        }
    }

    return res;
}

std::u32string str_replace(const std::u32string & str, char32_t match, const std::u32string & replacement) {
    std::u32string  res;
    std::size_t     i, j;

    for (i = 0; i < str.size(); i = j) {
        j = str.find(match, i);
        if (j == str.npos) { j = str.size(); }
        if (j > i) { res.append(str, i, j-i); }
        if (j < str.size()) { res += replacement; ++j; }
    }

    return res;
}

std::u32string str_replace(const std::u32string & str, const std::u32string & match_any, char32_t replacement) {
    std::u32string  res(str);

    for (std::size_t i = 0; i < res.size(); ++i) {
        if (match_any.npos != match_any.find(res[i])) {
            res[i] = replacement;
        }
    }

    return res;
}

std::u32string str_replace(const std::u32string & str, const std::u32string & match_any, const std::u32string & replacement) {
    std::u32string  res;
    std::size_t     i, j;

    for (i = 0; i < str.size(); i = j) {
        j = str.find_first_of(match_any, i);
        if (j == str.npos) { j = str.size(); }
        if (j > i) { res.append(str, i, j-i); }
        if (j < str.size()) { res += replacement; ++j; }
    }

    return res;
}

ustring str_replace(const ustring & str, char32_t match, char32_t replacement) {
    return str_replace(std::u32string(str), match, replacement);
}

ustring str_replace(const ustring & str, char32_t match, const ustring & replacement) {
    return str_replace(std::u32string(str), match, std::u32string(replacement));
}

ustring str_replace(const ustring & str, const ustring & match_any, char32_t replacement) {
    return str_replace(std::u32string(str), std::u32string(match_any), replacement);
}

ustring str_replace(const ustring & str, const ustring & match_any, const ustring & replacement) {
    return str_replace(std::u32string(str), std::u32string(match_any), std::u32string(replacement));
}

std::string str_escape(const std::string & str, const std::string & exceptions) {
    std::string res;

    for (char c: str) {
        if (str.npos == exceptions.find(c)) {
            switch (c) {
                case '\b': res += "\\b"; break;
                case '\f': res += "\\f"; break;
                case '\n': res += "\\n"; break;
                case '\r': res += "\\r"; break;
                case '\t': res += "\\t"; break;
                case '\v': res += "\\v"; break;
                case '\\': res += "\\\\"; break;
                case '"':  res += "\\\""; break;
                default:
                if (c < ' ' || c >= 0177) {
                    res += '\\';
                    res += char('0'+((c >> 6) & 07));
                    res += char('0'+((c >> 3) & 07));
                    res += char('0'+(c & 07));
                }

                else {
                    res += c;
                }
            }
        }

        else {
            res += c;
        }
    }

    return res;
}

std::string str_unescape(const std::string & str) {
    static const char * chars = "bfnrtv\\\"";
    static const char * trans = "\b\f\n\r\t\v\\\"";
    std::string res;
    std::size_t k, m, n = str.size();

    for (m = 0; m < n; m = k) {
        k = str.find('\\', m);
        if (k > m) { res.append(str, m, std::min(k, n)-m); }

        if (k < n && ++k < n) {
            if (auto p = std::strchr(chars, str[k])) {
                res += trans[p-chars];
                ++k;
            }

            else if ('X' == std::toupper(str[k])) {
                char c = str[++k] <= '9' ? str[k]-'0' : std::toupper(str[k])-55;
                c <<= 4; c += str[++k] <= '9' ? str[k]-'0' : std::toupper(str[k])-55;
                res += c;
                ++k;
            }

            else if ('0' <= str[k] && str[k] <= '7') {
                char c = str[k++]-'0';
                c <<= 3; c += str[k++]-'0';
                c <<= 3; c += str[k++]-'0';
                res += c;
            }

            else {
                res += str[k++];
            }
        }
    }

    return res;
}

bool str_not_empty(const std::u32string & s) noexcept {
    return !s.empty();
}

ustring::size_type str_like(const ustring & s, const ustring & match, std::size_t i) {
    if (s.empty() || match.empty() || i >= s.size()) { return ustring::npos; }
    auto ns = s.size(), nm = match.size(), n = ns+nm-i, p = i;
    if (nm > ns-i) { return ustring::npos; }

    if (n <= 8192) {
        // FIXME VLA -> std::vector<>?
        char32_t b[n];
        const char * c = s.c_str();
        for (std::size_t j = 0; j < i; ++j) { c = utf8_next(c); }
        for (; p < ns; ++p) { b[p-i] = char32_toupper(char32_from_pointer(c)); c = utf8_next(c); }
        c = match.c_str();
        for (n = 0; n < nm; ++n) { b[ns+n-i] = char32_toupper(char32_from_pointer(c)); c = utf8_next(c); }

        for (p = 0; p < ns-i; ++p) {
            if (nm > ns-p-i) { return ustring::npos; }
            for (n = 0; b[p+n] == b[ns+n-i] && n < nm && p+n < ns-i; ++n);
            if (n == nm) { return i+p; }
        }

        return ustring::npos;
    }

    return str_like(std::u32string(s), std::u32string(match), i);
}

std::u32string::size_type str_like(const std::u32string & s, const std::u32string & match, std::size_t i) {
    if (s.empty() || match.empty() || i >= s.size()) { return std::u32string::npos; }
    auto ns = s.size(), nm = match.size(), n = ns+nm-i, p = i;
    if (nm > ns-i) { return std::u32string::npos; }

    if (n <= 8192) {
        // FIXME VLA -> std::vector<>?
        char32_t b[n];
        for (; p < ns; ++p) { b[p-i] = char32_toupper(s[p]); }
        for (n = 0; n < nm; ++n) { b[ns+n-i] = char32_toupper(match[n]); }

        for (p = 0; p < ns-i; ++p) {
            if (nm > ns-p-i) { return std::u32string::npos; }
            for (n = 0; b[p+n] == b[ns+n-i] && n < nm && p+n < ns-i; ++n);
            if (n == nm) { return i+p; }
        }

        return std::u32string::npos;
    }

    return str_toupper(s).find(str_toupper(match), i);
}

std::vector<ustring> bool_words_ { "true", "yes", "y", "on", "false", "no", "n", "off", "none" };

bool str_boolean(const ustring & s) {
    static const std::vector<ustring> true_words_ { "true", "yes", "y", "on" };

    if (!str_similar(s, bool_words_)) {
        try { return 0 != std::stoll(s.raw(), nullptr, 0); }
        catch (...) { return false; }
    }

    return str_similar(s, true_words_);
}

std::u32string str_title(const std::u32string & s, const Locale & lc) {
    auto del = lc.wdelimiters();
    std::u32string res(s);
    std::size_t len = res.size(), pos = res.find_first_not_of(del);

    while (pos < len) {
        res[pos] = char32_toupper(res[pos]);
        pos = res.find_first_of(del, ++pos);
        if (pos < len) { pos = res.find_first_not_of(del, ++pos); }
    }

    return res;
}

ustring str_title(const ustring & s, const Locale & lc) {
    return str_title(std::u32string(s), lc);
}

} // namespace tau

//END

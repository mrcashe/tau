// ----------------------------------------------------------------------------
// SPDX-License-Identifier: BSD-2-Clause
// Copyright © 2014-2024 Konstantin Shmelkov <mrcashe@gmail.com>.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
// THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
// PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
// CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
// EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
// PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
// OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
// WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
// OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
// EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// ----------------------------------------------------------------------------

#include <tau/exception.hh>
#include <tau/object.hh>
#include <tau/sys.hh>
#include <loop-impl.hh>
#include <sys-impl.hh>
#include <algorithm>
#include <map>
#include <unordered_map>
#include <iostream>

namespace tau {

struct Object::Impl: public trackable {
    Object *            owner_;                 // Can not be nullptr!
    Object *            parent_ = nullptr;

    // Not-owning objects (raw pointers).
    using Objects = std::vector<Object *>;
    Objects             objects_;

    // Owning objects.
    using Shared = std::vector<Object_ptr>;
    Shared              shared_;
    Shared              sanitize_;

    // Foreign objects (raw pointers).
    // Those objects has parent different from our owner.
    Objects             foreign_;

    // Maps raw pointer to the object to its name.
    using Names = std::map<const Object *, std::string>;
    Names               names_;

    // Maps name to the raw pointer.
    using Pointers = std::unordered_map<std::string, Object *>;
    Pointers            pointers_;

    bool                destroy_ : 1    = false;
    bool                shutdown_: 1    = false;

    ustring             value_;

    signal<void()>      signal_destroy_;
    signal<void()>      signal_unparent_;
    signal<void(Object *)>  signal_parent_;
    signal<void(Object * current, Object * previous, const ustring & name)> * signal_link_ = nullptr;
    signal<const char *(const Object *)> *  signal_name_ = nullptr;

    // ------------------------------------------------------------------------
    // ------------------------------------------------------------------------

    Impl(Object * owner):
        owner_(owner)
    {
        signal_parent_.connect(fun(this, &Impl::on_parent));
        signal_unparent_.connect(fun(this, &Impl::on_unparent));
    }

    ~Impl() {
        destroy();
        if (signal_link_) { delete signal_link_; }
        if (signal_name_) { delete signal_name_; }
    }

    void destroy() {
        if (!destroy_) {
            destroy_ = true;
            shutdown(true);
            signal_destroy_();
        }
    }

    void shutdown(bool yes) {
        shutdown_ = yes;
        for (auto o: objects_) { o->shutdown(yes); }
        for (auto op: shared_) { op->shutdown(yes); }
    }

    std::size_t count_objects() const noexcept {
        return objects_.size()+shared_.size()+foreign_.size();
    }

    std::vector<Object *> objects() const {
        std::vector<Object *> v(count_objects());
        std::copy(objects_.begin(), objects_.end(), v.begin());
        std::copy(foreign_.begin(), foreign_.end(), v.begin()+objects_.size());
        std::transform(shared_.begin(), shared_.end(), v.begin()+objects_.size()+foreign_.size(), [](auto op) { return op.get(); } );
        return v;
    }

    // Link owning shared pointer.
    void link(Object_ptr op, const ustring & path) {
        auto v = path_explode(path);

        if (v.size() > 1 && "/" == v.front()) {
            if (parent_) { parent_->link(op, path); return; }
            v.erase(v.begin());
        }

        if (v.size() > 1) {
            if (auto co = lookup(v.front())) { co->link(op, path_implode({ ++v.begin(), v.end() }, '/')); return; }
            throw user_error(str_format(__func__, ": Object path lookup error: ", path));
        }

        bool had_parent = nullptr != op->parent();
        auto i = std::find(shared_.begin(), shared_.end(), op);

        if (shared_.end() == i) {
            std::erase(objects_, op.get());
            std::erase(foreign_, op.get());
            shared_.push_back(op);
            op->signal_parent()(owner_);
        }

        serve_name(had_parent, op.get(), v.empty() ? ustring() : v.back());
    }

    // Link not-owning raw pointer, maybe foreign.
    void link(Object * o, const ustring & path) {
        auto v = path_explode(path);

        if (v.size() > 1 && "/" == v.front()) {
            if (parent_) { parent_->link(o, path); return; }
            v.erase(v.begin());
        }

        if (v.size() > 1) {
            if (auto co = lookup(v.front())) { co->link(o, path_implode({ ++v.begin(), v.end() }, '/')); return; }
            throw user_error(str_format(__func__, ": Object path lookup error: ", path));
        }

        bool had_parent = nullptr != o->parent();

        if (!had_parent) {
            objects_.push_back(o);
            o->signal_parent()(owner_);
        }

        else if (o->parent() != owner_) {
            o->signal_parent().connect(bind_front(fun(this, &Impl::on_foreign_parent), o));
            foreign_.push_back(o);
        }

        serve_name(had_parent, o, v.empty() ? ustring() : v.back());
    }

    // Name related portion of link() methods.
    void serve_name(bool had_parent, Object * o, const ustring & name) {
        auto oname = name;
        if (name.empty() && signal_name_) { oname = signal_name_->operator()(o); }

        if (!oname.empty()) {
            auto i = pointers_.find(oname);

            // Name exists. Check name owner.
            if (i != pointers_.end()) {
                if (i->second != o) {
                    throw user_error(str_format("duplicate Object name ", oname));
                }

                // Renamed.
                if (i->first != oname) {
                    pointers_.erase(i);
                    pointers_[oname] = o;
                    names_[o] = oname;
                    if (signal_link_) { signal_link_->operator()(o, o, oname); }
                }
            }

            // New name.
            else {
                names_[o] = oname;
                pointers_[oname] = o;
                if (signal_link_) { signal_link_->operator()(o, had_parent ? o : nullptr, oname); }
            }
        }

        else {
            if (signal_link_) {
                auto i = names_.find(o);
                if (i != names_.end()) { signal_link_->operator()(o, had_parent ? o : nullptr, oname); }
                names_.erase(i);
            }

            else {
                names_.erase(o);
            }

            std::erase_if(pointers_, [o](auto & p) { return o == p.second; } );
        }
    }

    void unlink_all() {
        for (auto o: objects_) {
            o->signal_parent()(nullptr);

            if (!destroy_ && signal_link_) {   // FIXME shutdown_ ?
                ustring s;
                auto i = names_.find(o);
                if (i != names_.end()) { s = i->second; }
                signal_link_->operator()(nullptr, o, s);
            }
        }

        for (auto op: shared_) {
            op->signal_parent()(nullptr);

            if (!destroy_ && signal_link_) {   // FIXME shutdown_ ?
                ustring s;
                auto i = names_.find(op.get());
                if (i != names_.end()) { s = i->second; }
                signal_link_->operator()(nullptr, op.get(), s);
            }
        }

        if (!destroy_) {
            if (sanitize_.empty()) { Loop_impl::this_ptr()->alarm(fun(this, &Impl::on_sanitize), 11); }
            std::copy(shared_.begin(), shared_.end(), std::back_inserter(sanitize_));
        }

        shared_.clear();
        objects_.clear();
        names_.clear();
        pointers_.clear();
    }

    void unlink(Object * o) {
        bool yes = 0 != std::erase(objects_, o);

        if (!yes) {
            auto i = std::find_if(shared_.begin(), shared_.end(), [o](auto op) { return o == op.get(); } );
            yes = i != shared_.end();

            if (yes) {
                if (sanitize_.empty()) { Loop_impl::this_ptr()->alarm(fun(this, &Impl::on_sanitize), 11); }
                sanitize_.push_back(*i);
                shared_.erase(i);
            }
        }

        if (yes) {
            ustring s;
            auto i = names_.find(o);

            if (i != names_.end()) {
                s = i->second;
                pointers_.erase(s);
                names_.erase(i);
            }

            o->signal_parent()(nullptr);
            if (!destroy_ && signal_link_) { signal_link_->operator()(nullptr, o, s); }
        }
    }

    ustring name(const Object * o) const {
        auto i = names_.find(o);
        return i != names_.end() ? i->second : str_format(o).raw();
    }

    ustring epath() const {
        if (parent_) { return path_build(parent_->impl->epath(), parent_->impl->name(owner_), U'/'); }
        return "/";
    }

    Object * lookup(const ustring & p) {
        if (path_is_absolute(p)) {
            if (parent_) { return parent_->impl->lookup(p); }
            auto pv = path_explode(p);
            pv.erase(pv.begin());
            return pv.empty() ? owner_ : const_cast<Object *>(lookup(pv));
        }

        auto pv = path_explode(p);
        return const_cast<Object *>(lookup(pv));
    }

    const Object * lookup(const ustring & p) const {
        if (path_is_absolute(p)) {
            if (parent_) { return parent_->impl->lookup(p); }
            auto pv = path_explode(p);
            pv.erase(pv.begin());
            return pv.empty() ? owner_ : lookup(pv);
        }

        auto pv = path_explode(p);
        return lookup(pv);
    }

    const Object * lookup(std::vector<tau::ustring> & pv) const {
        if (!pv.empty()) {
            auto i = pointers_.find(pv.front());

            if (i != pointers_.end()) {
                pv.erase(pv.begin());
                return pv.empty() ? i->second : i->second->impl->lookup(pv);
            }
        }

        return nullptr;
    }

    void on_sanitize() {
        sanitize_.clear();
    }

    // Slot for our owner signal_parent().
    void on_parent(Object * parent) {
        auto was = parent_;
        parent_ = parent;
        if (!parent && was) { signal_unparent_(); }
    }

    void on_unparent() {
        for (auto o: objects_) { o->signal_unparent()(); }
        for (auto op: shared_) { op->signal_unparent()(); }
    }

    // Slot for foreign signal_parent().
    void on_foreign_parent(Object * o, Object * parent) {
        // Move to objects_ if became our child.
        if (parent && parent == owner_) {
            std::erase(foreign_, o);
            if (shared_.end() == std::find_if(shared_.begin(), shared_.end(), [o](auto op) { return o == op.get(); } )) { objects_.push_back(o); }
        }

        else if (!parent) {
            auto i = names_.find(o);
            if (i != names_.end()) { pointers_.erase(i->second); names_.erase(i); }
            std::erase(foreign_, o);
            std::erase(objects_, o);
            std::erase_if(shared_, [o](auto op) { return o == op.get(); } );
        }
    }

    signal<void(Object *, Object *, const ustring &)> & signal_link() {
        if (!signal_link_) { signal_link_ = new signal<void(Object *, Object *, const ustring &)>; }
        return *signal_link_;
    }

    signal<const char *(const Object *)> & signal_name() {
        if (!signal_name_) { signal_name_ = new signal<const char *(const Object *)>; }
        return *signal_name_;
    }
};

// ----------------------------------------------------------------------------
// ----------------------------------------------------------------------------

Object::Object():
    impl(new Impl(this))
{
}

Object::Object(const ustring & s):
    impl(new Impl(this))
{
    set_string(s);
}

Object::Object(bool yes):
    impl(new Impl(this))
{
    set_boolean(yes);
}

Object::Object(intmax_t i):
    impl(new Impl(this))
{
    set_integer(i);
}

Object::Object(double d):
    impl(new Impl(this))
{
    set_real(d);
}

Object::~Object() {
    delete impl;
}

void Object::link(Object * o, const ustring & name) {
    impl->link(o, name);
}

void Object::link(Object_ptr op, const ustring & name) {
    impl->link(op, name);
}

void Object::unlink(Object * o) {
    impl->unlink(o);
}

void Object::unlink_all() {
    impl->unlink_all();
}

void Object::destroy() {
    impl->destroy();
}

void Object::shutdown(bool yes) {
    impl->shutdown(yes);
}

std::size_t Object::count_objects() const noexcept {
    return impl->count_objects();
}

std::vector<Object *> Object::objects() const {
    return impl->objects();
}

ustring Object::name(const Object * o) const {
    return impl->name(o);
}

ustring Object::epath() const {
    return impl->epath();
}

Object * Object::lookup(const ustring & ep) {
    return impl->lookup(ep);
}

const Object * Object::lookup(const ustring & ep) const {
    return impl->lookup(ep);
}

bool Object::in_shutdown() const noexcept {
    return impl->shutdown_;
}

bool Object::in_destroy() const noexcept {
    return impl->destroy_;
}

Object * Object::parent() noexcept {
    return impl->parent_;
}

const Object * Object::parent() const noexcept {
    return impl->parent_;
}

Object_ptr Object::ptr(Object * o) {
    auto i = std::find_if(impl->shared_.begin(), impl->shared_.end(), [o](auto op) { return o == op.get(); } );
    return i != impl->shared_.end() ? *i : nullptr;
}

Object_cptr Object::ptr(const Object * o) const {
    auto i = std::find_if(impl->shared_.begin(), impl->shared_.end(), [o](auto op) { return o == op.get(); } );
    return i != impl->shared_.end() ? *i : nullptr;
}

std::vector<ustring> Object::names() const {
    std::vector<ustring> v(impl->pointers_.size());
    std::transform(impl->pointers_.begin(), impl->pointers_.end(), v.begin(), [](auto & p) { return p.first; } );
    return v;
}

void Object::set_string(const ustring & s) {
    impl->value_ = s;
}

ustring Object::get_string() const {
    return impl->value_;
}

void Object::set_boolean(bool yes) {
    impl->value_ = yes ? "true" : "false";
}

bool Object::get_boolean() const {
    return str_boolean(impl->value_);
}

void Object::set_integer(intmax_t i) {
    impl->value_ = str_format(i);
}

intmax_t Object::get_integer() const {
    return std::atoi(impl->value_.c_str());
}

void Object::set_real(double d) {
    impl->value_ = str_format(d);
}

double Object::get_real() const {
    return std::stod(impl->value_.raw());
}

signal<void(Object *, Object *, const ustring &)> & Object::signal_link() {
    return impl->signal_link();
}

signal<const char *(const Object *)> & Object::signal_name() {
    return impl->signal_name();
}

signal<void()> & Object::signal_unparent() {
    return impl->signal_unparent_;
}

signal<void(Object *)> & Object::signal_parent() {
    return impl->signal_parent_;
}

signal<void()> & Object::signal_destroy() {
    return impl->signal_destroy_;
}

} // namespace tau

//END

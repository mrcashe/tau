// ----------------------------------------------------------------------------
// SPDX-License-Identifier: BSD-2-Clause
// Copyright © 2014-2024 Konstantin Shmelkov <mrcashe@gmail.com>.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
// THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
// PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
// CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
// EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
// PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
// OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
// WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
// OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
// EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// ----------------------------------------------------------------------------

#ifndef __TAU_FONT_WIN_HH__
#define __TAU_FONT_WIN_HH__

#include <tau/timeval.hh>
#include <tau/signal.hh>
#include <font-impl.hh>
#include "defs-win.hh"

namespace tau {

struct Font_win: public Font_impl {
    HDC                         hdc_        = NULL;
    HFONT                       hfont_      = NULL;
    connection                  inv_cx_     { true };
    connection                  uncache_cx_ { true };
    mutable Timeval             tv_;
    mutable bool                ps_         = false;
    mutable ustring             psname_;

    Font_win(HDC hdc, unsigned dpi, const ustring & font_spec);
    Font_win(const Font_win & other) = delete;
    Font_win & operator=(const Font_win & other) = delete;
   ~Font_win();

    // Overrides Font_impl.
    ustring psname() const override;

    // Overrides Font_impl.
    Glyph_ptr glyph(char32_t wc) const override;

    // Overrides Font_impl.
    Glyph_ptr master_glyph(char32_t wc) const override;

    // Get TTF table by name.
    std::vector<char> table(std::string_view table_name) const;

    void invalidate();

    // Boot fonts on system startup.
    static void boot();

    // List available families.
    static std::vector<ustring> list_families();

    // List all available font faces for specified family.
    static std::vector<ustring> list_faces(const ustring & family);

    // Get "normal" font.
    static ustring normal();

    // Get "monospace" font.
    static ustring mono();

};

} // namespace tau

#endif // __TAU_FONT_WIN_HH__

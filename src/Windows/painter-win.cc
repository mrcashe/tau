// ----------------------------------------------------------------------------
// SPDX-License-Identifier: BSD-2-Clause
// Copyright © 2014-2024 Konstantin Shmelkov <mrcashe@gmail.com>.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
// THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
// PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
// CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
// EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
// PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
// OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
// WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
// OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
// EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// ----------------------------------------------------------------------------

#include <tau/font.hh>
#include <brush-impl.hh>
#include <glyph-impl.hh>
#include <pen-impl.hh>
#include "font-win.hh"
#include "display-win.hh"
#include "painter-win.hh"
#include "pixmap-win.hh"
#include "sys-win.hh"
#include "winface-win.hh"
#include <cmath>
#include <cstring>
#include <map>
#include <iostream>

namespace tau {

static const std::map<Oper, DWORD> rops_ = {
    { Oper::COPY,        SRCCOPY         },
    { Oper::CLEAR,       BLACKNESS       },
    { Oper::SET,         WHITENESS       }
};

Painter_win::Painter_win():
    Painter_impl(),
    dp_(Display_win::this_win_display().get())
{
    select_font_priv(Font::normal());
    dp_->loop()->signal_quit().connect(fun(this, &Painter_win::on_display_quit));
}

Painter_win::Painter_win(Winface_win * wf):
    Painter_impl(),
    dp_(Display_win::this_win_display().get()),
    hwnd_(wf->handle())
{
    hdc_ = GetDC(hwnd_);
    dpi_ = wf->dpi();
    setup();
    wstate().viewable_.set(wf->self()->size());
    wf->self()->signal_destroy().connect(fun(this, &Painter_win::on_window_destroy));
    dp_->loop()->signal_quit().connect(fun(this, &Painter_win::on_display_quit));
    update_clip_priv();
}

Painter_win::Painter_win(Winface_win * wf, PAINTSTRUCT * pstruct):
    Painter_impl(),
    dp_(Display_win::this_win_display().get())
{
    dpi_ = wf->dpi();
    wf->self()->signal_destroy().connect(fun(this, &Painter_win::on_window_destroy));
    dp_->loop()->signal_quit().connect(fun(this, &Painter_win::on_display_quit));
    select_font_priv(Font::normal());
    begin_paint(wf, pstruct);
}

Painter_win::Painter_win(Pixmap_win * pixmap):
    Painter_impl(),
    dp_(Display_win::this_win_display().get()),
    pixmap_(pixmap)
{
    if (pixmap_) {
        pixmap_->signal_destroy().connect(fun(this, &Painter_win::on_pixmap_destroy));
        dpi_ = Display_win::this_win_display()->dpi();
        wstate().viewable_.set(pixmap_->size());
    }

    dp_->loop()->signal_quit().connect(fun(this, &Painter_win::on_display_quit));
    select_font_priv(Font::normal());
    update_clip_priv();
}

Painter_win::~Painter_win() {
    done_dc();
}

void Painter_win::setup() {
    SetBkMode(hdc_, TRANSPARENT);
    SetMapMode(hdc_, MM_TEXT);
    SetTextAlign(hdc_, TA_BASELINE);
    select_font_priv(Font::normal());
}

void Painter_win::done_dc() {
    if (crgn_) {
        DeleteObject(crgn_);
        crgn_ = NULL;
    }

    if (hwnd_) {
        if (pstruct_) {
            EndPaint(hwnd_, pstruct_);
            pstruct_ = nullptr;
        }

        else if (hdc_) {
            ReleaseDC(hwnd_, hdc_);
        }

        hdc_ = NULL;
        hwnd_ = NULL;
    }
}

void Painter_win::begin_paint(Winface_win * wf, PAINTSTRUCT * pstruct) {
    done_dc();
    update_clip_priv();
    hdc_ = BeginPaint(wf->handle(), pstruct);

    if (hdc_) {
        pstruct_ = pstruct;
        hwnd_ = wf->handle();
        setup();
        wstate().viewable_.set(wf->self()->size());
    }
}

void Painter_win::end_paint() {
    done_dc();
}

DWORD Painter_win::rop(Oper op) {
    auto iter = rops_.find(op);
    return iter != rops_.end() ? iter->second : SRCCOPY;
}

void Painter_win::set_font_priv(Font_ptr fp) {
    if (hdc_ && fp) {
        if (auto wfp = std::dynamic_pointer_cast<Font_win>(fp)) {
            if (HFONT hfont = wfp->hfont_) {
                SelectFont(hdc_, hfont);
                state().font_ = wfp;
                state().fontspec_ = wfp->spec_;
            }
        }
    }
}

Font_ptr Painter_win::select_font_priv(const ustring & spec) {
    auto wfp = std::make_shared<Font_win>(hdc_, dpi_, spec);
    wfp->inv_cx_ = signal_invalidate_.connect(fun(wfp, &Font_win::invalidate));
    set_font_priv(wfp);
    return state().font_;
}

Vector Painter_win::text_size(const ustring & s) const {
    Vector v;

    if (hdc_ && !s.empty()) {
        SIZE wsize;
        GetTextExtentPoint32W(hdc_, std::wstring(s).data(), s.size(), &wsize);
        v.set(wsize.cx, wsize.cy);
    }

    return v;
}

// Overrides pure Painter_impl.
Vector Painter_win::text_size(const std::u32string & s) const {
    return text_size(ustring(s));
}

// Overrides pure Painter_impl.
std::vector<int> Painter_win::offsets(const ustring & s) const {
    std::vector<int> v(1+s.size());

    if (hdc_ && !s.empty()) {
        GCP_RESULTSW res;
        std::memset(&res, 0, sizeof(GCP_RESULTSW));
        res.lStructSize = sizeof(GCP_RESULTSW);
        res.lpCaretPos = v.data();
        res.nGlyphs = s.size();
        DWORD hw = GetCharacterPlacementW(hdc_, std::wstring(s).data(), s.size(), 0, &res, 0);
        v.back() = LOWORD(hw);
    }

    return v;
}

// Overrides pure Painter_impl.
std::vector<int> Painter_win::offsets(const std::u32string & s) const {
    return offsets(ustring(s));
}

// protected
// Overrides Painter_impl.
void Painter_win::stroke_prim_text(const Prim_text & p) {
    if (hdc_) {
        Point pt(matrix()*p.pos);
        pt -= woffset();
        SetTextColor(hdc_, p.color.bgr24());
        TextOutW(hdc_, pt.x(), pt.y(), p.str.data(), p.str.size());
    }
}

void Painter_win::set_clip() {
    if (hdc_) {
        if (!crgn_) { crgn_ = CreateRectRgn(cr_.left, cr_.top, cr_.right, cr_.bottom); }
        else { SetRectRgn(crgn_, cr_.left, cr_.top, cr_.right, cr_.bottom); }
        SelectClipRgn(hdc_, crgn_);
    }
}

void Painter_win::update_clip_priv() {
    cr_ = to_winrect(wstate().viewable_);
    set_clip();
}

// PS_SOLID         The pen is solid.
// PS_DASH          The pen is dashed. This style is valid only when the pen width is one or less in device units.
// PS_DOT           The pen is dotted. This style is valid only when the pen width is one or less in device units.
// PS_DASHDOT       The pen has alternating dashes and dots. This style is valid only when the pen width is one or less in device units.
// PS_DASHDOTDOT    The pen has alternating dashes and double dots. This style is valid only when the pen width is one or less in device units.
// PS_NULL          The pen is invisible.
// PS_INSIDEFRAME   The pen is solid. When this pen is used in any GDI drawing function that takes a bounding rectangle,
//                  the dimensions of the figure are shrunk so that it fits entirely in the bounding rectangle,
//                  taking into account the width of the pen. This applies only to geometric pens.
HPEN Painter_win::begin_stroke() {
    double dlw = state().pen_->line_width*matrix().max_factor();
    DWORD lw = dlw > 0.0 ? dlw : 1;
    DWORD style = dlw > 0.0 ? PS_GEOMETRIC : PS_COSMETIC;

    switch (state().pen_->line_style) {
        case Line::DASH:            style |= PS_DASH; break;
        case Line::DOT:             style |= (1 == lw ? PS_DOT : PS_DASHDOT); break;    // FIXME PS_DOT doesn't work with lw > 1?
        case Line::DASH_DOT:        style |= PS_DASHDOT; break;
        case Line::DASH_DOT_DOT:    style |= PS_DASHDOTDOT; break;
        default:                    style |= PS_SOLID;
    }

    switch (state().pen_->cap_style) {
        case Cap::FLAT:             style |= PS_ENDCAP_FLAT; break;
        case Cap::SQUARE:           style |= PS_ENDCAP_SQUARE; break;
        default:                    style |= PS_ENDCAP_ROUND;
    }

    switch (state().pen_->join_style) {
        case Join::BEVEL:           style |= PS_JOIN_BEVEL; break;
        case Join::MITER:           style |= PS_JOIN_MITER; break;
        default:                    style |= PS_JOIN_ROUND;
    }

    LOGBRUSH lb { BS_SOLID, state().pen_->color.bgr24() };
    HPEN pen = ExtCreatePen(style, lw, &lb, 0, NULL);
    SelectObject(hdc_, pen);
    SetROP2(hdc_, winrop(state().op_));
    return pen;
}

void Painter_win::stroke_prim_arc(const Prim_arc & o) {
    if (hdc_) {
        if (matrix().identity()) {
            Vector c = o.center-Vector(woffset().x(), woffset().y());
            int x1 = c.x()-o.radius;
            int y1 = c.y()-o.radius;
            int x2 = c.x()+o.radius;
            int y2 = c.y()+o.radius;
            int x3 = c.x()+o.radius*std::cos(o.angle1);
            int y3 = c.y()-o.radius*std::sin(o.angle1);
            int x4 = c.x()+o.radius*std::cos(o.angle2);
            int y4 = c.y()-o.radius*std::sin(o.angle2);

            HPEN hpen = begin_stroke();
            SetArcDirection(hdc_, o.angle2 < o.angle1 ? AD_CLOCKWISE: AD_COUNTERCLOCKWISE);
            Arc(hdc_, x1, y1, x2, y2, x3, y3, x4, y4);
            DeleteObject(hpen);
        }

        else {
            Painter_impl::stroke_prim_arc(o);
        }
    }
}

void Painter_win::fill_prim_arc(const Prim_arc & o) {
    if (hdc_) {
        if (matrix().identity()) {
            Vector c = o.center-Vector(woffset().x(), woffset().y());
            int x1 = c.x()-o.radius;
            int y1 = c.y()-o.radius;
            int x2 = c.x()+o.radius;
            int y2 = c.y()+o.radius;
            HBRUSH hbr = CreateSolidBrush(state().brush_->color().bgr24());
            HPEN pen = CreatePen(PS_NULL, 0, 0);
            SelectObject(hdc_, hbr);
            SelectObject(hdc_, pen);
            SetROP2(hdc_, winrop(state().op_));
            SetArcDirection(hdc_, o.angle2 < o.angle1 ? AD_CLOCKWISE: AD_COUNTERCLOCKWISE);

            // Chord.
            if (std::fabs(o.angle2-o.angle1) >= 2.0*std::numbers::pi) {
                Chord(hdc_, x1, y1, x2, y2, INT_MAX, INT_MAX, INT_MAX, INT_MAX);
            }

            else {
                int x3 = c.x()+o.radius*std::cos(o.angle1);
                int y3 = c.y()-o.radius*std::sin(o.angle1);
                int x4 = c.x()+o.radius*std::cos(o.angle2);
                int y4 = c.y()-o.radius*std::sin(o.angle2);

                if (o.pie) { Pie(hdc_, x1, y1, x2, y2, x3, y3, x4, y4); }
                else { Chord(hdc_, x1, y1, x2, y2, x3, y3, x4, y4); }
            }

            DeleteObject(pen);
            DeleteObject(hbr);
        }

        else {
            Painter_impl::fill_prim_arc(o);
        }
    }
}

// Overrides pure Painter_impl.
void Painter_win::stroke_polyline(const Point * pts, std::size_t npts) {
    if (hdc_ && npts > 1) {
        HPEN pen = begin_stroke();
        POINT apt[1+npts];
        std::size_t n = npts;
        for (POINT * pt = apt; n; --n, ++pt, ++pts) { *pt = to_winpoint(*pts); }
        Polyline(hdc_, apt, npts);
        DeleteObject(pen);
    }
}

// Overrides pure Painter_impl.
void Painter_win::stroke_rectangle(const Rect & r) {
    if (hdc_) {
        HPEN hpen = begin_stroke();
        HBRUSH brush = HBRUSH(GetStockObject(HOLLOW_BRUSH));
        RECT wr = to_winrect(r);
        SelectObject(hdc_, brush);
        Rectangle(hdc_, wr.left, wr.top, wr.right, wr.bottom);
        DeleteObject(brush);
        DeleteObject(hpen);
    }
}

// Overrides Painter_impl.
void Painter_win::fill_round_rectangle(const Prim_rect & o) {
    if (matrix().identity()) {
        HBRUSH hbr = CreateSolidBrush(state().brush_->color().bgr24());
        HPEN pen = CreatePen(PS_NULL, 0, 0);
        SelectObject(hdc_, hbr);
        SelectObject(hdc_, pen);
        SetROP2(hdc_, winrop(state().op_));
        RoundRect(hdc_, o.v1.x()-woffset().x(), o.v1.y()-woffset().y(), o.v2.x()-woffset().x(), o.v2.y()-woffset().y(), 2.0*o.radius, 2.0*o.radius);
        DeleteObject(pen);
        DeleteObject(hbr);
    }

    else {
        Painter_impl::fill_round_rectangle(o);
    }
}

// Overrides Painter_impl.
void Painter_win::stroke_curve(const Vector & start, const Curve & cv) {
    if (visible()) {
        if (matrix().identity()) {
            HPEN hpen = begin_stroke();

            if (3 == cv.order()) {
                const POINT pts[4] { { LONG(start.x()), LONG(start.y()) }, { LONG(cv.cp1().x()), LONG(cv.cp1().y()) }, { LONG(cv.cp2().x()), LONG(cv.cp2().y()) }, { LONG(cv.end().x()), LONG(cv.end().y()) } };
                PolyBezier(hdc_, pts, std::size(pts));
            }

            else if (2 == cv.order()) {
                const Vector cp1(start+(2.0/3.0)*(cv.cp()-start)), cp2(cv.end()+(2.0/3.0)*(cv.cp()-cv.end()));
                const POINT pts[4] { { LONG(start.x()), LONG(start.y()) }, { LONG(cp1.x()), LONG(cp1.y()) }, { LONG(cp2.x()), LONG(cp2.y()) }, { LONG(cv.end().x()), LONG(cv.end().y()) } };
                PolyBezier(hdc_, pts, std::size(pts));
            }

            else {
                MoveToEx(hdc_, start.x(), start.y(), NULL);
                LineTo(hdc_, cv.end().x(), cv.end().y());
            }

            DeleteObject(hpen);
        }

        else {
            Painter_impl::stroke_curve(start, cv);
        }
    }
}

void Painter_win::fill_rectangles(const Rect * rs, std::size_t nrs, const Color & c) {
    if (pixmap_) {
        pixmap_->fill_rectangles(rs, nrs, c);
    }

    else if (hdc_ && dp_ && nrs) {
        HBRUSH hbr = CreateSolidBrush(c.bgr24());
        SetROP2(hdc_, winrop(state().op_));

        for (; nrs--; ++rs) {
            if (*rs) {
                RECT rect = to_winrect(*rs);

                if (Oper::COPY == state().op_) {
                    FillRect(hdc_, &rect, hbr);
                }

                else {
                    HRGN rgn = CreateRectRgn(rect.left, rect.top, rect.right, rect.bottom);
                    FillRgn(hdc_, rgn, hbr);
                    DeleteObject(rgn);
                }
            }
        }

        DeleteObject(hbr);
    }
}

void Painter_win::fill_polygon(const Point * pts, std::size_t npts, const Color & color) {
    if (dp_ && hdc_ && npts >= 2) {
        HBRUSH hbr = CreateSolidBrush(color.bgr24());
        HPEN hpen = (HPEN)GetStockObject(NULL_PEN);
        SelectObject(hdc_, hbr);
        SelectObject(hdc_, hpen);
        SetROP2(hdc_, winrop(state().op_));
        POINT xps[npts];
        for (std::size_t n = 0; n < npts; ++n) { xps[n] = to_winpoint(pts[n]); }
        Polygon(hdc_, xps, npts);
        DeleteObject(hbr);
        DeleteObject(hpen);
    }
}

void Painter_win::draw_pixmap(Pixmap_cptr pix, const Point & pix_origin, const Size & pix_size, const Point & pt, bool transparent) {
    if (hdc_) {
        if (auto pix_ms = std::dynamic_pointer_cast<const Pixmap_win>(pix)) {
            if (HDC cdc = CreateCompatibleDC(hdc_)) {
                HBITMAP cbm = pix_ms->create_bitmap(hdc_);

                if (cbm) {
                    SelectObject(cdc, cbm);

                    if (transparent && 32 == pix->depth()) {
                        BLENDFUNCTION bf { AC_SRC_OVER, 0, 255, AC_SRC_ALPHA };
                        AlphaBlend(hdc_, pt.x(), pt.y(), pix_size.width(), pix_size.height(), cdc, pix_origin.x(), pix_origin.y(), pix_size.width(), pix_size.height(), bf);
                    }

                    else {
                        BitBlt(hdc_, pt.x(), pt.y(), pix_size.width(), pix_size.height(), cdc, pix_origin.x(), pix_origin.y(), rop(state().op_));
                    }
                }

                // Delete HDC before HBITMAP to prevent GDI resource leak!
                DeleteDC(cdc);
                if (cbm) { DeleteObject(cbm); }
            }
        }
    }
}

void Painter_win::on_window_destroy() {
    signal_invalidate_();
    state().font_.reset();
    state().fontspec_.clear();
    hdc_ = NULL;
    hwnd_ = NULL;
}

} // namespace tau

//END

// ----------------------------------------------------------------------------
// SPDX-License-Identifier: BSD-2-Clause
// Copyright © 2014-2024 Konstantin Shmelkov <mrcashe@gmail.com>.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
// THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
// PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
// CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
// EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
// PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
// OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
// WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
// OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
// EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// ----------------------------------------------------------------------------

#include "sys-win.hh"
#include <tau/exception.hh>
#include <tau/file.hh>
#include <tau/string.hh>
#include <tau/timeval.hh>
#include <locale-impl.hh>
#include <sys-impl.hh>
#include <cctype>
#include <cstring>
#include <filesystem>
#include <fstream>
#include <lmcons.h>
#include <shlwapi.h>
#include <iostream>

namespace {

tau::ustring root_dir() {
    wchar_t ww[MAX_PATH];
    if (GetWindowsDirectoryW(ww, MAX_PATH)) { return tau::ustring(ww); }
    return "C:\\";
}

} // anon namespace

namespace tau {

const char32_t keyfile_comment_separator_ = U';';
char32_t path_slash() noexcept { return U'\\'; }
char32_t path_sep() noexcept { return U';'; }

ustring special_folder(int csidl) {
    HRESULT hr;
    LPITEMIDLIST pidl = NULL;
    ustring result;

    hr = SHGetSpecialFolderLocation(NULL, csidl, &pidl);
    if (hr == S_OK) {
        wchar_t path[MAX_PATH+1];

        if (SHGetPathFromIDListW(pidl, path)) {
            result.assign(path);
        }

        CoTaskMemFree(pidl);
    }

    return result;
}

ustring ustr_error(DWORD error) {
    LPWSTR bufp = NULL;
    ustring result;

    FormatMessageW(FORMAT_MESSAGE_ALLOCATE_BUFFER|FORMAT_MESSAGE_FROM_SYSTEM,
                   NULL, error, MAKELANGID(LANG_ENGLISH, SUBLANG_ENGLISH_US),
                   (LPWSTR)&bufp, 0, NULL);

    if (*bufp) {
        result = str_trimright(bufp);
        LocalFree(bufp);
    }

    return result;
}

sys_error::sys_error(const ustring & extra_msg) {
    DWORD error = GetLastError();
    ustring err = ustr_error(error);

    if (!err.empty()) {
        gerror_ = error;
    }

    else {
        gerror_ = 0;
        err.assign("No error");
    }

    msg_ = str_format("system error, code is ", gerror_, ": ", err);
    if (!extra_msg.empty()) { msg_ += " ("+extra_msg+")"; }
}

struct tm Timeval::gmtime() const noexcept {
    time_t t(usec_/1000000);
    struct tm res;
    gmtime_s(&res, &t);
    return res;
}

struct tm Timeval::localtime() const noexcept {
    time_t t(usec_/1000000);
    struct tm res { 0 };
    localtime_s(&res, &t);
    return res;
}

ustring path_home() {
    ustring p = special_folder(CSIDL_PROFILE);
    return p;
}

ustring login_name() {
    unsigned len = UNLEN+1;
    wchar_t buffer[UNLEN+1];
    if (GetUserNameW(buffer, (LPDWORD) &len)) { return ustring(buffer, len); }
    return "somebody";
}

ustring path_tmp() {
    return str_getenv("TEMP", root_dir());
}

ustring path_user_data_dir() {
    ustring data_dir = special_folder(CSIDL_LOCAL_APPDATA);

    if (!data_dir.empty()) {
        return data_dir;
    }

    return path_build(path_home(), "Local Settings", "Application Data");
}

ustring path_user_config_dir() {
    return path_user_data_dir();
}

ustring path_user_cache_dir() {
    ustring data_dir = special_folder(CSIDL_INTERNET_CACHE);

    if (!data_dir.empty()) {
        return data_dir;
    }

    return path_build(path_home(), "Local Settings", "Temporary Internet Files");
}

ustring path_user_runtime_dir() {
    return path_user_cache_dir();
}

ustring path_user_desktop_dir() {
    return special_folder(CSIDL_DESKTOP);
}

ustring path_user_documents_dir() {
    return special_folder(CSIDL_MYDOCUMENTS);
}

ustring path_user_downloads_dir() {
    return path_build(path_home(), "Downloads");
}

ustring path_user_music_dir() {
    return special_folder(CSIDL_MYMUSIC);
}

ustring path_user_pictures_dir() {
    return special_folder(CSIDL_MYPICTURES);
}

ustring path_user_videos_dir() {
    return special_folder(CSIDL_MYVIDEO);
}

ustring path_user_templates_dir() {
    return special_folder(CSIDL_TEMPLATES);
}

ustring path_self() {
    wchar_t fn[4096];
    HMODULE hm = GetModuleHandle(NULL);
    DWORD n = GetModuleFileNameW(hm, fn, sizeof(fn)/2);
    return ustring(fn, n);
}

std::vector<ustring> file_list(const ustring & path) {
    WIN32_FIND_DATAW fdata;
    HANDLE h = FindFirstFileW(std::wstring(path_build(path, "*")).data(), &fdata);
    if (INVALID_HANDLE_VALUE == h) { throw sys_error(path); }
    std::vector<ustring> v;
    do { v.emplace_back(fdata.cFileName); } while (FindNextFileW(h, &fdata));
    FindClose(h);
    return v;
}

std::vector<ustring> file_glob(const ustring & path) {
    ustring dirp = ustring::npos == path_basename(path).find_first_of("*?") ? path : path_dirname(path);
    WIN32_FIND_DATAW fdata;
    HANDLE h = FindFirstFileW(std::wstring(path).data(), &fdata);
    if (INVALID_HANDLE_VALUE == h) { throw sys_error(path); }
    std::vector<ustring> v;

    do {
        ustring s(fdata.cFileName);
        if ('.' != *s.begin()) { v.emplace_back(path_build(dirp, s)); }
    } while (FindNextFileW(h, &fdata));

    FindClose(h);
    return v;
}

void file_mkdir(const ustring & path) {
    if (!file_exists(path)) {
        const ustring parent = path_dirname(path);
        if (file_exists(parent) && !file_is_dir(parent)) { throw user_error(str_format("file '", parent, "' exists but not a directory")); }
        else { file_mkdir(parent); }
        if (!CreateDirectoryW(std::wstring(path).data(), NULL)) { throw sys_error(path); }
    }

    else if (!file_is_dir(path)) {
        throw user_error(str_format("file '", path, "' exists but not a directory"));
    }
}

ustring path_cwd() {
    DWORD len = GetCurrentDirectoryW(0, NULL);

    if (0 != len) {
        wchar_t wd[len+1];
        GetCurrentDirectoryW(len, wd);
        return wd;
    }

    return ustring();
}

ustring path_dirname(const ustring & path) {
    std::size_t pos = path.find_last_of("/\\");

    if (path.npos != pos) {
        if (0 == pos) { return path.substr(0, 1); }

        if (2 == pos) {
            char c0 = toupper(path[0]);
            if ('A' <= c0 && c0 <= 'Z' && ':' == path[1]) { return path.substr(0, 3); }
        }

        return path.substr(0, pos);
    }

    return ".";
}

ustring path_real(const ustring & path) {
    wchar_t buffer[MAX_PATH+1];
    wchar_t * pname;
    DWORD res = GetFullPathNameW(std::wstring(path).c_str(), MAX_PATH, buffer, &pname);
    if (0 != res) { return buffer; }
    return path;
}

bool path_match(const ustring & pattern, const ustring & path) {
    std::wstring wpattern = pattern, wpath = path;
    return PathMatchSpecW(wpath.c_str(), wpattern.c_str());
}

ustring path_which(const ustring & cmd) {
    if (ustring::npos == cmd.find_first_of("/\\")) {
        for (const ustring & s: str_explode(str_getenv("PATH"), path_sep())) {
            ustring path = path_build(s, cmd);
            if (File(path).is_exec()) { return path; }
        }
    }

    return ustring();
}

void msleep(unsigned time_ms) {
    Sleep(time_ms);
}

RECT to_winrect(const Rect & r) {
    RECT wr;
    wr.left = r.left();
    wr.top = r.top();
    wr.right = wr.left+std::max(r.width(), 1U);
    wr.bottom = wr.top+std::max(r.height(), 1U);
    return wr;
}

Rect from_winrect(const RECT & rect) {
    return Rect(Point(rect.left, rect.top), Point(rect.right, rect.bottom));
}

static int CALLBACK efunc(const LOGFONTW * lf, const TEXTMETRICW * tm, DWORD ftype, LPARAM lp) {
    if (TRUETYPE_FONTTYPE == ftype) {
        std::vector<ustring> * vsp = reinterpret_cast<std::vector<ustring> *>(lp);
        vsp->push_back(lf->lfFaceName);
    }

    return 1;
}

// Fonts with @ before face name are enabled for vertical text.
std::vector<ustring> enum_dc_fonts(HDC hdc) {
    LOGFONTW lf;
    memset(&lf, 0, sizeof(lf));
    std::vector<ustring> sv;
    EnumFontFamiliesExW(hdc, &lf, efunc, LPARAM(&sv), 0);
    return sv;
}

Vector pointfx_to_vector(const POINTFX & pfx) {
    double x = pfx.x.value+(1.0/65536.0)*pfx.x.fract;
    double y = pfx.y.value+(1.0/65536.0)*pfx.y.fract;
    return Vector(x, y);
}

bool operator!=(const POINTFX & pfx1, const POINTFX & pfx2) {
    return pfx1.x.value != pfx2.x.value || pfx1.x.fract != pfx2.x.fract ||
    pfx1.y.value != pfx2.y.value || pfx1.y.fract != pfx2.y.fract;
}

POINT to_winpoint(const Point & pt) {
    POINT xp;
    xp.x = pt.x();
    xp.y = pt.y();
    return xp;
}

int winrop(Oper op) {
    static const struct { Oper op; int r2; } opers[] = {
        { Oper::COPY,    R2_COPYPEN      },
        { Oper::CLEAR,   R2_BLACK        },
        { Oper::SOURCE,  R2_COPYPEN      },
        { Oper::XOR,     R2_XORPEN       },
        { Oper::SET,     R2_WHITE        },
        { Oper::NOT,     R2_NOT          },
        { Oper::COPY,    R2_NOP          }
    };

    for (int i = 0; R2_NOP != opers[i].r2; ++i) {
        if (opers[i].op == op) {
            return opers[i].r2;
        }
    }

    return R2_COPYPEN;
}

void Locale_impl::init1() {
    LCID lcid = GetThreadLocale();
    char iso639[10];
    if (!GetLocaleInfo (lcid, LOCALE_SISO639LANGNAME, iso639, sizeof iso639)) { return; }
    char iso3166[10];
    if (!GetLocaleInfo (lcid, LOCALE_SISO3166CTRYNAME, iso3166, sizeof iso3166)) { return; }
    LANGID langid = LANGIDFROMLCID(lcid);
    const char * s = "";

    switch (PRIMARYLANGID(langid)) {
        case LANG_AZERI:
            switch (SUBLANGID(langid)) {
                case SUBLANG_AZERI_LATIN: s = "@Latn"; break;
                case SUBLANG_AZERI_CYRILLIC: s = "@Cyrl"; break;
            }

            break;

        case LANG_SERBIAN: // LANG_CROATIAN == LANG_SERBIAN
            switch (SUBLANGID(langid)) {
                case SUBLANG_SERBIAN_LATIN:
                case 0x06: // Serbian (Latin) - Bosnia and Herzegovina
                    s = "@Latn";
                break;
            }

            break;

        case LANG_UZBEK:
            switch (SUBLANGID(langid)) {
                case SUBLANG_UZBEK_LATIN: s = "@Latn"; break;
                case SUBLANG_UZBEK_CYRILLIC: s = "@Cyrl"; break;
            }

        break;
    }

    char buffer[64];
    std::snprintf(buffer, sizeof buffer, "%s_%s.CP%i%s", iso639, iso3166, GetACP(), s);
    spec = buffer;
    auto begin = spec.find('.');

    if (std::string::npos != begin) {
        ++begin;
        auto end = spec.find('@');
        enc = Encoding(spec.substr(begin, end-begin));
    }

    std::snprintf(buffer, sizeof buffer, "CP%i", GetOEMCP());
    iocharset = Encoding(buffer);
}

ustring::operator std::wstring() const {
    std::wstring ws;

    for (const char * p = str_.c_str(); '\0' != *p; p = utf8_next(p)) {
        char32_t wc = char32_from_pointer(p);
        char16_t c1, c2;
        char32_to_surrogate(wc, c1, c2);
        ws.push_back(c1);
        if (0 != c2) { ws.push_back(c2); }
    }

    return ws;
}

ustring & ustring::assign(const std::wstring & src) {
    str_from_utf16(*this, reinterpret_cast<const char16_t *>(src.c_str()), src.size());
    return *this;
}

ustring & ustring::assign(const wchar_t * src) {
    str_from_utf16(*this, reinterpret_cast<const char16_t *>(src), wcslen(src));
    return *this;
}

ustring & ustring::assign(const wchar_t * src, size_type n) {
    str_from_utf16(*this, reinterpret_cast<const char16_t *>(src), n);
    return *this;
}

bool path_same(const ustring & first_path, const ustring & second_path) {
    auto v1 = path_explode(str_toupper(path_real(first_path))), v2 = path_explode(str_toupper(path_real(second_path)));
    return std::ranges::equal(v1, v2);
}

ustring uri_scheme(const ustring & s) {
    if (s.npos != s.find('\\') || (s.size() > 1 && U':' == s[1] && char32_toupper(s[0]) >= U'A' && char32_toupper(s[0]) <= U'Z')) { return ustring(); }
    std::size_t pos = s.find_first_of(":\\/?#");
    return pos != s.npos && ':' == s[pos] ? s.substr(0, pos) : ustring();
}

ustring uri_notscheme(const ustring & s) {
    if (s.npos != s.find('\\') || (s.size() > 1 && U':' == s[1] && char32_toupper(s[0]) >= U'A' && char32_toupper(s[0]) <= U'Z')) { return s; }
    std::size_t pos = s.find_first_of(":/?#");
    return pos != s.npos && ':' == s[pos] ? s.substr(':' == s[pos] ? 1+pos : pos) : ustring();
}

bool uri_is_file(const ustring & uri) {
    if (uri.npos != uri.find('\\') || (uri.size() > 1 && U':' == uri[1] && char32_toupper(uri[0]) >= U'A' && char32_toupper(uri[0]) <= U'Z')) { return true; }
    auto sch = uri_scheme(uri);
    return !uri.empty() && (sch.empty() || str_similar("file", sch));
}

std::vector<ustring> list_drives() {
    std::vector<ustring> v;
    DWORD nbytes = GetLogicalDriveStringsA(0, NULL);

    if (0 != nbytes) {
        auto io = Locale().iocharset();
        char buf[1+nbytes];
        GetLogicalDriveStringsA(1+nbytes, buf);

        for (std::size_t i = 0; i < nbytes; ++i) {
            if ('\0' != buf[i]) {
                UINT drive_type = GetDriveType(buf+i);

                if (DRIVE_UNKNOWN != drive_type && DRIVE_NO_ROOT_DIR != drive_type) {
                    v.push_back(io.encode(buf+i));
                }
            }

            while ('\0' != buf[i]) { ++i; }
        }
    }

    return v;
}

// Declared in sys-impl.hh.
Mounts list_mounts() {
    Mounts mounts;
    for (auto & s: list_drives()) { mounts.emplace_back(s, s, false); }
    return mounts;
}

// Declared in sys-impl.hh
std::vector<ustring> list_mount_points() {
    return list_drives();
}

std::optional<std::vector<uint8_t>> shared_resource(const ustring & resource_name) {
    const ustring rname = str_replace(str_trimleft(resource_name, "/\\:"), "/\\", ':');

    if (HRSRC hr = FindResourceA(GetModuleHandle(NULL), rname.data(), RT_RCDATA)) {
        if (HGLOBAL hg = LoadResource(GetModuleHandle(NULL), hr)) {
            if (DWORD bytes = SizeofResource(GetModuleHandle(NULL), hr)) {
                if (LPVOID p = LockResource(hg)) {
                    std::vector<uint8_t> v(bytes);
                    std::memcpy(v.data(), p, bytes);
                    return v;
                }
            }
        }
    }

    return shared_file(resource_name);
}

// Based on MSDN example https://learn.microsoft.com/en-us/windows/win32/procthread/creating-a-child-process-with-redirected-input-and-output.
ustring pipe_in(const ustring & cmd) {
    std::wstring wis(cmd); std::string os;
    PROCESS_INFORMATION pi; STARTUPINFOW si;
    ZeroMemory(&pi, sizeof pi); ZeroMemory(&si, sizeof si); si.cb = sizeof si;
    HANDLE hin = NULL, hout = NULL;

    // Set the bInheritHandle flag so pipe handles are inherited.
    SECURITY_ATTRIBUTES sa { sizeof sa, NULL, true };

    // Create a pipe for the child process's STDOUT.
    if (!CreatePipe(&hin, &hout, &sa, 0)) { throw sys_error(__func__); }

    // Ensure the read handle to the pipe for STDOUT is not inherited.
    if (!SetHandleInformation(hin, HANDLE_FLAG_INHERIT, 0)) { throw sys_error(__func__); }

    // Create the child process.
    // Set up members of the PROCESS_INFORMATION structure.
    // Set up members of the STARTUPINFO structure.
    // This structure specifies the STDIN and STDOUT handles for redirection.
    si.hStdError = si.hStdOutput = hout;
    si.dwFlags = STARTF_USESTDHANDLES;

    BOOL succeed = CreateProcessW(
                            NULL,
                            wis.data(),         // command line
                            NULL,               // process security attributes
                            NULL,               // primary thread security attributes
                            TRUE,               // handles are inherited
                            CREATE_NO_WINDOW,   // creation flags
                            NULL,               // use parent's environment
                            NULL,               // use parent's current directory
                            &si,                // STARTUPINFO pointer
                            &pi);               // receives PROCESS_INFORMATION

    // If an error occurs, exit the application.
    if (!succeed) { throw sys_error(__func__); }

    // Wait until child process exits.
    WaitForSingleObject(pi.hProcess, INFINITE);

    // Close handles to the child process and its primary thread.
    // Some applications might keep these handles to monitor the status
    // of the child process, for example.
    CloseHandle(pi.hProcess);
    CloseHandle(pi.hThread);

    // Close handles to the stdin and stdout pipes no longer needed by the child process.
    // If they are not explicitly closed, there is no way to recognize that the child process has ended.
    CloseHandle(hout);

    // Read from pipe that is the standard output for child process.
    DWORD nr; char buffer[256];

    do {
        nr = 0;
        if (ReadFile(hin, buffer, sizeof buffer, &nr, NULL)) { os.append(buffer, nr); }
    } while (nr == sizeof buffer);

    return Locale().iocharset().decode(os);
}

#ifdef _MSC_VER
// FIXME What about this warning?
#pragma warning(disable : 4996)
#endif

// Declared in sys-impl.hh.
void boot_sys() {
    OSVERSIONINFOW ov;
    std::memset(&ov, 0, sizeof ov);
    ov.dwOSVersionInfoSize = sizeof ov;

    if (GetVersionExW(&ov)) {
        sysinfo_.uname += str_format(' ', ov.dwMajorVersion, '.', ov.dwMinorVersion, '-', ov.dwBuildNumber);
        if (L'\0' != *ov.szCSDVersion) { sysinfo_.uname += " "_tu+ov.szCSDVersion; }
        sysinfo_.osmajor = ov.dwMajorVersion, sysinfo_.osminor = ov.dwMinorVersion;
    }

    // FIXME %windir% doesn't work here!
    if (file_exists("c:\\windows\\syswow64\\wineboot.exe")) { sysinfo_.uname += " (Wine)"; }

    sysinfo_.locale = Locale().name();
    sysinfo_.iocharset = Locale().iocharset().name();

    SYSTEM_INFO si;
    GetSystemInfo(&si);
    sysinfo_.cores = si.dwNumberOfProcessors;
}

#ifdef _MSC_VER
ustring str_getenv(const ustring & env, const ustring & fallback) {
    wchar_t * buffer; std::size_t n;
    if (0 == _wdupenv_s(&buffer, &n, std::wstring(str_replace(env, "$(){}", "")).data())) { return buffer; }
    return fallback;
}
#else
ustring str_getenv(const ustring & env, const ustring & fallback) {
    ustring res = fallback;

    if (char * val = getenv(str_replace(env, "$(){}", "").data())) {
        res.assign(str_trimboth((Locale().iocharset().decode(val))));
    }

    return res;
}
#endif

} // namespace tau

//END

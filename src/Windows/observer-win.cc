// ----------------------------------------------------------------------------
// SPDX-License-Identifier: BSD-2-Clause
// Copyright © 2014-2024 Konstantin Shmelkov <mrcashe@gmail.com>.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
// THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
// PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
// CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
// EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
// PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
// OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
// WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
// OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
// EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// ----------------------------------------------------------------------------

/// @file observer-unix.cc The Observer_impl class implementation.
/// The header file is observer-impl.hh.
/// @since 0.7.0

#include "defs-win.hh"
#include <gettext-impl.hh>
#include <observer-impl.hh>
#include <sys-impl.hh>
#include <cstring>
#include <iostream>

namespace tau {

void Observer_impl::init() {
    init_user_dirs();
    std::list<ustring> removables, cdroms, remotes, drives;
    DWORD nbytes = GetLogicalDriveStringsA(0, NULL);

    if (0 != nbytes) {
        Locale loc;
        char buf[1+nbytes];
        GetLogicalDriveStringsA(1+nbytes, buf);

        for (std::size_t i = 0; i < nbytes; ++i) {
            if ('\0' != buf[i]) {
                UINT dtype = GetDriveType(buf+i);

                if (DRIVE_UNKNOWN != dtype && DRIVE_NO_ROOT_DIR != dtype) {
                    ustring name = loc.iocharset().encode(buf+i);
                    if (DRIVE_REMOVABLE == dtype) { removables.push_back(name); }
                    else if (DRIVE_CDROM == dtype) { cdroms.push_back(name); }
                    else if (DRIVE_REMOTE == dtype) { remotes.push_back(name); }
                    else { drives.push_back(name); }
                }
            }

            while ('\0' != buf[i]) { ++i; }
        }
    }

    if (!drives.empty()) {
        auto tp = std::make_shared<Label_impl>("Drives");
        tp->hint_margin_top(5); append(tp);
        for (const ustring & s: drives) { init_folder(s, "drive-harddisk", s); }
    }

    if (!cdroms.empty()) {
        auto tp = std::make_shared<Label_impl>(lgettext("CDROM Drives"));
        tp->hint_margin_top(5); append(tp);
        for (const ustring & s: cdroms) { init_folder(s, "drive-optical:drive-harddisk", s); }
    }

    if (!remotes.empty()) {
        auto tp = std::make_shared<Label_impl>(lgettext("Remote Drives"));
        tp->hint_margin_top(5);
        append(tp);
        for (const ustring & s: remotes) { init_folder(s, "folder-remote:folder", s); }
    }

    if (!removables.empty()) {
        auto tp = std::make_shared<Label_impl>(lgettext("Removable Drives"));
        tp->hint_margin_top(5); append(tp);
        for (const ustring & s: removables) { init_folder(s, "drive-removable-media:drive-harddisk", s); }
    }
}

} // namespace tau

//END

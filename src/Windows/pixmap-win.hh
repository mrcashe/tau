// ----------------------------------------------------------------------------
// SPDX-License-Identifier: BSD-2-Clause
// Copyright © 2014-2024 Konstantin Shmelkov <mrcashe@gmail.com>.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
// THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
// PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
// CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
// EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
// PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
// OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
// WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
// OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
// EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// ----------------------------------------------------------------------------

#ifndef __TAU_PIXMAP_WIN_HH__
#define __TAU_PIXMAP_WIN_HH__

#include <pixmap-impl.hh>
#include "defs-win.hh"
#include "painter-win.hh"

namespace tau {

struct Pixmap_sys {
    unsigned             depth_  = 0;
    std::size_t          stride_ = 0;
    std::vector<uint8_t> raw_;
    BITMAPINFO           info_ {{ 0 }};
};

class Pixmap_win: public Pixmap_impl {
public:

    explicit Pixmap_win(unsigned depth, const Size & sz=Size());

    // Overrides pure Pixmap_impl.
    Painter painter() override { return Painter(std::make_shared<Painter_win>(this)); }

    // Overrides pure Pixmap_impl.
    Size size() const override { return Size(sys.info_.bmiHeader.biWidth, -sys.info_.bmiHeader.biHeight); }

    // Overrides pure Pixmap_impl.
    int depth() const override { return sys.depth_; }

    // Overrides pure Pixmap_impl.
    std::size_t bytes() const override { return sys.raw_.size(); }

    // Overrides pure Pixmap_impl.
    const uint8_t * raw() const override { return sys.raw_.data(); }

    // Overrides pure Pixmap_impl.
    void resize(const Size & size) override { resize_priv(size); }

    // Overrides pure Pixmap_impl.
    void fill_rectangles(const Rect * rs, std::size_t nrs, const Color & c) override;

    // Overrides pure Pixmap_impl.
    void put_pixel(const Point & pt, const Color & c) override;

    // Overrides pure Pixmap_impl.
    Color get_pixel(const Point & pt) const override;

    // Overrides pure Pixmap_impl.
    void set_argb32(const Point & pt, const uint8_t * buffer, std::size_t nbytes) override;

    HBITMAP create_bitmap(HDC dc) const;

private:

    mutable Pixmap_sys sys;

private:

    unsigned bpp() const;
    void put_pixel_impl(const Point & pt, uint32_t rgb);
    uint32_t get_pixel_impl(const Point & pt) const;
    void resize_priv(const Size & sz);
};

} // namespace tau

#endif // __TAU_PIXMAP_WIN_HH__

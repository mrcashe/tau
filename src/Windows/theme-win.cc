// ----------------------------------------------------------------------------
// SPDX-License-Identifier: BSD-2-Clause
// Copyright © 2014-2024 Konstantin Shmelkov <mrcashe@gmail.com>.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
// THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
// PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
// CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
// EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
// PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
// OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
// WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
// OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
// EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// ----------------------------------------------------------------------------

#include "pixmap-win.hh"
#include "theme-win.hh"
#include <algorithm>
#include <cstring>
#include <iostream>
#include <mutex>

namespace {

std::recursive_mutex    mx_;
tau::Theme_win_ptr      root_;

} // anonymous namespace

namespace tau {

// Overrides Theme_impl.
// Tries to load icon from the PE file resource.
Pixmap_ptr Theme_win::icon_hook(const ustring & name, const ustring & context, int size) {
    if (HRSRC hr = FindResourceA(GetModuleHandle(NULL), str_format("ico:", size, ':', name).data(), RT_RCDATA)) {
        if (HGLOBAL hg = LoadResource(GetModuleHandle(NULL), hr)) {
            if (DWORD bytes = SizeofResource(GetModuleHandle(NULL), hr)) {
                if (LPVOID p = LockResource(hg)) {
                    try { return Pixmap_impl::create(reinterpret_cast<const uint8_t *>(p), bytes); }
                    catch (exception & x) { std::cerr << "** " << __func__ << ": " << x.what() << std::endl; }
                }
            }
        }
    }

    return nullptr;
}

// static
Theme_win_ptr Theme_win::root_win() {
    std::unique_lock lk(mx_);
    if (!root_) { root_ = std::make_shared<Theme_win>(); root_->boot(); }
    return root_;
}

// static
Theme_ptr Theme_impl::root() {
    return Theme_win::root_win();
}

} // namespace tau

//END

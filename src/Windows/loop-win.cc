// ----------------------------------------------------------------------------
// SPDX-License-Identifier: BSD-2-Clause
// Copyright © 2014-2024 Konstantin Shmelkov <mrcashe@gmail.com>.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
// THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
// PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
// CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
// EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
// PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
// OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
// WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
// OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
// EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// ----------------------------------------------------------------------------

#include <tau/exception.hh>
#include <tau/file.hh>
#include <tau/locale.hh>
#include <tau/sys.hh>
#include "loop-win.hh"
#include "sys-win.hh"
#include <algorithm>
#include <cstring>

namespace tau {

thread_local tau::Loop_win_ptr this_loop_;

} // anonymous namespace

namespace tau {

// Overrides Loop_impl.
void Loop_win::on_quit() {
    // this_loop_.reset();
}

// Overrides pure Loop_impl.
bool Loop_win::iterate(int timeout_ms) {
    std::size_t n = std::min(std::size_t(MAXIMUM_WAIT_OBJECTS), handles_.size());
    DWORD result = MsgWaitForMultipleObjects(n, handles_.data(), false, std::max(1, timeout_ms), QS_ALLINPUT);

    if (WAIT_TIMEOUT != result) {
        if (0 != n && result < WAIT_OBJECT_0+n) {
            HANDLE handle = handles_[result-WAIT_OBJECT_0];
            signal_chain_poll_(handle);
        }

        MSG msg;

        while (PeekMessageW(&msg, NULL, 0, 0, PM_REMOVE)) {
            TranslateMessage(&msg);
            DispatchMessageW(&msg);
        }

        return true;
    }

    return false;
}

// Overrides pure Loop_impl.
Event_ptr Loop_win::event() {
    auto evp = std::make_shared<Event_win>();
    HANDLE handle = evp->handle();
    handles_.push_back(handle);
    signal_chain_poll_.connect(fun(evp, &Event_win::on_poll));
    evp->signal_ready().connect(fun(evp, &Event_win::release));
    evp->signal_destroy().connect(bind_back(fun(this, &Loop_win::on_handle_die), handle));
    return evp;
}

// Overrides pure Loop_impl.
File_monitor_win_ptr Loop_win::create_file_monitor(const ustring & path, int umask) {
    auto fm = std::make_shared<File_monitor_win>(path, umask);
    HANDLE handle = fm->handle();
    handles_.push_back(handle);
    signal_chain_poll_.connect(fun(fm, &File_monitor_win::on_poll));
    fm->signal_destroy().connect(bind_back(fun(this, &Loop_win::on_handle_die), handle));
    return fm;
}

void Loop_win::on_handle_die(HANDLE handle) {
    auto p = handles_.data();

    for (std::size_t i = 0; i < handles_.size(); i++, p++) {
        if (*p == handle) {
            handles_.erase(handles_.begin()+i);
            break;
        }
    }
}

void Loop_win::handle_mount() {
    Drives drives = list_drives();

    for (const ustring & drv: drives) {
        if (drives_.end() == std::find(drives_.begin(), drives_.end(), drv)) {
            drives_.push_back(drv);
            signal_watch_(File::MOUNT, drv);
        }
    }
}

void Loop_win::handle_umount() {
    Drives drives = list_drives();
    bool rem;

    do {
        rem = false;

        for (auto i = drives_.begin(); i != drives_.end(); ++i) {
            ustring s = *i;

            if (drives.end() == std::find(drives.begin(), drives.end(), s)) {
                rem = true;
                drives_.erase(i);
                signal_watch_(File::UMOUNT, s);
                break;
            }
        }

    } while (rem);
}

// ----------------------------------------------------------------------------
// ----------------------------------------------------------------------------

// static
Loop_win_ptr Loop_win::this_win_loop() {
    if (this_loop_) { return this_loop_; }
    this_loop_ = std::static_pointer_cast<Loop_win>(Loop_impl::that_ptr(std::this_thread::get_id()));
    if (this_loop_) { return this_loop_; }
    this_loop_ = std::make_shared<Loop_win>();
    new_loop(this_loop_);
    return this_loop_;
}

// static
Loop_ptr Loop_impl::this_ptr() {
    return this_loop_ ? this_loop_ : Loop_win::this_win_loop();
}

} // namespace tau

//END

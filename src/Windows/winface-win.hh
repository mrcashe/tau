// ----------------------------------------------------------------------------
// SPDX-License-Identifier: BSD-2-Clause
// Copyright © 2014-2024 Konstantin Shmelkov <mrcashe@gmail.com>.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
// THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
// PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
// CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
// EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
// PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
// OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
// WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
// OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
// EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// ----------------------------------------------------------------------------

#ifndef __TAU_WINFACE_WIN_HH__
#define __TAU_WINFACE_WIN_HH__

#include "defs-win.hh"
#include <dialog-impl.hh>
#include <popup-impl.hh>
#include "display-win.hh"

namespace tau {

class Winface_win: public Winface {
public:

    Winface_win(Display_win * dp, HWND hwnd);

    // Overrides pure Winface.
    void init(Window_impl * wip) override;

    // Overrides pure Winface.
    Display_impl * display() override;

    // Overrides pure Winface.
    const Display_impl * display() const override;

    // Overrides pure Winface.
    void update() override;

    // Overrides pure Winface.
    void invalidate(const Rect & inval) override;

    // Overrides pure Winface.
    Painter_ptr painter() override;

    // Overrides pure Winface.
    void move(const Point & pt) override;

    // Overrides pure Winface.
    void move(const Rect & r) override;

    // Overrides pure Winface.
    void resize(const Size & size) override;

    // Overrides pure Winface.
    void minimize() override;

    // Overrides pure Winface.
    void maximize() override;

    // Overrides pure Winface.
    void restore() override;

    // Overrides pure Winface.
    void set_fullscreen(bool yes) override;

    // Overrides pure Winface.
    void set_cursor(Cursor_ptr cursor) override;

    // Overrides pure Winface.
    void unset_cursor() override;

    // Overrides pure Winface.
    void show_cursor(bool show) override;

    // Overrides pure Winface.
    void set_title(const ustring & title) override;

    // Overrides pure Winface.
    void set_icon(Pixmap_ptr icon) override;

    // Overrides pure Winface.
    bool visible() const override { return visible_; }

    Display_win * wdp() noexcept { return dp_; }
    const Display_win * wdp() const noexcept { return dp_; }
    double dpi() const noexcept { return dpi_; };

    Window_impl * self() { return self_; }
    const Window_impl * self() const { return self_; }
    HWND handle() const { return hwnd_; }

    void handle_maximize();
    void handle_minimize();
    void handle_restore();
    void handle_visibility(bool visible);

    void track_mouse_event();
    void untrack_mouse_event();

private:

    Display_win *       dp_;
    HWND                hwnd_;
    double              dpi_                = 96;
    unsigned            keep_style_         = 0;
    bool                tme_: 1             = false;  // TrackMouseEvent() called.
    bool                want_fullscreen_: 1 = false;
    bool                want_maximize_: 1   = false;
    bool                want_minimize_: 1   = false;
    bool                visible_: 1         = false;
    Window_impl *       self_               = nullptr;
    Toplevel_impl *     tpl_                = nullptr;
    Popup_impl *        popup_              = nullptr;
    HCURSOR             icursor_            = NULL;    // hCursor from class.
    RECT                keep_rect_;

private:

    void on_hints(Hints op);
    void on_close();
    void on_quit() { dp_ = nullptr; }
    void change_style_bits(LONG set, LONG reset);
    void on_enable(bool yes);
};

#define WINFACE_WIN(wip) std::static_pointer_cast<Winface_win>(wip->winface())

} // namespace tau

#endif // __TAU_WINFACE_WIN_HH__

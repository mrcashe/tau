// ----------------------------------------------------------------------------
// SPDX-License-Identifier: BSD-2-Clause
// Copyright © 2014-2024 Konstantin Shmelkov <mrcashe@gmail.com>.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
// THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
// PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
// CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
// EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
// PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
// OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
// WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
// OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
// EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// ----------------------------------------------------------------------------

#include <tau/exception.hh>
#include <tau/sys.hh>
#include <file-impl.hh>
#include "loop-win.hh"
#include <chrono>
#include <filesystem>
#include <map>
#include <mutex>
#include <iostream>

namespace fs = std::filesystem;
namespace chr = std::chrono;

namespace {

using User_icons = std::map<std::string, std::string_view>;

std::recursive_mutex    smx_;
User_icons              uicons_;

} // anonymous namespace

// ----------------------------------------------------------------------------
// ----------------------------------------------------------------------------

namespace tau {

struct File_win: public File_impl {
    Loop_win_ptr            loop_;
    File_monitor_win_ptr    mon_;
    signal<void(int, const ustring &)> signal_watch_;

    File_win(const ustring & uri) {
        uri_ = uri;
        update_stat();
    }

    void update_stat() {
        flags_ = 0;
        atime_ = ctime_ = mtime_ = 0;
        exists_ = false;
        bytes_ = 0;

        if (uri_is_file(uri_)) {
            try {
                std::wstring ws(path_real(uri_));
                exists_ = fs::exists(ws);

                if (exists_) {
                    if (fs::is_symlink(ws)) { flags_ |= IS_LNK; }
                    if (fs::is_directory(ws)) { flags_ |= IS_DIR; }
                    if (fs::is_fifo(ws)) { flags_ |= IS_FIFO; }
                    if (fs::is_regular_file(ws)) { flags_ |= IS_REG; bytes_ = fs::file_size(ws); }
                    if (fs::is_block_file(ws)) { flags_ |= IS_BLK; }
                    if (fs::is_character_file(ws)) { flags_ |= IS_CHR; }
                    if (fs::is_socket(ws)) { flags_ |= IS_SOCK; }
                    auto ft = fs::last_write_time(ws);
#ifdef _MSC_VER
                    auto tt = chr::duration_cast<chr::microseconds>(ft.time_since_epoch()).count();
                    atime_ = ctime_ = mtime_ = Timeval(tt);
#else
                    auto sys = chr::file_clock::to_sys(ft);
                    auto tt = chr::duration_cast<chr::microseconds>(sys.time_since_epoch()).count();
                    atime_ = ctime_ = mtime_ = Timeval(tt);
#endif
                }
            }

            catch (std::exception & x) {
                std::cerr << "** File: " << x.what() << std::endl;
            }

            catch (...) {
                std::cerr << "** File: unknown exception" << std::endl;
            }
        }
    }

    // Overrides pure File_impl.
    signal<void(int, const ustring &)> & signal_watch(int event_mask) override {
        if (!loop_) { loop_ = Loop_win::this_win_loop(); }

        if (!mon_) {
            try { mon_ = loop_->create_file_monitor(uri_, event_mask); }
            catch (...) {}
        }

        if (mon_) { mon_->signal_notify().connect(fun(signal_watch_)); }
        return signal_watch_;
    }

    // Overrides pure File_impl.
    bool is_exec() const noexcept override {
        if (exists_) {
            DWORD type;
            if (GetBinaryTypeW(std::wstring(uri_).c_str(), &type)) { return true; }
        }

        return false;
    }

    // Overrides pure File_impl.
    bool is_hidden() const noexcept override {
        DWORD attrs = GetFileAttributesW(std::wstring(uri_).c_str());

        if (INVALID_FILE_ATTRIBUTES != attrs) {
            return 0 != (FILE_ATTRIBUTE_HIDDEN & attrs);
        }

        return true;
    }

    // Overrides pure File_impl.
    bool is_removable() const noexcept override {
        if (uri_.size() > 1) {
            if (':' == uri_[1]) {
                UINT dtype = GetDriveType((std::string(1, uri_[0])+":\\").data());

                if (DRIVE_UNKNOWN != dtype && DRIVE_NO_ROOT_DIR != dtype) {
                    return DRIVE_REMOVABLE == dtype;
                }
            }
        }

        return false;
    }

    // Overrides pure File_impl.
    void rm(int opts=0, slot<void(int)> slot_async=slot<void(int)>()) override {
        fs::remove_all(std::wstring(uri_));
        exists_ = false;
    }

    // Overrides pure File_impl.
    std::string_view user_icon() const final {
        std::unique_lock lock(smx_);

        if (uicons_.empty()) {
            uicons_[tau::str_tolower(tau::path_home())] = "folder-home";
            uicons_[tau::str_tolower(tau::path_user_documents_dir())] = "folder-documents";
            uicons_[tau::str_tolower(tau::path_user_desktop_dir())] = "user-desktop";
            uicons_[tau::str_tolower(tau::path_user_downloads_dir())] = "folder-downloads:folder-download";
            uicons_[tau::str_tolower(tau::path_user_music_dir())] = "folder-sound:folder-music";
            uicons_[tau::str_tolower(tau::path_user_pictures_dir())] = "folder-image:folder-images";
            uicons_[tau::str_tolower(tau::path_user_videos_dir())] = "folder-video:folder-videos";
            uicons_[tau::str_tolower(tau::path_user_templates_dir())] = "folder-templates";
        }

        return uicons_[tau::str_tolower(uri_)];
    }
};

File_ptr File_impl::create(const ustring & uri) {
    return std::make_shared<File_win>(uri);
}

} // namespace tau

//END

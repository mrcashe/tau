// ----------------------------------------------------------------------------
// SPDX-License-Identifier: BSD-2-Clause
// Copyright © 2014-2024 Konstantin Shmelkov <mrcashe@gmail.com>.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
// THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
// PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
// CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
// EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
// PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
// OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
// WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
// OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
// EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// ----------------------------------------------------------------------------

#include <tau/exception.hh>
#include <tau/font.hh>
#include <true-type-font.hh>
#include "font-win.hh"
#include "sys-win.hh"
#include <algorithm>
#include <cstring>
#include <mutex>
#include <iostream>

namespace {

inline uint16_t u16(const char * b) {
    return uint8_t(b[1])|(uint16_t(uint8_t(b[0])) << 8);
}

struct Win_font {
    tau::ustring                orig_;
    tau::ustring                family_;
    std::vector<tau::ustring>   faces_;
};

std::recursive_mutex            smx_;
std::vector<Win_font>           fonts_;
tau::ustring                    font_normal_;
tau::ustring                    font_mono_;

int CALLBACK lister(const LOGFONTW * lf, const TEXTMETRICW * tm, DWORD ftype, LPARAM lp) {
    if (TRUETYPE_FONTTYPE == ftype) {
        tau::ustring orig(lf->lfFaceName);
        if (!orig.empty() && U'@' == orig[0]) { orig.erase(orig.begin()); }
        auto specv = tau::Font::explode(orig);
        tau::ustring family = tau::Font::family(specv);
        tau::ustring face = tau::Font::face(specv);
        auto i = std::find_if(fonts_.begin(), fonts_.end(), [&family](const Win_font & wf) { return tau::str_similar(family, wf.family_); } );

        if (fonts_.end() == i) {
            Win_font wf;
            wf.orig_ = orig;
            wf.family_ = family;
            wf.faces_.push_back(face);
            fonts_.push_back(wf);
        }

        else if (i->faces_.end() == std::find_if(i->faces_.begin(), i->faces_.end(), [&face](auto & s) { return tau::str_similar(face,s ); } )) {
            i->faces_.push_back(face);
        }
    }

    return 1;
}

} // anonymous namespace

// ----------------------------------------------------------------------------
// ----------------------------------------------------------------------------

namespace tau {

Font_win::Font_win(HDC hdc, unsigned dpi, const ustring & spec):
    hdc_(hdc)
{
    dpi_ = dpi;
    spec_ = spec;
    if (Font::points(spec_) < 1.0) { spec_ = Font::at_least(spec_, 10); }
    auto specv = Font::explode(spec);
    LOGFONT lf;
    memset(&lf, 0, sizeof(lf));
    lf.lfHeight = -((Font::points(spec_)*dpi)/72);
    lf.lfOutPrecision = OUT_DEFAULT_PRECIS;
    lf.lfClipPrecision = CLIP_DEFAULT_PRECIS;
    lf.lfQuality = DEFAULT_QUALITY;
    lf.lfPitchAndFamily = DEFAULT_PITCH|FF_DONTCARE;
    lf.lfWeight = FW_NORMAL;
    ustring fam = Font::family(specv);

    if (str_similar("Mono", specv)) {
        const ustring mono(fam+" Mono");
        auto i = std::find_if(fonts_.begin(), fonts_.end(), [&mono](auto & wf) { return str_similar(wf.orig_, mono); });
        if (i != fonts_.end()) { fam = mono; }
        lf.lfPitchAndFamily = FIXED_PITCH|FF_DONTCARE;
    }

    std::size_t len = std::min(std::size_t(LF_FACESIZE), fam.bytes());
    memcpy(lf.lfFaceName, fam.c_str(), len);
    lf.lfFaceName[len < LF_FACESIZE ? len : LF_FACESIZE-1] = '\0';
    lf.lfItalic = str_similar("Italic", specv) || str_similar("Oblique", specv);

    if (str_similar("Thin", specv)) { lf.lfWeight = FW_THIN; }
    else if (str_similar("ExtraLight", specv) || str_similar("UltraLight", specv)) { lf.lfWeight = FW_EXTRALIGHT; }
    else if (str_similar("Light", specv)) { lf.lfWeight = FW_LIGHT; }
    else if (str_similar("Medium", specv)) { lf.lfWeight = FW_MEDIUM; }
    else if (str_similar("SemiBold", specv) || str_similar("DemiBold", specv)) { lf.lfWeight = FW_SEMIBOLD; }
    else if (str_similar("ExtraBold", specv) || str_similar("UltraBold", specv) || str_similar("SuperBold", specv)) { lf.lfWeight = FW_EXTRABOLD; }
    else if (str_similar("Bold", specv)) { lf.lfWeight = FW_BOLD; }
    else if (str_similar("Heavy", specv) || str_similar("Black", specv)) { lf.lfWeight = FW_HEAVY; }

    hfont_ = CreateFontIndirectA(&lf);
    HFONT old_font = SelectFont(hdc, hfont_);
    OUTLINETEXTMETRICW otm;

    if (GetOutlineTextMetricsW(hdc, sizeof otm, &otm)) {
        ascent_  = otm.otmTextMetrics.tmAscent;
        descent_ = -otm.otmTextMetrics.tmDescent;
        linegap_ = otm.otmLineGap;
        min_.set(otm.otmrcFontBox.left, otm.otmrcFontBox.top);
        max_.set(otm.otmrcFontBox.right, otm.otmrcFontBox.bottom);
    }

    SelectFont(hdc, old_font);
}

Font_win::~Font_win() {
    invalidate();
}

ustring Font_win::psname() const {
    if (!ps_) {
        HFONT old_font = SelectFont(hdc_, hfont_);
        ps_ = true;
        auto v = table("name");

        if (!v.empty()) {
            const char * b = v.data();
            uint32_t format_selector = u16(b);

            if (0 == format_selector) {
                uint16_t nrec = u16(b+2);
                uint32_t storage = u16(b+4);
                uint32_t index = 6;

                for (uint16_t nr = 0; nr < nrec; ++nr, index += 12) {
                    uint16_t plat_id = u16(b+index);
                    uint16_t pse = u16(b+index+2);
                    uint16_t name_id = u16(b+index+6);
                    uint16_t ofs = storage+u16(b+index+10);
                    uint16_t pslen = u16(b+index+8);

                    if (6 == name_id) {
                        if (1 == plat_id && 0 == pse) {
                            psname_.assign(b+ofs, pslen);
                            break;
                        }

                        else if (3 == plat_id && (0 == pse || 1 == pse)) {
                            psname_.assign(reinterpret_cast<const char16_t *>(b+ofs), pslen/2);
                            break;
                        }
                    }
                }
            }
        }

        else {
            std::cout << "** " << __func__ << ": " << spec_ << ": failed to load 'name' table from TTF" << std::endl;
        }

        SelectFont(hdc_, old_font);
    }

    return psname_;
}

std::vector<char> Font_win::table(std::string_view table_name) const {
    std::vector<char> v;

    if (4 == table_name.size()) {
        HFONT old_font = SelectFont(hdc_, hfont_);
        DWORD tname = 0;
        for (int i = 3; i >= 0; --i) { tname <<= 8; tname |= uint8_t(table_name[i]); }
        DWORD nbytes = GetFontData(hdc_, tname, 0, NULL, 0);

        if (GDI_ERROR != nbytes) {
            v.resize(nbytes);
            if (nbytes != GetFontData(hdc_, tname, 0, v.data(), nbytes)) { v.clear(); }
        }

        SelectFont(hdc_, old_font);
    }

    return v;
}

Glyph_ptr Font_win::glyph(char32_t wc) const {
    auto glyph = std::make_shared<Glyph_impl>();
    GLYPHMETRICS gm;
    MAT2 mat { { 0, 1 }, { 0, 0 }, { 0, 0 }, { 0, 1 } };
    DWORD nbytes = GetGlyphOutlineW(hdc_, wc, GGO_NATIVE, &gm, 0, NULL, &mat);

    if (GDI_ERROR != nbytes) {
        HFONT old_font = SelectFont(hdc_, hfont_);
        uint8_t buffer[nbytes];
        const uint8_t * bmax = buffer+nbytes;

        if (0 < GetGlyphOutlineW(hdc_, wc, GGO_NATIVE, &gm, nbytes, buffer, &mat)) {
            int ox = gm.gmptGlyphOrigin.x;
            int oy = gm.gmptGlyphOrigin.y;
            int sx = gm.gmBlackBoxX;
            int sy = gm.gmBlackBoxY;

            glyph->min_.set(ox, oy-sy);
            glyph->max_.set(ox+sx, oy);
            glyph->advance_.set(gm.gmCellIncX, gm.gmCellIncY);

            for (const uint8_t * p = buffer; p < bmax; ) {
                auto * hdr = reinterpret_cast<const TTPOLYGONHEADER *>(p);
                const uint8_t * pmax = p+(hdr->cb);
                if (pmax > bmax || TT_POLYGON_TYPE != hdr->dwType) { break; }
                Vector vstart = pointfx_to_vector(hdr->pfxStart), vcur = vstart;
                Contour ctr(vstart);
                const uint8_t * cp = p+sizeof(*hdr);
                const POINTFX * pfx = nullptr;

                while (cp < pmax) {
                    auto * cv = reinterpret_cast<const TTPOLYCURVE *>(cp);
                    auto * fxs = reinterpret_cast<const POINTFX *>(cp+4);
                    const unsigned cpfx = cv->cpfx;
                    cp += 4;

                    // http://www.drdobbs.com/truetype-font-secrets/184403680
                    // Polyline records consist of a short (two-byte) integer n followed by n points.
                    // The last point of the previous record connects by a straight line to the first point,
                    // then straight lines connect subsequent points.
                    if (TT_PRIM_LINE == cv->wType) {
                        for (unsigned n = 0; n < cpfx; ++n) {
                            cp += sizeof(POINTFX);
                            pfx = fxs++;
                            vcur = pointfx_to_vector(*pfx);
                            ctr.line_to(vcur);
                        }
                    }

                    // http://www.drdobbs.com/truetype-font-secrets/184403680
                    // QSpline records also consist of a short integer n followed by n points,
                    // but only the last point lies on the glyph itself.
                    // These points define a connected series of n-1 Bezier curves.
                    else if (TT_PRIM_QSPLINE == cv->wType) {
                        Vector cpt;

                        for (unsigned n = 0; n < cpfx-1; ++n, ++fxs) {
                            cp += sizeof(POINTFX);
                            cpt = pointfx_to_vector(fxs[0]);

                            if (n < cpfx-2) {
                                Vector ncp = pointfx_to_vector(fxs[1]);
                                vcur.set( (cpt.x()+ncp.x())/2, (cpt.y()+ncp.y())/2 );
                                ctr.conic_to(cpt, vcur);
                            }
                        }

                        pfx = fxs;
                        vcur = pointfx_to_vector(fxs[0]);
                        ctr.conic_to(cpt, vcur);
                        cp += sizeof(POINTFX);
                    }

                    else if (TT_PRIM_CSPLINE == cv->wType) {
                        std::cerr << "** " << __func__ << '(' << str_unicode(wc) << "): " << spec_ << ": cubic spline yet not supported" << std::endl;
                        break;
                    }

                    else {
                        std::cerr << "** " << __func__ << '(' << str_unicode(wc) << "): " << spec_ << ": corrupted glyph data" << std::endl;
                        break;
                    }
                }

                if (pfx && hdr->pfxStart != *pfx) { ctr.line_to(vstart); }
                glyph->add_contour(ctr);
                p += hdr->cb;
            }
        }

        SelectFont(hdc_, old_font);
    }

    else {
        throw internal_error(str_format(__func__, '(', str_unicode(wc), "): ", spec_, ": GDI error"));
    }

    return glyph;
}

Glyph_ptr Font_win::master_glyph(char32_t wc) const {
    tv_ = Timeval::now();
    HFONT old_font = SelectFont(hdc_, hfont_);
    DWORD bytes = GetFontData(hdc_, 0, 0, NULL, 0);

    if (GDI_ERROR != bytes) {
        char b[bytes];
        GetFontData(hdc_, 0, 0, b, bytes);
        SelectFont(hdc_, old_font);

        try {
            True_type_font ttf(b, bytes);
            auto v = ttf.glyphs(std::u32string(1, wc));
            if (!v.empty()) { return v.front(); }
        }

        catch (...) {}
    }

    SelectFont(hdc_, old_font);
    return std::make_shared<Glyph_impl>();
}

void Font_win::invalidate() {
    hdc_ = NULL;
    if (hfont_) { DeleteFont(hfont_); }
    hfont_ = NULL;
    inv_cx_.drop();
}

// static
void Font_win::boot() {
    // List families.
    LOGFONTW lf;
    std::memset(&lf, 0, sizeof(lf));
    lf.lfCharSet = DEFAULT_CHARSET;
    lf.lfOutPrecision = OUT_TT_ONLY_PRECIS;
    lf.lfQuality = DEFAULT_QUALITY;
    lf.lfPitchAndFamily = FF_DONTCARE;

    if (HDC hdc = CreateDC("DISPLAY", NULL, NULL, NULL)) {
        EnumFontFamiliesExW(hdc, &lf, lister, 0, 0);
        DeleteDC(hdc);
    }

    // Find nice fonts.
    std::vector<ustring> nicev = { "Trebuchet MS", "Tahoma", "Times New Roman", "Arial", "Noto Sans", "Droid Sans", "DejaVu Sans" };
    double pts = 10.0;

    NONCLIENTMETRICS ncm;
    std::memset(&ncm, 0, sizeof ncm);
    ncm.cbSize = sizeof ncm;

    if (SystemParametersInfo(SPI_GETNONCLIENTMETRICS, sizeof ncm, &ncm, FALSE)) {
        if (HFONT hfont = CreateFontIndirect(&ncm.lfMessageFont)) {
            if (HDC hdc = CreateDC("DISPLAY", NULL, NULL, NULL)) {
                HFONT old_font = SelectFont(hdc, hfont);
                TEXTMETRIC tm;

                if (GetTextMetrics(hdc, &tm)) {
                    int pt = 0;
                    if (ncm.lfMessageFont.lfHeight < 0) { pt = -MulDiv(ncm.lfMessageFont.lfHeight+tm.tmInternalLeading, 72, GetDeviceCaps(hdc, LOGPIXELSY)); }
                    else if (ncm.lfMessageFont.lfHeight > 0) { pt = MulDiv(ncm.lfMessageFont.lfHeight, 72, GetDeviceCaps(hdc, LOGPIXELSY)); }
                    ustring spec = tau::Font::build(ncm.lfMessageFont.lfFaceName, "Regular", pt);
                    ustring fam = Font::family(spec);

                    if (fonts_.end() != std::find_if(fonts_.begin(), fonts_.end(), [&fam](const Win_font & wf) { return str_similar(fam, wf.family_); } )) {
                        nicev.push_back(fam);
                        pts = std::max(pts, Font::points(spec));
                    }
                }

                SelectFont(hdc, old_font);
                DeleteDC(hdc);
            }

            DeleteFont(hfont);
        }
    }

    for (const ustring & s: nicev) {
        if (fonts_.end() != std::find_if(fonts_.begin(), fonts_.end(), [s](const Win_font & wf) { return str_similar(s, wf.family_); } )) {
            font_normal_ = Font::at_least(s, pts);
            break;
        }
    }

    if (font_normal_.empty() && !fonts_.empty()) {
        font_normal_ = Font::at_least(fonts_.front().family_, pts);
    }

    nicev = { "Lucida Console", "Courier New", "Consolas", "Noto Mono", "Liberation Mono", "FreeMono", "Times New" };

    for (const ustring & s: nicev) {
        if (fonts_.end() != std::find_if(fonts_.begin(), fonts_.end(), [s](const Win_font & wf) { return str_similar(s, wf.orig_); } )) {
            font_mono_ = Font::at_least(s, pts);
            break;
        }
    }

    if (font_mono_.empty() && !font_normal_.empty()) {
        font_mono_ = Font::set_face(font_normal_, "Mono");
    }
}

// static
std::vector<ustring> Font_win::list_families() {
    std::vector<ustring> v;

    for (auto & wf: fonts_) {
        if (!str_similar(wf.family_, v)) {
            v.emplace_back(wf.family_);
        }
    }

    std::sort(v.begin(), v.end());
    return v;
}

// static
std::vector<ustring> Font_win::list_faces(const ustring & family) {
    std::vector<ustring> v;
    std::unique_lock lock(smx_);
    auto i = std::find_if(fonts_.begin(), fonts_.end(), [&family](const Win_font & wf) { return family == wf.family_; } );

    if (i != fonts_.end()) {
        v = i->faces_;
        const std::vector<ustring> ws = { "ExtraLight", "Light", "Medium", "SemiBold", "ExtraBold", "Bold", "Heavy" };
        for (auto & s: ws) { v.emplace_back(s); v.emplace_back(s+" Italic"); }
    }

    if (!str_similar("Regular", v)) { v.emplace_back("Regular"); }
    std::sort(v.begin(), v.end());
    return v;
}

// static
ustring Font_win::normal() {
    return font_normal_;
}

// static
ustring Font_win::mono() {
    return font_mono_;
}

} // namespace tau

//END

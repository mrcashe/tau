// ----------------------------------------------------------------------------
// SPDX-License-Identifier: BSD-2-Clause
// Copyright © 2014-2024 Konstantin Shmelkov <mrcashe@gmail.com>.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
// THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
// PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
// CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
// EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
// PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
// OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
// WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
// OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
// EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// ----------------------------------------------------------------------------

/// @file box-impl.cc The Box_impl class implementation.
/// The header is box-impl.hh.

#include <box-impl.hh>
#include <algorithm>
#include <iostream>

namespace tau {

Box_impl::Box_impl(unsigned spacing):
    spacing_(spacing),
    orient_(Orientation::RIGHT)
{
    init();
}

Box_impl::Box_impl(Orientation orient, unsigned spacing):
    spacing_(spacing),
    orient_(orient)
{
    init();
}

Box_impl::Box_impl(Align align, unsigned spacing):
    spacing_(spacing),
    align_(align),
    orient_(Orientation::RIGHT)
{
    init();
}

Box_impl::Box_impl(Orientation orient, Align align, unsigned spacing):
    spacing_(spacing),
    align_(align),
    orient_(orient)
{
    init();
}

void Box_impl::init() {
    signal_arrange_.connect(fun(this, &Box_impl::arrange));
    signal_display_in_.connect(fun(this, &Box_impl::update_requisition));
    signal_size_changed_.connect(fun(this, &Box_impl::arrange));
    signal_show_.connect(fun(this, &Box_impl::arrange));    // FIXME Doesn't work properly without it!
    signal_take_focus_.connect(fun(this, &Box_impl::on_take_focus));
    signal_children_changed_.connect(fun(this, &Box_impl::update_requisition));
    signal_children_changed_.connect(fun(this, &Box_impl::queue_arrange));
    action_focus_next().connect_before(fun(this, &Box_impl::on_focus_next));
    action_focus_previous().connect_before(fun(this, &Box_impl::on_focus_previous));
}

Box_impl::~Box_impl() {
    destroy();
    clear();
    if (signal_orientation_changed_) { delete signal_orientation_changed_; }
}

void Box_impl::update_requisition() {
    nvisible_ = 0;
    nsh_ = req_ = 0;
    unsigned x = 0, y = 0;

    for (auto & hol: holders_) {
        if (hol.wp_->hidden()) {
            hol.req_.reset();
            hol.mg_.reset();
        }

        else {
            ++nvisible_;
            hol.req_ = hol.wp_->required_size();
            hol.req_.update(hol.wp_->size_hint(), true);
            hol.req_.update_max(hol.wp_->min_size_hint());
            hol.req_.update_min(hol.wp_->max_size_hint(), true);
            double asp = hol.wp_->required_aspect_ratio();
            if (hol.wp_->has_aspect_ratio_hint()) { hol.req_.apply_aspect_ratio(asp); }
            Margin m = hol.wp_->margin_hint();
            hol.mg_.update(m.left+m.right, m.top+m.bottom);

            if (horizontal()) {
                if (hol.wp_->has_aspect_ratio_hint() || hol.sh_) {
                    ++nsh_;
                    req_ += hol.req_.width();
                }

                x += hol.req_.width()+hol.mg_.width();
                req_ += hol.mg_.width();
                y = std::max(y, (hol.req_.height()+hol.mg_.height()));
            }

            else {
                if (hol.wp_->has_aspect_ratio_hint() || hol.sh_) {
                    ++nsh_;
                    req_ += hol.req_.height();
                }

                y += hol.req_.height()+hol.mg_.height();
                req_ += hol.mg_.height();
                x = std::max(x, (hol.req_.width()+hol.mg_.width()));
            }
        }
    }

    unsigned nspace = nvisible_ > 1 ? spacing_*(nvisible_-1) : 0;
    req_ += nspace;
    if (horizontal()) { x += nspace; }
    else { y += nspace; }
    require_size(x, y);
}

void Box_impl::arrange() {
    Point origin;
    Size  sz, lz = logical_size();
    unsigned rem = 0, nextra = nvisible_-nsh_;
    int avail;

    if (horizontal()) {
        unsigned sw = lz.width();
        int x = Orientation::RIGHT == orient_ ? 0 : sw;
        avail = sw > req_ ? sw-req_ : 0;

        if (avail > 0 && nsh_ == nvisible_) {
            if (Align::FILL == align_) {
                nextra = nvisible_;
            }

            else if (Orientation::LEFT == orient_) {
                if (Align::END == align_) { x -= avail; }
                else if (Align::CENTER == align_) { x -= avail/2; }
            }

            else {
                if (Align::END == align_) { x += avail; }
                else if (Align::CENTER == align_) { x += avail/2; }
            }
        }

        if (nextra > 0) {
            rem = avail > 0 ? avail%nextra : 0;        // Calculate rem before nextra!
            nextra = avail > 0 ? avail/nextra : 0;
        }

        for (auto & hol: holders_) {
            if (!hol.wp_->hidden()) {
                Size z { hol.req_.width(), lz.height() };
                if (hol.wp_->has_aspect_ratio_hint()) { z.apply_aspect_ratio(hol.wp_->required_aspect_ratio(), z.height() > z.width()); }
                Margin m = hol.wp_->margin_hint();
                Size mg { m.left+m.right, m.top+m.bottom };
                unsigned w;

                if (hol.wp_->has_aspect_ratio_hint() || hol.sh_) {
                    w = z.width();
                    if (Align::FILL == align_ && nvisible_ == nsh_) { w += nextra; }
                }

                else {
                    w = nextra;
                    if (0 != rem) { ++w, --rem; }
                }

                origin.update((Orientation::RIGHT == orient_ ? x : x-w-mg.width()), 0);
                origin.translate(m.left, m.top);
                sz.update(w, lz.height()-mg.height());
                hol.wp_->invalidate();
                if (update_child_bounds(hol.wp_, origin, sz)) { hol.wp_->invalidate(); }
                int d = spacing_+w+mg.width();
                x += Orientation::RIGHT == orient_ ? d : -d;
            }
        }
    }

    else {
        unsigned sh = lz.height();
        int y = Orientation::DOWN == orient_ ? 0 : sh;
        avail = sh > req_ ? sh-req_ : 0;

        if (avail > 0 && nsh_ == nvisible_) {
            if (Align::FILL == align_) {
                nextra = nvisible_;
            }

            else if (Orientation::UP == orient_) {
                if (Align::END == align_) { y -= avail; }
                else if (Align::CENTER == align_) { y -= avail/2; }
            }

            else {
                if (Align::END == align_) { y += avail; }
                else if (Align::CENTER == align_) { y += avail/2; }
            }
        }

        if (nextra > 0) {
            rem = avail > 0 ? avail%nextra : 0;        // Calculate rem before nextra!
            nextra = avail > 0 ? avail/nextra : 0;
        }

        for (auto & hol: holders_) {
            if (!hol.wp_->hidden()) {
                Size z { lz.width(), std::min(lz.height(), hol.req_.height()) };
                if (hol.wp_->has_aspect_ratio_hint()) { z.apply_aspect_ratio(hol.wp_->required_aspect_ratio(), z.height() < z.width()); }
                Margin m = hol.wp_->margin_hint();
                Size mg { m.left+m.right, m.top+m.bottom };
                unsigned h;

                if (hol.wp_->has_aspect_ratio_hint() || hol.sh_) {
                    h = z.height();
                    if (Align::FILL == align_ && nvisible_ == nsh_) { h += nextra; }
                }

                else {
                    h = nextra;
                    if (0 != rem) { ++h, --rem; }
                }

                origin.update(0, (Orientation::DOWN == orient_ ? y : y-h-mg.height()));
                origin.translate(m.left, m.top);
                sz.update(lz.width()-mg.width(), h);
                invalidate(Rect(hol.wp_->origin(), hol.wp_->size()));
                if (update_child_bounds(hol.wp_, origin, sz)) { invalidate(Rect(hol.wp_->origin(), hol.wp_->size())); }
                int d = spacing_+h+mg.height();
                y += Orientation::DOWN == orient_ ? d : -d;
            }
        }
    }
}

void Box_impl::on_hints(Hiter hi, Hints op) {
    if (!in_shutdown()) {
        if (Hints::SHOW == op) {
            update_requisition();
            queue_arrange();
            invalidate();
        }

        else if (Hints::HIDE == op) {
            update_child_bounds(hi->wp_, INT_MIN, INT_MIN);
            update_requisition();
            queue_arrange();
            invalidate();
        }

        else {
            update_requisition();
            queue_arrange();
            invalidate();
        }
    }
}

void Box_impl::new_child(Hiter hi, Widget_ptr wp, bool shrink) {
    update_child_bounds(wp.get(), INT_MIN, INT_MIN);
    hi->wp_ = wp.get();
    hi->sh_ = shrink;
    hi->cx_ = wp->signal_hints_changed().connect(tau::bind_front(fun(this, &Box_impl::on_hints), hi));
    make_child(wp);
}

void Box_impl::append(Widget_ptr wp, bool shrink) {
    chk_parent(wp);
    new_child(holders_.insert(holders_.end(), Holder()), wp, shrink);
}

void Box_impl::prepend(Widget_ptr wp, bool shrink) {
    chk_parent(wp);
    new_child(holders_.insert(holders_.begin(), Holder()), wp, shrink);
}

void Box_impl::insert_before(Widget_ptr wp, const Widget_impl * other, bool shrink) {
    chk_parent(wp);
    auto i = holders_.begin();

    while (i != holders_.end()) {
        if (other == i->wp_) { break; }
        ++i;
    }

    new_child(holders_.insert(i, Holder()), wp, shrink);
}

void Box_impl::insert_after(Widget_ptr wp, const Widget_impl * other, bool shrink) {
    chk_parent(wp);
    auto i = holders_.begin();

    while (i != holders_.end()) {
        if (other == i->wp_) { ++i; break; }
        ++i;
    }

    new_child(holders_.insert(i, Holder()), wp, shrink);
}

void Box_impl::remove(Widget_impl * wp) {
    auto i = std::find_if(holders_.begin(), holders_.end(), [wp](auto & p) { return wp == p.wp_; } );

    if (i != holders_.end()) {
        holders_.erase(i);
        unparent({ wp });
    }
}

void Box_impl::remove_after(const Widget_impl * other) {
    if (holders_.size() > 1) {
        auto i = std::find_if(holders_.cbegin(), holders_.cend(), [other](auto & p) { return other == p.wp_; } );

        if (i != holders_.cend()) {
            auto j = ++i;

            if (j != holders_.end()) {
                auto wp = j->wp_;
                holders_.erase(j);
                unparent({ wp });
            }
        }
    }
}

void Box_impl::remove_before(const Widget_impl * other) {
    if (holders_.size() > 1) {
        auto i = std::find_if(holders_.cbegin(), holders_.cend(), [other](auto & p) { return other == p.wp_; } );

        if (i != holders_.cbegin()) {
            auto j = --i;

            if (j != holders_.begin()) {
                auto wp = j->wp_;
                holders_.erase(j);
                unparent({ wp });
            }
        }
    }
}

void Box_impl::remove_front() {
    if (!holders_.empty()) {
        auto wp = holders_.front().wp_;
        holders_.pop_front();
        unparent({ wp });
    }
}

void Box_impl::remove_back() {
    if (!holders_.empty()) {
        auto wp = holders_.back().wp_;
        holders_.pop_back();
        unparent({ wp });
    }
}

void Box_impl::clear() {
    holders_.clear();
    unparent_all();
}

void Box_impl::align(Align align) {
    if (align_ != align) {
        align_ = align;
        if (!empty()) { update_requisition(); queue_arrange(); }
    }
}

bool Box_impl::shrunk(const Widget_impl * wp) const noexcept {
    for (auto iter = holders_.begin(); iter != holders_.end(); ++iter) {
        if (wp == iter->wp_) {
            return iter->sh_;
        }
    }

    return false;
}

void Box_impl::shrink(Widget_impl * wp) {
    for (auto iter = holders_.begin(); iter != holders_.end(); ++iter) {
        if (wp == iter->wp_) {
            if (!iter->sh_) {
                iter->sh_ = true;
                update_requisition();
                queue_arrange();
            }

            break;
        }
    }
}

void Box_impl::expand(Widget_impl * wp) {
    for (auto iter = holders_.begin(); iter != holders_.end(); ++iter) {
        if (wp == iter->wp_) {
            if (iter->sh_) {
                iter->sh_ = false;
                update_requisition();
                queue_arrange();
            }

            break;
        }
    }
}

void Box_impl::shrink_all() {
    bool changed = false;

    for (auto iter = holders_.begin(); iter != holders_.end(); ++iter) {
        if (!iter->sh_) {
            iter->sh_ = true;
            changed = true;
        }
    }

    if (changed) {
        update_requisition();
        queue_arrange();
    }
}

void Box_impl::expand_all() {
    bool changed = false;

    for (auto iter = holders_.begin(); iter != holders_.end(); ++iter) {
        if (iter->sh_) {
            iter->sh_ = false;
            changed = true;
        }
    }

    if (changed) {
        update_requisition();
        queue_arrange();
    }
}

void Box_impl::orientation(Orientation orient) {
    if (orient_ != orient) {
        orient_ = orient;
        if (signal_orientation_changed_) { (*signal_orientation_changed_)(); }
        if (!empty()) { update_requisition(); queue_arrange(); }
    }
}

void Box_impl::set_spacing(unsigned spacing) {
    if (spacing_ != spacing) {
        spacing_ = spacing;
        if (!empty()) { update_requisition(); queue_arrange(); }
    }
}

bool Box_impl::on_take_focus() {
    if (focused_child_ && focused_child_->take_focus()) { return true; }

    if (Orientation::RIGHT == orient_ || Orientation::DOWN == orient_) {
        return std::any_of(holders_.begin(), holders_.end(), [](auto & hol) { return hol.wp_->take_focus(); });
    }

    return std::any_of(holders_.rbegin(), holders_.rend(), [](auto & hol) { return hol.wp_->take_focus(); });
}

bool Box_impl::on_focus_next() {
    return Orientation::RIGHT == orient_ || Orientation::DOWN == orient_ ? on_forward() : on_reverse();
}

bool Box_impl::on_focus_previous() {
    return Orientation::LEFT == orient_ || Orientation::UP == orient_ ? on_forward() : on_reverse();
}

bool Box_impl::on_forward() {
    auto i = std::find_if(holders_.begin(), holders_.end(), [](auto & hol) { return hol.wp_->focused(); });

    if (i != holders_.end()) {
        return !std::any_of(++i, holders_.end(), [](auto & hol) { return hol.wp_->take_focus(); });
    }

    return true;
}

bool Box_impl::on_reverse() {
    auto i = std::find_if(holders_.rbegin(), holders_.rend(), [](auto & hol) { return hol.wp_->focused(); });

    if (i != holders_.rend()) {
        return !std::any_of(++i, holders_.rend(), [](auto & hol) { return hol.wp_->take_focus(); });
    }

    return true;
}

signal<void()> & Box_impl::signal_orientation_changed() {
    if (!signal_orientation_changed_) { signal_orientation_changed_ = new signal<void()>; }
    return *signal_orientation_changed_;
}

} // namespace tau

//END

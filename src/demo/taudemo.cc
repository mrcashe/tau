// ----------------------------------------------------------------------------
// SPDX-License-Identifier: BSD-2-Clause
// Copyright © 2014-2024 Konstantin Shmelkov <mrcashe@gmail.com>.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
// THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
// PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
// CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
// EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
// PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
// OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
// WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
// OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
// EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// ----------------------------------------------------------------------------

#include <tau.hh>
#include <tau/gettext.hh>
#include <cstdlib>
#include <cstring>
#include <fstream>
#include <iomanip>
#include <iostream>
#include <map>
#include <mutex>
#include <random>
#include <thread>

namespace {

tau::Key_file kstate_;
std::vector<std::thread> threads_;

} // anonymous namespace

void run();

// ----------------------------------------------------------------------------
// ----------------------------------------------------------------------------

struct Main: tau::Toplevel {
    tau::Loop                       loop_;
    tau::Notebook                   notebook_       { tau::Side::RIGHT };
    std::random_device              rdev_;
    std::mt19937                    rng_            { rdev_() };

    tau::Action                     escape_ax_      { "Escape",     fun(this, &Main::close) };
    tau::Action                     next_page_ax_   { "<Alt>Down",  fun(this, &Main::on_next_page) };
    tau::Action                     prev_page_ax_   { "<Alt>Up",    fun(this, &Main::on_prev_page) };
    tau::Action                     n_action_       { "F12",        fun(this, &Main::new_thread) };

    tau::Counter                    ymax_           { 0, 1599, 0 };
    tau::Counter                    xmax_           { 0, 1599, 0 };
    tau::Counter                    ymin_           { 0,  999, 0 };
    tau::Counter                    xmin_           { 0,  999, 0 };

    std::size_t                     prev_row_       = 0;
    tau::Progress                   progress_       { tau::Border::SOLID };
    tau::Widget                     color_cont_;
    std::vector<std::string_view>   color_names_;

    struct Color_widgets {
        tau::Label name             { tau::Align::START };
        tau::Widget     w;
        tau::Label      value;
    };

    std::vector<Color_widgets> color_widgets_;

    int init_sysinfo_page(int pg) {
        tau::Roller roller;
        int page = notebook_.append_page(roller, pages_[pg].title);
        tau::Text text(tau::str_sysinfo(), tau::Align::START, tau::Align::START);
        text.conf().font().set(tau::Font::mono());
        text.conf().redirect(tau::Conf::BACKGROUND, tau::Conf::WHITESPACE_BACKGROUND);
        text.allow_select();
        roller.insert(text);
        return page;
    }

    bool on_list_delete(tau::List * list) {
        list->remove(list->current());
        return true;
    }

    void fill_list(tau::List * list) {
        // tau::Timeval t1 = tau::Timeval::now();

        for (int i = 0; i < 64; ++i) {
            tau::Label t(tau::str_format("Y=", i));
            int row = list->append_row(t);

            for (int j = -1; j < 3; ++j) {
                tau::Label tt(tau::str_format(i, ':', j));
                list->insert(row, tt, j);
            }
        }

        list->take_focus();
        list->select_front();
        // std::cout << __func__ << " filled in " << (tau::Timeval::now()-t1)/1000 << " ms" << std::endl;
    }

    int init_list_page(int pg) {
        tau::Roller roller;
        int n = notebook_.append_page(roller, pages_[pg].title);
        auto * list = new tau::List();
        roller.insert(manage(list));
        list->allocate_accel("Delete")->connect(bind_back(fun(this, &Main::on_list_delete), list));
        list->signal_display_in().connect(bind_back(fun(this, &Main::fill_list), list));
        roller.signal_take_focus().connect(fun(list, &tau::Widget::take_focus), true);
        return n;
    }

    void on_twins_ratio(double ratio, const tau::ustring & key) {
        kstate_.set_double(key, ratio);
    }

    int init_twins_page(int pg) {
        tau::Label label(pages_[pg].title);
        label.set_tooltip(dgettext("tau", "This page shows\nthe Twins container example"));
        tau::Frame frame(pages_[pg].title, tau::Border::DOUBLE, { "Crimson" }, 1, 12);
        frame.hint_margin(5);
        frame.conf().set(tau::Conf::BACKGROUND, "Lavender");
        int page = notebook_.append_page(frame, label);

        tau::Table table(5);
        table.hint_margin(4);
        table.conf().set(tau::Conf::BACKGROUND, "PeachPuff");
        table.conf().font().resize(7);
        frame.insert(table);

        {   tau::Twins twins(tau::Orientation::DOWN, kstate_.get_double("ratio_1", 0.4));
            twins.signal_ratio_changed().connect(bind_back(fun(this, &Main::on_twins_ratio), "ratio_1"));
            table.put(twins, 0, 0);
            tau::Text first("First@Orientation::DOWN"), second("Second@Orientation::DOWN");
            first.conf().set(tau::Conf::BACKGROUND, "LightBlue");
            second.conf().set(tau::Conf::BACKGROUND, "Lime");
            first.hint_margin(10);
            second.hint_margin(12);
            twins.insert(first, second);
        }

        {   tau::Twins twins(tau::Orientation::UP, kstate_.get_double("ratio_2", 0.4));
            twins.signal_ratio_changed().connect(bind_back(fun(this, &Main::on_twins_ratio), "ratio_2"));
            table.put(twins, 0, 1);
            tau::Text first("First@Orientation::UP"), second("Second@Orientation::UP");
            first.conf().set(tau::Conf::BACKGROUND, "LightBlue");
            second.conf().set(tau::Conf::BACKGROUND, "Lime");
            first.hint_margin(10);
            second.hint_margin(12);
            twins.insert(first, second);
        }

        {   tau::Twins twins(tau::Orientation::LEFT, kstate_.get_double("ratio_3", 0.4));
            twins.signal_ratio_changed().connect(bind_back(fun(this, &Main::on_twins_ratio), "ratio_3"));
            table.put(twins, 1, 0);
            tau::Text first("First@Orientation::LEFT"), second("Second@Orientation::LEFT");
            first.conf().set(tau::Conf::BACKGROUND, "LightBlue");
            second.conf().set(tau::Conf::BACKGROUND, "Lime");
            first.hint_margin(10);
            second.hint_margin(12);
            twins.insert(first, second);
        }

        {   tau::Twins twins(tau::Orientation::RIGHT, kstate_.get_double("ratio_4", 0.4));
            twins.signal_ratio_changed().connect(bind_back(fun(this, &Main::on_twins_ratio), "ratio_4"));
            table.put(twins, 1, 1);
            tau::Text first("First@Orientation::RIGHT"), second("Second@Orientation::RIGHT");
            first.conf().set(tau::Conf::BACKGROUND, "LightBlue");
            second.conf().set(tau::Conf::BACKGROUND, "Lime");
            first.hint_margin(10);
            second.hint_margin(12);
            twins.insert(first, second);
        }

        return page;
    }

    void on_bps_changed(const tau::ustring & s) {
        kstate_.set_string("bps", s);
    }

    int init_controls_page(int pg) {
        tau::Table table(2);
        table.set_column_margin(3, 6, 0), table.set_column_margin(6, 6, 0);
        table.signal_take_focus().connect(fun(this, &Main::enabled), true);
        table.hint_margin(4);
        table.align_column(7, tau::Align::CENTER);
        int y = 0, page = notebook_.append_page(table, pages_[pg].title);

        // Create Button with icon using icon name and size.
        tau::Button push_button("tau::Button", tau::Icon::SMALL, "document-save");
        table.put(push_button, tau::Align::FILL, tau::Align::FILL, 0, y, 1, 1, true, true);

        // Create Toggle using shared resource.
        if (auto bits = tau::shared_resource("resource/alpha.ico")) {
            auto pix = tau::Pixmap::create(bits->data(), bits->size());
            tau::Image img(pix, true);
            tau::Toggle toggle_button(img, "tau::Toggle");
            toggle_button.conf().font().make_bold();
            toggle_button.conf().set(tau::Conf::FOREGROUND, "White");
            toggle_button.conf().set(tau::Conf::BUTTON_BACKGROUND, "DeepSkyBlue");
            table.put(toggle_button, tau::Align::FILL, tau::Align::FILL, 1, y, 1, 1, true, true);
        }

        // Counter.
        tau::Counter counter(kstate_.get_integer("counter"), 1247, 1);
        counter.prepend("tau::Counter", 2, 2);
        counter.append("rpm", tau::Color("Brown"), 2, 2);
        table.put(counter, tau::Align::FILL, tau::Align::FILL, 3, y, 2, 1, true, true);
        counter.signal_value_changed().connect(tau::bind_front(fun(kstate_, static_cast<void(tau::Key_file::*)(std::string_view,double)>(&tau::Key_file::set_double)), "counter"));

        // Cycle.
        tau::Cycle cycle;
        cycle.prepend(dgettext("tau", "Speed")+':', tau::Color("Crimson"), 2, 4);
        cycle.prepend("tau::Cycle", 2, 2);
        cycle.append("bps", tau::Color("Brown"), 2, 2);
        table.put(cycle, tau::Align::FILL, tau::Align::FILL, 6, y, 3, 1, true, true);

        tau::Widget_ptr wp; auto current = kstate_.get_string("bps");
        const long bauds[] { 1200, 2400, 4800, 9600, 14400, 19200, 28800, 38400, 57600, 115200, 230400, 460800 };

        for (int i: bauds) {
            auto s = tau::str_format(i);
            tau::Label label(s);
            label.signal_select().connect(bind_back(tau::bind_front(fun(kstate_, static_cast<void(tau::Key_file::*)(std::string_view,const tau::ustring &)>(&tau::Key_file::set_string)), "bps"), s));
            label.conf().set(tau::Conf::BACKGROUND, "Yellow");
            cycle.add(label);
            if (s == current) { wp = label.ptr(); }
        }

        if (wp) { tau::Widget(wp).show(); }

        // Checks.
        ++y;
        auto label = tau::Label("tau::Check(XSTYLE)", tau::Align::END); table.put(label, tau::Align::END, tau::Align::CENTER, 0, y, 2, 1, true, true);
        tau::Check check(tau::Check::XSTYLE, tau::Border::SOLID, kstate_.get_boolean("xcheck")); table.put(check, tau::Align::START, tau::Align::CENTER, 2, y, 1, 1, true, true);
        check.signal_check().connect(bind_back(tau::bind_front(fun(kstate_, static_cast<void(tau::Key_file::*)(std::string_view,bool)>(&tau::Key_file::set_boolean)), "xcheck"), true));
        check.signal_uncheck().connect(bind_back(tau::bind_front(fun(kstate_, static_cast<void(tau::Key_file::*)(std::string_view,bool)>(&tau::Key_file::set_boolean)), "xcheck"), false));

        label = tau::Label("tau::Check(VSTYLE)"); table.put(label, tau::Align::END, tau::Align::CENTER, 3, y, 2, 1, true, true);
        tau::Check vcheck(tau::Check::VSTYLE, kstate_.get_boolean("vcheck")); table.put(vcheck, tau::Align::START, tau::Align::CENTER, 5, y, 1, 1, true, true);
        vcheck.signal_check().connect(bind_back(tau::bind_front(fun(kstate_, static_cast<void(tau::Key_file::*)(std::string_view,bool)>(&tau::Key_file::set_boolean)), "vcheck"), true));
        vcheck.signal_uncheck().connect(bind_back(tau::bind_front(fun(kstate_, static_cast<void(tau::Key_file::*)(std::string_view,bool)>(&tau::Key_file::set_boolean)), "vcheck"), false));

        label = tau::Label("tau::Check(QSTYLE)"); table.put(label, tau::Align::END, tau::Align::CENTER, 6, y, 2, 1, true, true);
        tau::Check qcheck(tau::Check::QSTYLE, kstate_.get_boolean("qcheck")); table.put(qcheck, tau::Align::START, tau::Align::CENTER, 8, y, 1, 1, true, true);
        qcheck.signal_check().connect(bind_back(tau::bind_front(fun(kstate_, static_cast<void(tau::Key_file::*)(std::string_view,bool)>(&tau::Key_file::set_boolean)), "qcheck"), true));
        qcheck.signal_uncheck().connect(bind_back(tau::bind_front(fun(kstate_, static_cast<void(tau::Key_file::*)(std::string_view,bool)>(&tau::Key_file::set_boolean)), "qcheck"), false));

        ++y;
        label = tau::Label(dgettext("tau", "tau::Roller below"));
        label.conf().set(tau::Conf::FOREGROUND, "Crimson"); label.conf().font().resize(7); table.put(label, 2, y, 4, 1, false, true);
        table.mark_back(tau::Color("#FBCDF6"), { 2, y, 6, y+2 });

        // Radio checks.
        ++y;
        label = tau::Label("tau::Check(RSTYLE)"); table.put(label, tau::Align::END, tau::Align::CENTER, 0, y, 2, 1, true, true);
        tau::Frame frame { tau::Border::INSET }; table.put(frame, tau::Align::START, tau::Align::FILL, 2, y, 4, 1, false, true);
        tau::Roller roller(tau::Orientation::WEST); roller.hint_margin(2); frame.insert(roller);
        tau::Box hb(3); hb.hint_margin(2, 2, 0, 0); roller.insert(hb);
        tau::Check check1(tau::Check::RSTYLE, true); hb.append(check1);
        for (int n = 0; n < 15; ++n) { tau::Check check2(tau::Check::RSTYLE); check2.join(check1); hb.append(check2); }

        // Spinners.
        label = tau::Label("tau::Spinner(LINE)"); table.put(label, tau::Align::END, tau::Align::CENTER, 6, y, 2, 1, true, true);
        label = tau::Label("tau::Spinner(BALL)"); table.put(label, tau::Align::END, tau::Align::CENTER, 6, y+1, 2, 1, true, true);
        tau::Spinner spinner1(tau::Spinner::LINE), spinner2;
        spinner2.conf().color(tau::Conf::FOREGROUND) = tau::Color("DodgerBlue");
        table.put(spinner1, tau::Align::START, tau::Align::FILL, 8, y), table.put(spinner2, tau::Align::START, tau::Align::FILL, 8, y+1);
        spinner1.start(), spinner2.start();

        // Progress.
        ++y;
        label = tau::Label("tau::Progress"); table.put(label, tau::Align::END, tau::Align::CENTER, 0, y, 1, 1, true, true);
        progress_.set_format("%$%% complete"), progress_.set_precision(1), table.put(progress_, 1, y, 5, 1, false, true);

        return page;
    }

    void set_row_color(std::size_t row, const tau::ustring & cname) {
        tau::Color c(cname);
        color_widgets_[row].name.assign(cname);
        color_widgets_[row].w.conf().item(tau::Conf::BACKGROUND).set(cname);
        color_widgets_[row].value.assign(c.html());
    }

    int init_colors_page(int pg) {
        color_names_ = tau::Color::list_css_names();
        color_widgets_.resize(color_names_.size());
        tau::Table table;
        color_cont_ = table;
        table.set_spacing(6, 5);
        tau::Scroller scroller;
        scroller.insert(table);
        scroller.hint_margin(3, 3, 4, 4);
        tau::Box box(tau::Orientation::RIGHT, 2);
        tau::Label label(pages_[pg].title);
        label.set_tooltip(dgettext("tau", "This page shows\ncolor boxes"));
        int page = notebook_.append_page(box, label);
        box.append(scroller);
        tau::Slider slider(scroller);
        box.append(slider, true);
        std::size_t row = 0;

        for (auto & cname: color_names_) {
            color_widgets_[row].w.hint_min_size(16, 0);
            color_widgets_[row].w.conf().redirect(tau::Conf::FONT, tau::Conf::MONOSPACE_FONT);
            color_widgets_[row].value.conf().redirect(tau::Conf::FONT, tau::Conf::MONOSPACE_FONT);
            table.put(color_widgets_[row].name, 0, row, 1, 1, true, true);
            table.put(color_widgets_[row].w, tau::Align::FILL, tau::Align::FILL, 1, row, 1, 1);
            table.put(color_widgets_[row].value, 2, row, 1, 1, true, true);
            set_row_color(row, cname);
            ++row;
        }

        return page;
    }

    int init_cursors_page(int pg) {
        tau::Roller roller;
        int page = notebook_.append_page(roller, pages_[pg].title);
        tau::Table table;
        table.set_column_spacing(5);
        table.set_row_spacing(5);
        roller.insert(table);

        tau::ustring path = tau::path_build(tau::path_share(), "cursors");

        if (tau::file_is_dir(path)) {
            for (auto & s: tau::file_glob(tau::path_build(path, "*"))) {
                try {
                    auto sizes = tau::Cursor::list_sizes(s);

                    if (!sizes.empty()) {
                        auto rng = table.span();
                        tau::Label title_text(tau::path_notdir(s));
                        table.put(title_text, -2, rng.ymax, 1, sizes.size(), false, true);

                        for (auto n: sizes) {
                            if (auto cur = tau::Cursor::load_from_file(s, n)) {
                                tau::Image img;
                                img.set_transparent();
                                table.put(img, 0, rng.ymax, 1, 1, true, true);
                                std::size_t n_frames = cur.frame_count();
                                tau::Size sz;

                                for (std::size_t fr = 0; fr < n_frames; ++fr) {
                                    if (tau::Pixmap pix = cur.pixmap(fr)) {
                                        img.add_pixmap(pix, cur.delay(fr));
                                        sz |= pix.size();
                                    }
                                }

                                tau::Label size_text(tau::str_format("Size: ", cur.size(), "\n(", sz.width(), "x" , sz.height(), " px)"));
                                table.put(size_text, -1, rng.ymax, 1, 1, true, true);
                                ++rng.ymax;
                            }
                        }
                    }
                }

                catch (tau::exception & x) {
                    std::cerr << "** " << s << ": " << x.what() << std::endl;
                }
            }
        }

        return page;
    }

    void on_colorsel(const tau::Color & c) {
        kstate_.set_string("colorsel", c.html());
    }

    int init_colorsel_page(int pg) {
        tau::Colorsel colorsel(tau::Color(kstate_.get_string("colorsel", "Blue")));
        colorsel.action_cancel().disable();
        colorsel.hint_margin(4);
        colorsel.signal_color_changed().connect(tau::fun(this, &Main::on_colorsel));
        return notebook_.append_page(colorsel, pages_[pg].title);
    }

    struct Page {
        tau::ustring        title;
        int                 page;
        tau::slot<int(int)> x;
    };

    std::vector<Page>   pages_ {
        { "Controls",   0, tau::fun(this, &Main::init_controls_page) },
        { "List",       1, tau::fun(this, &Main::init_list_page) },
        { "Cursors",    2, tau::fun(this, &Main::init_cursors_page) },
        { "Colors",     3, tau::fun(this, &Main::init_colors_page) },
        { "Twins",      4, tau::fun(this, &Main::init_twins_page) },
        { "Colorsel",   5, tau::fun(this, &Main::init_colorsel_page) },
        { "Sysinfo",    6, tau::fun(this, &Main::init_sysinfo_page) }
    };

    void new_thread() {
        threads_.emplace_back(std::thread(run));
    }

    void save_pages() {
        tau::Key_section & sect = kstate_.section("pages");

        for (auto & pg: pages_) {
            kstate_.set_integer(sect, pg.title, pg.page);
            if (pg.page == notebook_.current_page()) { kstate_.set_string(sect, "current", pg.title); }
        }
    }

    void on_page_reordered(int new_page, int old_page) {
        for (auto & pg: pages_) {
            if (pg.page == old_page) { pg.page = new_page; }
            else if (pg.page == new_page) { pg.page = old_page; }
        }

        save_pages();
    }

    void on_page_changed(int n) {
        save_pages();
    }

    void on_minmax_changed(double) {
        unsigned xmin = xmin_.value(), ymin = ymin_.value(), xmax = xmax_.value(), ymax = ymax_.value();

        if (xmin >= 200 && ymin >= 200) {
            hint_min_size(xmin, ymin);
            std::vector<intmax_t> v = { xmin, ymin };
            kstate_.set_integers(kstate_.section("main"), "min_size", v);
        }

        else {
            kstate_.remove_key(kstate_.section("main"), "min_size");
        }

        if (xmax >= 300 && ymax >= 300) {
            hint_max_size(xmax, ymax);
            std::vector<intmax_t> v = { xmax, ymax };
            kstate_.set_integers(kstate_.section("main"), "max_size", v);
        }

        else {
            kstate_.remove_key(kstate_.section("main"), "max_size");
        }
    }

    void on_timer() {
        static int div = 0;

        if (progress_.visible() && 0 == div) {
            double value = progress_.value();
            value += 1.25;
            if (value > progress_.max_value()) { value = 0.0; }
            progress_.set_value(value);
        }

        if (color_cont_.visible()) {
            std::uniform_int_distribution<std::mt19937::result_type> dist(0, color_widgets_.size()-1);
            int row = dist(rng_);
            set_row_color(row, color_names_[prev_row_]);
            prev_row_ = row;
        }

        if (++div == 8) { div = 0; }
    }

    void on_next_page() {
        notebook_.show_next();
    }

    void on_prev_page() {
        notebook_.show_previous();
    }

    void on_geometry_changed() {
        std::vector<intmax_t> v = { position().x(), position().y(), size().iwidth(), size().iheight() };
        kstate_.set_integers(kstate_.section("main"), "geometry", v);
    }

    // ----------------------------------------------------------------------
    // Constructor
    // ----------------------------------------------------------------------

    Main(const tau::Rect & bounds):
        Toplevel(bounds)
    {
        loop_ = tau::Loop::this_loop();

        {
            auto v = kstate_.get_integers(kstate_.section("main"), "min_size");
            if (v.size() > 1) { hint_min_size(v[0], v[1]); }
        }

        {
            auto v = kstate_.get_integers(kstate_.section("main"), "max_size");
            if (v.size() > 1) { hint_max_size(v[0], v[1]); }
        }

        tau::Box box0(tau::Orientation::DOWN);
        insert(box0);
        notebook_.hint_margin(8);
        box0.append(notebook_);

        tau::Box ctlbox(tau::Orientation::LEFT, 4);
        ctlbox.hint_margin(2, 2, 8, 2);
        ctlbox.conf().set(tau::Conf::WHITESPACE_BACKGROUND, "BlanchedAlmond");
        ctlbox.conf().font().resize(7);
        ctlbox.conf().font("edit_font").resize(7);
        box0.append(ctlbox, true);

        {
            tau::Frame frm(tau::Border::INSET);
            frm.hint_margin(2);
            ctlbox.append(frm, true);

            tau::Box box(tau::Orientation::LEFT, 4);
            box.hint_margin(2);
            frm.insert(box);

            box.append(ymax_);
            ymax_.set_tooltip(dgettext("tau", "Sets maximal window height, in pixels"));
            ymax_.append("px", 2, 2);
            ymax_.prepend("h:", 2, 2);
            ymax_.assign(max_size_hint().height());
            ymax_.set_step_value(10);
            ymax_.signal_value_changed().connect(fun(this, &Main::on_minmax_changed));

            box.append(xmax_);
            xmax_.set_tooltip(dgettext("tau", "Sets maximal window width, in pixels"));
            xmax_.append("px", 2, 2);
            xmax_.prepend("w:", 2, 2);
            xmax_.assign(max_size_hint().width());
            xmax_.set_step_value(10);
            xmax_.signal_value_changed().connect(fun(this, &Main::on_minmax_changed));

            tau::Label label("Max:", tau::Align::END);
            box.append(label, true);
        }

        {
            tau::Frame frm(tau::Border::INSET);
            frm.hint_margin(2);
            ctlbox.append(frm, true);

            tau::Box box(tau::Orientation::LEFT, 4);
            box.hint_margin(2);
            frm.insert(box);

            box.append(ymin_);
            ymin_.set_tooltip(dgettext("tau", "Sets minimal window height, in pixels"));
            ymin_.append("px", 2, 2);
            ymin_.prepend("h:", 2, 2);
            ymin_.assign(min_size_hint().height());
            ymin_.set_step_value(10);
            ymin_.signal_value_changed().connect(fun(this, &Main::on_minmax_changed));

            box.append(xmin_);
            xmin_.set_tooltip(dgettext("tau", "Sets minimal window width, in pixels"));
            xmin_.append("px", 2, 2);
            xmin_.prepend("w:", 2, 2);
            xmin_.assign(min_size_hint().width());
            xmin_.set_step_value(10);
            xmin_.signal_value_changed().connect(fun(this, &Main::on_minmax_changed));

            tau::Label label("Min:", tau::Align::END);
            box.append(label, true);
        }

        connect_action(escape_ax_);
        connect_action(next_page_ax_);
        connect_action(prev_page_ax_);
        connect_action(n_action_);

        signal_position_changed().connect(fun(this, &Main::on_geometry_changed));
        signal_size_changed().connect(fun(this, &Main::on_geometry_changed));
        loop_.alarm(fun(this, &Main::on_timer), 107, true);

        set_icon("tau", 48);
        tau::Key_section & sect = kstate_.section("pages");
        std::map<int, int> m;
        int ifb = pages_.size();

        for (auto & pg: pages_) {
            m[kstate_.get_integer(sect, pg.title, ifb++)] = pg.page;
        }

        for (auto & mp: m) {
            pages_[mp.second].page = pages_[mp.second].x(mp.second);
        }

        tau::ustring ctitle = kstate_.get_string(sect, "current", pages_[0].title);

        for (auto & pg: pages_) {
            if (pg.title == ctitle) {
                notebook_.show_page(pg.page);
                break;
            }
        }

        notebook_.take_focus();
        notebook_.signal_page_changed().connect(fun(this, &Main::on_page_changed));
        notebook_.signal_page_reordered().connect(fun(this, &Main::on_page_reordered));
    }
};

void run() {
    try {
        auto v = kstate_.get_integers(kstate_.section("main"), "geometry");
        tau::Rect bounds;
        if (v.size() > 3) { bounds.set(tau::Point(v[0], v[1]), tau::Size(v[2], v[3])); }
        Main wnd(bounds);
        wnd.set_title(tau::str_format("TAU Demo, tid=", std::this_thread::get_id()));
        tau::Loop().run();
    }

    catch (tau::exception & x) {
        std::cerr << "** taudemo: run(): tau::exception thrown: " << x.what() << std::endl;
    }

    catch (std::exception & x) {
        std::cerr << "** taudemo: run(): std::exception thrown: " << x.what() << std::endl;
    }

    catch (...) {
        std::cerr << "** taudemo: run(): unknown exception thrown: " << std::endl;
    }
}

int main(int argc, char * argv[]) {
    tau::Locale::set();
    kstate_.create_from_file(tau::path_build(tau::path_user_data_dir(), tau::program_name(), "state.ini"));
    tau::Timer timer(tau::fun(kstate_, &tau::Key_file::flush));
    kstate_.signal_changed().connect(bind_back(fun(timer, &tau::Timer::start), 6789, false));
    run();
    for (auto & thr: threads_) { thr.join(); }
    kstate_.flush();
    return 0;
}

//END

// ----------------------------------------------------------------------------
// SPDX-License-Identifier: BSD-2-Clause
// Copyright © 2014-2024 Konstantin Shmelkov <mrcashe@gmail.com>.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
// THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
// PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
// CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
// EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
// PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
// OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
// WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
// OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
// EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// ----------------------------------------------------------------------------

#include <tau.hh>
#include <tau/gettext.hh>
#include <algorithm>
#include <cstdlib>
#include <fstream>
#include <iostream>
#include <iomanip>
#include <map>
#include <unordered_map>

namespace {

tau::Key_file               kstate_;
std::vector<tau::ustring>   args_;
int                         line_   = -1;
int                         col_    = -1;
std::vector<intmax_t>       geom_;

} // anonymous namespace

// ----------------------------------------------------------------------------
// ----------------------------------------------------------------------------

struct Main: tau::Toplevel {
    struct Page {
        tau::Table      cont_;
        tau::Box        tab_;
        tau::Label      title_              { tau::Align::START, tau::Align::CENTER };
        tau::Label      tooltip_;
        tau::Edit       edit_;
        tau::Box        status_box_         { tau::Orientation::RIGHT, 3 };
        tau::Label      row_value_          { "0" };
        tau::Label      rows_value_         { "0" };
        tau::Label      col_value_          { "0" };
        tau::Label      uni_value_          { "U+0000" };
        tau::Label      enc_label_          { "UTF-8" };
        tau::Label      insert_label_       { dgettext("tau", "INSERT") };
        tau::Label      replace_label_      { dgettext("tau", "REPLACE") };
        tau::Action     action_zin_;
        tau::Action     action_zout_;
        tau::Timer      motion_timer_;
        tau::Icon       ico_                { "window-close", tau::Icon::SMALL };
        tau::Icon       save_ico_           { "document-save", tau::Icon::SMALL };
        tau::ustring    path_;
        tau::Timer      meta_timer_;
        tau::connection wcx_                { true };
        tau::connection encoding_cx_        { true };
        tau::connection enable_undo_cx_     { true };
        tau::connection disable_undo_cx_    { true };
        tau::connection enable_redo_cx_     { true };
        tau::connection disable_redo_cx_    { true };
        std::size_t     lines_              = 0;
        int             page_;
        uint64_t        metaid_             = 0;
        unsigned        font_size_          = 0;
        tau::File       file_;
    };

    using Pages = std::list<Page>;

    struct Meta_holder {
        tau::Timeval    atime_              { 0 };
        tau::ustring    path_;
        uint64_t        id_                 = 0;
    };

    using Metas = std::unordered_map<std::string, Meta_holder>;

    tau::Loop           loop_;
    tau::ustring        datadir_;
    Pages               pages_;
    Metas               metas_;
    tau::ustring        font_spec_;
    unsigned            font_size_          = 10;
    unsigned            max_recent_         = 24;
    tau::Pixmap_ptr     save_pix_;
    int                 ecurrent_           = -1;
    tau::connection     session_cx_;
    tau::connection     cx1_;
    tau::connection     cx2_;
    tau::connection     cx3_;
    tau::signal<bool()> signal_modified_;
    tau::Key_file       ksession_;

    tau::Menubar        menubar_;
    tau::Box            toolbar_            { 6 };
    tau::Menubox        recent_menu_;
    tau::Notebook       notebook_           { tau::Side::TOP };
    tau::Card           card_;

    tau::Action         action_escape_ { "Escape", tau::fun(this, &Main::on_escape) };
    tau::Action         file_quit_action_ { "<Ctrl>Q", dgettext("tau", "Quit"), "application-exit:window-close", fun(this, &Main::on_menu_file_quit) };

    tau::Action         action_new_ { "<Ctrl>N", dgettext("tau", "New"), "document-new", fun(this, &Main::on_menu_file_new) };
    tau::Action         action_open_ { "<Ctrl>O", dgettext("tau", "Open"), "document-open", dgettext("tau", "Open a File"), fun(this, &Main::on_menu_file_open) };
    tau::Action         action_save_ { "<Ctrl>S", dgettext("tau", "Save"), "document-save", dgettext("tau", "Save a File"), fun(this, &Main::on_menu_file_save) };
    tau::Action         file_save_as_action_ { "<Ctrl><Shift>S", dgettext("tau", "Save As"), "document-save-as", dgettext("tau", "Save the file with\na different name"), fun(this, &Main::on_menu_file_save_as) };
    tau::Action         file_save_all_action_ { "<Ctrl>L", dgettext("tau", "Save All"), "document-save-all", fun(this, &Main::on_menu_file_save_all) };
    tau::Action         file_close_action_ { "<Ctrl>W", dgettext("tau", "Close"), "document-close", fun(this, &Main::on_menu_file_close) };
    tau::Action         file_close_all_action_ { tau::KC_NONE, tau::KM_NONE, dgettext("tau", "Close All"), fun(this, &Main::on_menu_file_close_all) };
    tau::Action         file_close_others_action_ { "", dgettext("tau", "Close Others"), fun(this, &Main::close_others) };

    tau::Master_action  edit_undo_mx_ { "<Alt>BackSpace", dgettext("tau", "Undo"), "edit-undo" };
    tau::Action         edit_undo_action_ { edit_undo_mx_, fun(this, &Main::on_menu_edit_undo) };
    tau::Master_action  edit_redo_mx_ { "<Alt>Enter", dgettext("tau", "Redo"), "edit-redo" };
    tau::Action         edit_redo_action_ { edit_redo_mx_, fun(this, &Main::on_menu_edit_redo) };
    tau::Action         edit_unselect_ { tau::KC_NONE, tau::KM_NONE, dgettext("tau", "Unselect"), fun(this, &Main::on_menu_edit_unselect) };
    tau::Master_action  edit_select_all_mx_ { U'A', tau::KM_CONTROL, dgettext("tau", "Select All") };
    tau::Action         edit_action_select_all_ { edit_select_all_mx_, fun(this, &Main::on_menu_edit_select_all) };
    tau::Master_action  edit_cut_mx_ { "<Ctrl>X <Shift>Delete", dgettext("tau", "Cut"), "edit-cut" };
    tau::Action         edit_cut_action_ { edit_cut_mx_, fun(this, &Main::on_menu_edit_copy) };
    tau::Master_action  edit_copy_mx_ { "<Ctrl>C <Ctrl>Insert", dgettext("tau", "Copy"), "edit-copy" };
    tau::Action         edit_action_copy_ { edit_copy_mx_, fun(this, &Main::on_menu_edit_cut) };
    tau::Master_action  edit_paste_master_action_ { "<Ctrl>V <Shift>Insert", dgettext("tau", "Paste"), "edit-paste" };
    tau::Action         edit_paste_action_ { edit_paste_master_action_, fun(this, &Main::on_menu_edit_paste) };

    tau::Master_action  zin_mx_ { "<Ctrl>+ <Ctrl>=", dgettext("tau", "Increase Font"), "zoom-in" };
    tau::Action         zin_ax_ { zin_mx_, fun(this, &Main::on_menu_increase_font) };
    tau::Master_action  zout_mx_ { "<Ctrl>-", dgettext("tau", "Decrease Font"), "zoom-out" };
    tau::Action         zout_ax_ { zout_mx_ , fun(this, &Main::on_menu_decrease_font) };
    tau::Action         view_next_page_action_ { tau::KC_RIGHT, tau::KM_ALT, dgettext("tau", "Next Page"), "go-next", fun(this, &Main::on_menu_next_doc) };
    tau::Action         view_prev_page_action_ { tau::KC_LEFT, tau::KM_ALT, dgettext("tau", "Previous Page"), "go-previous", fun(this, &Main::on_menu_prev_doc) };

    tau::Action         settings_action_    { U'P', tau::KM_CONTROL, dgettext("tau", "Settings"), fun(this, &Main::on_menu_settings) };
    tau::Action         action_copy_        { tau::Theme().find_action("edit-copy") };

    Pages::iterator find_page(int n) {
        return std::find_if(pages_.begin(), pages_.end(), [n](auto & p) { return p.page_ == n; } );
    }

    Pages::const_iterator find_page(int n) const {
        return std::find_if(pages_.begin(), pages_.end(), [n](auto & p) { return p.page_ == n; } );
    }

    void on_menu_quit() {
        if (notebook_.visible()) {
            auto i = std::find_if(pages_.begin(), pages_.end(), [this](auto & pg) { return notebook_.current_page() == pg.page_; } );
            if (i != pages_.end()) { i->edit_.grab_focus(); }
        }
    }

    void open_recent(const tau::ustring & path) {
        int page = open_file(path);
        if (page >= 0) { notebook_.show_page(page); }
    }

    void on_display() {
        if (!args_.empty()) {
            if (args_.size() > 1) {
                for (auto & s: args_) {
                    open_file(s);
                }
            }

            else {
                int page = open_file(args_.front());

                if (page >= 0 && (line_ > 0 || col_ > 0)) {
                    for (auto & pg: pages_) {
                        if (pg.page_ == page) {
                            pg.edit_.move_to(line_ > 0 ? line_-1 : 0, col_ > 0 ? col_-1 : 0);
                            break;
                        }
                    }
                }
            }
        }

        else {
            load_session();
        }
    }

    int open_file(const tau::ustring & path) {
        for (auto & pg: pages_) {
            if (pg.path_ == path) {
                return pg.page_;
            }
        }

        try {
            set_cursor("wait:watch");
            tau::Buffer buffer = tau::Buffer::load_from_file(path);
            Page & pg = new_editor(buffer);
            pg.path_ = path;
            pg.title_.assign(tau::path_notdir(path));
            pg.tooltip_.assign(path);
            pg.tab_.set_tooltip(pg.tooltip_);
            pg.metaid_ = find_metaid(path);

            if (0 == pg.metaid_) {
                tau::Timeval now = tau::Timeval::now();
                pg.metaid_ = now;
                Meta_holder hol;
                hol.atime_ = now;
                hol.id_ = now;
                hol.path_ = path;
                metas_[tau::path_real(path)] = hol;
            }

            else {
                const tau::ustring name = tau::str_format(pg.metaid_, ".ini");
                const tau::ustring mpath = tau::path_build(tau::path_build(tau::path_user_data_dir(), tau::program_name(), "meta"), name);
                tau::Key_file kf(mpath);
                std::size_t row = kf.get_integer(kf.section("pos"), "row");
                std::size_t col = kf.get_integer(kf.section("pos"), "col");
                pg.edit_.move_to(row, col);
                pg.font_size_ = kf.get_integer(kf.section("font"), "size");
                pg.edit_.conf().font(tau::Conf::EDIT_FONT).set(tau::Font::resize(font_spec_, 0 != pg.font_size_ ? pg.font_size_ : font_size_));
            }

            pg.file_ = tau::File(path);
            pg.wcx_ = pg.file_.signal_watch(tau::File::EVENTS).connect(bind_back(tau::fun(this, &Main::on_watch), std::ref(pg)));
            pg.page_ = notebook_.append_page(pg.cont_, pg.tab_);
            loop_.alarm(tau::fun(this, &Main::save_metas), 16754);
            touch_session();
            touch_recent(pg.metaid_);
            unset_cursor();
            return pg.page_;
        }

        catch (tau::exception & x) {
            std::cerr << "** Main::open_file(): tau::exception caught: " << x.what() << std::endl;
        }

        catch (std::exception & x) {
            std::cerr << "** Main::open_file(): std::exception caught: " << x.what() << std::endl;
        }

        catch (...) {
            std::cerr << "** Main::open_file(): unknown exception caught: " << std::endl;
        }

        unset_cursor();
        return -1;
    }

    void update_recent_menu() {
        load_metas();
        recent_menu_.clear();
        auto v = kstate_.get_integers("recent");
        std::map<uint64_t, uint64_t> ids;
        std::vector<uint64_t> ats;

        for (uint64_t id: v) {
            for (auto & p: metas_) {
                if (p.second.id_ == id) {
                    uint64_t at = p.second.atime_;
                    ids[at] = id;
                    ats.push_back(at);
                    break;
                }
            }
        }

        std::sort(ats.begin(), ats.end(), std::greater<uint64_t>());

        for (uint64_t at: ats) {
            uint64_t id = ids[at];

            for (auto & p: metas_) {
                if (p.second.id_ == id) {
                    tau::Slot_menu_item item(tau::path_notdir(p.first), bind_back(tau::fun(this, &Main::open_recent), p.second.path_), tau::File(p.second.path_).icon(tau::Icon::SMALL));
                    tau::Text tooltip("@"+tau::path_dirname(p.second.path_));
                    tooltip.wrap(tau::Wrap::ELLIPSIZE_END);
                    tooltip.conf().font().resize(7);
                    item.set_tooltip(tooltip);
                    recent_menu_.append(item);
                    break;
                }
            }
        }

        // Clear histrory item.
        if (!recent_menu_.empty()) {
            recent_menu_.append_separator();
            recent_menu_.append("Clear History", tau::fun(this, &Main::clear_recent), "edit-clear-locationbar-ltr");
        }
    }

    void clear_recent() {
        kstate_.remove_key("recent");
        update_recent_menu();
    }

    void touch_recent(uint64_t rid) {
        load_metas();
        auto i = std::find_if(metas_.begin(), metas_.end(), [rid](auto & p) { return p.second.id_ == rid; });
        if (i != metas_.end()) { i->second.atime_ = tau::Timeval::now(); }

        std::map<uint64_t, uint64_t> ids;
        std::vector<uint64_t> ats;
        auto v = kstate_.get_integers("recent");
        if (v.end() == std::find(v.begin(), v.end(), rid)) { v.push_back(rid); }

        for (uint64_t id: v) {
            auto j = std::find_if(metas_.begin(), metas_.end(), [id](auto & p) { return p.second.id_ == id; });

            if (j != metas_.end()) {
                uint64_t at = j->second.atime_;
                ats.push_back(at);
                ids[at] = id;
            }
        }

        std::sort(ats.begin(), ats.end(), std::greater<uint64_t>());
        if (ats.size() > max_recent_) { ats.resize(max_recent_); }
        v.clear();
        for (uint64_t at: ats) { v.push_back(ids[at]); }
        kstate_.set_integers("recent", v);
        update_recent_menu();
    }

    void on_watch(unsigned mask, const tau::ustring & path, Page & page) {
        std::cout << "on_watch " << std::hex << mask << std::dec << ", " << path << std::endl;
    }

    void load_session() {
        load_metas();
        std::vector<int> v;

        for (auto & s: ksession_.list_sections()) {
            if (tau::str_has_prefix(s, "Page_")) {
                try { v.push_back(std::stoi(s.substr(5))); }
                catch (...) {}
            }
        }

        std::sort(v.begin(), v.end());

        for (auto & i: v) {
            auto & sect = ksession_.section(tau::str_format("Page_", i));
            uint64_t metaid = ksession_.get_integer(sect, "metaid");
            auto j = std::find_if(metas_.begin(), metas_.end(), [metaid](auto & p) { return p.second.id_ == metaid; } );
            if (j != metas_.end()) { open_file(j->second.path_); }
        }

        uint64_t current_metaid = ksession_.get_integer("current");
        auto k = std::find_if(pages_.begin(), pages_.end(), [current_metaid](auto & pg) { return pg.metaid_ == current_metaid; } );
        if (k != pages_.end()) { notebook_.show_page(k->page_); }
    }

    void save_session() {
        session_cx_.drop();
        ksession_.clear();
        int page = 1, npages = notebook_.page_count();
        uint64_t current_metaid = 0;

        for (int n = 0; n < npages; ++n) {
            for (Page & pg: pages_) {
                if (n == pg.page_ && !pg.path_.empty()) {
                    auto & sect = ksession_.section(tau::str_format("Page_", page++));
                    ksession_.set_integer(sect, "metaid", pg.metaid_);
                    if (pg.page_ == notebook_.current_page()) { current_metaid = pg.metaid_; }
                }
            }
        }

        if (0 != current_metaid) {
            ksession_.set_integer("current", current_metaid);
        }

        ksession_.flush();
    }

    void load_metas() {
        if (metas_.empty()) {
            try {
                tau::Key_file kf;
                kf.create_from_file(tau::path_build(datadir_, "metas.ini"));

                for (auto & s: kf.list_sections()) {
                    if (tau::File(s).is_regular()) {
                        tau::Key_section & sect = kf.section(s);
                        auto now = tau::Timeval::now();
                        Meta_holder hol;
                        hol.path_ = kf.get_string(sect, "path", s);
                        hol.atime_ = kf.get_integer(sect, "atime", now);
                        hol.id_ = kf.get_integer(sect, "id", now);
                        metas_[s] = hol;
                    }
                }
            }

            catch (tau::exception & x) {
                std::cerr << "** Main::load_metas(): tau::exception caught: " << x.what() << std::endl;
            }

            catch (std::exception & x) {
                std::cerr << "** Main::load_metas(): std::exception caught: " << x.what() << std::endl;
            }

            catch (...) {
                std::cerr << "** Main::load_metas(): unknown exception caught" << std::endl;
            }
        }
    }

    uint64_t find_metaid(const tau::ustring & path) {
        load_metas();
        auto iter = metas_.find(tau::path_real(path));
        return metas_.end() != iter ? iter->second.id_ : 0;
    }

    // Connected to editor's signal_focus_in().
    void update_pos(Page & pg) {
        tau::Buffer_citer i = pg.edit_.caret();

        if (i) {
            pg.row_value_.assign(tau::str_format(1+i.row()));
            pg.rows_value_.assign(tau::str_format(std::max(std::size_t(1), pg.edit_.buffer().rows())));
            pg.col_value_.assign(tau::str_format(1+i.col()));
            pg.uni_value_.assign(tau::key_code_to_string(*i));
        }

        else {
            pg.row_value_.assign("0");
            pg.col_value_.assign("0");
            pg.uni_value_.assign("U+0000");
        }
    }

    void on_utimer(Page & pg) {
        update_pos(pg);
        pg.meta_timer_.restart(7735);
    }

    void on_caret_motion(Page & pg) {
        pg.motion_timer_.start(50);
    }

    Page & new_editor(tau::Buffer buffer) {
        pages_.emplace_back();
        Page & pg = pages_.back();
        pg.motion_timer_.connect(bind_back(tau::fun(this, &Main::on_utimer), std::ref(pg)));

        pg.tab_.set_spacing(2);
        pg.tab_.hint_margin(2, 2, 0, 0);
        pg.tab_.append(pg.save_ico_, true);
        pg.save_ico_.hide();
        pg.title_.hint_min_size(22, 0);
        pg.title_.hint_max_size(142, 0);
        pg.title_.wrap(tau::Wrap::ELLIPSIZE_CENTER);
        pg.tab_.append(pg.title_);
        tau::Button btn(pg.ico_);
        btn.hide_relief();
        btn.connect(bind_back(tau::fun(this, &Main::on_page_button_click), std::ref(pg)));
        pg.tab_.append(btn, true);

        pg.tooltip_.hint_max_size(320, 0);
        pg.tooltip_.wrap(tau::Wrap::ELLIPSIZE_END);
        pg.tooltip_.conf().font().resize(7);

        pg.edit_.set_buffer(buffer);
        pg.encoding_cx_ = pg.edit_.buffer().signal_encoding_changed().connect(bind_back(tau::fun(this, &Main::on_buffer_encoding_changed), std::ref(pg)));
        pg.edit_.buffer().signal_changed().connect(bind_back(tau::fun(this, &Main::on_edit_changed), std::ref(pg)));

        pg.edit_.conf().font(tau::Conf::EDIT_FONT).set(tau::Font::resize(font_spec_, font_size_));
        pg.edit_.conf().signal_changed(tau::Conf::EDIT_FONT).connect(bind_back(tau::fun(this, &Main::on_edit_font_changed), std::ref(pg)));
        pg.edit_.signal_caret_motion().connect(bind_back(tau::fun(this, &Main::on_caret_motion), std::ref(pg)));
        pg.edit_.signal_focus_in().connect(bind_back(tau::fun(this, &Main::update_pos), std::ref(pg)));
        pg.edit_.signal_focus_out().connect(tau::fun(pg.edit_, &tau::Widget::hide_tooltip));
        pg.edit_.signal_selection_changed().connect(bind_back(tau::fun(this, &Main::on_edit_selection_changed), std::ref(pg)));
        pg.edit_.signal_modified().connect(bind_back(tau::fun(this, &Main::on_edit_modified), std::ref(pg)));
        pg.edit_.action_insert().connect(bind_back(tau::fun(this, &Main::on_edit_insert_toggled), std::ref(pg)));
        signal_modified_.connect(tau::fun(pg.edit_, &tau::Edit::modified));
        pg.edit_.action_cancel().disable();
        pg.meta_timer_.connect(bind_back(tau::fun(this, &Main::save_metadata), std::ref(pg)));

        pg.action_zin_.set_master_action(zin_mx_);
        pg.edit_.connect_action(pg.action_zin_);
        pg.action_zin_.connect(bind_back(tau::fun(this, &Main::on_edit_increase_font), std::ref(pg)));

        pg.action_zout_.set_master_action(zout_mx_);
        pg.edit_.connect_action(pg.action_zout_);
        pg.action_zout_.connect(bind_back(tau::fun(this, &Main::on_edit_decrease_font), std::ref(pg)));

        pg.edit_.action_select_all().set_master_action(edit_select_all_mx_);
        pg.edit_.action_copy().set_master_action(edit_copy_mx_);
        pg.edit_.action_cut().set_master_action(edit_cut_mx_);
        pg.edit_.action_paste().set_master_action(edit_paste_master_action_);
        pg.edit_.action_undo().set_master_action(edit_undo_mx_);
        pg.edit_.action_redo().set_master_action(edit_redo_mx_);

        tau::Scroller scroller;
        tau::Slider vslider(scroller, tau::Orientation::DOWN), hslider(scroller, tau::Orientation::RIGHT);
        tau::Table table;

        scroller.insert(pg.edit_);
        pg.cont_.put(scroller, 0, 0);
        pg.cont_.put(vslider, 1, 0, 1, 1, true, false);
        pg.cont_.put(hslider, 0, 1, 1, 1, false, true);
        pg.cont_.put(pg.status_box_, 0, 2, 2, 1, false, true);

        // Status bar.
        pg.status_box_.hint_margin(2);
        pg.status_box_.conf().font().enlarge(-2);

        // Current line label.
        tau::Frame frame(tau::Border::INSET);
        pg.status_box_.append(frame, true);
        tau::Box box(tau::Orientation::RIGHT, 3);
        frame.insert(box);
        tau::Label line("Line");
        line.hint_margin(0, 4, 0, 0);
        box.append(line, true);
        box.append(pg.row_value_, true);
        pg.row_value_.set_tooltip(dgettext("tau", "Shows current line number\nwithin text file"));

        tau::Text of("of ");
        box.append(of, true);
        box.append(pg.rows_value_, true);
        pg.rows_value_.set_tooltip(dgettext("tau", "Shows total line count\nwithin text file"));
        pg.lines_ = pg.edit_.buffer().rows();
        pg.rows_value_.assign(tau::str_format(pg.lines_));

        // Current position label.
        frame = tau::Frame(tau::Border::INSET);
        pg.status_box_.append(frame, true);
        box = tau::Box(tau::Orientation::RIGHT, 3);
        frame.insert(box);
        tau::Label pos("Pos:");
        box.append(pos, true);
        box.append(pg.col_value_, true);
        pg.col_value_.set_tooltip(dgettext("tau", "Shows current character index\nwithin current line"));

        // Current unicode label.
        frame = tau::Frame(tau::Border::INSET);
        pg.status_box_.append(frame, true);
        frame.insert(pg.uni_value_);
        pg.uni_value_.set_tooltip(dgettext("tau", "Shows current character\nUnicode value"));

        // Input mode indicator.
        frame = tau::Frame(tau::Border::INSET);
        pg.status_box_.append(frame, true);
        tau::Card cd;
        cd.set_tooltip(dgettext("tau", "Click here to change\nthe input mode"));
        cd.signal_mouse_down().connect(bind_back(tau::fun(this, &Main::on_insert_card), std::ref(pg)));
        cd.insert(pg.insert_label_);
        cd.insert(pg.replace_label_);
        frame.insert(cd);

        // Text encoding label.
        frame = tau::Frame(tau::Border::INSET);
        pg.status_box_.append(frame, true);
        pg.enc_label_.assign(buffer.encoding().name());
        frame.insert(pg.enc_label_);
        pg.enc_label_.set_tooltip(dgettext("tau", "Shows text encoding."));

        update_pos(pg);
        return pg;
    }

    bool on_insert_card(int mbt, int mm, const tau::Point & pt, Page & pg) {
        if (tau::MBT_LEFT == mbt) {
            auto & action = pg.edit_.action_insert();
            action.toggle();
            if (action.get()) { pg.replace_label_.show(); }
            else { pg.insert_label_.show(); }
            return true;
        }

        return false;
    }

    void on_menu_file_new() {
        Page & pg = new_editor(tau::Buffer());
        pg.page_ = notebook_.append_page(pg.cont_, pg.tab_);
        pg.title_.assign(dgettext("tau", "New File"));
        pg.metaid_ = tau::Timeval::now();
        notebook_.show_page(pg.page_);
    }

    void save_metadata(Page & pg) {
        pg.meta_timer_.stop();

        if (!pg.path_.empty()) {
            tau::Key_file kf;
            auto caret = pg.edit_.caret();
            kf.set_integer(kf.section("pos"), "row", caret.row());
            kf.set_integer(kf.section("pos"), "col", caret.col());
            if (0 != pg.font_size_) { kf.set_integer(kf.section("font"), "size", pg.font_size_); }

            try {
                tau::ustring dir = tau::path_build(tau::path_user_data_dir(), tau::program_name(), "meta");
                tau::file_mkdir(dir);
                tau::ustring name = tau::str_format(pg.metaid_, ".ini");
                tau::ustring path = tau::path_build(dir, name);
                kf.save_to_file(path);
            }

            catch (tau::exception & x) {
                std::cerr << "** Main::save_metadata(): tau::exception caught: " << x.what() << std::endl;
            }

            catch (std::exception & x) {
                std::cerr << "** Main::save_metadata(): std::exception caught: " << x.what() << std::endl;
            }

            catch (...) {
                std::cerr << "** Main::save_metadata(): unknown exception caught" << std::endl;
            }
        }
    }

    void save_metas() {
        for (Page & pg: pages_) {
            save_metadata(pg);
        }

        if (!metas_.empty()) {
            tau::Key_file kf;

            for (auto & p: metas_) {
                auto & sect = kf.section(p.first);
                kf.set_integer(sect, "atime", p.second.atime_);
                kf.set_integer(sect, "id", p.second.id_);
                kf.set_string(sect, "path", p.second.path_);
            }

            try {
                const tau::ustring dir = tau::path_build(tau::path_user_data_dir(), tau::program_name());
                tau::file_mkdir(dir);
                const tau::ustring path = tau::path_build(dir, "metas.ini");
                kf.save_to_file(path);
            }

            catch (tau::exception & x) {
                std::cerr << "** Main::save_metas(): tau::exception caught: " << x.what() << std::endl;
            }

            catch (std::exception & x) {
                std::cerr << "** Main::save_metas(): std::exception caught: " << x.what() << std::endl;
            }

            catch (...) {
                std::cerr << "** Main::save_metas(): unknown exception caught" << std::endl;
            }
        }
    }

    void on_notebook_page_added(int page) {
        if (!pages_.empty()) {
            file_close_all_action_.enable();
        }

        if (pages_.size() > 1) {
            view_next_page_action_.enable();
            view_prev_page_action_.enable();
        }

        touch_session();
    }

    void on_notebook_page_removed(int page) {
        if (ecurrent_ == page) {
            ecurrent_ = -1;
            edit_paste_master_action_.disable();
            file_save_as_action_.disable();
            file_close_action_.disable();
        }

        if (pages_.size() < 2) {
            view_next_page_action_.disable();
            view_prev_page_action_.disable();
        }

        if (pages_.empty()) {
            file_close_all_action_.disable();
        }

        auto i = std::find_if(pages_.begin(), pages_.end(), [page](auto & p) { return page == p.page_; } );

        if (i != pages_.end()) {
            if (!i->path_.empty()) { save_metadata(*i); }
            pages_.erase(i);
        }

        update_save_all();
        touch_session();
    }

    void touch_session() {
        session_cx_.drop();
        session_cx_ = loop_.alarm(fun(this, &Main::save_session), 16754);
    }

    void on_notebook_page_reordered(int new_page, int old_page) {
        auto i = std::find_if(pages_.begin(), pages_.end(), [old_page](auto & pg) { return old_page == pg.page_; } );
        if (i != pages_.end()) { i->page_ = new_page; }
    }

    void on_notebook_page_changed(int page) {
        auto i = find_page(page);

        if (i != pages_.end()) {
            ecurrent_ = page;
            i->edit_.take_focus();

            if (0 != i->metaid_ && i->edit_.modified()) { action_save_.enable(); }
            else { action_save_.disable(); }
            if (!i->edit_.empty()) { edit_select_all_mx_.enable(); }
            else { edit_select_all_mx_.disable(); }
            if (display().can_paste_text()) { edit_paste_master_action_.enable(); }
            else { edit_paste_master_action_.disable(); }
            if (pages_.size() > 1) { file_close_others_action_.enable(); }
            else { file_close_others_action_.disable(); }

            if (i->edit_.has_selection()) {
                edit_copy_mx_.enable();
                edit_cut_mx_.enable();
            }

            else {
                edit_copy_mx_.disable();
                edit_cut_mx_.disable();
            }

            if (i->edit_.action_undo().enabled()) { edit_undo_action_.enable(); }
            else { edit_undo_action_.disable(); }
            if (i->edit_.action_redo().enabled()) { edit_redo_action_.enable(); }
            else { edit_redo_action_.disable(); }
            i->enable_undo_cx_ = i->edit_.action_undo().signal_enable().connect(fun(edit_undo_action_, &tau::Action::enable));
            i->disable_undo_cx_ = i->edit_.action_undo().signal_disable().connect(fun(edit_undo_action_, &tau::Action::disable));
            i->enable_redo_cx_ = i->edit_.action_redo().signal_enable().connect(fun(edit_redo_action_, &tau::Action::enable));
            i->disable_redo_cx_ = i->edit_.action_redo().signal_disable().connect(fun(edit_redo_action_, &tau::Action::disable));

            file_close_action_.enable();
            file_save_as_action_.enable();
            zin_mx_.enable();
            zout_mx_.enable();
            touch_session();
        }

        else {
            i = find_page(ecurrent_);

            if (i != pages_.end()) {
                i->enable_undo_cx_.drop();
                i->enable_redo_cx_.drop();
                i->disable_undo_cx_.drop();
                i->disable_redo_cx_.drop();
            }

            ecurrent_ = -1;
            edit_undo_action_.disable();
            edit_redo_action_.disable();
            file_close_others_action_.disable();
            file_close_action_.disable();
            action_save_.disable();
            file_save_as_action_.disable();
            edit_copy_mx_.disable();
            edit_cut_mx_.disable();
            edit_select_all_mx_.disable();
            zin_mx_.disable();
            zout_mx_.disable();
        }

        update_save_all();
        update_title();
    }

    void quit() {
        close();
    }

    void show_pop(Widget & w) {
        menubar_.disable();
        toolbar_.disable();
        card_.insert(w);
        w.show();
        w.take_focus();
    }

    void close_pop() {
        if (notebook_.hidden()) {
            card_.remove_current();
            menubar_.enable();
            toolbar_.enable();
            notebook_.show();
            notebook_.take_focus();
            cx1_.drop();
            cx2_.drop();
            cx3_.drop();
        }
    }

    void on_escape() {
        if (full_screened()) {
            unfullscreen();
        }

        else if (notebook_.hidden()) {
            close_pop();
        }

        else {
            quit();
        }
    }

    void close_page(Page & pg) {
        if (!pg.path_.empty()) { save_metadata(pg); }
        notebook_.remove_page(pg.tab_);
    }

    void close_others() {
        if (notebook_.page_count() > 1) {
            uint64_t metaid = 0;
            int page = notebook_.current_page();

            for (Page & pg: pages_) {
                if (page == pg.page_) {
                    metaid = pg.metaid_;
                    break;
                }
            }

            if (0 != metaid) {
                bool done;

                do {
                    done = true;

                    for (Page & pg: pages_) {
                        if (metaid != pg.metaid_) {
                            close_page(pg);
                            done = false;
                            break;
                        }
                    }
                } while (!done);
            }
        }
    }

    void close_all() {
        while (!pages_.empty()) {
            close_page(pages_.back());
        }
    }

    void on_buffer_encoding_changed(const tau::Encoding & enc, Page & pg) {
        pg.enc_label_.assign(enc.name());
    }

    void on_edit_insert_toggled(bool replace, Page & pg) {
        if (replace) { pg.replace_label_.show(); }
        else { pg.insert_label_.show(); }
    }

    void on_edit_changed(Page & pg) {
        std::size_t lines = pg.edit_.buffer().rows();

        if (lines != pg.lines_) {
            pg.lines_ = lines;
            pg.rows_value_.assign(tau::str_format(pg.lines_));
        }

        pg.meta_timer_.restart(7439);
    }

    void on_edit_selection_changed(Page & pg) {
        if (!notebook_.hidden()) {
            if (notebook_.current_page() == pg.page_) {
                if (pg.edit_.has_selection()) {
                    edit_copy_mx_.enable();
                    edit_cut_mx_.enable();
                    edit_unselect_.enable();
                }

                else {
                    edit_copy_mx_.disable();
                    edit_cut_mx_.disable();
                    edit_unselect_.disable();
                }
            }
        }
    }

    void update_save_all() {
        if (signal_modified_()) { file_save_all_action_.enable(); }
        else { file_save_all_action_.disable(); }
    }

    void on_edit_modified(bool modified, Page & pg) {
        if (modified) { pg.save_ico_.show(); }
        else { pg.save_ico_.hide(); }

        if (0 != pg.metaid_ && pg.page_ == notebook_.current_page() && notebook_.visible()) {
            if (modified) { action_save_.enable(); }
            else { action_save_.disable(); }
        }

        update_save_all();
    }

    void save_page(Page & pg) {
        if (!pg.path_.empty()) {
            save_metadata(pg);
            pg.edit_.buffer().save_to_file(pg.path_);
            pg.save_ico_.hide();
        }
    }

    void on_page_button_click(Page & pg) {
        close_page(pg);
    }

    void on_fman_apply(tau::Fileman fm) {
        close_pop();

        if (tau::Fileman::OPEN == fm.mode()) {
            auto filenames = fm.selection();
            int first_page = -1, exist_page = -1;

            for (const tau::ustring & f: filenames) {
                tau::ustring path = path_build(fm.uri(), f);

                for (const Page & pg: pages_) {
                    if (pg.path_ == path) {
                        exist_page = pg.page_;
                        break;
                    }
                }

                if (-1 == exist_page) {
                    int page = open_file(path);
                    if (-1 == first_page) { first_page = page; }
                }
            }

            if (-1 == first_page) {
                if (-1 != exist_page) {
                    notebook_.show_page(exist_page);
                    update_title();
                }
            }

            else {
                notebook_.show_page(first_page);
                update_title();
            }
        }
    }

    void save_fileman(tau::Fileman fm) {
        fm.save_state(kstate_, kstate_.section("navigator"));
    }

    void save_dialog(std::string_view key, const tau::Window dlg) {
        tau::Rect bounds(dlg.position()-position(), dlg.size());
        std::vector<intmax_t> gv { bounds.left(), bounds.top(), bounds.width(), bounds.height() };
        kstate_.set_integers(key, gv);
    }

    void on_menu_file_open() {
        tau::ustring path;
        int page = notebook_.current_page();

        if (page >= 0) {
            for (const Page & pg: pages_) {
                if (pg.page_ == page) {
                    path = tau::path_dirname(pg.path_);
                    break;
                }
            }
        }

        if (path.empty()) { path = tau::path_home(); }
        tau::Fileman fm(tau::Fileman::OPEN, path);
        tau::Key_section & sect = kstate_.section("navigator");
        fm.load_state(kstate_, sect);
        fm.set_option("multiple");

        if (kstate_.get_boolean("file_dialogs")) {
            tau::Rect bounds;
            auto gv = kstate_.get_integers("open_geometry", 4);
            bounds.set(gv[0], gv[1], tau::Size(gv[2], gv[3]));
            tau::Dialog dlg(*this, "Open a file", bounds);
            dlg.insert(fm);
            cx1_ = fm.action_apply().connect(bind_back(fun(this, &Main::save_fileman), fm));
            cx2_ = fm.action_apply().connect(bind_back(fun(this, &Main::on_fman_apply), fm));
            cx3_ = fm.action_cancel().connect(bind_back(fun(this, &Main::save_fileman), fm));
            // We don't need immortal Fileman...
            fm.signal_unparent().connect(fun(cx1_, &tau::connection::drop));
            fm.signal_unparent().connect(fun(cx2_, &tau::connection::drop));
            fm.signal_unparent().connect(fun(cx3_, &tau::connection::drop));
            dlg.signal_close().connect(bind_back(fun(this, &Main::save_dialog), "open_geometry", dlg), true);
            dlg.show();
            dlg.run();
        }

        else if (!notebook_.hidden()) {
            cx1_ = fm.action_apply().connect(bind_back(fun(this, &Main::save_fileman), fm));
            cx2_ = fm.action_apply().connect(bind_back(fun(this, &Main::on_fman_apply), fm));
            fm.action_apply().connect(fun(this, &Main::close_pop));
            cx3_ = fm.action_cancel().connect(bind_back(fun(this, &Main::save_fileman), fm));
            fm.action_cancel().connect(fun(this, &Main::close_pop));
            set_title(tau::program_name()+": "+dgettext("tau", "Open a File"));
            show_pop(fm);
            fm.take_focus();
        }
    }

    void on_menu_file_save() {
        int page = notebook_.current_page();

        for (Page & pg: pages_) {
            if (pg.page_ == page) {
                save_page(pg);
                break;
            }
        }
    }

    // File/Save As
    void on_save_as_apply(tau::Fileman fm, Page & pg) {
        auto s = fm.entry();

        if (!s.empty()) {
            save_metadata(pg);
            pg.meta_timer_.stop();
            pg.title_.assign(s);
            pg.tooltip_.assign(fm.uri());
            pg.tab_.set_tooltip(pg.tooltip_);
            pg.path_ = tau::path_build(fm.uri(), s);
            pg.metaid_ = find_metaid(pg.path_);
            pg.edit_.buffer().save_to_file(pg.path_);
            pg.file_ = tau::File(pg.path_);
            pg.wcx_ = pg.file_.signal_watch(tau::File::EVENTS).connect(bind_back(fun(this, &Main::on_watch), std::ref(pg)));
            save_metadata(pg);
            pg.meta_timer_.start(7799);
        }
    }

    void on_menu_file_save_as() {
        if (!notebook_.hidden()) {
            tau::ustring path;
            int page = notebook_.current_page();
            Page * pgp = nullptr;

            if (page >= 0) {
                for (Page & pg: pages_) {
                    if (pg.page_ == page) {
                        pgp = &pg;
                        path = tau::path_dirname(pg.path_);
                        break;
                    }
                }
            }

            if (pgp) {
                tau::Fileman fman(tau::Fileman::SAVE, path.empty() ? tau::path_home() : path);
                tau::Key_section & sect = kstate_.section("navigator");
                fman.load_state(kstate_, sect);
                cx1_ = fman.action_apply().connect(bind_back(fun(this, &Main::on_save_as_apply), fman, std::ref(*pgp)));
                cx2_ = fman.action_apply().connect(bind_back(fun(this, &Main::on_fman_apply), fman));
                fman.action_cancel().connect(tau::fun(this, &Main::close_pop));
                fman.signal_unparent().connect(fun(cx1_, &tau::connection::drop));
                fman.signal_unparent().connect(fun(cx2_, &tau::connection::drop));
                set_title(tau::program_name()+": "+dgettext("tau", "Save File As"));
                show_pop(fman);
                fman.take_focus();
            }
        }
    }

    void on_menu_file_save_all() {
        for (Page & pg: pages_) { save_page(pg); }
    }

    void on_menu_file_quit() {
        quit();
    }

    void on_menu_file_close_all() {
        close_all();
    }

    void on_menu_file_close() {
        int page = notebook_.current_page();

        for (Page & pg: pages_) {
            if (pg.page_ == page) {
                close_page(pg);
                break;
            }
        }
    }

    void on_menu_edit_undo() {
        if (!notebook_.hidden()) {
            for (Page & pg: pages_) {
                if (pg.page_ == notebook_.current_page()) {
                    pg.edit_.action_undo().exec();
                    break;
                }
            }
        }
    }

    void on_menu_edit_redo() {
        if (!notebook_.hidden()) {
            for (Page & pg: pages_) {
                if (pg.page_ == notebook_.current_page()) {
                    pg.edit_.action_redo().exec();
                    break;
                }
            }
        }
    }

    void on_menu_edit_cut() {
        if (!notebook_.hidden()) {
            for (Page & pg: pages_) {
                if (pg.page_ == notebook_.current_page()) {
                    pg.edit_.action_cut().exec();
                    break;
                }
            }
        }
    }

    void on_menu_edit_copy() {
        if (!notebook_.hidden()) {
            for (Page & pg: pages_) {
                if (pg.page_ == notebook_.current_page()) {
                    pg.edit_.action_copy().exec();
                    break;
                }
            }
        }
    }

    void on_menu_edit_paste() {
        if (!notebook_.hidden()) {
            for (Page & pg: pages_) {
                if (pg.page_ == notebook_.current_page()) {
                    pg.edit_.action_paste().exec();
                    break;
                }
            }
        }
    }

    void on_menu_edit_select_all() {
        if (!notebook_.hidden()) {
            for (Page & pg: pages_) {
                if (pg.page_ == notebook_.current_page()) {
                    pg.edit_.select_all();
                    break;
                }
            }
        }
    }

    void on_menu_edit_unselect() {
        if (!notebook_.hidden()) {
            for (Page & pg: pages_) {
                if (pg.page_ == notebook_.current_page()) {
                    pg.edit_.unselect();
                    break;
                }
            }
        }
    }

    void on_menu_next_doc() {
        notebook_.show_next();
    }

    void on_menu_prev_doc() {
        notebook_.show_previous();
    }

    void on_edit_font_changed(Page & pg) {
        pg.meta_timer_.restart(5767);
        pg.font_size_ = pg.edit_.conf().font(tau::Conf::EDIT_FONT).size();
    }

    void show_font_tooltip(Page & pg) {
        tau::Label label(pg.edit_.conf().font(tau::Conf::EDIT_FONT));
        label.conf().font().resize(18);
        pg.edit_.show_tooltip(label, tau::Rect(pg.edit_.size()).center(), tau::Gravity::CENTER, 2478);
    }

    void on_edit_increase_font(Page & pg) {
        auto fi = pg.edit_.conf().font(tau::Conf::EDIT_FONT);
        double pts = fi.size();
        if (pts < 100.0) { fi.grow(1.0); show_font_tooltip(pg); }
    }

    void on_edit_decrease_font(Page & pg) {
        auto fi = pg.edit_.conf().font(tau::Conf::EDIT_FONT);
        double pts = fi.size();
        if (pts >= 2.0) { fi.grow(-1.0); show_font_tooltip(pg); }
    }

    void on_menu_increase_font() {
        if (!notebook_.hidden()) {
            auto i = find_page(notebook_.current_page());
            if (i != pages_.end()) { on_edit_increase_font(*i); }
        }
    }

    void on_menu_decrease_font() {
        if (!notebook_.hidden()) {
            if (!notebook_.hidden()) {
                auto i = find_page(notebook_.current_page());
                if (i != pages_.end()) { on_edit_decrease_font(*i); }
            }
        }
    }

    void on_clipboard_changed() {
        if (ecurrent_ >= 0 && display().can_paste_text()) { edit_paste_master_action_.enable(); }
        else  { edit_paste_master_action_.disable(); }
    }

    void update_title() {
        if (!notebook_.hidden()) {
            tau::ustring title = tau::program_name();

            if (0 != notebook_.page_count()) {
                int page = notebook_.current_page();

                if (-1 != page) {
                    for (auto & pg: pages_) {
                        if (pg.page_ == page) {
                            title += ": ";
                            if (!pg.path_.empty()) { title += tau::path_notdir(pg.path_); }
                            else { title += "(unnamed)"; }
                            pg.edit_.take_focus();
                            break;
                        }
                    }
                }
            }

            set_title(title);
        }
    }

    void populate_main_menu(tau::Menu & mainmenu) {
        tau::Menubox edit_menu;
        mainmenu.append(dgettext("tau", "Edit"), edit_menu);
        edit_menu.append(edit_undo_action_);
        edit_menu.append(edit_redo_action_);
        edit_menu.append_separator();
        edit_menu.append(edit_cut_action_);
        edit_menu.append(edit_action_copy_);
        edit_menu.append(edit_paste_action_);
        edit_menu.append_separator();
        edit_menu.append(edit_action_select_all_);
        edit_menu.append(edit_unselect_);

        tau::Menubox file_menu;
        mainmenu.prepend(dgettext("tau", "File"), file_menu);
        file_menu.prepend(action_new_);
        file_menu.append_separator();
        file_menu.append(action_open_);
        tau::Submenu_item recent_item(dgettext("tau", "Open Recent"), recent_menu_, "document-open-recent:document-open");
        update_recent_menu();
        file_menu.append(recent_item);
        file_menu.append_separator();
        file_menu.append(action_save_);
        file_menu.append(file_save_as_action_);
        file_menu.append(file_save_all_action_);
        file_menu.append_separator();
        file_menu.append(file_close_action_);
        file_menu.append(file_close_all_action_);
        file_menu.append(file_close_others_action_);
        file_menu.append_separator();
        file_menu.append(file_quit_action_);

        tau::Menubox view_menu;
        mainmenu.append(dgettext("tau", "View"), view_menu);
        view_menu.append(view_next_page_action_);
        view_menu.append(view_prev_page_action_);
        view_menu.append_separator();
        view_menu.append(zin_ax_);
        view_menu.append(zout_ax_);

        mainmenu.append(settings_action_);
    }

    void on_menu_settings() {
        if (!notebook_.hidden()) {
            set_title(tau::program_name()+": "+dgettext("tau", "Settings"));

            // Box with the nested scroller box and the button box.
            tau::Box box0(tau::Orientation::DOWN, 4);
            box0.hint_margin(4);
            show_pop(box0);

            // Nested box with scroller and slider.
            tau::Box box1(tau::Orientation::RIGHT, 2);
            box0.append(box1);
            tau::Frame frame(tau::Border::INSET);
            box1.append(frame);
            tau::Scroller scroller;
            scroller.hint_margin(2);
            frame.insert(scroller);
            tau::Slider slider(scroller, tau::Orientation::DOWN, true);
            box1.append(slider, true);

            // Button box at the bottom.
            tau::Box button_box(tau::Orientation::RIGHT);
            button_box.align(tau::Align::CENTER);
            box0.append(button_box, true);
            tau::Button close_button(dgettext("tau", "Close"));
            close_button.connect(fun(this, &Main::close_pop));
            button_box.append(close_button, true);

            // --- Information tape as a box.
            tau::Box tape(tau::Orientation::DOWN);
            tape.hint_margin(4, 4, 2, 2);
            scroller.insert(tape);

            // -- Font selector.
            frame = tau::Frame(tau::Border::SOLID);
            frame.set_border(4, 0, 0, 0);
            frame.hint_margin(0, 0, 4, 4);
            tape.append(frame, true);

            tau::Box ibox(tau::Orientation::DOWN, 6);
            ibox.hint_margin_left(4);
            frame.insert(ibox);
            tau::Label label(dgettext("tau", "Default editor font"), tau::Align::START);
            label.conf().font().make_bold();
            label.conf().font().enlarge(4);
            ibox.append(label, true);
            tau::Label remark(dgettext("tau", "This font used by text editor"), tau::Align::START);
            remark.conf().font().enlarge(-2);
            ibox.append(remark, true);

            auto i = find_page(notebook_.current_page());
            unsigned font_size = i != pages_.end() ? i->font_size_ : font_size_;

            tau::Fontsel fsel(tau::Font::resize(font_spec_, font_size));
            fsel.show_monospace_only(kstate_.get_boolean("monospace"));
            fsel.hint_margin(4, 4, 4, 0);
            fsel.hint_max_size(0, 3*size().height()/4);
            tape.append(fsel, true);
            fsel.signal_font_activated().connect(fun(this, &Main::set_font));
            cx1_ = fsel.signal_display_out().connect(bind_back(fun(this, &Main::save_monospace), fsel.ptr()));
            fsel.action_cancel().connect(fun(this, &Main::close_pop));
            fsel.action_apply().connect(fun(this, &Main::close_pop));
            fsel.take_focus();

            // -- Options.
            frame = tau::Frame(tau::Border::SOLID);
            frame.set_border(4, 0, 0, 0);
            frame.hint_margin(0, 0, 4, 4);
            tape.append(frame, true);

            label = tau::Label(dgettext("tau", "Options"), tau::Align::START);
            label.conf().font().make_bold();
            label.conf().font().enlarge(4);
            label.hint_margin_left(4);
            frame.insert(label);

            tau::Table table(8, 2);
            tape.append(table, true);

            tau::Check ck(tau::Check::QSTYLE, kstate_.get_boolean("file_dialogs"));
            table.put(ck, 0, 0, 1, 1, true, true);
            label = tau::Label(dgettext("tau", "File Operations in dialogs"));
            table.put(label, tau::Align::START, tau::Align::CENTER, 1, 0, 1, 1, true, true);
            ck.signal_check().connect(bind_back(fun(kstate_, static_cast<void (tau::Key_file::*)(std::string_view, bool)>(&tau::Key_file::set_boolean)), "file_dialogs", true));
            ck.signal_uncheck().connect(bind_back(fun(kstate_, static_cast<void (tau::Key_file::*)(std::string_view, bool)>(&tau::Key_file::set_boolean)), "file_dialogs", false));
        }
    }

    void save_monospace(tau::Widget_ptr wp) {
        tau::Fontsel fsel(wp);
        kstate_.set_boolean("monospace", fsel.monospace_only_visible());
    }

    void set_font(const tau::ustring & spec) {
        const tau::ustring rspec = tau::Font::without_points(spec);
        font_spec_ = rspec;
        font_size_ = tau::Font::points(spec);
        kstate_.set_integer("font-size", font_size_);
        kstate_.set_string("font", rspec);

        for (Page & pg: pages_) {
            pg.font_size_ = font_size_;
            pg.edit_.conf().font(tau::Conf::EDIT_FONT).set(spec);
        }
    }

    bool on_iterate(const tau::Loop::Slot_iterate & s) {
        try { s(); }
        catch (tau::exception & x) { error_screen(x.what()); }
        catch (std::exception & x) { error_screen(x.what()); }
        catch (...) { error_screen(dgettext("tau", "Unknown Error")); }
        return true;
    }

    void error_screen(const tau::ustring & msg) {
        set_title(tau::program_name()+": "+dgettext("tau", "Unrecoverable Fault"));
        tau::Box hbox(tau::Orientation::EAST, tau::Align::CENTER);
        hbox.conf().redirect(tau::Conf::BACKGROUND, tau::Conf::ERROR_BACKGROUND);
        insert(hbox);
        hbox.connect_action(action_copy_);

        tau::Box vbox(tau::Orientation::SOUTH, tau::Align::CENTER, 12);
        hbox.append(vbox);

        tau::Icon icon("dialog-warning", tau::Icon::HEAVY);
        vbox.append(icon, true);

        tau::Label label(dgettext("tau", "Unrecoverable Fault"));
        label.conf().color(tau::Conf::FOREGROUND) = tau::Color("Red");
        label.conf().font().resize(18);
        label.conf().font().make_bold();
        vbox.append(label, true);

        std::u32string s;

        for (std::size_t i = 0; i < msg.size(); ++i) {
            if (i && 0 == (i%32)) { s += U'\n'; }
            s += msg[i];
        }

        auto text = tau::Text(s);
        action_copy_.connect(tau::bind_back(tau::fun(this, &Main::on_text_copy), text.ptr()));
        text.conf().color(tau::Conf::FOREGROUND) = tau::Color("Yellow");
        text.conf().redirect(tau::Conf::FONT, tau::Conf::MONOSPACE_FONT);
        text.conf().font().resize(15);
        text.allow_select();
        vbox.append(text, true);

        auto accels = action_copy_.accels();
        if (!accels.empty()) { text.set_tooltip(dgettext("tau", "You can select and copy error message by pressing")+" <"+accels.front()->label()+'>'); }

        label = tau::Label(dgettext("tau", "Press button to quit program"));
        label.conf().color(tau::Conf::FOREGROUND) = tau::Color("Red");
        label.conf().font().make_bold();
        vbox.append(label, true);

        hbox = tau::Box(tau::Orientation::EAST, tau::Align::CENTER);
        vbox.append(hbox, true);

        tau::Button btn("Quit");
        btn.connect(fun(this, &Main::close));
        hbox.append(btn, true);
        btn.allow_focus();
        btn.take_focus();
    }

    // @param wp is tau::Text::ptr().
    void on_text_copy(tau::Widget_ptr wp) {
        tau::Text txt(wp);
        if (txt.has_selection()) { txt.action_copy().exec(); }
    }

    void metrics() {
        if (auto wp = lookup("Ntm")) { tau::Label(wp).assign(tau::str_format(tau::metric(tau::Metric::TIMERS))); }
        if (auto wp = lookup("Nw")) { tau::Label(wp).assign(tau::str_format(tau::metric(tau::Metric::WIDGETS))); }
        if (auto wp = lookup("Ns")) { tau::Label(wp).assign(tau::str_format(tau::metric(tau::Metric::SIGNALS))); }
        if (auto wp = lookup("Nsl")) { tau::Label(wp).assign(tau::str_format(tau::metric(tau::Metric::SLOTS))); }
        if (auto wp = lookup("Nt")) { tau::Label(wp).assign(tau::str_format(tau::metric(tau::Metric::TRACKABLES))); }
        if (auto wp = lookup("Ntr")) { tau::Label(wp).assign(tau::str_format(tau::metric(tau::Metric::TRACKS))); }
    }

    void on_geometry() {
        tau::Point pos = position();
        tau::Size  sz  = size();
        geom_[0] = pos.x();
        geom_[1] = pos.y();
        geom_[2] = sz.iwidth();
        geom_[3] = sz.iheight();
        kstate_.set_integers(kstate_.root(), "geometry", geom_);
    }

    Main(const tau::Rect & bounds=tau::Rect()):
        Toplevel(bounds)
    {
        loop_ = tau::Loop::this_loop();
        loop_.signal_iterate().connect(fun(this, &Main::on_iterate), true);
        datadir_ = tau::path_build(tau::path_user_data_dir(), tau::program_name());
        tau::file_mkdir(datadir_);
        ksession_.create_from_file(tau::path_build(datadir_, "session.ini"));
        font_spec_ = kstate_.get_string("font", tau::Font::mono());
        font_size_ = kstate_.get_integer("font-size", 10);

        connect_action(action_escape_);
        connect_action(file_quit_action_);
        connect_action(action_new_);
        connect_action(action_open_);
        action_save_.disable();
        connect_action(action_save_);
        connect_action(file_save_as_action_);
        file_save_as_action_.disable();
        connect_action(file_save_all_action_);
        file_save_all_action_.disable();
        connect_action(file_close_action_);
        file_close_action_.disable();
        connect_action(file_close_all_action_);
        file_close_all_action_.disable();
        connect_action(file_close_others_action_);
        file_close_others_action_.disable();

        edit_undo_action_.disable();
        connect_action(edit_undo_action_);
        edit_redo_action_.disable();
        connect_action(edit_redo_action_);

        edit_unselect_.disable();
        connect_action(edit_unselect_);

        edit_select_all_mx_.disable();
        connect_action(edit_action_select_all_);

        edit_copy_mx_.disable();
        connect_action(edit_action_copy_);

        edit_cut_mx_.disable();
        connect_action(edit_cut_action_);

        edit_paste_master_action_.disable();
        connect_action(edit_paste_action_);

        zin_mx_.disable();
        connect_action(zin_ax_);

        zout_mx_.disable();
        connect_action(zout_ax_);

        connect_action(view_next_page_action_);
        view_next_page_action_.disable();

        connect_action(view_prev_page_action_);
        view_prev_page_action_.disable();

        connect_action(settings_action_);

        signal_menu().connect(tau::fun(menubar_, &tau::Menubar::activate));
        signal_close().connect(tau::fun(this, &Main::save_metas));
        signal_close().connect(tau::fun(this, &Main::save_session));
        signal_take_focus().connect(fun(notebook_, &tau::Widget::take_focus), true);

        notebook_.signal_page_added().connect(fun(this, &Main::on_notebook_page_added));
        notebook_.signal_page_removed().connect(fun(this, &Main::on_notebook_page_removed));
        notebook_.signal_page_reordered().connect(fun(this, &Main::on_notebook_page_reordered));
        notebook_.signal_page_changed().connect(fun(this, &Main::on_notebook_page_changed));
        notebook_.signal_show().connect(fun(this, &Main::update_title));

        tau::Display().signal_clipboard_changed().connect(fun(this, &Main::on_clipboard_changed));

        tau::Box box0(tau::Orientation::DOWN);
        insert(box0);
        populate_main_menu(menubar_);
        box0.append(menubar_, true);
        tau::Bin bin0;
        box0.append(bin0, true);
        toolbar_.hint_margin(3);
        bin0.insert(toolbar_);
        card_.insert(notebook_);
        box0.append(card_);

        toolbar_.conf().boolean(tau::Conf::BUTTON_LABEL) = false;
        tau::Button button(action_open_); toolbar_.append(button, true);
        button = tau::Button(action_save_); toolbar_.append(button, true);
        button = tau::Button(file_save_as_action_); toolbar_.append(button, true);
        tau::Separator sep; toolbar_.append(sep, true);
        button = tau::Button(edit_undo_action_); toolbar_.append(button, true);
        button = tau::Button(edit_redo_action_); toolbar_.append(button, true);
        sep = tau::Separator(); toolbar_.append(sep, true);
        button = tau::Button(edit_action_copy_); toolbar_.append(button, true);
        button = tau::Button(edit_cut_action_); toolbar_.append(button, true);
        button = tau::Button(edit_paste_action_); toolbar_.append(button, true);

        update_title();
        set_icon("tau", 48);
        signal_display_in().connect(tau::fun(this, &Main::on_display));
        signal_size_changed().connect(tau::fun(this, &Main::on_geometry));
        signal_position_changed().connect(tau::fun(this, &Main::on_geometry));
        menubar_.signal_quit().connect(tau::fun(this, &Main::on_menu_quit));

        // Show some system metrics at the bottom.
        tau::Box met(3);
        met.disallow_focus();
        met.conf().set(tau::Conf::BACKGROUND, "#333");
        met.conf().set(tau::Conf::FOREGROUND, "Cyan");
        box0.append(met, true);

        tau::Label label("Ntm"); label.hint_margin_left(3); met.append(label, true);
        label = tau::Label("0"); met.append(label, true); link(label, "Ntm");

        label = tau::Label("Nw"); label.hint_margin_left(3); met.append(label, true);
        label = tau::Label("0"); met.append(label, true); link(label, "Nw");

        label = tau::Label("Ns"); label.hint_margin_left(3); met.append(label, true);
        label = tau::Label("0"); met.append(label, true); link(label, "Ns");

        label = tau::Label("Nsl"); label.hint_margin_left(3); met.append(label, true);
        label = tau::Label("0"); met.append(label, true); link(label, "Nsl");

        label = tau::Label("Nt"); label.hint_margin_left(3); met.append(label, true);
        label = tau::Label("0"); met.append(label, true); link(label, "Nt");

        label = tau::Label("Ntr"); label.hint_margin_left(3); met.append(label, true);
        label = tau::Label("0"); met.append(label, true); link(label, "Ntr");

        loop_.alarm(fun(this, &Main::metrics), 4945, true);
        metrics();
    }
};

int main(int argc, char * argv[]) {
    try {
        tau::Locale::set();

        for (int i = 1; i < argc; ++i) {
            std::string arg(argv[i]);

            if ('-' != arg[0]) {
                tau::ustring path = tau::Locale().iocharset().decode(arg);
                if (!tau::path_is_absolute(path)) { path = tau::path_build(tau::path_cwd(), path); }
                if (args_.end() == std::find(args_.begin(), args_.end(), path)) { args_.push_back(path); }
            }

            else {
                if (("-l" == arg || "--line" == arg) && i+1 < argc) { line_ = std::stoi(argv[++i]); }
                else if (("-c" == arg || "--column" == arg) && i+1 < argc) { col_ = std::stoi(argv[++i]); }
            }
        }

        kstate_.create_from_file(tau::path_build(tau::path_user_data_dir(), tau::program_name(), "state.ini"));
        geom_ = kstate_.get_integers("geometry", 4);
        tau::Timer timer(fun(kstate_, &tau::Key_file::flush));
        kstate_.signal_changed().connect(bind_back(fun(timer, &tau::Timer::start), 7401, false));
        Main w(tau::Rect(geom_[0], geom_[1], tau::Size(geom_[2], geom_[3])));
        tau::Loop().run();
        kstate_.set_integers("geometry", geom_);
        kstate_.flush();
    }

    catch (tau::exception & x) { std::cerr << "** tau::exception thrown: " << x.what() << std::endl; }
    catch (std::exception & x) { std::cerr << "** std::exception thrown: " << x.what() << std::endl; }
    catch (...) { std::cerr << "** unknown exception thrown: " << std::endl; }
    return 0;
}

//END

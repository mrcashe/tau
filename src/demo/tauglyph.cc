// ----------------------------------------------------------------------------
// SPDX-License-Identifier: BSD-2-Clause
// Copyright © 2014-2024 Konstantin Shmelkov <mrcashe@gmail.com>.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
// THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
// PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
// CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
// EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
// PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
// OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
// WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
// OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
// EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// ----------------------------------------------------------------------------

#include <tau.hh>
#include <algorithm>
#include <cstdlib>
#include <cstring>
#include <cmath>
#include <fstream>
#include <iostream>
#include <iomanip>
#include <map>

tau::Point              pos_                        { 200, 200 };
tau::Size               sz_                         { 700, 500 };
tau::Key_file           kstate_;

struct Main: public tau::Toplevel {
    tau::ustring        spec_;
    char32_t            gchar_                      = 32;
    tau::Matrix         mat_;

    tau::Widget         area_;
    tau::List_text      families_;
    tau::List_text      faces_;
    tau::Painter        painter_;
    tau::Text           ex_                         { tau::Align::LEFT };       // Exception screen.

    tau::Label          char_label_;
    tau::Label          code_label_;
    tau::Label          font_label_;
    tau::Label          ps_label_                   { "---" };
    tau::Label          pitch_label_                { "?" };
    tau::Label          gxbear_label_               { "0.00", tau::Align::END };
    tau::Label          gybear_label_               { "0.00", tau::Align::END };
    tau::Label          gxmin_label_                { "0.00", tau::Align::END };
    tau::Label          gymin_label_                { "0.00", tau::Align::END };
    tau::Label          gxmax_label_                { "0.00", tau::Align::END };
    tau::Label          gymax_label_                { "0.00", tau::Align::END };
    tau::Label          gxadv_label_                { "0.00", tau::Align::END };
    tau::Label          gyadv_label_                { "0.00", tau::Align::END };

    tau::Action         action_escape_              { "Escape Cancel", fun(this, &Main::close) };
    tau::Action         action_previous_            { tau::KC_LEFT, tau::KM_NONE, fun(this, &Main::on_glyph_prev) };
    tau::Action         action_next_                { tau::KC_RIGHT, tau::KM_NONE, fun(this, &Main::on_glyph_next) };
    tau::Action         action_previous_family_     { tau::KC_UP, tau::KM_NONE, fun(this, &Main::on_family_prev) };
    tau::Action         action_next_family_         { tau::KC_DOWN, tau::KM_NONE, fun(this, &Main::on_family_next) };
    tau::Action         action_previous_face_       { tau::KC_UP, tau::KM_CONTROL, fun(this, &Main::on_face_prev) };
    tau::Action         action_next_face_           { tau::KC_DOWN, tau::KM_CONTROL, fun(this, &Main::on_face_next) };
    tau::Toggle_action  action_view_grid_           { "F2", "G", "", "Show/Hide Grid", fun(this, &Main::on_view_grid) };
    tau::Toggle_action  action_view_mas_            { "F3", "MAS", "", "Show/Hide Master Contour", fun(this, &Main::on_view_mas) };
    tau::Toggle_action  action_view_ctr_            { "F4", "CTR", "", "Show/Hide Scaled Contour", fun(this, &Main::on_view_ctr) };
    tau::Toggle_action  action_view_pix_            { "F5", "PIX", "", "Show/Hide Pixmap", fun(this, &Main::on_view_pix) };
    tau::Action         zin_                        { "<Ctrl>+ <Ctrl>=", fun(this, &Main::increase_font) };
    tau::Action         zout_                       { "<Ctrl>-", fun(this, &Main::decrease_font) };

    tau::connection     grid_cx_;
    tau::connection     pixmap_cx_;
    tau::connection     mas_cx_;
    tau::connection     ctr_cx_;

    tau::Glyph          glyph_;
    tau::Font           font_;
    tau::Pixmap         gpix_;

    tau::Absolute       absolute_;
    tau::ustring        test_text_                  { " : -+178(}|[/>@$#&*AabCDeFgHijKLMNpQRSTUVWXyZ" };

    int                 ascent_                     = 0;
    int                 descent_                    = 0;
    int                 npx_                        = 0;
    int                 npy_                        = 0;
    int                 gxmin_                      = 0;
    int                 gymin_                      = 0;
    int                 gxmax_                      = 0;
    int                 gymax_                      = 0;
    double              scale_                      = 1.0;
    double              x0_                         = 0.0;
    double              y0_                         = 0.0;
    double              ox_                         = 0.0;
    double              oy_                         = 0.0;

    Main():
        Toplevel(tau::Rect(pos_, sz_))
    {
        set_title("TAU Glyph Viewer");
        area_.hint_min_size(60);
        gchar_ = kstate_.get_integer("gchar", 32);

        signal_position_changed().connect(fun(this, &Main::on_geometry_changed));
        signal_size_changed().connect(fun(this, &Main::on_geometry_changed));

        connect_action(action_escape_);
        connect_action(action_previous_);
        connect_action(action_next_);
        connect_action(action_previous_family_);
        connect_action(action_previous_face_);
        connect_action(action_next_family_);
        connect_action(action_next_face_);
        connect_action(action_view_grid_);
        connect_action(action_view_mas_);
        connect_action(action_view_ctr_);
        connect_action(action_view_pix_);
        connect_action(zin_);
        connect_action(zout_);

        // Connect painting functions.
        grid_cx_ = area_.signal_paint().connect(fun(this, &Main::draw_grid));
        pixmap_cx_ = area_.signal_paint().connect(fun(this, &Main::draw_pixmap));
        mas_cx_ = area_.signal_paint().connect(fun(this, &Main::draw_master));
        ctr_cx_ = area_.signal_paint().connect(fun(this, &Main::draw_scaled));

        bool yes = kstate_.get_boolean("grid", true);
        if (!yes) { grid_cx_.block(); }
        action_view_grid_.setup(yes);

        yes = kstate_.get_boolean("master", true);
        if (!yes) { mas_cx_.block(); }
        action_view_mas_.setup(yes);

        yes = kstate_.get_boolean("scaled", true);
        if (!yes) { ctr_cx_.block(); }
        action_view_ctr_.setup(yes);

        yes = kstate_.get_boolean("pixmap", true);
        if (!yes) { pixmap_cx_.block(); }
        action_view_pix_.setup(yes);

        area_.signal_mouse_wheel().connect(fun(this, &Main::on_area_mouse_wheel));
        area_.signal_size_changed().connect(fun(this, &Main::update_font));
        area_.signal_size_changed().connect(fun(this, &Main::fetch_area_painter));
        area_.signal_display_in().connect(fun(this, &Main::fetch_area_painter));

        spec_ = kstate_.get_string("font");
        if (!spec_.empty()) { area_.conf().font().set(spec_); }
        else { spec_ = conf().font().spec(); }

        // Top box (vertical).
        tau::Box box0(tau::Orientation::DOWN);
        insert(box0);

        // Central box (horizontal) including view area (left) and info pane (right).
        tau::Box cbox(4);
        box0.append(cbox);

        area_.hint_margin(2);
        tau::Frame frame("Glyph View", tau::Border::RIDGE);
        frame.insert(area_);
        frame.hint_margin(4, 2, 0, 4);
        cbox.append(frame);

        // Info box (vertical) inserted into central box.
        tau::Box ibox(tau::Orientation::DOWN, 4);
        ibox.hint_margin(0, 4, 0, 0);
        cbox.append(ibox, true);

        tau::Frame iframe("Glyph Info", tau::Border::OUTSET);
        ibox.append(iframe, true);

        // Glyphinfo table being inserted into info frame.
        // Info frame inserted into info box.
        tau::Table itable(5, 2);
        itable.conf().font().set(tau::Font::mono());
        itable.conf().font().enlarge(-2);
        itable.align_column(2, tau::Align::END);
        itable.set_column_margin(2, 8, 2);
        itable.hint_margin(2);
        iframe.insert(itable);

        // Glyph minimal labels.
        tau::Label label("Minimal", tau::Align::START, tau::Align::CENTER);
        itable.put(label, 0, 0, 1, 2, true);
        tau::Label x("x:", tau::Align::END); itable.put(x, 1, 0, 1, 1);
        tau::Label y("y:", tau::Align::END); itable.put(y, 1, 1, 1, 1);
        itable.put(gxmin_label_, 2, 0, 1, 1, true, true);
        itable.put(gymin_label_, 2, 1, 1, 1, true, true);

        // Glyph maximal labels.
        label = tau::Label("Maximal", tau::Align::START, tau::Align::CENTER);
        itable.put(label, 0, 2, 1, 2, true);
        x = tau::Label("x:", tau::Align::END); itable.put(x, 1, 2, 1, 1);
        y = tau::Label("y:", tau::Align::END); itable.put(y, 1, 3, 1, 1);
        itable.put(gxmax_label_, 2, 2, 1, 1, true, true);
        itable.put(gymax_label_, 2, 3, 1, 1, true, true);

        // Glyph advance labels.
        label = tau::Label("Advance", tau::Align::START, tau::Align::CENTER);
        itable.put(label, 0, 4, 1, 2, true);
        x = tau::Label("x:", tau::Align::END); itable.put(x, 1, 4, 1, 1);
        y = tau::Label("y:", tau::Align::END); itable.put(y, 1, 5, 1, 1);
        itable.put(gxadv_label_, 2, 4, 1, 1, true, true);
        itable.put(gyadv_label_, 2, 5, 1, 1, true, true);

        // Glyph bearing labels.
        label = tau::Label("Bearing", tau::Align::START, tau::Align::CENTER);
        itable.put(label, 0, 6, 1, 2, true);
        x = tau::Label("x:", tau::Align::END); itable.put(x, 1, 6, 1, 1);
        y = tau::Label("y:", tau::Align::END); itable.put(y, 1, 7, 1, 1);
        itable.put(gxbear_label_, 2, 6, 1, 1, true, true);
        itable.put(gybear_label_, 2, 7, 1, 1, true, true);

        // Font families.
        frame = tau::Frame("Font Families", tau::Border::GROOVE);
        ibox.append(frame, true);
        tau::Box box;
        frame.insert(box);
        families_.hint_max_size(220, 140);
        families_.signal_select_text().connect(fun(this, &Main::on_family_selected));
        tau::Scroller scroller;
        scroller.insert(families_);
        box.append(scroller);
        tau::Slider slider(scroller);
        box.append(slider, true);

        // Font faces.
        frame = tau::Frame("Font Faces", tau::Border::GROOVE);
        ibox.append(frame, true);
        box = tau::Box();
        frame.insert(box);
        scroller = tau::Scroller();
        box.append(scroller);
        faces_.hint_min_size(0, 80);
        faces_.hint_max_size(220, 120);
        faces_.signal_select_text().connect(fun(this, &Main::on_face_selected));
        scroller.insert(faces_);
        slider = tau::Slider(scroller);
        box.append(slider, true);

        // Button box.
        tau::Box bbox(tau::Align::CENTER, 4);
        ibox.append(bbox, true);
        tau::Toggle grid_button(action_view_grid_); bbox.append(grid_button, true);
        tau::Toggle mas_button(action_view_mas_); bbox.append(mas_button, true);
        tau::Toggle ctr_button(action_view_ctr_); bbox.append(ctr_button, true);
        tau::Toggle pix_button(action_view_pix_); bbox.append(pix_button, true);

        // Test text absolute container.
        tau::Box abox;
        abox.hint_margin(4);
        abox.conf().item(tau::Conf::BACKGROUND).set("Black");
        abox.align(tau::Align::CENTER);
        absolute_.hint_min_size(10);
        absolute_.hint_margin(5);
        abox.append(absolute_, true);
        box0.append(abox, true);

        // Status bar frame.
        frame = tau::Frame(tau::Border::RIDGE);
        frame.hint_margin(4, 4, 0, 4);
        frame.conf().font().enlarge(-2);
        frame.conf().item(tau::Conf::BACKGROUND).set("#404040");
        frame.conf().item(tau::Conf::FOREGROUND).set("#17BBF0");
        box0.append(frame, true);

        // Status box (horizontal).
        tau::Box status(2);
        status.hint_margin(2);
        frame.insert(status);

        // Character status item.
        label = tau::Label("Char:");
        status.append(label, true);
        frame = tau::Frame(tau::Border::SOLID);
        status.append(frame, true);
        char_label_.hint_margin(2, 2, 0, 0);
        char_label_.hint_min_size(32, 0);
        char_label_.conf().item(tau::Conf::FOREGROUND).set("White");
        char_label_.assign(tau::ustring(1, gchar_));
        frame.insert(char_label_);

        // Character code status item.
        label = tau::Label("Code:");
        label.hint_margin_left(6);
        status.append(label, true);
        frame = tau::Frame(tau::Border::SOLID);
        status.append(frame, true);
        code_label_.hint_margin(2, 2, 0, 0);
        code_label_.conf().font().set(tau::Font::enlarge(tau::Font::mono(), -2));
        code_label_.assign(tau::str_unicode(gchar_));
        frame.insert(code_label_);

        // Font spec status item.
        label = tau::Label("Font:");
        label.hint_margin_left(6);
        status.append(label, true);
        frame = tau::Frame(tau::Border::SOLID);
        status.append(frame, true);
        font_label_.hint_margin(2, 2, 0, 0);
        font_label_.assign(spec_);
        frame.insert(font_label_);

        // PostScript Name item.
        label = tau::Label("PS:");
        label.hint_margin_left(6);
        status.append(label, true);
        frame = tau::Frame(tau::Border::SOLID);
        status.append(frame, true);
        ps_label_.hint_margin(2, 2, 0, 0);
        frame.insert(ps_label_);

        // Pitch item.
        label = tau::Label("Pitch:");
        label.hint_margin_left(6);
        status.append(label, true);
        frame = tau::Frame(tau::Border::SOLID);
        status.append(frame, true);
        pitch_label_.hint_margin(2, 2, 0, 0);
        frame.insert(pitch_label_);

        // Exception screen.
        ex_.conf().redirect(tau::Conf::BACKGROUND, tau::Conf::ERROR_BACKGROUND);
        ex_.conf().set(tau::Conf::FOREGROUND, "Brown");
        ex_.hint_margin(4, 4, 0, 4);
        ex_.hide();
        box0.append(ex_, true);

        signal_display_in().connect(fun(this, &Main::on_display_in));
        set_icon("tau", 48);
    }

    void on_display_in() {
        families_.append(tau::Font::list_families());
        families_.select_similar(tau::Font::family(spec_));
        update_glyph();
    }

    tau::Pixmap raster_glyph(tau::Glyph glyph) {
        tau::Rect r = glyph.bounds();
        tau::Pixmap pix(32, r.size());

        if (auto pr = pix.painter()) {
            pr.set_brush(tau::Color());
            pr.paint();
            pr.move_to(-r.left(), std::ceil(glyph.max().y()));
            pr.glyph(glyph);
            pr.set_brush(tau::Color("White"));
            pr.fill();
        }

        return pix;
    }

    void on_geometry_changed() {
        sz_ = size();
        pos_  = position();
    }

    void increase_font() {
        tau::ustring spec = area_.conf().font().spec();
        int pt = tau::Font::points(spec);

        if (pt < 100) {
            spec_ = tau::Font::resize(spec, ++pt);
            update_font();
        }
    }

    void decrease_font() {
        tau::ustring spec = area_.conf().font().spec();
        int pt = tau::Font::points(spec);

        if (pt > 1) {
            spec_ = tau::Font::resize(spec, --pt);
            update_font();
        }
    }

    void update_test_text() {
        absolute_.clear();

        if (auto pr = absolute_.painter()) {
            try {
                int xx = 0;
                std::vector<tau::Pixmap> pixx;
                std::vector<tau::Point> pts;

                if (tau::Font font = pr.select_font(spec_)) {
                    for (char32_t wc: test_text_) {
                        if (tau::Glyph glyph = font.glyph(wc)) {
                            tau::Pixmap pix = raster_glyph(glyph);
                            pixx.push_back(pix);
                            int x = std::floor(glyph.bearing().x());
                            int y = std::ceil(font.ascent())-std::ceil(glyph.max().y());
                            pts.emplace_back(x+xx, y);
                            tau::Vector adv = glyph.advance();
                            int x_off = std::ceil(adv.x());
                            xx += x_off+2;
                        }
                    }

                    xx = 0;
                    absolute_.hint_size(0, std::max(10.0, std::ceil(font.ascent())+std::ceil(std::fabs(font.descent()))));

                    for (auto pix: pixx) {
                        tau::Image img(pix, false);
                        absolute_.put(img, pts[xx++]);
                    }
                }
            }

            catch (tau::exception & x) {
                ex_.assign(x.what());
                ex_.show();
                std::cerr << "** " << __func__ << x.what() << std::endl;
            }
        }
    }

    void on_family_selected(const tau::ustring &) {
        tau::ustring face = tau::Font::face(spec_);
        auto facev = tau::Font::list_faces(families_.str());
        std::sort(facev.begin(), facev.end());
        faces_.clear();
        for (auto & s: facev) { faces_.append(s); }
        if (INT_MIN == faces_.select(face)) { faces_.select_front(); }
        spec_ = tau::Font::build(families_.str(), faces_.str(), tau::Font::points(spec_));
        update_font();
    }

    void on_face_selected(const tau::ustring &) {
        spec_ = tau::Font::build(families_.str(), faces_.str(), tau::Font::points(spec_));
        update_font();
    }

    void fill_faces() {
        auto sel = faces_.str();
        auto v = tau::Font::list_faces(tau::Font::family(spec_));
        std::sort(v.begin(), v.end());
        faces_.clear();
        for (auto & s: v) { faces_.append(s); }
        if (INT_MIN == faces_.select(!sel.empty() ? sel : tau::Font::face(spec_))) { faces_.select_front(); }
    }

    void update_font() {
        ex_.hide();
        font_label_.assign(spec_);
        area_.conf().font().set(spec_);
        kstate_.set_string("font", spec_);

        if (auto pr = painter()) {
            try {
                font_ = pr.select_font(spec_);

                if (font_) {
                    auto psname = font_.psname();
                    ps_label_.assign(psname.empty() ? "---" : psname);
                    pitch_label_.assign(font_.monospace() ? "Fixed" : "Variable");
                    update_test_text();
                    ascent_  = std::ceil(font_.ascent());
                    descent_ = std::floor(font_.descent());
                    npy_ = ascent_-descent_;
                    glyph_ = pr.font().glyph(gchar_);
                    update_glyphinfo();
                    gpix_ = tau::Pixmap(32);

                    std::size_t width = area_.size().width();
                    std::size_t height = area_.size().height();
                    y0_ = npy_+descent_;
                    tau::Vector fmin = glyph_.min()-tau::Vector(1, 1);
                    tau::Vector fmax = glyph_.max()+tau::Vector(1, 1);
                    npx_ = std::max(1, int(std::ceil(fmax.x())-std::ceil(fmin.x())));
                    x0_ = -std::ceil(fmin.x());
                    scale_ = std::min(width/npx_, height/npy_);

                    tau::Vector gmin = glyph_.min();
                    tau::Vector gmax = glyph_.max();
                    gxmin_ = x0_+std::floor(gmin.x());
                    gymin_ = y0_-std::floor(gmin.y());
                    gxmax_ = x0_+std::ceil(gmax.x());
                    gymax_ = y0_-std::ceil(gmax.y());
                    ox_ = 0.5*(width-(npx_*scale_));
                    oy_ = 0.5*(height-(npy_*scale_));

                    mat_ = tau::Matrix();
                    mat_.translate(ox_, oy_);
                    mat_.scale(scale_);

                    area_.invalidate();
                }
            }

            catch (tau::exception & x) {
                ex_.assign(x.what());
                ex_.show();
                std::cerr << "** " << __func__ << ": " << x.what() << std::endl;
            }
        }
    }

    void update_glyph() {
        kstate_.set_integer("gchar", gchar_);
        char_label_.assign(tau::ustring(1, gchar_));
        code_label_.assign(tau::str_unicode(gchar_));
        update_font();
    }

    void next_glyph() {
        char32_t gc = gchar_;

        if (0x11ffff != gc) {
            if (0x007e == gc) { gc = 0x00a0; }
            else { ++gc; }

            if (gc != gchar_) {
                gchar_ = gc;
                update_glyph();
                test_text_.replace(0, 1, 1, gc);
                update_test_text();
            }
        }
    }

    void prev_glyph() {
        char32_t gc = gchar_;

        if (0x0000 != gc) {
            if (0x00a0 == gc) { gc = 0x007e; }
            else { --gc; }

            if (gc != gchar_) {
                gchar_ = gc;
                update_glyph();
                test_text_.replace(0, 1, 1, gc);
                update_test_text();
            }
        }
    }

    void on_glyph_next() {
        next_glyph();
    }

    void on_glyph_prev() {
        prev_glyph();
    }

    void on_family_next() {
        families_.action_next().exec();
    }

    void on_family_prev() {
        families_.action_previous().exec();
    }

    void on_face_next() {
        faces_.action_next().exec();
    }

    void on_face_prev() {
        faces_.action_previous().exec();
    }

    void fetch_area_painter() {
        if (auto pr = area_.painter()) {
            painter_ = pr;
        }
    }

    bool on_area_mouse_wheel(int delta, uint16_t mb_all, const tau::Point & where) {
        if ((tau::MM_CONTROL|tau::MM_RIGHT) & mb_all) {
            if (delta > 0) {
                increase_font();
            }

            else if (delta < 0) {
                decrease_font();
            }
        }

        else {
            if (delta > 0) {
                next_glyph();
            }

            else if (delta < 0) {
                prev_glyph();
            }
        }

        return true;
    }

    void update_glyphinfo() {
        if (glyph_) {
            gxmin_label_.assign(tau::str_format(std::locale(), std::fixed, std::setprecision(2), glyph_.min().x()));
            gymin_label_.assign(tau::str_format(std::locale(), std::fixed, std::setprecision(2), glyph_.min().y()));
            gxmax_label_.assign(tau::str_format(std::locale(), std::fixed, std::setprecision(2), glyph_.max().x()));
            gymax_label_.assign(tau::str_format(std::locale(), std::fixed, std::setprecision(2), glyph_.max().y()));
            gxadv_label_.assign(tau::str_format(std::locale(), std::fixed, std::setprecision(2), glyph_.advance().x()));
            gyadv_label_.assign(tau::str_format(std::locale(), std::fixed, std::setprecision(2), glyph_.advance().y()));
            gxbear_label_.assign(tau::str_format(std::locale(), std::fixed, std::setprecision(2), glyph_.bearing().x()));
            gybear_label_.assign(tau::str_format(std::locale(), std::fixed, std::setprecision(2), glyph_.bearing().y()));
        }

        else {
            gxmin_label_.assign("---");
            gymin_label_.assign("---");
            gxmax_label_.assign("---");
            gymax_label_.assign("---");
            gxadv_label_.assign("---");
            gyadv_label_.assign("---");
            gxbear_label_.assign("---");
            gybear_label_.assign("---");
        }
    }

    void draw_contour(tau::Painter pr, const tau::Contour & ctr, const tau::Color & color) {
        double bd = 3/scale_;
        tau::Vector cur = ctr.start(), end;

        for (auto & curve: ctr) {
            end = curve.end();
            pr.rectangle(x0_+cur.x()+bd, y0_-cur.y()+bd, x0_+cur.x()-bd, y0_-cur.y()-bd);
            pr.set_pen(tau::Pen(color, 2*scale_, tau::Line::SOLID, tau::Cap::ROUND));
            pr.fill();

            if (2 == curve.order()) {
                tau::Vector cp = curve.cp1();
                pr.rectangle(x0_+cp.x()+bd, y0_-cp.y()+bd, x0_+cp.x()-bd, y0_-cp.y()-bd);
                pr.set_brush(color);
                pr.fill();

                pr.move_to(x0_+cur.x(), y0_-cur.y());
                pr.conic_to(x0_+cp.x(), y0_-cp.y(), x0_+end.x(), y0_-end.y());
                pr.set_pen(tau::Pen(color, 2*scale_, tau::Line::SOLID, tau::Cap::ROUND));
                pr.stroke();
            }

            else {
                pr.move_to(x0_+cur.x(), y0_-cur.y());
                pr.line_to(x0_+end.x(), y0_-end.y());
                pr.set_pen(tau::Pen(color, 2*scale_, tau::Line::SOLID, tau::Cap::ROUND));
                pr.stroke();
            }

            cur = end;
        }
    }

    bool draw_master(tau::Painter pr, const tau::Rect & inval) {
        pr.push();
        pr.matrix() = mat_;

        if (font_)  {
            try {
                auto mas = font_.master_glyph(gchar_), g = font_.glyph(gchar_);
                double d1 = mas.advance().x(), d2 = g.advance().x();
                tau::Matrix m;
                m.scale(0.0 != d1 ? d2/d1 : 0);
                for (auto ctr: mas.contours()) { ctr *= m; draw_contour(pr, ctr, tau::Color("Orange")); }
            }

            catch (tau::exception & x) {
                ex_.assign(x.what());
                ex_.show();
                std::cerr << "** " << __func__ << ": " << x.what() << std::endl;
            }
        }

        pr.pop();
        return false;
    }

    bool draw_scaled(tau::Painter pr, const tau::Rect & inval) {
        pr.push();
        pr.matrix() = mat_;

        if (font_) {
            try {
                for (auto & ctr: font_.glyph(gchar_).contours()) {
                    draw_contour(pr, ctr, tau::Color("DeepPink"));
                }
            }

            catch (tau::exception & x) {
                ex_.assign(x.what());
                ex_.show();
                std::cerr << "** " << __func__ << ": " << x.what() << std::endl;
            }
        }

        pr.pop();
        return false;
    }

    bool draw_pixmap(tau::Painter pr, const tau::Rect & inval) {
        pr.push();
        pr.matrix() = mat_;

        if (glyph_)  {
            gpix_ = raster_glyph(glyph_);

            for (int x = 0; x < npx_; ++x) {
                double bd = 0.05;

                for (int y = 0; y < npy_; ++y) {
                    tau::Color c = tau::Color("PowderBlue");

                    if (!gpix_.empty()) {
                        if (x >= gxmin_ && x < gxmax_ && y >= gymax_ && y < gymin_) {
                            int px = x-gxmin_, py = y-gymax_;
                            c = gpix_.get_pixel(px, py);
                        }
                    }

                    pr.rectangle(x+bd, y+bd, x-bd+1.0, y-bd+1.0);
                    pr.set_brush(c);
                    pr.fill();
                }
            }
        }

        pr.pop();
        return false;
    }

    bool draw_grid(tau::Painter pr, const tau::Rect & inval) {
        pr.push();
        pr.matrix() = mat_;
        tau::Matrix m = mat_.inverted();
        tau::Vector vn = m*tau::Vector(0, 0);
        tau::Vector vk = m*tau::Vector(area_.size().width(), area_.size().height());

        pr.move_to(vn.x(), y0_);
        pr.line_to(vk.x(), y0_);
        pr.move_to(x0_, vn.y());
        pr.line_to(x0_, vk.y());
        pr.set_pen(tau::Pen(tau::Color("Black"), 4, tau::Line::DASH));
        pr.stroke();

        if (vk.y() > vn.y()) {
            for (double y = std::floor(vn.y()); y < vk.y(); y += 1.0) {
                pr.move_to(vn.x(), y);
                pr.line_to(vk.x(), y);
            }
        }

        else {
            for (double y = std::floor(vk.y()); y < vn.y(); y += 1.0) {
                pr.move_to(vn.x(), y);
                pr.line_to(vk.x(), y);
            }
        }

        for (double x = std::ceil(vn.x()); x < vk.x(); x += 1.0) {
            pr.move_to(x, vn.y());
            pr.line_to(x, vk.y());
        }

        pr.set_pen(tau::Pen(tau::Color("Gray"), 0, tau::Line::DASH));
        pr.stroke();
        pr.pop();
        return false;
    }

    void on_view_grid(bool yes) {
        if (!yes) { grid_cx_.block(); }
        else { grid_cx_.unblock(); }
        kstate_.set_boolean("grid", yes);
        area_.invalidate();
    }

    void on_view_mas(bool yes) {
        if (!yes) { mas_cx_.block(); }
        else { mas_cx_.unblock(); }
        kstate_.set_boolean("master", yes);
        area_.invalidate();
    }

    void on_view_ctr(bool yes) {
        if (!yes) { ctr_cx_.block(); }
        else { ctr_cx_.unblock(); }
        kstate_.set_boolean("scaled", yes);
        area_.invalidate();
    }

    void on_view_pix(bool yes) {
        if (!yes) { pixmap_cx_.block(); }
        else { pixmap_cx_.unblock(); }
        kstate_.set_boolean("pixmap", yes);
        area_.invalidate();
    }
};

int main(int argc, char * argv[]) {
    try {
        tau::Locale::set();
        kstate_.create_from_file(tau::path_build(tau::path_user_config_dir(), tau::program_name(), "state.ini"));
        tau::Timer timer(fun(kstate_, &tau::Key_file::flush));
        kstate_.signal_changed().connect(bind_back(fun(timer, &tau::Timer::start), 7738, false));
        auto v = kstate_.get_integers("geometry", 5);
        pos_.set(v[0], v[1]);
        sz_.update(v[2], v[3]);
        Main wnd;
        tau::Loop().run();
        std::vector<intmax_t> iv = { pos_.x(), pos_.y(), sz_.iwidth(), sz_.iheight() };
        kstate_.set_integers("geometry", iv);
        kstate_.flush();
    }

    catch (tau::exception & x) { std::cerr << "** tau::exception thrown: " << x.what() << std::endl; }
    catch (std::exception & x) { std::cerr << "** std::exception thrown: " << x.what() << std::endl; }
    catch (...) { std::cerr << "** tunknown exception thrown: " << std::endl; }
    kstate_.flush();
    return 0;
}

//END

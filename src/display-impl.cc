// ----------------------------------------------------------------------------
// SPDX-License-Identifier: BSD-2-Clause
// Copyright © 2014-2024 Konstantin Shmelkov <mrcashe@gmail.com>.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
// THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
// PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
// CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
// EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
// PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
// OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
// WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
// OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
// EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// ----------------------------------------------------------------------------

#include <dialog-impl.hh>
#include <display-impl.hh>
#include <loop-impl.hh>
#include <algorithm>
#include <iostream>

namespace {

tau::signal<void(tau::Display_impl *)>  signal_open_;

} // anonymous namespace

// ----------------------------------------------------------------------------
// ----------------------------------------------------------------------------

namespace tau {

Display_impl::Display_impl() {
    signal_quit_.connect(fun(this, &Display_impl::on_quit));
}

void Display_impl::on_quit() {
    windows_.clear();
    sanitize_.clear();
    family_listers_.clear();
}

    Window_impl * Display_impl::set_mouse_owner(Window_impl * wip, const Point & pt) {
    if (mouse_owner_ != wip) {
        reset_mouse_owner();

        if (winptr(wip) && wip->enabled()) {
            mouse_owner_ = wip;
            wip->handle_mouse_enter(pt);
        }
    }

    return mouse_owner_;
}

void Display_impl::reset_mouse_owner() {
    auto wip = winptr(mouse_owner_);
    mouse_owner_ = nullptr;
    if (wip) { wip->handle_mouse_leave(); }
}

Window_ptr Display_impl::winptr(Widget_impl * wp) noexcept {
    auto i = std::find_if(windows_.begin(), windows_.end(), [wp](auto wip) { return wp == wip.get(); } );
    return i != windows_.end() ? std::dynamic_pointer_cast<Window_impl>(*i) : nullptr;
}

Window_cptr Display_impl::winptr(const Widget_impl * wp) const noexcept {
    auto i = std::find_if(windows_.begin(), windows_.end(), [wp](auto wip) { return wp == wip.get(); } );
    return i != windows_.end() ? std::dynamic_pointer_cast<Window_impl>(*i) : nullptr;
}

void Display_impl::add_window(Window_ptr wip) {
    windows_.push_front(wip);
    link(wip.get());
    loop()->signal_start().connect(fun(wip, &Window_impl::show));
    loop()->signal_quit().connect(fun(wip, &Window_impl::close));
    wip->signal_close().connect(bind_back(fun(this, &Display_impl::on_window_close), wip.get()));
    wip->signal_disable().connect(bind_back(fun(this, &Display_impl::on_window_disable), wip.get()));

    // Try to set icon.
    if (auto tip = std::dynamic_pointer_cast<Toplevel_impl>(wip)) {
        if (!std::dynamic_pointer_cast<Dialog_impl>(tip)) {
            tip->set_icon(program_name());
        }
    }
}

// Bound to Window's signal_close().
void Display_impl::on_window_close(Window_impl * wp) {
    if (auto wip = winptr(wp)) {
        wip->clear();   // FIXME decide for user?
        if (sanitize_.empty()) { loop()->alarm(fun(this, &Display_impl::on_sanitize), 11); }
        sanitize_.push_front(wip);
        wp->handle_display(false);
        windows_.remove(wip);
        unlink(wp);
        if (wp == modal_window_) { modal_window_  = nullptr; }
        if (wp == focused_) { focused_ = nullptr; }
        if (wp == mouse_grabber_) { mouse_grabber_ = nullptr; }
        if (wp == mouse_owner_) { mouse_owner_  = nullptr; }
    }
}

void Display_impl::focus_window(Window_impl * wip) {
    if (wip != focused_ && wip->focusable()) {
        if (auto wip = focused_) {
            focused_ = nullptr;
            wip->handle_focus_out();
        }

        if (wip) {
            focused_ = wip;
            wip->handle_focus_in();
        }
    }
}

bool Display_impl::unfocus_window(Window_impl * wip) {
    if (wip == focused_) {
        focused_ = nullptr;
        wip->handle_focus_out();
        return true;
    }

    return false;
}

// Overridden by Display_xcb.
void Display_impl::allow_screensaver() {
    if (0 != screensaver_counter_) {
        screensaver_counter_--;
    }
}

// Overridden by Display_xcb.
void Display_impl::disallow_screensaver() {
    screensaver_counter_++;
}

Widget_ptr Display_impl::focus_endpoint() noexcept {
    if (modal_window_) { return modal_window_->focus_endpoint(); }
    if (focused_) { return focused_->focus_endpoint(); }
    return nullptr;
}

Widget_cptr Display_impl::focus_endpoint() const noexcept {
    if (modal_window_) { return modal_window_->focus_endpoint(); }
    if (focused_) { return focused_->focus_endpoint(); }
    return nullptr;
}

// static
signal<void(Display_impl *)> & Display_impl::signal_open() {
    return signal_open_;
}

void Display_impl::on_window_disable(Window_impl * wip) {
    if (wip->disabled()) {
        if (wip == mouse_owner_) { reset_mouse_owner(); }
        unfocus_window(wip);
    }
}

// Slot for signal_alarm().
void Display_impl::on_sanitize() {
    if (std::any_of(sanitize_.begin(), sanitize_.end(), [](auto op) { return op->running(); } )) {
        std::cerr << "** " << this << " Display_impl::" << __func__ << "(): objects still running, continue" << std::endl;
        loop()->alarm(fun(this, &Display_impl::on_sanitize), 11);
    }

    else {
        sanitize_.clear();
        if (windows_.empty()) { loop()->quit(); }
    }
}

// Font family lister.
void Display_impl::on_family_run(Family_lister & lst) {
    if (famv_.end() == lst.iter) { lst.s(""); family_listers_.remove_if([lst](auto & el) { return &el == &lst; }); }
    else { lst.s(*lst.iter++); }
}

// Font family lister.
void Display_impl::list_families_async(slot<void(ustring)> slot_next) {
    auto & lst = family_listers_.emplace_front();
    lst.iter = famv_.begin();
    lst.s = slot_next;
    lst.cx = loop()->signal_run().connect(bind_back(fun(this, &Display_impl::on_family_run), std::ref(lst)));
}

} // namespace tau

//END

// ----------------------------------------------------------------------------
// SPDX-License-Identifier: BSD-2-Clause
// Copyright © 2014-2024 Konstantin Shmelkov <mrcashe@gmail.com>.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
// THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
// PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
// CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
// EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
// PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
// OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
// WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
// OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
// EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// ----------------------------------------------------------------------------

/// @file observer-impl.cc The Observer_impl class implementation.
/// The header file is observer-impl.hh.
/// @since 0.7.0

#include <tau/file.hh>
#include <tau/string.hh>
#include <gettext-impl.hh>
#include <loop-impl.hh>
#include <observer-impl.hh>
#include <iostream>

namespace tau {

Observer_impl::Observer_impl(const ustring & uri) {
    signal_activate_row().connect(fun(this, &Observer_impl::on_activate));
    auto loop = Loop_impl::this_ptr();

    for (const ustring & s: loop->mounts()) {
        if (File(s).is_removable()) {
            removables_.push_back(s);
        }
    }

    loop->signal_watch().connect(fun(this, &Observer_impl::on_watch));
    init();
    select_uri(uri);
}

void Observer_impl::select_uri(const ustring & uri) {
    auto v1 = path_explode(path_real(uri));

    if (!v1.empty()) {
        int longest = -1, y = INT_MIN;

        for (auto & p: map_) {
            auto v2 = path_explode(path_real(p.first));
            int n = 0;

            for (auto i = v2.begin(); n < int(v1.size()) && i != v2.end(); ++i, ++n) {
                if (!path_same(*i, v1[n])) { break; }
            }

            if (n == int(v2.size()) && n > longest) { longest = n; y = p.second; }
        }

        select(y);
    }
}

void Observer_impl::init_folder(const ustring & label, std::string_view icon_names, const ustring & uri) {
    auto dir = file_is_dir(uri) ? uri : path_home();
    auto tp = std::make_shared<Label_impl>(label, Align::START);
    tp->set_tooltip(dir);
    int y = append_row(tp, true);
    map_.emplace(dir, y);
    Icon_ptr ico = std::make_shared<Icon_impl>(icon_names, Icon::DEFAULT);
    insert(y, ico, -1, true);
}

void Observer_impl::init_user_dirs() {
    init_folder(lgettext("Home Folder"), "go-home:folder", path_home());
    init_folder(lgettext("Documents"), "folder-documents:folder", path_user_documents_dir());
    init_folder(lgettext("Desktop"), "user-desktop:folder", path_user_desktop_dir());
    init_folder(lgettext("Downloads"), "folder-downloads:folder-download:folder", path_user_downloads_dir());
    init_folder(lgettext("Music"), "folder-sound:folder-music:folder", path_user_music_dir());
    init_folder(lgettext("Pictures"), "folder-image:folder-images:folder", path_user_pictures_dir());
    init_folder(lgettext("Videos"), "folder-video:folder-videos:folder", path_user_videos_dir());
    init_folder(lgettext("Templates"), "folder-templates:folder", path_user_templates_dir());
}

void Observer_impl::on_watch(int flags, const ustring & mp) {
    if (File::UMOUNT & flags) {
        auto i = std::find_if(removables_.begin(), removables_.end(), [mp](auto & s) { return str_similar(mp, s); });
        if (i != removables_.end()) { removables_.erase(i); clear(); init(); }
    }

    else if (File::MOUNT & flags) {
        if (File(mp).is_removable()) {
            removables_.push_back(mp);
            clear(); init();
        }
    }
}

void Observer_impl::on_activate(int y) {
    auto i = std::find_if(map_.begin(), map_.end(), [y](auto & p) { return y == p.second; });
    if (i != map_.end()) { signal_activate_uri_(i->first); }
}

} // namespace tau

//END

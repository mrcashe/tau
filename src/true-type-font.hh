// ----------------------------------------------------------------------------
// SPDX-License-Identifier: BSD-2-Clause
// Copyright © 2014-2024 Konstantin Shmelkov <mrcashe@gmail.com>.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
// THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
// PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
// CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
// EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
// PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
// OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
// WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
// OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
// EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// ----------------------------------------------------------------------------

#ifndef __TAU_TRUE_TYPE_FONT_HH__
#define __TAU_TRUE_TYPE_FONT_HH__

#include "defs-impl.hh"
#include <tau/geometry.hh>
#include <tau/ustring.hh>
#include <filesystem>
#include <map>
#include <vector>

namespace tau {

class True_type_font {
public:

    True_type_font(const char * mem, std::size_t bytes);
    True_type_font(const ustring & fp);

    ustring family() const { return family_; }
    ustring face() const { return facename_.empty() ? "Regular" : facename_; }
    ustring fontname() const { return fontname_; }
    ustring psname() const { return psname_; }
    Rect bounds() const noexcept { return bbox_; }
    unsigned upm() const noexcept { return upm_; }
    int ascent() const noexcept { return hhea_.ascent; }
    int descent() const noexcept { return hhea_.descent; }
    int linegap() const noexcept { return hhea_.linegap; }
    int max_advance() const noexcept { return hhea_.max_advance; }
    int max_x_extent() const noexcept { return hhea_.max_x_extent; }

    /// @return minimal left sidebearing as first and minimal right sidebearing as second.
    std::pair<int, int> sidebearing() const noexcept { return { hhea_.min_lsb, hhea_.min_rsb }; }

    /// @return caret slope rise as first and caret slope run as second.
    std::pair<int, int> caret_slope() const noexcept { return { hhea_.caret_slope_rise, hhea_.caret_slope_run }; }
    std::vector<Glyph_ptr> glyphs(const std::u32string & str);
    Glyph_ptr zero();

private:

    struct Entry {
        uint32_t            ofs;
        uint32_t            len;
        uint32_t            cs;
    };

    struct Horz_header {
        int16_t             ascent              = 0;
        int16_t             descent             = 0;
        int16_t             linegap             = 0;
        int16_t             max_advance         = 0;
        int16_t             min_lsb             = 0;        // Left sidebearing.
        int16_t             min_rsb             = 0;        // Right sidebearing.
        int16_t             max_x_extent        = 0;
        bool                caret_slope_rise    = false;
        bool                caret_slope_run     = false;
        uint16_t            rcount              = 0;
    };

    struct Horz_metrics {
        uint16_t            adv                 = 0;
        int16_t             lsb                 = 0;
    };

    struct Loca {
        uint32_t            ofs;
        std::streamsize     len;
    };

    using Char_map          = std::map<char32_t, uint16_t>;
    using Loca_table        = std::vector<Loca>;
    using Horz_table        = std::vector<Horz_metrics>;
    using Entries           = std::map<ustring, Entry>;

    std::filesystem::path   path_;
    const char *            mem_            = nullptr;
    std::size_t             bytes_          = 0;
    Entries                 entries_;
    ustring                 family_;
    ustring                 facename_       { "Regular" };
    ustring                 fontname_       { "Unknown" };
    ustring                 psname_;
    unsigned                upm_            = 0;            // Units per EM.
    bool                    baseline0_      = false;        // Baseline at Y = 0.
    bool                    lsb_            = false;        // Left sidebearing at X = 0.
    bool                    idepend_        = false;        // Instructions may depend on point size.
    bool                    pforce_         = false;        // Force PPEM...
    bool                    ialter_         = false;        // Instructions may alter advance.
    bool                    loca32_         = false;        // Long LOCA str_format.
    unsigned                gcount_         = 0;            // Glyph count.
    Glyph_ptr               zero_;                          // 0th glyph.
    Rect                    bbox_;                          // Font bounding box.
    Char_map                cmap_;                          // Char code -> glyph index.
    Loca_table              loca_;                          // Glyph location table.
    Horz_header             hhea_;                          // Horizontal header.
    Horz_table              hmtx_;                          // Horizontal metrics.

private:

    Glyph_ptr glyph(std::istream * is, char32_t wc, uint16_t gindex);

    void preload();
    void preload(std::ifstream & is);

    void load_maxp();
    void load_maxp(std::istream & is);
    void load_maxp(const char * b, std::size_t len);

    void load_hmtx();
    void load_hmtx(std::istream & is);
    void load_hmtx(const char * b, std::size_t len);

    void load_hhea();
    void load_hhea(std::istream & is);
    void load_hhea(const char * b, std::size_t len);

    void load_loca();
    void load_loca(std::istream & is);
    void load_loca(const char * b, std::size_t len);

    void load_cmap();
    void load_cmap(std::istream & is);
    void load_cmap(const char * b, std::size_t len);

    void load_head();
    void load_head(std::ifstream & is);
    void load_head(const char * b, std::size_t len);

    void load_name();
    void load_name(std::ifstream & is);
    void load_name(const char * b, std::size_t len);
};

} // namespace tau

#endif // __TAU_TRUE_TYPE_FONT_HH__

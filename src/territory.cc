// ----------------------------------------------------------------------------
// SPDX-License-Identifier: BSD-2-Clause
// Copyright © 2014-2024 Konstantin Shmelkov <mrcashe@gmail.com>.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
// THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
// PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
// CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
// EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
// PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
// OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
// WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
// OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
// EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// ----------------------------------------------------------------------------

#include <tau/string.hh>
#include <locale-impl.hh>
#include <gettext-impl.hh>

namespace tau {

struct Territory_data {
    const char *        code2;
    const char *        code3;
    int                 num;
    const char *        ename;                              // English name.
    const char *        efull;                              // Full English name.
    const char *        eabbrev;                            // English abbreviation.
};

} // namespace tau

// ----------------------------------------------------------------------------
// ----------------------------------------------------------------------------

namespace {

tau::Territory_data data_[] = {
    {   .code2          = "",
        .code3          = "",
        .num            = 0,
        .ename          = gettext_noop("Unknown")
    },
    {   .code2          = "AF",
        .code3          = "AFG",
        .num            = 4,
        .ename          = gettext_noop("Afghanistan")
    },
    {   .code2          = "AX",
        .code3          = "ALA",
        .num            = 248,
        .ename          = gettext_noop("Åland Islands")
    },
    {   .code2          = "AL",
        .code3          = "ALB",
        .num            = 8,
        .ename          = gettext_noop("Albania")
    },
    {   .code2          = "DZ",
        .code3          = "DZA",
        .num            = 12,
        .ename          = gettext_noop("Algeria")
    },
    {   .code2          = "AS",
        .code3          = "ASM",
        .num            = 16,
        .ename          = gettext_noop("American Samoa")
    },
    {   .code2          = "AD",
        .code3          = "AND",
        .num            = 20,
        .ename          = gettext_noop("Andorra")
    },
    {   .code2          = "AO",
        .code3          = "AGO",
        .num            = 24,
        .ename          = gettext_noop("Angola")
    },
    {   .code2          = "AI",
        .code3          = "AIA",
        .num            = 660,
        .ename          = gettext_noop("Anguilla")
    },
    {   .code2          = "AQ",
        .code3          = "ATA",
        .num            = 10,
        .ename          = gettext_noop("Antarctica")
    },
    {   .code2          = "AG",
        .code3          = "ATG",
        .num            = 28,
        .ename          = gettext_noop("Antigua and Barbuda")
    },
    {   .code2          = "AR",
        .code3          = "ARG",
        .num            = 32,
        .ename          = gettext_noop("Argentina")
    },
    {   .code2          = "AM",
        .code3          = "ARM",
        .num            = 51,
        .ename          = gettext_noop("Armenia")
    },
    {   .code2          = "AW",
        .code3          = "ABW",
        .num            = 533,
        .ename          = gettext_noop("Aruba")
    },
    {   .code2          = "AU",
        .code3          = "AUS",
        .num            = 36,
        .ename          = gettext_noop("Australia"),
        .efull          = gettext_noop("Commonwealth of Australia")
    },
    {   .code2          = "AT",
        .code3          = "AUT",
        .num            = 40,
        .ename          = gettext_noop("Austria")
    },
    {   .code2          = "AZ",
        .code3          = "AZE",
        .num            = 31,
        .ename          = gettext_noop("Azerbaijan")
    },
    {   .code2          = "BS",
        .code3          = "BHS",
        .num            = 44,
        .ename          = gettext_noop("Bahamas")
    },
    {   .code2          = "BH",
        .code3          = "BHR",
        .num            = 48,
        .ename          = gettext_noop("Bahrain")
    },
    {   .code2          = "BD",
        .code3          = "BGD",
        .num            = 50,
        .ename          = gettext_noop("Bangladesh")
    },
    {   .code2          = "BB",
        .code3          = "BRB",
        .num            = 52,
        .ename          = gettext_noop("Barbados")
    },
    {   .code2          = "BY",
        .code3          = "BLR",
        .num            = 112,
        .ename          = gettext_noop("Belarus"),
        .efull          = gettext_noop("Republic of Belarus")
    },
    {   .code2          = "BE",
        .code3          = "BEL",
        .num            = 56,
        .ename          = gettext_noop("Belgium")
    },
    {   .code2          = "BZ",
        .code3          = "BLZ",
        .num            = 84,
        .ename          = gettext_noop("Belize")
    },
    {   .code2          = "BJ",
        .code3          = "BEN",
        .num            = 204,
        .ename          = gettext_noop("Benin")
    },
    {   .code2          = "BM",
        .code3          = "BMU",
        .num            = 60,
        .ename          = gettext_noop("Bermuda")
    },
    {   .code2          = "BT",
        .code3          = "BTN",
        .num            = 64,
        .ename          = gettext_noop("Bhutan")
    },
    {   .code2          = "BO",
        .code3          = "BOL",
        .num            = 68,
        .ename          = gettext_noop("Bolivia"),
        .efull          = gettext_noop("Bolivia (Plurinational State of)")
    },
    {   .code2          = "BQ",
        .code3          = "BES",
        .num            = 535,
        .ename          = gettext_noop("Bonaire, Sint Eustatius and Saba")
    },
    {   .code2          = "BA",
        .code3          = "BIH",
        .num            = 70,
        .ename          = gettext_noop("Bosnia and Herzegovina")
    },
    {   .code2          = "BW",
        .code3          = "BWA",
        .num            = 72,
        .ename          = gettext_noop("Botswana")
    },
    {   .code2          = "BV",
        .code3          = "BVT",
        .num            = 74,
        .ename          = gettext_noop("Bouvet Island")
    },
    {   .code2          = "BR",
        .code3          = "BRA",
        .num            = 76,
        .ename          = gettext_noop("Brazil")
    },
    {   .code2          = "IO",
        .code3          = "IOT",
        .num            = 86,
        .ename          = gettext_noop("British Indian Ocean Territory")
    },
    {   .code2          = "BN",
        .code3          = "BRN",
        .num            = 96,
        .ename          = gettext_noop("Brunei"),
        .efull          = gettext_noop("Brunei Darussalam")
    },
    {   .code2          = "BG",
        .code3          = "BGR",
        .num            = 100,
        .ename          = gettext_noop("Bulgaria")
    },
    {   .code2          = "BF",
        .code3          = "BFA",
        .num            = 854,
        .ename          = gettext_noop("Burkina Faso")
    },
    {   .code2          = "BI",
        .code3          = "BDI",
        .num            = 108,
        .ename          = gettext_noop("Burundi")
    },
    {   .code2          = "CV",
        .code3          = "CPV",
        .num            = 132,
        .ename          = gettext_noop("Cabo Verde")
    },
    {   .code2          = "KH",
        .code3          = "KHM",
        .num            = 116,
        .ename          = gettext_noop("Cambodia")
    },
    {   .code2          = "CM",
        .code3          = "CMR",
        .num            = 120,
        .ename          = gettext_noop("Cameroon")
    },
    {   .code2          = "CA",
        .code3          = "CAN",
        .num            = 124,
        .ename          = gettext_noop("Canada")
    },
    {   .code2          = "KY",
        .code3          = "CYM",
        .num            = 136,
        .ename          = gettext_noop("Cayman Islands")
    },
    {   .code2          = "CF",
        .code3          = "CAF",
        .num            = 140,
        .ename          = gettext_noop("Central African Republic")
    },
    {   .code2          = "TD",
        .code3          = "TCD",
        .num            = 148,
        .ename          = gettext_noop("Chad")
    },
    {   .code2          = "CL",
        .code3          = "CHL",
        .num            = 152,
        .ename          = gettext_noop("Chile")
    },
    {   .code2          = "CN",
        .code3          = "CHN",
        .num            = 156,
        .ename          = gettext_noop("China"),
        .efull          = gettext_noop("People Republic of China"),
        .eabbrev        = gettext_noop("PRC")
    },
    {   .code2          = "CX",
        .code3          = "CXR",
        .num            = 162,
        .ename          = gettext_noop("Christmas Island")
    },
    {   .code2          = "CC",
        .code3          = "CCK",
        .num            = 166,
        .ename          = gettext_noop("Cocos Islands"),
        .efull          = gettext_noop("Cocos (Keeling) Islands")
    },
    {   .code2          = "CO",
        .code3          = "COL",
        .num            = 170,
        .ename          = gettext_noop("Colombia")
    },
    {   .code2          = "KM",
        .code3          = "COM",
        .num            = 174,
        .ename          = gettext_noop("Comoros")
    },
    {   .code2          = "CG",
        .code3          = "COG",
        .num            = 178,
        .ename          = gettext_noop("Congo")
    },
    {   .code2          = "CD",
        .code3          = "COD",
        .num            = 180,
        .ename          = gettext_noop("Congo (Democratic Republic of the)")
    },
    {   .code2          = "CK",
        .code3          = "COK",
        .num            = 184,
        .ename          = gettext_noop("Cook Islands")
    },
    {   .code2          = "CR",
        .code3          = "CRI",
        .num            = 188,
        .ename          = gettext_noop("Costa Rica")
    },
    {   .code2          = "CI",
        .code3          = "CIV",
        .num            = 384,
        .ename          = gettext_noop("Côte d'Ivoire")
    },
    {   .code2          = "HR",
        .code3          = "HRV",
        .num            = 191,
        .ename          = gettext_noop("Croatia")
    },
    {   .code2          = "CU",
        .code3          = "CUB",
        .num            = 192,
        .ename          = gettext_noop("Cuba")
    },
    {   .code2          = "CW",
        .code3          = "CUW",
        .num            = 531,
        .ename          = gettext_noop("Curaçao")
    },
    {   .code2          = "CY",
        .code3          = "CYP",
        .num            = 196,
        .ename          = gettext_noop("Cyprus")
    },
    {   .code2          = "CZ",
        .code3          = "CZE",
        .num            = 203,
        .ename          = gettext_noop("Czechia")
    },
    {   .code2          = "DK",
        .code3          = "DNK",
        .num            = 208,
        .ename          = gettext_noop("Denmark")
    },
    {   .code2          = "DJ",
        .code3          = "DJI",
        .num            = 262,
        .ename          = gettext_noop("Djibouti")
    },
    {   .code2          = "DM",
        .code3          = "DMA",
        .num            = 212,
        .ename          = gettext_noop("Dominica")
    },
    {   .code2          = "DO",
        .code3          = "DOM",
        .num            = 214,
        .ename          = gettext_noop("Dominican Republic")
    },
    {   .code2          = "EC",
        .code3          = "ECU",
        .num            = 218,
        .ename          = gettext_noop("Ecuador")
    },
    {   .code2          = "EG",
        .code3          = "EGY",
        .num            = 818,
        .ename          = gettext_noop("Egypt"),
        .efull          = gettext_noop("Arab Republic of Egypt"),
        .eabbrev        = gettext_noop("ARE")
    },
    {   .code2          = "SV",
        .code3          = "SLV",
        .num            = 222,
        .ename          = gettext_noop("El Salvador")
    },
    {   .code2          = "GQ",
        .code3          = "GNQ",
        .num            = 226,
        .ename          = gettext_noop("Equatorial Guinea")
    },
    {   .code2          = "ER",
        .code3          = "ERI",
        .num            = 232,
        .ename          = gettext_noop("Eritrea")
    },
    {   .code2          = "EE",
        .code3          = "EST",
        .num            = 233,
        .ename          = gettext_noop("Estonia")
    },
    {   .code2          = "ET",
        .code3          = "ETH",
        .num            = 231,
        .ename          = gettext_noop("Ethiopia")
    },
    {   .code2          = "FK",
        .code3          = "FLK",
        .num            = 238,
        .ename          = gettext_noop("Falkland Islands (Malvinas)")
    },
    {   .code2          = "FO",
        .code3          = "FRO",
        .num            = 234,
        .ename          = gettext_noop("Faroe Islands")
    },
    {   .code2          = "FJ",
        .code3          = "FJI",
        .num            = 242,
        .ename          = gettext_noop("Fiji")
    },
    {   .code2          = "FI",
        .code3          = "FIN",
        .num            = 246,
        .ename          = gettext_noop("Finland")
    },
    {   .code2          = "FR",
        .code3          = "FRA",
        .num            = 250,
        .ename          = gettext_noop("France")
    },
    {   .code2          = "GF",
        .code3          = "GUF",
        .num            = 254,
        .ename          = gettext_noop("French Guiana")
    },
    {   .code2          = "PF",
        .code3          = "PYF",
        .num            = 258,
        .ename          = gettext_noop("French Polynesia")
    },
    {   .code2          = "TF",
        .code3          = "ATF",
        .num            = 260,
        .ename          = gettext_noop("French Southern Territories")
    },
    {   .code2          = "GA",
        .code3          = "GAB",
        .num            = 266,
        .ename          = gettext_noop("Gabon")
    },
    {   .code2          = "GM",
        .code3          = "GMB",
        .num            = 270,
        .ename          = gettext_noop("Gambia")
    },
    {   .code2          = "GE",
        .code3          = "GEO",
        .num            = 268,
        .ename          = gettext_noop("Georgia")
    },
    {   .code2          = "DE",
        .code3          = "DEU",
        .num            = 276,
        .ename          = gettext_noop("Germany")
    },
    {   .code2          = "GH",
        .code3          = "GHA",
        .num            = 288,
        .ename          = gettext_noop("Ghana")
    },
    {   .code2          = "GI",
        .code3          = "GIB",
        .num            = 292,
        .ename          = gettext_noop("Gibraltar")
    },
    {   .code2          = "GR",
        .code3          = "GRC",
        .num            = 300,
        .ename          = gettext_noop("Greece")
    },
    {   .code2          = "GL",
        .code3          = "GRL",
        .num            = 304,
        .ename          = gettext_noop("Greenland")
    },
    {   .code2          = "GD",
        .code3          = "GRD",
        .num            = 308,
        .ename          = gettext_noop("Grenada")
    },
    {   .code2          = "GP",
        .code3          = "GLP",
        .num            = 312,
        .ename          = gettext_noop("Guadeloupe")
    },
    {   .code2          = "GU",
        .code3          = "GUM",
        .num            = 316,
        .ename          = gettext_noop("Guam")
    },
    {   .code2          = "GT",
        .code3          = "GTM",
        .num            = 320,
        .ename          = gettext_noop("Guatemala")
    },
    {   .code2          = "GG",
        .code3          = "GGY",
        .num            = 831,
        .ename          = gettext_noop("Guernsey")
    },
    {   .code2          = "GN",
        .code3          = "GIN",
        .num            = 324,
        .ename          = gettext_noop("Guinea")
    },
    {   .code2          = "GW",
        .code3          = "GNB",
        .num            = 624,
        .ename          = gettext_noop("Guinea-Bissau")
    },
    {   .code2          = "GY",
        .code3          = "GUY",
        .num            = 328,
        .ename          = gettext_noop("Guyana")
    },
    {   .code2          = "HT",
        .code3          = "HTI",
        .num            = 332,
        .ename          = gettext_noop("Haiti")
    },
    {   .code2          = "HM",
        .code3          = "HMD",
        .num            = 334,
        .ename          = gettext_noop("Heard Island and McDonald Islands")
    },
    {   .code2          = "VA",
        .code3          = "VAT",
        .num            = 336,
        .ename          = gettext_noop("Holy See")
    },
    {   .code2          = "HN",
        .code3          = "HND",
        .num            = 340,
        .ename          = gettext_noop("Honduras")
    },
    {   .code2          = "HK",
        .code3          = "HKG",
        .num            = 344,
        .ename          = gettext_noop("Hong Kong")
    },
    {   .code2          = "HU",
        .code3          = "HUN",
        .num            = 348,
        .ename          = gettext_noop("Hungary")
    },
    {   .code2          = "IS",
        .code3          = "ISL",
        .num            = 352,
        .ename          = gettext_noop("Iceland")
    },
    {   .code2          = "IN",
        .code3          = "IND",
        .num            = 356,
        .ename          = gettext_noop("India"),
        .efull          = gettext_noop("Republic of India")
    },
    {   .code2          = "ID",
        .code3          = "IDN",
        .num            = 360,
        .ename          = gettext_noop("Indonesia")
    },
    {   .code2          = "IR",
        .code3          = "IRN",
        .num            = 364,
        .ename          = gettext_noop("Iran"),
        .efull          = gettext_noop("Islamic Republic of Iran"),
        .eabbrev        = gettext_noop("IRI")
    },
    {   .code2          = "IQ",
        .code3          = "IRQ",
        .num            = 368,
        .ename          = gettext_noop("Iraq")
    },
    {   .code2          = "IE",
        .code3          = "IRL",
        .num            = 372,
        .ename          = gettext_noop("Ireland")
    },
    {   .code2          = "IM",
        .code3          = "IMN",
        .num            = 833,
        .ename          = gettext_noop("Isle of Man")
    },
    {   .code2          = "IL",
        .code3          = "ISR",
        .num            = 376,
        .ename          = gettext_noop("Israel")
    },
    {   .code2          = "IT",
        .code3          = "ITA",
        .num            = 380,
        .ename          = gettext_noop("Italy")
    },
    {   .code2          = "JM",
        .code3          = "JAM",
        .num            = 388,
        .ename          = gettext_noop("Jamaica")
    },
    {   .code2          = "JP",
        .code3          = "JPN",
        .num            = 392,
        .ename          = gettext_noop("Japan")
    },
    {   .code2          = "JE",
        .code3          = "JEY",
        .num            = 832,
        .ename          = gettext_noop("Jersey")
    },
    {   .code2          = "JO",
        .code3          = "JOR",
        .num            = 400,
        .ename          = gettext_noop("Jordan")
    },
    {   .code2          = "KZ",
        .code3          = "KAZ",
        .num            = 398,
        .ename          = gettext_noop("Kazakhstan")
    },
    {   .code2          = "KE",
        .code3          = "KEN",
        .num            = 404,
        .ename          = gettext_noop("Kenya")
    },
    {   .code2          = "KI",
        .code3          = "KIR",
        .num            = 296,
        .ename          = gettext_noop("Kiribati")
    },
    {   .code2          = "KP",
        .code3          = "PRK",
        .num            = 408,
        .ename          = gettext_noop("North Korea"),
        .efull          = gettext_noop("Democratic People's Republic of Korea")
    },
    {   .code2          = "KR",
        .code3          = "KOR",
        .num            = 410,
        .ename          = gettext_noop("South Korea"),
        .efull          = gettext_noop("Republic of Korea")
    },
    {   .code2          = "KW",
        .code3          = "KWT",
        .num            = 414,
        .ename          = gettext_noop("Kuwait")
    },
    {   .code2          = "KG",
        .code3          = "KGZ",
        .num            = 417,
        .ename          = gettext_noop("Kyrgyzstan")
    },
    {   .code2          = "LA",
        .code3          = "LAO",
        .num            = 418,
        .ename          = gettext_noop("Laos"),
        .efull          = gettext_noop("Lao People's Democratic Republic")
    },
    {   .code2          = "LV",
        .code3          = "LVA",
        .num            = 428,
        .ename          = gettext_noop("Latvia")
    },
    {   .code2          = "LB",
        .code3          = "LBN",
        .num            = 422,
        .ename          = gettext_noop("Lebanon")
    },
    {   .code2          = "LS",
        .code3          = "LSO",
        .num            = 426,
        .ename          = gettext_noop("Lesotho")
    },
    {   .code2          = "LR",
        .code3          = "LBR",
        .num            = 430,
        .ename          = gettext_noop("Liberia")
    },
    {   .code2          = "LY",
        .code3          = "LBY",
        .num            = 434,
        .ename          = gettext_noop("Libya")
    },
    {   .code2          = "LI",
        .code3          = "LIE",
        .num            = 438,
        .ename          = gettext_noop("Liechtenstein")
    },
    {   .code2          = "LT",
        .code3          = "LTU",
        .num            = 440,
        .ename          = gettext_noop("Lithuania")
    },
    {   .code2          = "LU",
        .code3          = "LUX",
        .num            = 442,
        .ename          = gettext_noop("Luxembourg")
    },
    {   .code2          = "MO",
        .code3          = "MAC",
        .num            = 446,
        .ename          = gettext_noop("Macao")
    },
    {   .code2          = "MK",
        .code3          = "MKD",
        .num            = 807,
        .ename          = gettext_noop("Macedonia"),
        .efull          = gettext_noop("Macedonia (the former Yugoslav Republic of)")
    },
    {   .code2          = "MG",
        .code3          = "MDG",
        .num            = 450,
        .ename          = gettext_noop("Madagascar")
    },
    {   .code2          = "MW",
        .code3          = "MWI",
        .num            = 454,
        .ename          = gettext_noop("Malawi")
    },
    {   .code2          = "MY",
        .code3          = "MYS",
        .num            = 458,
        .ename          = gettext_noop("Malaysia")
    },
    {   .code2          = "MV",
        .code3          = "MDV",
        .num            = 462,
        .ename          = gettext_noop("Maldives")
    },
    {   .code2          = "ML",
        .code3          = "MLI",
        .num            = 466,
        .ename          = gettext_noop("Mali")
    },
    {   .code2          = "MT",
        .code3          = "MLT",
        .num            = 470,
        .ename          = gettext_noop("Malta")
    },
    {   .code2          = "MH",
        .code3          = "MHL",
        .num            = 584,
        .ename          = gettext_noop("Marshall Islands")
    },
    {   .code2          = "MQ",
        .code3          = "MTQ",
        .num            = 474,
        .ename          = gettext_noop("Martinique")
    },
    {   .code2          = "MR",
        .code3          = "MRT",
        .num            = 478,
        .ename          = gettext_noop("Mauritania")
    },
    {   .code2          = "MU",
        .code3          = "MUS",
        .num            = 480,
        .ename          = gettext_noop("Mauritius")
    },
    {   .code2          = "YT",
        .code3          = "MYT",
        .num            = 175,
        .ename          = gettext_noop("Mayotte")
    },
    {   .code2          = "MX",
        .code3          = "MEX",
        .num            = 484,
        .ename          = gettext_noop("Mexico")
    },
    {   .code2          = "FM",
        .code3          = "FSM",
        .num            = 583,
        .ename          = gettext_noop("Micronesia"),
        .efull          = gettext_noop("Federated States of Micronesia")
    },
    {   .code2          = "MD",
        .code3          = "MDA",
        .num            = 498,
        .ename          = gettext_noop("Moldova"),
        .efull          = gettext_noop("Republic of Moldova")
    },
    {   .code2          = "MC",
        .code3          = "MCO",
        .num            = 492,
        .ename          = gettext_noop("Monaco")
    },
    {   .code2          = "MN",
        .code3          = "MNG",
        .num            = 496,
        .ename          = gettext_noop("Mongolia")
    },
    {   .code2          = "ME",
        .code3          = "MNE",
        .num            = 499,
        .ename          = gettext_noop("Montenegro")
    },
    {   .code2          = "MS",
        .code3          = "MSR",
        .num            = 500,
        .ename          = gettext_noop("Montserrat")
    },
    {   .code2          = "MA",
        .code3          = "MAR",
        .num            = 504,
        .ename          = gettext_noop("Morocco")
    },
    {   .code2          = "MZ",
        .code3          = "MOZ",
        .num            = 508,
        .ename          = gettext_noop("Mozambique")
    },
    {   .code2          = "MM",
        .code3          = "MMR",
        .num            = 104,
        .ename          = gettext_noop("Myanmar")
    },
    {   .code2          = "NA",
        .code3          = "NAM",
        .num            = 516,
        .ename          = gettext_noop("Namibia")
    },
    {   .code2          = "NR",
        .code3          = "NRU",
        .num            = 520,
        .ename          = gettext_noop("Nauru")
    },
    {   .code2          = "NP",
        .code3          = "NPL",
        .num            = 524,
        .ename          = gettext_noop("Nepal")
    },
    {   .code2          = "NL",
        .code3          = "NLD",
        .num            = 528,
        .ename          = gettext_noop("Netherlands")
    },
    {   .code2          = "NC",
        .code3          = "NCL",
        .num            = 540,
        .ename          = gettext_noop("New Caledonia")
    },
    {   .code2          = "NZ",
        .code3          = "NZL",
        .num            = 554,
        .ename          = gettext_noop("New Zealand")
    },
    {   .code2          = "NI",
        .code3          = "NIC",
        .num            = 558,
        .ename          = gettext_noop("Nicaragua")
    },
    {   .code2          = "NE",
        .code3          = "NER",
        .num            = 562,
        .ename          = gettext_noop("Niger")
    },
    {   .code2          = "NG",
        .code3          = "NGA",
        .num            = 566,
        .ename          = gettext_noop("Nigeria")
    },
    {   .code2          = "NU",
        .code3          = "NIU",
        .num            = 570,
        .ename          = gettext_noop("Niue")
    },
    {   .code2          = "NF",
        .code3          = "NFK",
        .num            = 574,
        .ename          = gettext_noop("Norfolk Island")
    },
    {   .code2          = "MP",
        .code3          = "MNP",
        .num            = 580,
        .ename          = gettext_noop("Northern Mariana Islands")
    },
    {   .code2          = "NO",
        .code3          = "NOR",
        .num            = 578,
        .ename          = gettext_noop("Norway")
    },
    {   .code2          = "OM",
        .code3          = "OMN",
        .num            = 512,
        .ename          = gettext_noop("Oman")
    },
    {   .code2          = "PK",
        .code3          = "PAK",
        .num            = 586,
        .ename          = gettext_noop("Pakistan"),
        .efull          = gettext_noop("Islamic Republic of Pakistan"),
        .eabbrev        = gettext_noop("IRP")
    },
    {   .code2          = "PW",
        .code3          = "PLW",
        .num            = 585,
        .ename          = gettext_noop("Palau")
    },
    {   .code2          = "PS",
        .code3          = "PSE",
        .num            = 275,
        .ename          = gettext_noop("Palestine"),
        .efull          = gettext_noop("State of Palestine")
    },
    {   .code2          = "PA",
        .code3          = "PAN",
        .num            = 591,
        .ename          = gettext_noop("Panama")
    },
    {   .code2          = "PG",
        .code3          = "PNG",
        .num            = 598,
        .ename          = gettext_noop("Papua New Guinea")
    },
    {   .code2          = "PY",
        .code3          = "PRY",
        .num            = 600,
        .ename          = gettext_noop("Paraguay")
    },
    {   .code2          = "PE",
        .code3          = "PER",
        .num            = 604,
        .ename          = gettext_noop("Peru")
    },
    {   .code2          = "PH",
        .code3          = "PHL",
        .num            = 608,
        .ename          = gettext_noop("Philippines")
    },
    {   .code2          = "PN",
        .code3          = "PCN",
        .num            = 612,
        .ename          = gettext_noop("Pitcairn")
    },
    {   .code2          = "PL",
        .code3          = "POL",
        .num            = 616,
        .ename          = gettext_noop("Poland")
    },
    {   .code2          = "PT",
        .code3          = "PRT",
        .num            = 620,
        .ename          = gettext_noop("Portugal")
    },
    {   .code2          = "PR",
        .code3          = "PRI",
        .num            = 630,
        .ename          = gettext_noop("Puerto Rico")
    },
    {   .code2          = "QA",
        .code3          = "QAT",
        .num            = 634,
        .ename          = gettext_noop("Qatar")
    },
    {   .code2          = "RE",
        .code3          = "REU",
        .num            = 638,
        .ename          = gettext_noop("Réunion")
    },
    {   .code2          = "RO",
        .code3          = "ROU",
        .num            = 642,
        .ename          = gettext_noop("Romania")
    },
    {   .code2          = "RU",
        .code3          = "RUS",
        .num            = 643,
        .ename          = gettext_noop("Russia"),
        .efull          = gettext_noop("Russian Federation"),
        .eabbrev        = gettext_noop("RF")
    },
    {   .code2          = "RW",
        .code3          = "RWA",
        .num            = 646,
        .ename          = gettext_noop("Rwanda")
    },
    {   .code2          = "BL",
        .code3          = "BLM",
        .num            = 652,
        .ename          = gettext_noop("Saint Barthélemy")
    },
    {   .code2          = "SH",
        .code3          = "SHN",
        .num            = 654,
        .ename          = gettext_noop("Saint Helena, Ascension and Tristan da Cunha")
    },
    {   .code2          = "KN",
        .code3          = "KNA",
        .num            = 659,
        .ename          = gettext_noop("Saint Kitts and Nevis")
    },
    {   .code2          = "LC",
        .code3          = "LCA",
        .num            = 662,
        .ename          = gettext_noop("Saint Lucia")
    },
    {   .code2          = "MF",
        .code3          = "MAF",
        .num            = 663,
        .ename          = gettext_noop("Saint Martin (French part)")
    },
    {   .code2          = "PM",
        .code3          = "SPM",
        .num            = 666,
        .ename          = gettext_noop("Saint Pierre and Miquelon")
    },
    {   .code2          = "VC",
        .code3          = "VCT",
        .num            = 670,
        .ename          = gettext_noop("Saint Vincent and the Grenadines")
    },
    {   .code2          = "WS",
        .code3          = "WSM",
        .num            = 882,
        .ename          = gettext_noop("Samoa")
    },
    {   .code2          = "SM",
        .code3          = "SMR",
        .num            = 674,
        .ename          = gettext_noop("San Marino")
    },
    {   .code2          = "ST",
        .code3          = "STP",
        .num            = 678,
        .ename          = gettext_noop("Sao Tome and Principe")
    },
    {   .code2          = "SA",
        .code3          = "SAU",
        .num            = 682,
        .ename          = gettext_noop("Saudi Arabia")
    },
    {   .code2          = "SN",
        .code3          = "SEN",
        .num            = 686,
        .ename          = gettext_noop("Senegal")
    },
    {   .code2          = "RS",
        .code3          = "SRB",
        .num            = 688,
        .ename          = gettext_noop("Serbia")
    },
    {   .code2          = "SC",
        .code3          = "SYC",
        .num            = 690,
        .ename          = gettext_noop("Seychelles")
    },
    {   .code2          = "SL",
        .code3          = "SLE",
        .num            = 694,
        .ename          = gettext_noop("Sierra Leone")
    },
    {   .code2          = "SG",
        .code3          = "SGP",
        .num            = 702,
        .ename          = gettext_noop("Singapore")
    },
    {   .code2          = "SX",
        .code3          = "SXM",
        .num            = 534,
        .ename          = gettext_noop("Sint Maarten (Dutch part)")
    },
    {   .code2          = "SK",
        .code3          = "SVK",
        .num            = 703,
        .ename          = gettext_noop("Slovakia")
    },
    {   .code2          = "SI",
        .code3          = "SVN",
        .num            = 705,
        .ename          = gettext_noop("Slovenia")
    },
    {   .code2          = "SB",
        .code3          = "SLB",
        .num            = 90,
        .ename          = gettext_noop("Solomon Islands")
    },
    {   .code2          = "SO",
        .code3          = "SOM",
        .num            = 706,
        .ename          = gettext_noop("Somalia")
    },
    {   .code2          = "ZA",
        .code3          = "ZAF",
        .num            = 710,
        .ename          = gettext_noop("South Africa"),
        .efull          = gettext_noop("Republic of South Africa"),
        .eabbrev        = gettext_noop("RSA")
    },
    {   .code2          = "GS",
        .code3          = "SGS",
        .num            = 239,
        .ename          = gettext_noop("South Georgia and the South Sandwich Islands")
    },
    {   .code2          = "SS",
        .code3          = "SSD",
        .num            = 728,
        .ename          = gettext_noop("South Sudan")
    },
    {   .code2          = "ES",
        .code3          = "ESP",
        .num            = 724,
        .ename          = gettext_noop("Spain")
    },
    {   .code2          = "LK",
        .code3          = "LKA",
        .num            = 144,
        .ename          = gettext_noop("Sri Lanka")
    },
    {   .code2          = "SD",
        .code3          = "SDN",
        .num            = 729,
        .ename          = gettext_noop("Sudan")
    },
    {   .code2          = "SR",
        .code3          = "SUR",
        .num            = 740,
        .ename          = gettext_noop("Suriname")
    },
    {   .code2          = "SJ",
        .code3          = "SJM",
        .num            = 744,
        .ename          = gettext_noop("Svalbard and Jan Mayen")
    },
    {   .code2          = "SZ",
        .code3          = "SWZ",
        .num            = 748,
        .ename          = gettext_noop("Swaziland")
    },
    {   .code2          = "SE",
        .code3          = "SWE",
        .num            = 752,
        .ename          = gettext_noop("Sweden")
    },
    {   .code2          = "CH",
        .code3          = "CHE",
        .num            = 756,
        .ename          = gettext_noop("Switzerland")
    },
    {   .code2          = "SY",
        .code3          = "SYR",
        .num            = 760,
        .ename          = gettext_noop("Syria"),
        .efull          = gettext_noop("Syrian Arab Republic")
    },
    {   .code2          = "TW",
        .code3          = "TWN",
        .num            = 158,
        .ename          = gettext_noop("Taiwan"),
        .efull          = gettext_noop("Taiwan, Province of China")
    },
    {   .code2          = "TJ",
        .code3          = "TJK",
        .num            = 762,
        .ename          = gettext_noop("Tajikistan")
    },
    {   .code2          = "TZ",
        .code3          = "TZA",
        .num            = 834,
        .ename          = gettext_noop("Tanzania"),
        .efull          = gettext_noop("United Republic of Tanzania")
    },
    {   .code2          = "TH",
        .code3          = "THA",
        .num            = 764,
        .ename          = gettext_noop("Thailand")
    },
    {   .code2          = "TL",
        .code3          = "TLS",
        .num            = 626,
        .ename          = gettext_noop("Timor-Leste")
    },
    {   .code2          = "TG",
        .code3          = "TGO",
        .num            = 768,
        .ename          = gettext_noop("Togo")
    },
    {   .code2          = "TK",
        .code3          = "TKL",
        .num            = 772,
        .ename          = gettext_noop("Tokelau")
    },
    {   .code2          = "TO",
        .code3          = "TON",
        .num            = 776,
        .ename          = gettext_noop("Tonga")
    },
    {   .code2          = "TT",
        .code3          = "TTO",
        .num            = 780,
        .ename          = gettext_noop("Trinidad and Tobago")
    },
    {   .code2          = "TN",
        .code3          = "TUN",
        .num            = 788,
        .ename          = gettext_noop("Tunisia")
    },
    {   .code2          = "TR",
        .code3          = "TUR",
        .num            = 792,
        .ename          = gettext_noop("Turkey")
    },
    {   .code2          = "TM",
        .code3          = "TKM",
        .num            = 795,
        .ename          = gettext_noop("Turkmenistan")
    },
    {   .code2          = "TC",
        .code3          = "TCA",
        .num            = 796,
        .ename          = gettext_noop("Turks and Caicos Islands")
    },
    {   .code2          = "TV",
        .code3          = "TUV",
        .num            = 798,
        .ename          = gettext_noop("Tuvalu")
    },
    {   .code2          = "UG",
        .code3          = "UGA",
        .num            = 800,
        .ename          = gettext_noop("Uganda")
    },
    {   .code2          = "UA",
        .code3          = "UKR",
        .num            = 804,
        .ename          = gettext_noop("Ukraine")
    },
    {   .code2          = "AE",
        .code3          = "ARE",
        .num            = 784,
        .ename          = gettext_noop("United Arab Emirates"),
        .eabbrev        = gettext_noop("UAE")
    },
    {   .code2          = "GB",
        .code3          = "GBR",
        .num            = 826,
        .ename          = gettext_noop("United Kingdom"),
        .efull          = gettext_noop("United Kingdom of Great Britain and Northern Ireland"),
        .eabbrev        = gettext_noop("UK")
    },
    {   .code2          = "US",
        .code3          = "USA",
        .num            = 840,
        .ename          = gettext_noop("United States"),
        .efull          = gettext_noop("United States of America"),
        .eabbrev        = gettext_noop("USA")
    },
    {   .code2          = "UM",
        .code3          = "UMI",
        .num            = 581,
        .ename          = gettext_noop("United States Minor Outlying Islands")
    },
    {   .code2          = "UY",
        .code3          = "URY",
        .num            = 858,
        .ename          = gettext_noop("Uruguay")
    },
    {   .code2          = "UZ",
        .code3          = "UZB",
        .num            = 860,
        .ename          = gettext_noop("Uzbekistan")
    },
    {   .code2          = "VU",
        .code3          = "VUT",
        .num            = 548,
        .ename          = gettext_noop("Vanuatu")
    },
    {   .code2          = "VE",
        .code3          = "VEN",
        .num            = 862,
        .ename          = gettext_noop("Venezuela"),
        .efull          = gettext_noop("Bolivarian Republic of Venezuela")
    },
    {   .code2          = "VN",
        .code3          = "VNM",
        .num            = 704,
        .ename          = gettext_noop("Viet Nam")
    },
    {   .code2          = "VG",
        .code3          = "VGB",
        .num            = 92,
        .ename          = gettext_noop("British Virgin Islands")
    },
    {   .code2          = "VI",
        .code3          = "VIR",
        .num            = 850,
        .ename          = gettext_noop("Virgin Islands (U.S.)")
    },
    {   .code2          = "WF",
        .code3          = "WLF",
        .num            = 876,
        .ename          = gettext_noop("Wallis and Futuna")
    },
    {   .code2          = "EH",
        .code3          = "ESH",
        .num            = 732,
        .ename          = gettext_noop("Western Sahara")
    },
    {   .code2          = "YE",
        .code3          = "YEM",
        .num            = 887,
        .ename          = gettext_noop("Yemen")
    },
    {   .code2          = "ZM",
        .code3          = "ZMB",
        .num            = 894,
        .ename          = gettext_noop("Zambia")
    },
    {   .code2          = "ZW",
        .code3          = "ZWE",
        .num            = 716,
        .ename          = gettext_noop("Zimbabwe")
    }
};

tau::Territory_data * find_data(std::string_view code) {
    std::string ucode(code);
    for (std::size_t i = 0; i < ucode.size(); ++i) { ucode[i] = std::toupper(ucode[i]); }

    for (auto * p = data_; p < data_+std::size(data_); ++p) {
        if (ucode == p->code2 || ucode == p->code3) {
            return p;
        }
    }

    return data_;
}

tau::Territory_data * find_data(int numeric) {
    for (auto * p = data_; p < data_+std::size(data_); ++p) {
        if (numeric == p->num) {
            return p;
        }
    }

    return data_;
}

}  // anonymous namespace

// ----------------------------------------------------------------------------
// ----------------------------------------------------------------------------

namespace tau {

Territory::Territory():
    data(data_)
{
    if (locale_ptr_) {
        data = locale_ptr_->terr.data;
    }
}

Territory::Territory(std::string_view iso_code):
    data(find_data(iso_code))
{
}

Territory::Territory(int numeric):
    data(find_data(numeric))
{
}

Territory::Territory(const Territory & other):
    data(other.data)
{
}

Territory & Territory::operator=(const Territory & other) {
    if (this != &other) { data = other.data; }
    return *this;
}

Territory::~Territory() {}

// static
Territory Territory::system() {
    return sys_locale_ptr_ ?  sys_locale_ptr_->terr : Territory();
}

bool Territory::operator==(const Territory & other) const {
    return data == other.data;
}

bool Territory::operator!=(const Territory & other) const {
    return data != other.data;
}

ustring Territory::full() const {
    return lgettext(data->efull && '\0' != *data->efull ? data->efull : ename());
}

ustring Territory::name() const {
    return data->ename ? lgettext(data->ename) : "";
}

ustring Territory::abbrev() const {
    return lgettext(data->eabbrev && '\0' != *data->eabbrev ? data->eabbrev : ename());
}

std::string Territory::efull() const {
    return data->efull && '\0' != *data->efull ? data->efull : ename();
}

std::string Territory::ename() const {
    return data->ename ? data->ename : "";
}

std::string Territory::eabbrev() const {
    return data->eabbrev && '\0' != *data->eabbrev ? data->eabbrev : ename();
}

std::string Territory::code() const {
    return data->code2 && '\0' != *data->code2 ? data->code2 : (data->code3 && '\0' != *data->code3  ? data->code3 : "");
}

std::string Territory::code3() const {
    return data->code3 && '\0' != *data->code3 ? data->code3 : (data->code2 && '\0' != *data->code2  ? data->code2 : "");
}

int Territory::numeric_code() const {
    return data->num;
}

// static
std::vector<std::string> Territory::iso_codes() {
    std::vector<std::string> v(std::size(data_));
    unsigned i = 0;
    for (auto * p = data_; p < data_+std::size(data_); ++p) { v[i++] = '\0' !=  *p->code2 ? p->code2 : p->code3; }
    return v;
}

// static
std::vector<int> Territory::numeric_codes() {
    std::vector<int> v(std::size(data_));
    unsigned i = 0;
    for (auto * p = data_; p < data_+std::size(data_); ++p) { v[i++] = p->num; }
    return v;
}

} // namespace tau

//END

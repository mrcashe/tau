// ----------------------------------------------------------------------------
// SPDX-License-Identifier: BSD-2-Clause
// Copyright © 2014-2024 Konstantin Shmelkov <mrcashe@gmail.com>.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
// THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
// PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
// CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
// EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
// PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
// OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
// WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
// OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
// EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// ----------------------------------------------------------------------------

#include <tau/display.hh>
#include <display-impl.hh>
#include <loop-impl.hh>

namespace tau {

Display::Display():
    impl(Display_impl::this_ptr())
{
}

Display::Display(Display_ptr dp):
    impl(dp)
{
}

Display_ptr Display::ptr() {
    return impl;
}

Display_cptr Display::ptr() const {
    return impl;
}

Loop Display::loop() {
    return impl->loop();
}

const Loop Display::loop() const {
    return impl->loop();
}

int Display::id() const noexcept {
    return impl->id();
}

Size Display::size_px() const noexcept {
    return impl->size_px();
}

Size Display::size_mm() const noexcept {
    return impl->size_mm();
}

int Display::dpi() const noexcept {
    return impl->dpi();
}

int Display::depth() const noexcept {
    return impl->depth();
}

bool Display::can_paste_text() const noexcept {
    return impl->can_paste_text();
}

void Display::paste_text() {
    impl->paste_text();
}

void Display::copy_text(const ustring & str) {
    impl->copy_text(str);
}

void Display::allow_screensaver() {
    impl->allow_screensaver();
}

void Display::disallow_screensaver() {
    impl->disallow_screensaver();
}

bool Display::screensaver_allowed() const noexcept {
    return impl->screensaver_allowed();
}

Widget_ptr Display::focus_endpoint() noexcept {
    return impl->focus_endpoint();
}

Widget_cptr Display::focus_endpoint() const noexcept {
    return impl->focus_endpoint();
}

Widget_ptr Display::lookup(const ustring & ep) {
    auto op = impl->lookup(ep);
    return op ? impl->winptr(static_cast<Widget_impl *>(op)) : nullptr;
}

Widget_cptr Display::lookup(const ustring & ep) const {
    auto op = impl->lookup(ep);
    return op ? impl->winptr(static_cast<const Widget_impl *>(op)) : nullptr;
}

signal<void()> & Display::signal_clipboard_changed() {
    return impl->signal_clipboard_changed();
}

// static
Display Display::open(const ustring & args) {
    return Display(Display_impl::open(args));
}

} // namespace tau

//END

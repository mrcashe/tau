// ----------------------------------------------------------------------------
// SPDX-License-Identifier: BSD-2-Clause
// Copyright © 2014-2024 Konstantin Shmelkov <mrcashe@gmail.com>.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
// THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
// PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
// CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
// EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
// PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
// OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
// WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
// OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
// EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// ----------------------------------------------------------------------------

/// @file header.hh Header class.

#ifndef __TAU_HEADER_IMPL_HH__
#define __TAU_HEADER_IMPL_HH__

#include <tau/separator.hh>
#include <tau/timer.hh>
#include <bin-impl.hh>

namespace tau {

class Header_impl: public Bin_impl {
public:

    Header_impl(List_ptr list, bool resize_allowed=true);
    Header_impl(Separator::Style sep_style, List_ptr list, bool resize_allowed=true);

    void show_column(int column);
    void show_column(int column, const ustring & title, Align align=Align::START);
    void show_column(int column, Widget_ptr title);
    void hide_column(int column);

    void show_sort_marker(int column, bool descend=false);
    void hide_sort_marker();

    void allow_resize();
    void disallow_resize();
    bool resize_allowed() const noexcept { return resize_allowed_; }

    void set_separator_style(Separator::Style sep_style);
    Separator::Style separator_style() const noexcept { return sep_style_; }

    signal<void(int)> & signal_click() { return signal_click_; }
    signal<void(int)> & signal_width_changed() { return signal_width_changed_; }

private:

    // Header.
    struct Header {
        int             column_;                // Column where header is bound to.
        Box_ptr         box_;                   // Header widget.
        Widget_ptr      title_;                 // Title widget.
        Widget_ptr      marker_;                // Sort marker.
        Separator_ptr   sep_;                   // Separator after header.
        bool            fix_        { false };  // Fix mouse button press.
        int             lx_;                    // Last x mouse position during separator dragging.
        int             cw_;                    // Column width before separator dragging.
        unsigned        min_;                   // Minimal column with before header was shown.
        connection      down_cx_    { true };   // Mouse down connection.
        connection      up_cx_      { true };   // Mouse up connection.
        connection      motion_cx_  { true };   // Mouse motion connection.
    };

    using Headers = std::list<Header>;

    Headers             headers_;
    Scroller_ptr        scroller_;
    Absolute_ptr        abs_;
    List_ptr            list_;
    Separator::Style    sep_style_          = Separator::HANDLE;
    bool                resize_allowed_;
    Timer               tmr_                { fun(this, &Header_impl::arrange_list) };

    signal<void(int)>   signal_click_;
    signal<void(int)>   signal_width_changed_;

private:

    void init(List_ptr list);
    void arrange_list();
    void sync_scrollers(Hints);

    void on_column_bounds_changed(int);
    void on_requisition(Hints, int column);
    bool on_sep_mouse_up(int mbt, int mm, const Point & pt, Header & hdr);
    bool on_sep_mouse_down(int mbt, int mm, const Point & pt, Header & hdr);
    void on_sep_mouse_motion(int mm, const Point & pt, Header & hdr);
    bool on_header_mouse_down(int mbt, int mm, const Point & pt, int column);
};

} // namespace tau

#endif // __TAU_HEADER_IMPL_HH__

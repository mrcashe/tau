// ----------------------------------------------------------------------------
// SPDX-License-Identifier: BSD-2-Clause
// Copyright © 2014-2024 Konstantin Shmelkov <mrcashe@gmail.com>.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
// THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
// PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
// CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
// EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
// PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
// OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
// WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
// OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
// EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// ----------------------------------------------------------------------------

#include <tau/painter.hh>
#include <layered-container.hh>
#include <algorithm>
#include <iostream>

namespace tau {

Layered_container::Layered_container() {
    signal_display_out_.connect(fun(this, &Layered_container::on_display_out));
    signal_child_.connect(fun(this, &Layered_container::on_child));
}

// Overrides Widget_impl.
void Layered_container::handle_paint(Painter pr, const Rect & inval) {
    Widget_impl::handle_paint(pr, inval);
    for (auto & p: viewables_) { paint_children(pr, inval, p.second.data(), p.second.size(), &Widget_impl::handle_paint); }
}

// Overrides Widget_impl.
void Layered_container::handle_backpaint(Painter pr, const Rect & inval) {
    Widget_impl::handle_backpaint(pr, inval);
    for (auto & p: viewables_) { paint_children(pr, inval, p.second.data(), p.second.size(), &Widget_impl::handle_backpaint); }
}

void Layered_container::on_child_viewable(Widget_impl * wp, bool yes) {
    if (!in_shutdown()) {
        auto i = layers_.find(wp);
        int level = i != layers_.end() ? i->second : 0;

        if (yes) {
            viewables_[level].push_back(wp);
        }

        else {
            auto j = std::find(begin(viewables_[level]), end(viewables_[level]), wp);
            if (j != viewables_[level].end()) { viewables_[level].erase(j); }
            if (viewables_[level].empty()) { viewables_.erase(level); }
        }

        update_mouse_owner();
    }
}

// Overrides pure Container_impl.
void Layered_container::rise(Widget_impl * wp) {
    auto i = layers_.find(wp);

    if (i != layers_.end()) {
        on_child_viewable(wp, false);
        i->second++;
        if (wp->viewable_area()) { on_child_viewable(wp, true); wp->invalidate(); }
    }
}

// Overrides pure Container_impl.
void Layered_container::lower(Widget_impl * wp) {
    auto i = layers_.find(wp);

    if (i != layers_.end()) {
        on_child_viewable(wp, false);
        if (0 != i->second) { i->second--; }
        if (wp->viewable_area()) { on_child_viewable(wp, true); wp->invalidate(); }
    }
}

// Overrides pure Container_impl.
int Layered_container::level(const Widget_impl * wp) const noexcept {
    int lv = 0;
    auto i = layers_.find(const_cast<Widget_impl *>(wp));
    if (i != layers_.end()) { lv = i->second; }
    return container_ ? container_->level(this)+lv : lv;
}

// Overrides pure Container_impl.
Widget_impl * Layered_container::mouse_target(const Point & pt) {
    if (!in_shutdown()) {
        if (mouse_grabber_) { return mouse_grabber_; }

        for (auto i = viewables_.crbegin(); i != viewables_.crend(); ++i) {
            for (auto wp: i->second) {
                if (Rect(wp->origin(), wp->size()).contains(pt)) {
                    if (wp->enabled()) {
                        return wp;
                    }
                }
            }
        }
    }

    return nullptr;
}

void Layered_container::on_display_out() {
    viewables_.clear();
}

void Layered_container::on_child(Widget_ptr wp, int op) {
    // TODO 0.8 enum it!
    if (0 == op) { layers_.erase(wp.get()); }
    else { layers_[wp.get()] = 0; }
}

} // namespace tau

//END

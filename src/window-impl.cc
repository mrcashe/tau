// ----------------------------------------------------------------------------
// SPDX-License-Identifier: BSD-2-Clause
// Copyright © 2014-2024 Konstantin Shmelkov <mrcashe@gmail.com>.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
// THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
// PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
// CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
// EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
// PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
// OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
// WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
// OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
// EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// ----------------------------------------------------------------------------

/// @file window-impl.cc Window_impl class implementation.
/// The header is window-impl.hh.

#include <tau/brush.hh>
#include <tau/painter.hh>
#include <display-impl.hh>
#include <loop-impl.hh>
#include <theme-impl.hh>
#include <window-impl.hh>
#include <iostream>

namespace tau {

Window_impl::Window_impl() {
    signal_focus_in_.connect(fun(this, &Window_impl::on_focus_in));
    signal_focus_out_.connect(fun(this, &Window_impl::suspend_focus));
    signal_close_tooltip_.connect(fun(this, &Window_impl::on_close_tooltip));
}

void Window_impl::on_focus_in() {
    resume_focus();
    if (!empty() && !widget()->focused()) { take_focus(); }
}

void Window_impl::close() {
    arrange_timer_.stop();
    end_modal();
    ungrab_mouse();
    if (!closed_) { closed_ = true; signal_close_(); }
    hide();
}

// Overrides Widget_impl.
// Overridden by Popup_impl.
Point Window_impl::to_screen(const Point & pt) const noexcept {
    return pt+position();
}

// Overrides Widget_impl.
bool Window_impl::has_modal() const noexcept {
    return display() && display()->modal_window() == this;
}

// Overrides Container_impl.
// Overrides Widget_impl.
bool Window_impl::grab_modal_up(Widget_impl * caller) {
    if (caller == modal_child_) {
        return true;
    }

    if (grabs_modal()) {
        return caller == this;
    }

    if (!has_modal() && display() && display()->grab_modal(this)) {
        set_modal_child(caller);
        return true;
    }

    return false;
}

// Overrides Container_impl.
// Overrides Widget_impl.
bool Window_impl::end_modal_up(Widget_impl * caller) {
    if (!in_shutdown() && (this == caller || modal_child_ == caller) && display()) {
        if (auto mc = modal_child_) {
            modal_child_ = nullptr;
            mc->clear_modal();
        }

        if (display()->end_modal(this)) {
            if (focused()) { resume_focus(); }
            return true;
        }
    }

    return false;
}

// Overrides Container_impl.
// Overrides Widget_impl.
int Window_impl::grab_focus_up(Widget_impl * caller) {
    int res = -1;

    if (caller && !in_shutdown() && !has_modal()) {
        res = 1;

        if (caller != focused_child_) {
            if (!focused() && has_display()) {
                res = display()->grab_focus(this) ? 0 : -1;
            }

            if (res >= 0) {
                if (auto fc = focused_child_) { focused_child_ = nullptr; fc->clear_focus(); }
                if (this != caller) { focused_child_ = caller; }
                if (res > 0) { caller->handle_focus_in(); }
            }
        }
    }

    return res;
}

// Overrides Container_impl.
// Overrides Widget_impl.
void Window_impl::drop_focus_up(Widget_impl * caller) {
    if (auto fc = focused_child_) {
        focused_child_ = nullptr;
        if (fc == caller && fc != modal_child_) { fc->clear_focus(); }
    }
}

bool Window_impl::update_position(const Point & pt) {
    bool changed = position_.update(pt);
    if (changed) { signal_position_changed_(); }
    return changed;
}

// From window system: client area changed.
void Window_impl::handle_client_area(const Rect & r) {
    if (r != client_area_) {
        client_area_ = r;
    }
}

// Overrides Container_impl.
// Overrides Widget_impl.
bool Window_impl::grab_mouse_up(Widget_impl * caller) {
    if (!in_shutdown() && enabled()) {
        if (caller != mouse_grabber_ && display()) {
            ungrab_mouse_down();
            mouse_grabber_ = this != caller ? caller : nullptr;
            display()->grab_mouse(this);
        }

        return true;
    }

    return false;
}

// Overrides Container_impl.
// Overrides Widget_impl.
bool Window_impl::ungrab_mouse_up(Widget_impl * caller) {
    if (!in_shutdown() && caller && display()) {
        if (caller == mouse_grabber_) {
            mouse_grabber_ = nullptr;
        }

        if (this != display()->mouse_grabber()) {
            return true;
        }

        if (!mouse_grabber_) {
            display()->ungrab_mouse(this);
            return true;
        }
    }

    return false;
}

// Overrides Container_impl.
// Overrides Widget_impl.
bool Window_impl::grabs_mouse() const noexcept {
    return !in_shutdown() && !mouse_grabber_ && display() && this == display()->mouse_grabber();
}

// Overrides Widget_impl.
bool Window_impl::hover() const noexcept {
    return !in_shutdown() && display() && (this == display()->mouse_owner() || this == display()->mouse_grabber());
}

// Overrides Widget_impl.
// Overridden by Popup_impl.
Point Window_impl::where_mouse() const noexcept {
    Point pt(INT_MIN, INT_MIN);

    if (!in_shutdown() && display()) {
        Point co = client_area().top_left();
        pt.set(display()->where_mouse()-position()-co);
    }

    return pt;
}

void Window_impl::move(const Point & pt) { winface_->move(pt); }
void Window_impl::move(const Rect & r) { winface_->move(r); }
void Window_impl::resize(const Size & size) { winface_->resize(size); }

// Overrides Widget_impl.
bool Window_impl::cursor_visible() const noexcept {
    return !hidden() && size() && !cursor_hidden();
}

// Overrides Widget_impl.
void Window_impl::set_cursor_up(Cursor_ptr cursor, Widget_impl * wip) {
    if (!in_shutdown() && (!cursor_ || this == wip)) {
        winface_->set_cursor(cursor);
    }
}

// Overrides Widget_impl.
void Window_impl::unset_cursor_up(Widget_impl * wip) {
    if (!in_shutdown()) {
        if (cursor_ && this != wip) {
            winface_->set_cursor(cursor_);
        }

        else {
            winface_->unset_cursor();
        }
    }
}

// Overrides Widget_impl.
void Window_impl::show_cursor_up() {
    if (!cursor_hidden_) {
        winface_->show_cursor(true);
    }
}

// Overrides Widget_impl.
void Window_impl::hide_cursor_up() {
    winface_->show_cursor(false);
}

// Overrides Widget_impl.
void Window_impl::invalidate(const Rect & inval) {
    if (arrange_timer_.running()) { arrange_timer_.stop(); sync_arrange(); }
    if (winface_) { winface_->invalidate(inval ? inval : Rect(size())); }
}

// Overrides Widget_impl.
Painter Window_impl::painter() {
    return winface_->painter();
}

void Window_impl::flush_arrange() {
    arrange_timer_.flush();
}

void Window_impl::update() {
    flush_arrange();
    winface_->update();
}

// Overrides Container_impl.
Widget_ptr Window_impl::focus_endpoint() noexcept {
    if (container_) { return Container_impl::focus_endpoint(); }
    if (auto cp = Container_impl::focus_endpoint()) { return cp; }
    return display() ? display()->winptr(this) : nullptr;
}

// Overrides Container_impl.
Widget_cptr Window_impl::focus_endpoint() const noexcept {
    if (container_) { return Container_impl::focus_endpoint(); }
    if (auto cp = Container_impl::focus_endpoint()) { return cp; }
    return display() ? display()->winptr(this) : nullptr;
}

// Overrides Container_impl.
void Window_impl::queue_arrange_up() {
    if (!in_shutdown() && !in_arrange_) { arrange_timer_.start(ARRANGE_TIMEOUT); } // About 30 ms.
}

void Window_impl::open_tooltip(Widget_impl * owner, unsigned time_ms) {
    if (auto dp = display()) {
        if (auto lp = dp->loop()) {
            tooltip_owner_ = owner;
            lp->alarm(fun(signal_close_tooltip_), time_ms);
        }
    }
}

// Overrides Widget_impl.
bool Window_impl::on_backpaint(Painter pr, const Rect & inval) {
    pr.set_brush(conf().brush(Conf::BACKGROUND));
    pr.paint();
    return false;
}

} // namespace tau

//END

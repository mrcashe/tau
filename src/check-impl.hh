// ----------------------------------------------------------------------------
// SPDX-License-Identifier: BSD-2-Clause
// Copyright © 2014-2024 Konstantin Shmelkov <mrcashe@gmail.com>.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
// THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
// PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
// CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
// EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
// PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
// OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
// WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
// OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
// EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// ----------------------------------------------------------------------------

#ifndef __TAU_CHECK_IMPL_HH__
#define __TAU_CHECK_IMPL_HH__

#include <tau/action.hh>
#include <tau/check.hh>
#include <frame-impl.hh>
#include <iostream>

namespace tau {

class Check_impl: public Frame_impl {
public:

    using Style = Check::Style;

    Check_impl(bool checked=false);
    Check_impl(Style cs, bool checked=false);
    Check_impl(Border bs, bool checked=false);
    Check_impl(Style cs, Border bs, bool checked=false);
    ~Check_impl() { destroy(); }

    void set_check_style(Style cs);
    Style check_style() const noexcept { return check_style_; }
    void check();
    void uncheck();
    void set(bool state);
    void setup(bool state);
    void toggle();
    bool checked() const noexcept { return checked_; }

    void join(Check_ptr other);
    bool joined() const noexcept { return radio_signal_->size() > 1; }

    void allow_edit() { edit_allowed_ = true; }
    void disallow_edit() { edit_allowed_ = false; }
    bool edit_allowed() const noexcept { return edit_allowed_; }

    // Overrides Frame_impl.
    void set_border_style(Border bs) override {
        user_border_style_ = bs;
        Frame_impl::set_border_style(bs);
    }

    Border border_style() const noexcept { return user_border_style_; }
    void set_border_width(unsigned npx);
    unsigned border_width() const noexcept { return user_border_width_; }

    signal<void()> & signal_check() { return signal_check_; }
    signal<void()> & signal_uncheck() { return signal_uncheck_; }

private:

    enum { RADIO_CHECK, RADIO_SETUP, RADIO_DISABLE, RADIO_ENABLE };

    using Radio_signal = signal<void(int)>;
    using Radio_signal_ptr = std::shared_ptr<Radio_signal>;

    bool                checked_;
    Style               check_style_;
    Action              action_toggle_          { "Space Enter", fun(this, &Check_impl::toggle) };
    Action              action_cancel_          { "Escape Cancel", fun(this, &Check_impl::on_cancel) };
    Border              user_border_style_;
    unsigned            user_border_width_      { 1 };
    bool                edit_allowed_           { true };
    Radio_signal_ptr    radio_signal_;
    connection          radio_cx_;

    signal<void()>      signal_check_;
    signal<void()>      signal_uncheck_;

private:

    void init();
    void set_style();
    void draw_check(Painter pr);
    void redraw();

    void on_cancel();
    bool on_paint(Painter pr, const Rect & inval);
    bool on_mouse_down(int mbt, int mm, const Point & pt);
    void on_radio_signal(int code);
    void on_enable();
    void on_disable();
    void on_font_changed();
};

} // namespace tau

#endif // __TAU_CHECK_IMPL_HH__

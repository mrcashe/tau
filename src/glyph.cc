// ----------------------------------------------------------------------------
// Copyright © 2014-2024 Konstantin Shmelkov <mrcashe@gmail.com>.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
// THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
// PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
// CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
// EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
// PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
// OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
// WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
// OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
// EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// ----------------------------------------------------------------------------

#include <tau/glyph.hh>
#include <glyph-impl.hh>

namespace tau {

Glyph::Glyph() {}

Glyph::Glyph(Glyph_ptr gl): impl(gl) {}

Glyph::operator bool() const {
    return nullptr != impl;
}

void Glyph::reset() {
    impl.reset();
}

Glyph_ptr Glyph::ptr() {
    return impl;
}

Glyph_cptr Glyph::ptr() const {
    return impl;
}

Vector Glyph::min() const {
    return impl ? impl->min_ : Vector();
}

Vector Glyph::max() const {
    return impl ? impl->max_ : Vector();
}

Vector Glyph::bearing() const {
    return impl ? impl->bearing_ : Vector();
}

Vector Glyph::advance() const {
    return impl ? impl->advance_ : Vector();
}

Rect Glyph::bounds() const {
    return impl ? impl->bounds() : Rect();
}

std::vector<Contour> Glyph::contours() const {
    return impl ? impl->contours_ : std::vector<Contour>();
}

} // namespace tau

//END

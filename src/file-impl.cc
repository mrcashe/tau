// ----------------------------------------------------------------------------
// SPDX-License-Identifier: BSD-2-Clause
// Copyright © 2014-2024 Konstantin Shmelkov <mrcashe@gmail.com>.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
// THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
// PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
// CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
// EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
// PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
// OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
// WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
// OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
// EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// ----------------------------------------------------------------------------

#include <tau/exception.hh>
#include <tau/icon.hh>
#include <tau/string.hh>
#include <file-impl.hh>
#include <gettext-impl.hh>
#include <theme-impl.hh>
#include <filesystem>
#include <fstream>
#include <iostream>

namespace fs = std::filesystem;

namespace {

tau::ustring type_for_mime(std::string_view mime) {
    if ("application/x-executable" == mime) { return gettext_noop("Executable"); }
    return gettext_noop("Unknown");
}

} // anonymous namespace

// ----------------------------------------------------------------------------
// ----------------------------------------------------------------------------

namespace tau {

void File_impl::cp(const ustring & dest, int) {
    std::ifstream is(fs::path(std::wstring(uri_)), std::ios::binary);
    if (!is.good()) { throw sys_error(str_format("File_impl::cp(): ", uri_)); }

    std::ofstream os(fs::path(std::wstring(dest)), std::ios::binary);
    if (!os.good()) { throw sys_error(str_format("File_impl::cp(): ", dest)); }

    char buffer[4096];

    while (!is.eof()) {
        is.read(buffer, sizeof(buffer));
        std::streamsize n_bytes = is.gcount();
        os.write(buffer, n_bytes);
        if (!os.good()) { throw sys_error(str_format("File_impl::cp(): failed to write into ", dest)); }
    }

    os << std::flush;
    os.close(), is.close();
}

std::optional<ustring> File_impl::read_link() const {
    if (uri_is_file(uri_) && is_link()) {
        auto path = fs::read_symlink(std::wstring(uri_));
        ustring l = path.wstring();
        if (!path.is_absolute()) { l = path_build(path_dirname(uri_), l); }
        return l;
    }

    return std::nullopt;
}

std::string File_impl::mime() const {
    if (mime_.empty()) {
        mime_ = "application/octet-stream";
    }

    return mime_;
}

ustring File_impl::type(const Language & lang) const {
    if (type_.empty()) {
        if (is_dir()) { type_ = gettext_noop("Folder"); }
        else if (is_block()) { type_ = gettext_noop("Block Device"); }
        else if (is_char()) { type_ = gettext_noop("Character Device"); }
        else if (is_socket()) { type_ = gettext_noop("Socket"); }
        else if (is_fifo()) { type_ = gettext_noop("Channel"); }
        else { type_ = type_for_mime(mime()); }
    }

    return lang == Language() ? lgettext(type_) : type_;
}

Pixmap_cptr File_impl::icon(int icon_size) const {
    ustring iconame;

    if (is_dir()) {
        iconame = user_icon();
        if (iconame.empty()) { iconame = "folder"; }
    }

    else if (is_block()) { iconame = "drive-harddisk"; }
    else if (is_char()) { iconame = "character-device"; }
    else if (is_fifo()) { iconame = "pipe"; }
    else if (is_socket()) { iconame = "socket"; }
    else { iconame = str_replace(mime(), U'/', U'-'); }
    return Theme_impl::root()->get_icon(iconame+":application-octet-stream", icon_size);
}

} // namespace tau

//END

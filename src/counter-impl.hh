// ----------------------------------------------------------------------------
// SPDX-License-Identifier: BSD-2-Clause
// Copyright © 2014-2024 Konstantin Shmelkov <mrcashe@gmail.com>.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
// THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
// PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
// CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
// EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
// PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
// OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
// WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
// OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
// EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// ----------------------------------------------------------------------------

#ifndef __TAU_COUNTER_IMPL_HH__
#define __TAU_COUNTER_IMPL_HH__

#include <tau/action.hh>
#include <frame-impl.hh>
#include <gettext-impl.hh>

namespace tau {

class Counter_impl: public Frame_impl {
public:

    Counter_impl(double value=0.0, double max_value=0.0, double min_value=0.0);
    Counter_impl(Border bs, double value=0.0, double max_value=0.0, double min_value=0.0);

    void allow_edit();
    void disallow_edit();
    bool editable() const noexcept;

    void show_buttons();
    void hide_buttons();
    bool buttons_visible() const noexcept;

    void assign(double value);
    void assign();
    void clear();
    bool empty() const noexcept;
    double value() const noexcept { return value_; }
    Border border_style() const noexcept { return border_left_style(); }

    ustring str() const;
    std::u32string wstr() const;
    void set_min_value(double min_value);
    double minimal_value() const noexcept { return min_value_; }
    void set_max_value(double max_value);
    double max_value() const noexcept { return max_value_; }
    void set_step_value(double step_value);
    double step_value() const noexcept { return step_value_; }
    void set_precision(int prec);
    int precision() const noexcept { return precision_; }
    void set_fixed();
    void unset_fixed();
    bool fixed() const noexcept { return fixed_; }
    void set_width(int width);
    int width() const noexcept { return width_; }
    void set_fill(char c);
    char fill() const noexcept { return fill_; }
    void set_caps();
    void unset_caps();
    bool caps() const noexcept { return caps_; }
    void set_base(int base);
    int base() const noexcept { return base_; }

    void increase() { do_step(true); }
    void decrease() { do_step(false); }
    void increase_page();
    void decrease_page();

    void append(Widget_ptr wp, bool shrink=false);
    Widget_ptr append(const ustring & text, unsigned margin_left=0, unsigned margin_right=0);
    Widget_ptr append(const ustring & text, const Color & color, unsigned margin_left=0, unsigned margin_right=0);
    void prepend(Widget_ptr wp, bool shrink=false);
    Widget_ptr prepend(const ustring & text, unsigned margin_left=0, unsigned margin_right=0);
    Widget_ptr prepend(const ustring & text, const Color & color, unsigned margin_left=0, unsigned margin_right=0);

    Action & action_cancel();
    Action & action_activate();

    signal<void(double)> & signal_activate() { return signal_activate_; }
    signal<void(double)> & signal_value_changed() { return signal_value_changed_; }

private:

    Box_impl *          bbox_;          // Button box.
    Box_impl *          ibox_;          // Internal box.
    Box_impl *          box_;           // Outer box.
    Entry_impl *        entry_;

    double              value_          = 0.0;
    double              min_value_      = 0.0;
    double              max_value_      = 0.0;
    double              step_value_     = 1.0;
    int                 base_           = 10;
    int                 precision_      = 0;
    int                 width_          = 0;
    char                fill_           = ' ';
    bool                fixed_          = true;
    bool                caps_           = false;

    Action              action_activate_        { KC_ENTER, KM_NONE, fun(this, &Counter_impl::on_activate) };
    Action              action_up_              { KC_UP, KM_NONE, "", "picto-inc", lgettext("Increase Value"), fun(this, &Counter_impl::drop_entry_focus) };
    Action              action_down_            { KC_DOWN, KM_NONE, "", "picto-dec", lgettext("Decrease Value"), fun(this, &Counter_impl::drop_entry_focus) };
    Action              action_previous_page_   { KC_PAGE_UP, KM_NONE };
    Action              action_next_page_       { KC_PAGE_DOWN, KM_NONE };

    signal<void(double)> signal_activate_;
    signal<void(double)> signal_value_changed_;

private:

    void init(double value, double max_value, double min_value);
    void do_step(bool inc);
    ustring format_value(double v);
    bool update_value(double v, bool force=false);
    void update_from_entry();
    void draw_value();
    void drop_entry_focus();
    double get_value();
    void adjust_precision(double example);
    void adjust_width();

    void on_activate();
    void on_entry_activate();
    bool on_mouse_wheel(int delta, int mm, const Point & where);
    void on_focus_in();
    void on_focus_out();
    bool on_validate(const tau::ustring & s);
    bool on_approve(const tau::ustring & s);
    void on_border_changed();
    void on_conf_radius();
};

} // namespace tau

#endif // __TAU_COUNTER_IMPL_HH__

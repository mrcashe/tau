// ----------------------------------------------------------------------------
// SPDX-License-Identifier: BSD-2-Clause
// Copyright © 2014-2024 Konstantin Shmelkov <mrcashe@gmail.com>.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
// THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
// PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
// CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
// EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
// PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
// OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
// WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
// OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
// EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// ----------------------------------------------------------------------------

#include <tau/brush.hh>
#include <tau/exception.hh>
#include <tau/string.hh>
#include <defs-impl.hh>
#include <display-impl.hh>
#include <gettext-impl.hh>
#include <loop-impl.hh>
#include <menubox-impl.hh>
#include <menu-item-impl.hh>
#include <text-impl.hh>
#include <cmath>
#include <iostream>

namespace tau {

Text_impl::Text_impl() {
    init();
}

Text_impl::Text_impl(Align xalign, Align yalign):
    Label_impl(xalign, yalign)
{
    init();
}

Text_impl::Text_impl(const ustring & s, Align xalign, Align yalign):
    Label_impl(s, xalign, yalign)
{
    init();
}

Text_impl::Text_impl(const std::u32string & ws, Align xalign, Align yalign):
    Label_impl(ws, xalign, yalign)
{
    init();
}

Text_impl::Text_impl(Buffer buf, Align xalign, Align yalign):
    Label_impl(buf, xalign, yalign)
{
    init();
}

void Text_impl::init() {
    signal_size_changed_.connect(fun(this, &Text_impl::on_size_changed));
    signal_selection_changed_.connect(fun(this, &Text_impl::on_selection_changed));
    signal_show_.connect(fun(this, &Text_impl::refresh_caret));
    signal_display_in_.connect(fun(this, &Text_impl::on_display));
    signal_display_out_.connect(fun(caret_cx_, &connection::drop));
    signal_focus_in_.connect(fun(this, &Text_impl::on_focus_in));
    signal_focus_out_.connect(fun(this, &Text_impl::on_focus_out));
    take_cx_ = signal_take_focus_.connect(fun(this, &Text_impl::grab_focus));
    take_cx_.block();
    conf().signal_changed(Conf::FONT).connect(fun(this, &Text_impl::on_font_changed), true);
    conf().signal_changed(Conf::FONT).connect(fun(this, &Text_impl::refresh_caret));
    conf().signal_changed(Conf::FONT).connect(fun(this, &Text_impl::scroll_to_caret));
    conf().signal_changed(Conf::EDIT_FONT).connect(fun(this, &Text_impl::on_font_changed), true);
    conf().signal_changed(Conf::EDIT_FONT).connect(fun(this, &Text_impl::refresh_caret));
    conf().signal_changed(Conf::EDIT_FONT).connect(fun(this, &Text_impl::scroll_to_caret));

    signal_mouse_down().connect(fun(this, &Text_impl::on_mouse_down));
    signal_mouse_up().connect(fun(this, &Text_impl::on_mouse_up));
    signal_mouse_motion().connect(fun(this, &Text_impl::on_mouse_motion));
    signal_mouse_leave().connect(fun(this, &Text_impl::on_mouse_leave));
    action_copy_.disable();

    Action_base * v[] {
        &action_previous_,              &action_select_previous_,       &action_next_,
        &action_select_next_,           &action_previous_line_,         &action_select_previous_line_,
        &action_next_line_,             &action_select_next_line_,      &action_previous_word_,
        &action_select_previous_word_,  &action_next_word_,             &action_select_next_word_,
        &action_home_,                  &action_select_home_,           &action_end_,
        &action_select_to_eol_,         &action_sof_,                   &action_select_to_sof_,
        &action_eof_,                   &action_select_to_eof_,         &action_previous_page_,
        &action_select_previous_page_,  &action_next_page_,             &action_select_next_page_,
        &action_select_all_,            &action_copy_,                  &action_cancel_
    };

    for (auto p: v) { connect_action(*p); }
}

Text_impl::~Text_impl() {
    if (signal_caret_motion_) { delete signal_caret_motion_; }
    if (signal_click_) { delete signal_click_; }
}

// Overrides Label_impl.
// Overridden by Edit_impl.
void Text_impl::init_buffer() {
    Label_impl::init_buffer();
    caret_ = buffer_.cbegin();
    xhint_ = 0;
    refresh_caret();
    scroll_to_caret();
}

void Text_impl::on_size_changed() {
    refresh_caret();
    scroll_to_caret();
}

void Text_impl::on_selection_changed() {
    action_copy_.par_enable(has_selection());
}

void Text_impl::on_display() {
    if (select_allowed_) { set_cursor("text:ibeam"); }
    refresh_caret();
    scroll_to_caret();
}

bool Text_impl::on_mouse_down(int mbt, int mm, const Point & pt) {
    if (MBT_LEFT == mbt) {
        unselect();
        auto i = iter(pt);
        if (i && !caret_) { caret_ = i; }
        if (select_allowed_) { msel_ = i; move_to(i); }
        if (signal_click_) { (*signal_click_)(); return true; }
        return grab_focus();
    }

    return false;
}

bool Text_impl::on_mouse_up(int mbt, int mm, const Point & pt) {
    if (MBT_LEFT == mbt) {
        msel_.reset();
        emsel_.reset();
    }

    else if (MBT_RIGHT == mbt && has_selection()) {
        if (auto wip = window()) {
            auto menu = std::make_shared<Menubox_impl>();
            auto item = std::make_shared<Slot_menu_impl>(lgettext("Copy"), fun(action_copy_, &Action::exec), "edit-copy");
            menu->append(item);
            try { menu->popup(wip, menu); return true; }
            catch (exception & x) { std::cerr << __func__ << ": " << x.what() << std::endl; }
        }
    }

    return false;
}

void Text_impl::on_mouse_leave() {
    msel_.reset(), emsel_.reset();
}

void Text_impl::on_mouse_motion(int mm, const Point & pt) {
    if (select_allowed_ && (MM_LEFT & mm) && msel_) {
        auto i = iter(pt);

        if (msel_ != i && emsel_ != i) {
            update_selection(emsel_ ? emsel_ : msel_, i);
            move_to(i); emsel_ = i;
        }
    }
}

void Text_impl::on_focus_in() {
    if (caret_enabled_) {
        if (!caret_) { caret_ = buffer_.cbegin(); }
        show_caret(), scroll_to_caret();
    }
}

void Text_impl::on_focus_out() {
    hide_caret();
}

// Overrides Label_impl.
void Text_impl::on_buffer_replace(Buffer_citer b, Buffer_citer e, const std::u32string & replaced) {
    auto cr(e);
    Label_impl::on_buffer_replace(b, e, replaced);
    move_to(cr); hint_x();
}

// Overrides Label_impl.
void Text_impl::on_buffer_erase(Buffer_citer b, Buffer_citer e, const std::u32string & erased) {
    wipe_caret();
    unselect();
    auto cr(b);
    Label_impl::on_buffer_erase(b, e, erased);
    move_to(cr); hint_x();
}

// Overrides Label_impl.
void Text_impl::on_buffer_insert(Buffer_citer b, Buffer_citer e) {
    Label_impl::on_buffer_insert(b, e);
    if (!insert2_cx_.blocked()) { move_to(e); hint_x(); }
}

// Overrides Label_impl.
void Text_impl::on_wrap() {
    Label_impl::on_wrap();
    scroll_to_caret();
}

// Overrides Label_impl.
void Text_impl::render(Painter pr, const Rect & r) {
    bool caret_exposed = caret_exposed_;
    wipe_caret();
    render_pro(pr, r);
    if (caret_exposed) { expose_caret(); }
}

void Text_impl::hint_x() {
    xhint_ = x_at_col(caret_.row(), caret_.col());
}

void Text_impl::update_selection(Buffer_citer i, Buffer_citer j) {
    if (i && j && i != j) {
        if (!sel_ && !esel_) {
            sel_ = i;
            esel_ = j;
            if (esel_ < sel_) { std::swap(sel_, esel_); }
            update_range(sel_, esel_);
        }

        else if (i == esel_) {
            esel_ = j;
            if (esel_ < sel_) { std::swap(sel_, esel_); }
            if (esel_ == sel_) { unselect(); }
            else if (j < i) { update_range(j, i); }
            else { update_range(sel_, j); }
        }

        else if (i == sel_) {
            sel_ = j;
            if (esel_ < sel_) { std::swap(sel_, esel_); }
            if (esel_ == sel_) { unselect(); }
            else if (j < i) { update_range(j, i); }
            else { update_range(i, esel_); }
        }

        signal_selection_changed_();
    }
}

void Text_impl::update_caret() {
    rcaret_.reset();
    int x1 = 0, x2 = 0, y1 = oy_, y2 = y1;

    if (buffer_.empty()) {
        if (Align::END == xalign_) {
            x1 = vport_.width();
        }

        else if (Align::CENTER == xalign_) {
            x1 = vport_.width()/2;
        }

        if (Align::END == yalign_) {
            y1 += vport_.height()-font_height_;
            y2 = y1;
        }

        else if (Align::CENTER == yalign_) {
            y1 = (vport_.height()-font_height_)/2;
            y2 = y1+font_height_;
        }
    }

    else if (caret_.row() < rows_.size()) {
        auto & row = rows_[caret_.row()];
        x1 = x_at_col(row, caret_.col());
        y1 += row.ybase_-row.ascent_;
        y2 += row.ybase_+row.descent_;
        if (!insert_ && caret_.col() < row.cmax_) { x2 = x_at_col(row, 1+caret_.col()); }
    }

    x2 = std::max(x2, x1+wcaret_);
    if (y1 == y2) { y2 += font_height_; }
    rcaret_.update_width(1+x2-x1);
    rcaret_.update_origin(x1, y1);
    rcaret_.update_height(1+y2-y1);
    ccaret_ = conf().item(Conf::FOREGROUND).str();
}

void Text_impl::draw_caret(Painter pr) {
    if (pr && rcaret_) {
        pr.push();
        pr.clear();
        pr.rectangle(rcaret_.left(), rcaret_.top(), rcaret_.right(), rcaret_.bottom());
        pr.set_brush(ccaret_);
        pr.set_oper(Oper::NOT);
        pr.fill();
        pr.pop();
    }
}

void Text_impl::on_caret_timer() {
    if (!caret_refresh_) {
        if (caret_exposed_) { wipe_caret(); }
        else { expose_caret(); }
    }

    caret_refresh_ = false;
}

void Text_impl::refresh_caret() {
    wipe_caret();

    if (caret_visible_ && visible()) {
        if (auto dp = display()) {
            if (!xz_) {
                xz_.set(text_size("W").width(), 0);
                update_requisition();
            }

            if (auto loop = dp->loop()) {
                caret_refresh_ = true;
                update_caret();
                caret_cx_ = loop->alarm(fun(this, &Text_impl::on_caret_timer), CARET_TIMEOUT, true);
                expose_caret();
            }
        }
    }
}

void Text_impl::expose_caret() {
    if (caret_visible_ && visible()) {
        caret_exposed_ = true;
        draw_caret(rnd_painter());
    }
}

void Text_impl::wipe_caret() {
    if (caret_exposed_) {
        caret_exposed_ = false;
        draw_caret(rnd_painter());
    }
}

void Text_impl::scroll_to_caret() {
    if (caret_enabled_ && !buffer_.empty() && caret_.row() < rows_.size() && vport_) {
        Point ofs(vport_.top_left());
        auto & row = rows_[caret_.row()];
        int y1 = oy_+row.ybase_-row.ascent_;
        int y2 = oy_+row.ybase_+row.descent_;
        int x1 = x_at_col(caret_.row(), caret_.col());
        int x2 = x1+8;

        if (y1 < ofs.y()) { ofs.update_y(y1); }
        else if (y2 >= vport_.bottom()) { ofs.update_y(y2-vport_.height()); }

        if (focused()) {
            if (x1 < ofs.x()) {
                int x = x1, w = vport_.width(), d = w/8;
                if (x > d) { x -= d; }
                ofs.update_x(x < w ? 0 : x);
            }

            else if (x2 > vport_.right()) {
                int w = vport_.width(), d = w/8, x = x1+d;
                if (x >= int(text_width_)) { x = text_width_+text_size("W").width(); }
                ofs.update_x(x-vport_.width());
            }
        }

        offset(focused() && wrapped_ ? 0 : ofs.x(), ofs.y());
    }
}

void Text_impl::move_to(std::size_t row, std::size_t col) {
    std::size_t r = caret_.row(), c = caret_.col();
    caret_.move_to(row, col);
    if ((caret_.row() != r || caret_.col() != c) && !in_shutdown() && signal_caret_motion_) { (*signal_caret_motion_)(); }
    refresh_caret();
    scroll_to_caret();
}

void Text_impl::move_to(const Buffer_citer i) {
    move_to(i.row(), i.col());
}

Buffer_citer Text_impl::caret() const {
    return caret_;
}

void Text_impl::enable_caret() {
    if (!caret_enabled_) {
        caret_enabled_ = true;
        allow_focus();
        allow_select();
        take_cx_.unblock();
        refresh_caret();
    }
}

void Text_impl::disable_caret() {
    if (caret_enabled_) {
        caret_enabled_ = false;
        caret_cx_.drop();
        take_cx_.block();
        hide_caret();
        disallow_focus();
        xz_.reset();
        update_requisition();
    }
}

void Text_impl::show_caret() {
    if (!caret_visible_) {
        caret_visible_ = true;
        refresh_caret();
    }
}

void Text_impl::hide_caret() {
    if (caret_visible_) {
        if (caret_exposed_) { wipe_caret(); }
        caret_cx_.drop();
        caret_visible_ = false;
    }
}

std::size_t Text_impl::hinted_pos(std::size_t ri) {
    int x1 = 0;

    if (xhint_ > 0 && ri < rows_.size()) {
        auto & row = rows_[ri];

        for (std::size_t n = 1; n < row.cmax_; ++n) {
            int x2 = x_at_col(row, n);

            if (x2 >= xhint_) {
                std::size_t col = x2-xhint_ < xhint_-x1 ? n : n-1;
                Buffer_citer c(caret_, ri, col);
                if (char32_is_modifier(*c)) { ++c; }
                return c.col();
            }

            else {
                x1 = x2;
            }
        }

        return row.cmax_;
    }

    return x1;
}

void Text_impl::allow_select() {
    if (!select_allowed_) {
        select_allowed_ = true;
        set_cursor("text:ibeam");
    }
}

void Text_impl::disallow_select() {
    if (select_allowed_) {
        unselect();
        select_allowed_ = false;
        disable_caret();
        unset_cursor();
    }
}

void Text_impl::select(std::size_t row1, std::size_t col1, std::size_t row2, std::size_t col2) {
    select(buffer_.citer(row1, col1), buffer_.citer(row2, col2));
}

void Text_impl::select(Buffer_citer b, Buffer_citer e) {
    unselect();

    if (select_allowed_) {
        if (buffer_.cend() < b) {
            b = buffer_.cend();
        }

        if (e < b) { std::swap(b, e); }

        if (buffer_.cend() < e) {
            e = buffer_.cend();
        }

        for (; b != buffer_.cbegin(); --b) {
            if (!char32_is_modifier(*b)) { break; }
        }

        for (; e != buffer_.cend(); ++e) {
            if (!char32_is_modifier(*e)) { break; }
        }

        sel_ = b, esel_ = e;
        update_range(b, e);
        signal_selection_changed_();
    }
}

// From Action.
void Text_impl::on_select_all() {
    if (!buffer_.empty()) {
        auto b(buffer_.cbegin()), e(buffer_.cend());
        if (b == sel_ && e == esel_) { unselect(); }
        else { select(b, e); }
    }
}

// From interface.
void Text_impl::select_all() {
    if (!buffer_.empty()) {
        select(buffer_.cbegin(), buffer_.cend());
    }
}

void Text_impl::unselect() {
    if (sel_ && esel_) {
        Buffer_citer b(sel_), e(esel_);
        sel_.reset(); esel_.reset();
        update_range(b, e);
        signal_selection_changed_();
    }
}

void Text_impl::left() {
    if (caret_) {
        Buffer_citer j = caret_;

        if (0 == j.col() && 0 != j.row()) {
            j.backward_line();
            j.move_to_eol();
        }

        else {
            --j;
        }

        move_to(j);
        hint_x();
    }
}

void Text_impl::move_left() {
    unselect();
    left();
}

void Text_impl::select_left() {
    Buffer_citer i(caret_);
    left();
    update_selection(i, caret_);
}

void Text_impl::right() {
    if (caret_) {
        Buffer_citer j = caret_;

        if (char32_is_newline(*j)) {
            while (!j.eof() && caret_.row() == j.row()) { ++j; }
        }

        else {
            ++j;
        }

        move_to(j);
        hint_x();
    }
}

void Text_impl::move_right() {
    unselect();
    right();
}

void Text_impl::select_right() {
    Buffer_citer i(caret_);
    right();
    update_selection(i, caret_);
}

void Text_impl::up() {
    if (caret_) {
        if (0 != caret_.row()) {
            Buffer_citer i = caret_;
            std::size_t pos = 0 != xhint_ ? hinted_pos(i.row()-1) : i.col();
            i.move_to(i.row()-1, pos);
            move_to(i);
        }
    }
}

void Text_impl::move_up() {
    unselect();
    up();
}

void Text_impl::select_up() {
    Buffer_citer i(caret_);
    up();
    update_selection(i, caret_);
}

void Text_impl::down() {
    if (caret_) {
        std::size_t dest_row = caret_.row()+1;

        if (dest_row < rows()) {
            Buffer_citer i = caret_;
            std::size_t pos = 0 != xhint_ ? hinted_pos(dest_row) : i.col();
            i.move_to(dest_row, pos);
            move_to(i);
        }
    }
}

void Text_impl::move_down() {
    unselect();
    down();
}

void Text_impl::select_down() {
    Buffer_citer i(caret_);
    down();
    update_selection(i, caret_);
}

void Text_impl::backward_word() {
    if (caret_) {
        unselect();
        Buffer_citer i = caret_;
        i.backward_word();
        move_to(i);
        hint_x();
    }
}

void Text_impl::select_word_left() {
    if (caret_) {
        Buffer_citer i(caret_), j(caret_);
        j.backward_word();
        move_to(j);
        update_selection(i, j);
        hint_x();
    }
}

void Text_impl::forward_word() {
    if (caret_) {
        unselect();
        Buffer_citer i = caret_;
        i.forward_word();
        move_to(i);
        hint_x();
    }
}

void Text_impl::select_word_right() {
    if (caret_) {
        Buffer_citer i(caret_), j(caret_);
        j.forward_word();
        move_to(j);
        update_selection(i, j);
        hint_x();
    }
}

void Text_impl::home() {
    if (caret_ && caret_.col() > 0) {
        auto i(caret_);
        i.move_to_sol();
        i.skip_blanks();
        if (i.col() >= caret_.col()) { i.move_to_sol(); }
        move_to(i);
        hint_x();
    }
}

void Text_impl::move_home() {
    unselect();
    home();
}

void Text_impl::select_home() {
    Buffer_citer i(caret_);
    home();
    update_selection(i, caret_);
}

void Text_impl::move_to_eol() {
    if (caret_) {
        unselect();
        Buffer_citer i(caret_);
        i.move_to_eol();
        move_to(i);
        hint_x();
    }
}

void Text_impl::select_to_eol() {
    if (caret_) {
        Buffer_citer i(caret_), j(caret_);
        j.move_to_eol();
        move_to(j);
        update_selection(i, j);
        hint_x();
    }
}

void Text_impl::move_to_sof() {
    if (caret_) {
        unselect();
        Buffer_citer i(caret_);
        i.move_to(0, 0);
        move_to(i);
        hint_x();
    }
}

void Text_impl::select_to_sof() {
    if (caret_) {
        Buffer_citer i(caret_), j(caret_);
        j.move_to(0, 0);
        move_to(j);
        update_selection(i, j);
        hint_x();
    }
}

void Text_impl::move_to_eof() {
    if (caret_) {
        unselect();
        move_to(buffer_.cend());
        hint_x();
    }
}

void Text_impl::select_to_eof() {
    if (caret_) {
        Buffer_citer i(caret_);
        move_to(buffer_.cend());
        update_selection(i, caret_);
        hint_x();
    }
}

void Text_impl::page_up() {
    if (vport_ && caret_) {
        std::size_t nrows = rows_.size(), ri = caret_.row();

        if (ri < nrows && ri > 0) {
            const Row & row1 = rows_[ri];
            int yb = oy_+row1.ybase_-vport_.height(), y2 = std::max(0, yb);
            std::size_t ri2;

            for (ri2 = ri; ri2 > 0; --ri2) {
                const Row & row2 = rows_[ri2-1];
                if (oy_+row2.ybase_ <= y2) break;
            }

            if (ri2 > 0) {
                const Row & row3 = rows_[ri2];
                int top1 = oy_+row1.ybase_-row1.ascent_;
                int top3 = oy_+row3.ybase_-row3.ascent_;
                Point sp;
                if (top1 < vport_.top()) { sp.set(vport_.left(), oy_+row3.ybase_-row3.ascent_); }
                else if (top1 > vport_.bottom()) { sp.set(vport_.left(), oy_+row3.ybase_+row3.descent_-vport_.height()); }
                else if (top3 < vport_.top()) { sp.set(vport_.left(), oy_+row3.ybase_-row3.ascent_-top1+vport_.top()); }
                offset(sp);
                move_to(ri2, caret_.col());
            }

            else {
                move_to(0, 0);
            }
        }
    }
}

void Text_impl::move_page_up() {
    unselect();
    page_up();
}

void Text_impl::select_page_up() {
    Buffer_citer i(caret_);
    page_up();
    update_selection(i, caret_);
}

void Text_impl::page_down() {
    if (vport_ && caret_) {
        std::size_t nrows = rows(), ri = caret_.row();

        if (ri < nrows) {
            const Row & row1 = rows_[ri];
            int y2 = oy_+vport_.height()+row1.ybase_;
            std::size_t ri2;

            for (ri2 = ri; ri2 < nrows; ++ri2) {
                const Row & row2 = rows_[ri2+1];
                if (oy_+row2.ybase_ >= y2) break;
            }

            if (ri2 < nrows) {
                const Row & row3 = rows_[ri2];
                int top1 = oy_+row1.ybase_-row1.ascent_;
                int bottom3 = oy_+row3.ybase_+row3.descent_;
                Point sp;

                if (top1 < vport_.top()) {
                    sp.set(vport_.left(), oy_+row3.ybase_-row3.ascent_);
                }

                else if (top1 > vport_.bottom()) {
                    sp.set(vport_.left(), oy_+row3.ybase_+row3.descent_-vport_.height());
                }

                else if (bottom3 > vport_.bottom()) {
                    sp.set(vport_.left(), oy_+row3.ybase_-row3.ascent_-top1+vport_.top());
                }

                offset(sp);
                move_to(ri2, caret_.col());
            }

            else {
                Buffer_citer i(buffer_.cend());
                move_to(i.row(), i.col());
            }
        }
    }
}

void Text_impl::move_page_down() {
    unselect();
    page_down();
}

void Text_impl::select_page_down() {
    auto i(caret_);
    page_down();
    update_selection(i, caret_);
}

void Text_impl::copy() {
    if (sel_ && esel_) {
        if (auto dp = display()) {
            dp->copy_text(selection());
        }
    }
}

void Text_impl::on_font_changed() {
    if (auto pr = rnd_painter()) { wcaret_ = std::max(2.0, std::ceil(pr.text_size(U"W").x()/8)); }
    xz_.update(wcaret_, 0);
}

signal<void()> & Text_impl::signal_caret_motion() {
    if (!signal_caret_motion_) { signal_caret_motion_ = new signal<void()>; }
    return *signal_caret_motion_;
}

signal<void()> & Text_impl::signal_click() {
    if (!signal_click_) { signal_click_ = new signal<void()>; }
    return *signal_click_;
}

} // namespace tau

//END

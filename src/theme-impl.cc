// ----------------------------------------------------------------------------
// SPDX-License-Identifier: BSD-2-Clause
// Copyright © 2014-2024 Konstantin Shmelkov <mrcashe@gmail.com>.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
// THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
// PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
// CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
// EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
// PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
// OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
// WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
// OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
// EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// ----------------------------------------------------------------------------

#include <tau/exception.hh>
#include <tau/file.hh>
#include <tau/font.hh>
#include <tau/icon.hh>
#include <tau/key-file.hh>
#include <tau/locale.hh>
#include <tau/sys.hh>
#include <tau/string.hh>
#include <cursor-impl.hh>
#include <display-impl.hh>
#include <event-impl.hh>
#include <gettext-impl.hh>
#include <pixmap-impl.hh>
#include <theme-impl.hh>
#include <widget-impl.hh>
#include <popup-impl.hh>
#include <algorithm>
#include <cstring>
#include <forward_list>
#include <fstream>
#include <iostream>

namespace {

struct Cursor_theme {
    std::vector<tau::ustring>   roots;
    std::vector<tau::ustring>   inherits;
    std::vector<int>            inherited;
    tau::ustring                name;
};

struct Icon_dir {
    tau::ustring                path;
    tau::ustring                type;
    tau::ustring                context;
    bool                        scalable        = false;
    bool                        files_listed    = false;
    std::list<tau::ustring>     files;
    int                         size            = 0;
    unsigned                    scale           = 1;
    int                         threshold       = 2;
    int                         min_size        = 0;
    int                         max_size        = 0;
};

struct Icon_theme {
    std::vector<tau::ustring>   roots;
    std::vector<tau::ustring>   inherits;
    std::vector<Icon_dir>       dirs;
    std::vector<int>            inherited;
    tau::ustring                name;
    tau::ustring                name_i18n;
    tau::ustring                comment;
    tau::ustring                comment_i18n;
    tau::ustring                example;
    bool                        hidden          = false;
};

using Dirs                      = std::list<tau::ustring>;
using Cursor_map                = std::unordered_map<std::string, std::string>; // Maps Name->Path.
using Pixmap_map                = std::unordered_map<std::string, std::string>; // Maps Name->Path.
using Cursor_themes             = std::vector<Cursor_theme>;
using Icon_themes               = std::vector<Icon_theme>;
using Bound                     = std::forward_list<int>;

std::recursive_mutex            mx_;
std::atomic_int                 nicon_dirs_         { 0 };
std::atomic_int                 ifallback_          { -1 };
Dirs                            icon_dirs_;
Dirs                            pixmap_dirs_;
Dirs                            cursor_dirs_;
Cursor_themes                   cursor_themes_;
Icon_themes                     icon_themes_;
Cursor_map                      cursor_map_;
Pixmap_map                      pixmap_map_;
Bound                           bound_icon_themes_;

tau::ustring                    icon_theme_names_;
tau::ustring                    fallback_theme_name_;

const char * picto_down_xpm_ =
    "/* XPM */\n"
    "static char * picto_down_xpm[] = {\n"
    "\"8 8 8 1\",\n"
    "\"  c None\",\n"
    "\". c #2E3537\",\n"
    "\"+ c #2D3335\",\n"
    "\"@ c #2D3435\",\n"
    "\"# c #2E3337\",\n"
    "\"$ c #2F3436\",\n"
    "\"% c #2D3436\",\n"
    "\"& c #2D3336\",\n"
    "\"        \",\n"
    "\"        \",\n"
    "\".+    @#\",\n"
    "\" $%  $% \",\n"
    "\"  $$$$  \",\n"
    "\"   &%   \",\n"
    "\"        \",\n"
    "\"        \"};\n"
;

const char * picto_left_xpm_ =
    "/* XPM */\n"
    "static char * picto_left_xpm[] = {\n"
    "\"8 8 8 1\",\n"
    "\"  c None\",\n"
    "\". c #2E3337\",\n"
    "\"+ c #2E3436\",\n"
    "\"@ c #2F3436\",\n"
    "\"# c #2D3436\",\n"
    "\"$ c #2D3336\",\n"
    "\"% c #2D3335\",\n"
    "\"& c #2E3437\",\n"
    "\"     .  \",\n"
    "\"    +@  \",\n"
    "\"   @#   \",\n"
    "\"  @#    \",\n"
    "\"  $+    \",\n"
    "\"   $+   \",\n"
    "\"    @%  \",\n"
    "\"     &  \"};\n"
;

const char * picto_right_xpm_ =
    "/* XPM */\n"
    "static char * picto_right_xpm[] = {\n"
    "\"8 8 8 1\",\n"
    "\"  c None\",\n"
    "\". c #2E3337\",\n"
    "\"+ c #2F3436\",\n"
    "\"@ c #2E3436\",\n"
    "\"# c #2D3336\",\n"
    "\"$ c #2D3436\",\n"
    "\"% c #2D3335\",\n"
    "\"& c #2E3437\",\n"
    "\"  .     \",\n"
    "\"  +@    \",\n"
    "\"   #@   \",\n"
    "\"    #@  \",\n"
    "\"    +$  \",\n"
    "\"   +$   \",\n"
    "\"  %+    \",\n"
    "\"  &     \"}\n"
;

const char * picto_up_xpm_ =
    "/* XPM */\n"
    "static char * picto_up_xpm[] = {\n"
    "\"8 8 9 1\",\n"
    "\"  c None\",\n"
    "\". c #2F3436\",\n"
    "\"+ c #2E3436\",\n"
    "\"@ c #2D3436\",\n"
    "\"# c #2D3336\",\n"
    "\"$ c #2E3537\",\n"
    "\"% c #2D3335\",\n"
    "\"& c #2D3435\",\n"
    "\"* c #2E3337\",\n"
    "\"        \",\n"
    "\"        \",\n"
    "\"   .+   \",\n"
    "\"  ....  \",\n"
    "\" .@  #+ \",\n"
    "\"$%    &*\",\n"
    "\"        \",\n"
    "\"        \"};\n"
;

const char * picto_close_xpm_ =
    "/* XPM */\n"
    "static char * picto_close_xpm[] = {\n"
    "\"12 12 10 1\",\n"
    "\"  c None\",\n"
    "\". c #2E3436\",\n"
    "\"+ c #2D3537\",\n"
    "\"@ c #2D3436\",\n"
    "\"# c #2E3537\",\n"
    "\"$ c #2E3337\",\n"
    "\"% c #2F3435\",\n"
    "\"& c #2D3435\",\n"
    "\"* c #2E3435\",\n"
    "\"= c #2F3436\",\n"
    "\"            \",\n"
    "\"            \",\n"
    "\"            \",\n"
    "\"   .+  ..   \",\n"
    "\"   @.@#.$   \",\n"
    "\"    %..&    \",\n"
    "\"    *..%    \",\n"
    "\"   #.==.@   \",\n"
    "\"   .@  @.   \",\n"
    "\"            \",\n"
    "\"            \",\n"
    "\"            \"};\n"
;

const char * picto_inc_xpm_ =
    "/* XPM */\n"
    "static char * picto_inc_xpm[] = {\n"
    "\"7 3 2 1\",\n"
    "\"  c None\",\n"
    "\". c #0000FF\",\n"
    "\"   .   \",\n"
    "\"  ...  \",\n"
    "\" ..... \"};\n"
;

const char * picto_dec_xpm_ =
    "/* XPM */\n"
    "static char * picto_dec_xpm[] = {\n"
    "\"7 3 2 1\",\n"
    "\"  c None\",\n"
    "\". c #0000FF\",\n"
    "\" ..... \",\n"
    "\"  ...  \",\n"
    "\"   .   \"};\n"
;

const char * picto_folder16_ =
    "/* XPM */\n"
    "static char *dummy[]={\n"
    "\"16 16 154 2\",\n"
    "\"Qt c None\",\n"
    "\"ao c #00060e\",\n"
    "\"ax c #000710\",\n"
    "\"ap c #000918\",\n"
    "\"aq c #000918\",\n"
    "\"ar c #000918\",\n"
    "\"as c #000919\",\n"
    "\"at c #000918\",\n"
    "\"au c #000919\",\n"
    "\"aw c #000918\",\n"
    "\"av c #000919\",\n"
    "\"an c #304157\",\n"
    "\"ac c #365579\",\n"
    "\".8 c #708596\",\n"
    "\".# c #42648f\",\n"
    "\".e c #476e9b\",\n"
    "\".r c #4a71a2\",\n"
    "\".k c #4b79b0\",\n"
    "\".U c #193a63\",\n"
    "\".V c #203d60\",\n"
    "\".0 c #224c7f\",\n"
    "\"#6 c #274466\",\n"
    "\".X c #27538a\",\n"
    "\".Y c #2a558b\",\n"
    "\".Z c #2a558c\",\n"
    "\".S c #2b5993\",\n"
    "\".P c #2d5a93\",\n"
    "\".Q c #2e5a93\",\n"
    "\".R c #2e5b93\",\n"
    "\".H c #2f517a\",\n"
    "\".O c #2f5b93\",\n"
    "\"#0 c #39679b\",\n"
    "\".N c #3a6aa3\",\n"
    "\".s c #3d6392\",\n"
    "\"#3 c #3e74b0\",\n"
    "\"#2 c #3f74b0\",\n"
    "\"#4 c #4074b0\",\n"
    "\"#1 c #4076b4\",\n"
    "\"#5 c #437ab9\",\n"
    "\"#Q c #495f7b\",\n"
    "\".W c #49709f\",\n"
    "\".f c #4972a6\",\n"
    "\".G c #4976ad\",\n"
    "\"#J c #4a617c\",\n"
    "\"#Z c #4b5f7c\",\n"
    "\".T c #4c74a6\",\n"
    "\"#j c #4d6179\",\n"
    "\"#A c #4e627d\",\n"
    "\"#u c #4e637d\",\n"
    "\".M c #4f7fbf\",\n"
    "\"ab c #506481\",\n"
    "\"#K c #5079a9\",\n"
    "\"#B c #517aaa\",\n"
    "\".b c #517fb7\",\n"
    "\".a c #5180b7\",\n"
    "\".l c #5180ba\",\n"
    "\"#v c #527baa\",\n"
    "\".d c #527eb8\",\n"
    "\".c c #527fb7\",\n"
    "\".J c #5283c2\",\n"
    "\".L c #5284c2\",\n"
    "\"#k c #537ca9\",\n"
    "\".m c #5380ba\",\n"
    "\".o c #5381ba\",\n"
    "\".n c #5382ba\",\n"
    "\".9 c #54799f\",\n"
    "\".q c #5481bb\",\n"
    "\".p c #5482ba\",\n"
    "\".K c #5585c2\",\n"
    "\".u c #568ccd\",\n"
    "\"#7 c #587fad\",\n"
    "\".v c #588ccf\",\n"
    "\".1 c #6d8fb1\",\n"
    "\".I c #77a2d6\",\n"
    "\"ah c #78a3d9\",\n"
    "\".w c #79a3d8\",\n"
    "\"aj c #79a3d9\",\n"
    "\"ak c #7aa3d9\",\n"
    "\"ag c #7ca3d9\",\n"
    "\"ae c #7da3d9\",\n"
    "\".E c #7da7dd\",\n"
    "\".F c #7da9e2\",\n"
    "\"al c #7ea3d8\",\n"
    "\"ai c #7ea3d9\",\n"
    "\"af c #7ea4d9\",\n"
    "\".D c #7fa9df\",\n"
    "\"#M c #80a7d8\",\n"
    "\"#N c #81a7d8\",\n"
    "\"ad c #81a8dd\",\n"
    "\"am c #81a9e0\",\n"
    "\".C c #81aade\",\n"
    "\"#O c #82a7d8\",\n"
    "\"#G c #83a9d9\",\n"
    "\"a# c #83a9da\",\n"
    "\"#S c #83a9db\",\n"
    "\"#F c #83aad9\",\n"
    "\"#H c #84a9d9\",\n"
    "\"#X c #84a9da\",\n"
    "\"#W c #84a9db\",\n"
    "\"#D c #84aad9\",\n"
    "\".B c #84ade1\",\n"
    "\"#9 c #85a9da\",\n"
    "\"#U c #85a9db\",\n"
    "\"#E c #85aad9\",\n"
    "\".A c #85aedf\",\n"
    "\"a. c #86a9da\",\n"
    "\"#T c #86a9db\",\n"
    "\"#n c #86aad9\",\n"
    "\"#y c #86acda\",\n"
    "\".t c #86afe1\",\n"
    "\"#V c #87a9db\",\n"
    "\"#m c #87aad9\",\n"
    "\"#x c #87acda\",\n"
    "\"#L c #87acdd\",\n"
    "\"#8 c #87addf\",\n"
    "\"#p c #87aeda\",\n"
    "\"#P c #87aee1\",\n"
    "\".z c #87afe1\",\n"
    "\"#o c #88abda\",\n"
    "\"#e c #88acda\",\n"
    "\".y c #88afe1\",\n"
    "\"#f c #89acda\",\n"
    "\"#s c #89aeda\",\n"
    "\"#R c #89aee0\",\n"
    "\"#C c #89afde\",\n"
    "\"#Y c #89b1e4\",\n"
    "\"#g c #8aacda\",\n"
    "\"#r c #8aaeda\",\n"
    "\".x c #8aafe0\",\n"
    "\"aa c #8ab0e3\",\n"
    "\"#l c #8bb0df\",\n"
    "\"#I c #8bb1e3\",\n"
    "\"#q c #8caeda\",\n"
    "\"#h c #8db0db\",\n"
    "\"#w c #8db1df\",\n"
    "\"#z c #8db3e4\",\n"
    "\"#d c #8eb1dc\",\n"
    "\"#i c #8eb4e3\",\n"
    "\".h c #8eb6ea\",\n"
    "\".g c #8fb9ef\",\n"
    "\".i c #90b8eb\",\n"
    "\"#t c #91b5e5\",\n"
    "\".j c #93bbef\",\n"
    "\"#c c #b4d4ed\",\n"
    "\"## c #badaf1\",\n"
    "\"#b c #bbdbf3\",\n"
    "\".5 c #bcdbf2\",\n"
    "\".3 c #bcdbf3\",\n"
    "\".4 c #bcdbf4\",\n"
    "\"#a c #bcdcf2\",\n"
    "\".2 c #beddf4\",\n"
    "\".7 c #c1e2fb\",\n"
    "\".6 c #c3dff5\",\n"
    "\"#. c #c7e4f8\",\n"
    "\"QtQtQtQtQtQtQtQtQtQtQtQtQtQtQtQt\",\n"
    "\".#.a.b.c.d.eQtQtQtQtQtQtQtQtQtQt\",\n"
    "\".f.g.h.i.j.k.b.l.m.l.n.o.p.q.rQt\",\n"
    "\".s.t.u.v.w.x.y.z.A.B.C.D.E.F.GQt\",\n"
    "\".H.I.J.K.L.M.N.O.P.Q.R.Q.S.T.UQt\",\n"
    "\".V.W.X.Y.Z.0.1.2.3.3.3.4.5.6.7.8\",\n"
    "\".9#.###a#a#b#c#d#e#f#g#g#f#h#i#j\",\n"
    "\"#k#l#m#m#n#n#o#p#q#r#s#r#r#r#t#u\",\n"
    "\"#v#w#x#x#f#y#x#x#y#g#y#x#x#x#z#A\",\n"
    "\"#B#C#D#E#F#E#G#D#E#D#F#E#D#H#I#J\",\n"
    "\"#K#L#M#N#N#O#N#O#M#M#M#O#N#N#P#Q\",\n"
    "\"#B#R#S#S#T#U#V#U#W#U#T#W#T#X#Y#Z\",\n"
    "\"#0#1#2#2#2#3#4#2#4#2#2#2#2#2#5#6\",\n"
    "\"#7#8#X#9a.#9#X#X#9a.#9a##9#Xaaab\",\n"
    "\"acadaeafagahaeafafaiajakafalaman\",\n"
    "\"aoapaqarasatauavawauatasaraqapax\"};\n"
;

const char * picto_folder22_ =
    "/* XPM */\n"
    "static char *dummy[]={\n"
    "\"22 22 237 2\",\n"
    "\"Qt c None\",\n"
    "\"#n c #000103\",\n"
    "\".Q c #06090c\",\n"
    "\".7 c #05080b\",\n"
    "\"bz c #000002\",\n"
    "\"bQ c #000104\",\n"
    "\"bA c #000e1e\",\n"
    "\"bP c #000f1e\",\n"
    "\"bO c #000f1d\",\n"
    "\"bB c #01101e\",\n"
    "\"bN c #000f1d\",\n"
    "\"bC c #000f1e\",\n"
    "\"bM c #000f1d\",\n"
    "\"bL c #00101d\",\n"
    "\"bD c #01101e\",\n"
    "\"bE c #00101e\",\n"
    "\"bF c #000f1d\",\n"
    "\"bG c #000f1d\",\n"
    "\"bH c #000f1d\",\n"
    "\"bI c #010f1d\",\n"
    "\"bK c #000f1d\",\n"
    "\"bJ c #000f1d\",\n"
    "\".# c #41628c\",\n"
    "\".w c #4a71a1\",\n"
    "\".e c #4e77aa\",\n"
    "\"#B c #90adc8\",\n"
    "\"by c #4f668a\",\n"
    "\"bh c #4d76a6\",\n"
    "\".m c #4e79b1\",\n"
    "\"#A c #b6d6ef\",\n"
    "\"aW c #214f81\",\n"
    "\"aO c #285d96\",\n"
    "\"#o c #2a4e76\",\n"
    "\"#u c #2c5588\",\n"
    "\"#m c #2d5585\",\n"
    "\".8 c #2e4e75\",\n"
    "\"a7 c #2f537b\",\n"
    "\"#f c #2f5b8f\",\n"
    "\"aU c #2f68a7\",\n"
    "\"aR c #2f69a7\",\n"
    "\"#k c #305a8e\",\n"
    "\"#i c #305b8f\",\n"
    "\"aS c #3068a7\",\n"
    "\"aT c #3069a7\",\n"
    "\"aP c #3069a8\",\n"
    "\"#j c #315b8f\",\n"
    "\"aQ c #3169a7\",\n"
    "\"aV c #3169a8\",\n"
    "\"#h c #325b8f\",\n"
    "\"#e c #325b90\",\n"
    "\"#g c #335c8f\",\n"
    "\".R c #345983\",\n"
    "\"#t c #366299\",\n"
    "\"#q c #36639a\",\n"
    "\"#r c #36639b\",\n"
    "\"#s c #37629b\",\n"
    "\"#d c #3a669a\",\n"
    "\"aX c #3a689c\",\n"
    "\".x c #3f6696\",\n"
    "\"#l c #466c9b\",\n"
    "\"a3 c #4679b5\",\n"
    "\"a1 c #467ab5\",\n"
    "\"a4 c #467bb5\",\n"
    "\".6 c #4773a9\",\n"
    "\"aZ c #4779b5\",\n"
    "\"a0 c #477ab5\",\n"
    "\"aY c #477bb6\",\n"
    "\".f c #4870a3\",\n"
    "\"a5 c #487ab5\",\n"
    "\"a2 c #487bb5\",\n"
    "\"#c c #497ab5\",\n"
    "\"a6 c #497cb7\",\n"
    "\".a c #4e7ab2\",\n"
    "\"#p c #4f78aa\",\n"
    "\".d c #4f7ab1\",\n"
    "\".v c #4f7bb2\",\n"
    "\".p c #4f7cb4\",\n"
    "\".c c #507bb2\",\n"
    "\".u c #507cb2\",\n"
    "\".r c #507cb3\",\n"
    "\".o c #507db4\",\n"
    "\".n c #507db5\",\n"
    "\"#. c #5080bc\",\n"
    "\"#a c #5080bd\",\n"
    "\"aw c #5179a9\",\n"
    "\"ao c #517aaa\",\n"
    "\".b c #517bb2\",\n"
    "\".t c #517cb2\",\n"
    "\".s c #517cb3\",\n"
    "\"#b c #5180bd\",\n"
    "\"#1 c #527caa\",\n"
    "\".q c #527db4\",\n"
    "\"#7 c #537baa\",\n"
    "\"#O c #537cab\",\n"
    "\"## c #5381bd\",\n"
    "\".2 c #5386c5\",\n"
    "\".0 c #5486c5\",\n"
    "\".4 c #5487c5\",\n"
    "\".Z c #5487c6\",\n"
    "\".1 c #5586c5\",\n"
    "\".Y c #5588c7\",\n"
    "\".T c #5689c7\",\n"
    "\".W c #568ac8\",\n"
    "\".3 c #5887c5\",\n"
    "\".U c #588ac8\",\n"
    "\"aH c #597395\",\n"
    "\".X c #5989c8\",\n"
    "\"av c #5a7495\",\n"
    "\"aN c #5a7496\",\n"
    "\"an c #5b7596\",\n"
    "\".V c #5b8ac8\",\n"
    "\"a8 c #5c84b0\",\n"
    "\".A c #5c91d3\",\n"
    "\"af c #5d7797\",\n"
    "\".z c #5d91d2\",\n"
    "\"#C c #5e86ae\",\n"
    "\".B c #5e91d3\",\n"
    "\"#6 c #5f7797\",\n"
    "\"#0 c #5f7997\",\n"
    "\"#N c #607996\",\n"
    "\".C c #6090d2\",\n"
    "\"bg c #667d9e\",\n"
    "\".5 c #6b97cd\",\n"
    "\"bl c #6c97cc\",\n"
    "\"bp c #6d97cc\",\n"
    "\"bj c #6e97cc\",\n"
    "\".9 c #6e98cc\",\n"
    "\"bv c #6f97cc\",\n"
    "\"bw c #6f98cc\",\n"
    "\"bq c #7098cc\",\n"
    "\"br c #7199cc\",\n"
    "\"bx c #7199cd\",\n"
    "\"bs c #7298cc\",\n"
    "\"bu c #7299cc\",\n"
    "\"bo c #7398cc\",\n"
    "\"bt c #7399cc\",\n"
    "\"bk c #739acc\",\n"
    "\"bi c #7497cc\",\n"
    "\"bn c #7499cc\",\n"
    "\"bm c #7797cc\",\n"
    "\".P c #78a5dd\",\n"
    "\".S c #79a3d6\",\n"
    "\".O c #79a5dc\",\n"
    "\".N c #7ba6dc\",\n"
    "\".M c #7ca7dd\",\n"
    "\".D c #7da6da\",\n"
    "\".L c #7da8dd\",\n"
    "\"aA c #80a7d8\",\n"
    "\".K c #80a8dd\",\n"
    "\".J c #80aade\",\n"
    "\"aD c #81a7d8\",\n"
    "\"az c #81a8d8\",\n"
    "\"at c #81a9d8\",\n"
    "\".I c #81aade\",\n"
    "\"ay c #82a7d8\",\n"
    "\"aE c #82a8d8\",\n"
    "\"ap c #82a9d8\",\n"
    "\"aJ c #82a9db\",\n"
    "\"aB c #83a7d8\",\n"
    "\"aq c #83a9d8\",\n"
    "\"aL c #83a9db\",\n"
    "\"aG c #83aadb\",\n"
    "\".H c #83acde\",\n"
    "\".y c #83ace0\",\n"
    "\"ar c #84a9d8\",\n"
    "\"aK c #84a9db\",\n"
    "\"ak c #84aada\",\n"
    "\"au c #84abdb\",\n"
    "\"aC c #85a7d8\",\n"
    "\"as c #85a9d8\",\n"
    "\"ax c #85a9da\",\n"
    "\"ah c #85aada\",\n"
    "\"bc c #85aadb\",\n"
    "\"aM c #85abdd\",\n"
    "\".G c #85acdf\",\n"
    "\".F c #85addf\",\n"
    "\"aF c #86a7d8\",\n"
    "\"ag c #86aada\",\n"
    "\"bb c #86aadb\",\n"
    "\"#9 c #86abdb\",\n"
    "\"aI c #86abdd\",\n"
    "\"a# c #86acdb\",\n"
    "\"al c #87aada\",\n"
    "\"b# c #87aadb\",\n"
    "\"ai c #87abda\",\n"
    "\"ac c #87abdb\",\n"
    "\"aa c #87acdb\",\n"
    "\"bf c #87acdd\",\n"
    "\"ad c #88abdb\",\n"
    "\"ab c #88acdb\",\n"
    "\"am c #88acdc\",\n"
    "\"#4 c #88adda\",\n"
    "\"#T c #88addb\",\n"
    "\"ba c #89aadb\",\n"
    "\"aj c #89abda\",\n"
    "\"be c #89abdb\",\n"
    "\"a. c #89acdb\",\n"
    "\"#3 c #89adda\",\n"
    "\"#R c #89addb\",\n"
    "\"a9 c #89addc\",\n"
    "\"ae c #89addd\",\n"
    "\"#8 c #89aedc\",\n"
    "\"#5 c #89afdd\",\n"
    "\".E c #89afe0\",\n"
    "\".g c #89b4ea\",\n"
    "\"b. c #8aabdb\",\n"
    "\"bd c #8aacdb\",\n"
    "\"#2 c #8aadda\",\n"
    "\"#Q c #8aaddb\",\n"
    "\"#K c #8aafdb\",\n"
    "\"#V c #8aafdc\",\n"
    "\".h c #8ab3e7\",\n"
    "\"#S c #8baedb\",\n"
    "\"#J c #8bafdb\",\n"
    "\"#P c #8bafdc\",\n"
    "\"#U c #8caedb\",\n"
    "\"#I c #8cafdb\",\n"
    "\"#W c #8cafdc\",\n"
    "\"#M c #8cb1de\",\n"
    "\".i c #8cb3e7\",\n"
    "\".j c #8cb4e7\",\n"
    "\".k c #8cb5e8\",\n"
    "\"#Y c #8db0dc\",\n"
    "\"#X c #8eafdc\",\n"
    "\".l c #8eb7ea\",\n"
    "\"#L c #8fb2dd\",\n"
    "\"#Z c #90b1de\",\n"
    "\"#v c #95b7d5\",\n"
    "\"#H c #9abde1\",\n"
    "\"#G c #b2d3ed\",\n"
    "\"#E c #b2d4ec\",\n"
    "\"#F c #b3d4ec\",\n"
    "\"#x c #b5d5ee\",\n"
    "\"#w c #b5d6ee\",\n"
    "\"#y c #b6d6ee\",\n"
    "\"#z c #bbd9ef\",\n"
    "\"#D c #bddbf0\",\n"
    "\"QtQtQtQtQtQtQtQtQtQtQtQtQtQtQtQtQtQtQtQtQtQt\",\n"
    "\"QtQtQtQtQtQtQtQtQtQtQtQtQtQtQtQtQtQtQtQtQtQt\",\n"
    "\".#.a.b.c.c.b.d.eQtQtQtQtQtQtQtQtQtQtQtQtQtQt\",\n"
    "\".f.g.h.i.j.k.l.m.n.o.p.q.r.s.s.t.u.v.t.v.wQt\",\n"
    "\".x.y.z.A.B.C.D.E.F.F.G.H.I.J.K.L.M.N.O.P.n.Q\",\n"
    "\".R.S.T.U.V.W.X.Y.Z.0.1.2.2.3.1.0.1.4.0.5.6.7\",\n"
    "\".8.9#.###a#a#b#c#d#e#f#g#h#i#j#h#j#j#k#l#m#n\",\n"
    "\"#o#p#q#r#r#s#t#u#v#w#w#x#y#w#x#w#x#w#x#z#A#B\",\n"
    "\"#C#D#E#F#F#F#F#G#H#I#J#J#K#J#I#J#J#I#J#L#M#N\",\n"
    "\"#O#P#Q#R#Q#S#T#U#J#V#W#V#X#V#P#Y#P#V#W#K#Z#0\",\n"
    "\"#1#V#2#3#3#3#2#4#4#4#3#2#4#3#2#3#3#2#3#3#5#6\",\n"
    "\"#7#8#9a.a#a.aaabaca#abacaba.aaa.#9a#abadaeaf\",\n"
    "\"#7abagagagahagaiajagakahalakagagajagakalaman\",\n"
    "\"aoaiapaqaqarapaqasaqarapapaqaqarasatararauav\",\n"
    "\"awaxayayazaAayaBaCaDaBaDaEaAaBaFaDaDaBaAaGaH\",\n"
    "\"#7aIaJaJaKaLaLaKaKaKaLaLaLaKaLaJaKaKaLaLaMaN\",\n"
    "\"aOaPaQaQaRaSaTaQaTaTaTaTaUaQaTaTaRaTaQaTaVaW\",\n"
    "\"aXaYaZa0a0a0a1a2a1a0aZ#ca2a3a1a4a5a1a5a0a6a7\",\n"
    "\"a8a9acb.b#adbabbbbbcbdb#bbbeb#b##9acadb#bfbg\",\n"
    "\"bhbibjbkblbmbnbobnbpbqbrbsbmbtbubtbvbwbnbxby\",\n"
    "\"bzbAbBbCbDbEbFbGbHbIbJbJbKbHbGbFbLbMbNbObPbQ\",\n"
    "\"QtQtQtQtQtQtQtQtQtQtQtQtQtQtQtQtQtQtQtQtQtQt\"};\n"
;

const char * picto_octet16_ =
    "/* XPM */\n"
    "static char *dummy[]={\n"
    "\"16 16 112 2\",\n"
    "\"Qt c None\",\n"
    "\"#i c #000000\",\n"
    "\"#p c #000000\",\n"
    "\"#y c #000000\",\n"
    "\"#T c #000000\",\n"
    "\"#H c #000000\",\n"
    "\"#P c #000000\",\n"
    "\".# c #010101\",\n"
    "\"#Q c #010101\",\n"
    "\".c c #000000\",\n"
    "\"#S c #000000\",\n"
    "\"## c #000000\",\n"
    "\".a c #000000\",\n"
    "\".b c #000000\",\n"
    "\"#I c #000000\",\n"
    "\"#R c #000000\",\n"
    "\".f c #000000\",\n"
    "\".e c #000000\",\n"
    "\".p c #000000\",\n"
    "\"#h c #181717\",\n"
    "\"#o c #171717\",\n"
    "\"#x c #171717\",\n"
    "\"#G c #161717\",\n"
    "\"#O c #161616\",\n"
    "\".9 c #b3b3b3\",\n"
    "\"#E c #c5c4c4\",\n"
    "\"#n c #ececec\",\n"
    "\"#g c #fefefe\",\n"
    "\".V c #0e0e0e\",\n"
    "\".X c #111111\",\n"
    "\".O c #171717\",\n"
    "\".P c #191919\",\n"
    "\".N c #1b1b1b\",\n"
    "\".G c #2e2e2e\",\n"
    "\".W c #333333\",\n"
    "\".I c #373737\",\n"
    "\".Z c #3a3a3a\",\n"
    "\"#u c #3c3c3c\",\n"
    "\"#s c #414141\",\n"
    "\".H c #444444\",\n"
    "\".L c #4f4f4f\",\n"
    "\"#e c #535252\",\n"
    "\"#c c #595a5a\",\n"
    "\".J c #5b5b5b\",\n"
    "\".S c #676767\",\n"
    "\"#l c #696969\",\n"
    "\"#k c #6b6b6b\",\n"
    "\".Q c #6c6c6c\",\n"
    "\"#t c #777777\",\n"
    "\".Y c #797979\",\n"
    "\".K c #838383\",\n"
    "\"#d c #848484\",\n"
    "\".8 c #a7a7a7\",\n"
    "\"#m c #a9a9a9\",\n"
    "\".7 c #aaaaaa\",\n"
    "\"#f c #b0b0b0\",\n"
    "\"#v c #b3b3b3\",\n"
    "\".R c #bbbbbb\",\n"
    "\".g c #d3d1d1\",\n"
    "\".j c #d4d2d2\",\n"
    "\".l c #d4d3d3\",\n"
    "\".n c #d4d4d4\",\n"
    "\".q c #d5d4d4\",\n"
    "\".r c #d6d4d4\",\n"
    "\".t c #d6d5d5\",\n"
    "\".v c #d7d6d6\",\n"
    "\"#N c #d7d7d7\",\n"
    "\".A c #d8d7d7\",\n"
    "\"#w c #dbd9d9\",\n"
    "\".M c #dcdbdb\",\n"
    "\".U c #dcdcdc\",\n"
    "\"#j c #dfdede\",\n"
    "\".i c #e3e1e1\",\n"
    "\"#. c #e3e2e2\",\n"
    "\"#D c #e3e3e3\",\n"
    "\".y c #e5e4e4\",\n"
    "\".z c #e6e5e5\",\n"
    "\".6 c #e8e7e7\",\n"
    "\"#b c #e8e8e8\",\n"
    "\"#r c #e9e9e9\",\n"
    "\"#M c #eaeaea\",\n"
    "\"#C c #ebebeb\",\n"
    "\"#a c #edecec\",\n"
    "\"#q c #ededed\",\n"
    "\".h c #efeded\",\n"
    "\".o c #efeeee\",\n"
    "\".k c #f0eded\",\n"
    "\".B c #f0efef\",\n"
    "\"#B c #f0f0f0\",\n"
    "\".m c #f1efef\",\n"
    "\".x c #f1f0f0\",\n"
    "\"#F c #f1f1f1\",\n"
    "\".u c #f2f1f1\",\n"
    "\".w c #f3f2f2\",\n"
    "\"#L c #f3f3f3\",\n"
    "\".F c #f3f4f4\",\n"
    "\".s c #f4f3f3\",\n"
    "\".E c #f5f4f4\",\n"
    "\".4 c #f6f6f6\",\n"
    "\".5 c #f7f6f6\",\n"
    "\"#z c #f7f7f7\",\n"
    "\"#J c #f8f8f8\",\n"
    "\".1 c #f8f9f9\",\n"
    "\"#K c #f9f8f8\",\n"
    "\".3 c #f9f9f9\",\n"
    "\".2 c #faf9f9\",\n"
    "\"#A c #fafafa\",\n"
    "\".0 c #fcfbfb\",\n"
    "\".T c #fcfcfc\",\n"
    "\".C c #fefdfd\",\n"
    "\".D c #fffefe\",\n"
    "\".d c #ffffff\",\n"
    "\"Qt.#.a.b.b.b.b.b.b.b.b.b.b.a.cQt\",\n"
    "\"Qt.b.d.d.d.d.d.d.d.d.d.d.d.d.eQt\",\n"
    "\"Qt.f.d.g.h.i.j.i.k.l.m.n.o.d.pQt\",\n"
    "\"Qt.f.d.q.m.r.s.t.u.v.w.v.x.d.pQt\",\n"
    "\"Qt.f.d.l.x.y.q.z.w.v.s.A.w.d.pQt\",\n"
    "\"Qt.f.d.w.B.C.D.D.E.d.C.d.F.d.pQt\",\n"
    "\"Qt.f.d.A.d.G.H.I.u.J.K.L.d.d.pQt\",\n"
    "\"Qt.f.d.M.d.N.O.P.d.Q.R.S.T.d.pQt\",\n"
    "\"Qt.f.d.U.d.V.W.X.s.H.Y.Z.w.0.pQt\",\n"
    "\"Qt.f.d.1.2.3.4.5.6.7.8.8.9#.##Qt\",\n"
    "\"Qt.f.d#a#b#c#d#e#f.d.d#g.o#h#iQt\",\n"
    "\"Qt.f.d#j.d#k.R#l#m.d#n.r#o#pQtQt\",\n"
    "\"Qt.f.d#q#r#s#t#u#v#g#w#x#yQtQtQt\",\n"
    "\"Qt.f.d#z#A#B#C#D#E#F#G#HQtQtQtQt\",\n"
    "\"Qt#I.d#J#K#z#L#M#N#O#PQtQtQtQtQt\",\n"
    "\"Qt#Q.b#R#R#R#R#R#S#TQtQtQtQtQtQt\"};\n"
;

const char * picto_octet22_ =
    "/* XPM */\n"
    "static char *dummy[]={\n"
    "\"22 22 142 2\",\n"
    "\"Qt c None\",\n"
    "\".# c #010101\",\n"
    "\"ah c #010101\",\n"
    "\"al c #000000\",\n"
    "\"#B c #000000\",\n"
    "\"#F c #000000\",\n"
    "\"#N c #000000\",\n"
    "\"#U c #000000\",\n"
    "\"ac c #000000\",\n"
    "\".a c #000000\",\n"
    "\"#2 c #000000\",\n"
    "\"ak c #000000\",\n"
    "\".b c #000000\",\n"
    "\"ai c #000000\",\n"
    "\"aj c #000000\",\n"
    "\"#q c #000000\",\n"
    "\".c c #000000\",\n"
    "\".e c #000000\",\n"
    "\"ag c #7d7d7d\",\n"
    "\"#A c #adadad\",\n"
    "\"ab c #8f8e8e\",\n"
    "\"#7 c #939292\",\n"
    "\"#M c #979595\",\n"
    "\"#E c #9b9a9a\",\n"
    "\"#T c #9a9999\",\n"
    "\"#1 c #9b9a9a\",\n"
    "\"#z c #7f7f7f\",\n"
    "\"#L c #e1dfdf\",\n"
    "\"#0 c #e4e2e2\",\n"
    "\"#D c #fcfbfb\",\n"
    "\"#u c #070707\",\n"
    "\"#r c #0e0e0e\",\n"
    "\"#j c #1f1f1f\",\n"
    "\"#k c #222222\",\n"
    "\"#i c #232323\",\n"
    "\"#a c #252525\",\n"
    "\"## c #272727\",\n"
    "\"#w c #292929\",\n"
    "\"#4 c #2d2d2d\",\n"
    "\"#b c #313131\",\n"
    "\"#t c #343434\",\n"
    "\"#. c #363636\",\n"
    "\"#s c #383838\",\n"
    "\".3 c #3d3d3d\",\n"
    "\".0 c #3e3e3e\",\n"
    "\"#H c #4c4c4c\",\n"
    "\".1 c #4d4d4d\",\n"
    "\".7 c #4e4e4e\",\n"
    "\".2 c #505050\",\n"
    "\".4 c #515151\",\n"
    "\"#J c #575757\",\n"
    "\"#x c #5d5d5d\",\n"
    "\"#o c #5e5e5e\",\n"
    "\"#X c #606060\",\n"
    "\"#5 c #626262\",\n"
    "\"#6 c #646464\",\n"
    "\"#y c #656565\",\n"
    "\"#f c #676767\",\n"
    "\"#I c #6d6d6d\",\n"
    "\"#c c #6e6e6e\",\n"
    "\".6 c #717171\",\n"
    "\".5 c #737373\",\n"
    "\"#v c #898989\",\n"
    "\"#Q c #8b8b8b\",\n"
    "\"#Z c #9c9c9c\",\n"
    "\"#C c #9f9f9f\",\n"
    "\"a. c #a4a4a4\",\n"
    "\"a# c #acabab\",\n"
    "\"#l c #aeaeae\",\n"
    "\"aa c #afaeae\",\n"
    "\"#e c #b1b1b1\",\n"
    "\"#d c #b5b5b5\",\n"
    "\"#n c #bababa\",\n"
    "\"#m c #bebebe\",\n"
    "\"#Y c #bfbfbf\",\n"
    "\".f c #c4c3c3\",\n"
    "\".h c #c6c5c5\",\n"
    "\".o c #c8c6c6\",\n"
    "\".k c #c8c7c7\",\n"
    "\".r c #c9c8c8\",\n"
    "\".I c #c9c9c9\",\n"
    "\"#8 c #cac8c8\",\n"
    "\".A c #cac9c9\",\n"
    "\".z c #cacaca\",\n"
    "\".s c #cbcaca\",\n"
    "\".u c #cbcbcb\",\n"
    "\".B c #cccbcb\",\n"
    "\".L c #cccccc\",\n"
    "\".C c #cdcccc\",\n"
    "\".E c #cdcdcd\",\n"
    "\".G c #cecdcd\",\n"
    "\".S c #cecece\",\n"
    "\".V c #cfcfcf\",\n"
    "\".W c #d0d0d0\",\n"
    "\"#G c #d1d1d1\",\n"
    "\"#p c #d2d2d2\",\n"
    "\".Z c #d4d4d4\",\n"
    "\".Y c #d5d5d5\",\n"
    "\"#h c #d6d6d6\",\n"
    "\".X c #d8d8d8\",\n"
    "\".U c #d9d9d9\",\n"
    "\"ad c #dadada\",\n"
    "\".q c #dcdbdb\",\n"
    "\".8 c #dedede\",\n"
    "\".p c #dfdddd\",\n"
    "\".j c #dfdede\",\n"
    "\".l c #e0dfdf\",\n"
    "\"#S c #e3e1e1\",\n"
    "\"#V c #e3e2e2\",\n"
    "\".P c #e3e3e3\",\n"
    "\".T c #e4e4e4\",\n"
    "\"#W c #e5e4e4\",\n"
    "\".O c #e5e5e5\",\n"
    "\"#g c #e6e5e5\",\n"
    "\".K c #e6e6e6\",\n"
    "\".R c #e7e7e7\",\n"
    "\".M c #e8e8e8\",\n"
    "\"#K c #efeeee\",\n"
    "\"#R c #f0efef\",\n"
    "\"af c #f2f2f2\",\n"
    "\"#9 c #f4f4f4\",\n"
    "\"ae c #f5f5f5\",\n"
    "\"#P c #f5f6f6\",\n"
    "\".g c #f6f4f4\",\n"
    "\".i c #f6f5f5\",\n"
    "\"#O c #f6f6f6\",\n"
    "\".m c #f7f7f7\",\n"
    "\".n c #f8f7f7\",\n"
    "\".9 c #f9f8f8\",\n"
    "\".t c #f9f9f9\",\n"
    "\".y c #faf9f9\",\n"
    "\".Q c #fafafa\",\n"
    "\".x c #fbfafa\",\n"
    "\".w c #fbfbfb\",\n"
    "\".H c #fcfbfb\",\n"
    "\".J c #fcfcfc\",\n"
    "\".v c #fdfcfc\",\n"
    "\".F c #fdfdfd\",\n"
    "\"#3 c #fefdfd\",\n"
    "\".N c #fefefe\",\n"
    "\".D c #fffefe\",\n"
    "\".d c #ffffff\",\n"
    "\"Qt.#.a.b.b.b.b.b.b.b.b.b.b.b.b.b.b.b.a.#QtQt\",\n"
    "\"Qt.c.d.d.d.d.d.d.d.d.d.d.d.d.d.d.d.d.d.cQtQt\",\n"
    "\"Qt.e.d.f.g.h.i.j.k.l.m.k.n.o.i.p.h.q.d.eQtQt\",\n"
    "\"Qt.e.d.r.n.s.t.u.v.u.w.u.x.u.y.z.x.r.d.eQtQt\",\n"
    "\"Qt.e.d.A.y.B.w.C.D.E.F.G.F.C.H.u.H.z.d.eQtQt\",\n"
    "\"Qt.e.d.I.w.u.J.K.L.M.N.E.D.L.J.O.A.P.d.eQtQt\",\n"
    "\"Qt.e.d.H.m.x.w.Q.F.w.N.N.Q.d.t.w.Q.m.d.eQtQt\",\n"
    "\"Qt.e.d.u.J.K.E.R.N.M.S.M.d.S.d.E.J.T.U.eQtQt\",\n"
    "\"Qt.e.d.S.F.V.d.W.d.V.d.V.d.W.d.V.N.E.d.eQtQt\",\n"
    "\"Qt.e.d.S.F.V.d.W.d.U.d.X.d.S.d.Y.d.Z.d.eQtQt\",\n"
    "\"Qt.e.d.u.J.R.E.M.d.0.1.2.3.u.4.5.6.7.8.eQtQt\",\n"
    "\"Qt.e.d.x.x.9.J.y.d#.###a#b.T#c#d#e#f.d.eQtQt\",\n"
    "\"Qt.e.d.T.B.K.H#g#h###i#j#k#l#f#m#n#o#p#qQtQt\",\n"
    "\"Qt.e.d.C.d.G.F.E.d#r#s#t#u#v#w#x#y#z#A#BQtQt\",\n"
    "\"Qt.e.d.E.d.S.F.E.w#C#l#c.d.F.d.N#D#E#FQtQtQt\",\n"
    "\"Qt.e.d.T.z.O.t.T#G#H#I#J.d.m#K#L#M#NQtQtQtQt\",\n"
    "\"Qt.e.d#O.J.m.m#P.d#y#l#Q.H#R#S#T#UQtQtQtQtQt\",\n"
    "\"Qt.e.d#V.z#W.9#S.S#X#Y#Z.w#0#1#2QtQtQtQtQtQt\",\n"
    "\"Qt.e.d.B#3.L.w.s.d#4#5#6.y#7.aQtQtQtQtQtQtQt\",\n"
    "\"Qt.e.d.s.v.B.y#8#9a.a#aaabacQtQtQtQtQtQtQtQt\",\n"
    "\"Qt.c.d#9adae.daf.Z.O.Rag#UQtQtQtQtQtQtQtQtQt\",\n"
    "\"QtahaiajajajajajajajakalQtQtQtQtQtQtQtQtQtQt\"}\n";
;

const struct { const char * name, * xpm; int size; } pictos_[] {
    { "go-up",                      picto_up_xpm_,      8  },
    { "go-down",                    picto_down_xpm_,    8  },
    { "go-previous",                picto_left_xpm_,    8  },
    { "go-next",                    picto_right_xpm_,   8  },
    { "window-close",               picto_close_xpm_,   12 },
    { "picto-inc",                  picto_inc_xpm_,     8  },
    { "picto-dec",                  picto_dec_xpm_,     8  },
    { "folder",                     picto_folder16_,    16 },
    { "folder",                     picto_folder22_,    22 },
    { "application-octet-stream",   picto_octet16_,     16 },
    { "application-octet-stream",   picto_octet22_,     22 }
};

struct Action_def {
    const char *    name;
    const char *    label;
    const char *    icon_name;
    const char *    tooltip;
    const char *    accels;
};

const Action_def action_defs_[] {
    //     name         label                               icon                tooltip                                            accels
    { "dialog-apply",   gettext_noop("Apply"),              "dialog-ok",        gettext_noop("Apply changes"),                     "Enter"                      },
    { "dialog-cancel",  gettext_noop("Cancel"),             "dialog-cancel",    gettext_noop("Cancel changes"),                    "Escape Cancel"              },
    { "edit-copy",      gettext_noop("Copy"),               "edit-copy",        gettext_noop("Copy selection to clipboard"),       "<Ctrl>C <Ctrl>Insert"       },
    { "edit-cut",       gettext_noop("Cut"),                "edit-cut",         gettext_noop("Cut selection to clipboard"),        "<Ctrl>X <Ctrl>Delete"       },
    { "edit-paste",     gettext_noop("Paste"),              "edit-paste",       gettext_noop("Paste selection from clipboard"),    "<Ctrl>V <Shift>Insert"      },
    { "focus-next",     gettext_noop("Focus Next"),         "go-next",          gettext_noop("Focus Next Child"),                  "Tab"                        },
    { "focus-previous", gettext_noop("Focus Previous"),     "go-previous",      gettext_noop("Focus Previous Child"),              "<Shift>Tab <Shift>TabL"     },
    { "pan-left",       gettext_noop("Pan Left"),           "",                 gettext_noop("Pan Left"),                          "<Ctrl><Alt>Left"            },
    { "pan-right",      gettext_noop("Pan Right"),          "",                 gettext_noop("Pan Right"),                         "<Ctrl><Alt>Right"           },
    { "pan-up",         gettext_noop("Pan Up"),             "",                 gettext_noop("Pan Up"),                            "<Ctrl>Up"                   },
    { "pan-down",       gettext_noop("Pan Down"),           "",                 gettext_noop("Pan Down"),                          "<Ctrl>Down"                 },
    { nullptr,          nullptr,                            nullptr,            nullptr,                                           nullptr                      }
};

// Default style items.
const struct { tau::Conf::Item id; const char * value; } items_[] = {
    { tau::Conf::FONT,                  ""              },
    { tau::Conf::MONOSPACE_FONT,        ""              },
    { tau::Conf::EDIT_FONT,             ""              },
    { tau::Conf::TOOLTIP_FONT,          ""              },
    { tau::Conf::MENU_FONT,             ""              },
    { tau::Conf::FOREGROUND,            "Black"         },
    { tau::Conf::ALT_FOREGROUND,        "Snow"          },
    { tau::Conf::WEAK_FOREGROUND,       "#BBB"          },
    { tau::Conf::MENU_FOREGROUND,       "DarkSlateGray" },
    { tau::Conf::SLIDER_FOREGROUND,     "AliceBlue"     },
    { tau::Conf::PROGRESS_FOREGROUND,   "Blue"          },
    { tau::Conf::ACCEL_FOREGROUND,      "#395CC8"       },
    { tau::Conf::PROGRESS_BACKGROUND,   "DeepSkyBlue"   },
    { tau::Conf::BACKGROUND,            "LightGray"     },
    { tau::Conf::ALT_BACKGROUND,        "#888"          },
    { tau::Conf::WHITESPACE_BACKGROUND, "Snow"          },
    { tau::Conf::MENU_BACKGROUND,       "Silver"        },
    { tau::Conf::SELECT_BACKGROUND,     "DeepSkyBlue"   },
    { tau::Conf::BUTTON_BACKGROUND,     "Gainsboro"     },
    { tau::Conf::SLIDER_BACKGROUND,     "DarkGray"      },
    { tau::Conf::INFO_BACKGROUND,       "#95C8AF"       },
    { tau::Conf::HELP_BACKGROUND,       "Aqua"          },
    { tau::Conf::WARNING_BACKGROUND,    "#F8ED89"       },
    { tau::Conf::ERROR_BACKGROUND,      "#EC99B5"       },
    { tau::Conf::TOOLTIP_FOREGROUND,    "Navy"          },
    { tau::Conf::HIGHLIGHT_BACKGROUND,  "#C5FBFB"       },
    { tau::Conf::RADIUS,                "0"             },
    { tau::Conf::ICON_SIZE,             "4"             },  // FIXME How to embed Icon:: constant here?
    { tau::Conf::BUTTON_LABEL,          "true"          }
};

struct Pixmap_holder {
    tau::Pixmap_ptr pixmap;
    tau::Timeval    tv;
};

using Pixmap_cache = std::unordered_map<std::string, Pixmap_holder>;

Pixmap_cache        icon_cache_;
Pixmap_cache        pixmap_cache_;

void cache_icon(tau::Pixmap_ptr icon, const tau::ustring & name, const tau::ustring & context, int size) {
    Pixmap_holder hol;
    hol.pixmap = icon;
    hol.tv = tau::Timeval::now();
    tau::ustring key = tau::str_toupper(tau::str_format(name, '-', (context.empty() ? "ANY" : context), '-', size));
    std::unique_lock lk(mx_);
    icon_cache_[key] = hol;
}

tau::Pixmap_cptr uncache_icon(const tau::ustring & name, const tau::ustring & context, int size) {
    tau::ustring key = tau::str_toupper(str_format(name, '-', (context.empty() ? "ANY" : context), '-', size));
    std::unique_lock lk(mx_);
    auto iter = icon_cache_.find(key);

    if (iter != icon_cache_.end()) {
        iter->second.tv = tau::Timeval::now();
        return iter->second.pixmap;
    }

    return nullptr;
}

void cache_pixmap(tau::Pixmap_ptr pixmap, const tau::ustring & name) {
    Pixmap_holder hol;
    hol.pixmap = pixmap;
    hol.tv = tau::Timeval::now();
    tau::ustring key = tau::str_toupper(tau::str_trim(name));
    std::unique_lock ml(mx_);
    pixmap_cache_[key] = hol;
}

tau::Pixmap_cptr uncache_pixmap(const tau::ustring & name) {
    tau::ustring key = tau::str_toupper(str_trim(name));
    std::unique_lock ml(mx_);
    auto iter = pixmap_cache_.find(key);

    if (iter != pixmap_cache_.end()) {
        iter->second.tv = tau::Timeval::now();
        return iter->second.pixmap;
    }

    return nullptr;
}

// Do not cache icon if param size <= 0.
tau::Pixmap_ptr find_icon_in_dir(Icon_dir & dir, const tau::ustring & name, const tau::ustring & context, int size) {
    if (tau::file_is_dir(dir.path)) {
        if (!dir.files_listed) {
            try {
                for (auto & file: file_list(dir.path)) {
                    if (tau::file_is_regular(tau::path_build(dir.path, file))) {
                        if (tau::Pixmap_impl::signal_file_supported()(file)) {
                            dir.files.push_back(file);
                        }
                    }
                }

                dir.files_listed = true;
            }

            catch (tau::exception & x) { std::cerr << "** Theme_impl::find_icon_in_dir: dir=" << dir.path << ": " << x.what() << std::endl; return nullptr; }
            catch (std::exception & x) { std::cerr << "** Theme_impl::find_icon_in_dir: dir=" << dir.path << ": " << x.what() << std::endl; return nullptr; }
            catch (...) { std::cerr << "** Theme_impl::find_icon_in_dir: dir=" << dir.path << ": unknown exception caught" << std::endl; return nullptr; }
        }

        std::list<tau::ustring> blacklist;

        for (auto & file: dir.files) {
            tau::ustring basename = tau::path_basename(file);

            if (tau::str_similar(basename, name)) {
                tau::ustring path = tau::path_build(dir.path, file);

                try {
                    if (auto pixmap = tau::Pixmap_impl::load_from_file(path)) {
                        if (size > 0) { cache_icon(pixmap, name, context, size); }
                        for (auto & s: blacklist) { dir.files.remove(s); }
                        return pixmap;
                    }
                }

                catch (tau::exception & x) {
                    std::cerr << "** Theme_impl::find_icon_in_dir: file=" << path << ": " << x.what() << std::endl;
                    blacklist.push_back(file);
                    break;
                }

                catch (std::exception & x) {
                    std::cerr << "** Theme_impl::find_icon_in_dir: file=" << path << ": " << x.what() << std::endl;
                    blacklist.push_back(file);
                    break;
                }

                catch (...) {
                    std::cerr << "** Theme_impl::find_icon_in_dir: file=" << path << ": unknown exception caught" << std::endl;
                    blacklist.push_back(file);
                    break;
                }
            }
        }

        for (auto & s: blacklist) { dir.files.remove(s); }
    }

    return nullptr;
}

} // anonymous namespace

// ----------------------------------------------------------------------------
// ----------------------------------------------------------------------------

namespace tau {

Theme_impl::Theme_impl() {
    Display_impl::signal_open().connect(fun(this, &Theme_impl::on_display_open));
}

// Overridden by Theme_unix.
void Theme_impl::boot() {
    boot_linkage();

    auto p = path_dirname(path_self());
    add_icon_dir(p);
    add_pixmap_dir(p);
    add_cursor_dir(p);

    auto tid = std::this_thread::get_id();
    update_this_thread();
    auto si = sysinfo();
    ustring prefix = path_prefix(), share = path_build(prefix, "share", program_name());

    add_cursor_dir(path_build(prefix, "cursors"));
    add_pixmap_dir(path_build(prefix, "pixmaps"));
    add_icon_dir(path_build(prefix, "icons"));

    add_cursor_dir(path_build(prefix, "share", "cursors"));
    add_pixmap_dir(path_build(prefix, "share", "pixmaps"));
    add_icon_dir(path_build(prefix, "share", "icons"));

    add_cursor_dir(path_build(share, "cursors"));
    add_pixmap_dir(path_build(share, "pixmaps"));
    add_icon_dir(path_build(share, "icons"));

    add_cursor_dir(path_build(sysinfo_.lib_prefix, "share", "cursors"));
    add_pixmap_dir(path_build(sysinfo_.lib_prefix, "share", "pixmaps"));
    add_icon_dir(path_build(sysinfo_.lib_prefix, "share", "icons"));

    share = path_build(prefix, "share", str_format("tau-", sysinfo_.Major, '.', sysinfo_.Minor));
    add_cursor_dir(path_build(share, "cursors"));
    add_pixmap_dir(path_build(share, "pixmaps"));
    add_icon_dir(path_build(share, "icons"));

    auto i = threads_.find(tid);
    if (i == threads_.end()) { throw internal_error("Theme_impl::init_actions(): unable to setup thread"); }
    Thread & thr = i->second;

    // Init actions.
    for (const Action_def * ap = action_defs_; ap->name; ++ap) {
        auto [ i, succeed ] = thr.actions.emplace(ap->name, Master_action());
        Master_action & ma = i->second;
        ma.set_label(ap->label);
        ma.set_icon(ap->icon_name);
        ma.set_tooltip(ap->tooltip);
        ma.add_accels(ap->accels);
    }

    boot_fallback_theme("Tau");
    for (unsigned k = 0; k < std::size(items_); ++k) { conf_.set(items_[k].id, items_[k].value); }
    update_fonts();
    conf_.signal_set().connect(fun(this, &Theme_impl::on_conf_set));
}

void Theme_impl::update_fonts() {
    auto norm = Font::normal();
    conf_.set(Conf::FONT, norm);
    conf_.set(Conf::MONOSPACE_FONT, Font::mono());
    conf_.set(Conf::EDIT_FONT, norm);
    conf_.set(Conf::TOOLTIP_FONT, Font::enlarge(norm, -2));
    conf_.set(Conf::MENU_FONT, Font::enlarge(norm, -1));
}

void Theme_impl::boot_cursor_themes(const ustring & names) {
    set_cursor_theme(names);
}

void Theme_impl::boot_icon_themes(const ustring & names) {
    set_icon_theme(names);
}

void Theme_impl::boot_fallback_theme(const ustring & name) {
    int itheme = find_icon_theme_nolock(name);
    while (itheme < 0 && 0 != nicon_dirs_)  { feed_icon_dir(name); itheme = find_icon_theme_nolock(name); }

    if (itheme >= 0) {
        fallback_theme_name_ = name;
        ifallback_ = itheme;
    }
}

void Theme_impl::sweep() {
    Timeval now = Timeval::now();
    std::unique_lock ml(mmx_);

    if (pixmap_cache_.size() > 100) {
        std::list<std::string> outdated;

        for (auto i = pixmap_cache_.begin(); i != pixmap_cache_.end(); ++i) {
            if (now-i->second.tv > 300000000) {
                outdated.push_back(i->first);
            }

            for (const std::string & key: outdated) {
                pixmap_cache_.erase(key);
            }
        }
    }

    if (icon_cache_.size() > 400) {
        std::list<std::string> outdated;

        for (auto i = icon_cache_.begin(); i != icon_cache_.end(); ++i) {
            if (now-i->second.tv > 900000000) {
                outdated.push_back(i->first);
            }

            for (const std::string & key: outdated) {
                icon_cache_.erase(key);
            }
        }
    }
}

void Theme_impl::add_icon_dir(const ustring & dir) {
    if (file_is_dir(dir)) {
        std::unique_lock sl(mx_);

        if (icon_dirs_.end() == std::find(icon_dirs_.begin(), icon_dirs_.end(), dir)) {
            icon_dirs_.push_back(dir);
            nicon_dirs_++;
        }
    }
}

void Theme_impl::add_pixmap_dir(const ustring & dir) {
    if (file_is_dir(dir)) {
        std::unique_lock sl(mx_);

        if (pixmap_dirs_.end() == std::find(pixmap_dirs_.begin(), pixmap_dirs_.end(), dir)) {
            pixmap_dirs_.push_back(dir);
        }
    }
}

void Theme_impl::add_cursor_dir(const ustring & dir) {
    if (file_is_dir(dir)) {
        std::unique_lock sl(mx_);

        if (cursor_dirs_.end() == std::find(cursor_dirs_.begin(), cursor_dirs_.end(), dir)) {
            cursor_dirs_.push_back(dir);
        }
    }
}

bool Theme_impl::feed_icon_root(const ustring & root, const ustring & stop_after) const {
    ustring this_filename = path_notdir(root);
    std::vector<ustring> subdirs;

    for (const ustring & file: file_list(root)) {
        if ("." != file && ".." != file) {
            if (file_is_dir(path_build(root, file))) {
                subdirs.push_back(file);
            }
        }
    }

    Key_file kf(path_build(root, "index.theme"), ',');
    kf.lock();
    Key_section * theme_sect = kf.has_section("Icon Theme") ? &kf.section("Icon Theme") : nullptr;
    ustring theme_name = theme_sect ? kf.get_string(*theme_sect, "Name", this_filename) : this_filename;
    int itheme = -1;
    std::vector<ustring> inherits;

    if (theme_sect) {
        inherits = str_explode(kf.get_string(*theme_sect, "Inherits"), ',');

        if (kf.has_key(*theme_sect, "Directories")) {
            std::unique_lock sl(mx_);
            itheme = find_icon_theme_nolock(theme_name);

            if (itheme < 0 || !str_similar(root, icon_themes_[itheme].roots)) {
                if (itheme < 0) {
                    itheme = icon_themes_.size();
                    icon_themes_.emplace_back();
                    icon_themes_[itheme].name = theme_name;
                }

                auto & iref = icon_themes_[itheme];
                iref.roots.push_back(root);
                if (ifallback_ < 0 && str_similar(theme_name, fallback_theme_name_)) { ifallback_ = itheme; }

                for (const ustring & s: inherits) {
                    iref.inherits.push_back(s);
                    int inherited = find_icon_theme_nolock(s);
                    if (inherited >= 0) { iref.inherited.push_back(inherited); }
                }

                // Save new pointer to the themes that inherit this new theme.
                for (Icon_theme & theme: icon_themes_) {
                    if (&theme != &iref) {
                        if (str_similar(theme_name, theme.inherits)) {
                            if (theme.inherited.end() == std::find(theme.inherited.begin(), theme.inherited.end(), itheme)) {
                                theme.inherited.push_back(itheme);
                            }
                        }
                    }
                }

                iref.comment += kf.get_string(*theme_sect, "Comment");
                iref.hidden = kf.get_boolean(*theme_sect, "Hidden");
                iref.example += kf.get_string(*theme_sect, "Example");

                for (const ustring & dir: kf.get_strings(*theme_sect, "Directories")) {
                    if (kf.has_section(dir)) {
                        ustring path = path_build(root, dir);

                        if (file_is_dir(path)) {
                            Icon_dir idir;
                            Key_section & sect = kf.section(dir);
                            idir.path = path;
                            idir.context = kf.get_string(sect, "Context");
                            idir.size = kf.get_integer(sect, "Size");
                            idir.scale = kf.get_integer(sect, "Scale", 1);
                            idir.threshold = kf.get_integer(sect, "Threshold", 2);
                            idir.min_size = kf.get_integer(sect, "MinSize");
                            idir.max_size = kf.get_integer(sect, "MaxSize");
                            idir.type = kf.get_string(sect, "Type");
                            if (str_similar(idir.type, "Scalable")) { idir.scalable = true; }
                            iref.dirs.push_back(idir);
                        }
                    }
                }

                // Update icon_theme_names_.
                if (!iref.hidden) {
                    auto v = str_explode(icon_theme_names_, ":;");
                    if (v.end() == std::find(v.begin(), v.end(), theme_name)) { v.push_back(theme_name); }
                    icon_theme_names_ = str_implode(v, ':');
                }
            }
        }
    }

    int ctheme = -1;

    if (subdirs.end() != std::find(subdirs.begin(), subdirs.end(), "cursors")) {
        ustring cursors_root = path_build(root, "cursors");
        std::unique_lock sl(mx_);
        ctheme = find_cursor_theme_nolock(theme_name);

        if (ctheme < 0 || !str_similar(cursors_root, cursor_themes_[ctheme].roots)) {
            if (ctheme < 0) {
                ctheme = cursor_themes_.size();
                cursor_themes_.emplace_back();
                cursor_themes_[ctheme].name = theme_name;
            }

            Cursor_theme & rctheme = cursor_themes_[ctheme];
            rctheme.roots.push_back(cursors_root);

            for (const ustring & s: inherits) {
                rctheme.inherits.push_back(s);
                int inherited = find_cursor_theme_nolock(s);
                if (inherited >= 0) { rctheme.inherited.push_back(inherited); }
            }

            // Save new pointer to the themes that inherit this new theme.
            for (Cursor_theme & theme: cursor_themes_) {
                if (&theme != &rctheme) {
                    if (str_similar(theme_name, theme.inherits)) {
                        if (theme.inherited.end() == std::find(theme.inherited.begin(), theme.inherited.end(), ctheme)) {
                            theme.inherited.push_back(ctheme);
                        }
                    }
                }
            }
        }
    }

    if (itheme >= 0 || ctheme >= 0) {
        if (str_similar(theme_name, str_explode(stop_after, ":;"))) {
            return true;
        }
    }

    for (const ustring & file: subdirs) {
        if (feed_icon_root(path_build(root, file), stop_after)) { // recursive call.
            return true;
        }
    }

    return false;
}

void Theme_impl::feed_icon_dir(const ustring & stop_after) const {
    ustring dir;

    {   std::unique_lock sl(mx_);

        if (!icon_dirs_.empty()) {
            dir = icon_dirs_.front();
        }
    }

    if (!dir.empty()) {
        feed_icon_root(dir, stop_after);

        {
            std::unique_lock lock(mx_);
            icon_dirs_.remove(dir);
            nicon_dirs_ = icon_dirs_.size();
        }
    }
}

int Theme_impl::find_cursor_theme(const tau::ustring & name) const {
    std::unique_lock sl(mx_);
    return find_cursor_theme_nolock(name);
}

int Theme_impl::find_cursor_theme_nolock(const tau::ustring & names) const {
    auto v = str_explode(names, ":;");
    Cursor_theme * p = cursor_themes_.data();
    for (std::size_t n = 0; n < cursor_themes_.size(); n++, p++) { if (str_similar(p->name, v)) return n; }
    return -1;
}

std::vector<ustring> Theme_impl::list_cursor_themes() const {
    while (0 != nicon_dirs_) { feed_icon_dir(); }
    std::vector<ustring> v;
    std::unique_lock sl(mx_);
    for (const Cursor_theme & theme: cursor_themes_) { v.push_back(theme.name); }
    return v;
}

void Theme_impl::set_cursor_theme(const ustring & names) {
    int save = icursor_, icursor = find_cursor_theme(names);
    while (icursor < 0 && 0 != nicon_dirs_) { feed_icon_dir(names); icursor = find_cursor_theme(names); }

    if (icursor >= 0 && icursor != save) {
        std::unique_lock ml(mmx_);
        icursor_ = icursor;
        for (auto & p: threads_) { p.second.event_cursor_theme_changed->emit(); }
    }
}

Cursor_ptr Theme_impl::find_cursor_in_theme(int ic, const std::vector<ustring> & unames, std::vector<int> & seen, int size) {
    auto & rctheme = cursor_themes_[ic];
    std::vector<ustring> roots; { std::unique_lock sl(mx_); roots = rctheme.roots; }

    for (const ustring & root: roots) {
        if (seen.end() == std::find(seen.begin(), seen.end(), ic)) {
            seen.push_back(ic);

            try {
                for (const ustring & file: file_glob(path_build(root, "*"))) {
                    ustring base = str_trim(str_toupper(path_basename(file)));

                    for (const ustring & name: unames) {
                        if (name == base) {
                            try {
                                if (auto cursor = Cursor_impl::load_from_file(file, size)) {
                                    return cursor;
                                }
                            }

                            catch (...) { break; }
                        }
                    }
                }
            }

            catch (...) { break; }
        }
    }

    std::vector<int> inherited; { std::unique_lock sl(mx_); inherited = rctheme.inherited; }

    for (int ii: inherited) {
        if (auto cursor = find_cursor_in_theme(ii, unames, seen, size)) { // recursive call.
            return cursor;
        }
    }

    return nullptr;
}

Cursor_ptr Theme_impl::find_cursor(const ustring & names, int size) {
    auto tid = std::this_thread::get_id();
    if (0 == size) { size = cursor_size_; }
    std::vector<ustring> unames;
    for (const ustring & s: str_explode(names, ":;")) { unames.push_back(str_trim(str_toupper(s))); }

    {
        std::unique_lock ml(mmx_);
        auto i = threads_.find(tid);

        for (const ustring & name: unames) {
            // Trying to uncache cursor for thread cache.
            if (i != threads_.end()) {
                if (auto cursor = uncache_cursor(i->second.cursor_cache, name, size)) {
                    return cursor;
                }
            }
        }
    }

    if (icursor_ >= 0) {
        std::vector<int> seen;

        if (auto cursor = find_cursor_in_theme(icursor_, unames, seen, size)) {
            return cursor;
        }
    }

    // Now try to search in cursor map.
    for (const ustring & name: unames) {
        ustring path;

        {
            std::unique_lock sl(mx_);
            auto i = cursor_map_.find(name);
            if (i != cursor_map_.end()) { path = i->second; }
        }

        if (!path.empty()) {
            try {
                if (auto cursor = Cursor_impl::load_from_file(path, size)) {
                    return cursor;
                }
            }

            catch (...) {
                std::unique_lock sl(mx_);
                cursor_map_.erase(name);
            }
        }
    }

    // Search in booted directories.
    Dirs dirs; { std::unique_lock sl(mx_); dirs = cursor_dirs_; }

    for (const ustring & dir: dirs) {
        try {
            for (const ustring & path: file_find(dir, "*")) {
                ustring base = str_trim(str_toupper(path_basename(path)));

                // Copy to cursor map.
                {
                    std::unique_lock sl(mx_);
                    if (cursor_map_.find(base) == cursor_map_.end()) {
                        cursor_map_[base] = path;
                    }
                }

                for (const ustring & name: unames) {
                    if (base == name) {
                        try {
                            if (auto cursor = Cursor_impl::load_from_file(path, size)) {
                                return cursor;
                            }
                        }

                        catch (...) { break; }
                    }
                }
            }

            // All cursor paths from current directory are copied
            // to the cursor map, so we can erase that directory.
            {
                std::unique_lock sl(mx_);
                auto i = std::find(cursor_dirs_.begin(), cursor_dirs_.end(), dir);
                if (i != cursor_dirs_.end()) { cursor_dirs_.erase(i); }
            }
        }

        catch (...) {}
    }

    // Trying to obtain cursor from Display.
    {
        std::unique_lock ml(mmx_);
        auto i = threads_.find(tid);

        if (i != threads_.end()) {
            // Display_xcb uses case sensitive cursor lookup, so don't uppercase cursor name here!
            for (const ustring & s: str_explode(names, ":;")) {
                if (auto cursor = i->second.lookup(s)) {
                    cache_cursor(i->second.cursor_cache, cursor, str_trim(str_toupper(s)), size);
                    return cursor;
                }
            }
        }
    }

    return nullptr;
}

void Theme_impl::cache_cursor(Cursor_cache & cache, Cursor_ptr cursor, const ustring & name, int size) {
    Cursor_holder hol;
    hol.cursor = cursor;
    hol.tv = Timeval::now();
    ustring key = str_format(name, '_', size);
    cache[key] = hol;
}

Cursor_ptr Theme_impl::uncache_cursor(Cursor_cache & cache, const ustring & name, int size) {
    ustring key = str_format(name, '_', size);
    auto iter = cache.find(key);

    if (iter != cache.end()) {
        iter->second.tv = Timeval::now();
        return iter->second.cursor;
    }

    return nullptr;
}

Pixmap_cptr Theme_impl::find_pixmap(const ustring & names) {
    auto v = str_explode(names, ":;");

    // Trying to uncache first given variant.
    if (!v.empty()) {
        auto pixmap = uncache_pixmap(v[0]);
        if (pixmap) { return pixmap; }
    }

    for (const ustring & name: v) {
        ustring key = str_toupper(str_trim(name)), path;
        { std::unique_lock sl(mx_); auto i = pixmap_map_.find(key); if (i != pixmap_map_.end()) path = i->second; }

        if (!path.empty() && Pixmap_impl::signal_file_supported()(path)) {
            try {
                if (auto pixmap = Pixmap_impl::load_from_file(path)) {
                    cache_pixmap(pixmap, name);
                    return pixmap;
                }
            }

            catch (...) { break; }
        }
    }

    std::list<ustring> dirs;
    { std::unique_lock sl(mx_); dirs = pixmap_dirs_; }

    for (const ustring & dir: dirs) {
        try {
            for (const ustring & path: file_find(dir, "*")) {
                ustring base = str_toupper(str_trim(path_basename(path)));

                {
                    std::unique_lock lock(mx_);
                    auto i = pixmap_map_.find(base);
                    if (i == pixmap_map_.end()) { pixmap_map_[base] = path; }
                }

                for (const ustring & name: v) {
                    if (str_toupper(str_trim(name)) == base && tau::Pixmap_impl::signal_file_supported()(path)) {
                        try {
                            if (auto pixmap = Pixmap_impl::load_from_file(path)) {
                                cache_pixmap(pixmap, name);
                                return pixmap;
                            }
                        }

                        catch (exception & x) {
                            std::cerr << "** Theme_impl::find_pixmap(): " << x.what() << std::endl;
                            break;
                        }
                    }
                }
            }

            {
                std::unique_lock sl(mx_);
                auto i = std::find(pixmap_dirs_.begin(), pixmap_dirs_.end(), dir);
                if (i != pixmap_dirs_.end()) { pixmap_dirs_.erase(i); }
            }
        }

        catch (exception & x) {
            std::cerr << "** Theme_impl::find_pixmap(): " << x.what() << std::endl;
            break;
        }
    }

    // Trying to uncache all variants
    if (v.size() > 1) {
        for (const ustring & name: v) {
            auto pixmap = uncache_pixmap(name);
            if (pixmap) { return pixmap; }
        }
    }

    // Try to search into "picto-*".
    for (auto & name: v) {
        if (auto pixmap = find_picto(name, 0)) {
            cache_pixmap(pixmap, name);
            return pixmap;
        }
    }

    return nullptr;
}

int Theme_impl::find_icon_theme(const ustring & names) const {
    std::unique_lock lock(mx_);
    return find_icon_theme_nolock(names);
}

int Theme_impl::find_icon_theme_nolock(const ustring & names) const {
    auto v = str_explode(names, ":;");
    Icon_theme * p = icon_themes_.data();

    for (std::size_t n = 0; n != icon_themes_.size(); n++, p++) {
        if (str_similar(p->name, v)) {
            return n;
        }
    }

    return -1;
}

void Theme_impl::set_icon_theme(const ustring & names) {
    int save = iicon_, iicon = find_icon_theme(names);

    while (iicon < 0 && 0 != nicon_dirs_) {
        feed_icon_dir(names);
        iicon = find_icon_theme(names);
    }

    if (iicon >= 0 && iicon != save) {
        std::unique_lock ml(mmx_);
        iicon_ = iicon;
        icon_cache_.clear();
        for (auto & p: threads_) { p.second.event_icon_theme_changed->emit(); }
    }
}

std::vector<ustring> Theme_impl::list_icon_themes() const {
    while (0 != nicon_dirs_) { feed_icon_dir(); }
    ustring names; { std::unique_lock lock(mx_); names = icon_theme_names_; }
    return str_explode(names, ":;");
}

int Theme_impl::icon_pixels(int icon_size) const noexcept {
    return icon_size <= Icon::LARGEST && icon_size >= 0 ? icon_sizes_[icon_size] : std::max(0, icon_size);
}

Pixmap_ptr Theme_impl::find_icon_in_theme(int itheme, const std::vector<ustring> & unames, const ustring & context, std::set<ustring> & seen, int size) {
    auto & theme = icon_themes_[itheme]; // Don't need to lock here.

    if (!seen.contains(theme.name)) {
        seen.insert(theme.name);
        std::unique_lock sl(mx_);

        for (auto & name: unames) {
            int best = -1, dmin = INT_MAX, dindex = 0;

            for (auto & dir: theme.dirs) {
                if (!dir.scalable) {
                    if (!context.empty() && !str_similar(dir.context, context)) {
                        continue;
                    }

                    if (0 != dir.size && find_icon_in_dir(dir, name, context, -1)) {
                        int ds = dir.size >= size ? dir.size-size : size-dir.size;
                        if (ds < dmin) { dmin = ds, best = dindex; }
                    }
                }

                dindex++;
            }

            if (best >= 0) {
                if (auto pixmap = find_icon_in_dir(theme.dirs[best], name, context, size)) {
                    return pixmap;
                }
            }

            dindex = 0;

            for (auto & dir: theme.dirs) {
                if (dindex != best) {
                    if (auto pixmap = find_icon_in_dir(dir, name, context, size)) {
                        return pixmap;
                    }
                }

                dindex++;
            }
        }
    }

    std::vector<int> inherited; { std::unique_lock lock(mx_); inherited = theme.inherited; }

    for (int ii: inherited) {
        if (auto pixmap = find_icon_in_theme(ii, unames, context, seen, size)) { // recursive call.
            return pixmap;
        }
    }

    return nullptr;
}

Pixmap_cptr Theme_impl::find_icon(const ustring & names, int size, const ustring & context) {
    size = icon_pixels(size);
    std::vector<ustring> unames = str_explode(names, ":;");

    // Try to uncache only first icon.
    if (!unames.empty()) {
        if (auto pixmap = uncache_icon(unames[0], context, size)) {
            return pixmap;
        }
    }

    // Search in icon theme, if it set and in bound themes.
    std::forward_list<int> ts;
    if (ifallback_ >= 0) { ts.push_front(ifallback_); }
    ts.merge(bound_icon_themes_);
    if (iicon_ >= 0) { ts.push_front(iicon_); }
    std::set<ustring> seen;

    // Main search loop.
    for (int ii: ts) {
        // First try to search within given context.
        if (auto pixmap = find_icon_in_theme(ii, unames, context, seen, size)) {
            return pixmap;
        }

        // Now try any context.
        if (!context.empty()) {
            seen.clear();
            if (auto pixmap = find_icon_in_theme(ii, unames, ustring(), seen, size)) { return pixmap; }
        }
    }

    // Try to use hook.
    // Hook currently used on Windows.
    for (const ustring & name: str_explode(names, ":;")) {
        if (auto pixmap = icon_hook(str_trim(name), context, size)) {
            cache_icon(pixmap, str_toupper(str_trim(name)), context, size);
            return pixmap;
        }
    }

    // Try to uncache all given variants.
    if (unames.size() > 1) {
        for (const ustring & name: unames) {
            if (auto pixmap = uncache_icon(name, context, size)) {
                return pixmap;
            }
        }
    }

    // Give up, try for hardcoded pictos.
    for (auto & name: unames) {
        if (auto pixmap = find_picto(name, size)) {
            cache_icon(pixmap, name, context, size);
            return pixmap;
        }
    }

    // The very last chance...
    return find_pixmap(names);
}

Pixmap_ptr Theme_impl::find_picto(const ustring & name, int size) {
    for (std::size_t i = 0; i < std::size(pictos_); ++i) {
        if (str_similar(name, pictos_[i].name) && (!size || size == pictos_[i].size)) {
            try {
                if (auto pix = Pixmap_impl::create(reinterpret_cast<const uint8_t *>(pictos_[i].xpm), std::strlen(pictos_[i].xpm))) {
                    return pix;
                }
            }

            catch (bad_pixmap & x) {
                std::cerr << __func__ << ": " << x.what() << std::endl;
            }
        }
    }

    return nullptr;
}

Pixmap_ptr Theme_impl::get_icon(const ustring & names, int size, const ustring & context) {
    if (auto pixmap = find_icon(names, size, context)) { return pixmap->dup(); }
    size = icon_pixels(size);
    auto pix = Pixmap_impl::create(1, size);
    cache_icon(pix, str_explode(names, ":;").front(), context, size);
    return pix;
}

ustring Theme_impl::cursor_theme() const {
    if (-1 != icursor_) {
        std::unique_lock sl(mx_);
        return cursor_themes_[icursor_].name;
    }

    return ustring();
}

ustring Theme_impl::icon_theme() const {
    if (-1 != iicon_) {
        std::unique_lock sl(mx_);
        return icon_themes_[iicon_].name;
    }

    return ustring();
}

void Theme_impl::take_cursor_lookup_slot(slot<Cursor_ptr(ustring)> s) {
    update_this_thread();
    std::unique_lock ml(mmx_);
    auto i = threads_.find(std::this_thread::get_id());
    if (i != threads_.end()) { i->second.lookup = s; }
}

Theme_impl::Thread & Theme_impl::update_this_thread() {
    auto tid = std::this_thread::get_id();
    std::unique_lock lk(mmx_);
    auto i = threads_.find(tid);

    if (i == threads_.end()) {
        auto [ j, succeed ] = threads_.emplace(tid, Thread()); i = j;

        if (auto loop = Loop_impl::that_ptr(tid)) {
            loop->signal_quit().connect(bind_back(fun(this, &Theme_impl::on_loop_quit), loop.get()));

            if (!cleanup_loop_) {
                cleanup_loop_ = loop.get();
                loop->alarm(fun(this, &Theme_impl::sweep), 61678, true);
            }

            j->second.event_cursor_theme_changed = loop->event();
            j->second.event_icon_theme_changed = loop->event();
            j->second.event_conf = loop->event();
            j->second.event_conf->signal_ready().connect(fun(this, &Theme_impl::on_conf_event));
        }
    }

    if (i != threads_.end()) {
        update_fonts();
        auto & cnf = i->second.conf;
        for (auto & name: conf_.items()) { cnf.set(name, conf_.str(name)); }
    }

    return i->second;
}

void Theme_impl::on_loop_quit(Loop_impl * loop) {
    auto tid = std::this_thread::get_id();
    std::unique_lock lk(mmx_);
    threads_.erase(tid);

    if (cleanup_loop_ == loop) {
        cleanup_loop_ = nullptr;

        if (!threads_.empty()) {
            auto i = threads_.begin();
            Loop_ptr lptr = Loop_impl::that_ptr(i->first);

            if (lptr) {
                // Switching cleanup to the first existing loop.
                cleanup_loop_ = lptr.get();
                lptr->alarm(fun(this, &Theme_impl::sweep), 59321, true);
            }
        }
    }
}

signal<void()> & Theme_impl::signal_cursors_changed() {
    auto tid = std::this_thread::get_id();
    update_this_thread();
    std::unique_lock lk(mmx_);
    auto i = threads_.find(tid);
    if (i == threads_.end()) { throw internal_error("Theme_impl::signal_cursors_changed(): unable to setup thread"); }
    return i->second.event_cursor_theme_changed->signal_ready();
}

signal<void()> & Theme_impl::signal_icons_changed() {
    auto tid = std::this_thread::get_id();
    update_this_thread();
    std::unique_lock lk(mmx_);
    auto i = threads_.find(tid);
    if (i == threads_.end()) { throw internal_error("Theme_impl::signal_icons_changed(): unable to setup thread"); }
    return i->second.event_icon_theme_changed->signal_ready();
}

Master_action * Theme_impl::find_action(const std::string & name) {
    auto tid = std::this_thread::get_id();
    update_this_thread();
    std::unique_lock lk(mmx_);
    auto i = threads_.find(tid);
    if (i == threads_.end()) { throw internal_error("Theme_impl::find_action(): unable to setup thread"); }
    auto j = i->second.actions.find(name);
    if (j != i->second.actions.end()) { return &j->second; }
    return nullptr;
}

Conf & Theme_impl::this_conf() {
    auto & thr = update_this_thread();
    return thr.conf;
}

void Theme_impl::bind_icon_theme(const ustring & name) {
    std::unique_lock lock(mx_);
    int itheme = find_icon_theme_nolock(name);
    while (itheme < 0 && 0 != nicon_dirs_)  { feed_icon_dir(name); itheme = find_icon_theme_nolock(name); }

    if (itheme >= 0) {
        bound_icon_themes_.push_front(itheme);
        bound_icon_themes_.sort();
        bound_icon_themes_.unique();
    }
}

void Theme_impl::on_display_open(Display_impl *) {
    update_this_thread();
}

void Theme_impl::on_conf_set(std::string_view name, std::string_view value) {
    std::unique_lock lock(mmx_);

    for (auto & thr: threads_) {
        thr.second.conf_pipe.emplace_back(name, value);
        thr.second.event_conf->emit();
    }
}

void Theme_impl::on_conf_event() {
    auto & thr = update_this_thread();
    std::unique_lock lock(mmx_);

    while (!thr.conf_pipe.empty()) {
        auto [ name, val ] = thr.conf_pipe.front();
        thr.conf_pipe.pop_front();
        thr.conf.set(name, val);
    }
}

} // namespace tau

//END

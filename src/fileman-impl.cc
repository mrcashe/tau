// ----------------------------------------------------------------------------
// SPDX-License-Identifier: BSD-2-Clause
// Copyright © 2014-2024 Konstantin Shmelkov <mrcashe@gmail.com>.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
// THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
// PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
// CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
// EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
// PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
// OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
// WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
// OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
// EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// ----------------------------------------------------------------------------

#include <tau/exception.hh>
#include <tau/key-file.hh>
#include <tau/navigator.hh>
#include <tau/sys.hh>
#include <tau/painter.hh>
#include <button-impl.hh>
#include <check-impl.hh>
#include <cycle-text-impl.hh>
#include <display-impl.hh>
#include <entry-impl.hh>
#include <fileman-impl.hh>
#include <frame-impl.hh>
#include <icon-impl.hh>
#include <menubox-impl.hh>
#include <menu-item-impl.hh>
#include <roller-impl.hh>
#include <scroller-impl.hh>
#include <separator-impl.hh>
#include <slider-impl.hh>
#include <dialog-impl.hh>
#include <algorithm>
#include <iostream>

namespace {

const std::vector<tau::ustring> saveable_ { "bytes", "date", "hidden", "type", "backward" };

} // anonymous namespace

// ----------------------------------------------------------------------------
// ----------------------------------------------------------------------------

namespace tau {

Fileman_impl::Fileman_impl(Mode fm_mode, const ustring & uri):
    Twins_impl(Orientation::LEFT, 0.75),
    mode_(fm_mode),
    dir_creation_allowed_(Fileman::OPEN != fm_mode)
{
    navi_= std::make_shared<Navigator_impl>(uri);
    add_to_history(uri);
    navi_->signal_dir_changed().connect(fun(this, &Fileman_impl::on_dir_changed));
    navi_->signal_file_select().connect(bind_back(fun(this, &Fileman_impl::on_file_select), true));
    navi_->signal_file_unselect().connect(bind_back(fun(this, &Fileman_impl::on_file_select), false));
    navi_->signal_file_activate().connect(fun(this, &Fileman_impl::on_file_activate));
    navi_->signal_mouse_down().connect(fun(this, &Fileman_impl::on_navi_mouse_down), true);

    table_ = std::make_shared<Table_impl>(0, 3);
    table_->hint_margin(2, 2, 0, 0);
    table_->set_column_margin(1, 0, 3);
    table_->set_row_margin(2, 0, 3);
    insert_first(table_);

    // Path buttons.
    Roller_ptr roller = std::make_shared<Roller_impl>(Orientation::RIGHT);
    table_->put(roller, 0, -1, 4, 1, false, true);
    pathbox_ = std::make_shared<Box_impl>(Orientation::RIGHT);
    roller->insert(pathbox_);

    // Navigator inside of frame.
    navi_->hint_margin(2, 0, 2, 2);
    Frame_ptr frame = std::make_shared<Frame_impl>(Border::INSET);
    frame->insert(navi_);
    table_->put(frame, 0, 0, 3, 1, false, false);

    // File name or path entry.
    auto tp = std::make_shared<Label_impl>(lgettext("Name")+':', tau::Align::END);
    tp->hint_margin(3, 3, 0, 0);
    table_->put(tp, Align::END, Align::CENTER, 0, 1, 1, 1, true, true);
    entry_ = std::make_shared<Entry_impl>();
    table_->put(entry_, 1, 1, Fileman::BROWSE == mode_ ? 2 : 1, 1, false, true);
    entry_->signal_activate().connect(fun(this, &Fileman_impl::on_entry_activate));
    entry_->signal_changed().connect(fun(this, &Fileman_impl::on_entry_changed));
    entry_->signal_mouse_down().connect(fun(this, &Fileman_impl::on_entry_mouse_down));

    // Filters.
    tp = std::make_shared<Label_impl>(lgettext("Filter")+':', tau::Align::END);
    tp->hint_margin(3, 3, 0, 0);
    table_->put(tp, Align::END, Align::CENTER, 0, 2, 1, 1, true, true);
    filters_ = std::make_shared<Cycle_impl>();
    table_->put(filters_, 1, 2, Fileman::BROWSE == mode_ ? 2 : 1, 1, false, true);
    add_filter("*", "All Files");

    // Buttons "Open"/"Save" & "Cancel".
    if (Fileman::BROWSE != mode_) {
        action_apply_.set_label(Fileman::SAVE == mode_ ? lgettext("Save") : lgettext("Open"));
        Button_ptr button = std::make_shared<Button_impl>(action_apply_);
        table_->put(button, Align::FILL, Align::CENTER, 2, 1, 1, 1, true, true);
        button = std::make_shared<Button_impl>(action_cancel_);
        table_->put(button, Align::FILL, Align::CENTER, 2, 2, 1, 1, true, true);
    }

    auto tools = std::make_shared<Box_impl>(Orientation::DOWN, 4);
    tools->hint_margin(2, 2, 0, 0);
    table_->put(tools, 3, 0, 1, 1, true, false);

    tools->conf().boolean(Conf::BUTTON_LABEL) = false;
    up_button_ = std::make_shared<Button_impl>(action_updir_);
    tools->append(up_button_, true);
    tools->append(std::make_shared<Button_impl>(action_previous_), true);
    tools->append(std::make_shared<Button_impl>(action_next_), true);
    tools->append(std::make_shared<Separator_impl>(), true);
    tools->append(std::make_shared<Button_impl>(action_refresh_), true);
    tools->append(std::make_shared<Toggle_impl>(action_hidden_), true);
    tools->append(std::make_shared<Button_impl>(action_mkdir_), true);

    action_previous_.disable();
    action_next_.disable();
    action_apply_.disable();
    if (!dir_creation_allowed_) { action_mkdir_.disable(); action_mkdir_.hide(); }
    action_refresh_.connect(fun(navi_, &Navigator_impl::refresh));

    connect_action(action_cancel_);
    connect_action(action_next_);
    connect_action(action_previous_);
    connect_action(action_updir_);
    connect_action(action_apply_);
    connect_action(action_refresh_);
    connect_action(action_mkdir_);
    connect_action(action_hidden_);
    connect_action(zoom_in_ax_);
    connect_action(zoom_out_ax_);

    signal_display_in_.connect(fun(this, &Fileman_impl::on_display));
    signal_take_focus_.connect(fun(entry_, &Widget_impl::take_focus), true);

    focus_after(navi_.get(), entry_.get());
    focus_after(entry_.get(), filters_.get());
    focus_after(filters_.get(), navi_.get());
}

void Fileman_impl::on_display() {
    if (observer_visible_) {
        show_observer();
    }
}

ustring Fileman_impl::uri() const {
    return navi_->uri();
}

void Fileman_impl::set_uri(const ustring & uri) {
    navi_->set_uri(uri);
    add_to_history(uri);
}

ustring Fileman_impl::entry() const {
    return entry_->str();
}

std::vector<ustring> Fileman_impl::selection() const {
    std::vector<ustring> v(selection_.size());
    std::copy(selection_.begin(), selection_.end(), v.begin());
    return v;
}

// FIXME Change behaviour in future? When List_impl emits signal_activate_row(), it does not clear its selection.
void Fileman_impl::on_file_activate(const ustring & path) {
    selection_.insert(path_notdir(path));
    apply();
}

void Fileman_impl::entry_from_selection() {
    ustring s;

    for (auto & name: selection_) {
        if (navi_->has_option("dir_select") || navi_->has_option("dirs_only") || !file_is_dir(path_build(uri(), name))) {
            if (name.npos != name.find_first_of(Locale().blanks())) { s += '"'; s += name+"\" "; }
            else { s += name+' '; }
        }
    }

    entry_->assign(str_trimboth(s));
}

// From Navigator.
void Fileman_impl::on_file_select(const ustring & filename, bool yes) {
    if (yes) {
        if (navi_->has_option("multiple")) { selection_.insert(filename); }
        else { selection_.clear(), selection_.insert(filename); }
        ustring p = path_build(navi_->uri(), filename);
        enable_apply(!file_is_dir(p) || navi_->has_option("dir_select") || navi_->has_option("dirs_only"));
        entry_from_selection();
        entry_->move_to(entry_->str().size());
    }

    else {
        if (navi_->has_option("multiple")) { selection_.erase(filename); }
        entry_from_selection();
    }
}

void Fileman_impl::on_dir_changed(const ustring & path) {
    entry_->clear();
    selection_.clear();
    pathbox_->clear();
    enable_apply(false);
    auto v = path_explode(path);
    action_updir_.par_enable(v.size() > 1);

    for (auto i = v.begin(); i != v.end(); ) {
        Button_ptr button = std::make_shared<Button_impl>(*i);
        button->hint_min_size(14, 0);
        button->connect(bind_back(fun(this, &Fileman_impl::set_uri), path_implode({ v.begin(), ++i })));
        button->hide_relief();
        pathbox_->append(button, true);
        if (i-v.begin() > 1 && i != v.end()) { pathbox_->append(std::make_shared<Label_impl>(ustring(1, path_slash())), true); }
    }
}

void Fileman_impl::apply() {
    if (Fileman::SAVE == mode_ && !overwrite_allowed_) {
        ustring path = path_build(uri(), entry_->str());

        if (file_exists(path)) {
            if (auto dp = display()) {
                if (auto wip = toplevel()) {
                    Box_ptr box = std::make_shared<Box_impl>(Orientation::DOWN, 5);
                    box->align(Align::CENTER);
                    box->hint_margin(8);

                    auto tp = std::make_shared<Label_impl>(str_format(lgettext("File"), ' ', entry_->str(), ' ', lgettext("exists"), '.'));
                    box->append(tp, true);

                    tp = std::make_shared<Label_impl>(lgettext("Are your sure to overwrite it?"));
                    box->append(tp, true);

                    Box_ptr bbox = std::make_shared<Box_impl>(Orientation::RIGHT, 12);
                    bbox->align(Align::CENTER);
                    bbox->hint_margin(0, 0, 10, 0);
                    box->append(bbox, true);

                    Button_ptr yes = std::make_shared<Button_impl>(lgettext("Yes"), Icon::MEDIUM, "dialog-ok"_tu);
                    yes->connect(fun(yes, &Widget_impl::quit_dialog));
                    yes->connect(fun(this, &Widget_impl::quit_dialog));
                    yes->connect(fun(user_apply_ax_, &Action::exec));
                    bbox->append(yes, true);

                    Button_ptr no = std::make_shared<Button_impl>(lgettext("No"), Icon::MEDIUM, "dialog-cancel"_tu);
                    no->connect(fun(yes, &Widget_impl::quit_dialog));
                    bbox->append(no, true);

                    auto dlg = dp->create_dialog(wip);
                    dlg->set_title(lgettext("Confirm File Overwrite"));
                    dlg->insert(box);
                    dlg->show();
                    dlg->run();
                    return;
                }
            }
        }
    }

    quit_dialog();
    user_apply_ax_.exec();
}

void Fileman_impl::on_apply() {
    if (Fileman::OPEN == mode_) {
        if (!selection_.empty()) {
            apply();
        }
    }

    else {
        if (!entry_->empty()) {
            apply();
        }
    }
}

void Fileman_impl::on_cancel() {
    entry_->clear();
    selection_.clear();
    quit_dialog();
}

void Fileman_impl::on_entry_changed() {
    if (selection_.size() > 1) {
        enable_apply(true);
    }

    else {
        const ustring s = entry_->str();
        ustring p = path_build(uri(), s);

        if (("." == s || ".." == s || s.npos != s.find_first_of("/\\")) || s.empty()
            || (!(navi_->has_option("dir_select") || navi_->has_option("dirs_only")) && file_is_dir(p)))
        {
            enable_apply(false);
        }

        else {
            if (Fileman::OPEN == mode_) {
                if (!file_exists(p)) {
                    enable_apply(false);
                }

                else {
                    if (!navi_->has_option("multiple")) { selection_.insert(s); }
                    enable_apply(true);
                }
            }

            else {
                enable_apply(true);
            }
        }
    }
}

void Fileman_impl::on_entry_activate(const ustring & s) {
    if (!s.empty()) {
        if ("." == s || ".." == s || s.npos != s.find_first_of("/\\")) {
            ustring p = path_is_absolute(s) ? s : path_real(path_build(uri(), s));
            if (file_exists(p)) { entry_->clear(); set_uri(p); }
        }

        else {
            ustring p = path_build(uri(), s);

            if (file_is_dir(p)) {
                if (navi_->has_option("dir_select")) {
                    apply();
                }

                else {
                    entry_->clear();
                    set_uri(p);
                }
            }

            else {
                if (Fileman::OPEN == mode_) {
                    if (file_exists(p)) {
                        if (!navi_->has_option("multiple")) { selection_.insert(s); }
                        apply();
                    }
                }

                else if (Fileman::SAVE == mode_) {
                    apply();
                }
            }
        }
    }
}

bool Fileman_impl::on_entry_mouse_down(int mbt, int mm, const Point & pt) {
    if (MBT_LEFT == mbt) {
        if (!entry_->empty()) { entry_->move_to(entry_->str().size()); entry_->select_all(); }
        return true;
    }

    return false;
}

bool Fileman_impl::on_navi_mouse_down(int mbt, int mm, const Point & pt) {
    if (MBT_RIGHT == mbt) {
        popup_configure();
        return true;
    }

    return false;
}

void Fileman_impl::on_mkdir() {
    if (auto dp = display()) {
        if (auto wip = toplevel()) {
            Box_ptr box = std::make_shared<Box_impl>(Orientation::DOWN);
            box->hint_margin(8);
            box->align(Align::CENTER);

            auto tp = std::make_shared<Label_impl>(lgettext("Create new folder in")+':', Align::START, Align::CENTER);
            box->append(tp, true);

            tp = std::make_shared<Label_impl>(uri(), Align::START, Align::CENTER);
            box->append(tp, true);

            auto ent = std::make_shared<Entry_impl>();
            box->append(ent, true);
            ent->action_cancel().connect(fun(ent, &Widget_impl::quit_dialog));
            ent->signal_activate().connect(bind_back(fun(this, &Fileman_impl::on_mkdir_activate), ent.get()));
            ent->signal_changed().connect(bind_back(fun(this, &Fileman_impl::on_mkdir_changed), std::ref(*ent)));

            auto bbox = std::make_shared<Box_impl>(Orientation::RIGHT, Align::CENTER, 12);
            bbox->hint_margin(0, 0, 10, 4);
            box->append(bbox, true);

            mkdir_ok_button_ = std::make_shared<Button_impl>("OK", Icon::SMALL, "dialog-ok"_tu);
            bbox->append(mkdir_ok_button_, true);
            mkdir_ok_button_->connect(bind_back(fun(this, &Fileman_impl::on_mkdir_apply), ent.get()));
            mkdir_ok_button_->disable();

            auto cancel_button = std::make_shared<Button_impl>(lgettext("Cancel"), Icon::SMALL, "dialog-cancel"_tu);
            bbox->append(cancel_button, true);
            cancel_button->connect(fun(cancel_button, &Widget_impl::quit_dialog));

            auto dlg = dp->create_dialog(wip);
            dlg->set_title(lgettext("Create Folder"));
            dlg->insert(box);
            dlg->show();
            ent->take_focus();
            dlg->run();
            mkdir_ok_button_.reset();
        }
    }
}

void Fileman_impl::mkdir(const ustring & path) {
    if (!path.empty()) {
        try { file_mkdir(path); set_uri(path); entry_->clear(); }
        catch (tau::exception & x) { std::cerr << "** Fileman_impl::mkdir(): " << x.what() << std::endl; }
    }
}

void Fileman_impl::on_mkdir_apply(Entry_impl * entry) {
    const ustring path = path_build(uri(), entry->str());
    mkdir(path);
    entry->quit_dialog();
}

void Fileman_impl::on_mkdir_activate(const ustring & dirname, Entry_impl * entry) {
    const ustring path = path_build(uri(), dirname);
    mkdir(path);
    entry->quit_dialog();
}

void Fileman_impl::on_mkdir_changed(Entry_impl & entry) {
    const ustring s = entry.str();

    if (s.empty() || ".." == s || "." == s || "../" == s || "./" == s || "..\\" == s || ".\\" == s) {
        mkdir_ok_button_->disable();
    }

    else {
        ustring p = path_build(uri(), s);
        if (file_exists(p)) { mkdir_ok_button_->disable(); }
        else { mkdir_ok_button_->enable(); }
    }
}

void Fileman_impl::show_observer() {
    observer_visible_ = true;

    if (!has_second()) {
        auto frame = std::make_shared<Frame_impl>(Border::GROOVE);
        frame->hint_margin_right(3);
        insert_second(frame);

        auto vbox = std::make_shared<Box_impl>(Orientation::DOWN, 3);
        vbox->hint_margin(3);
        frame->insert(vbox);

        auto hbox = std::make_shared<Box_impl>(Orientation::RIGHT, 8);
        vbox->append(hbox, true);
        hbox->append(std::make_shared<Label_impl>(lgettext("Observer"), Align::START, Align::CENTER));

        auto button = std::make_shared<Button_impl>(std::make_shared<Icon_impl>("window-close", Icon::TINY));
        button->set_tooltip(lgettext("Close Observer"));
        button->hide_relief();
        button->connect(fun(this, &Fileman_impl::hide_observer));
        hbox->append(button, true);

        observer_ = std::make_shared<Observer_impl>(navi_->uri());
        observer_cx_ = observer_->signal_activate_uri().connect(fun(this, &Fileman_impl::on_observer_uri));
        navi_->signal_dir_changed().connect(fun(observer_, &Observer_impl::select_uri));
        vbox->append(observer_);
        focus_before(observer_, navi_);
    }
}

void Fileman_impl::hide_observer() {
    remove_second();
    observer_visible_ = false;
    observer_.reset();
}

void Fileman_impl::on_filter_select(Entry_impl * ep) {
    ustring s = ep->str();
    if (!s.empty()) { navi_->set_filter(s); }
}

void Fileman_impl::add_filter(const ustring & patterns, const ustring & title) {
    if (filters_->empty()) { navi_->set_filter(patterns); }
    Box_ptr box = std::make_shared<Box_impl>(); filters_->add(box);
    auto wp = std::make_shared<Entry_impl>(patterns);
    wp->signal_activate().connect(fun(navi_, &Navigator_impl::set_filter));
    wp->signal_select().connect(bind_back(fun(this, &Fileman_impl::on_filter_select), wp.get()));
    wp->action_activate().connect_after(fun(this, &Fileman_impl::disabled), true); // Block wp->drop_focus().
    box->append(wp);

    if (!title.empty()) {
        Label_ptr lp = std::make_shared<Label_impl>(" "_tu+title+' ', Align::END);
        lp->conf().redirect(Conf::BACKGROUND, Conf::SELECT_BACKGROUND);
        lp->conf().redirect(Conf::FOREGROUND, Conf::ALT_FOREGROUND);
        lp->hint_margin_left(2); box->append(lp, true);
        wp->signal_changed().connect(bind_back(fun(box, &Box_impl::remove), lp.get()));
    }
}

void Fileman_impl::popup_configure() {
    Menubox_ptr menu = std::make_shared<Menubox_impl>();
    Menubox_ptr sort_menu = std::make_shared<Menubox_impl>();
    Submenu_ptr sort_item = std::make_shared<Submenu_impl>(lgettext("Sort"), sort_menu);
    menu->append(sort_item);

    Check_menu_ptr sort_name = std::make_shared<Check_menu_impl>(lgettext("By Name"), Check::RSTYLE, "name" == navi_->sorted_by());
    sort_name->signal_check().connect(bind_back(fun(navi_, &Navigator_impl::sort_by), "name"));
    sort_menu->append(sort_name);

    Check_menu_ptr sort_size = std::make_shared<Check_menu_impl>(lgettext("By Size"), Check::RSTYLE, "bytes" == navi_->sorted_by());
    sort_size->join(sort_name);
    sort_size->signal_check().connect(bind_back(fun(navi_, &Navigator_impl::sort_by), "bytes"));
    sort_menu->append(sort_size);

    Check_menu_ptr sort_date = std::make_shared<Check_menu_impl>(lgettext("By Date"), Check::RSTYLE, "date" == navi_->sorted_by());
    sort_date->join(sort_name);
    sort_date->signal_check().connect(bind_back(fun(navi_, &Navigator_impl::sort_by), "date"));
    sort_menu->append(sort_date);

    Check_menu_ptr sort_unsorted = std::make_shared<Check_menu_impl>(lgettext("Keep Unsorted"), Check::RSTYLE, "" == navi_->sorted_by());
    sort_unsorted->join(sort_name);
    sort_unsorted->signal_check().connect(bind_back(fun(navi_, &Navigator_impl::sort_by), ""));
    sort_menu->append(sort_unsorted);

    Menubox_ptr columns_menu = std::make_shared<Menubox_impl>();
    Submenu_ptr columns_item = std::make_shared<Submenu_impl>(lgettext("Columns"), columns_menu);
    menu->append(columns_item);

    Check_menu_ptr columns_size = std::make_shared<Check_menu_impl>(lgettext("Show File Size"), Check::VSTYLE, has_option("bytes"));
    columns_size->signal_check().connect(bind_back(fun(this, &Fileman_impl::set_option), "bytes"));
    columns_size->signal_uncheck().connect(bind_back(fun(this, &Fileman_impl::reset_option), "bytes"));
    columns_menu->append(columns_size);

    Check_menu_ptr columns_date = std::make_shared<Check_menu_impl>(lgettext("Show File Date"), Check::VSTYLE, has_option("date"));
    columns_date->signal_check().connect(bind_back(fun(this, &Fileman_impl::set_option), "date"));
    columns_date->signal_uncheck().connect(bind_back(fun(this, &Fileman_impl::reset_option), "date"));
    columns_menu->append(columns_date);

    Check_menu_ptr columns_typ = std::make_shared<Check_menu_impl>(lgettext("Show File Type"), Check::VSTYLE, has_option("type"));
    columns_typ->signal_check().connect(bind_back(fun(this, &Fileman_impl::set_option), "type"));
    columns_typ->signal_uncheck().connect(bind_back(fun(this, &Fileman_impl::reset_option), "type"));
    columns_menu->append(columns_typ);

    sort_menu->append_separator();

    Check_menu_ptr sort_back = std::make_shared<Check_menu_impl>(lgettext("Sort Descent"), Check::VSTYLE, navi_->has_option("backward"));
    sort_back->signal_check().connect(bind_back(fun(this, &Fileman_impl::set_option), "backward"));
    sort_back->signal_uncheck().connect(bind_back(fun(this, &Fileman_impl::reset_option), "backward"));
    sort_menu->append(sort_back);

    menu->append_separator();

    Check_menu_ptr show_hidden = std::make_shared<Check_menu_impl>(lgettext("Show Hidden Files"), Check::VSTYLE, has_option("hidden"));
    show_hidden->signal_check().connect(bind_back(fun(this, &Fileman_impl::on_show_hidden), true));
    show_hidden->signal_uncheck().connect(bind_back(fun(this, &Fileman_impl::on_show_hidden), false));
    menu->append(show_hidden);

    Check_menu_ptr show_observer = std::make_shared<Check_menu_impl>(lgettext("Show Observer"), Check::VSTYLE, observer_visible_);
    show_observer->signal_check().connect(fun(this, &Fileman_impl::show_observer));
    show_observer->signal_uncheck().connect(fun(this, &Fileman_impl::hide_observer));
    menu->append(show_observer);

    if (auto wip = toplevel()) {
        try { menu->popup(wip, menu, to_parent(wip, where_mouse())); }
        catch (exception & x) { std::cerr << __func__ << ": " << x.what() << std::endl; }
    }
}

bool Fileman_impl::next_avail() const {
    return ihistory_ != history_.begin();
}

bool Fileman_impl::prev_avail() const {
    auto i = ihistory_;
    return i != history_.end() && ++i != history_.end();
}

void Fileman_impl::add_to_history(const ustring & path) {
    auto i = ihistory_;
    if (i != history_.begin()) { history_.erase(history_.begin(), --i); }
    history_.remove(path);
    history_.push_front(path);
    ihistory_ = history_.begin();
    refresh_history();
}

void Fileman_impl::next() {
    if (next_avail()) {
        navi_->set_uri(*--ihistory_);
        refresh_history();
    }
}

void Fileman_impl::prev() {
    if (prev_avail()) {
        navi_->set_uri(*++ihistory_);
        refresh_history();
    }
}

void Fileman_impl::refresh_history() {
    action_next_.par_enable(next_avail());
    action_previous_.par_enable(prev_avail());
}

void Fileman_impl::updir() {
    ustring p = path_dirname(navi_->uri());
    if (p != uri()) { set_uri(p); }
}

void Fileman_impl::on_show_hidden(bool show) {
    if (show) { set_option("hidden"); }
    else { reset_option("hidden"); }
}

void Fileman_impl::load_state(Key_file & kf, Key_section & sect) {
    auto v = kf.get_strings(sect, "options");
    for (auto & s: v) { set_option(s); }
    navi_->sort_by(kf.get_string(sect, "sort", "name"));
    set_ratio(kf.get_double(sect, "ratio", ratio()));
    navi_->conf().integer(Conf::ICON_SIZE) = kf.get_integer(sect, "navi_px", Icon::MEDIUM);
}

void Fileman_impl::save_state(Key_file & kf, Key_section & sect) {
    std::vector<ustring> v;

    for (auto & s: str_explode(navi_->options(), ':')) {
        if (str_similar(s, saveable_)) {
            v.push_back(s);
        }
    }

    kf.set_strings(sect, "options", v);
    kf.set_string(sect, "sort", navi_->sorted_by());
    kf.set_double(sect, "ratio", ratio());
    kf.set_integer(sect, "navi_px", navi_->conf().integer(Conf::ICON_SIZE));
}

void Fileman_impl::set_option(std::string_view opt) {
    ustring s(opt);
    if (str_similar("hidden", s)) { action_hidden_.set(true); }
    else if (str_similar("observer", s)) { show_observer(); }
    navi_->set_option(opt);
}

void Fileman_impl::reset_option(std::string_view opt) {
    ustring s(opt);
    if (str_similar("hidden", s)) { action_hidden_.set(false); }
    else if (str_similar("observer", s)) { hide_observer(); }
    navi_->reset_option(opt);
}

bool Fileman_impl::has_option(std::string_view opt) const noexcept {
    if (str_similar(opt, "observer")) { return observer_visible_; }
    return navi_->has_option(opt);
}

std::string Fileman_impl::options(char32_t sep) const {
    std::string s = navi_->options(sep);
    if (observer_visible_) { s += sep; s += "observer"; }
    return s;
}

void Fileman_impl::enable_apply(bool yes) {
    if (yes) {
        apply_timer_.stop();
        action_apply_.enable();
    }

    else {
        if (apply_timer_.empty()) { apply_timer_.connect(fun(action_apply_, &Action::disable)); }
        apply_timer_.start(131);
    }
}

void Fileman_impl::zoom_in() {
    navi_->action_zoom_in()();
}

void Fileman_impl::zoom_out() {
    navi_->action_zoom_out()();
}

void Fileman_impl::on_observer_uri(const ustring & uri) {
    observer_cx_.block();
    set_uri(uri);
    observer_cx_.unblock();
    navi_->take_focus();
}

} // namespace tau

//END

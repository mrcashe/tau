// ----------------------------------------------------------------------------
// SPDX-License-Identifier: BSD-2-Clause
// Copyright © 2014-2024 Konstantin Shmelkov <mrcashe@gmail.com>.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
// THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
// PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
// CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
// EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
// PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
// OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
// WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
// OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
// EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// ----------------------------------------------------------------------------

#include <sys-impl.hh>
#include <Unix/loop-unix.hh>
#include <Unix/sys-unix.hh>
#include <Unix/watcher-poll.hh>
#include <xcb/cursor-xcb.hh>
#include <xcb/pixmap-xcb.hh>
#include <unistd.h>
#include <sys/mount.h>
#include <sys/sysctl.h>
#include <sys/utsname.h>
#include <iostream>
#include <mutex>

namespace {

std::recursive_mutex    mx_;
tau::ustring            path_self_;

} // anonymous namespace

namespace tau {

ustring path_self() {
    ustring s; { std::unique_lock lk(mx_); s = path_self_; }
    if (!s.empty()) { return s; }

    int oid[4] = { CTL_KERN, KERN_PROC, KERN_PROC_PATHNAME, getpid() };
    size_t oldlen = 0;
    int result = sysctl(oid, 4, NULL, &oldlen, NULL, 0);

    if (0 == result && 0 != oldlen) {
        char buffer[oldlen+1];
        result = sysctl(oid, 4, buffer, &oldlen, NULL, 0);

        if (0 == result) {
            s = buffer;
            std::unique_lock lk(mx_);
            path_self_ = s;
            return s;
        }
    }

    std::cerr << "** " << __func__ << ": sysctl() failed: " << strerror(errno) << std::endl;
    return ustring();
}

// Declared in sys-impl.hh.
Mounts list_mounts() {
    Mounts mounts;

    struct statfs * st = nullptr;
    int ns = getmntinfo(&st, MNT_WAIT);

    for (int i = 0; i < ns; ++i) {
        Mount mnt;
        mnt.dev = st[i].f_mntfromname;
        mnt.mpoint = st[i].f_mntonname;
        mnt.removable = mnt.dev.starts_with("/dev/da");
        mounts.push_back(mnt);
    }

    return mounts;
}

// Declared in sys-impl.hh.
void boot_sys() {
    boot_unix();
    struct utsname un;

    if (0 == uname(&un)) {
        std::string s = str_getenv("UNAME_r", un.release);
        auto i = s.find_first_not_of("0123456789.");
        if (i != s.npos) { s.erase(i); }
        auto v = str_explode(s, '.');
        if (!v.empty()) { sysinfo_.osmajor = std::atoi(v[0].data()); }
        if (v.size() > 1) { sysinfo_.osminor = std::atoi(v[1].data()); }
    }

    u_int ncpu;
    std::size_t len = sizeof ncpu;
    sysctlbyname("hw.ncpu", &ncpu, &len , 0, 0);
    sysinfo_.cores = ncpu;
}

// static
Watcher_ptr Watcher_impl::create() {
    return std::make_shared<Watcher_poll>();
}

// static
Pixmap_ptr Pixmap_impl::create(int depth, const Size & sz) {
    return std::make_shared<Pixmap_xcb>(depth, sz);
}

// static
Cursor_ptr Cursor_impl::create() {
    return std::make_shared<Cursor_xcb>();
}

} // namespace tau

//END

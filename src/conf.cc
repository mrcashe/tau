// ----------------------------------------------------------------------------
// SPDX-License-Identifier: BSD-2-Clause
// Copyright © 2014-2024 Konstantin Shmelkov <mrcashe@gmail.com>.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
// THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
// PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
// CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
// EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
// PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
// OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
// WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
// OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
// EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// ----------------------------------------------------------------------------

#include <tau/brush.hh>
#include <tau/conf.hh>
#include <tau/exception.hh>
#include <tau/font.hh>
#include <tau/locale.hh>
#include <tau/string.hh>
#include <sys-impl.hh>
#include <algorithm>
#include <cinttypes>
#include <forward_list>
#include <iomanip>
#include <map>
#include <stack>
#include <unordered_map>
#include <iostream>

namespace tau {

using Names = std::map<int, std::string>;

const Names dict_ {
    { tau::Conf::FONT,                      "font"                          },
    { tau::Conf::MONOSPACE_FONT,            "mono_font"                     },
    { tau::Conf::EDIT_FONT,                 "edit_font"                     },
    { tau::Conf::TOOLTIP_FONT,              "tooltip_font"                  },
    { tau::Conf::MENU_FONT,                 "menu_font"                     },
    { tau::Conf::FOREGROUND,                "foreground"                    },
    { tau::Conf::ALT_FOREGROUND,            "alt_foreground"                },
    { tau::Conf::WEAK_FOREGROUND,           "weak_foreground"               },
    { tau::Conf::MENU_FOREGROUND,           "menu_foreground"               },
    { tau::Conf::SLIDER_FOREGROUND,         "slider_foreground"             },
    { tau::Conf::PROGRESS_FOREGROUND,       "progress_foreground"           },
    { tau::Conf::ACCEL_FOREGROUND,          "accel_foreground"              },
    { tau::Conf::BACKGROUND,                "background"                    },
    { tau::Conf::ALT_BACKGROUND,            "alt_background"                },
    { tau::Conf::PROGRESS_BACKGROUND,       "progress_background"           },
    { tau::Conf::WHITESPACE_BACKGROUND,     "whitespace_background"         },
    { tau::Conf::MENU_BACKGROUND,           "menu_background"               },
    { tau::Conf::SELECT_BACKGROUND,         "select_background"             },
    { tau::Conf::BUTTON_BACKGROUND,         "button_background"             },
    { tau::Conf::SLIDER_BACKGROUND,         "slider_background"             },
    { tau::Conf::INFO_BACKGROUND,           "info_background"               },
    { tau::Conf::HELP_BACKGROUND,           "help_background"               },
    { tau::Conf::WARNING_BACKGROUND,        "warning_background"            },
    { tau::Conf::ERROR_BACKGROUND,          "error_background"              },
    { tau::Conf::TOOLTIP_FOREGROUND,        "tooltip_foreground"            },
    { tau::Conf::HIGHLIGHT_BACKGROUND,      "highlight_background"          },
    { tau::Conf::RADIUS,                    "radius"                        },
    { tau::Conf::ICON_SIZE,                 "icon_size"                     },
    { tau::Conf::BUTTON_LABEL,              "button_label"                  }
};

static std::string dict(int i) {
    auto j = dict_.find(i);
    return j != dict_.end() ? j->second : "";
}

static std::string lower(std::string_view sv) {
    std::string res(sv);

    for (std::size_t i = 0; i < res.size(); ++i) {
        res[i] = std::tolower(res[i]);
    }

    return res;
}

} // anonymous namespace

// ----------------------------------------------------------------------------
// ----------------------------------------------------------------------------

namespace tau {

struct Conf_impl {
    struct Holder;          // A Conf_item holder.
    using  Holders          = std::forward_list<Holder *>;
    using  Map              = std::unordered_map<std::string, Holder>;
    using  Children         = std::vector<Conf_impl *>;
    using  Stack            = std::stack<ustring>;

    struct Holder {
        Conf_item           pub_;                       // Public part of the item, also keeps pointer to the parent Conf_impl.
        std::string_view    name_;
        ustring             value_;                     // Own value.
        ustring             fmt_;                       // Format string.
        ustring             pvalue_;                    // Parent value.
        bool                parent_         = false;    // This item was set from parent.
        Holders             redirections_;              // Holders redirected from this.
        Holders             sets_;                      // Holders set from this.
        Stack               stack_;                     // Value stack.
        Holder *            redirected_     = nullptr;  // This item redirected from that.
        Holder *            source_         = nullptr;  // This item set from that.
        signal<void()> *    signal_changed_ = nullptr;

        Holder(Conf_impl * cnf, const ustring & value=ustring()):
            pub_(cnf),
            value_(value)
        {
        }

        ~Holder() {
            if (signal_changed_) { delete signal_changed_; }
        }

        bool is_set() const noexcept {
            return !value_.empty() || !fmt_.empty() || redirected_ || source_;
        }

        ustring str() const {
            if (fmt_.empty()) { return value_.empty() ? pvalue_ : value_; }
            ustring res;
            std::size_t pos, len = fmt_.size(), end;

            for (pos = 0; pos < len; pos = end) {
                std::size_t fspec = fmt_.find(U'%', pos);
                end = std::min(len, fspec);
                res += fmt_.substr(pos, end-pos);

                if (fspec < len) {
                    switch (fmt_[fspec+1]) {
                        case U'%': res += U'%'; break;
                        case U'v': res += value_.empty() ? pvalue_ : value_; break;
                    }

                    end += 2;
                }
            }

            return res;
        }

        signal<void()> & signal_changed() {
            if (!signal_changed_) { signal_changed_ = new signal<void()>; }
            return *signal_changed_;
        }

        void changed() const {
            if (signal_changed_) { (*signal_changed_)(); }
        }
    };

    // ----------------------------------------------------------------------------
    // ----------------------------------------------------------------------------

    Conf_wptr           parent_;
    Map                 holders_;
    Children            children_;
    Conf_item           mt_ { this };
    signal<void(std::string_view, std::string_view)> signal_set_;

    // Default constructor.
    Conf_impl() = default;

    // Destructor.
    ~Conf_impl() {
       unparent();
    }

    // Copy constructor.
    Conf_impl(const Conf_impl & other) {
        holders_ = other.holders_;
        rebind();
    }

    // Copy operator.
    Conf_impl & operator=(const Conf_impl & other) {
        if (this != &other) {
            holders_ = other.holders_;
            rebind();
        }

        return *this;
    }

    // Move constructor.
    Conf_impl(Conf_impl && other):
        holders_(std::move(other.holders_))
    {
        rebind();
    }

    // Move operator.
    Conf_impl & operator=(Conf_impl && other) {
        holders_ = std::move(other.holders_);
        rebind();
        return *this;
    }

    void rebind() {
        for (auto & i: holders_) {
            i.second.pub_ = this;
        }
    }

    Holder * find(std::string_view name) noexcept {
        auto i = holders_.find(std::string(name));
        return i != holders_.end() ? &i->second : nullptr;
    }

    const Holder * find(std::string_view name) const noexcept {
        auto i = holders_.find(std::string(name));
        return i != holders_.end() ? &i->second : nullptr;
    }

    Holder * ifind(Conf_item * cnf) noexcept {
        auto i = std::find_if(holders_.begin(), holders_.end(), [cnf](auto & p) { return cnf == &p.second.pub_; } );
        return i != holders_.end() ? &i->second : nullptr;
    }

    const Holder * ifind(const Conf_item * cnf) const noexcept {
        auto i = std::find_if(holders_.begin(), holders_.end(), [cnf](auto & p) { return cnf == &p.second.pub_; } );
        return i != holders_.end() ? &i->second : nullptr;
    }

    Holder * new_holder(std::string_view name, const ustring & value=ustring()) {
        auto [i, ok] = holders_.emplace(name, this);
        i->second.name_ = i->first;
        iset(&i->second, value);
        return &i->second;
    }

    Holder * set(std::string_view name, const ustring & value=ustring()) {
        if (auto ip = find(name)) { iset(ip, value); return ip; }
        auto hol = new_holder(name, value);
        if (auto phol = hbot(name)) { pset(hol, phol->str()); }
        return hol;
    }

    Holder * push(std::string_view name, const ustring & value=ustring()) {
        if (auto ip = find(name)) {
            if (ip->is_set()) { ip->stack_.push(str_implode({ ip->value_, ip->fmt_ }, '|')); }
            iset(ip, value);
            return ip;
        }

        auto hol = new_holder(name, value);
        if (auto phol = hbot(name)) { pset(hol, phol->str()); }
        return hol;
    }

    Holder * forward(std::string_view name) {
        auto hol = new_holder(name);
        if (auto phol = hbot(name)) { pset(hol, phol->str()); }
        return hol;
    }

    void download() {
        for (auto & p: holders_) {
            if (!p.second.redirected_) {
                if (auto hol = hbot(p.first)) {
                    pset(&p.second, hol->str());
                    ustring val = p.second.str();
                    for (auto * h: p.second.sets_) { iset(h, val); }
                    for (auto * ri: hol->redirections_) { redirect(ri->name_, hol->name_, true); }
                }
            }
        }

        for (auto cnf: children_) { cnf->download(); }
    }

    void set_parent(Conf_ptr parent) {
        parent_ = parent;
        parent->children_.push_back(this);
        download();
    }

    void unparent() {
        if (auto pp = parent_.lock()) {
            for (auto & p: holders_) {
                auto redirs = p.second.redirections_;

                for (auto hol: redirs) {
                    if (hol->parent_) {
                        unredirect(hol->name_);
                    }
                }
            }

            std::erase(pp->children_, this);
            parent_.reset();
        }
    }

    Holder * hbot(std::string_view name) {
        for (auto pp = parent_.lock(); pp; pp = pp->parent_.lock()) {
            if (auto bot = pp->find(name)) {
                return bot;
            }
        }

        return nullptr;
    }

    void pset(Holder * hol, const ustring & pvalue) {
        for (auto ip: hol->redirections_) { pset(ip, pvalue); }
        ustring was = hol->str();
        hol->pvalue_ = pvalue;
        auto val = hol->str();

        if (val != was) {
            hol->changed();
            for (auto cnf: children_) { cnf->npset(hol->name_, val); }
        }
    }

    void npset(std::string_view name, const ustring & pvalue) {
        if (auto hol = find(name)) {
            if (!hol->redirected_) {
                pset(hol, pvalue);
            }
        }

        else {
            for (auto cnf: children_) {
                cnf->npset(name, pvalue);
            }
        }
    }

    void iset(Holder * hol, const ustring & value) {
        for (auto h: hol->redirections_) { iset(h, value); }
        ustring was = hol->str();
        if (value.find(U'%') < value.size()) { hol->fmt_ = value; }
        else { hol->value_ = value; }
        ustring val = hol->str();

        if (val != was) {
            hol->changed();
            signal_set_(hol->name_, val);
            for (auto cnf: children_) { cnf->npset(hol->name_, val); }
        }

        for (auto h: hol->sets_) { iset(h, val); }
    }

    void iunset(Holder * hol) {
        ustring val = hol->str();

        if (hol->stack_.empty()) {
            hol->value_.clear();
            hol->fmt_.clear();
        }

        else {
            auto v = str_explode(hol->stack_.top(), '|');
            hol->value_ = v.front();
            if (v.size() > 1) { hol->value_ = v.back(); }
        }

        if (hol->source_) {
            hol->source_->sets_.remove(hol);
            hol->source_ = nullptr;
        }

        if (hol->redirected_) {
            pset(hol, hol->redirected_->pvalue_);
            hol->fmt_ = hol->redirected_->fmt_;
            iset(hol, hol->redirected_->value_);
        }

        else if (hol->stack_.empty()) {
            if (auto bot = hbot(hol->name_)) {
                pset(hol, bot->str());
            }
        }

        else {
            hol->stack_.pop();
        }

        auto nval = hol->str();

        if (val != nval) {
            hol->changed();
            signal_set_(hol->name_, nval);
            for (auto * cnf: children_) { cnf->npset(hol->name_, nval); }
        }
    }

    void unset(const std::string & name) {
        if (auto hol = find(name)) {
            iunset(hol);
        }
    }

    Holder * set_from(const std::string & dest, const std::string & src, bool push) {
        auto j = set(dest), i = find(src);
        if (!i) { i = forward(src); }
        if (!j) { j = forward(dest); }
        j->source_ = i;
        if (push && !j->value_.empty()) { j->stack_.push(str_implode({ j->value_, j->fmt_ }, '|')); }
        i->sets_.push_front(j);
        iset(j, i->str());
        return j;
    }

    void redirect(std::string_view dest, std::string_view src, bool from_parent=false) {
        auto i = find(src), j = find(dest);
        if (!i) { i = forward(src); }
        if (!j) { j = forward(dest); }

        if (i != j && !j->redirected_ && i->redirected_ != j && !(from_parent && j->is_set())) {
            i->redirections_.remove(j);
            i->redirections_.push_front(j);
            j->redirected_ = i;
            j->parent_ = from_parent;

            if (j->value_.empty() && !j->source_) {
                j->fmt_ = i->fmt_;
                pset(j, i->pvalue_);
                iset(j, i->value_);
            }

            for (auto cnf: children_) { cnf->redirect(dest, src, true); }
        }
    }

    void unredirect(std::string_view name) {
        if (auto hol = find(name)) {
            if (hol->redirected_) {
                hol->redirected_->redirections_.remove(hol);
                hol->redirected_ = nullptr;
                hol->parent_ = false;
                if (auto bot = hbot(hol->name_)) { pset(hol, bot->str()); }
            }
        }

        for (auto cnf: children_) { cnf->unredirect(name); }
    }

    bool redirected(const std::string & name) const noexcept {
        if (auto ip = find(name)) { return nullptr != ip->redirected_; }
        return false;
    }

    std::string redirection(std::string_view name) const {
        if (auto ip = find(name)) {
            if (ip->redirected_) {
                return std::string(ip->redirected_->name_);
            }
        }

        return std::string();
    }
};

// ----------------------------------------------------------------------------
// ----------------------------------------------------------------------------

Conf_item::Conf_item(Conf_impl * cnf):
    cnf_(cnf)
{
}

ustring Conf_item::str() const {
    if (cnf_) {
        if (auto ip = cnf_->ifind(this)) {
            return ip->str();
        }
    }

    return ustring();
}

void Conf_item::set(const ustring & val) {
    if (cnf_) {
        if (auto ip = cnf_->ifind(this)) {
            cnf_->iset(ip, val);
        }
    }
}

ustring & Conf_item::format() {
    if (cnf_) {
        if (auto ip = cnf_->ifind(this)) {
            return ip->fmt_;
        }
    }

    throw internal_error("Conf_item::format(): have pure cnf_");
}

signal<void()> & Conf_item::signal_changed() {
    if (cnf_) {
        if (auto ip = cnf_->ifind(this)) {
            return ip->signal_changed();
        }
    }

    throw internal_error("Conf_item::signal_changed(): have pure cnf_");
}

void Conf_item::unset() {
    if (cnf_) {
        if (auto ip = cnf_->ifind(this)) {
            cnf_->iunset(ip);
        }
    }
}

bool Conf_item::is_set() const noexcept {
    if (cnf_) {
        if (auto ip = cnf_->ifind(this)) {
            return ip->is_set();
        }
    }

    return false;
}

// ----------------------------------------------------------------------------
// ----------------------------------------------------------------------------

Font_conf::Font_conf(Conf_item & cnf):
    cnf_(cnf)
{
}

ustring Font_conf::spec() const {
    auto v = Font::explode(cnf_.str());
    return Font::build(v);
}

void Font_conf::set(const ustring & spec) {
    cnf_.set(spec);
}

double Font_conf::size() const {
    return Font::points(cnf_.str());
}

void Font_conf::clear_size() {
    auto fv = str_explode(cnf_.format(), Locale().blanks());
    std::erase_if(fv, [](auto & s) { return s.npos == s.find_first_not_of("+-=.,0123456789eE"); });
    cnf_.format() = str_implode(fv, ' ');
}

double Font_conf::resize(double pts) {
    double res = Font::points(spec());

    if (pts > 0.0) {
        clear_size();
        ustring fmt = cnf_.format();
        if (fmt.npos == fmt.find(U'%')) { fmt = "%v"; }
        fmt = str_format(fmt, " =", pts);
        set(fmt);
    }

    return res;
}

double Font_conf::grow(double pts) {
    auto v = Font::explode(spec());
    double res = Font::points(v);

    if (!v.empty()) {
        clear_size();
        pts += res;
        set(Font::build(Font::family(v), Font::face(v), pts));
    }

    return res;
}

double Font_conf::enlarge(double pts) {
    double res = Font::points(spec());
    ustring fmt = cnf_.format();
    auto fv = str_explode(fmt, Locale().blanks());
    bool done;

    do {
        done = true;

        for (auto i = fv.begin(); i != fv.end(); ++i) {
            if (ustring::npos == i->find_first_not_of("=.,0123456789eE")) {
                done = false;
                fv.erase(i);
                break;
            }
        }
    } while (!done);

    fmt = str_implode(fv, ' ');
    if (ustring::npos == fmt.find(U'%')) { fmt = "%v"; }
    fmt = str_format(fmt, " ", std::showpos, pts);
    set(fmt);
    return res;
}

void Font_conf::add_face(const ustring & face_elements) {
    ustring fmt = cnf_.format();
    if (fmt.npos == fmt.find(U'%')) { fmt = "%v"; }
    fmt = str_format(fmt, ' ', face_elements);
    set(fmt);
}

void Font_conf::set_face(const ustring & face) {
    set(Font::set_face(spec(), face));
}

void Font_conf::make_bold() {
    ustring fmt = cnf_.format();
    if (fmt.npos == fmt.find(U'%')) { fmt = "%v"; }
    fmt = str_format(fmt, ' ', "Bold");
    set(fmt);
}

void Font_conf::make_italic() {
    ustring fmt = cnf_.format();
    if (fmt.npos == fmt.find(U'%')) { fmt = "%v"; }
    fmt = str_format(fmt, ' ', "Italic");
    set(fmt);
}

Font_conf::operator ustring() const {
    return spec();
}

void Font_conf::operator=(const ustring & s) {
    set(s);
}

// ----------------------------------------------------------------------------
// ----------------------------------------------------------------------------

Color_conf::Color_conf(Conf_item & si):
    cnf_(si)
{
}

Color Color_conf::value() const noexcept {
    return Color(cnf_.str());
}

void Color_conf::set(const Color & color) {
    cnf_.set(color.html());
}

Color_conf::operator Color() const noexcept {
    return value();
}

Color_conf & Color_conf::operator=(const Color & color) {
    set(color);
    return *this;
}

// ----------------------------------------------------------------------------
// ----------------------------------------------------------------------------

Brush_conf::Brush_conf(Conf_item & si):
    cnf_(si)
{
}

Brush Brush_conf::value() const noexcept {
    return Brush(cnf_.str());
}

void Brush_conf::set(const Brush & brush) {
    cnf_.set(brush.str());
}

Brush_conf::operator Brush() const noexcept {
    return value();
}

Brush_conf & Brush_conf::operator=(const Brush & brush) {
    set(brush);
    return *this;
}

// ----------------------------------------------------------------------------
// ----------------------------------------------------------------------------

Bool_conf::Bool_conf(Conf_item & si):
    cnf_(si)
{
}

bool Bool_conf::value(bool fallback) const noexcept {
    ustring s = cnf_.str();
    return !str_similar(s, bool_words_) && !str_is_numeric(s) ? fallback : str_boolean(s);
}

void Bool_conf::set(bool val) {
    cnf_.set(str_format(std::boolalpha, val));
}

Bool_conf::operator bool() const noexcept {
    return value();
}

Bool_conf & Bool_conf::operator=(bool val) {
    set(val);
    return *this;
}

// ----------------------------------------------------------------------------
// ----------------------------------------------------------------------------

Int_conf::Int_conf(Conf_item & si):
    cnf_(si)
{
}

intmax_t Int_conf::value(intmax_t fallback) const noexcept {
    ustring s = cnf_.str();
    const char * nptr = s.c_str();
    char * endptr;
    intmax_t val = std::strtoimax(nptr, &endptr, 0);
    return endptr != nptr ? val : fallback;
}

void Int_conf::set(intmax_t val) {
    cnf_.set(str_format(val));
}

Int_conf::operator intmax_t() const noexcept {
    return value();
}

Int_conf & Int_conf::operator=(intmax_t val) {
    set(val);
    return *this;
}

// ----------------------------------------------------------------------------
// ----------------------------------------------------------------------------

Conf::Conf():
    impl(std::make_shared<Conf_impl>())
{
}

Conf::~Conf() {}

Conf::Conf(const Conf & other) {
    impl = std::make_shared<Conf_impl>(*(other.impl));
}

Conf & Conf::operator=(const Conf & other) {
    if (this != &other) { impl = std::make_shared<Conf_impl>(*(other.impl)); }
    return *this;
}

void Conf::set_parent(Conf & parent) {
    impl->set_parent(parent.impl);
}

void Conf::unparent() {
    impl->unparent();
}

// TODO search within parents.
bool Conf::avail(std::string_view name) const noexcept {
    return provides(name);
}

// TODO search within parents.
bool Conf::avail(Item ename) const noexcept {
    return provides(ename);
}

bool Conf::provides(std::string_view name) const noexcept {
    return nullptr != impl->find(lower(name));
}

bool Conf::provides(Item ename) const noexcept {
    return nullptr != impl->find(dict(ename));
}

Conf_item & Conf::item(std::string_view name) {
    auto ln = lower(name);
    auto hol = impl->find(ln);
    if (!hol) { hol = impl->forward(ln); }
    return hol->pub_;
}

const Conf_item & Conf::item(std::string_view name) const {
    auto ln = lower(name);
    auto hol = impl->find(ln);
    if (!hol) { hol = impl->forward(ln); }
    return hol->pub_;
}

Conf_item & Conf::item(Item ename) {
    auto ln = dict(ename);
    auto hol = impl->find(ln);
    if (!hol) { hol = impl->forward(ln); }
    return hol->pub_;
}

const Conf_item & Conf::item(Item ename) const {
    auto ln = dict(ename);
    auto hol = impl->find(ln);
    if (!hol) { hol = impl->forward(ln); }
    return hol->pub_;
}

ustring Conf::str(std::string_view name) const {
    return item(name).str();
}

ustring Conf::str(Item ename) const {
    return item(ename).str();
}

Font_conf Conf::font(std::string_view name) {
    auto ln = lower(name);
    auto hol = impl->find(ln);
    if (!hol) { hol = impl->forward(ln); }
    return hol->pub_;
}

Font_conf Conf::font(Item ename) {
    auto ln = dict(ename);
    auto hol = impl->find(ln);
    if (!hol) { hol = impl->forward(ln); }
    return hol->pub_;
}

Font_conf Conf::font(std::string_view name) const {
    auto ln = lower(name);
    auto hol = impl->find(ln);
    return hol ? hol->pub_ : impl->mt_;;
}

Font_conf Conf::font(Item ename) const {
    auto ln = dict(ename);
    auto hol = impl->find(ln);
    return hol ? hol->pub_ : impl->mt_;;
}

Color_conf Conf::color(std::string_view name) {
    auto ln = lower(name);
    auto hol = impl->find(ln);
    if (!hol) { hol = impl->forward(ln); }
    return hol->pub_;
}

Color_conf Conf::color(Item ename) {
    auto ln = dict(ename);
    auto hol = impl->find(ln);
    if (!hol) { hol = impl->forward(ln); }
    return hol->pub_;
}

Color_conf Conf::color(std::string_view name) const {
    auto ln = lower(name);
    auto hol = impl->find(ln);
    return hol ? hol->pub_ : impl->mt_;;
}

Color_conf Conf::color(Item ename) const {
    auto ln = dict(ename);
    auto hol = impl->find(ln);
    return hol ? hol->pub_ : impl->mt_;;
}

Brush_conf Conf::brush(std::string_view name) {
    auto ln = lower(name);
    auto hol = impl->find(ln);
    if (!hol) { hol = impl->forward(ln); }
    return hol->pub_;
}

Brush_conf Conf::brush(Item ename) {
    auto ln = dict(ename);
    auto hol = impl->find(ln);
    if (!hol) { hol = impl->forward(ln); }
    return hol->pub_;
}

Brush_conf Conf::brush(std::string_view name) const {
    auto ln = lower(name);
    auto hol = impl->find(ln);
    return hol ? hol->pub_ : impl->mt_;;
}

Brush_conf Conf::brush(Item ename) const {
    auto ln = dict(ename);
    auto hol = impl->find(ln);
    return hol ? hol->pub_ : impl->mt_;;
}

Bool_conf Conf::boolean(std::string_view name) {
    auto ln = lower(name);
    auto hol = impl->find(ln);
    if (!hol) { hol = impl->forward(ln); }
    return hol->pub_;
}

Bool_conf Conf::boolean(Item ename) {
    auto ln = dict(ename);
    auto hol = impl->find(ln);
    if (!hol) { hol = impl->forward(ln); }
    return hol->pub_;
}

Bool_conf Conf::boolean(std::string_view name) const {
    auto ln = lower(name);
    auto hol = impl->find(ln);
    return hol ? hol->pub_ : impl->mt_;;
}

Bool_conf Conf::boolean(Item ename) const {
    auto ln = dict(ename);
    auto hol = impl->find(ln);
    return hol ? hol->pub_ : impl->mt_;;
}

Int_conf Conf::integer(std::string_view name) {
    auto ln = lower(name);
    auto hol = impl->find(ln);
    if (!hol) { hol = impl->forward(ln); }
    return hol->pub_;
}

Int_conf Conf::integer(Item ename) {
    auto ln = dict(ename);
    auto hol = impl->find(ln);
    if (!hol) { hol = impl->forward(ln); }
    return hol->pub_;
}

Int_conf Conf::integer(std::string_view name) const {
    auto ln = lower(name);
    auto hol = impl->find(ln);
    return hol ? hol->pub_ : impl->mt_;;
}

Int_conf Conf::integer(Item ename) const {
    auto ln = dict(ename);
    auto hol = impl->find(ln);
    return hol ? hol->pub_ : impl->mt_;;
}

bool Conf::is_set(std::string_view name) const noexcept {
    auto ip = impl->find(lower(name));
    return ip && ip->is_set();
}

bool Conf::is_set(Item ename) const noexcept {
    auto ip = impl->find(dict(ename));
    return ip && ip->is_set();
}

signal<void()> & Conf::signal_changed(std::string_view name) {
    auto ln = lower(name);
    auto hol = impl->find(ln);
    if (!hol) { hol = impl->forward(ln); }
    return hol->signal_changed();
}

signal<void()> & Conf::signal_changed(Item ename) {
    auto ln = dict(ename);
    auto hol = impl->find(ln);
    if (!hol) { hol = impl->forward(ln); }
    return hol->signal_changed();
}

Conf_item & Conf::set(std::string_view name, const ustring & value) {
    return impl->set(lower(name), value)->pub_;
}

Conf_item & Conf::set(Item ename, const ustring & value) {
    return impl->set(dict(ename), value)->pub_;
}

Conf_item & Conf::push(std::string_view name, const ustring & value) {
    return impl->push(lower(name), value)->pub_;
}

Conf_item & Conf::push(Item ename, const ustring & value) {
    return impl->push(dict(ename), value)->pub_;
}

Conf_item & Conf::set_from(std::string_view dest, std::string_view src, bool push) {
    return impl->set_from(lower(dest), lower(src), push)->pub_;
}

Conf_item & Conf::set_from(Item edest, std::string_view src, bool push) {
    return impl->set_from(dict(edest), lower(src), push)->pub_;
}

Conf_item & Conf::set_from(std::string_view dest, Item esrc, bool push) {
    return impl->set_from(lower(dest), dict(esrc), push)->pub_;
}

Conf_item & Conf::set_from(Item edest, Item esrc, bool push) {
    return impl->set_from(dict(edest), dict(esrc), push)->pub_;
}

void Conf::redirect(std::string_view dest, std::string_view src) {
    impl->redirect(lower(dest), lower(src));
}

void Conf::redirect(Item edest, std::string_view src) {
    impl->redirect(dict(edest), lower(src));
}

void Conf::redirect(std::string_view dest, Item esrc) {
    impl->redirect(lower(dest), dict(esrc));
}

void Conf::redirect(Item edest, Item esrc) {
    impl->redirect(dict(edest), dict(esrc));
}

void Conf::unredirect(std::string_view dest) {
    impl->unredirect(lower(dest));
}

void Conf::unredirect(Item edest) {
    impl->unredirect(dict(edest));
}

// TODO 0.8 remove
bool Conf::redirected(std::string_view name) const noexcept {
    return impl->redirected(lower(name));
}

// TODO 0.8 remove
bool Conf::redirected(Item ename) const noexcept {
    return impl->redirected(dict(ename));
}

// TODO 0.8 remove
std::string Conf::redirection(std::string_view name) const {
    return impl->redirection(lower(name));
}

// TODO 0.8 remove
std::string Conf::redirection(Item ename) const {
    return impl->redirection(dict(ename));
}

void Conf::unset(std::string_view name) {
    impl->unset(lower(name));
}

void Conf::unset(Item ename) {
    impl->unset(dict(ename));
}

std::vector<std::string> Conf::items() const {
    std::vector<std::string> v(impl->holders_.size());
    unsigned i = 0;
    for (auto & p: impl->holders_) { v[i++] = p.first; }
    return v;
}

signal<void(std::string_view, std::string_view)> & Conf::signal_set() {
    return impl->signal_set_;
}

} // namespace tau

//END

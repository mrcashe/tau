// ----------------------------------------------------------------------------
// SPDX-License-Identifier: BSD-2-Clause
// Copyright © 2014-2024 Konstantin Shmelkov <mrcashe@gmail.com>.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
// THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
// PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
// CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
// EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
// PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
// OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
// WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
// OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
// EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// ----------------------------------------------------------------------------

#ifndef __TAU_FONT_IMPL_HH__
#define __TAU_FONT_IMPL_HH__

#include <tau/exception.hh>
#include <glyph-impl.hh>
#include <iostream>

namespace tau {

struct Font_impl {
    Font_impl() = default;
    Font_impl(const Font_impl &) = delete;
    Font_impl & operator=(const Font_impl &) = delete;

    virtual ~Font_impl() {}

    void check_monospace() const {
        if (monospace_ < 0) {
            monospace_ = 0;
            char_width_ = 0;
            static const char32_t * ws = U"|W_min";
            bool same = true;

            try {
                Glyph_ptr g = glyph(*ws);
                int w = std::ceil(g->advance_.x()), nc = 0;
                for (const Contour & ctr: g->contours_) { nc += ctr.size(); }

                for (const char32_t * p = ws+1; *p; ++p) {
                    Glyph_ptr gg = glyph(*p);
                    int ww = std::ceil(gg->advance_.x()), nnc = 0;
                    if (w != ww) { return; }
                    for (const Contour & ctr: gg->contours_) { nnc += ctr.size(); }
                    if (nnc != nc) { same = false; }
                }

                if (!same) { char_width_ = w; ++monospace_; }
            }

            catch (exception & x) {
                std::cerr << __func__ << ": " << x.what() << std::endl;
            }
        }
    }

    bool monospace() const {
        check_monospace();
        return monospace_ > 0;
    }

    unsigned char_width() const {
        check_monospace();
        return char_width_;
    }

    unsigned height() const noexcept {
        return std::ceil(ascent_)+std::ceil(std::fabs(descent_));
    }

    /// @since 0.7.0
    int iascent() const noexcept {
        return std::ceil(ascent_);
    }

    /// @since 0.7.0
    int idescent() const noexcept {
        return std::ceil(std::fabs(descent_));
    }

    // Overridden by Font_unix.
    // Overridden by Font_win.
    virtual ustring psname() const = 0;

    // Overridden by Font_unix.
    // Overridden by Font_win.
    virtual Glyph_ptr glyph(char32_t wc) const = 0;

    // Overridden by Font_unix.
    // Overridden by Font_win.
    virtual Glyph_ptr master_glyph(char32_t wc) const = 0;

    mutable int         monospace_  = -1;    // -1 means need test, 0 means variable pitch, 1 means fixed pitch.
    mutable unsigned    char_width_ = 0;
    ustring             spec_;
    Vector              min_;
    Vector              max_;
    unsigned            dpi_        = 0;
    double              ascent_     = 0.0;
    double              descent_    = 0.0;
    double              linegap_    = 0.0;
};

} // namespace tau

#endif // __TAU_FONT_IMPL_HH__

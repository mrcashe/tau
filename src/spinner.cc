// ----------------------------------------------------------------------------
// SPDX-License-Identifier: BSD-2-Clause
// Copyright © 2014-2024 Konstantin Shmelkov <mrcashe@gmail.com>.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
// THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
// PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
// CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
// EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
// PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
// OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
// WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
// OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
// EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// ----------------------------------------------------------------------------

#include <tau/spinner.hh>
#include "spinner-impl.hh"

namespace tau {

#define SPINNER_IMPL (std::static_pointer_cast<Spinner_impl>(impl))

Spinner::Spinner():
    Widget(std::make_shared<Spinner_impl>())
{
}

Spinner::Spinner(Style spinner_style):
    Widget(std::make_shared<Spinner_impl>(spinner_style))
{
}

Spinner::Spinner(const Spinner & other):
    Widget(other.impl)
{
}

Spinner & Spinner::operator=(const Spinner & other) {
    Widget::operator=(other);
    return *this;
}

Spinner::Spinner(Spinner && other):
    Widget(other.impl)
{
}

Spinner & Spinner::operator=(Spinner && other) {
    Widget::operator=(other);
    return *this;
}

Spinner::Spinner(Widget_ptr wp):
    Widget(std::dynamic_pointer_cast<Spinner_impl>(wp))
{
}

Spinner & Spinner::operator=(Widget_ptr wp) {
    Widget::operator=(std::dynamic_pointer_cast<Spinner_impl>(wp));
    return *this;
}

void Spinner::spinner_style(Style spinner_style) {
    SPINNER_IMPL->spinner_style(spinner_style);
}

Spinner::Style Spinner::spinner_style() const noexcept {
    return SPINNER_IMPL->spinner_style();
}

void Spinner::start() {
    SPINNER_IMPL->start();
}

void Spinner::stop() {
    SPINNER_IMPL->stop();
}

bool Spinner::active() const noexcept {
    return SPINNER_IMPL->active();
}

} // namespace tau

//END

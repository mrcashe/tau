// ----------------------------------------------------------------------------
// SPDX-License-Identifier: BSD-2-Clause
// Copyright © 2014-2024 Konstantin Shmelkov <mrcashe@gmail.com>.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
// THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
// PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
// CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
// EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
// PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
// OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
// WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
// OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
// EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// ----------------------------------------------------------------------------

#include "glyph-impl.hh"
#include <cmath>

namespace tau {

Rect Glyph_impl::bounds() const {
    if (contours_.empty()) {
        Size adv(advance_);

        if (0 == adv.height() && 0 == adv.width()) {
            return Rect();
        }

        if (0 != adv.height() && 0 != adv.width()) {
            return Rect(adv);
        }

        if (0 != adv.width()) {
            return Rect(Size(adv.width(), 1));
        }

        return Rect(Size(1, adv.height()));
    }

    int xpmin = std::floor(min_.x()), xpmax = std::ceil(max_.x());
    int ypmin = std::floor(min_.y()), ypmax = std::ceil(max_.y());
    return Rect(Point(xpmin, ypmin), Point(xpmax, ypmax));
}

Glyph_ptr Glyph_impl::glyph(const Matrix & mat) {
    Glyph_ptr gl = std::make_shared<Glyph_impl>();
    gl->min_ = mat*(Vector(bbox_.left(), bbox_.top()));
    gl->max_ = mat*(Vector(bbox_.right(), bbox_.bottom()));
    gl->advance_ = mat*advance_;
    gl->bearing_ = mat*(Vector(bearing_.x(), bbox_.top()));

    for (auto & mctr: contours_) {
        Contour ctr(mctr);
        ctr.transform(mat);
        gl->contours_.push_back(ctr);
    }

    return gl;
}

void Glyph_impl::hint() {
}

} // namespace tau

//END

// ----------------------------------------------------------------------------
// SPDX-License-Identifier: BSD-2-Clause
// Copyright © 2014-2024 Konstantin Shmelkov <mrcashe@gmail.com>.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
// THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
// PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
// CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
// EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
// PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
// OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
// WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
// OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
// EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// ----------------------------------------------------------------------------

#include <tau/separator.hh>
#include <separator-impl.hh>

namespace tau {

#define SEPARATOR_IMPL (std::static_pointer_cast<Separator_impl>(impl))

Separator::Separator(Style separator_style):
    Widget(std::make_shared<Separator_impl>(separator_style))
{
}

Separator::Separator(const Separator & other):
    Widget(other.impl)
{
}

Separator & Separator::operator=(const Separator & other) {
    Widget::operator=(other);
    return *this;
}

Separator::Separator(Separator && other):
    Widget(other.impl)
{
}

Separator & Separator::operator=(Separator && other) {
    Widget::operator=(other);
    return *this;
}

Separator::Separator(Widget_ptr wp):
    Widget(std::dynamic_pointer_cast<Separator_impl>(wp))
{
}

Separator & Separator::operator=(Widget_ptr wp) {
    Widget::operator=(std::dynamic_pointer_cast<Separator_impl>(wp));
    return *this;
}

void Separator::set_separator_style(Style separator_style) {
    SEPARATOR_IMPL->set_separator_style(separator_style);
}

Separator::Style Separator::separator_style() const noexcept {
    return SEPARATOR_IMPL->separator_style();
}

} // namespace tau

//END

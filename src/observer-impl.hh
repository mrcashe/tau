// ----------------------------------------------------------------------------
// SPDX-License-Identifier: BSD-2-Clause
// Copyright © 2014-2024 Konstantin Shmelkov <mrcashe@gmail.com>.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
// THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
// PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
// CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
// EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
// PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
// OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
// WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
// OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
// EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// ----------------------------------------------------------------------------

/// @file observer-impl.hh The Observer_impl class declaration.
/// The implementation is in observer-impl.cc.
/// @since 0.7.0

#ifndef __TAU_OBSERVER_IMPL_HH__
#define __TAU_OBSERVER_IMPL_HH__

#include <icon-impl.hh>
#include <label-impl.hh>
#include <list-impl.hh>
#include <map>

namespace tau {

class Observer_impl: public List_impl {
public:

    Observer_impl(const ustring & uri=ustring());
    void select_uri(const ustring & uri);
    signal<void(const ustring &)> & signal_activate_uri() { return signal_activate_uri_; }

private:

    using Map = std::multimap<ustring, int>;

    std::vector<ustring>            removables_;
    Map                             map_;
    signal<void(const ustring &)>   signal_activate_uri_;

private:

    // Platform dependent: source at observer-unix.cc, observer-win.cc.
    void init();

    void init_user_dirs();
    void init_folder(const ustring & label, std::string_view icon_names, const ustring & uri);
    void on_watch(int flags, const ustring & mp);
    void on_activate(int y);
};

} // namespace tau

#endif // __TAU_OBSERVER_IMPL_HH__

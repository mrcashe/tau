// ----------------------------------------------------------------------------
// SPDX-License-Identifier: BSD-2-Clause
// Copyright © 2014-2024 Konstantin Shmelkov <mrcashe@gmail.com>.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
// THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
// PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
// CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
// EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
// PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
// OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
// WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
// OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
// EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// ----------------------------------------------------------------------------

#include <tau/navigator.hh>
#include <navigator-impl.hh>

namespace tau {

#define NAVIGATOR_IMPL (std::static_pointer_cast<Navigator_impl>(impl))

Navigator::Navigator(const ustring & path):
    Widget(std::make_shared<Navigator_impl>(path))
{
}

Navigator::Navigator(const Navigator & other):
    Widget(other.impl)
{
}

Navigator & Navigator::operator=(const Navigator & other) {
    Widget::operator=(other);
    return *this;
}

Navigator::Navigator(Navigator && other):
    Widget(other.impl)
{
}

Navigator & Navigator::operator=(Navigator && other) {
    Widget::operator=(other);
    return *this;
}

Navigator::Navigator(Widget_ptr wp):
    Widget(std::dynamic_pointer_cast<Navigator_impl>(wp))
{
}

Navigator & Navigator::operator=(Widget_ptr wp) {
    Widget::operator=(std::dynamic_pointer_cast<Navigator_impl>(wp));
    return *this;
}

void Navigator::set_uri(const ustring & uri) {
    NAVIGATOR_IMPL->set_uri(uri);
}

ustring Navigator::uri() const {
    return NAVIGATOR_IMPL->uri();
}

void Navigator::refresh() {
    NAVIGATOR_IMPL->refresh();
}

void Navigator::sort_by(const ustring & col) {
    NAVIGATOR_IMPL->sort_by(col);
}

ustring Navigator::sorted_by() const {
    return NAVIGATOR_IMPL->sorted_by();
}

void Navigator::set_option(std::string_view opt) {
    NAVIGATOR_IMPL->set_option(opt);
}

void Navigator::reset_option(std::string_view opt) {
    NAVIGATOR_IMPL->reset_option(opt);
}

bool Navigator::has_option(std::string_view opt) const {
    return NAVIGATOR_IMPL->has_option(opt);
}

std::string Navigator::options(char32_t sep) const {
    return NAVIGATOR_IMPL->options(sep);
}

void Navigator::set_filter(const ustring & patterns) {
    NAVIGATOR_IMPL->set_filter(patterns);
}

ustring Navigator::filter() const {
    return NAVIGATOR_IMPL->filter();
}

signal<void(const ustring &)> & Navigator::signal_file_select() {
    return NAVIGATOR_IMPL->signal_file_select();
}

signal<void(const ustring &)> & Navigator::signal_file_unselect() {
    return NAVIGATOR_IMPL->signal_file_unselect();
}

signal<void(const ustring &)> & Navigator::signal_file_activate() {
    return NAVIGATOR_IMPL->signal_file_activate();
}

signal<void(const ustring &)> & Navigator::signal_dir_changed() {
    return NAVIGATOR_IMPL->signal_dir_changed();
}

} // namespace tau

//END

// ----------------------------------------------------------------------------
// SPDX-License-Identifier: BSD-2-Clause
// Copyright © 2014-2024 Konstantin Shmelkov <mrcashe@gmail.com>.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
// THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
// PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
// CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
// EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
// PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
// OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
// WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
// OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
// EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// ----------------------------------------------------------------------------

#include <tau/string.hh>
#include <card-impl.hh>
#include <cycle-text-impl.hh>
#include <entry-impl.hh>
#include <iostream>

namespace tau {

Cycle_text_impl::Cycle_text_impl(Border bs):
    Cycle_impl(bs),
    align_(Align::CENTER)
{
    init();
}

Cycle_text_impl::Cycle_text_impl(Align align, Border bs):
    Cycle_impl(bs),
    align_(align)
{
    init();
}

Cycle_text_impl::Cycle_text_impl(const std::vector<ustring> & sv, Border bs):
    Cycle_impl(bs),
    align_(Align::CENTER)
{
    init();
    add(sv);
}

Cycle_text_impl::Cycle_text_impl(const std::vector<ustring> & sv, Align align, Border bs):
    Cycle_impl(bs),
    align_(align)
{
    init();
    add(sv);
}

void Cycle_text_impl::init() {
    signal_select_id().connect(fun(this, &Cycle_text_impl::on_select_id));
    card_->hint_margin(2, 2, 0, 0);
    action_edit_.connect(bind_void(fun(this, &Cycle_text_impl::edit)));
    connect_action(action_edit_, true);
    action_edit_.disable();
    signal_activate_edit_.connect(fun(this, &Cycle_text_impl::on_edit_activate));
    signal_cancel_edit_.connect(fun(this, &Cycle_text_impl::end_edit));
    signal_mouse_double_click().connect(fun(this, &Cycle_text_impl::on_mouse_double_click), true);
    signal_mouse_down().connect(fun(this, &Cycle_text_impl::on_mouse_down), true);
    signal_take_focus().connect(fun(this, &Cycle_text_impl::grab_focus), true);
}

void Cycle_text_impl::allow_edit() {
    if (!editable_) {
        editable_ = true;
        action_edit_.enable();
        for (auto wp: holders_) { wp->allow_edit(); }
    }
}

void Cycle_text_impl::disallow_edit() {
    if (editable_) {
        editable_ = false;
        action_edit_.disable();
        for (auto wp: holders_) { wp->disallow_edit(); }
    }
}

int Cycle_text_impl::add(const ustring & s, Align align, const ustring & tooltip) {
    std::u32string ws(s);

    if (signal_approve_(ws) && signal_validate_(ws)) {
        auto tp = std::make_shared<Entry_impl>(ws, align, Border::NONE);
        tp->set_tooltip(tooltip);
        if (!editable_) { tp->disallow_edit(); }
        tp->wrap(wrap_);
        int id = Cycle_impl::add(tp);
        holders_.emplace_front(tp.get());
        tp->signal_activate().connect(tau::bind_front(fun(signal_activate_text_), id));
        tp->signal_validate().connect(fun(signal_validate_));
        tp->signal_approve().connect(fun(signal_approve_));
        tp->signal_changed().connect(bind_back(fun(this, &Cycle_text_impl::on_changed), id, tp.get()));
        tp->signal_take_focus().connect(fun(this, &Cycle_text_impl::focusable), true);
        signal_new_text_(ws);
        return id;
    }

    return INT_MIN;
}

int Cycle_text_impl::add(const std::vector<ustring> & sv, Align align) {
    int first = INT_MIN;

    for (auto & s: sv) {
        int y = add(s, align);
        if (INT_MIN == y) { break; }
        if (INT_MIN == first) { first = y; }
    }

    return first;
}

// Overrides Cycle_impl.
void Cycle_text_impl::clear() {
    for (auto wp: holders_) { signal_remove_text_(wp->wstr()); }
    holders_.clear();
    Cycle_impl::clear();
}

int Cycle_text_impl::select(const ustring & s, bool fix) {
    int id = INT_MIN;

    if (!fixed()) {
        auto i = std::find_if(holders_.begin(), holders_.end(), [s](auto wp) { return s == wp->str(); });
        if (i != holders_.end()) { id = Cycle_impl::select(*i, fix); }
    }

    return id;
}

int Cycle_text_impl::select_similar(const ustring & s, bool fix) {
    int id = INT_MIN;

    if (!fixed()) {
        auto i = std::find_if(holders_.begin(), holders_.end(), [s](auto wp) { return str_similar(s, wp->str()); });
        if (i != holders_.end()) { id = Cycle_impl::select(*i, fix); }
    }

    return id;
}

int Cycle_text_impl::setup(const ustring & s, bool fix) {
    int id = INT_MIN;

    if (!fixed()) {
        auto i = std::find_if(holders_.begin(), holders_.end(), [s](auto wp) { return s == wp->str(); });
        if (i != holders_.end()) { id = Cycle_impl::setup(*i, fix); }
    }

    return id;
}

int Cycle_text_impl::setup_similar(const ustring & s, bool fix) {
    int id = INT_MIN;

    if (!fixed()) {
        auto i = std::find_if(holders_.begin(), holders_.end(), [s](auto wp) { return str_similar(s, wp->str()); });
        if (i != holders_.end()) { id = Cycle_impl::setup(*i, fix); }
    }

    return id;
}

std::size_t Cycle_text_impl::remove(const ustring & s) {
    std::size_t n = 0;
    auto i = std::find_if(holders_.begin(), holders_.end(), [s](auto wp) { return wp->str() == s; } );

    if (i != holders_.end()) {
        auto ws = (*i)->wstr();
        n = Cycle_impl::remove(*i);
        holders_.erase(i);
        signal_remove_text_(ws);
    }

    return n;
}

std::size_t Cycle_text_impl::remove(int id) {
    std::size_t n = 0;

    if (auto wp = dynamic_cast<Entry_impl *>(Cycle_impl::widget(id))) {
        signal_remove_text_(wp->wstr());
        holders_.remove(wp);
        n = Cycle_impl::remove(id);
    }

    return n;
}

ustring Cycle_text_impl::str(int id) const {
    auto wp = dynamic_cast<Entry_impl *>(Cycle_impl::widget(id));
    return wp ? wp->str() : ustring();
}

ustring Cycle_text_impl::str() const {
    auto ep = dynamic_cast<Entry_impl *>(card_->current());
    return ep ? ep->str() : ustring();
}

std::u32string Cycle_text_impl::wstr(int id) const {
    auto wp = dynamic_cast<Entry_impl *>(Cycle_impl::widget(id));
    return wp ? wp->wstr() : std::u32string();
}

std::u32string Cycle_text_impl::wstr() const {
    auto ep = dynamic_cast<Entry_impl *>(card_->current());
    return ep ? ep->wstr() : std::u32string();
}

std::vector<ustring> Cycle_text_impl::strings() const {
    std::vector<ustring> v(holders_.size());
    unsigned i = 0;
    for (auto wp: holders_) { v[i++] = wp->str(); }
    return v;
}

std::vector<std::u32string> Cycle_text_impl::wstrings() const {
    std::vector<std::u32string> v(holders_.size());
    unsigned i = 0;
    for (auto wp: holders_) { v[i++] = wp->wstr(); }
    return v;
}

Widget_ptr Cycle_text_impl::widget(const ustring & s) {
    const std::u32string ws(s);
    auto i = std::find_if(holders_.begin(), holders_.end(), [ws](auto wp) { return wp->wstr() == ws; });
    return i != holders_.end() ? compound()->chptr(*i) : nullptr;
}

Widget_cptr Cycle_text_impl::widget(const ustring & s) const {
    const std::u32string ws(s);
    auto i = std::find_if(holders_.begin(), holders_.end(), [ws](auto wp) { return wp->wstr() == ws; });
    return i != holders_.end() ? compound()->chptr(*i) : nullptr;
}

void Cycle_text_impl::text_align(Align align) {
    if (align_ != align) {
        align_ = align;
        for (auto wp: holders_) { wp->text_align(align); }
    }
}

void Cycle_text_impl::wrap(Wrap wm) {
    if (wrap_ != wm) {
        wrap_ = wm;
        for (auto wp: holders_) { wp->wrap(wm); }
    }
}

Widget_ptr Cycle_text_impl::edit() {
    if (!editor()) {
        if (auto cur = std::dynamic_pointer_cast<Entry_impl>(compound()->chptr(card_->current()))) {
            cur->allow_edit();

            // Focus successful.
            if (cur->grab_focus()) {
                action_edit_.disable();
                edit_activate_cx_ = cur->signal_activate().connect(tau::bind_front(fun(signal_activate_edit_), current()));
                edit_cancel_cx_ = cur->action_cancel().connect(bind_back(fun(this, &Cycle_text_impl::on_edit_cancel), current()));
                edit_focus_out_cx_ = cur->signal_focus_out().connect(bind_back(fun(this, &Cycle_text_impl::on_edit_cancel), current()));
                return cur;
            }

            // Failed to gain focus.
            else if (!editable_) {
                cur->disallow_edit();
            }
        }
    }

    return nullptr;
}

// Slot for signal_activate_edit_.
void Cycle_text_impl::on_edit_activate(int id, const std::u32string &) {
    end_edit(id);
}

// Slot for signal_cancel_edit_.
// Slot for entry->signal_focus_out().
void Cycle_text_impl::on_edit_cancel(int id) {
    signal_cancel_edit_(id);
    end_edit(id);
}

void Cycle_text_impl::end_edit(int id) {
    edit_activate_cx_.drop();
    edit_cancel_cx_.drop();
    edit_focus_out_cx_.drop();
    action_edit_.par_enable(editable_);
    if (!editable_) { for (auto wp: holders_) wp->disallow_edit(); }
}

Widget_ptr Cycle_text_impl::editor() {
    auto i = std::find_if(holders_.begin(), holders_.end(), [](auto wp) { return wp->focused(); });
    return i != holders_.end() ? compound()->chptr(*i) : nullptr;
}

Widget_cptr Cycle_text_impl::editor() const {
    auto i = std::find_if(holders_.begin(), holders_.end(), [](auto wp) { return wp->focused(); });
    return i != holders_.end() ? compound()->chptr(*i) : nullptr;
}

void Cycle_text_impl::on_select_id(int id) {
    if (auto wp = dynamic_cast<Entry_impl *>(Cycle_impl::widget(id))) {
        signal_select_text_(wp->wstr());
    }
}

void Cycle_text_impl::on_changed(int id, const Entry_impl * ep) {
    signal_text_changed_(id, ep->wstr());
}

bool Cycle_text_impl::on_mouse_down(int mbt, int mm, const Point & pt) {
    return MBT_LEFT == mbt && !focused() && grab_focus();
}

bool Cycle_text_impl::on_mouse_double_click(int mbt, int mm, const Point & pt) {
    return MBT_LEFT == mbt && edit();
}

} // namespace tau

//END

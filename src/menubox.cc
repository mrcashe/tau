// ----------------------------------------------------------------------------
// SPDX-License-Identifier: BSD-2-Clause
// Copyright © 2014-2024 Konstantin Shmelkov <mrcashe@gmail.com>.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
// THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
// PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
// CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
// EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
// PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
// OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
// WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
// OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
// EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// ----------------------------------------------------------------------------

#include <tau/menu.hh>
#include <tau/toplevel.hh>
#include <menubox-impl.hh>
#include <toplevel-impl.hh>

namespace tau {

#define MENUBOX_IMPL (std::static_pointer_cast<Menubox_impl>(impl))

Menubox::Menubox():
    Menu(std::make_shared<Menubox_impl>())
{
}

Menubox::Menubox(const Menubox & other):
    Menu(other.impl)
{
}

Menubox & Menubox::operator=(const Menubox & other) {
    Menu::operator=(other);
    return *this;
}

Menubox::Menubox(Menubox && other):
    Menu(other.impl)
{
}

Menubox & Menubox::operator=(Menubox && other) {
    Menu::operator=(other);
    return *this;
}

Menubox::Menubox(Widget_ptr wp):
    Menu(std::dynamic_pointer_cast<Menubox_impl>(wp))
{
}

Menubox & Menubox::operator=(Widget_ptr wp) {
    Menu::operator=(std::dynamic_pointer_cast<Menubox_impl>(wp));
    return *this;
}

Widget_ptr Menubox::popup() {
    return MENUBOX_IMPL->popup(ptr(), nullptr);
}

Widget_ptr Menubox::popup(Toplevel & root) {
    return MENUBOX_IMPL->popup(std::static_pointer_cast<Window_impl>(root.ptr()).get(), ptr(), nullptr);
}

Widget_ptr Menubox::popup(Toplevel & root, Gravity gravity) {
    return MENUBOX_IMPL->popup(std::static_pointer_cast<Window_impl>(root.ptr()).get(), ptr(), gravity, nullptr);
}

Widget_ptr Menubox::popup(Toplevel & root, const Point & origin) {
    return MENUBOX_IMPL->popup(std::static_pointer_cast<Window_impl>(root.ptr()).get(), ptr(), origin, nullptr);
}

Widget_ptr Menubox::popup(Toplevel & root, const Point & origin, Gravity gravity) {
    return MENUBOX_IMPL->popup(std::static_pointer_cast<Window_impl>(root.ptr()).get(), ptr(), origin, gravity, nullptr);
}

} // namespace tau

//END

// ----------------------------------------------------------------------------
// SPDX-License-Identifier: BSD-2-Clause
// Copyright © 2014-2024 Konstantin Shmelkov <mrcashe@gmail.com>.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
// THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
// PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
// CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
// EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
// PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
// OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
// WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
// OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
// EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// ----------------------------------------------------------------------------

#include <tau/signal.hh>
#include <sys-impl.hh>
#include <iostream>

namespace tau {

trackable::trackable() {
    ++trackables_;
}

trackable::trackable(const trackable & src) {
    ++trackables_;
}

trackable::trackable(trackable && src) {
    ++trackables_;
}

trackable::~trackable() {
    --trackables_;
    tau::tracks_ -= tracks_.size();
    for (auto sp: tracks_) { sp->reset(); sp->disconnect(); }
}

void trackable::track(slot_ptr sp) const {
    ++tau::tracks_;
    tracks_.emplace_back(sp);
}

void trackable::untrack(slot_ptr sp) const {
    tau::tracks_ -= std::erase(tracks_, sp);
}

trackable & trackable::operator=(const trackable & src) {
    return *this;
}

trackable & trackable::operator=(trackable && src) {
    return *this;
}

// ----------------------------------------------------------------------------
// ----------------------------------------------------------------------------

signal_base::signal_base() {
    ++signals_;
}

signal_base::~signal_base() {
    --signals_;
}

connection signal_base::link(slot_base & slot) {
    slot.link(this);
    return slot.cx();
}

// ----------------------------------------------------------------------------
// ----------------------------------------------------------------------------

slot_base::slot_base() {
    ++slots_;
}

slot_base::~slot_base() {
    --slots_;
    untrack();
    if (impl_) { impl_->link(nullptr); }
}

void slot_base::untrack() {
    if (impl_) {
        if (impl_->target()) { impl_->target()->untrack(impl_); }
        impl_->reset();
    }
}

void slot_base::disconnect() {
    untrack();

    if (auto s = signal_) {
        signal_ = nullptr;
        s->erase(this);
    }
}

void slot_base::link(signal_base * signal) noexcept {
    signal_ = signal;
}

connection slot_base::cx() {
    return connection(impl_);
}

void slot_base::track() {
    if (impl_ && impl_->target()) {
        impl_->target()->track(impl_);
    }
}

bool slot_base::empty() const noexcept {
    return !impl_ || impl_->empty();
}

void slot_base::reset() {
    if (impl_) {
        impl_->reset();
    }
}

trackable * slot_base::target() noexcept {
    return impl_ ? impl_->target() : nullptr;
}

// ----------------------------------------------------------------------------
// ----------------------------------------------------------------------------

connection::connection(bool autodrop):
    autodrop_(autodrop)
{
}

connection::~connection() {
    if (autodrop_) { drop(); }
}

connection::connection(const connection & other):
    slot_(other.slot_),
    autodrop_(other.autodrop_)
{
}

connection::connection(connection && other):
    slot_(other.slot_),
    autodrop_(other.autodrop_)
{
    other.slot_.reset();
}

connection::connection(slot_ptr slot):
    slot_(slot)
{
}

connection & connection::operator=(const connection & other) {
    if (this != &other) {
        if (autodrop_) { drop(); }
        slot_ = other.slot_;
        autodrop_ = autodrop_ || other.autodrop_;
    }

    return *this;
}

connection & connection::operator=(connection && other) {
    if (autodrop_) { drop(); }
    slot_ = other.slot_;
    other.slot_.reset();
    autodrop_ = autodrop_ || other.autodrop_;
    // other.autodrop_ = false;
    return *this;
}

void connection::drop() {
    if (slot_) {
        slot_->disconnect();
        slot_.reset();
    }
}

void connection::set_autodrop() noexcept {
    autodrop_ = true;
}

void connection::unset_autodrop() noexcept {
    autodrop_ = false;
}

bool connection::autodrop() const noexcept {
    return autodrop_;
}

bool connection::blocked() const noexcept {
    return slot_ && slot_->blocked();
}

void connection::block() noexcept {
    if (slot_) {
        slot_->block();
    }
}

void connection::unblock() noexcept {
    if (slot_) {
        slot_->unblock();
    }
}

bool connection::empty() const noexcept {
    return !slot_ || slot_->empty();
}

// ----------------------------------------------------------------------------
// ----------------------------------------------------------------------------

slot_impl::slot_impl(slot_base * base):
    base_(base)
{
}

slot_impl::~slot_impl() {
}

bool slot_impl::blocked() const noexcept {
    return 0 != blocked_;
}

void slot_impl::block() noexcept {
    ++blocked_;
}

void slot_impl::unblock() noexcept {
    if (0 != blocked_) {
        --blocked_;
    }
}

// Called by owning slot.
void slot_impl::link(slot_base * base) noexcept {
    base_ = base;
}

// Called by connection and trackable.
void slot_impl::disconnect() {
    if (auto base = base_) {
        base_ = nullptr;
        base->disconnect();
    }
}

} // namespace tau

//END

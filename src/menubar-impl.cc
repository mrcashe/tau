// ----------------------------------------------------------------------------
// SPDX-License-Identifier: BSD-2-Clause
// Copyright © 2014-2024 Konstantin Shmelkov <mrcashe@gmail.com>.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
// THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
// PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
// CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
// EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
// PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
// OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
// WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
// OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
// EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// ----------------------------------------------------------------------------

#include <display-impl.hh>
#include <menu-item-impl.hh>
#include <menubar-impl.hh>
#include <iostream>             // std::cerr

namespace tau {

Menubar_impl::Menubar_impl():
    Menu_impl(Orientation::RIGHT)
{
    auto box = std::make_shared<Box_impl>(Orientation::RIGHT, 12);
    insert(box);
    box_ = box.get();
    connect_action(action_left_);
    connect_action(action_right_);
    signal_mouse_down().connect(fun(this, &Menubar_impl::on_mouse_down));
    signal_mouse_leave().connect(fun(this, &Menubar_impl::on_mouse_leave));
    signal_quit_.connect(fun(this, &Menubar_impl::unselect_current));
    signal_quit_.connect(fun(this, &Menubar_impl::drop_focus));
    signal_quit_.connect(bind_void(fun(this, &Menubar_impl::end_modal)));
}

void Menubar_impl::on_left() {
    select_prev();
    open_current();
}

void Menubar_impl::on_right() {
    select_next();
    open_current();
}

// "ESCAPE" came from the child menu.
void Menubar_impl::child_menu_cancel() {
    reset_submenu();
    quit_menu();
}

// "LEFT" came from the child menu.
void Menubar_impl::child_menu_left() {
    reset_submenu();
    select_prev();
    open_current();
    if (!submenu_) { grab_modal(); }
}

// "RIGHT" came from the child menu.
void Menubar_impl::child_menu_right() {
    reset_submenu();
    select_next();
    open_current();
    if (!submenu_) { grab_modal(); }
}

// Overrides pure Menu_impl.
void Menubar_impl::mark_item(Widget_impl * wp, bool select) {
    if (select) {
        wp->signal_select()();
        wp->conf().set_from(Conf::BACKGROUND, Conf::SELECT_BACKGROUND);
    }

    else {
        wp->signal_unselect()();
        wp->conf().unset(Conf::BACKGROUND);
    }
}

bool Menubar_impl::on_mouse_down(int mbt, int mm, const Point & pt) {
    if (!hover_item(pt)) {
        ungrab_mouse();
        unselect_current();
        if (grabs_modal()) { end_modal(), quit_menu(); }
        return true;
    }

    return false;
}

void Menubar_impl::on_mouse_leave() {
    if (!submenu_) {
        unselect_current();

        if (grabs_modal()) {
            ungrab_mouse();
            end_modal();
            quit_menu();
        }
    }
}

// Overrides pure Menu_impl.
void Menubar_impl::append(Widget_ptr wp, bool) {
    box_->append(wp, true);
    add(wp);
}

// Overrides pure Menu_impl.
void Menubar_impl::prepend(Widget_ptr wp, bool) {
    box_->prepend(wp, true);
    add(wp);
}

// Overrides pure Menu_impl.
void Menubar_impl::insert_before(Widget_ptr wp, const Widget_impl * other, bool) {
    box_->insert_before(wp, other, true);
    add(wp);
}

// Overrides pure Menu_impl.
void Menubar_impl::insert_after(Widget_ptr wp, const Widget_impl * other, bool) {
    box_->insert_after(wp, other, true);
    add(wp);
}

// Overrides Menu_impl.
void Menubar_impl::remove(Widget_impl * wp) {
    box_->remove(wp);
    Menu_impl::remove(wp);
}

// Overrides Menu_impl.
void Menubar_impl::clear() {
    box_->clear();
    Menu_impl::clear();
}

bool Menubar_impl::activate() {
    if (!grab_modal()) {
        std::cerr << "** Menubar::activate(): failed to gain modal focus" << std::endl;
        return false;
    }

    grab_mouse();
    select_item(current_item());
    open_current();
    return true;
}

} // namespace tau

//END

// ----------------------------------------------------------------------------
// SPDX-License-Identifier: BSD-2-Clause
// Copyright © 2014-2024 Konstantin Shmelkov <mrcashe@gmail.com>.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
// THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
// PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
// CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
// EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
// PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
// OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
// WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
// OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
// EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// ----------------------------------------------------------------------------

#include <tau/list.hh>
#include <list-text-impl.hh>

#define LIST_IMPL (std::static_pointer_cast<List_impl>(impl))
#define LIST_TEXT_IMPL (std::static_pointer_cast<List_text_impl>(impl))

namespace tau {

List_text::List_text(Align align):
    List_base(std::make_shared<List_text_impl>(align))
{
}

List_text::List_text(unsigned spacing, Align align):
    List_base(std::make_shared<List_text_impl>(spacing, align))
{
}

List_text::List_text(unsigned xspacing, unsigned yspacing, Align align):
    List_base(std::make_shared<List_text_impl>(xspacing, yspacing, align))
{
}

List_text::List_text(const std::vector<ustring> & sv, Align align):
    List_base(std::make_shared<List_text_impl>(sv, align))
{
}

List_text::List_text(const std::vector<ustring> & sv, unsigned spacing, Align align):
    List_base(std::make_shared<List_text_impl>(sv, spacing, align))
{
}

List_text::List_text(const std::vector<ustring> & sv, unsigned xspacing, unsigned yspacing, Align align):
    List_base(std::make_shared<List_text_impl>(sv, xspacing, yspacing, align))
{
}

List_text::List_text(const List_text & other):
    List_base(other.impl)
{
}

List_text & List_text::operator=(const List_text & other) {
    List_base::operator=(other);
    return *this;
}

List_text::List_text(List_text && other):
    List_base(other.impl)
{
}

List_text & List_text::operator=(List_text && other) {
    List_base::operator=(other);
    return *this;
}

List_text::List_text(Widget_ptr wp):
    List_base(std::dynamic_pointer_cast<List_text_impl>(wp))
{
}

List_text & List_text::operator=(Widget_ptr wp) {
    List_base::operator=(std::dynamic_pointer_cast<List_text_impl>(wp));
    return *this;
}

int List_text::append(const ustring & str, bool shrink) {
    return LIST_TEXT_IMPL->append(str, shrink);
}

int List_text::append(const ustring & str, Align align, bool shrink) {
    return LIST_TEXT_IMPL->append(str, align, shrink);
}

int List_text::append(const std::vector<ustring> & sv, bool shrink) {
    return LIST_TEXT_IMPL->append(sv, shrink);
}

int List_text::append(const std::vector<ustring> & sv, Align align, bool shrink) {
    return LIST_TEXT_IMPL->append(sv, align, shrink);
}

int List_text::prepend(const ustring & str, bool shrink) {
    return LIST_TEXT_IMPL->prepend(str, shrink);
}

int List_text::prepend(const ustring & str, Align align, bool shrink) {
    return LIST_TEXT_IMPL->prepend(str, align, shrink);
}

int List_text::prepend(const std::vector<ustring> & sv, bool shrink) {
    return LIST_TEXT_IMPL->prepend(sv, shrink);
}

int List_text::prepend(const std::vector<ustring> & sv, Align align, bool shrink) {
    return LIST_TEXT_IMPL->prepend(sv, align, shrink);
}

int List_text::insert(const ustring & str, int row, bool shrink) {
    return LIST_TEXT_IMPL->insert(str, row, shrink);
}

int List_text::insert(const ustring & str, int row, Align align, bool shrink) {
    return LIST_TEXT_IMPL->insert(str, row, align, shrink);
}

int List_text::insert(const std::vector<ustring> & sv, int row, bool shrink) {
    return LIST_TEXT_IMPL->insert(sv, row, shrink);
}

int List_text::insert(const std::vector<ustring> & sv, int row, Align align, bool shrink) {
    return LIST_TEXT_IMPL->insert(sv, row, align, shrink);
}

int List_text::insert_before(const ustring & str, const ustring & other, bool shrink) {
    return LIST_TEXT_IMPL->insert_before(str, other, shrink);
}

int List_text::insert_before(const ustring & str, const ustring & other, Align align, bool shrink) {
    return LIST_TEXT_IMPL->insert_before(str, other, align, shrink);
}

int List_text::insert_before(const std::vector<ustring> & sv, const ustring & other, bool shrink) {
    return LIST_TEXT_IMPL->insert_before(sv, other, shrink);
}

int List_text::insert_before(const std::vector<ustring> & sv, const ustring & other, Align align, bool shrink) {
    return LIST_TEXT_IMPL->insert_before(sv, other, align, shrink);
}

int List_text::insert_after(const ustring & str, const ustring & other, bool shrink) {
    return LIST_TEXT_IMPL->insert_after(str, other, shrink);
}

int List_text::insert_after(const ustring & str, const ustring & other, Align align, bool shrink) {
    return LIST_TEXT_IMPL->insert_after(str, other, align, shrink);
}

int List_text::insert_after(const std::vector<ustring> & sv, const ustring & other, bool shrink) {
    return LIST_TEXT_IMPL->insert_after(sv, other, shrink);
}

int List_text::insert_after(const std::vector<ustring> & sv, const ustring & other, Align align, bool shrink) {
    return LIST_TEXT_IMPL->insert_after(sv, other, align, shrink);
}

int List_text::insert(int row, const ustring & str, int col, bool shrink) {
    return LIST_TEXT_IMPL->insert(row, str, col, shrink);
}

int List_text::insert(int row, const ustring & str, int col, Align align, bool shrink) {
    return LIST_TEXT_IMPL->insert(row, str, col, align, shrink);
}

int List_text::insert(int row, Widget & w, int col, bool shrink) {
    return LIST_IMPL->insert(row, w.ptr(), col, shrink);
}

int List_text::insert(int row, Widget & w, int col, Align align, bool shrink) {
    return LIST_IMPL->insert(row, w.ptr(), col, align, shrink);
}

int List_text::select(int row, bool fix) {
    return LIST_IMPL->select(row, fix);
}

int List_text::select(const ustring & str, bool fix) {
    return LIST_TEXT_IMPL->select(str, fix);
}

int List_text::select_similar(const ustring & str, bool fix) {
    return LIST_TEXT_IMPL->select_similar(str, fix);
}

std::size_t List_text::remove(int row) {
    return LIST_IMPL->remove(row);
}

std::size_t List_text::remove(const ustring & s, bool similar) {
    return LIST_TEXT_IMPL->remove(s, similar);
}

std::size_t List_text::remove(Widget & w) {
    return LIST_IMPL->remove(w.ptr().get());
}

ustring List_text::str(int row) const {
    return LIST_TEXT_IMPL->str(row);
}

std::u32string List_text::wstr(int row) const {
    return LIST_TEXT_IMPL->wstr(row);
}

std::vector<ustring> List_text::strings() const {
    return LIST_TEXT_IMPL->strings();
}

std::vector<std::u32string> List_text::wstrings() const {
    return LIST_TEXT_IMPL->wstrings();
}

ustring List_text::str() const {
    return LIST_TEXT_IMPL->str();
}

std::u32string List_text::wstr() const {
    return LIST_TEXT_IMPL->wstr();
}

int List_text::find(const ustring & s, int ymin, int ymax) const {
    return LIST_TEXT_IMPL->find(s, ymin, ymax);
}

int List_text::similar(const ustring & s, int ymin, int ymax) const {
    return LIST_TEXT_IMPL->similar(s, ymin, ymax);
}

int List_text::like(const ustring & s, int ymin, int ymax) const {
    return LIST_TEXT_IMPL->like(s, ymin, ymax);
}

bool List_text::contains(const ustring & s, bool similar) const {
    return LIST_TEXT_IMPL->contains(s, similar);
}

void List_text::text_align(Align align) {
    LIST_TEXT_IMPL->text_align(align);
}

Align List_text::text_align() const noexcept {
    return LIST_TEXT_IMPL->text_align();
}

void List_text::wrap(Wrap wrap) {
    LIST_TEXT_IMPL->wrap(wrap);
}

Wrap List_text::wrap() const noexcept {
    return LIST_TEXT_IMPL->wrap();
}

void List_text::allow_edit() {
    LIST_TEXT_IMPL->allow_edit();
}

void List_text::disallow_edit() {
    LIST_TEXT_IMPL->disallow_edit();
}

bool List_text::editable() const noexcept {
    return LIST_TEXT_IMPL->editable();
}

void List_text::allow_edit(int row) {
    LIST_TEXT_IMPL->allow_edit(row);
}

void List_text::disallow_edit(int row) {
    LIST_TEXT_IMPL->disallow_edit(row);
}

bool List_text::editable(int row) const noexcept {
    return LIST_TEXT_IMPL->editable(row);
}

void List_text::allow_edit(const ustring & text) {
    LIST_TEXT_IMPL->allow_edit(text);
}

void List_text::disallow_edit(const ustring & text) {
    LIST_TEXT_IMPL->disallow_edit(text);
}

bool List_text::editable(const ustring & text) const noexcept {
    return LIST_TEXT_IMPL->editable(text);
}

Widget_ptr List_text::edit(int row) {
    return LIST_TEXT_IMPL->edit(row);
}

Widget_ptr List_text::editor() {
    return LIST_TEXT_IMPL->editor();
}

Widget_cptr List_text::editor() const {
    return LIST_TEXT_IMPL->editor();
}

signal<void(int, const std::u32string &)> & List_text::signal_activate_edit() {
    return LIST_TEXT_IMPL->signal_activate_edit();
}

signal<void(int)> & List_text::signal_cancel_edit() {
    return LIST_TEXT_IMPL->signal_cancel_edit();
}

signal<void(const std::u32string &)> & List_text::signal_select_text() {
    return LIST_TEXT_IMPL->signal_select_text();
}

signal<void(const std::u32string &)> & List_text::signal_activate_text() {
    return LIST_TEXT_IMPL->signal_activate_text();
}

signal<void(int, const std::u32string &)> & List_text::signal_text_changed() {
    return LIST_TEXT_IMPL->signal_text_changed();
}

signal<void(int, const std::u32string &)> & List_text::signal_remove_text() {
    return LIST_TEXT_IMPL->signal_remove_text();
}

signal_all<const std::u32string &> & List_text::signal_validate() {
    return LIST_TEXT_IMPL->signal_validate();
}

signal_all<const std::u32string &> & List_text::signal_approve() {
    return LIST_TEXT_IMPL->signal_approve();
}

} // namespace tau

//END

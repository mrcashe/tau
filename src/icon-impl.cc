// ----------------------------------------------------------------------------
// SPDX-License-Identifier: BSD-2-Clause
// Copyright © 2014-2024 Konstantin Shmelkov <mrcashe@gmail.com>.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
// THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
// PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
// CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
// EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
// PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
// OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
// WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
// OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
// EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// ----------------------------------------------------------------------------

#include <tau/action.hh>
#include <tau/brush.hh>
#include <tau/painter.hh>
#include <display-impl.hh>
#include <icon-impl.hh>
#include <pixmap-impl.hh>
#include <theme-impl.hh>
#include <iostream>

namespace tau {

Icon_impl::Icon_impl() {
    init();
}

Icon_impl::Icon_impl(int icon_size):
    icon_size_(icon_size)
{
    init();
}

Icon_impl::Icon_impl(const ustring & icon_name, int icon_size):
    icon_name_(icon_name),
    icon_size_(icon_size)
{
    init();
}

Icon_impl::Icon_impl(Pixmap_cptr pix, int icon_size):
    Image_impl(pix, true),
    icon_size_(icon_size)
{
    init();
}

Icon_impl::Icon_impl(Action_base & action, int icon_size, Action::Flags items):
    icon_size_(icon_size)
{
    init();
    icon_name_ = action.icon_name();
    if (!action.enabled()) { freeze(); }
    if (!action.visible()) { disappear(); }
    action.signal_disable().connect(fun(this, &Icon_impl::freeze));
    action.signal_enable().connect(fun(this, &Icon_impl::thaw));
    action.signal_hide().connect(fun(this, &Icon_impl::disappear));
    action.signal_show().connect(fun(this, &Icon_impl::appear));

    if (items & Action::TOOLTIP) {
        set_tooltip(action.tooltip());
        action.signal_tooltip_changed().connect(bind_back(fun(this, static_cast<void(Widget_impl::*)(const ustring &, Align)>(&Widget_impl::set_tooltip)), Align::CENTER));
    }

    if (items & Action::ICON) {
        assign(action.icon_name(), icon_size_);
        action.signal_icon_changed().connect(fun(this, static_cast<void(Icon_impl::*)(const ustring &)>(&Icon_impl::assign)));
    }
}

Icon_impl::~Icon_impl() {
    if (signal_click_) { delete signal_click_; }
}

void Icon_impl::init() {
    if (Icon::DEFAULT == icon_size_) { icon_size_cx_ = conf().signal_changed(Conf::ICON_SIZE).connect(fun(this, &Icon_impl::update_pixmap)); }
    signal_display_in_.connect(fun(this, &Icon_impl::on_display_in), true);
    signal_display_out_.connect(fun(this, &Icon_impl::on_display_out));
    set_transparent();
}

void Icon_impl::update_pixmap() {
    update_requisition();

    if (!icon_name_.empty()) {
        auto theme = Theme_impl::root();
        int icon_size = Icon::DEFAULT != icon_size_ ? icon_size_ : conf().integer(Conf::ICON_SIZE);

        if (auto pix = theme->find_icon(icon_name_, icon_size)) {
            theme_cx_ = theme->signal_icons_changed().connect(fun(this, &Icon_impl::update_pixmap));
            set_pixmap(pix, true);
        }
    }
}

void Icon_impl::assign(const ustring & icon_name, int icon_size) {
    if (icon_name_ != icon_name || icon_size_ != icon_size) {
        ani_.clear();
        cani_.clear();
        icon_name_ = icon_name;
        icon_size_ = icon_size;
        update_pixmap();
        if (empty() && has_display()) { clear(); }
    }
}

void Icon_impl::assign(const ustring & icon_name) {
    if (icon_name_ != icon_name) {
        ani_.clear();
        cani_.clear();
        icon_name_ = icon_name;
        update_pixmap();
        if (empty() && has_display()) { clear(); }
    }
}

void Icon_impl::assign(Pixmap_cptr pix) {
    if (pix) {
        theme_cx_.drop();
        icon_name_.clear();
        icon_size_ = pix->size().max();
        set_pixmap(pix, true);
    }

    else {
        std::cerr << "** Icon_impl::assign(): got pure pixmap" << std::endl;
    }
}

void Icon_impl::resize(int icon_size) {
    if (icon_size_ != icon_size) {
        icon_size_ = icon_size;
        icon_size_cx_.drop();
        if (Icon::DEFAULT == icon_size_) { icon_size_cx_ = conf().signal_changed(Conf::ICON_SIZE).connect(fun(this, &Icon_impl::update_pixmap)); }
        update_pixmap();
    }
}

int Icon_impl::icon_size() const noexcept {
    int px = 0;
    if (Icon::DEFAULT == icon_size_) { px = conf().integer(Conf::ICON_SIZE); }
    return Theme_impl::root()->icon_pixels(px);
}

// Overrides Image_impl.
bool Icon_impl::update_requisition() {
    unsigned px = icon_size();
    Size z(px);
    if (!cani_.empty()) { z.update_min(cani_.front().pix->size()); }
    else if (!ani_.empty()) { z.update_min(ani_.front().pix->size()); }
    return require_size(z);
}

bool Icon_impl::on_mouse_down(int mbt, int mm, const Point & pt) {
    if (MBT_LEFT == mbt && signal_click_) { signal_click_->operator()(); return true; }
    return false;
}

void Icon_impl::on_display_in() {
    if (cani_.empty() && !icon_name_.empty()) {
        update_pixmap();
    }
}

void Icon_impl::on_display_out() {
    theme_cx_.drop();
}

signal<void()> & Icon_impl::signal_click() {
    if (!signal_click_) {
        signal_click_ = new signal<void()>;
        signal_mouse_down().connect(fun(this, &Icon_impl::on_mouse_down), true);
    }

    return *signal_click_;
}

} // namespace tau

//END

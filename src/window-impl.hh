// ----------------------------------------------------------------------------
// SPDX-License-Identifier: BSD-2-Clause
// Copyright © 2014-2024 Konstantin Shmelkov <mrcashe@gmail.com>.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
// THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
// PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
// CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
// EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
// PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
// OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
// WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
// OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
// EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// ----------------------------------------------------------------------------

#ifndef __TAU_WINDOW_IMPL_HH__
#define __TAU_WINDOW_IMPL_HH__

#include <tau/enums.hh>
#include <tau/timer.hh>
#include <bin-impl.hh>

namespace tau {

struct Winface: public trackable {
    virtual ~Winface() = default;
    virtual void init(Window_impl * wip) = 0;

    virtual Display_impl * display() = 0;
    virtual const Display_impl * display() const = 0;

    virtual void update() = 0;
    virtual void invalidate(const Rect & inval) = 0;
    virtual Painter_ptr painter() = 0;

    virtual void move(const Point & pt) = 0;
    virtual void move(const Rect & r) = 0;
    virtual void resize(const Size & size) = 0;

    virtual void minimize() = 0;
    virtual void maximize() = 0;
    virtual void restore() = 0;
    virtual void set_fullscreen(bool yes) = 0;
    virtual bool visible() const = 0;

    virtual void set_cursor(Cursor_ptr cursor) = 0;
    virtual void unset_cursor() = 0;
    virtual void show_cursor(bool show) = 0;
    virtual void set_title(const ustring & title) = 0;
    virtual void set_icon(Pixmap_ptr icon) = 0;
};

// ----------------------------------------------------------------------------
// ----------------------------------------------------------------------------

class Window_impl: public Bin_impl {
public:

    Winface_ptr winface() noexcept { return winface_; }
    Winface_cptr winface() const noexcept { return winface_; }

    // Overrides Widget_impl.
    Display_impl * display() noexcept override { return winface_->display(); }

    // Overrides Widget_impl.
    const Display_impl * display() const noexcept override { return winface_->display(); }

    // Overrides Widget_impl.
    bool cursor_visible() const noexcept override;

    // Overrides Widget_impl.
    bool has_modal() const noexcept override;

    // Overrides Container_impl.
    // Overrides Widget_impl.
    bool grab_modal_up(Widget_impl * caller) override;

    // Overrides Container_impl.
    // Overrides Widget_impl.
    bool end_modal_up(Widget_impl * caller) override;

    // Overrides Container_impl.
    // Overrides Widget_impl.
    int  grab_focus_up(Widget_impl * caller) override;

    // Overrides Container_impl.
    // Overrides Widget_impl.
    void drop_focus_up(Widget_impl * caller) override;

    // Overrides Widget_impl.
    bool has_window() const noexcept override { return true; }

    // Overrides Widget_impl.
    void set_cursor_up(Cursor_ptr cursor, Widget_impl * wip) override;

    // Overrides Widget_impl.
    void unset_cursor_up(Widget_impl * wip) override;

    // Overrides Container_impl.
    // Overrides Window_impl.
    void show_cursor_up() override;

    // Overrides Widget_impl.
    void hide_cursor_up() override;

    // Overrides Widget_impl.
    Window_impl * window() noexcept override { return this; }

    // Overrides Widget_impl.
    const Window_impl * window() const noexcept override { return this; }

    // Overrides Widget_impl.
    // Overridden by Popup_impl.
    Point to_screen(const Point & pt=Point()) const noexcept override;

    // Overrides Widget_impl.
    // Overridden by Popup_impl.
    Point where_mouse() const noexcept override;

    // Overrides Widget_impl.
    bool hover() const noexcept override;

    // Overrides Container_impl.
    // Overrides Widget_impl.
    bool grab_mouse_up(Widget_impl * wi) override;

    // Overrides Container_impl.
    // Overrides Widget_impl.
    bool ungrab_mouse_up(Widget_impl * caller) override;

    // Overrides Container_impl.
    // Overrides Widget_impl.
    bool grabs_mouse() const noexcept override;

    // Overrides Widget_impl.
    void invalidate(const Rect & inval=Rect()) override;

    // Overrides Widget_impl.
    Painter painter() override;

    // Overrides Container_impl.
    Widget_ptr focus_endpoint() noexcept override;

    // Overrides Container_impl.
    Widget_cptr focus_endpoint() const noexcept override;

    // Overridden by Dialog_impl.
    virtual void close();

    void move(const Point & pt);
    void move(const Rect & r);
    void move(int x, int y) { move(Point(x, y)); }
    void move(const Point & pt, const Size & z) { move(Rect(pt, z)); }
    void move(int x, int y, unsigned width, unsigned height) { move(Rect(x, y, Size(width, height))); }
    void resize(const Size & sz);
    void resize(unsigned width, unsigned height) { resize(Size(width, height)); }
    void update();

    bool update_position(const Point & position);
    Point position() const noexcept { return position_; }

    void handle_client_area(const Rect & r);
    Rect client_area() const noexcept { return client_area_; }

    void open_tooltip(Widget_impl * owner, unsigned time_ms);
    const Widget_impl * tooltip_owner() const noexcept { return tooltip_owner_; }
    void flush_arrange();

    signal<void()> & signal_close() { return signal_close_; }
    signal<void()> & signal_position_changed() { return signal_position_changed_; }
    signal<void()> & signal_close_tooltip() { return signal_close_tooltip_; }

protected:

    Winface_ptr         winface_;
    Point               position_;              // Position within the screen.
    Rect                client_area_;

    signal<void()>      signal_close_;
    signal<void()>      signal_position_changed_;

protected:

    Window_impl();
   ~Window_impl() { destroy(); }

    // Overrides Container_impl.
    void queue_arrange_up() override;

    // Overrides Widget_impl.
    bool on_backpaint(Painter pr, const Rect & inval) override;

private:

    bool                closed_                 = false;
    signal<void()>      signal_close_tooltip_;
    Timer               arrange_timer_          { fun(this, &Window_impl::sync_arrange) };
    Widget_impl *       tooltip_owner_          = nullptr;

private:

    void on_close_tooltip() { tooltip_owner_ = nullptr; }
    void on_focus_in();
};

} // namespace tau

#endif // __TAU_WINDOW_IMPL_HH__

// ----------------------------------------------------------------------------
// SPDX-License-Identifier: BSD-2-Clause
// Copyright © 2014-2024 Konstantin Shmelkov <mrcashe@gmail.com>.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
// THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
// PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
// CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
// EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
// PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
// OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
// WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
// OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
// EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// ----------------------------------------------------------------------------

#include <tau/string.hh>
#include <algorithm>
#include <cstdlib>
#include <cstring>
#include <iostream>

namespace {

// Return 7 if ASCII, otherwise - 8.
unsigned is_ascii(const char * s) noexcept {
    for (; '\0' != *s; ++s) {
        if (*s < 0) {
            return 8;
        }
    }

    return 7;
}

ssize_t utf8_pointer_to_offset(const char * str, const char * pos) {
    const char * s = str;
    ssize_t offset = 0;

    if (pos < str) {
        offset = -utf8_pointer_to_offset(pos, str);
    }

    else {
        while (*s && s < pos) {
            s = tau::utf8_next(s);
            offset++;
        }
    }

    return offset;
}

char * utf8_offset_to_pointer(const char * str, ssize_t offset) {
    const char * s = str;

    if (offset >= 0) {
        while (offset--) {
            s = tau::utf8_next(s);
        }
    }

    else {
        while (offset < 0) {
            const char * s1 = s;
            s += offset;
            while ((*s & 0xc0) == 0x80) { --s; }
            offset += utf8_pointer_to_offset(s, s1);
        }
    }

    return (char *)s;
}

// First overload: stop on '\0' character.
std::size_t utf8_byte_offset(const char * str, std::size_t offset) {
    if (offset == tau::ustring::npos) { return tau::ustring::npos; }
    const char * p;

    for (p = str; 0 != offset; --offset) {
        if ('\0' == *p) { return tau::ustring::npos; }
        p += tau::utf8_charlen(*p);
    }

    return p-str;
}

// Second overload: stop when reaching maxlen.
std::size_t utf8_byte_offset(const char * str, std::size_t offset, std::size_t maxlen) {
    if (offset == tau::ustring::npos) { return tau::ustring::npos; }

    const char * pend = str+maxlen;
    const char * p;

    for (p = str; offset != 0; --offset) {
        if ('\0' == *p || p >= pend) { return tau::ustring::npos; }
        p += tau::utf8_charlen(*p);
    }

    return p-str;
}

// Converts byte offset to UTF-8 character offset.
std::size_t utf8_char_offset(const std::string & str, std::size_t offset) {
    if (offset == tau::ustring::npos) { return tau::ustring::npos; }
    auto * pdata = str.data();
    return utf8_pointer_to_offset(pdata, pdata+offset);
}

// Takes UTF-8 character offset and count in ci and cn.
// Returns the byte offset and count in i and n.
struct utf_substr_bounds {
    std::size_t i, n;

    utf_substr_bounds(const std::string & str, std::size_t ci, std::size_t cn):
        i(utf8_byte_offset(str.data(), ci, str.size())),
        n(tau::ustring::npos)
    {
        if (i != tau::ustring::npos) {
            n = utf8_byte_offset(str.data()+i, cn, str.size()-i);
        }
    }
};

// Helper for helper:-)
std::size_t utf8_find_first_helper(const char * s, const char * e, std::size_t i, const char32_t * match, const char32_t * ematch, bool find_not_of) {
    for (; *s && s < e; s = tau::utf8_next(s), ++i) {
        auto * found = std::find(match, ematch, tau::char32_from_pointer(s));
        if ((found != ematch) != find_not_of) { return i; }
    }

    return tau::ustring::npos;
}

// Helper to implement ustring::find_first_of() and find_first_not_of().
// Returns the UTF-8 character offset, or tau::ustring::npos if not found.
std::size_t utf8_find_first_of(const std::string & str, std::size_t i, const char * match, ssize_t match_size, bool find_not_of) {
    const std::size_t byte_offset = utf8_byte_offset(str.data(), i, str.size());
    if (byte_offset >= str.size()) { return tau::ustring::npos; }
    ssize_t wmsize = tau::utf8_strlen(match, match_size);

    if (std::abs(wmsize) <= 8192) {
        char32_t wmatch[std::abs(wmsize)];

        if (wmsize > 0) {
            for (ssize_t j = 0; j < wmsize; ++j) {
                wmatch[j] = tau::char32_from_pointer(match);
                match = tau::utf8_next(match);
            }
        }

        else {
            for (ssize_t j = 0; j < -wmsize; ++j) {
                wmatch[j] = uint8_t(*match++);
            }
        }

        return utf8_find_first_helper(str.data()+byte_offset, str.data()+str.size(), i, wmatch, wmatch+std::abs(wmsize), find_not_of);
    }

    std::u32string wmatch = tau::utf8_to_utf32(match, match_size);
    return utf8_find_first_helper(str.data()+byte_offset, str.data()+str.size(), i, wmatch.data(), wmatch.data()+wmatch.size(), find_not_of);
}

// Yet another helper for helper:-)
std::size_t utf8_find_last_helper(const char * s, const char * e, std::size_t i, const char32_t * match, const char32_t * ematch, bool find_not_of) {
    auto * p = s;
    std::size_t str_len = e-s;

    // Set p one byte beyond the actual start position.
    std::size_t byte_offset = utf8_byte_offset(s, i, str_len);
    p += byte_offset < str_len ? byte_offset+1 : str_len;

    while (p > s) {
        // Move to previous character.
        do { --p; } while((static_cast<unsigned char>(*p) & 0xC0u) == 0x80);
        auto * found = std::find(match, ematch, tau::char32_from_pointer(p));
        if ((found != ematch) != find_not_of) { return utf8_pointer_to_offset(s, p); }
    }

    return tau::ustring::npos;
}

// Helper to implement ustring::find_last_of() and find_last_not_of().
// Returns the UTF-8 character offset, or tau::ustring::npos if not found.
std::size_t utf8_find_last_of(const std::string & str, std::size_t i, const char * match, ssize_t match_size, bool find_not_of) {
    if (str.empty() || '\0' == *match) { return tau::ustring::npos; }
    ssize_t wmsize = tau::utf8_strlen(match, match_size);

    if (std::abs(wmsize) <= 8192) {
        char32_t wmatch[std::abs(wmsize)];

        if (wmsize > 0) {
            for (ssize_t j = 0; j < wmsize; ++j) {
                wmatch[j] = tau::char32_from_pointer(match);
                match = tau::utf8_next(match);
            }
        }

        else {
            for (ssize_t j = 0; j < -wmsize; ++j) {
                wmatch[j] = uint8_t(*match++);
            }
        }

        return utf8_find_last_helper(str.data(), str.data()+str.size(), i, wmatch, wmatch+std::abs(wmsize), find_not_of);
    }

    std::u32string wmatch = tau::utf8_to_utf32(match, match_size);
    return utf8_find_last_helper(str.data(), str.data()+str.size(), i, wmatch.data(), wmatch.data()+wmatch.size(), find_not_of);
}

} // anonymous namespace

// ----------------------------------------------------------------------------
// ----------------------------------------------------------------------------

namespace tau {

// Internal; declared in sys-impl.hh.
void str_from_utf32(ustring & s, const char32_t * p, std::size_t nchars) {
    s.clear();

    for (; *p && nchars; ++p, --nchars) {
        s.push_back(*p);
    }
}

// Internal; declared in sys-impl.hh.
void str_from_utf16(ustring & s, const char16_t * p, std::size_t nchars) {
    s.clear();
    char16_t surr = 0;

    for (; *p && nchars; ++p, --nchars) {
        char16_t wc = *p;

        if (char16_is_surrogate(wc)) {
            if (0 != surr) {
                s.push_back(char32_from_surrogate(surr, wc));
                surr = 0;
            }

            else {
                surr = wc;
            }
        }

        else {
            if (0 != surr) { break; }
            else { s.push_back(char32_t(wc)); }
        }
    }
}

// ----------------------------------------------------------------------------
// ----------------------------------------------------------------------------

ustring::ustring() {}

ustring::~ustring() {}

ustring::ustring(const ustring & other):
    str_(other.str_),
    len_(other.len_)
{
}

ustring::ustring(ustring && other):
    str_(std::move(other.str_)),
    len_(other.len_)
{
    other.len_ = SSIZE_MAX;
}

ustring::ustring(const ustring & src, size_type i, size_type n):
    len_(SSIZE_MAX)
{
    const utf_substr_bounds bounds(src.str_, i, n);
    str_.assign(src.str_, bounds.i, bounds.n);
}

ustring::ustring(const char * src, size_type n):
    str_(src, n),
    len_(SSIZE_MAX)
{
}

ustring::ustring(const char * src):
    str_(src),
    len_(SSIZE_MAX)
{
}

ustring::ustring(size_type n, char32_t wc) {
    assign(n, wc);
}

ustring::ustring(size_type n, char c):
    str_(n, c),
    len_(SSIZE_MAX)
{
}

ustring::ustring(const std::string & src):
    str_(src),
    len_(SSIZE_MAX)
{
}

ustring::ustring(std::string && src):
    str_(std::move(src)),
    len_(SSIZE_MAX)
{
}

ustring::ustring(const std::u32string & src):
    len_(SSIZE_MAX)
{
    str_from_utf32(*this, reinterpret_cast<const char32_t *>(src.c_str()), src.size());
}

ustring::ustring(const std::u16string & src):
    len_(SSIZE_MAX)
{
    str_from_utf16(*this, reinterpret_cast<const char16_t *>(src.c_str()), src.size());
}

ustring::ustring(const std::wstring & ws):
    len_(SSIZE_MAX)
{
    assign(ws);
}

ustring::ustring(const wchar_t * src):
    len_(SSIZE_MAX)
{
    assign(src);
}

ustring::ustring(const wchar_t * src, size_type n):
    len_(SSIZE_MAX)
{
    assign(src, n);
}

ustring::ustring(const char16_t * raw):
    len_(SSIZE_MAX)
{
    assign(raw);
}

ustring::ustring(const char16_t * raw, size_type n):
    len_(SSIZE_MAX)
{
    assign(raw, n);
}

ustring::ustring(std::string_view sv):
    str_(sv),
    len_(SSIZE_MAX)
{
}

void ustring::swap(ustring & other) {
    str_.swap(other.str_);
    std::swap(len_, other.len_);
}

ustring & ustring::operator=(const ustring & other) {
    if (this != &other) {
        str_ = other.str_;
        len_ = other.len_;
    }

    return *this;
}

ustring & ustring::operator=(ustring && other) {
    if (this != &other) {
        len_ = other.len_;
        str_ = std::move(other.str_);
        other.len_ = SSIZE_MAX;
    }

    return *this;
}

ustring & ustring::operator=(const std::string & src) {
    str_ = src;
    len_ = SSIZE_MAX;
    return *this;
}

ustring & ustring::operator=(std::string && src) {
    str_ = std::move(src);
    len_ = SSIZE_MAX;
    return *this;
}

ustring & ustring::operator=(const char * src) {
    str_ = src;
    len_ = SSIZE_MAX;
    return *this;
}

ustring & ustring::operator=(char32_t wc) {
    char buffer[4];
    auto len = char32_len(wc);
    for (std::size_t i = len-1; i; --i) { buffer[i] = (wc & 0x3f) | 0x80; wc >>= 6; }
    buffer[0] = wc|utf8_leader(len);
    str_.assign(buffer, len);
    len_ = 1;
    return *this;
}

ustring & ustring::operator=(char c) {
    str_ = c;
    len_ = -1;
    return *this;
}

ustring & ustring::assign(const ustring & src) {
    str_ = src.str_;
    len_ = src.len_;
    return *this;
}

ustring & ustring::assign(const ustring & src, size_type i, size_type n) {
    const utf_substr_bounds bounds(src.str_, i, n);
    str_.assign(src.str_, bounds.i, bounds.n);
    len_ = SSIZE_MAX;
    return *this;
}

ustring & ustring::assign(const char * src, size_type n) {
    str_.assign(src, n);
    len_ = SSIZE_MAX;
    return *this;
}

ustring& ustring::assign(const char * src) {
    str_ = src;
    len_ = SSIZE_MAX;
    return *this;
}

ustring & ustring::assign(size_type n, char32_t wc) {
    if (wc < 128) {
        str_.assign(n, static_cast<char>(wc));
        len_ = -n;
    }

    else {
        clear();
        while (n--) { push_back(wc); }
    }

    return *this;
}

ustring & ustring::assign(size_type n, char c) {
    str_.assign(n, c);
    len_ = SSIZE_MAX;
    return *this;
}

ustring & ustring::assign(std::string_view sv) {
    str_.assign(sv);
    len_ = SSIZE_MAX;
    return *this;
}

ustring & ustring::assign(const char16_t * raw) {
    auto * p = raw;
    for (; L'\0' != *p; ++p);
    str_from_utf16(*this, raw, p-raw);
    return *this;
}

ustring & ustring::assign(const char16_t * raw, size_type n) {
    str_from_utf16(*this, raw, n);
    return *this;
}

ustring & ustring::operator+=(const ustring & src) {
    return append(src);
}

ustring & ustring::operator+=(const char * src) {
    return append(src);
}

ustring & ustring::operator+=(char32_t wc) {
    push_back(wc);
    return *this;
}

void ustring::push_back(char32_t wc) {
    if (len_ <= 0 && wc < 128) {
        str_.push_back(wc);
        --len_;
    }

    else {
        len_ = SSIZE_MAX;
        char buffer[4];
        auto len = char32_len(wc);
        for (std::size_t i = len-1; i; --i) { buffer[i] = (wc & 0x3f) | 0x80; wc >>= 6; }
        buffer[0] = wc|utf8_leader(len);
        str_.append(buffer, len);
    }
}

ustring & ustring::operator+=(char c) {
    push_back(c);
    return *this;
}

void ustring::push_back(char c) {
    str_ += c;
    if (len_ <= 0) { --len_; }
    else { len_ = SSIZE_MAX; }
    len_ = SSIZE_MAX;
}

ustring & ustring::append(const ustring & src) {
    str_.append(src.str_);
    len_ = SSIZE_MAX;
    return *this;
}

ustring & ustring::append(const ustring & src, size_type i, size_type n) {
    const utf_substr_bounds bounds(src.str_, i, n);
    str_.append(src.str_, bounds.i, bounds.n);
    len_ = SSIZE_MAX;
    return *this;
}

ustring & ustring::append(const char * src, size_type n) {
    str_.append(src, n);
    len_ = SSIZE_MAX;
    return *this;
}

ustring & ustring::append(const char * src) {
    str_ += src;
    len_ = SSIZE_MAX;
    return *this;
}

ustring & ustring::append(size_type n, char32_t wc) {
    char buffer[4];
    auto len = char32_len(wc);
    for (std::size_t i = len-1; i; --i) { buffer[i] = (wc & 0x3f) | 0x80; wc >>= 6; }
    buffer[0] = wc|utf8_leader(len);

    auto pos = str_.size();
    str_.append(n*len, ' ');
    for (auto i = n; i; --i, pos += len) { str_.replace(pos, len, buffer); }
    len_ = SSIZE_MAX;
    return *this;
}

ustring & ustring::append(size_type n, char c) {
    str_.append(n, c);
    len_ = SSIZE_MAX;
    return *this;
}

ustring & ustring::insert(size_type i, const ustring & src) {
    str_.insert(utf8_byte_offset(str_.data(), i, str_.size()), src.str_);
    len_ = SSIZE_MAX;
    return *this;
}

ustring & ustring::insert(size_type i, const ustring & src, size_type i2, size_type n) {
    const utf_substr_bounds bounds2(src.str_, i2, n);
    str_.insert(utf8_byte_offset(str_.data(), i, str_.size()), src.str_, bounds2.i, bounds2.n);
    len_ = SSIZE_MAX;
    return *this;
}

ustring & ustring::insert(size_type i, const char * src, size_type bytes) {
    str_.insert(utf8_byte_offset(str_.data(), i, str_.size()), src, bytes);
    len_ = SSIZE_MAX;
    return *this;
}

ustring & ustring::insert(size_type i, const char * src) {
    str_.insert(utf8_byte_offset(str_.data(), i, str_.size()), src);
    len_ = SSIZE_MAX;
    return *this;
}

ustring & ustring::insert(size_type i, size_type n, char32_t uc) {
    str_.insert(utf8_byte_offset(str_.data(), i, str_.size()), ustring(n, uc).str_);
    len_ = SSIZE_MAX;
    return *this;
}

ustring & ustring::insert(size_type i, size_type n, char c) {
    str_.insert(utf8_byte_offset(str_.data(), i, str_.size()), n, c);
    len_ = SSIZE_MAX;
    return *this;
}

ustring::iterator ustring::insert(iterator p, char32_t wc) {
    const size_type offset = p.base()-str_.begin();
    char buffer[4];
    auto len = char32_len(wc);
    for (std::size_t i = len-1; i; --i) { buffer[i] = (wc & 0x3f) | 0x80; wc >>= 6; }
    buffer[0] = wc|utf8_leader(len);
    str_.insert(offset, buffer, len);
    len_ = SSIZE_MAX;
    return iterator(str_.begin()+offset);
}

ustring::iterator ustring::insert(iterator p, char c) {
    len_ = SSIZE_MAX;
    return iterator(str_.insert(p.base(), c));
}

void ustring::insert(iterator p, size_type n, char32_t uc) {
    len_ = SSIZE_MAX;
    str_.insert(p.base()-str_.begin(), ustring(n, uc).str_);
}

void ustring::insert(iterator p, size_type n, char c) {
    len_ = SSIZE_MAX;
    str_.insert(p.base(), n, c);
}

ustring & ustring::replace(size_type i, size_type n, const ustring & src) {
    const utf_substr_bounds bounds(str_, i, n);
    str_.replace(bounds.i, bounds.n, src.str_);
    len_ = SSIZE_MAX;
    return *this;
}

ustring & ustring::replace(size_type i, size_type n, const ustring & src, size_type i2, size_type n2) {
    const utf_substr_bounds bounds (str_, i, n);
    const utf_substr_bounds bounds2 (src.str_, i2, n2);
    str_.replace(bounds.i, bounds.n, src.str_, bounds2.i, bounds2.n);
    len_ = SSIZE_MAX;
    return *this;
}

ustring & ustring::replace(size_type i, size_type n, const char * src, size_type bytes) {
    const utf_substr_bounds bounds (str_, i, n);
    str_.replace(bounds.i, bounds.n, src, bytes);
    len_ = SSIZE_MAX;
    return *this;
}

ustring & ustring::replace(size_type i, size_type n, const char * src) {
    const utf_substr_bounds bounds(str_, i, n);
    str_.replace(bounds.i, bounds.n, src);
    len_ = SSIZE_MAX;
    return *this;
}

ustring & ustring::replace(size_type i, size_type n, size_type n2, char32_t uc) {
    const utf_substr_bounds bounds(str_, i, n);
    str_.replace(bounds.i, bounds.n, ustring(n2, uc).str_);
    len_ = SSIZE_MAX;
    return *this;
}

ustring & ustring::replace(size_type i, size_type n, size_type n2, char c) {
    const utf_substr_bounds bounds(str_, i, n);
    str_.replace(bounds.i, bounds.n, n2, c);
    len_ = SSIZE_MAX;
    return *this;
}

ustring & ustring::replace(iterator pbegin, iterator pend, const ustring & src) {
    str_.replace(pbegin.base(), pend.base(), src.str_);
    len_ = SSIZE_MAX;
    return *this;
}

ustring & ustring::replace(iterator pbegin, iterator pend, const char * src, size_type n) {
    str_.replace(pbegin.base(), pend.base(), src, utf8_byte_offset(src, n));
    len_ = SSIZE_MAX;
    return *this;
}

ustring & ustring::replace(iterator pbegin, iterator pend, const char * src) {
    str_.replace(pbegin.base(), pend.base(), src);
    len_ = SSIZE_MAX;
    return *this;
}

ustring & ustring::replace(iterator pbegin, iterator pend, size_type n, char32_t uc) {
    str_.replace(pbegin.base(), pend.base(), ustring(n, uc).str_);
    len_ = SSIZE_MAX;
    return *this;
}

ustring & ustring::replace(iterator pbegin, iterator pend, size_type n, char c) {
    str_.replace(pbegin.base(), pend.base(), n, c);
    len_ = SSIZE_MAX;
    return *this;
}

void ustring::clear() {
    str_.erase();
    len_ = 0;
}

ustring & ustring::erase() {
    clear();
    return *this;
}

ustring & ustring::erase(size_type i, size_type n) {
    const utf_substr_bounds bounds(str_, i, n);
    str_.erase(bounds.i, bounds.n);
    len_ = SSIZE_MAX;
    return *this;
}

ustring::iterator ustring::erase(iterator p) {
    iterator iter_end = p;
    ++iter_end;
    len_ = SSIZE_MAX;
    return iterator(str_.erase(p.base(), iter_end.base()));
}

ustring::iterator ustring::erase(iterator pbegin, iterator pend) {
    len_ = SSIZE_MAX;
    return iterator(str_.erase(pbegin.base(), pend.base()));
}

int ustring::compare(const char * rhs) const noexcept {
    if (str_.empty() && '\0' == *rhs) { return 0; }
    return len_ <= 0 ? strcmp(str_.data(), rhs) : strcoll(str_.data(), rhs);
}

int ustring::compare(const ustring & rhs) const noexcept {
    return compare(rhs.str_.c_str());
}

int ustring::compare(size_type i, size_type n, const ustring & rhs) const noexcept {
    return ustring(*this, i, n).compare(rhs);
}

int ustring::compare(size_type i, size_type n, const ustring & rhs, size_type i2, size_type n2) const noexcept {
    return ustring(*this, i, n).compare(ustring(rhs, i2, n2));
}

int ustring::compare(size_type i, size_type n, const char * rhs, size_type bytes) const noexcept {
    return ustring(*this, i, n).compare(ustring(rhs, bytes));
}

int ustring::compare(size_type i, size_type n, const char * rhs) const noexcept {
    return ustring(*this, i, n).compare(rhs);
}

ustring::value_type ustring::operator[](size_type i) const {
    if (SSIZE_MAX == len_) { len_ = utf8_strlen(str_.data()); }

    if (len_ < 0) {
        return static_cast<uint8_t>(str_[i]);
    }

    else {
        return char32_from_pointer(utf8_offset_to_pointer(str_.data(), i));
    }
}

ustring::value_type ustring::at(size_type i) const {
    if (SSIZE_MAX == len_) { len_ = utf8_strlen(str_.data()); }

    if (len_ < 0) {
        return static_cast<uint8_t>(str_.at(i));
    }

    else {
        size_type byte_offset = utf8_byte_offset(str_.data(), i, str_.size());
        return char32_from_pointer(&str_.at(byte_offset));
    }
}

ustring::value_type ustring::front() const noexcept {
    return operator[](0);
}

ustring::value_type ustring::back() const noexcept {
    return operator[](size()-1);
}

ustring::iterator ustring::begin() {
    return iterator(str_.begin());
}

ustring::iterator ustring::end() {
    return iterator(str_.end());
}

ustring::const_iterator ustring::begin() const {
    return const_iterator(str_.begin());
}

ustring::const_iterator ustring::end() const {
    return const_iterator(str_.end());
}

ustring::reverse_iterator ustring::rbegin() {
    return reverse_iterator(iterator(str_.end()));
}

ustring::reverse_iterator ustring::rend() {
    return reverse_iterator(iterator(str_.begin()));
}

ustring::const_reverse_iterator ustring::rbegin() const {
    return const_reverse_iterator(const_iterator(str_.end()));
}

ustring::const_reverse_iterator ustring::rend() const {
    return const_reverse_iterator(const_iterator(str_.begin()));
}

ustring::size_type ustring::find(const ustring & str, size_type i) const noexcept {
    if (len_ <= 0 && str.len_ <= 0) { return str_.find(str.str_, i); }
    return utf8_char_offset(str_, str_.find(str.str_, utf8_byte_offset(str_.data(), i, str_.size())));
}

ustring::size_type ustring::find(const char * str, size_type i, size_type bytes) const noexcept {
    return utf8_char_offset(str_, str_.find(str, utf8_byte_offset(str_.data(), i, str_.size()), bytes));
}

ustring::size_type ustring::find(const char * str, size_type i) const noexcept {
    return utf8_char_offset(str_, str_.find(str, utf8_byte_offset(str_.data(), i, str_.size())));
}

ustring::size_type ustring::find(char32_t wc, size_type i) const noexcept {
    if (len_ <= 0 && wc < 128) { return str_.find(wc, i); }
    char buffer[4];
    auto len = char32_len(wc);
    for (std::size_t i = len-1; i; --i) { buffer[i] = (wc & 0x3f) | 0x80; wc >>= 6; }
    buffer[0] = wc|utf8_leader(len);
    const std::string s(buffer, len);
    return utf8_char_offset(str_, str_.find(s, utf8_byte_offset(str_.data(), i, str_.size())));
}

ustring::size_type ustring::rfind(const ustring & str, size_type i) const noexcept {
    return utf8_char_offset(str_, str_.rfind(str.str_, utf8_byte_offset(str_.data(), i, str_.size())));
}

ustring::size_type ustring::rfind(const char * str, size_type i, size_type bytes) const noexcept {
    return utf8_char_offset(str_, str_.rfind(str, utf8_byte_offset(str_.data(), i, str_.size()), bytes));
}

ustring::size_type ustring::rfind(const char * str, size_type i) const noexcept {
    return utf8_char_offset(str_, str_.rfind(str, utf8_byte_offset(str_.data(), i, str_.size())));
}

ustring::size_type ustring::rfind(char32_t wc, size_type i) const noexcept {
    char buffer[4];
    auto len = char32_len(wc);
    for (std::size_t i = len-1; i; --i) { buffer[i] = (wc & 0x3f) | 0x80; wc >>= 6; }
    buffer[0] = wc|utf8_leader(len);
    const std::string s(buffer, len);
    return utf8_char_offset(str_, str_.rfind(s, utf8_byte_offset(str_.data(), i, str_.size())));
}

bool ustring::contains(const ustring & s) const noexcept {
    return npos != str_.find(s);
}

bool ustring::contains(char32_t wc) const noexcept {
    return npos != find(wc);
}

bool ustring::starts_with(const ustring & s) const noexcept {
    return s.size() <= size() && 0 == compare(0, s.size(), s);
}

bool ustring::starts_with(char32_t wc) const noexcept {
    return !empty() && front() == wc;
}

bool ustring::ends_with(const ustring & s) const noexcept {
    return s.size() <= size() && 0 == compare(size()-s.size(), s.size(), s);
}

bool ustring::ends_with(char32_t wc) const noexcept {
    return !empty() && back() == wc;
}

ustring::size_type ustring::find_first_of(const ustring & match, size_type i) const noexcept {
    if (len_ <= 0 && match.len_ <= 0) { return str_.find_first_of(match.str_, i); }
    return utf8_find_first_of(str_, i, match.str_.data(), match.str_.size(), false);
}

ustring::size_type ustring::find_first_of(const char * match, size_type i, size_type bytes) const noexcept {
    return utf8_find_first_of(str_, i, match, bytes, false);
}

ustring::size_type ustring::find_first_of(const char * match, size_type i) const noexcept {
    return utf8_find_first_of(str_, i, match, -1, false);
}

ustring::size_type ustring::find_first_of(char32_t wc, size_type i) const noexcept {
    return find(wc, i);
}

ustring::size_type ustring::find_first_of(char c, size_type i) const noexcept {
    return find(c, i);
}

ustring::size_type ustring::find_last_of(const ustring & match, size_type i) const noexcept {
    if (len_ <= 0 && match.len_ <= 0) { return str_.find_last_of(match.str_, i); }
    return utf8_find_last_of(str_, i, match.str_.data(), match.str_.size(), false);
}

ustring::size_type ustring::find_last_of(const char * match, size_type i, size_type bytes) const noexcept {
    return utf8_find_last_of(str_, i, match, bytes, false);
}

ustring::size_type ustring::find_last_of(const char* match, size_type i) const noexcept {
    return utf8_find_last_of(str_, i, match, -1, false);
}

ustring::size_type ustring::find_last_of(char32_t uc, size_type i) const noexcept {
    return rfind(uc, i);
}

ustring::size_type ustring::find_last_of(char c, size_type i) const noexcept {
    return rfind(c, i);
}

ustring::size_type ustring::find_first_not_of(const ustring & match, size_type i) const noexcept {
    if (len_ <= 0 && match.len_ <= 0) { return str_.find_first_not_of(match.str_, i); }
    return utf8_find_first_of(str_, i, match.str_.data(), match.str_.size(), true);
}

ustring::size_type ustring::find_first_not_of(const char* match, size_type i, size_type bytes) const noexcept {
    return utf8_find_first_of(str_, i, match, bytes, true);
}

ustring::size_type ustring::find_first_not_of(const char* match, size_type i) const noexcept {
    return utf8_find_first_of(str_, i, match, -1, true);
}

ustring::size_type ustring::find_first_not_of(char32_t uc, size_type i) const noexcept {
    const size_type bi = utf8_byte_offset(str_.data(), i, str_.size());

    if(bi != npos) {
        const char * const pbegin = str_.data();
        const char * const pend = pbegin+str_.size();

        for (const char * p = pbegin+bi; p < pend; p = utf8_next(p), ++i) {
            if (char32_from_pointer(p) != uc) { return i; }
        }
    }

    return npos;
}

ustring::size_type ustring::find_first_not_of(char c, size_type i) const noexcept {
    if (len_ <= 0) { return str_.find_first_not_of(c, i); }
    const char * pbegin = str_.data();
    size_type str_size = str_.size();
    const size_type bi = utf8_byte_offset(pbegin, i, str_size);

    if (bi != npos) {
        const char * pend = pbegin+str_.size();

        for (const char * p = pbegin+bi; p < pend; p = utf8_next(p), ++i) {
            if (*p != c) { return i; }
        }
    }

    return npos;
}

ustring::size_type ustring::find_last_not_of(const ustring & match, size_type i) const noexcept {
    if (len_ <= 0 && match.len_ <= 0) { return str_.find_last_not_of(match.str_, i); }
    return utf8_find_last_of(str_, i, match.str_.data(), match.str_.size(), true);
}

ustring::size_type ustring::find_last_not_of(const char * match, size_type i, size_type bytes) const noexcept {
    return utf8_find_last_of(str_, i, match, bytes, true);
}

ustring::size_type ustring::find_last_not_of(const char * match, size_type i) const noexcept {
    return utf8_find_last_of(str_, i, match, -1, true);
}

ustring::size_type ustring::find_last_not_of(char32_t uc, size_type i) const noexcept {
    const char * const pbegin = str_.data();
    const char * const pend = pbegin+str_.size();
    size_type i_cur = 0;
    size_type i_found = npos;

    for (const char * p = pbegin; p < pend && i_cur <= i; p = utf8_next(p), ++i_cur) {
        if (char32_from_pointer(p) != uc) {
            i_found = i_cur;
        }
    }

    return i_found;
}

ustring::size_type ustring::find_last_not_of(char c, size_type i) const noexcept {
    if (len_ <= 0) { return str_.find_last_not_of(c, i); }
    const char * const pbegin = str_.data();
    const char * const pend = pbegin+str_.size();
    size_type i_cur = 0;
    size_type i_found = npos;

    for (const char * p = pbegin; p < pend && i_cur <= i; p = utf8_next(p), ++i_cur) {
        if (*p != c) {
            i_found = i_cur;
        }
    }

    return i_found;
}

ustring::size_type ustring::size() const noexcept {
    if (SSIZE_MAX == len_) { len_ = utf8_strlen(str_.data()); }
    return std::abs(len_);
}

ustring ustring::substr(size_type i, size_type n) const {
    return ustring(*this, i, n);
}

const std::string & ustring::raw() const noexcept {
    len_ = SSIZE_MAX;
    return str_;
}

void ustring::resize(size_type n, char32_t uc) {
    const size_type size_now = size();

    if (n < size_now) {
        erase(n, npos);
    }

    else if (n > size_now) {
        append(n-size_now, uc);
    }
}

void ustring::resize(size_type n, char c) {
    const size_type size_now = size();

    if (n < size_now) {
        erase(n, npos);
    }

    else if (n > size_now) {
        str_.append(n-size_now, c);
        len_ = SSIZE_MAX;
    }
}

// Note that copy() requests UTF-8 character offsets as
// parameters, but returns the number of copied bytes.
ustring::size_type ustring::copy(char * dest, size_type n, size_type i) const {
    const utf_substr_bounds bounds(str_, i, n);
    return str_.copy(dest, bounds.n, bounds.i);
}

unsigned ustring::bits() const noexcept {
    return size() ? (len_ > 0 ? 21 : is_ascii(data())) : 7;
}

bool operator==(const ustring::const_iterator & lhs, const ustring::const_iterator & rhs) noexcept {
    return (lhs.base() == rhs.base());
}

bool operator!=(const ustring::const_iterator& lhs, const ustring::const_iterator& rhs) noexcept {
    return (lhs.base() != rhs.base());
}

bool operator<(const ustring::const_iterator & lhs, const ustring::const_iterator & rhs) noexcept {
    return (lhs.base() < rhs.base());
}

bool operator>(const ustring::const_iterator & lhs, const ustring::const_iterator & rhs) noexcept {
    return (lhs.base() > rhs.base());
}

bool operator<=(const ustring::const_iterator & lhs, const ustring::const_iterator & rhs) noexcept {
    return (lhs.base() <= rhs.base());
}

bool operator>=(const ustring::const_iterator & lhs, const ustring::const_iterator & rhs) noexcept {
    return (lhs.base() >= rhs.base());
}

bool operator==(const ustring & lhs, const ustring & rhs) noexcept {
    return (lhs.compare(rhs) == 0);
}

bool operator==(const ustring & lhs, const char * rhs) noexcept {
    return (lhs.compare(rhs) == 0);
}

bool operator==(const char * lhs, const ustring & rhs) noexcept {
    return (rhs.compare(lhs) == 0);
}

bool operator!=(const ustring & lhs, const ustring & rhs) noexcept {
    return (lhs.compare(rhs) != 0);
}

bool operator!=(const ustring & lhs, const char * rhs) noexcept {
    return (lhs.compare(rhs) != 0);
}

bool operator!=(const char * lhs, const ustring & rhs) noexcept {
    return (rhs.compare(lhs) != 0);
}

bool operator<(const ustring & lhs, const ustring & rhs) noexcept {
    return (lhs.compare(rhs) < 0);
}

bool operator<(const ustring & lhs, const char * rhs) noexcept {
    return (lhs.compare(rhs) < 0);
}

bool operator<(const char * lhs, const ustring & rhs) noexcept {
    return (rhs.compare(lhs) > 0);
}

bool operator>(const ustring & lhs, const ustring & rhs) noexcept {
    return (lhs.compare(rhs) > 0);
}

bool operator>(const ustring & lhs, const char * rhs) noexcept {
    return (lhs.compare(rhs) > 0);
}

bool operator>(const char * lhs, const ustring & rhs) noexcept {
    return (rhs.compare(lhs) < 0);
}

bool operator<=(const ustring & lhs, const ustring & rhs) noexcept {
    return (lhs.compare(rhs) <= 0);
}

bool operator<=(const ustring & lhs, const char * rhs) noexcept {
    return (lhs.compare(rhs) <= 0);
}

bool operator<=(const char * lhs, const ustring & rhs) noexcept {
    return (rhs.compare(lhs) >= 0);
}

bool operator>=(const ustring & lhs, const ustring & rhs) noexcept {
    return (lhs.compare(rhs) >= 0);
}

bool operator>=(const ustring & lhs, const char * rhs) noexcept {
    return (lhs.compare(rhs) >= 0);
}

bool operator>=(const char * lhs, const ustring & rhs) noexcept {
    return (rhs.compare(lhs) <= 0);
}

ustring operator+(const ustring & lhs, const ustring & rhs) {
    ustring temp(lhs);
    temp += rhs;
    return temp;
}

ustring operator+(const ustring & lhs, const char * rhs) {
    ustring temp(lhs);
    temp += rhs;
    return temp;
}

ustring operator+(const char * lhs, const ustring & rhs) {
    ustring temp(lhs);
    temp += rhs;
    return temp;
}

ustring operator+(const ustring & lhs, char32_t rhs) {
    ustring temp(lhs);
    temp += rhs;
    return temp;
}

ustring operator+(char32_t lhs, const ustring & rhs) {
    ustring temp(1, lhs);
    temp += rhs;
    return temp;
}

ustring operator+(const ustring & lhs, char rhs) {
    ustring temp(lhs);
    temp += rhs;
    return temp;
}

ustring operator+(char lhs, const ustring & rhs) {
    ustring temp(1, lhs);
    temp += rhs;
    return temp;
}

ustring::operator std::u16string() const {
    std::u16string ws;

    for (const char * p = str_.c_str(); '\0' != *p; p = utf8_next(p)) {
        char32_t wc = char32_from_pointer(p);
        char16_t c1, c2;
        char32_to_surrogate(wc, c1, c2);
        ws.push_back(c1);
        if (0 != c2) { ws.push_back(c2); }
    }

    return ws;
}

ustring::operator std::u32string() const {
    return utf8_to_utf32(str_.data());
}

ustring::operator std::string_view() const noexcept {
    return str_.operator std::string_view();
}

std::istream & operator>>(std::istream & is, ustring & str) {
    std::string s;
    is >> s;
    str = s;
    return is;
}

// TODO Uncomment this in 0.8!
// std::ostream & operator<<(std::ostream & os, const ustring & str) {
//     os << Locale().iocharset().encode(str).data();
//     return os;
// }
std::ostream & operator<<(std::ostream & os, const ustring & str) {
    os << str.data();
    return os;
}

std::wostream & operator<<(std::wostream & os, const ustring & str) {
    os << std::wstring(str).data();
    return os;
}

} // namespace tau

tau::ustring operator""_tu(const char * s, std::size_t n) {
    return tau::ustring(s, 0, n);
}

//END

// ----------------------------------------------------------------------------
// SPDX-License-Identifier: BSD-2-Clause
// Copyright © 2014-2024 Konstantin Shmelkov <mrcashe@gmail.com>.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
// THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
// PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
// CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
// EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
// PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
// OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
// WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
// OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
// EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// ----------------------------------------------------------------------------

/// @file label-impl.cc The Label class implementation (Label_impl).

#include <tau/brush.hh>
#include <tau/glyph.hh>
#include <tau/string.hh>
#include <label-impl.hh>
#include <algorithm>
#include <cmath>
#include <iostream>

namespace tau {

Label_impl::Label_impl() {
    init();
    init_buffer();
}

Label_impl::Label_impl(Align xalign, Align yalign):
    xalign_(xalign),
    yalign_(yalign)
{
    init();
    init_buffer();
}

Label_impl::Label_impl(const ustring & s, Align xalign, Align yalign):
    xalign_(xalign),
    yalign_(yalign)
{
    init();
    init_buffer();
    assign(s);
}

Label_impl::Label_impl(const std::u32string & ws, Align xalign, Align yalign):
    xalign_(xalign),
    yalign_(yalign)
{
    init();
    init_buffer();
    assign(ws);
}

Label_impl::Label_impl(Buffer buf, Align xalign, Align yalign):
    xalign_(xalign),
    yalign_(yalign)
{
    init();
    set_buffer(buf);
}

void Label_impl::init() {
    conf().signal_changed(Conf::FOREGROUND).connect(bind_back(fun(this, &Label_impl::invalidate), Rect()));
    conf().signal_changed(Conf::FONT).connect(fun(this, &Label_impl::on_font_changed));
    conf().signal_changed(Conf::EDIT_FONT).connect(fun(this, &Label_impl::on_font_changed));
    signal_size_changed_.connect(fun(this, &Label_impl::on_size_changed));
    signal_show_.connect(fun(this, &Label_impl::update_vport));
    signal_hide_.connect(fun(this, &Label_impl::update_vport));
    signal_display_in_.connect(fun(this, &Label_impl::on_display_in));
    signal_focus_in_.connect(fun(this, &Label_impl::on_focus_in));
    signal_focus_out_.connect(fun(this, &Label_impl::on_focus_out));
    signal_enable_.connect(fun(this, &Label_impl::on_enable));
    signal_disable_.connect(fun(this, &Label_impl::on_disable));
    signal_offset_changed().connect(fun(this, &Label_impl::update_vport));
    signal_paint().connect(fun(this, &Label_impl::on_paint));
}

// Overridden by Text_impl.
// Overridden by Edit_impl.
void Label_impl::init_buffer() {
    insert_cx_ = buffer_.signal_insert().connect(fun(this, &Label_impl::on_buffer_insert));
    replace_cx_ = buffer_.signal_replace().connect(fun(this, &Label_impl::on_buffer_replace));
    erase_cx_ = buffer_.signal_erase().connect(fun(this, &Label_impl::on_buffer_erase));
}

void Label_impl::set_buffer(Buffer buf) {
    clear();
    buffer_ = buf;
    init_buffer();
    insert2_cx_.block();
    buffer_.signal_insert()(buffer_.cbegin(), buffer_.cend());
    insert2_cx_.unblock();
}

void Label_impl::assign(const std::u32string & ws) {
    if (1 != rows_.size() || ws != buffer_.wstr()) {
        clear_i(); buffer_.assign(ws);
        if (buffer_.empty()) { buffer_.signal_insert()(buffer_.cbegin(), buffer_.cend()); } // Force signal emission if empty buffer.
    }
}

void Label_impl::wipe_all() {
    if (auto pr = rnd_painter()) {
        pr.push(); pr.clear();
        pr.set_brush(conf().brush(Conf::BACKGROUND));
        pr.paint(); pr.pop();
    }
}

void Label_impl::clear_i() {
    wipe_all();
    buffer_.clear();
    rows_.clear();
    sel_.reset();
    esel_.reset();
}

// Overridden by Edit_impl.
void Label_impl::clear() {
    clear_i();
    update_requisition();
}

void Label_impl::set_spacing(unsigned spc) {
    if (spacing_ != spc) {
        spacing_ = spc;
        align_all();
    }
}

void Label_impl::text_align(Align xalign, Align yalign) {
    if (xalign_ != xalign || yalign_ != yalign) {
        xalign_ = xalign;
        yalign_ = yalign;
        align_all();
    }
}

void Label_impl::update_requisition() {
    if (buffer_.empty()) { require_size(xz_+text_size("|")); }
    else { require_size(xz_+Size(text_width_, text_height_)); }
}

Font Label_impl::select_font(Painter pr) {
    Font font;

    if (pr) {
        if (fonts_.front()) { font = fonts_.front(); pr.set_font(font); }
        else { font = pr.select_font(conf().font(font_item_)); }
    }

    return font;
}

Size Label_impl::text_size(const ustring & s) {
    Size size;

    if (Painter pr = rnd_painter()) {
        pr.set_font(fonts_.front());
        Vector v = pr.text_size(s);
        size.set(std::ceil(v.x()), font_height_);
    }

    return size;
}

Size Label_impl::text_size(const std::u32string & s) {
    Size size;

    if (Painter pr = rnd_painter()) {
        pr.set_font(fonts_.front());
        Vector v = pr.text_size(s);
        size.set(std::ceil(v.x()), font_height_);
    }

    return size;
}

void Label_impl::wrap(Wrap wrap) {
    if (wrap_ != wrap) {
        wrap_ = wrap;
        on_wrap();
    }
}

void Label_impl::update_vport() {
    vport_.set(offset(), size());
}

Rect Label_impl::row_bounds(std::size_t rn) const noexcept {
    if (rn < rows_.size()) {
        auto & row = rows_[rn];
        return Rect(0, oy_+row.ybase_-row.ascent_, size().width()-1, oy_+row.ybase_+row.descent_-1);
    }

    return Rect(0, text_height_, size().width()-1, text_height_+font_height_-1);
}

Painter Label_impl::wipe_area(int x1, int y1, int x2, int y2, Painter pr) {
    if (!pr) { pr = rnd_painter(); }

    if (pr) {
        pr.push();
        pr.clear();
        pr.rectangle(x1, y1, x2, y2);
        pr.set_brush(conf().brush(Conf::BACKGROUND));
        pr.fill();
        pr.pop();
    }

    return pr;
}

void Label_impl::align_all() {
    if (align(rows_.begin(), rows_.end())) {
        invalidate();
    }
}

void Label_impl::translate_rows(Iter i, Iter last, int dy) {
    if (last < i) { std::swap(i, last); }
    for (; i != rows_.end() && i <= last; ++i) { i->ybase_ += dy; }
}

void Label_impl::update_range(Buffer_citer b, Buffer_citer e) {
    if (b && e && b.row() < rows_.size() && e.row() < rows_.size()) {
        if (e < b) { std::swap(b, e); }
        if (0 == e.row()) { --e; }

        auto & row1 = rows_[b.row()], row2 = rows_[e.row()];
        int y1 = oy_+row1.ybase_-row1.ascent_;
        int y2 = oy_+row2.ybase_+row2.descent_;
        if (e.row() >= rows_.size()-1) { y2 = vport_.bottom(); }
        int x1 = vport_.left(), x2 = vport_.right();

        if (b.row() == e.row()) {
            x1 = x_at_col(b.row(), b.col());
            x2 = x_at_col(e.row(), e.col());
        }

        render(rnd_painter(), Rect(x1, y1, x2, y2));
    }
}

bool Label_impl::align(Iter i, Iter last) {
    bool changed = false;
    unsigned w = vport_.width();

    if (vport_) {
        if (i > last) { std::swap(i, last); }
        int ey = vport_.height()-text_height_, oy = 0;

        if (ey > 0) {
            if (Align::CENTER == yalign_) { oy = ey/2; }
            else if (Align::END == yalign_) { oy = ey; }
        }

        if (oy_ != oy) { oy_ = oy; changed = true; }

        for (; i != rows_.end() && i <= last; ++i) {
            int ex = w-i->width_, ox = 0;

            if (ex > 0) {
                if (Align::CENTER == xalign_) { ox = ex/2; }
                else if (Align::END == xalign_) { ox = ex; }
            }

            if (i->ox_ != ox) { i->ox_ = ox; changed = true; }
        }
    }

    return changed;
}

void Label_impl::calc_ellipsis(Iter i, Painter pr) {
    if ((Wrap::ELLIPSIZE_START == wrap_ || Wrap::ELLIPSIZE_CENTER == wrap_ || Wrap::ELLIPSIZE_END == wrap_) && i->cmax_ && vport_.width() && i->width_ > vport_.width()) {
        wrapped_ = true;
        pr.set_font(fonts_[0]);
        unsigned ew = std::ceil(pr.text_size(ellipsis_).x());
        int w = vport_.iwidth()-ew;
        std::size_t col;

        if (Wrap::ELLIPSIZE_RIGHT == wrap_) {
            for (col = 0; i->ofs_[col] < w && col < i->cmax_; ++col);
            i->e1_ = col-1;
        }

        else if (Wrap::ELLIPSIZE_CENTER == wrap_) {
            w /= 2;
            for (col = 0; i->ofs_[col] < w && col < i->cmax_; ++col);
            i->e1_ = col;
            for (col = i->cmax_-1; col && w > int(i->width_)-i->ofs_[col]; --col);
            i->e2_ = 1+col;
        }

        // Assume Wrap::ELLIPSIZE_START.
        else {
            for (col = i->cmax_-1; col && w > int(i->width_)-i->ofs_[col]; --col);
            i->e2_ = 1+col;
        }
    }
}

// Distribute words if there are free space.
// TODO Improve editing of such text.
void Label_impl::calc_fill(Iter i) {
    unsigned w = vport_.width();

    if (0 != i->cmax_ && i->width_ < w) {
        unsigned nb = std::max(std::size_t(1), i->blanks_.size()), wover = w-i->width_, wx = wover/nb, rem = wover%nb, x = 0;

        if (!i->blanks_.empty()) {
            auto k = i->blanks_.begin();

            for (auto j = i->ofs_.begin(); j != i->ofs_.end(); ++j) {
                if (j-i->ofs_.begin() == int(*k)) {
                    ++k; x += wx;
                    if (rem) { ++x; --rem; }
                }

                *j += x;
            }

            i->width_ = w;
        }

        else {
            for (auto & o: i->ofs_) { o += wx/2; }
            i->width_ += wx/2;
        }
    }
}

void Label_impl::calc_single_run(Iter i, Painter pr) {
    auto font = fonts_[0];
    pr.set_font(font);
    int x = 0;                  // Screen X coordinate.
    int cw = font.char_width(); // Monospace character width.
    auto e = buffer_.citer(i-rows_.begin(), 0), b = e; e.move_to(i->cmax_);
    auto s = b.wstr(e), wb = Align::FILL == xalign_ ? Locale().wblanks() : U"\t";
    i->ascent_ = std::max(i->ascent_, font.iascent());
    i->descent_ = std::max(i->descent_, font.idescent());
    std::size_t ichar = 0;      // Character index within string.
    std::size_t pos = 0;        // Position including TABs.
    std::size_t j, k, m;

    for (m = 0; m < i->cmax_; m = k) {
        int w = 0;
        k = std::min(i->cmax_, s.find_first_of(wb, m));

        // Monospace.
        if (cw) {
            for (j = 0; j < k-m; ++j) { i->ofs_[j+ichar] = cw*(j+pos); }
            w = cw*(k-m);
        }

        // Var pitch.
        else {
            auto ofs = pr.offsets(s.substr(m, k-m));
            w = ofs.back();
            std::transform(ofs.begin(), ofs.begin()+(k-m), i->ofs_.begin()+ichar, [x](int & xx) { return xx+x; });
        }

        pos += k-m, ichar += k-m;
        i->width_ += w; x += w;

        // Add Blank.
        if (k < i->cmax_) {
            i->blanks_.push_back(ichar);
            i->ofs_[ichar++] = x;

            if (U'\t' == s[k]) {
                j = tab_width_-(pos%tab_width_);
                w = cw ? j*cw : std::ceil(pr.text_size(ustring(j, U' ')).x());
            }

            else {
                j = 1;
                w = cw ? j*cw : std::ceil(pr.text_size(ustring(1, s[k])).x());
            }

            pos += j, ++k;
            i->width_ += w; x += w;
        }
    }

    if (x > 0) { i->width_ = 1+x; }
}

void Label_impl::calc(Iter i, Painter pr) {
    i->ascent_ = 0, i->descent_ = 0, i->width_ = 0, i->ox_ = 0, i->e1_ = 0, i->e2_ = 0;
    i->blanks_.clear();
    auto b = buffer_.citer(i-rows_.begin(), 0), e = b; e.move_to_eol();
    i->cmax_ = e-b;
    i->ofs_.assign(i->cmax_, 0);
    if (!pr) { pr = rnd_painter(); }

    if (pr && fonts_[0]) {
        calc_single_run(i, pr);
        calc_ellipsis(i, pr);
    }
}

void Label_impl::calc_all() {
    int ybase = 0;
    text_height_ = text_width_ = 0;
    wrapped_ = false;
    auto pr = rnd_painter();

    for (auto i = rows_.begin(); i != rows_.end(); ++i) {
        calc(i, pr);
        i->ybase_ = ybase+i->ascent_;
        ybase += i->ascent_+i->descent_+spacing_;
        text_height_ += i->ascent_+i->descent_;
        text_width_ = std::max(text_width_, i->width_);
    }

    if (Align::FILL == xalign_) {
        for (auto i = rows_.begin(); i != rows_.end(); ++i) {
            calc_fill(i);
        }
    }

    if (rows() > 1 && spacing_) { text_height_ += (rows()-1)*spacing_; }
    update_requisition();
    align_all();
}

int Label_impl::calc_height(Citer i, Citer last) {
    if (last < i) { std::swap(i, last); }
    int h = 0;

    for (; i != rows_.end() && i <= last; ) {
        h += i->ascent_+i->descent_;
        if (++i != rows_.end()) { h += spacing_; }
    }

    return h;
}

int Label_impl::calc_width(Citer i, Citer last) {
    if (last < i) { std::swap(i, last); }
    unsigned w = 0;
    for (; i != rows_.end() && i <= last; ++i) { w = std::max(w, i->width_); }
    return w;
}

int Label_impl::x_at_col(const Row & row, std::size_t col) const {
    return row.ox_+(col < row.cmax_ ? row.ofs_[col] : row.width_);
}

std::size_t Label_impl::col_at_x(const Row & row, int x) const {
    x -= row.ox_;
    if (x >= int(row.width_)) { return row.cmax_; }
    std::size_t n = 0;
    int x0 = 0, x1;

    if (x > 0) {
        for (; n < row.cmax_; ++n, x0 = x1) {
            x1 = n+1 < row.cmax_ ? row.ofs_[n+1] : row.width_;
            if (x >= x0 && x < x1) { break; }
        }
    }

    return n;
}

std::size_t Label_impl::row_at_y(int y) const noexcept {
    if (y >= 0) {
        auto i = at_y(y);
        return i != rows_.end() ? i-rows_.begin() : rows_.size()-1;
    }

    return 0;
}

Label_impl::Iter Label_impl::at_y(int y) noexcept {
    const int oy = y-oy_;
    return std::find_if(rows_.begin(), rows_.end(), [oy](auto & row) { return row.ybase_+row.descent_ >= oy; } );
}

Label_impl::Citer Label_impl::at_y(int y) const noexcept {
    const int oy = y-oy_;
    return std::find_if(rows_.begin(), rows_.end(), [oy](auto & row) { return row.ybase_+row.descent_ >= oy; } );
}

void Label_impl::render_ellipsized(Painter pr, Citer i) {
    std::size_t n = i-rows_.begin();
    int ybase = oy_+i->ybase_;
    wipe_area(vport_.left(), ybase-i->ascent_, vport_.right(), ybase+i->descent_, pr);
    select_font(pr);
    std::u32string s;
    if (i->e1_) { s.assign(buffer_.citer(n, 0).wstr(buffer_.citer(n, i->e1_))); }
    s += ellipsis_;
    if (i->e2_) { s += buffer_.citer(n, i->e2_).wstr(buffer_.citer(n, i->cmax_)); }
    int x = Align::LEFT == xalign_ ? 0 : std::max(0, int(vport_.iwidth()-std::ceil(pr.text_size(s).x())));
    if (Align::MIDDLE == xalign_) { x /= 2; }
    pr.move_to(x, ybase);
    pr.text(s, enabled() ? conf().color(Conf::FOREGROUND) : conf().brush(Conf::BACKGROUND).value().color().faded());
    pr.stroke();
}

void Label_impl::render(Painter pr, Citer i, std::size_t pos) {
    std::size_t n = i-rows_.begin();
    pos = std::min(pos, i->cmax_);
    std::size_t col = std::max(pos, col_at_x(*i, vport_.left()));            // Starting column.
    std::size_t ecol = std::min(i->cmax_, 1+col_at_x(*i, vport_.right()));   // Ending column.

    // Include modifiers before starting column.
    for (Buffer_citer c = buffer_.citer(n, col); col > 0; --col, --c) {
        if (!char32_is_modifier(*c)) { break; }
    }

    // Include modifiers after ending column.
    for (Buffer_citer c = buffer_.citer(n, ecol); c.col() < i->cmax_; ++ecol, ++c) {
        if (!char32_is_modifier(*c)) { break; }
    }

    Brush bg = conf().brush(Conf::BACKGROUND);

    if (sel_ && esel_ && (n > sel_.row() || (n == sel_.row() && col >= sel_.col())) && n <= esel_.row()) {
        bg = Brush(conf().color(Conf::SELECT_BACKGROUND));
    }

    int ybase = oy_+i->ybase_;
    int y1 = ybase-i->ascent_;
    int y2 = ybase+i->descent_;
    select_font(pr);

    if (col < ecol) {
        std::size_t col1 = ecol;

        while (col < col1) {
            auto j = std::find_if(i->blanks_.begin(), i->blanks_.end(), [col](auto n) { return n >= col; });
            std::size_t col2 = col1, blank = j != i->blanks_.end() ? *j : i->cmax_;

            if (sel_ && esel_) {
                if (sel_.row() == n && col < sel_.col()) {
                    col2 = std::min(col1, sel_.col());
                }

                else if (sel_.row() == n && col == sel_.col()) {
                    bg = Brush(conf().color(Conf::SELECT_BACKGROUND));
                    if (esel_.row() == n && col < esel_.col()) { col2 = std::min(col1, esel_.col()); }
                }

                else if (esel_.row() == n && col < esel_.col()) {
                    bg = Brush(conf().color(Conf::SELECT_BACKGROUND));
                    col2 = std::min(col1, esel_.col());
                }

                else if (esel_.row() == n && col >= esel_.col()) {
                    bg = conf().brush(Conf::BACKGROUND).value().color();
                }
            }

            col2 = std::min(col2, blank);
            int x1 = x_at_col(*i, col);

            if (col2 > col) {
                pr.rectangle(x1, y1, x_at_col(*i, col2), y2);
                pr.set_brush(bg); pr.fill();
                Color c = enabled() ? conf().color(Conf::FOREGROUND) : conf().brush(Conf::BACKGROUND).value().color().faded();
                pr.move_to(x1, ybase);
                auto b = buffer_.citer(n, col), e = buffer_.citer(n, col2);
                pr.text(b.wstr(e), c);
                pr.stroke();
                col += col2-col;
            }

            else {
                ++col;
                pr.rectangle(x1, y1, x_at_col(*i, col), y2);
                pr.set_brush(bg); pr.fill();
            }
        }
    }

    // Draw an empty rectangle after EOL.
    if (ecol >= i->cmax_) {
        if (esel_ && esel_.row() == n && esel_.col() <= i->cmax_) { bg = conf().brush(Conf::BACKGROUND); }
        pr.rectangle(i->ox_+i->width_, y1, vport_.right(), y2);
        pr.set_brush(bg);
        pr.fill();
    }
}

void Label_impl::render_pro(Painter pr, const Rect & r) {
    if (pr && vport_ && r) {
        auto i = at_y(r.top()), e = at_y(r.bottom());

        if (i != rows_.end()) {
            pr.push();

            do {
                if (!focused() && (i->e1_ || i->e2_)) { render_ellipsized(pr, i); }
                else { render(pr, i, col_at_x(*i, r.left())); }
            } while (i++ != e && i != rows_.end());

            pr.pop();
        }
    }
}

Buffer_citer Label_impl::iter(const Point & pt) const {
    std::size_t y = row_at_y(pt.y());
    std::size_t x = col_at_x(y, pt.x());
    return iter(y, x);
}

void Label_impl::update_font() {
    if (auto pr = rnd_painter()) {
        auto spec = conf().font(font_item_).spec();
        fonts_.front() = pr.select_font(spec);

        if (fonts_.front()) {
            font_height_ = fonts_.front().height();
            wspace_ = fonts_.front().char_width();
            if (0 == wspace_) { wspace_ = std::ceil(pr.text_size(U" ").x()); }
        }
    }
}

void Label_impl::change_font_style(Conf::Item fi) {
    font_item_ = fi;
    update_font();
}

void Label_impl::on_font_changed() {
    update_font();
    calc_all();
    invalidate();
}

void Label_impl::on_display_in() {
    change_font_style(font_item_);
    calc_all();
}

void Label_impl::on_size_changed() {
    update_vport();
    calc_all();
}

bool Label_impl::on_paint(Painter pr, const Rect & inval) {
    render(pr, inval);
    return true;
}

// Overridden by Text_impl.
void Label_impl::on_wrap() {
    calc_all();
    invalidate();
}

// Overridden by Text_impl.
void Label_impl::on_buffer_replace(Buffer_citer b, Buffer_citer e, const std::u32string & replaced) {
    if (b.row() == e.row()) {
        auto i = rows_.begin()+b.row();
        int h0 = i->ascent_+i->descent_;
        calc(i, rnd_painter());
        if (Align::FILL == xalign_) { calc_fill(i); }
        int h1 = i->ascent_+i->descent_;
        if (i->width_ < text_width_) { text_width_ = calc_width(i, rows_.end()); }
        e.move_to_eol();
        if (h1 != h0) { translate_rows(i+1, rows_.end(), h1-h0); e = buffer_.cend(); }
        update_requisition();
        align(i, i);
        update_range(b, e);
    }
}

// Overridden by Text_impl.
void Label_impl::on_buffer_erase(Buffer_citer b, Buffer_citer e, const std::u32string & erased) {
    if (auto pr = rnd_painter()) {
        if (e < b) { std::swap(b, e); }
        if (buffer_.empty()) { clear(); return; }
        auto i = rows_.begin()+b.row();

        if (e.row() == b.row()) {
            int h0 = i->ascent_+i->descent_;
            int y1 = oy_+i->ybase_-i->ascent_;
            int y2 = oy_+i->ybase_+i->descent_;

            calc(i, pr);
            if (Align::FILL == xalign_) { calc_fill(i); }
            int h1 = i->ascent_+i->descent_;
            y1 = std::min(y1, oy_+i->ybase_-i->ascent_);
            y2 = std::max(y2, oy_+i->ybase_+i->descent_);

            if (xalign_ != Align::START) {
                wipe_area(vport_.left(), y1, vport_.right(), y2, pr);
                b.move_to_sol();
            }

            if (i->width_ < text_width_) { text_width_ = calc_width(rows_.begin(), rows_.end()); }
            e.move_to_eol();
            if (h1 != h0) { translate_rows(i+1, rows_.end(), h1-h0); e = buffer_.cend(); }
        }

        else {
            auto j = rows_.begin()+e.row();
            int hdel = calc_height(i+1, j);
            rows_.erase(i, j);
            translate_rows(i, rows_.end(), -hdel);
            calc_all();
            e = buffer_.cend();
            wipe_area(vport_.left(), i->ybase_-i->ascent_, vport_.right(), vport_.bottom(), pr);
        }

        update_requisition();
        bool aligned = align(rows_.begin(), rows_.end());
        if (aligned) { b.move_to_sol(); e.move_to_eol(); }
        update_range(b, e);
    }
}

// Overridden by Text_impl.
void Label_impl::on_buffer_insert(Buffer_citer b, Buffer_citer e) {
    if (rows_.empty()) { rows_.emplace_back(); }
    if (e < b) { std::swap(b, e); }
    std::size_t nlines = e.row()-b.row();
    while (nlines--) { rows_.emplace(rows_.begin()+b.row()); }
    if (e.row() > b.row()) { e = buffer_.cend(); }
    if (Align::START != xalign_) { b.move_to_sol(); e.move_to_eol(); }
    calc_all();
    update_range(b, e);
}

void Label_impl::on_focus_in() {
    if (Wrap::NONE != wrap_) {
        on_wrap();
    }
}

void Label_impl::on_focus_out() {
    if (Wrap::NONE != wrap_) {
        on_wrap();
        offset(0, offset().y());
    }
}

void Label_impl::on_enable() {
    if (enabled() && !empty()) {
        invalidate();
    }
}

void Label_impl::on_disable() {
    if (!enabled() && !empty()) {
        invalidate();
    }
}

} // namespace tau

//END

// ----------------------------------------------------------------------------
// SPDX-License-Identifier: BSD-2-Clause
// Copyright © 2014-2024 Konstantin Shmelkov <mrcashe@gmail.com>.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
// THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
// PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
// CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
// EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
// PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
// OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
// WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
// OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
// EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// ----------------------------------------------------------------------------

#include <tau/fileman.hh>
#include <tau/navigator.hh>
#include <fileman-impl.hh>

namespace tau {

#define FILEMAN_IMPL (std::static_pointer_cast<Fileman_impl>(impl))

Fileman::Fileman(Mode mode, const ustring & path):
    Widget(std::make_shared<Fileman_impl>(mode, path))
{
}

Fileman::Fileman(const Fileman & other):
    Widget(other.impl)
{
}

Fileman & Fileman::operator=(const Fileman & other) {
    Widget::operator=(other);
    return *this;
}

Fileman::Fileman(Fileman && other):
    Widget(other.impl)
{
}

Fileman & Fileman::operator=(Fileman && other) {
    Widget::operator=(other);
    return *this;
}

Fileman::Fileman(Widget_ptr wp):
    Widget(std::dynamic_pointer_cast<Fileman_impl>(wp))
{
}

Fileman & Fileman::operator=(Widget_ptr wp) {
    Widget::operator=(std::dynamic_pointer_cast<Fileman_impl>(wp));
    return *this;
}

Navigator Fileman::navigator() noexcept {
    return Navigator(FILEMAN_IMPL->navigator());
}

Fileman::Mode Fileman::mode() const noexcept {
    return FILEMAN_IMPL->mode();
}

ustring Fileman::uri() const {
    return FILEMAN_IMPL->uri();
}

void Fileman::set_uri(const ustring & uri) {
    FILEMAN_IMPL->set_uri(uri);
}

std::vector<ustring> Fileman::selection() const {
    return FILEMAN_IMPL->selection();
}

ustring Fileman::entry() const {
    return FILEMAN_IMPL->entry();
}

void Fileman::add_filter(const ustring & patterns, const ustring & title) {
    FILEMAN_IMPL->add_filter(patterns, title);
}

ustring Fileman::filter() const {
    return FILEMAN_IMPL->filter();
}

void Fileman::set_option(std::string_view opt) {
    FILEMAN_IMPL->set_option(opt);
}

void Fileman::reset_option(std::string_view opt) {
    FILEMAN_IMPL->reset_option(opt);
}

bool Fileman::has_option(std::string_view opt) const noexcept {
    return FILEMAN_IMPL->has_option(opt);
}

std::string Fileman::options(char32_t sep) const {
    return FILEMAN_IMPL->options(sep);
}

void Fileman::allow_overwrite() {
    FILEMAN_IMPL->allow_overwrite();
}

void Fileman::disallow_overwrite() {
    FILEMAN_IMPL->disallow_overwrite();
}

bool Fileman::overwrite_allowed() const noexcept {
    return FILEMAN_IMPL->overwrite_allowed();
}

void Fileman::load_state(Key_file & kf, Key_section & sect) {
    FILEMAN_IMPL->load_state(kf, sect);
}

void Fileman::save_state(Key_file & kf, Key_section & sect) {
    FILEMAN_IMPL->save_state(kf, sect);
}

Action & Fileman::action_apply() {
    return FILEMAN_IMPL->action_apply();
}

Action & Fileman::action_cancel() {
    return FILEMAN_IMPL->action_cancel();
}

} // namespace tau

//END

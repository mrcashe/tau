// ----------------------------------------------------------------------------
// SPDX-License-Identifier: BSD-2-Clause
// Copyright © 2014-2024 Konstantin Shmelkov <mrcashe@gmail.com>.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
// THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
// PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
// CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
// EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
// PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
// OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
// WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
// OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
// EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// ----------------------------------------------------------------------------

/// @name cycle-base.cc The Cycle_base class implementation.

#include <tau/cycle.hh>
#include <cycle-text-impl.hh>

namespace tau {

#define CYCLE_IMPL (std::static_pointer_cast<Cycle_impl>(impl))

Cycle_base::Cycle_base(Widget_ptr wp):
    Container(wp)
{
}

Cycle_base & Cycle_base::operator=(Widget_ptr wp) {
    Container::operator=(std::dynamic_pointer_cast<Cycle_impl>(wp));
    return *this;
}

void Cycle_base::set_border_style(Border bs) {
    CYCLE_IMPL->set_border_style(bs);
}

Border Cycle_base::border_style() const noexcept {
    return CYCLE_IMPL->border_style();
}

bool Cycle_base::empty() const noexcept {
    return CYCLE_IMPL->empty();
}

std::size_t Cycle_base::count() const noexcept {
    return CYCLE_IMPL->count();
}

void Cycle_base::clear() {
    CYCLE_IMPL->clear();
}

Widget_ptr Cycle_base::widget(int id) {
    return CYCLE_IMPL->compound()->chptr(CYCLE_IMPL->widget(id));
}

Widget_cptr Cycle_base::widget(int id) const {
    return CYCLE_IMPL->compound()->chptr(CYCLE_IMPL->widget(id));
}

void Cycle_base::append(Widget & w, bool shrink) {
    CYCLE_IMPL->append(w.ptr(), shrink);
}

Widget_ptr Cycle_base::append(const ustring & text, unsigned margin_left, unsigned margin_right) {
    return CYCLE_IMPL->append(text, margin_left, margin_right);
}

Widget_ptr Cycle_base::append(const ustring & text, const Color & color, unsigned margin_left, unsigned margin_right) {
    return CYCLE_IMPL->append(text, color, margin_left, margin_right);
}

void Cycle_base::prepend(Widget & w, bool shrink) {
    CYCLE_IMPL->prepend(w.ptr(), shrink);
}

Widget_ptr Cycle_base::prepend(const ustring & text, unsigned margin_left, unsigned margin_right) {
    return CYCLE_IMPL->prepend(text, margin_left, margin_right);
}

Widget_ptr Cycle_base::prepend(const ustring & text, const Color & color, unsigned margin_left, unsigned margin_right) {
    return CYCLE_IMPL->prepend(text, color, margin_left, margin_right);
}

int Cycle_base::current() const noexcept {
    return CYCLE_IMPL->current();
}

bool Cycle_base::fixed() const noexcept {
    return CYCLE_IMPL->fixed();
}

void Cycle_base::unfix() {
    CYCLE_IMPL->unfix();
}

Action & Cycle_base::action_cancel() {
    return CYCLE_IMPL->action_cancel();
}

Action & Cycle_base::action_activate() {
    return CYCLE_IMPL->action_activate();
}

signal<void(int)> & Cycle_base::signal_select_id() {
    return CYCLE_IMPL->signal_select_id();
}

signal<void(int)> & Cycle_base::signal_new_id() {
    return CYCLE_IMPL->signal_new_id();
}

signal<void(int)> & Cycle_base::signal_remove_id() {
    return CYCLE_IMPL->signal_remove_id();
}

} // namespace tau

//END

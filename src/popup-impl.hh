// ----------------------------------------------------------------------------
// SPDX-License-Identifier: BSD-2-Clause
// Copyright © 2014-2024 Konstantin Shmelkov <mrcashe@gmail.com>.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
// THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
// PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
// CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
// EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
// PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
// OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
// WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
// OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
// EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// ----------------------------------------------------------------------------

#ifndef __TAU_POPUP_IMPL_HH__
#define __TAU_POPUP_IMPL_HH__

#include <window-impl.hh>

namespace tau {

class Popup_impl: public Window_impl {
protected:

    Popup_impl(Winface_ptr winface, const Point & upos, Window_ptr wpp, Gravity gravity);

public:

    // Overrides Widget_impl.
    Window_impl * toplevel() noexcept override;

    // Overrides Widget_impl.
    const Window_impl * toplevel() const noexcept override;

    Window_ptr parent_window() { return wpp_; }
    Window_cptr parent_window() const { return wpp_; }

    // Overrides Widget_impl.
    // Overrides Window_impl.
    Point to_screen(const Point & pt=Point()) const noexcept override;

    // Overrides Widget_impl.
    Point to_parent(const Container_impl * ci, const Point & pt=Point()) const noexcept override;

    // Overrides Widget_impl.
    // Overrides Window_impl.
    Point where_mouse() const noexcept override;


    void gravity(Gravity gravity);
    Gravity gravity() const { return gravity_; }

private:

    Point       upos_;
    Gravity     gravity_ = Gravity::TOP_LEFT;
    Window_ptr  wpp_;                   // An optional parent window.

private:

    void adjust(Hints);

    void on_show();
    void on_parent(Object * parent);
};

} // namespace tau

#endif // __TAU_POPUP_IMPL_HH__

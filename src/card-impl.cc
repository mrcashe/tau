// ----------------------------------------------------------------------------
// SPDX-License-Identifier: BSD-2-Clause
// Copyright © 2014-2024 Konstantin Shmelkov <mrcashe@gmail.com>.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
// THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
// PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
// CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
// EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
// PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
// OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
// WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
// OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
// EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// ----------------------------------------------------------------------------

#include <card-impl.hh>
#include <algorithm>
#include <iostream>

namespace tau {

Card_impl::Card_impl() {
    signal_arrange_.connect(fun(this, &Card_impl::arrange));
    signal_take_focus_.connect(fun(this, &Card_impl::on_take_focus));
    signal_display_in_.connect(bind_back(fun(this, &Card_impl::update_requisition), nullptr, Hints::SIZE));
    signal_size_changed_.connect(fun(this, &Card_impl::arrange));
}

void Card_impl::insert(Widget_ptr wp, bool take_focus) {
    chk_parent(wp);
    update_child_bounds(wp.get(), INT_MIN, INT_MIN);
    wp->hide();
    auto & hol = holders_.emplace_back();
    hol.wp = wp.get();
    hol.hints_cx = wp->signal_hints_changed().connect(tau::bind_front(fun(this, &Card_impl::update_requisition), wp.get()));
    make_child(wp);
    update_requisition(wp.get(), Hints::SIZE);
    queue_arrange();
    if (take_focus || 1 == holders_.size()) { wp->show(), wp->take_focus(); }
}

Widget_impl * Card_impl::current() {
    for (auto i = holders_.begin(); i != holders_.end(); ++i) {
        if (!i->wp->hidden() && i->wp != hiding_) {
            return i->wp;
        }
    }

    return nullptr;
}

const Widget_impl * Card_impl::current() const {
    for (auto i = holders_.begin(); i != holders_.end(); ++i) {
        if (!i->wp->hidden() && i->wp != hiding_) {
            return i->wp;
        }
    }

    return nullptr;
}

std::size_t Card_impl::remove_current() {
    return remove(current());
}

std::size_t Card_impl::remove_others() {
    std::size_t n = 0;

    if (auto wp = current()) {
        for (auto cp: children()) {
            if (cp.get() != wp) {
                n += remove(cp.get());
            }
        }
    }

    return n;
}

std::size_t Card_impl::remove(Widget_impl * wi) {
    auto i = std::find_if(holders_.begin(), holders_.end(), [wi](const Holder & hol) { return hol.wp == wi; } );

    if (i != holders_.end()) {
        std::list<Widget_impl *> v { wi };
        invalidate_children(v);
        wi->hide();
        holders_.erase(i);
        unparent(v);
        req_.reset();
        for (auto & hol: holders_) { req_ |= child_requisition(hol.wp); }
        require_size(req_);
        if (holders_.empty()) { invalidate(); }
        else { queue_arrange(); }
        return 1;
    }

    return 0;
}

void Card_impl::clear() {
    holders_.clear();
    req_.reset();
    unparent_all();
    require_size(0);
    invalidate();
}

Size Card_impl::child_requisition(const Widget_impl * wp) {
    Size req = wp->required_size();
    req.update(wp->size_hint(), true);
    req.update_max(wp->min_size_hint());
    req.update_min(wp->max_size_hint(), true);
    Margin m = wp->margin_hint();
    req.increase(m.left+m.right, m.top+m.bottom);
    return req;
}

void Card_impl::update_requisition(Widget_impl * wp, Hints op) {
    if (!in_shutdown()) {
        if (Hints::SHOW == op) {
            showing_ = wp;

            if (!hiding_) {
                for (auto & hol: holders_) {
                    if (hol.wp != wp) {
                        hol.wp->hide();
                    }
                }

                queue_arrange();
            }

            if (focused()) { wp->take_focus(); }
            showing_ = nullptr;
        }

        else if (Hints::HIDE == op) {
            hiding_ = wp;
            update_child_bounds(wp, INT_MIN, INT_MIN);

            if (!showing_) {
                if (holders_.size() > 1) {
                    auto i = std::find_if(holders_.begin(), holders_.end(), [wp](auto & hol) { return wp == hol.wp; });

                    if (i != holders_.end()) {
                        auto j = i++;    // keep iter.

                        if (i != holders_.end()) { // try to show next child.
                            i->wp->show();
                        }

                        else if (j != holders_.begin()) {
                            --j;
                            j->wp->show();
                        }
                    }
                }

                queue_arrange();
            }

            hiding_ = nullptr;
        }

        else {
            if (wp) { req_ |= child_requisition(wp); }
            require_size(req_);
        }
    }
}

void Card_impl::arrange() {
    auto i = std::find_if(holders_.begin(), holders_.end(), [](auto & hol) { return !hol.wp->hidden(); });

    if (i != holders_.end()) {
        Margin m = i->wp->margin_hint();
        if (update_child_bounds(i->wp, m.left, m.top, size()-Size(m.left+m.right, m.top+m.bottom))) { invalidate(); }
    }
}

void Card_impl::show_next() {
    if (holders_.size() > 1) {
        if (auto cw = current()) {
            for (auto i = holders_.begin(); i != holders_.end(); ++i) {
                if (i->wp == cw) {
                    if (++i != holders_.end()) { i->wp->show(); }
                    else { holders_.front().wp->show(); }
                    return;
                }
            }
        }
    }
}

void Card_impl::show_previous() {
    if (holders_.size() > 1) {
        if (auto cw = current()) {
            for (auto i = holders_.rbegin(); i != holders_.rend(); ++i) {
                const Holder & hol = *i;

                if (hol.wp == cw) {
                    if (++i != holders_.rend()) { i->wp->show(); }
                    else { holders_.back().wp->show(); }
                    return;
                }
            }
        }
    }
}

bool Card_impl::on_take_focus() {
    auto wp = current();
    if (wp && wp->take_focus()) { return true; }
    return grab_focus();
}

} // namespace tau

//END

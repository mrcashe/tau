// ----------------------------------------------------------------------------
// SPDX-License-Identifier: BSD-2-Clause
// Copyright © 2014-2024 Konstantin Shmelkov <mrcashe@gmail.com>.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
// THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
// PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
// CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
// EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
// PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
// OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
// WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
// OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
// EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// ----------------------------------------------------------------------------

/// @file frame-impl.cc The Frame_impl (wrapped Frame) class implementation.
/// Header is frame-impl.hh.

#include <tau/brush.hh>
#include <tau/painter.hh>
#include <tau/pen.hh>
#include <frame-impl.hh>
#include <label-impl.hh>
#include <numbers>
#include <iostream>

namespace tau {

Frame_impl::Frame_impl() {
    init();
}

Frame_impl::Frame_impl(const ustring & label, Align align) {
    init();
    init_border_style(Border::GROOVE, 0, 0, 0, 0, 0);
    set_label(label, align);
}

Frame_impl::Frame_impl(Border bs, unsigned border_width, unsigned border_radius) {
    init();
    init_border_style(bs, border_width, border_radius, border_radius, border_radius, border_radius);
}

Frame_impl::Frame_impl(Border bs, unsigned border_width, unsigned rtop_left, unsigned rtop_right, unsigned rbottom_right, unsigned rbottom_left) {
    init();
    init_border_style(bs, border_width, rtop_left, rtop_right, rbottom_right, rbottom_left);
}

Frame_impl::Frame_impl(Border bs, const Color & border_color, unsigned border_width, unsigned rtop_left, unsigned rtop_right, unsigned rbottom_right, unsigned rbottom_left) {
    init();
    init_border_style(bs, border_width, rtop_left, rtop_right, rbottom_right, rbottom_left);
    set_border_color(border_color);
}

Frame_impl::Frame_impl(Border bs, const Color & border_color, unsigned border_width, unsigned border_radius) {
    init();
    init_border_style(bs, border_width, border_radius, border_radius, border_radius, border_radius);
    set_border_color(border_color);
}

Frame_impl::Frame_impl(const ustring & label, Border bs, unsigned border_width, unsigned border_radius) {
    init();
    init_border_style(bs, border_width, border_radius, border_radius, border_radius, border_radius);
    set_label(label);
}

Frame_impl::Frame_impl(const ustring & label, Border bs, unsigned border_width, unsigned rtop_left, unsigned rtop_right, unsigned rbottom_right, unsigned rbottom_left) {
    init();
    init_border_style(bs, border_width, rtop_left, rtop_right, rbottom_right, rbottom_left);
    set_label(label);
}

Frame_impl::Frame_impl(const ustring & label, Border bs, const Color & border_color, unsigned border_width, unsigned border_radius) {
    init();
    init_border_style(bs, border_width, border_radius, border_radius, border_radius, border_radius);
    set_border_color(border_color);
    set_label(label);
}

Frame_impl::Frame_impl(const ustring & label, Border bs, const Color & border_color, unsigned border_width, unsigned rtop_left, unsigned rtop_right, unsigned rbottom_right, unsigned rbottom_left) {
    init();
    init_border_style(bs, border_width, rtop_left, rtop_right, rbottom_right, rbottom_left);
    set_border_color(border_color);
    set_label(label);
}

Frame_impl::Frame_impl(const ustring & label, Align align, Border bs, unsigned border_width, unsigned border_radius) {
    init();
    init_border_style(bs, border_width, border_radius, border_radius, border_radius, border_radius);
    set_label(label, align);
}

Frame_impl::Frame_impl(const ustring & label, Align align, Border bs, unsigned border_width, unsigned rtop_left, unsigned rtop_right, unsigned rbottom_right, unsigned rbottom_left) {
    init();
    init_border_style(bs, border_width, rtop_left, rtop_right, rbottom_right, rbottom_left);
    set_label(label, align);
}

Frame_impl::Frame_impl(const ustring & label, Align align, Border bs, const Color & border_color, unsigned border_width, unsigned border_radius) {
    init();
    init_border_style(bs, border_width, border_radius, border_radius, border_radius, border_radius);
    set_border_color(border_color);
    set_label(label, align);
}

Frame_impl::Frame_impl(const ustring & label, Align align, Border bs, const Color & border_color, unsigned border_width, unsigned rtop_left, unsigned rtop_right, unsigned rbottom_right, unsigned rbottom_left) {
    init();
    init_border_style(bs, border_width, rtop_left, rtop_right, rbottom_right, rbottom_left);
    set_border_color(border_color);
    set_label(label, align);
}

void Frame_impl::init() {
    signal_arrange_.connect(fun(this, &Frame_impl::arrange), true);
    // signal_size_changed_.connect(bind_back(fun(this, &Frame_impl::update_requisition), Hints::SIZE));
    signal_display_in_.connect(bind_back(fun(this, &Frame_impl::update_requisition), Hints::SIZE));
    signal_size_changed_.connect(fun(this, &Frame_impl::arrange));
    signal_paint().connect(fun(this, &Frame_impl::on_paint));
}

Frame_impl::~Frame_impl() {
    destroy();
    if (signal_border_changed_) { delete signal_border_changed_; }
}

unsigned Frame_impl::min_border_size(Border bs) {
    switch (bs) {
        case Border::SOLID:
        case Border::DOTTED:
        case Border::DASHED:
        case Border::INSET:
        case Border::OUTSET:
            return 1;

        case Border::GROOVE:
        case Border::RIDGE:
            return 2;

        case Border::DOUBLE:
            return 3;

        default: ;
    }

    return 0;
}

void Frame_impl::init_border_style(Border bs, unsigned width, unsigned rtop_left, unsigned rtop_right, unsigned rbottom_right, unsigned rbottom_left) {
    unsigned s = min_border_size(bs);
    set_border_style_priv(0 != s ? bs : Border::NONE);
    s = std::max(s, width);
    set_borders(s);
    set_radius(rtop_left, rtop_right, rbottom_right, rbottom_left);
    left_ = right_ = top_ = bottom_ = s;
}

void Frame_impl::insert(Widget_ptr wp) {
    if (wp) {
        chk_parent(wp);
        if (cp_) { unparent({ cp_ }); }
        cp_ = wp.get();
        make_child(wp);
        update_child_bounds(wp.get(), INT_MIN, INT_MIN);
        child_hints_cx_ = wp->signal_hints_changed().connect(fun(this, &Frame_impl::update_requisition));
        child_focus_cx_ = signal_take_focus().connect(fun(wp, &Widget_impl::take_focus));
        child_back_cx_ = wp->conf().signal_changed(Conf::BACKGROUND).connect(bind_back(fun(this, &Frame_impl::invalidate), Rect()));
        update_requisition();
        queue_arrange();
    }
}

void Frame_impl::clear() {
    if (auto wp = cp_) {
        child_hints_cx_.drop();
        child_focus_cx_.drop();
        child_back_cx_.drop();
        cp_ = nullptr;
        update_child_bounds(wp, INT_MIN, INT_MIN);
        unparent({ wp });
        update_requisition();
        invalidate();
    }
}

bool Frame_impl::border_visible() const noexcept {
    return  (Border::NONE != border_left_style_ && (eb_.left || left_)) ||
            (Border::NONE != border_right_style_ && (eb_.right || right_)) ||
            (Border::NONE != border_top_style_ && (eb_.top || top_)) ||
            (Border::NONE != border_bottom_style_ && (eb_.bottom || bottom_));
}

void Frame_impl::update_label_foreground() {
    if (label_) {
        Opt_color * color;

        switch (lpos_) {
            case Side::BOTTOM:  color = &border_bottom_color_; break;
            case Side::LEFT:    color = &border_left_color_; break;
            case Side::RIGHT:   color = &border_right_color_; break;
            default: color = &border_top_color_;
        }

        if (*color) { label_->conf().color(Conf::FOREGROUND) = **color; }
        else { label_->conf().set(Conf::FOREGROUND, conf().str(Conf::FOREGROUND)); }
    }
}

void Frame_impl::set_label(Widget_ptr wp, Align align) {
    if (wp) {
        chk_parent(wp);
        unset_label();
        label_ = wp.get();
        update_child_bounds(wp.get(), INT_MIN, INT_MIN);
        make_child(wp);
        update_label_foreground();
        label_hints_cx_ = wp->signal_hints_changed().connect(fun(this, &Frame_impl::on_label_requisition));
        align_label(align);
        update_requisition();
        queue_arrange();
    }
}

void Frame_impl::unset_label() {
    if (auto wp = label_) {
        lb_.reset();
        label_hints_cx_.drop();
        label_ = nullptr;
        update_child_bounds(wp, INT_MIN, INT_MIN);
        unparent({ wp });
        update_requisition();
        queue_arrange();
    }
}

void Frame_impl::set_label(const ustring & text, Align align) {
    unset_label();
    auto label = std::make_shared<Label_impl>(text);
    label->hint_margin(3, 3, 1, 1);
    set_label(label, align);
}

void Frame_impl::set_label(const ustring & label, Side label_pos, Align align) {
    set_label(label, align);
    move_label(label_pos);
}

void Frame_impl::set_label(Widget_ptr wp, Side label_pos, Align align) {
    set_label(wp, align);
    move_label(label_pos);
}

Size Frame_impl::child_requisition(Widget_impl * wp) {
    Size req;

    if (wp && !wp->hidden()) {
        req = wp->required_size();
        req.update(wp->size_hint(), true);
        req.update_max(wp->min_size_hint());
        req.update_min(wp->max_size_hint(), true);
        Margin m = wp->margin_hint();
        req.increase(m.left+m.right, m.top+m.bottom);
    }

    return req;
}

Margin Frame_impl::eborders() const {
    Margin m;

    unsigned rtl = rtop_left(), rtr = rtop_right(), rbr = rbottom_right(), rbl = rbottom_left();
    m.left = rtl || rbl ? std::min(std::max(rtl, rbl), left_) : left_;
    m.right = rtr || rbr ? std::min(std::max(rtr, rbr), right_) : right_;
    m.top = rtl || rtr ? std::min(std::max(rtl, rtr), bottom_) : top_;
    m.bottom = rbl || rbr ? std::min(std::max(rbl, rbr), bottom_) : bottom_;

    return m;
}

Margin Frame_impl::placement(const Margin & eb) const {
    Margin m;

    unsigned rtl = 0.71*double(rtop_left()), rtr = 0.71*double(rtop_right()), rbr = 0.71*double(rbottom_right()), rbl = 0.71*double(rbottom_left());
    m.left = std::max(eb.left, std::max(rtl, rbl));
    m.right = std::max(eb.right, std::max(rtr, rbr));
    m.top = std::max(eb.top, std::max(rtl, rtr));
    m.bottom = std::max(eb.bottom, std::max(rbl, rbr));

    return m;
}

void Frame_impl::update_requisition(Hints op) {
    if (Hints::SHOW == op) {
        update_requisition();
        queue_arrange();
    }

    else if (Hints::HIDE == op) {
        update_child_bounds(cp_, INT_MIN, INT_MIN);
        update_requisition();
        queue_arrange();
    }

    else {
        Margin eb = eborders(), pl = placement(eb);

        if (Size req = child_requisition(label_)) {
            if (Side::TOP == lpos_) { pl.top = std::max(pl.top, req.height()); }
            else if (Side::BOTTOM == lpos_) { pl.bottom = std::max(pl.bottom, req.height()); }
            else if (Side::LEFT == lpos_) { pl.left = std::max(pl.left, req.width()); }
            else if (Side::RIGHT == lpos_) { pl.right = std::max(pl.right, req.width()); }
        }

        Size rs(pl.left+pl.right, pl.top+pl.bottom);
        rs.increase(child_requisition(cp_));
        require_size(rs);
    }
}

void Frame_impl::arrange() {
    Size  csize(size()), req(child_requisition(label_));
    Point corigin;
    eb_ = eborders();
    Margin pl = placement(eb_);
    csize.decrease(pl.left+pl.right, pl.top+pl.bottom);
    corigin.translate(pl.left, pl.top);

    lleft_ = left_;
    lright_ = right_;
    ltop_ = top_;
    lbottom_ = bottom_;

    if (req) {
        unsigned w = std::min(req.width(), csize.width());
        unsigned h = std::min(req.height(), csize.height()/2);
        int x, y;

        if (Side::BOTTOM == lpos_) {
            unsigned new_bottom = std::max(pl.bottom, req.height());
            csize.decrease(0, new_bottom-pl.bottom);
            pl.bottom = new_bottom;
            x = corigin.x();
            y = size().height()-pl.bottom+(pl.bottom-req.height())/2;
            lbottom_ = std::max(lbottom_, req.height());

            if (w < csize.width()) {
                if (Align::CENTER == align_) { x += (csize.width()-w)/2; }
                else if (Align::END == align_) { x += csize.width()-w; }
            }
        }

        else if (Side::LEFT == lpos_) {
            unsigned new_left = std::max(pl.left, req.width());
            csize.decrease(new_left-pl.left, 0);
            corigin.translate(new_left-pl.left, 0);
            x = 0;
            y = corigin.y();
            lleft_ = std::max(lleft_, req.width());

            if (h < csize.height()) {
                if (Align::CENTER == align_) { y += (csize.height()-h)/2; }
                else if (Align::END == align_) { y += csize.height()-h; }
            }
        }

        else if (Side::RIGHT == lpos_) {
            unsigned new_right = std::max(pl.right, req.width());
            csize.decrease(new_right-pl.right, 0);
            pl.right = new_right;
            x = size().width()-pl.right;
            y = corigin.y();
            lright_ = std::max(lright_, req.width());

            if (h < csize.height()) {
                if (Align::CENTER == align_) { y += (csize.height()-h)/2; }
                else if (Align::END == align_) { y += csize.height()-h; }
            }
        }

        else {
            unsigned new_top = std::max(pl.top, req.height());
            csize.decrease(0, new_top-pl.top);
            corigin.translate(0, new_top-pl.top);
            pl.top = new_top;
            x = corigin.x();
            y = (pl.top-req.height())/2;
            ltop_ = std::max(ltop_, req.height());

            if (w < csize.width()) {
                if (Align::CENTER == align_) { x += (csize.width()-w)/2; }
                else if (Align::END == align_) { x += csize.width()-w; }
            }
        }

        Point origin(x, y);
        Size size(w, h);
        lb_.set(origin, size);
        Margin m = label_->margin_hint();
        origin.translate(m.left, m.top);
        size.decrease(m.left+m.right, m.top+m.bottom);
        if (update_child_bounds(label_, origin, size)) { invalidate(); }
    }

    if (cp_) {
        Margin m = cp_->margin_hint();
        corigin.translate(m.left, m.top);
        csize.decrease(m.left+m.right, m.top+m.bottom);
        if (update_child_bounds(cp_, corigin, csize)) { invalidate(); }
    }
}

void Frame_impl::on_label_requisition(Hints op) {
    if (Hints::SHOW == op) {
        update_requisition();
        queue_arrange();
    }

    else if (Hints::HIDE == op) {
        lb_.reset();
        update_child_bounds(label_, INT_MIN, INT_MIN);
        update_requisition();
        queue_arrange();
    }

    else {
        update_requisition();
        queue_arrange();
    }
}

void Frame_impl::align_label(Align align) {
    if (align_ != align) {
        align_ = align;

        if (label_) {
            arrange();
            if (signal_border_changed_) { signal_border_changed_->operator()(); }
        }
    }
}

void Frame_impl::render_border(Painter pr) {
    if (Size ws = size()) {
        static constexpr double DARKEN = 0.15, LITEN = 0.1;
        const unsigned rtl = rtop_left(), rtr = rtop_right(), rbr = rbottom_right(), rbl = rbottom_left();
        Color c, c2;
        Line ls = Line::SOLID;

        // ------- Left -------

        if (0 != eb_.left) {
            ls = Line::SOLID;
            int x0 = 0, y0 = (ltop_-eb_.top)/2, y1 = ws.height()-(lbottom_-eb_.bottom)/2, y2 = y1;

            if (lb_) {
                if (Side::LEFT == lpos_) { x0 = lb_.center().x()-(eb_.left/2); y2 = lb_.top(); }
                else if (Side::TOP == lpos_) { ++y0; }
            }

            c = border_left_color_ ? *border_left_color_ : conf().brush(Conf::BACKGROUND).value().color(), c2 = c;
            y0 += rtl;
            y2 = y2 == y1 ? y2-rbl : y2;
            y1 -= rbl;

            switch (border_left_style_) {
                case Border::DOTTED:
                    ls = Line::DOT;
                    goto a21;

                case Border::DASHED:
                    ls = Line::DASH;
                case Border::SOLID:
                    a21:
                    c = border_left_color_ ? *border_left_color_ : conf().color(Conf::FOREGROUND);
                    goto a1;

                case Border::OUTSET:
                    c.lighter(LITEN);
                    goto a1;

                case Border::INSET:
                    c.darker(DARKEN);
                a1:
                    pr.move_to(x0+eb_.left/2, y0); pr.line_to(x0+eb_.left/2, y2);
                    if (y2 != y1) { pr.move_to(x0+eb_.left/2, lb_.bottom()); pr.line_to(x0+eb_.left/2, y1); }
                    pr.set_pen(Pen(c, eb_.left, ls));
                    pr.stroke();
                    break;

                case Border::DOUBLE:
                    c = border_left_color_ ? *border_left_color_ : conf().color(Conf::FOREGROUND);
                    pr.move_to(x0+eb_.left/3, y0); pr.line_to(x0+eb_.left/3, y2);
                    pr.move_to(x0+eb_.left/3+eb_.left/3+eb_.left/3, y0); pr.line_to(x0+eb_.left/3+eb_.left/3+eb_.left/3, y2);

                    if (y2 != y1) {
                        pr.move_to(x0+eb_.left/3, lb_.bottom()); pr.line_to(x0+eb_.left/3, y1);
                        pr.move_to(x0+eb_.left/3+eb_.left/3+eb_.left/3, lb_.bottom()); pr.line_to(x0+eb_.left/3+eb_.left/3+eb_.left/3, y1);
                    }

                    pr.set_pen(Pen(c, eb_.left/3, ls));
                    pr.stroke();
                    break;

                case Border::GROOVE:
                    c.darker(DARKEN);
                    c2.lighter(LITEN);
                    goto b1;

                case Border::RIDGE:
                    c.lighter(LITEN);
                    c2.darker(DARKEN);
                b1:
                    pr.move_to(x0+eb_.left/4, y0); pr.line_to(x0+eb_.left/4, y2);
                    if (y2 != y1) { pr.move_to(x0+eb_.left/4, lb_.bottom()); pr.line_to(x0+eb_.left/4, y1); }
                    pr.set_pen(Pen(c, eb_.left/2, ls));
                    pr.stroke();

                    pr.move_to(x0+eb_.left/2+eb_.left/4, y0); pr.line_to(x0+eb_.left/2+eb_.left/4, y2);
                    if (y2 != y1) { pr.move_to(x0+eb_.left/2+eb_.left/4, lb_.bottom()); pr.line_to(x0+eb_.left/2+eb_.left/4, y1); }
                    pr.set_pen(Pen(c2, eb_.left/2, ls));
                    pr.stroke();
                    break;

                default:
                    ;
            }
        }

        // ------- Right -------

        if (0 != eb_.right) {
            ls = Line::SOLID;
            int x0 = ws.width()-eb_.right, y0 = (ltop_-eb_.top)/2, y1 = ws.height()-(lbottom_-eb_.bottom)/2, y2 = y1;

            if (lb_) {
                if (Side::RIGHT == lpos_) { y2 = lb_.top(); x0 = lb_.center().x()-(eb_.right/4); }
                else if (Side::TOP == lpos_) { ++y0; }

            }

            c = border_right_color_ ? *border_right_color_ : conf().brush(Conf::BACKGROUND).value().color(), c2 = c;
            y0 += rtr;
            y2 = y2 == y1 ? y2-rbr : y2;
            y1 -= rbr;

            switch (border_right_style_) {
                case Border::DOTTED:
                    ls = Line::DOT;
                    goto a11;

                case Border::DASHED:
                    ls = Line::DASH;
                case Border::SOLID:
                    a11:
                    c = border_right_color_ ? *border_right_color_ : conf().color(Conf::FOREGROUND);
                    goto a2;

                case Border::OUTSET:
                    c.darker(DARKEN);
                    goto a2;

                case Border::INSET:
                    c.lighter(LITEN);
                    a2:
                    pr.move_to(x0+eb_.right/2, y0); pr.line_to(x0+eb_.right/2, y2);
                    if (y2 != y1) { pr.move_to(x0+eb_.right/2, lb_.bottom()); pr.line_to(x0+eb_.right/2, y1); }
                    pr.set_pen(Pen(c, eb_.right, ls));
                    pr.stroke();
                    break;

                case Border::DOUBLE:
                    c = border_right_color_ ? *border_right_color_ : conf().color(Conf::FOREGROUND);
                    pr.move_to(x0+eb_.right/3-1, y0); pr.line_to(x0+eb_.right/3-1, y2);
                    pr.move_to(x0+eb_.right/3+eb_.right/3+eb_.right/3-1, y0); pr.line_to(x0+eb_.right/3+eb_.right/3+eb_.right/3-1, y2);

                    if (y2 != y1) {
                        pr.move_to(x0+eb_.right/3, lb_.bottom()); pr.line_to(x0+eb_.right/3, y1);
                        pr.move_to(x0+eb_.right/3+eb_.right/3+eb_.right/3, lb_.bottom()); pr.line_to(x0+eb_.right/3+eb_.right/3+eb_.right/3, y1);
                    }

                    pr.set_pen(Pen(c, eb_.right/3));
                    pr.stroke();
                    break;

                case Border::GROOVE:
                    c.lighter(LITEN);
                    c2.darker(DARKEN);
                    goto b2;

                case Border::RIDGE:
                    c.darker(DARKEN);
                    c2.lighter(LITEN);
                    b2:
                    pr.move_to(x0+eb_.right/4, y0); pr.line_to(x0+eb_.right/4, y2);
                    if (y2 != y1) { pr.move_to(x0+eb_.right/4, lb_.bottom()); pr.line_to(x0+eb_.right/4, y1); }
                    pr.set_pen(Pen(c, eb_.right/2));
                    pr.stroke();

                    pr.move_to(x0+eb_.right/2+eb_.right/4, y0); pr.line_to(x0+eb_.right/2+eb_.right/4, y2);
                    if (y2 != y1) { pr.move_to(x0+eb_.right/2+eb_.right/4, lb_.bottom()); pr.line_to(x0+eb_.right/2+eb_.right/4, y1); }
                    pr.set_pen(Pen(c2, eb_.right/2));
                    pr.stroke();
                    break;

                default:
                    ;
            }
        }

        // -------- Top --------

        if (0 != eb_.top) {
            ls = Line::SOLID;
            int x0 = (lleft_-eb_.left)/2, x1 = ws.width()-(lright_-eb_.right)/2, x2 = x1, y0 = 0;

            if (lb_ && Side::TOP == lpos_) {
                x2 = lb_.left();
                y0 = lb_.center().y()-(eb_.top/2);
            }

            c = border_top_color_ ? *border_top_color_ : conf().brush(Conf::BACKGROUND).value().color(), c2 = c;

            x0 += rtl;
            x2 = x2 == x1 ? x2-rtr : x2;
            x1 -= rtr;

            switch (border_top_style_) {
                case Border::DOTTED:
                    ls = Line::DOT;
                    goto a8;

                case Border::DASHED:
                    ls = Line::DASH;
                case Border::SOLID:
                    a8:
                    c = border_top_color_ ? *border_top_color_ : conf().color(Conf::FOREGROUND);
                    goto a3;

                case Border::INSET:
                    c.darker(DARKEN);
                    goto a3;

                case Border::OUTSET:
                    c.lighter(LITEN);
                    a3:
                    pr.move_to(x0, y0+eb_.top/2); pr.line_to(x2, y0+eb_.top/2);
                    if (x2 != x1) { pr.move_to(lb_.right(), y0+eb_.top/2); pr.line_to(x1, y0+eb_.top/2); }
                    if (rtl) { pr.arc(rtl+lleft_/2, y0+rtl+eb_.top/2, rtl, std::numbers::pi, std::numbers::pi/2.0); }
                    if (rtr) { pr.arc(ws.width()-rtr-lright_/2, y0+rtr+eb_.top/2, rtr, 0, std::numbers::pi/2.0); }
                    pr.set_pen(Pen(c, eb_.top, ls));
                    pr.stroke();
                    break;

                case Border::DOUBLE:
                    c = border_top_color_ ? *border_top_color_ : conf().color(Conf::FOREGROUND);
                    pr.move_to(x0, y0+eb_.top/3); pr.line_to(x2, y0+eb_.top/3);
                    pr.move_to(x0, y0+eb_.top/3+eb_.top/3+eb_.top/3); pr.line_to(x2, y0+eb_.top/3+eb_.top/3+eb_.top/3);

                    if (x2 != x1) {
                        pr.move_to(lb_.right(), y0+eb_.top/3); pr.line_to(x1, y0+eb_.top/3);
                        pr.move_to(lb_.right(), y0+eb_.top/3+eb_.top/3+eb_.top/3); pr.line_to(x1, y0+eb_.top/3+eb_.top/3+eb_.top/3);
                    }

                    if (rtl) {
                        pr.arc(rtl+(lleft_-eb_.left)/2+eb_.left/3, y0+rtl+eb_.top/3, rtl, std::numbers::pi, std::numbers::pi/2.0);
                        pr.arc(rtl+(lleft_-eb_.left)/2+eb_.left/3, y0+rtl+eb_.top/3, rtl-eb_.top/3-eb_.top/3, std::numbers::pi, std::numbers::pi/2.0);
                    }

                    if (rtr) {
                        pr.arc(ws.width()-rtr-(lright_-eb_.top)/2-1, y0+rtr+eb_.right/3, rtr, 0, std::numbers::pi/2.0);
                        pr.arc(ws.width()-rtr-(lright_-eb_.top)/2-1, y0+rtr+eb_.right/3, rtr-eb_.top/3-eb_.top/3, 0, std::numbers::pi/2.0);
                    }

                    pr.set_pen(Pen(c, eb_.right/3));
                    pr.stroke();
                    break;

                case Border::GROOVE:
                    c.darker(DARKEN);
                    c2.lighter(LITEN);
                    goto b3;

                case Border::RIDGE:
                    c.lighter(LITEN);
                    c2.darker(DARKEN);
                    b3:
                    y0 += eb_.top;
                    pr.move_to(x0, y0+eb_.top/4); pr.line_to(x2, y0+eb_.top/4);
                    if (x2 != x1) { pr.move_to(lb_.right(), y0+eb_.top/4); pr.line_to(x1, y0+eb_.top/4); }
                    pr.set_pen(Pen(c, eb_.right/2));
                    pr.stroke();

                    pr.move_to(x0, y0+eb_.top/2+eb_.top/4); pr.line_to(x2, y0+eb_.top/2+eb_.top/4);
                    if (x2 != x1) { pr.move_to(lb_.right(), y0+eb_.top/2+eb_.top/4); pr.line_to(x1, y0+eb_.top/2+eb_.top/4); }
                    pr.set_pen(Pen(c2, eb_.top/2));
                    pr.stroke();

                    if (rtl|rtr) {
                        int r2 = (71*eb_.top)/100;
                        if (rtl) { pr.arc(rtl+(lleft_-eb_.left)/2, y0+rtl, rtl-r2/4, std::numbers::pi, std::numbers::pi/2.0); }
                        if (rtr) { pr.arc(ws.width()-rtr-1-(lright_-eb_.right)/2, y0+rtr, rtr-r2/4, 0, std::numbers::pi/2.0); }
                        pr.set_pen(Pen(c, eb_.top/2));
                        pr.stroke();

                        if (rtl) { pr.arc(rtl+(lleft_-eb_.left)/2, y0+rtl, rtl-r2, std::numbers::pi, std::numbers::pi/2.0); }
                        if (rtr) { pr.arc(ws.width()-rtr-1-(lright_-eb_.right)/2, y0+rtr, rtr-r2, 0, std::numbers::pi/2.0); }
                        pr.set_pen(Pen(c2, eb_.top/2));
                        pr.stroke();
                    }

                    break;

                default:
                    ;
            }
        }

        // -------- Bottom --------

        if (0 != eb_.bottom) {
            int x0 = (lleft_-eb_.left)/2, x1 = ws.width()-(lright_-eb_.right)/2, x2 = x1, y0 = ws.height()-1-eb_.bottom/2;
            ls = Line::SOLID;

            if (lb_ && Side::BOTTOM == lpos_) {
                y0 = lb_.center().y()-(eb_.bottom/2);
                x2 = lb_.left();
            }

            c = border_bottom_color_ ? *border_bottom_color_ : conf().brush(Conf::BACKGROUND).value().color(), c2 = c;
            x0 += rbl;
            x2 = x2 == x1 ? x2-rbr : x2;
            x1 -= rbr;

            switch (border_bottom_style_) {
                case Border::DOTTED:
                    ls = Line::DOT;
                    goto a7;

                case Border::DASHED:
                    ls = Line::DASH;
                case Border::SOLID:
                    a7:
                    c = border_bottom_color_ ? *border_bottom_color_ : conf().color(Conf::FOREGROUND);
                    goto a4;

                case Border::INSET:
                    c.lighter(LITEN);
                    goto a4;

                case Border::OUTSET:
                    c.darker(DARKEN);
                    a4:
                    pr.move_to(x0, y0+eb_.bottom/2); pr.line_to(x2, y0+eb_.bottom/2);
                    if (x2 != x1) { pr.move_to(lb_.right(), y0+eb_.bottom/2); pr.line_to(x1, y0+eb_.bottom/2); }
                    if (rbl) { pr.arc(x0-eb_.left, y0-rbl+eb_.bottom/2, rbl, std::numbers::pi, 3.0*std::numbers::pi/2.0); }
                    if (rbr) { pr.arc(x1+eb_.right, y0-rbr+eb_.bottom/2, rbr, 0, -std::numbers::pi/2.0); }
                    pr.set_pen(Pen(c, eb_.bottom, ls));
                    pr.stroke();
                    break;

                case Border::DOUBLE:
                    c = border_bottom_color_ ? *border_bottom_color_ : conf().color(Conf::FOREGROUND);
                    pr.move_to(x0, y0+eb_.bottom/3); pr.line_to(x2, y0+eb_.bottom/3);
                    pr.move_to(x0, y0+eb_.bottom/3+eb_.bottom/3+eb_.bottom/3); pr.line_to(x2, y0+eb_.bottom/3+eb_.bottom/3+eb_.bottom/3);

                    if (x2 != x1) {
                        pr.move_to(lb_.right(), y0+eb_.bottom/3); pr.line_to(x1, y0+eb_.bottom/3);
                        pr.move_to(lb_.right(), y0+eb_.bottom/3+eb_.bottom/3+eb_.bottom/3); pr.line_to(x1, y0+eb_.bottom/3+eb_.bottom/3+eb_.bottom/3);
                    }

                    if (rbl) {
                        pr.arc(rbl+(lleft_-eb_.left+eb_.left)/2, y0-rbl+eb_.bottom, rbl, std::numbers::pi, 3.0*std::numbers::pi/2.0);
                        pr.arc(rbl+(lleft_-eb_.left+eb_.left)/2, y0-rbl+eb_.bottom, rbl-eb_.bottom/3-eb_.bottom/3, std::numbers::pi, 3.0*std::numbers::pi/2.0);
                    }

                    if (rbr) {
                        pr.arc(ws.width()-rbr-(lright_-eb_.right)/2-1, y0-rbl+eb_.bottom, rbr, 0, -std::numbers::pi/2.0);
                        pr.arc(ws.width()-rbr-(lright_-eb_.right)/2-1, y0-rbl+eb_.bottom, rbr-eb_.bottom/3-eb_.bottom/3, 0, -std::numbers::pi/2.0);
                    }

                    pr.set_pen(Pen(c, eb_.right/3));
                    pr.stroke();
                    break;

                case Border::GROOVE:
                    c.lighter(LITEN);
                    c2.darker(DARKEN);
                    goto b4;

                case Border::RIDGE:
                    c.darker(DARKEN);
                    c2.lighter(LITEN);
                    b4:
                    pr.move_to(x0, y0+eb_.bottom/4); pr.line_to(x2, y0+eb_.bottom/4);
                    if (x2 != x1) { pr.move_to(lb_.right(), y0+eb_.bottom/4); pr.line_to(x1, y0+eb_.bottom/4); }
                    pr.set_pen(Pen(c, eb_.right/2));
                    pr.stroke();

                    pr.move_to(x0, y0+eb_.bottom/2+eb_.bottom/4); pr.line_to(x2, y0+eb_.bottom/2+eb_.bottom/4);
                    if (x2 != x1) { pr.move_to(lb_.right(), y0+eb_.bottom/2+eb_.bottom/4); pr.line_to(x1, y0+eb_.bottom/2+eb_.bottom/4); }
                    pr.set_pen(Pen(c2, eb_.bottom/2));
                    pr.stroke();

                    if (rbl|rbr) {
                        int r2 = (71*eb_.bottom)/100;
                        int yl = y0-rbl+eb_.bottom/2+eb_.bottom/4, yr = y0-rbr+eb_.bottom/2+eb_.bottom/4;
                        if (rbl) { pr.arc(rbl+(lleft_-eb_.left)/2, yl, rbl-r2/4, std::numbers::pi, 3.0*std::numbers::pi/2.0); }
                        if (rbr) { pr.arc(ws.width()-rbr-1-(lright_-eb_.right)/2, yr, rbr-r2/4, 2.0*std::numbers::pi, 3.0*std::numbers::pi/2.0); }
                        pr.set_pen(Pen(c2, eb_.bottom/2));
                        pr.stroke();

                        if (rbl) { pr.arc(rbl+(lleft_-eb_.left)/2, yl, rbl-r2, std::numbers::pi, 3.0*std::numbers::pi/2.0); }
                        if (rbr) { pr.arc(ws.width()-rbr-1-(lright_-eb_.right)/2, yr, rbr-r2, 2.0*std::numbers::pi, 3.0*std::numbers::pi/2.0); }
                        pr.set_pen(Pen(c, eb_.bottom/2));
                        pr.stroke();
                    }

                    break;

                default:
                    ;
            }
        }
    }
}

void Frame_impl::render_background(Painter pr, const Rect & inval) {
    Size z = size();
    const unsigned rtl = rtop_left(), rtr = rtop_right(), rbr = rbottom_right(), rbl = rbottom_left();

    if (rtl|rtr|rbr|rbl) {
        if (container_) {
            pr.set_brush(container_->conf().brush(Conf::BACKGROUND));
            pr.paint();
        }

        // Draw arcs.
        if (rtl) { pr.arc(rtl, rtl, rtl, std::numbers::pi, std::numbers::pi/2.0, true); }
        if (rtr) { pr.arc(z.width()-rtr, rtr, rtr, 0, std::numbers::pi/2.0, true); }
        if (rbr) { pr.arc(z.width()-rbr, z.height()-rbr, rbr, 0, -std::numbers::pi/2.0, true); }
        if (rbl) { pr.arc(rbl, z.height()-rbl, rbl, std::numbers::pi, 3.0*std::numbers::pi/2.0, true); }

        // Top rectangle(s).
        if (rtl|rtr) {
            pr.rectangle(rtl, 0, z.width()-rtr, rtl);
            if (rtl != rtr) { pr.rectangle(rtl < rtr ? 0 : rtl, std::min(rtl, rtr), z.width()-(rtr > rtl ? rtr : 0), std::max(rtl, rtr)); }
        }

        // Bottom rectangle(s).
        if (rbl|rbr) {
            pr.rectangle(rbl, z.height()-std::min(rbl, rbr), z.width()-rbr, z.height());
            if (rbl != rbr) { pr.rectangle(rbl < rbr ? 0 : rbl, z.height()-std::max(rbl, rbr), z.width()-(rbr > rbl ? rbr : 0), z.height()-std::min(rbl, rbr)); }
        }

        // Middle rectangle.
        int y0 = std::max(rtl, rtr), y1 = z.height()-std::max(rbl, rbr);
        if (y1 > y0) { pr.rectangle(0, y0, z.width(), y1); }
    }

    else {
        pr.rectangle(lleft_, ltop_, z.width()-lright_, z.height()-lbottom_);
    }

    // Fill all.
    bool use_child_bg = cp_ && border_visible() && cp_->conf().is_set(Conf::BACKGROUND);
    pr.set_brush(use_child_bg ? cp_->conf().brush(Conf::BACKGROUND) : conf().brush(Conf::BACKGROUND));
    pr.fill();
}

// Overrides Widget_impl.
bool Frame_impl::on_backpaint(Painter pr, const Rect & inval) {
    if (((rtop_left_|rtop_right_|rbottom_right_|rbottom_left_) && conf().is_set(Conf::BACKGROUND)) || (cp_ && border_visible() && cp_->conf().is_set(Conf::BACKGROUND))) {
        render_background(pr, inval);
    }

    else {
        Widget_impl::on_backpaint(pr, inval);
    }

    return false;
}

bool Frame_impl::on_paint(Painter pr, const Rect & inval) {
    render_border(pr);
    return false;
}

void Frame_impl::move_label(Side label_pos) {
    if (lpos_ != label_pos) {
        lpos_ = label_pos;

        if (label_) {
            update_label_foreground();
            lb_.reset();
            queue_arrange();
            if (signal_border_changed_) { signal_border_changed_->operator()(); }
        }
    }
}

bool Frame_impl::set_left_style(Border bs) {
    if (border_left_style_ != bs) {
        unsigned px = min_border_size(bs);
        border_left_style_ = 0 != px ? bs : Border::NONE;
        set_left_border(std::max(left_, px));
        return true;
    }

    return false;
}

bool Frame_impl::set_right_style(Border bs) {
    if (border_right_style_ != bs) {
        unsigned px = min_border_size(bs);
        border_right_style_ = 0 != px ? bs : Border::NONE;
        set_right_border(std::max(right_, px));
        return true;
    }

    return false;
}

bool Frame_impl::set_top_style(Border bs) {
    if (border_top_style_ != bs) {
        unsigned px = min_border_size(bs);
        border_top_style_ = 0 != px ? bs : Border::NONE;
        set_top_border(std::max(top_, px));
        return true;
    }

    return false;
}

bool Frame_impl::set_bottom_style(Border bs) {
    if (border_bottom_style_ != bs) {
        unsigned px = min_border_size(bs);
        border_bottom_style_ = 0 != px ? bs : Border::NONE;
        set_bottom_border(std::max(bottom_, px));
        return true;
    }

    return false;
}

bool Frame_impl::set_style(Border bs) {
    bool changed = false;
    if (set_left_style(bs)) { changed = true; }
    if (set_right_style(bs)) { changed = true; }
    if (set_top_style(bs)) { changed = true; }
    if (set_bottom_style(bs)) { changed = true; }
    return changed;
}

void Frame_impl::set_border_left_style(Border bs) {
    if (set_left_style(bs)) {
        queue_arrange();
        if (signal_border_changed_) { signal_border_changed_->operator()(); }
        invalidate();
    }
}

void Frame_impl::set_border_right_style(Border bs) {
    if (set_right_style(bs)) {
        queue_arrange();
        if (signal_border_changed_) { signal_border_changed_->operator()(); }
        invalidate();
    }
}

void Frame_impl::set_border_top_style(Border bs) {
    if (set_top_style(bs)) {
        queue_arrange();
        if (signal_border_changed_) { signal_border_changed_->operator()(); }
        invalidate();
    }
}

void Frame_impl::set_border_bottom_style(Border bs) {
    if (set_bottom_style(bs)) {
        queue_arrange();
        if (signal_border_changed_) { signal_border_changed_->operator()(); }
        invalidate();
    }
}

void Frame_impl::set_border_style(Border left, Border right, Border top, Border bottom) {
    bool changed = false;

    if (set_left_style(left)) { changed = true; }
    if (set_right_style(right)) { changed = true; }
    if (set_top_style(top)) { changed = true; }
    if (set_bottom_style(bottom)) { changed = true; }

    if (changed) {
        queue_arrange();
        if (signal_border_changed_) { signal_border_changed_->operator()(); }
        invalidate();
    }
}

// Overridden by Counter_impl.
void Frame_impl::set_border_style_priv(Border bs) {
    if (set_style(bs)) {
        queue_arrange();
        if (signal_border_changed_) { signal_border_changed_->operator()(); }
        invalidate();
    }
}

bool Frame_impl::set_left_color(const Color & color) {
    if (!border_left_color_ || *border_left_color_ != color) {
        border_left_color_ = color;
        return 0 != eb_.left;
    }

    return false;
}

bool Frame_impl::set_right_color(const Color & color) {
    if (!border_right_color_ || *border_right_color_ != color) {
        border_right_color_ = color;
        return 0 != eb_.right;
    }

    return false;
}

bool Frame_impl::set_top_color(const Color & color) {
    if (!border_top_color_ || *border_top_color_ != color) {
        border_top_color_ = color;
        return 0 != eb_.top;
    }

    return false;
}

bool Frame_impl::set_bottom_color(const Color & color) {
    if (!border_bottom_color_ || *border_bottom_color_ != color) {
        border_bottom_color_ = color;
        return 0 != eb_.bottom;
    }

    return false;
}

bool Frame_impl::set_colors(const Color & color) {
    bool changed = set_left_color(color);
    if (set_right_color(color)) { changed = true; }
    if (set_top_color(color)) { changed = true; }
    if (set_bottom_color(color)) { changed = true; }
    return changed;
}

void Frame_impl::set_border_left_color(const Color & color) {
    if (set_left_color(color)) {
        if (signal_border_changed_) { signal_border_changed_->operator()(); }
        update_label_foreground();
        invalidate();
    }
}

void Frame_impl::set_border_right_color(const Color & color) {
    if (set_right_color(color)) {
        if (signal_border_changed_) { signal_border_changed_->operator()(); }
        update_label_foreground();
        invalidate();
    }
}

void Frame_impl::set_border_top_color(const Color & color) {
    if (set_top_color(color)) {
        if (signal_border_changed_) { signal_border_changed_->operator()(); }
        update_label_foreground();
        invalidate();
    }
}

void Frame_impl::set_border_bottom_color(const Color & color) {
    if (set_bottom_color(color)) {
        if (signal_border_changed_) { signal_border_changed_->operator()(); }
        update_label_foreground();
        invalidate();
    }
}

void Frame_impl::set_border_color(const Color & color) {
    if (set_colors(color)) {
        if (signal_border_changed_) { signal_border_changed_->operator()(); }
        update_label_foreground();
        invalidate();
    }
}

void Frame_impl::unset_border_color() {
    bool changed = false;

    if (border_left_color_) {
        border_left_color_.reset();
        if (0 != eb_.left) { changed = true; }
    }

    if (border_right_color_) {
        border_right_color_.reset();
        if (0 != eb_.right) { changed = true; }
    }

    if (border_top_color_) {
        border_top_color_.reset();
        if (0 != eb_.top) { changed = true; }
    }

    if (border_bottom_color_) {
        border_bottom_color_.reset();
        if (0 != eb_.bottom) { changed = true; }
    }

    if (changed) {
        update_label_foreground();
        if (signal_border_changed_) { signal_border_changed_->operator()(); }
        invalidate();
    }
}

void Frame_impl::unset_border_left_color() {
    if (border_left_color_) {
        border_left_color_.reset();

        if (0 != eb_.left) {
            update_label_foreground();
            if (signal_border_changed_) { signal_border_changed_->operator()(); }
            invalidate();
        }
    }
}

void Frame_impl::unset_border_right_color() {
    if (border_right_color_) {
        border_right_color_.reset();

        if (0 != eb_.right) {
            update_label_foreground();
            if (signal_border_changed_) { signal_border_changed_->operator()(); }
            invalidate();
        }
    }
}

void Frame_impl::unset_border_top_color() {
    if (border_top_color_) {
        border_top_color_.reset();

        if (0 != eb_.top) {
            update_label_foreground();
            if (signal_border_changed_) { signal_border_changed_->operator()(); }
            invalidate();
        }
    }
}

void Frame_impl::unset_border_bottom_color() {
    if (border_bottom_color_) {
        border_bottom_color_.reset();

        if (0 != eb_.bottom) {
            update_label_foreground();
            if (signal_border_changed_) { signal_border_changed_->operator()(); }
            invalidate();
        }
    }
}

void Frame_impl::set_border_color(const Color & left, const Color & right, const Color & top, const Color & bottom) {
    bool changed = set_left_color(left);
    if (set_right_color(right)) { changed = true; }
    if (set_top_color(top)) { changed = true; }
    if (set_bottom_color(bottom)) { changed = true; }

    if (changed) {
        update_label_foreground();
        if (signal_border_changed_) { signal_border_changed_->operator()(); }
        invalidate();
    }
}

bool Frame_impl::set_left_border(unsigned px) {
    px = ceil_border(px, border_left_style_);

    if (left_ != px) {
        left_ = px;
        return true;
    }

    return false;
}

bool Frame_impl::set_right_border(unsigned px) {
    px = ceil_border(px, border_right_style_);

    if (right_ != px) {
        right_ = px;
        return true;
    }

    return false;
}

bool Frame_impl::set_top_border(unsigned px) {
    px = ceil_border(px, border_top_style_);

    if (top_ != px) {
        top_ = px;
        return true;
    }

    return false;
}

bool Frame_impl::set_bottom_border(unsigned px) {
    px = ceil_border(px, border_bottom_style_);

    if (bottom_ != px) {
        bottom_ = px;
        return true;
    }

    return false;
}

bool Frame_impl::set_borders(unsigned px) {
    bool changed = set_left_border(px);
    if (set_right_border(px)) { changed = true; }
    if (set_top_border(px)) { changed = true; }
    if (set_bottom_border(px)) { changed = true; }
    return changed;
}

void Frame_impl::set_border_left(unsigned px) {
    if (set_left_border(px)) {
        update_requisition();
        if (signal_border_changed_) { signal_border_changed_->operator()(); }
        queue_arrange();
    }
}

void Frame_impl::set_border_right(unsigned px) {
    if (set_right_border(px)) {
        update_requisition();
        if (signal_border_changed_) { signal_border_changed_->operator()(); }
        queue_arrange();
    }
}

void Frame_impl::set_border_top(unsigned px) {
    if (set_top_border(px)) {
        update_requisition();
        if (signal_border_changed_) { signal_border_changed_->operator()(); }
        queue_arrange();
    }
}

void Frame_impl::set_border_bottom(unsigned px) {
    if (set_bottom_border(px)) {
        update_requisition();
        if (signal_border_changed_) { signal_border_changed_->operator()(); }
        queue_arrange();
    }
}

void Frame_impl::set_border(unsigned px) {
    if (set_borders(px)) {
        update_requisition();
        if (signal_border_changed_) { signal_border_changed_->operator()(); }
        queue_arrange();
    }
}

void Frame_impl::set_border(unsigned left, unsigned right, unsigned top, unsigned bottom) {
    bool changed = false;
    if (set_left_border(left)) { changed = true; }
    if (set_right_border(right)) { changed = true; }
    if (set_top_border(top)) { changed = true; }
    if (set_bottom_border(bottom)) { changed = true; }

    if (changed) {
        update_requisition();
        if (signal_border_changed_) { signal_border_changed_->operator()(); }
        queue_arrange();
    }
}

void Frame_impl::set_border(unsigned px, Border bs) {
    bool changed = set_style(bs);
    if (set_borders(px)) { changed = true; }

    if (changed) {
        update_requisition();
        if (signal_border_changed_) { signal_border_changed_->operator()(); }
        queue_arrange();
    }
}

void Frame_impl::set_border(unsigned px, Border bs, unsigned rtop_left, unsigned rtop_right, unsigned rbottom_right, unsigned rbottom_left) {
    bool changed = set_style(bs);
    if (set_borders(px)) { changed = true; }
    if (set_radius(rtop_left, rtop_right, rbottom_right, rbottom_left)) { changed = true; }

    if (changed) {
        update_requisition();
        if (signal_border_changed_) { signal_border_changed_->operator()(); }
        queue_arrange();
    }
}

void Frame_impl::set_border(unsigned px, Border bs, const Color & color) {
    bool changed = set_style(bs);
    if (set_borders(px)) { changed = true; }
    if (set_colors(color)) { changed = true; }

    if (changed) {
        update_requisition();
        if (signal_border_changed_) { signal_border_changed_->operator()(); }
        queue_arrange();
    }

    else {
        invalidate();
    }
}

void Frame_impl::set_border(unsigned px, Border bs, const Color & color, unsigned rtop_left, unsigned rtop_right, unsigned rbottom_right, unsigned rbottom_left) {
    bool changed = set_style(bs);
    if (set_borders(px)) { changed = true; }
    if (set_colors(color)) { changed = true; }
    if (set_radius(rtop_left, rtop_right, rbottom_right, rbottom_left)) { changed = true; }

    if (changed) {
        update_requisition();
        if (signal_border_changed_) { signal_border_changed_->operator()(); }
        queue_arrange();
    }

    else {
        invalidate();
    }
}

void Frame_impl::set_border_left(unsigned px, Border bs) {
    bool changed = false;
    if (set_left_style(bs)) { changed = true; }
    if (set_left_border(px)) { changed = true; }

    if (changed) {
        update_requisition();
        if (signal_border_changed_) { signal_border_changed_->operator()(); }
        queue_arrange();
    }
}

void Frame_impl::set_border_right(unsigned px, Border bs) {
    bool changed = false;
    if (set_right_style(bs)) { changed = true; }
    if (set_right_border(px)) { changed = true; }

    if (changed) {
        update_requisition();
        if (signal_border_changed_) { signal_border_changed_->operator()(); }
        queue_arrange();
    }
}

void Frame_impl::set_border_top(unsigned px, Border bs) {
    bool changed = false;
    if (set_top_style(bs)) { changed = true; }
    if (set_top_border(px)) { changed = true; }

    if (changed) {
        update_requisition();
        if (signal_border_changed_) { signal_border_changed_->operator()(); }
        queue_arrange();
    }
}

void Frame_impl::set_border_bottom(unsigned px, Border bs) {
    bool changed = false;
    if (set_bottom_style(bs)) { changed = true; }
    if (set_bottom_border(px)) { changed = true; }

    if (changed) {
        update_requisition();
        if (signal_border_changed_) { signal_border_changed_->operator()(); }
        queue_arrange();
    }
}

void Frame_impl::set_border_left(unsigned px, Border bs, const Color & color) {
    bool changed = false;
    if (set_left_style(bs)) { changed = true; }
    if (set_left_border(px)) { changed = true; }
    if (set_left_color(color)) { changed = true; }

    if (changed) {
        update_requisition();
        if (signal_border_changed_) { signal_border_changed_->operator()(); }
        queue_arrange();
    }

    else {
        invalidate();
    }
}

void Frame_impl::set_border_right(unsigned px, Border bs, const Color & color) {
    bool changed = false;
    if (set_right_style(bs)) { changed = true; }
    if (set_right_border(px)) { changed = true; }
    if (set_right_color(color)) { changed = true; }

    if (changed) {
        update_requisition();
        if (signal_border_changed_) { signal_border_changed_->operator()(); }
        queue_arrange();
    }

    else {
        invalidate();
    }
}

void Frame_impl::set_border_top(unsigned px, Border bs, const Color & color) {
    bool changed = false;
    if (set_top_style(bs)) { changed = true; }
    if (set_top_border(px)) { changed = true; }
    if (set_top_color(color)) { changed = true; }

    if (changed) {
        update_requisition();
        if (signal_border_changed_) { signal_border_changed_->operator()(); }
        queue_arrange();
    }

    else {
        invalidate();
    }
}

void Frame_impl::set_border_bottom(unsigned px, Border bs, const Color & color) {
    bool changed = false;
    if (set_bottom_style(bs)) { changed = true; }
    if (set_bottom_border(px)) { changed = true; }
    if (set_bottom_color(color)) { changed = true; }

    if (changed) {
        update_requisition();
        if (signal_border_changed_) { signal_border_changed_->operator()(); }
        queue_arrange();
    }

    else {
        invalidate();
    }
}

unsigned Frame_impl::ceil_border(unsigned px, Border bs) {
    unsigned min_px = min_border_size(bs);
    if (min_px < 2) { return px; }
    unsigned quo = px/min_px, rem = px%min_px;
    return rem ? (1+quo)*min_px : px;
}

bool Frame_impl::set_radius_top_left(unsigned radius) {
    if (rtop_left_ != radius) {
        rtop_left_ = radius;
        return true;
    }

    return false;
}

bool Frame_impl::set_radius_top_right(unsigned radius) {
    if (rtop_right_ != radius) {
        rtop_right_ = radius;
        return true;
    }

    return false;
}

bool Frame_impl::set_radius_bottom_right(unsigned radius) {
    if (rbottom_right_ != radius) {
        rbottom_right_ = radius;
        return true;
    }

    return false;
}

bool Frame_impl::set_radius_bottom_left(unsigned radius) {
    if (rbottom_left_ != radius) {
        rbottom_left_ = radius;
        return true;
    }

    return false;
}

bool Frame_impl::set_radius(unsigned rtop_left, unsigned rtop_right, unsigned rbottom_right, unsigned rbottom_left) {
    bool changed = set_radius_top_left(rtop_left);
    if (set_radius_top_right(rtop_right)) { changed = true; }
    if (set_radius_bottom_right(rbottom_right)) { changed = true; }
    if (set_radius_bottom_left(rbottom_left)) { changed = true; }
    return changed;
}

// Overriden by Button_base_impl.
void Frame_impl::set_border_top_left_radius(unsigned radius) {
    if (set_radius_top_left(radius)) {
        update_requisition();
        queue_arrange();
        if (signal_border_changed_) { signal_border_changed_->operator()(); }
    }
}

// Overriden by Button_base_impl.
void Frame_impl::set_border_top_right_radius(unsigned radius) {
    if (set_radius_top_right(radius)) {
        update_requisition();
        queue_arrange();
        if (signal_border_changed_) { signal_border_changed_->operator()(); }
    }
}

// Overriden by Button_base_impl.
void Frame_impl::set_border_bottom_left_radius(unsigned radius) {
    if (set_radius_bottom_left(radius)) {
        update_requisition();
        queue_arrange();
        if (signal_border_changed_) { signal_border_changed_->operator()(); }
    }
}

// Overriden by Button_base_impl.
void Frame_impl::set_border_bottom_right_radius(unsigned radius) {
    if (set_radius_bottom_right(radius)) {
        update_requisition();
        queue_arrange();
        if (signal_border_changed_) { signal_border_changed_->operator()(); }
    }
}

// Overriden by Button_base_impl.
void Frame_impl::set_border_radius(unsigned radius) {
    if (set_radius(radius, radius, radius, radius)) {
        update_requisition();
        queue_arrange();
        if (signal_border_changed_) { signal_border_changed_->operator()(); }
    }
}

// Overriden by Button_base_impl.
void Frame_impl::set_border_radius(unsigned top_left, unsigned top_right, unsigned bottom_right, unsigned bottom_left) {
    bool changed = set_radius_top_left(top_left);
    if (set_radius_top_right(top_right)) { changed = true; }
    if (set_radius_bottom_right(bottom_right)) { changed = true; }
    if (set_radius_bottom_left(bottom_left)) { changed = true; }

    if (changed) {
        update_requisition();
        queue_arrange();
        if (signal_border_changed_) { signal_border_changed_->operator()(); }
    }
}

unsigned Frame_impl::rtop_left() const {
    return std::min(rtop_left_, size().min()/2);
}

unsigned Frame_impl::rtop_right() const {
    return std::min(rtop_right_, size().min()/2);
}

unsigned Frame_impl::rbottom_right() const {
    return std::min(rbottom_right_, size().min()/2);
}

unsigned Frame_impl::rbottom_left() const {
    return std::min(rbottom_left_, size().min()/2);
}

signal<void()> & Frame_impl::signal_border_changed() {
    if (!signal_border_changed_) { signal_border_changed_ = new signal<void()>; }
    return *signal_border_changed_;
}

} // namespace tau

//END

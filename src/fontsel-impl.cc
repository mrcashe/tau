// ----------------------------------------------------------------------------
// SPDX-License-Identifier: BSD-2-Clause
// Copyright © 2014-2024 Konstantin Shmelkov <mrcashe@gmail.com>.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
// THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
// PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
// CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
// EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
// PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
// OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
// WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
// OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
// EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// ----------------------------------------------------------------------------

#include <tau/exception.hh>
#include <tau/font.hh>
#include <tau/language.hh>
#include <tau/painter.hh>
#include <button-impl.hh>
#include <box-impl.hh>
#include <check-impl.hh>
#include <counter-impl.hh>
#include <entry-impl.hh>
#include <frame-impl.hh>
#include <fontsel-impl.hh>
#include <icon-impl.hh>
#include <list-text-impl.hh>
#include <loop-impl.hh>
#include <roller-impl.hh>
#include <scroller-impl.hh>
#include <slider-impl.hh>
#include <spinner-impl.hh>
#include <iostream>

namespace tau {

Fontsel_impl::Fontsel_impl():
    Table_impl(4)
{
    init();
}

Fontsel_impl::Fontsel_impl(std::string_view spec, const ustring & sample):
    Table_impl(4),
    sample_(sample)
{
    init();
    select(spec);
}

void Fontsel_impl::init() {
    set_column_margin(4, 6, 0);

    Frame_ptr frame = std::make_shared<Frame_impl>(Border::RIDGE);
    put(frame, 0, 0, 3, 1);
    Box_ptr box1 = std::make_shared<Box_impl>(Orientation::RIGHT, 4);
    frame->set_label(box1);
    auto spinner = std::make_shared<Spinner_impl>();
    spinner_ = spinner.get();
    box1->append(spinner, true);
    ifamilies_ = lgettext("Families");
    auto tp = std::make_shared<Label_impl>(ifamilies_);
    famlabel_ = tp.get();
    box1->append(tp, true);
    spinner_->hide();

    auto roller = std::make_shared<Roller_impl>();
    frame->insert(roller);
    auto families = std::make_shared<List_text_impl>();
    families_ = families.get();
    roller->insert(families);
    families_->signal_select_text().connect(fun(this, &Fontsel_impl::on_family_selected));
    families_->signal_activate_text().connect(fun(this, &Fontsel_impl::on_family_activated));
    families_->action_cancel().disable();
    families_->hint_min_size(0, 192);
    roller->signal_focus_in().connect(bind_back(fun(this, &Fontsel_impl::on_list_focus), roller.get(), true));
    roller->signal_focus_out().connect(bind_back(fun(this, &Fontsel_impl::on_list_focus), roller.get(), false));

    frame = std::make_shared<Frame_impl>(lgettext("Face"), Align::CENTER, Border::RIDGE);
    put(frame, 3, 0, 2, 1);
    auto table = std::make_shared<Table_impl>();
    frame->insert(table);
    auto faces = std::make_shared<List_text_impl>();
    faces_ = faces.get();
    table->put(faces, 0, 0);
    faces_->signal_select_text().connect(fun(this, &Fontsel_impl::on_face_selected));
    faces_->signal_activate_text().connect(fun(this, &Fontsel_impl::on_face_activated));
    faces_->action_cancel().disable();
    table->signal_focus_in().connect(bind_back(fun(this, &Fontsel_impl::on_list_focus), table.get(), true));
    table->signal_focus_out().connect(bind_back(fun(this, &Fontsel_impl::on_list_focus), table.get(), false));

    box1 = std::make_shared<Box_impl>(4);
    put(box1, 0, 1, 3, 1, false, true);
    auto ck = std::make_shared<Check_impl>();
    monocheck_ = ck.get();
    monocheck_->disallow_focus();
    monocheck_->signal_check().connect(bind_back(fun(this, &Fontsel_impl::on_monospace), true));
    monocheck_->signal_uncheck().connect(bind_back(fun(this, &Fontsel_impl::on_monospace), false));
    box1->append(ck);
    tp = std::make_shared<Label_impl>(lgettext("Show monospace fonts only"), Align::START);
    box1->append(tp);

    auto counter = std::make_shared<Counter_impl>(10, 200, 1);
    counter_ = counter.get();
    counter_->disable();
    counter_->prepend(lgettext("Size")+':', 2, 4);
    counter_->append("pt", { "Blue" }, 4, 2);
    auto icon = std::make_shared<Icon_impl>(zin_, Icon::SMALL);
    icon->hint_margin(4, 0, 0, 0);
    counter_->append(icon, true);
    icon = std::make_shared<Icon_impl>(zout_, Icon::SMALL);
    icon->hint_margin(2, 2, 0, 0);
    counter_->append(icon, true);
    counter_->signal_value_changed().connect(fun(this, &Fontsel_impl::on_counter_value_changed));
    put(counter, 3, 1, 2, 1, false, true);

    frame = std::make_shared<Frame_impl>(lgettext("Sample")+':', Align::CENTER, Border::RIDGE);
    frame->hint_margin(0, 0, 4, 4);
    put(frame, 0, 2, 5, 1, false, true);

    auto entry = std::make_shared<Entry_impl>(Align::CENTER, Border::NONE);
    entry_ = entry.get();
    entry_->hint_margin(3);
    entry_->signal_hints_changed().connect(fun(this, &Fontsel_impl::on_sample_requisition_changed), true);
    entry_->hint_min_size(0, 52);
    entry_->hint_max_size(0, 122);
    entry_->signal_activate().connect(fun(this, &Fontsel_impl::on_entry_activate));
    entry_->signal_focus_in().connect(fun(entry_, &Entry_impl::select_all));
    entry_->signal_focus_out().connect(fun(entry_, &Entry_impl::unselect));
    entry_->signal_changed().connect(fun(this, &Fontsel_impl::on_entry_changed));
    entry_->assign(sample_.empty() ? Language().sample() : sample_);
    entry_->set_tooltip(lgettext("Click on the sample text to edit it"));
    frame->insert(entry);

    ustring hint = lgettext("Selected font specification");
    tp = std::make_shared<Label_impl>(lgettext("Specification")+':', Align::START);
    put(tp, Align::START, Align::CENTER, 0, 13, 1, 1, true, true);
    tp->set_tooltip(hint);
    tp = std::make_shared<Text_impl>(Align::END);
    fontspec_ = tp.get();
    fontspec_->wrap(Wrap::ELLIPSIZE_MIDDLE);
    put(tp, 1, 13, 1, 1, false, true);
    fontspec_->set_tooltip(hint);

    hint = lgettext("PostScript font name");
    tp = std::make_shared<Label_impl>("PostScript:", Align::START);
    put(tp, Align::START, Align::CENTER, 0, 14, 1, 1, true, true);
    tp->set_tooltip(hint);
    tp = std::make_shared<Text_impl>(Align::END);
    psname_ = tp.get();
    psname_->wrap(Wrap::ELLIPSIZE_MIDDLE);
    put(tp, 1, 14, 1, 1, false, true);
    psname_->set_tooltip(hint);

    hint = lgettext("Normal (Default) system font");
    tp = std::make_shared<Label_impl>(lgettext("Normal")+':', Align::START);
    put(tp, Align::START, Align::CENTER, 2, 13, 1, 1, true, true);
    tp->set_tooltip(hint);
    tp = std::make_shared<Text_impl>(Font::normal(), Align::END);
    tp->set_tooltip(hint);
    tp->wrap(Wrap::ELLIPSIZE_CENTER);
    normal_ = tp.get();
    put(tp, 3, 13, 1, 1, false, true);

    hint = lgettext("Default system monospace font");
    tp = std::make_shared<Label_impl>(lgettext("Monospace")+':', Align::START);
    put(tp, Align::START, Align::CENTER, 2, 14, 1, 1, true, true);
    tp->set_tooltip(hint);
    tp = std::make_shared<Text_impl>(Font::mono(), Align::END);
    tp->set_tooltip(hint);
    tp->wrap(Wrap::ELLIPSIZE_CENTER);
    mono_ = tp.get();
    put(tp, 3, 14, 1, 1, false, true);

    set_column_margin(2, 16, 0);
    auto apply = std::make_shared<Button_impl>(apply_, Action::ALL);
    apply->allow_focus();
    put(apply, Align::FILL, Align::FILL, 4, 13, 1, 1, true, true);
    auto btn = std::make_shared<Button_impl>(cancel_, Action::ALL);
    put(btn, Align::FILL, Align::FILL, 4, 14, 1, 1, true, true);

    apply_.disable();
    apply_.connect(fun(this, &Fontsel_impl::on_apply));
    zin_.connect(fun(counter_, &Counter_impl::increase));
    zout_.connect(fun(counter_, &Counter_impl::decrease));
    cancel_.connect(fun(this, &Widget_impl::quit_dialog));

    connect_action(zin_);
    connect_action(zout_);
    connect_action(cancel_);

    focus_after(faces_, families_);
    focus_after(counter_, faces_);
    focus_after(entry_, counter_);
    focus_after(apply.get(), entry_);
    focus_after(families_, apply.get());

    signal_display_in_.connect(fun(this, &Fontsel_impl::on_display));
    signal_take_focus_.connect(fun(families_, &Widget_impl::take_focus), true);
    signal_size_changed_.connect(fun(this, &Fontsel_impl::on_size_changed));
}

void Fontsel_impl::on_display() {
    ustring s = uspec_.empty() ? Font::normal() : uspec_;
    aspec_ = spec_ = s;
    enable_apply(aspec_ != uspec_);
    fontspec_->assign(spec_);
    update_families();
    select(s);
    update_entry();
    update_tooltips();
}

void Fontsel_impl::reset_famlabel() {
    famlabel_->assign(ifamilies_);
}

void Fontsel_impl::on_next_family(const ustring & fam) {
    // End of async load.
    if (fam.empty()) {
        families_->unset_cursor();
        spinner_->stop();
        spinner_->hide();
        if (!families_->has_selection()) { select(aspec_.empty() ? uspec_ : aspec_); }
        Loop_impl::this_ptr()->alarm(fun(this, &Fontsel_impl::reset_famlabel), 2248);
        families_->take_focus();
    }

    else {
        ustring s;

        if (monocheck_->checked()) {
            if (Painter pr = painter()) {
                for (const ustring & face: Font::list_faces(fam)) {
                    try {
                        if (Font font = pr.select_font(Font::build(fam, face, 10))) {
                            if (font.monospace()) {
                                s = fam;
                            }
                        }
                    }

                    catch (bad_font & x) {}
                }
            }
        }

        else {
            s = fam;
        }

        if (!s.empty() && !families_->contains(s, true)) {
            families_->append(s);
            famlabel_->assign(str_format(ifamilies_, " (", families_->count(), ')'));

            if (!families_->has_selection()) {
                s = aspec_.empty() ? uspec_ : aspec_;
                if (families_->contains(Font::family(s), true)) { select(s); }
            }
        }
    }
}

void Fontsel_impl::update_families() {
    families_->clear();
    spinner_->show();
    spinner_->start();
    families_->set_cursor("watch:wait");
    reset_famlabel();
    Font::list_families_async(fun(this, &Fontsel_impl::on_next_family));
}

void Fontsel_impl::select(std::string_view spec) {
    uspec_ = spec;
    uface_ = Font::face(spec);
    ustring family = Font::family(spec);
    face_ = Font::face(spec);
    double pt = Font::points(spec);
    if (pt < 1) { pt = Font::points(Font::normal()); }
    counter_->assign(pt);
    if (!families_->contains(family)) { family = Font::family(monocheck_->checked() ? Font::mono() : Font::normal()); }
    families_->select(family);
    on_family_selected(family);
}

void Fontsel_impl::set_sample(const ustring & sample) {
    if (!sample.empty()) {
        sample_ = sample;
        entry_->assign(sample_);
    }
}

void Fontsel_impl::update_entry() {
    if (Painter pr = entry_->painter()) {
        entry_->conf().font(Conf::EDIT_FONT) = spec_;
        Font font = pr.select_font(spec_);
        ustring psname = font.psname();
        psname_->assign(psname.empty() ? "Not available" : psname);
    }
}

// An exception might be thrown during this call!
void Fontsel_impl::update_font() {
    try {
        if (!family_.empty() && !face_.empty() && counter_->value() >= 1) {
            ustring spc = Font::build(family_, face_, counter_->value());
            if (spc.empty()) { spc = Font::normal(); }

            if (spec_ != spc) {
                spec_ = spc;
                signal_selection_changed_(spec_);
                fontspec_->assign(spec_);
                update_entry();
            }
        }
    }

    catch (exception & x) {
        std::cerr << "** " << __func__ << ": " << x.what() << std::endl;
    }
}

void Fontsel_impl::on_face_selected(const ustring & str) {
    face_ = str;
    update_font();
    enable_apply(!str_similar(spec_, aspec_));
    counter_->par_enable(!str.empty());
}

void Fontsel_impl::on_face_activated(const ustring & str) {
    face_ = str;
    update_font();
    enable_apply(false);
    if (!spec_.empty() && !str_similar(spec_, aspec_)) { aspec_ = spec_; enable_apply(true); }
    signal_font_activated_(spec_);
    apply_.exec();
    enable_apply(false);
}

void Fontsel_impl::update_faces() {
    faces_->clear();
    auto v = Font::list_faces(family_);
    std::sort(v.begin(), v.end());
    auto mono = lgettext("MONO");
    auto var = lgettext("VAR");

    for (const ustring & s: v) {
        if (Painter pr = painter()) {
            try {
                Font font = pr.select_font(Font::build(family_, s, counter_->value()));

                if (!monocheck_->checked() || font.monospace()) {
                    int row = faces_->append(s);
                    faces_->insert(row, font.monospace() ? mono : var, 0, Align::CENTER);
                }
            }
            
            catch (bad_font & x) {}
        }
    }
}

void Fontsel_impl::on_family_selected(const ustring & str) {
    family_ = str;
    ustring face = faces_->str();
    update_faces();
    if (faces_->contains(face, true)) { faces_->select_similar(face); }
    else if (faces_->contains(uface_, true)) { faces_->select_similar(uface_); }
    else { faces_->select_front(); }
    on_face_selected(faces_->str());
}

void Fontsel_impl::on_family_activated(const ustring & str) {
    family_ = str;
    update_font();
    enable_apply(false);
    if (!str_similar(spec_, aspec_)) { aspec_ = spec_; enable_apply(true); }
    signal_font_activated_(spec_);
    apply_.exec();
    enable_apply(false);
}

void Fontsel_impl::on_counter_value_changed(double value) {
    hsample_ = 0;
    entry_->hint_size(0);
    update_font();
    enable_apply(!str_similar(spec_, aspec_));
}

void Fontsel_impl::on_sample_requisition_changed(Hints) {
    std::size_t h = entry_->required_size().height();
    if (0 != hsample_ && h > hsample_) { entry_->hint_size(0, h); }
    hsample_ = h;
}

void Fontsel_impl::update_tooltips() {
    auto table = std::make_shared<Table_impl>();
    table->set_column_spacing(8);
    table->hint_margin(4, 4, 2, 2);
    table->align_column(0, Align::START);
    table->align_column(1, Align::END);
    ustring spc = Font::resize(conf().font().spec(), 7);

    auto tp = std::make_shared<Label_impl>("++size:", Align::END);
    table->put(tp, 0, 0, 1, 1, false, true);
    tp->conf().font() = spc;

    tp = std::make_shared<Label_impl>("--size:", Align::END);
    table->put(tp, 0, 1, 1, 1, false, true);
    tp->conf().font() = spc;

    ustring iaccels, oaccels;

    for (auto & accel: zin_.accels()) {
        if (!iaccels.empty()) { iaccels += ' '; }
        iaccels += accel->label();
    }

    for (auto & accel: zout_.accels()) {
        if (!oaccels.empty()) { oaccels += ' '; }
        oaccels += accel->label();
    }

    spc = Font::set_face(spc, "Bold");
    tp = std::make_shared<Label_impl>(iaccels, Align::END);
    table->put(tp, 1, 0, 1, 1, true, true);
    tp->conf().font() = spc;

    tp = std::make_shared<Label_impl>(oaccels, Align::END);
    table->put(tp, 1, 1, 1, 1, true, true);
    tp->conf().font() = spc;

    counter_->set_tooltip(table);
}

void Fontsel_impl::on_entry_changed() {
    sample_ = entry_->str();
}

void Fontsel_impl::focus_next() {
    if (focused()) {
        if (families_->focused()) { faces_->take_focus(); }
        else if (faces_->focused()) { counter_->take_focus(); }
        else { families_->take_focus(); }
    }
}

void Fontsel_impl::focus_previous() {
    if (focused()) {
        if (counter_->focused()) { faces_->take_focus(); }
        else if (families_->focused()) { counter_->take_focus(); }
        else { families_->take_focus(); }
    }
}

void Fontsel_impl::on_entry_activate(const ustring & s) {
    families_->take_focus();
}

void Fontsel_impl::on_apply() {
    signal_font_activated_(spec_);
    aspec_ = spec_;
    enable_apply(false);
}

void Fontsel_impl::on_monospace(bool yes) {
    update_families();
    update_faces();
    if (!families_->has_selection()) { families_->select_front(); }
}

void Fontsel_impl::set_show_monospace_only(bool yes) {
    if (yes) { monocheck_->check(); }
    else { monocheck_->uncheck(); }
    on_monospace(yes);
}

bool Fontsel_impl::monospace_only_visible() const {
    return monocheck_->checked();
}

void Fontsel_impl::enable_apply(bool yes) {
    if (yes) {
        apply_timer_.stop();
        apply_.enable();
    }

    else {
        if (apply_timer_.empty()) { apply_timer_.connect(fun(apply_, &Action::disable)); }
        apply_timer_.start(138);
    }
}

void Fontsel_impl::on_list_focus(Widget_impl * wp, bool in) {
    if (in) {
        wp->conf().set_from(Conf::BACKGROUND, Conf::HIGHLIGHT_BACKGROUND);
        wp->conf().set_from(Conf::WHITESPACE_BACKGROUND, Conf::HIGHLIGHT_BACKGROUND);
    }

    else {
        wp->conf().unset(Conf::BACKGROUND);
        wp->conf().unset(Conf::WHITESPACE_BACKGROUND);
    }
}

void Fontsel_impl::on_size_changed() {
    unsigned w = size().width()/6;
    fontspec_->hint_max_size(w, 0);
    psname_->hint_max_size(w, 0);
    normal_->hint_max_size(w, 0);
    mono_->hint_max_size(w, 0);
    families_->hint_max_size(0, 2*size().height()/3);
}

} // namespace tau

//END

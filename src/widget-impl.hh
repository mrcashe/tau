// ----------------------------------------------------------------------------
// SPDX-License-Identifier: BSD-2-Clause
// Copyright © 2014-2024 Konstantin Shmelkov <mrcashe@gmail.com>.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
// THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
// PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
// CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
// EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
// PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
// OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
// WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
// OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
// EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// ----------------------------------------------------------------------------

#ifndef __TAU_WIDGET_IMPL_HH__
#define __TAU_WIDGET_IMPL_HH__

#include <tau/enums.hh>
#include <tau/input.hh>
#include <tau/object.hh>
#include <tau/geometry.hh>
#include <tau/conf.hh>
#include <tau/ustring.hh>
#include <defs-impl.hh>
#include <forward_list>

namespace tau {

// The base class of whole graphics object hierarchy.
// Almost every descendant derives this class to extend it's capabilities.
class Widget_impl: public Object {
    friend class Widget;

public:

    Widget_impl();

    virtual ~Widget_impl();

    // Overridden by Window_impl.
    virtual bool cursor_visible() const noexcept;

    // Overridden by Window_impl.
    virtual bool has_modal() const noexcept;

    // Overridden by Container_impl.
    // Overridden by Window_impl.
    virtual bool grab_modal_up(Widget_impl * caller);

    // Overridden by Container_impl.
    // Overridden by Window_impl.
    virtual bool end_modal_up(Widget_impl * caller);

    // Overridden by Container_impl.
    // Overridden by Window_impl.
    virtual int  grab_focus_up(Widget_impl * caller);

    // Overridden by Container_impl.
    // Overridden by Window_impl.
    virtual void drop_focus_up(Widget_impl * caller);

    // Overridden by Container_impl.
    virtual void resume_focus();

    // Overridden by Container_impl.
    virtual void suspend_focus();

    // Overridden by Container_impl.
    // Overridden by Window_impl.
    virtual bool grab_mouse_up(Widget_impl * wi);

    // Overridden by Container_impl.
    // Overridden by Window_impl.
    virtual bool ungrab_mouse_up(Widget_impl * caller);

    // Overridden by Container_impl.
    // Overridden by Window_impl.
    virtual bool grabs_mouse() const noexcept;

    // Overridden by Container_impl.
    virtual bool handle_accel(char32_t kc, int km);

    // Overridden by Container_impl.
    virtual bool handle_key_down(char32_t kc, int km);

    // Overridden by Container_impl.
    virtual bool handle_key_up(char32_t kc, int km);

    // Overridden by Container_impl.
    virtual bool handle_input(const ustring & s, int src);

    // Overridden by Container_impl.
    virtual bool handle_mouse_down(int mbt, int mm, const Point & pt);

    // Overridden by Container_impl.
    virtual bool handle_mouse_up(int mbt, int mm, const Point & pt);

    // Overridden by Container_impl.
    virtual bool handle_mouse_double_click(int mbt, int mm, const Point & pt);

    // Overridden by Container_impl.
    virtual void handle_mouse_motion(int mm, const Point & pt);

    // Overridden by Container_impl.
    virtual void handle_mouse_enter(const Point & pt);

    // Overridden by Container_impl.
    virtual void handle_mouse_leave();

    // Overridden by Container_impl.
    virtual bool handle_mouse_wheel(int delta, int mm, const Point & pt);

    // Overridden by Flat_container.
    // Overridden by Layered_container.
    virtual void handle_paint(Painter pr, const Rect & inval);

    // Overridden by Flat_container.
    // Overridden by Layered_container.
    virtual void handle_backpaint(Painter pr, const Rect & inval);

    // Overridden by Container_impl.
    // Overridden by Window_impl.
    virtual bool grabs_modal() const noexcept;

    // Overridden by Window_impl.
    virtual bool has_window() const noexcept { return !in_shutdown() && nullptr != window(); }

    // Overridden by Window_impl.
    virtual bool hover() const noexcept;

    // Overridden by Window_impl.
    virtual void invalidate(const Rect & r=Rect());

    // Overridden by Window_impl.
    virtual Painter painter();

    // Overridden by Dialog_impl.
    virtual void quit_dialog();

    // Overridden by Window_impl.
    // Overridden by Container_impl.
    virtual void set_cursor_up(Cursor_ptr cursor, Widget_impl * wip);

    // Overridden by Window_impl.
    // Overridden by Container_impl.
    virtual void unset_cursor_up(Widget_impl * wip);

    // Overridden by Container_impl.
    // Overridden by Window_impl.
    virtual void show_cursor_up();

    // Overridden by Window_impl.
    virtual void hide_cursor_up();

    // Overridden by Container_impl.
    virtual void clear_focus();

    // Overridden by Container_impl.
    virtual void clear_modal();

    // Overridden by Window_impl.
    virtual Point to_screen(const Point & pt=Point()) const noexcept;

    // Overridden by Window_impl.
    virtual Point where_mouse() const noexcept;

    // Overriden by Scroller_impl.
    virtual void track_pan(Widget_impl * wp);

    // Overridden by Window_impl.
    // Overridden by Container_impl.
    virtual Window_impl * window() noexcept;

    // Overridden by Window_impl.
    // Overridden by Container_impl.
    virtual const Window_impl * window() const noexcept;

    // Overridden by Toplevel_impl.
    // Overridden by Popup_impl.
    virtual Window_impl * toplevel() noexcept;

    // Overridden by Toplevel_impl.
    // Overridden by Popup_impl.
    virtual const Window_impl * toplevel() const noexcept;

    // Overridden by Popup_impl.
    // Overridden by Container_impl.
    virtual Point to_parent(const Container_impl * ci, const Point & pt=Point()) const noexcept;

    // Overridden by Window_impl.
    virtual Display_impl * display() noexcept;

    // Overridden by Window_impl.
    virtual const Display_impl * display() const noexcept;

    // Overridden by Container_impl.
    virtual bool running() const noexcept { return signal_run_ && !signal_run_->empty(); }

    // Overridden by Container_impl.
    virtual signal<bool(int, int, Point)> & signal_mouse_down();

    // Overridden by Container_impl.
    virtual signal<bool(int, int, Point)> & signal_mouse_double_click();

    // Overridden by Container_impl.
    virtual signal<bool(int, int, Point)> & signal_mouse_up();

    // Overridden by Container_impl.
    virtual signal<void(int, Point)> & signal_mouse_motion();

    // Overridden by Container_impl.
    virtual signal<void(Point)> & signal_mouse_enter();

    // Overridden by Container_impl.
    virtual signal<void()> & signal_mouse_leave();

    // Overridden by Container_impl.
    virtual signal<bool(int, int, Point)> & signal_mouse_wheel();

    // Overridden by Container_impl.
    virtual signal<bool(char32_t, int)> & signal_key_down();

    // Overridden by Container_impl.
    virtual signal<bool(char32_t, int)> & signal_key_up();

    // Overridden by Container_impl.
    virtual signal<bool(const ustring &, int)> & signal_input();

    void        enable();
    void        disable();
    void        par_enable(bool yes);
    bool        disabled() const noexcept { return disabled_ || frozen_; }
    bool        frozen() const noexcept { return frozen_; }
    bool        enabled() const noexcept { return sensitive_ && !disabled() && !in_shutdown(); }
    bool        selected() const noexcept { return selected_; }

    void        hide();
    void        show();
    void        par_show(bool yes);
    bool        hidden() const noexcept { return hidden_ || disappeared_; }
    bool        visible() const noexcept;

    bool        grab_modal();
    bool        end_modal();
    void        allow_focus();
    void        disallow_focus();
    bool        grab_focus();
    void        drop_focus();
    bool        focusable() const noexcept;
    bool        focused() const noexcept { return focused_; }
    bool        take_focus();
    bool        grab_mouse();
    bool        ungrab_mouse();

    bool        has_parent() const noexcept { return nullptr != container_; }
    bool        has_parent(const Widget_impl * cwp) const noexcept;
    bool        has_display() const noexcept { return has_display_; }
    bool        scrollable() const noexcept;
    Rect        to_screen(const Rect & r) const noexcept { return Rect(to_screen(r.top_left()), r.size()); }
    Rect        to_parent(const Container_impl * cp, const Rect & r) const noexcept { return Rect(to_parent(cp, r.top_left()), r.size()); }

    bool        hint_size(const Size & sz);
    bool        hint_size(unsigned width, unsigned height) { return hint_size(Size(width, height)); }
    Size        size_hint() const noexcept { return size_hint_; }

    bool        hint_min_size(const Size & sz);
    bool        hint_min_size(unsigned width, unsigned height) { return hint_min_size(Size(width, height)); }
    Size        min_size_hint() const noexcept { return min_size_hint_; }

    bool        hint_max_size(const Size & sz);
    bool        hint_max_size(unsigned width, unsigned height) { return hint_max_size(Size(width, height)); }
    Size        max_size_hint() const noexcept { return max_size_hint_; }

    bool        hint_margin_left(unsigned left);
    bool        hint_margin_right(unsigned right);
    bool        hint_margin_top(unsigned top);
    bool        hint_margin_bottom(unsigned bottom);
    bool        hint_margin(unsigned left, unsigned right, unsigned top, unsigned bottom) { return hint_margin({ left, right, top, bottom }); }
    bool        hint_margin(unsigned all) { return hint_margin({ all, all, all, all }); }
    bool        hint_margin(const Margin & margin);
    Margin      margin_hint() const noexcept { return margin_hint_; }

    bool        hint_aspect_ratio(double ratio);
    double      required_aspect_ratio() const noexcept;
    bool        has_aspect_ratio_hint() const noexcept { return required_aspect_ratio() >= 0.01; }

    connection  connect_run(slot<void()> slot, bool prepend=false);

protected:

    bool        require_aspect_ratio(double ratio);

public:

    Point       origin() const noexcept { return origin_; }
    bool        update_origin(const Point & pt);
    bool        update_origin(int x, int y) { return update_origin(Point(x, y)); }

    Size        size() const noexcept { return size_; }
    bool        update_size(const Size & sz);
    bool        update_size(unsigned width, unsigned height) { return update_size(Size(width, height)); }
    Size        required_size() const noexcept { return required_size_; }
    Size        logical_size() const noexcept;

    void        handle_sensitivity(bool yes);
    void        handle_visibility(bool visible);
    void        handle_display(bool in);
    void        handle_focus_in();
    void        handle_focus_out();

    void        offset(const Point & pt);
    void        offset(int x, int y);
    Point       offset() const noexcept;

    Point       worigin() const noexcept { return worigin_; }
    Point       poffset() const noexcept { return poffset_; }
    Rect        viewable_area() const noexcept { return viewable_area_; }

    bool        has_tooltip() const noexcept;
    void        hide_tooltip();
    void        set_tooltip(const ustring & s, Align align=Align::FILL);
    void        set_tooltip(Widget_ptr wp);
    Widget_ptr  show_tooltip(const ustring & s, Align align=Align::FILL);
    Widget_ptr  show_tooltip(const ustring & s, const Point & pt, Gravity gravity, unsigned time_ms=0);
    Widget_ptr  show_tooltip(const ustring & s, Align align, const Point & pt, Gravity gravity, unsigned time_ms=0);
    void        show_tooltip(Widget_ptr wp);
    void        show_tooltip(Widget_ptr wp, const Point & pt, Gravity gravity, unsigned time_ms=0);
    void        unset_tooltip();
    void        allow_tooltip();
    void        disallow_tooltip();
    bool        tooltip_allowed() const noexcept { return tooltip_allowed_; }

    void        set_cursor(Cursor_ptr cursor);
    void        set_cursor(const ustring & name, unsigned size=0);
    void        unset_cursor();
    void        show_cursor();
    void        hide_cursor();
    Cursor_ptr  cursor() noexcept { return cursor_; }
    bool        cursor_hidden() const noexcept { return cursor_hidden_; }

    Conf &      conf() noexcept { return conf_; }
    const Conf & conf() const noexcept { return conf_; }

    void        update_pdata();

    Accel *     allocate_accel(char32_t kc, int km=KM_NONE, bool prepend=false);
    Accel *     allocate_accel(const ustring & spec, bool prepend=false);
    connection  connect_accel(Accel & accel, bool prepend=false);
    void        connect_action(Action_base & action, bool prepend=false);

    void        rise();
    void        lower();
    int         level() const noexcept;

    Container_impl * container() noexcept { return container_; }
    const Container_impl * container() const noexcept { return container_; }

    void            facade_in(Widget * w);
    void            facade_out(Widget * w);
    Widget *        facade() { return facade_; }
    const Widget *  facade() const { return facade_; }

    signal<bool(Painter, Rect)> & signal_paint();
    signal<bool(Painter, Rect)> & signal_backpaint();
    signal<void()> & signal_size_changed() { return signal_size_changed_; }
    signal<void(Hints)> & signal_hints_changed() { return signal_hints_changed_; }
    signal<void()> & signal_enable() { return signal_enable_; }
    signal<void()> & signal_disable() { return signal_disable_; }
    signal<void()> & signal_focus_in() { return signal_focus_in_; }
    signal<void()> & signal_focus_out() { return signal_focus_out_; }
    signal<void()> & signal_select() { return signal_select_; }
    signal<void()> & signal_unselect() { return signal_unselect_; }
    signal<void()> & signal_show() { return signal_show_; }
    signal<void()> & signal_hide() { return signal_hide_; }
    signal<void()> & signal_display_in() { return signal_display_in_; }
    signal<void()> & signal_display_out() { return signal_display_out_; }
    signal<bool()> & signal_take_focus() { return signal_take_focus_; }
    signal<void()> & signal_origin_changed() { return signal_origin_changed_; }
    signal<void()> & signal_offset_changed();
    signal<bool(char32_t, int)> & signal_accel();

    /// INTERNAL. Emitted when focusable property resets.
    signal<void()> & signal_unchain();

protected:

    Container_impl *    container_              = nullptr;
    Cursor_ptr          cursor_;
    bool                cursor_hidden_ : 1      = false;
    bool                hidden_: 1              = false;

    signal<void()>      signal_size_changed_;
    signal<void()>      signal_origin_changed_;
    signal<void(Hints)> signal_hints_changed_;
    signal<void()>      signal_enable_;
    signal<void()>      signal_disable_;
    signal<void()>      signal_focus_in_;
    signal<void()>      signal_focus_out_;
    signal<void()>      signal_select_;
    signal<void()>      signal_unselect_;
    signal<void()>      signal_show_;
    signal<void()>      signal_hide_;
    signal<void()>      signal_display_in_;
    signal<void()>      signal_display_out_;
    signal<bool()>      signal_take_focus_;
    signal<void()>      signal_pdata_changed_;

    signal<bool(int, int, Point)> * signal_mouse_down_          = nullptr;
    signal<bool(int, int, Point)> * signal_mouse_double_click_  = nullptr;
    signal<bool(int, int, Point)> * signal_mouse_up_            = nullptr;
    signal<void(int, Point)> * signal_mouse_motion_             = nullptr;
    signal<void(Point)> * signal_mouse_enter_                   = nullptr;
    signal<void()> * signal_mouse_leave_                        = nullptr;
    signal<bool(int, int, Point)> * signal_mouse_wheel_         = nullptr;
    signal<bool(char32_t, int)> * signal_key_down_              = nullptr;
    signal<bool(char32_t, int)> * signal_key_up_                = nullptr;
    signal<bool(const ustring &, int)> * signal_input_          = nullptr;

protected:

    void freeze();
    void thaw();
    void disappear();
    void appear();
    void expose_tooltip();
    bool require_size(const Size & size);
    bool require_size(unsigned width, unsigned height) { return require_size(Size(width, height)); }

    // Overriden by Container_impl.
    virtual void on_tooltip_timer() { expose_tooltip(); }

    // Overriden by Frame_impl.
    // Overriden by Table_impl.
    // Overriden by Window_impl.
    virtual bool on_backpaint(Painter pr, const Rect & inval);

private:

    ustring     tooltip_text_;
    Align       tooltip_align_;
    Widget_ptr  tooltip_widget_;
    unsigned    tooltip_delay_          = 611;
    connection  tooltip_timer_cx_       { true  };
    connection  tooltip1_cx_            { true  };
    connection  tooltip2_cx_            { true  };
    bool        tooltip_allowed_: 1     { true  };
    bool        tooltip_exposed_ : 1    { false };

    ustring     cursor_name_;
    unsigned    cursor_size_            = 0;

    Conf        conf_;
    Point       origin_;
    Point       worigin_                { INT_MIN, INT_MIN };  // Origin within Window.
    Point       poffset_                { INT_MIN, INT_MIN };  // Is for Painting Offset.
    Rect        viewable_area_;
    Size        size_;
    Size        size_hint_;
    Size        min_size_hint_;
    Size        max_size_hint_;
    Size        required_size_;
    double      required_aspect_ratio_  = 0.0;
    double      aspect_ratio_hint_      = 0.0;

    bool        has_display_ : 1        = false;
    bool        focused_ : 1            = false;
    bool        visible_ : 1            = false;
    bool        sensitive_ : 1          = true;
    bool        disappeared_ : 1        = false;
    bool        disabled_ : 1           = false;
    bool        frozen_ : 1             = false;
    bool        focusable_ : 1          = false;
    bool        selected_ : 1           = false;
    bool        dropped_ : 1            = false;        // TODO REWORK? Focus has been dropped by drop_focus().

    Margin      margin_hint_;
    Widget *    facade_                 = nullptr;      // Facade pointer.

    /// @warning Accels MUST be stored within lists, not vector or map because user has persistent pointer to it!
    using Accels = std::forward_list<Accel>;
    Accels      accels_;

    connection  run1_cx_                { true };       // Connects Loop & signal_run_.
    connection  run2_cx_                { true };       // Connects signal_run_ & on_run().
    connection  pan_cx_                 { true };
    connection  cursor_theme_cx_        { true };

    signal<bool(char32_t, int)> * signal_accel_                     = nullptr;
    signal<void()> * signal_offset_changed_                         = nullptr;
    signal<bool(Painter, Rect)> * signal_paint_                     = nullptr;
    signal<bool(Painter, Rect)> * signal_backpaint_                 = nullptr;
    signal<void()> * signal_run_                                    = nullptr;
    signal<void()> * signal_unchain_                                = nullptr;

private:

    void update_cursor();
    void enter_cursor();
    void leave_cursor();
    void refresh_track_pan();

    void on_enable();
    void on_disable();
    void on_size_changed();
    void on_show();
    void on_hide();
    void on_action_accel_added(const Accel & accel);
    void on_pan_changed();
    void trigger_tooltip_timer();
    void on_tooltip_close();
    bool on_tooltip_mouse(bool result, int, int, const Point &);
    void on_parent(Object * oparent);
    void on_display_in();
    void on_run();
    void on_select();
    void on_unselect();
};

} // namespace tau

#endif // __TAU_WIDGET_IMPL_HH__

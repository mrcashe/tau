// ----------------------------------------------------------------------------
// SPDX-License-Identifier: BSD-2-Clause
// Copyright © 2014-2024 Konstantin Shmelkov <mrcashe@gmail.com>.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
// THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
// PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
// CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
// EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
// PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
// OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
// WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
// OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
// EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// ----------------------------------------------------------------------------

#include <tau/observer.hh>
#include <tau/exception.hh>
#include <tau/string.hh>
#include <observer-impl.hh>

namespace tau {

#define OBSERVER_IMPL (std::static_pointer_cast<Observer_impl>(impl))

Observer::Observer(const ustring & uri):
    Widget(std::make_shared<Observer_impl>(uri))
{
}

Observer::Observer(const Observer & other):
    Widget(other.impl)
{
}

Observer & Observer::operator=(const Observer & other) {
    Widget::operator=(other);
    return *this;
}

Observer::Observer(Observer && other):
    Widget(other.impl)
{
}

Observer & Observer::operator=(Observer && other) {
    Widget::operator=(other);
    return *this;
}

Observer::Observer(Widget_ptr wp):
    Widget(std::dynamic_pointer_cast<Observer_impl>(wp))
{
}

Observer & Observer::operator=(Widget_ptr wp) {
    if (!std::dynamic_pointer_cast<Observer_impl>(wp)) { throw user_error(str_format(this, " Observer::operator=(Widget_ptr): got pure or incompatible implementation pointer")); }
    impl = wp;
    return *this;
}

void Observer::select_uri(const ustring & uri) {
    OBSERVER_IMPL->select_uri(uri);
}

signal<void(const ustring &)> &  Observer::signal_activate_uri() {
    return OBSERVER_IMPL->signal_activate_uri();
}

} // namespace tau

//END

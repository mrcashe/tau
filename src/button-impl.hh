// ----------------------------------------------------------------------------
// SPDX-License-Identifier: BSD-2-Clause
// Copyright © 2014-2024 Konstantin Shmelkov <mrcashe@gmail.com>.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
// THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
// PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
// CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
// EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
// PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
// OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
// WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
// OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
// EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// ----------------------------------------------------------------------------

/// @file button-impl.hh Button_base_impl, Button_impl and Toggle_impl class declarations.
/// Source file is button-impl.cc. Public interface is tau/button.hh, interface implementation is button.cc.

#ifndef __TAU_BUTTON_IMPL_HH__
#define __TAU_BUTTON_IMPL_HH__

#include <tau/accel.hh>
#include <tau/timer.hh>
#include <frame-impl.hh>

namespace tau {

// ----------------------------------------------------------------------------
// Button_base_impl Button_base_impl Button_base_impl Button_base_impl
// ----------------------------------------------------------------------------

class Button_base_impl: public Frame_impl {
protected:

    Button_base_impl();
    explicit Button_base_impl(unsigned radius);
    explicit Button_base_impl(unsigned rtop_left, unsigned rtop_right, unsigned rbottom_right, unsigned rbottom_left);

    explicit Button_base_impl(const ustring & label);
    explicit Button_base_impl(const ustring & label, unsigned radius);
    explicit Button_base_impl(const ustring & label, unsigned rtop_left, unsigned rtop_right, unsigned rbottom_right, unsigned rbottom_left);

    explicit Button_base_impl(Widget_ptr img);
    explicit Button_base_impl(Widget_ptr img, unsigned radius);
    explicit Button_base_impl(Widget_ptr img, unsigned rtop_left, unsigned rtop_right, unsigned rbottom_right, unsigned rbottom_left);

    explicit Button_base_impl(Widget_ptr img, const ustring & label);
    explicit Button_base_impl(Widget_ptr img, const ustring & label, unsigned radius);
    explicit Button_base_impl(Widget_ptr img, const ustring & label, unsigned rtop_left, unsigned rtop_right, unsigned rbottom_right, unsigned rbottom_left);

    explicit Button_base_impl(int icon_size, const ustring & icon_name);
    explicit Button_base_impl(int icon_size, const ustring & icon_name, unsigned radius);
    explicit Button_base_impl(int icon_size, const ustring & icon_name, unsigned rtop_left, unsigned rtop_right, unsigned rbottom_right, unsigned rbottom_left);

    explicit Button_base_impl(const ustring & label, int icon_size, const ustring & icon_name);
    explicit Button_base_impl(const ustring & label, int icon_size, const ustring & icon_name, unsigned radius);
    explicit Button_base_impl(const ustring & label, int icon_size, const ustring & icon_name, unsigned rtop_left, unsigned rtop_right, unsigned rbottom_right, unsigned rbottom_left);

    explicit Button_base_impl(Action_base & action, Action::Flags items);
    explicit Button_base_impl(Action_base & action, unsigned radius, Action::Flags items);
    explicit Button_base_impl(Action_base & action, unsigned rtop_left, unsigned rtop_right, unsigned rbottom_right, unsigned rbottom_left, Action::Flags items);

    explicit Button_base_impl(Action_base & action, int icon_size, Action::Flags items);
    explicit Button_base_impl(Action_base & action, int icon_size, unsigned radius, Action::Flags items);
    explicit Button_base_impl(Action_base & action, int icon_size, unsigned rtop_left, unsigned rtop_right, unsigned rbottom_right, unsigned rbottom_left, Action::Flags items);

    explicit Button_base_impl(Master_action & action, Action::Flags items);
    explicit Button_base_impl(Master_action & action, unsigned radius, Action::Flags items);
    explicit Button_base_impl(Master_action & action, unsigned rtop_left, unsigned rtop_right, unsigned rbottom_right, unsigned rbottom_left, Action::Flags items);

    explicit Button_base_impl(Master_action & action, int icon_size, Action::Flags items);
    explicit Button_base_impl(Master_action & action, int icon_size, unsigned radius, Action::Flags items);
    explicit Button_base_impl(Master_action & action, int icon_size, unsigned rtop_left, unsigned rtop_right, unsigned rbottom_right, unsigned rbottom_left, Action::Flags items);

public:

    void set_image(Widget_ptr wp);
    void set_label(const ustring & text);
    void set_icon(const ustring & icon_name, int icon_size);
    int  icon_size() const noexcept;
    void resize_icon(int icon_size);
    void show_relief();
    void hide_relief();
    bool relief_visible() const noexcept;

    // Overrides Frame_impl.
    void set_border_top_left_radius(unsigned radius) override;

    // Overrides Frame_impl.
    void set_border_top_right_radius(unsigned radius) override;

    // Overrides Frame_impl.
    void set_border_bottom_right_radius(unsigned radius) override;

    // Overrides Frame_impl.
    void set_border_bottom_left_radius(unsigned radius) override;

    // Overrides Frame_impl.
    void set_border_radius(unsigned radius) override;

    // Overrides Frame_impl.
    void set_border_radius(unsigned top_left, unsigned top_right, unsigned bottom_right, unsigned bottom_left) override;

    void unset_border_top_left_radius();
    void unset_border_top_right_radius();
    void unset_border_bottom_right_radius();
    void unset_border_bottom_left_radius();
    void unset_border_radius();

    // Overrides Frame_impl.
    unsigned border_top_left_radius() const noexcept override { return radius_top_left_ < 0 ? conf().integer(Conf::RADIUS) : radius_top_left_; }

    // Overrides Frame_impl.
    unsigned border_top_right_radius() const noexcept override { return radius_top_right_ < 0 ? conf().integer(Conf::RADIUS) : radius_top_right_; }

    // Overrides Frame_impl.
    unsigned border_bottom_right_radius() const noexcept override { return radius_bottom_right_ < 0 ? conf().integer(Conf::RADIUS) : radius_bottom_right_; }

    // Overrides Frame_impl.
    unsigned border_bottom_left_radius() const noexcept override { return radius_bottom_left_ < 0 ? conf().integer(Conf::RADIUS) : radius_bottom_left_; }

protected:

    Table_ptr   table_;
    Widget_ptr  image_;
    Widget_ptr  label_;
    Timer       timer_;
    ustring     text_;

    bool        pressed_            = false;
    bool        fix_press_          = false;
    bool        relief_visible_     = true;
    connection  timer_cx_           { true };
    connection  enable_cx_          { true };
    connection  disable_cx_         { true };
    connection  show_cx_            { true };
    connection  hide_cx_            { true };

protected:

    void init(Action_base & action, int icon_size, Action::Flags items);
    void init(Master_action & action, int icon_size, Action::Flags items);

    void on_mouse_enter(const Point & pt);
    bool on_mouse_down(int mbt, int mm, const Point & position);
    bool on_mouse_up(int mbt, int mm, const Point & position);

    void press();
    void redraw();

    virtual void on_release() {}
    virtual void on_press() {}

private:

    ustring     tooltip_;
    int         radius_top_left_        = -1;
    int         radius_top_right_       = -1;
    int         radius_bottom_left_     = -1;
    int         radius_bottom_right_    = -1;

private:

    void init(int rtop_left, int rtop_right, int rbottom_right, int rbottom_left);
    void update_radius();
    void on_conf_radius();
    void on_background_changed();
    void on_mouse_leave();
    void on_button_label();
    void on_action_label_changed(const ustring & label);
    void on_action_tooltip_changed(const ustring & tooltip, const Action_base & action);
    void on_master_tooltip_changed(const ustring & tooltip, const Master_action & action);
    void on_action_accel_changed(const Accel & accel, const Action_base & action);
    void on_master_accel_changed(const Accel & accel, const Master_action & action);
    void set_action_tooltip(const Action_base & action);
    void set_action_tooltip(const Master_action & action);
    void set_action_tooltip(const std::vector<const Accel *> & accels);
};

// ----------------------------------------------------------------------------
// Button_impl Button_impl Button_impl Button_impl Button_impl Button_impl
// ----------------------------------------------------------------------------

class Button_impl: public Button_base_impl {
public:

    Button_impl();
    ~Button_impl() { destroy(); }
    explicit Button_impl(unsigned radius);
    explicit Button_impl(unsigned rtop_left, unsigned rtop_right, unsigned rbottom_right, unsigned rbottom_left);

    explicit Button_impl(const ustring & label);
    explicit Button_impl(const ustring & label, unsigned radius);
    explicit Button_impl(const ustring & label, unsigned rtop_left, unsigned rtop_right, unsigned rbottom_right, unsigned rbottom_left);

    explicit Button_impl(Widget_ptr img);
    explicit Button_impl(Widget_ptr img, unsigned radius);
    explicit Button_impl(Widget_ptr img, unsigned rtop_left, unsigned rtop_right, unsigned rbottom_right, unsigned rbottom_left);

    explicit Button_impl(Widget_ptr img, const ustring & label);
    explicit Button_impl(Widget_ptr img, const ustring & label, unsigned radius);
    explicit Button_impl(Widget_ptr img, const ustring & label, unsigned rtop_left, unsigned rtop_right, unsigned rbottom_right, unsigned rbottom_left);

    explicit Button_impl(int icon_size, const ustring & icon_name);
    explicit Button_impl(int icon_size, const ustring & icon_name, unsigned radius);
    explicit Button_impl(int icon_size, const ustring & icon_name, unsigned rtop_left, unsigned rtop_right, unsigned rbottom_right, unsigned rbottom_left);

    explicit Button_impl(const ustring & label, int icon_size, const ustring & icon_name);
    explicit Button_impl(const ustring & label, int icon_size, const ustring & icon_name, unsigned radius);
    explicit Button_impl(const ustring & label, int icon_size, const ustring & icon_name, unsigned rtop_left, unsigned rtop_right, unsigned rbottom_right, unsigned rbottom_left);

    explicit Button_impl(Action & action, Action::Flags items=Action::ALL);
    explicit Button_impl(Action & action, unsigned radius, Action::Flags items=Action::ALL);
    explicit Button_impl(Action & action, unsigned rtop_left, unsigned rtop_right, unsigned rbottom_right, unsigned rbottom_left, Action::Flags items=Action::ALL);

    explicit Button_impl(Action & action, int icon_size, Action::Flags items=Action::ALL);
    explicit Button_impl(Action & action, int icon_size, unsigned radius, Action::Flags items=Action::ALL);
    explicit Button_impl(Action & action, int icon_size, unsigned rtop_left, unsigned rtop_right, unsigned rbottom_right, unsigned rbottom_left, Action::Flags items=Action::ALL);

    explicit Button_impl(Master_action & action, Action::Flags items=Action::ALL);
    explicit Button_impl(Master_action & action, unsigned radius, Action::Flags items=Action::ALL);
    explicit Button_impl(Master_action & action, unsigned rtop_left, unsigned rtop_right, unsigned rbottom_right, unsigned rbottom_left, Action::Flags items=Action::ALL);

    explicit Button_impl(Master_action & action, int icon_size, Action::Flags items=Action::ALL);
    explicit Button_impl(Master_action & action, int icon_size, unsigned radius, Action::Flags items=Action::ALL);
    explicit Button_impl(Master_action & action, int icon_size, unsigned rtop_left, unsigned rtop_right, unsigned rbottom_right, unsigned rbottom_left, Action::Flags items=Action::ALL);

    void click();
    void enable_repeat() noexcept { repeat_ = true; }
    void disable_repeat() noexcept { repeat_ = false; }
    void set_repeat_delay(unsigned first, unsigned next);

    bool repeat_enabled() const noexcept { return repeat_; }
    unsigned repeat_delay() const noexcept { return repeat_delay_; }
    unsigned repeat_interval() const noexcept { return repeat_interval_; }
    void set_action(Action & action, Action::Flags items=Action::ALL);
    void set_action(Action & action, int icon_size, Action::Flags items=Action::ALL);
    void select(Action & action);

    connection connect(const slot<void()> & slot, bool prepend=false) {
        return signal_click_.connect(slot, prepend);
    }

    connection connect(slot<void()> && slot, bool prepend=false) {
        return signal_click_.connect(slot, prepend);
    }

protected:

    // Overrides Button_base_impl.
    void on_press() override;

    // Overrides Button_base_impl.
    void on_release() override;

private:

    bool            repeat_             { false };
    unsigned        repeat_delay_       { 392   };
    unsigned        repeat_interval_    { 124   };
    Accel           space_accel_        { U' ', KM_NONE, fun(this, &Button_impl::on_keyboard_activate)      };
    Accel           enter_accel_        { KC_ENTER, KM_NONE, fun(this, &Button_impl::on_keyboard_activate)  };
    connection      exec_cx_            { true };
    signal<void()>  signal_click_;

private:

    void init();
    bool on_keyboard_activate();
    void on_release_timeout();
    void on_repeat_timeout();
    void on_mouse_leave();
    void on_disable();
    void on_select(Action & action);
    void on_unselect(Action & action);
};

// ----------------------------------------------------------------------------
// Toggle_impl Toggle_impl Toggle_impl Toggle_impl Toggle_impl Toggle_impl
// ----------------------------------------------------------------------------

class Toggle_impl: public Button_base_impl {
public:

    Toggle_impl();
    ~Toggle_impl() { destroy(); }
    explicit Toggle_impl(unsigned radius);
    explicit Toggle_impl(unsigned rtop_left, unsigned rtop_right, unsigned rbottom_right, unsigned rbottom_left);

    explicit Toggle_impl(const ustring & label);
    explicit Toggle_impl(const ustring & label, unsigned radius);
    explicit Toggle_impl(const ustring & label, unsigned rtop_left, unsigned rtop_right, unsigned rbottom_right, unsigned rbottom_left);

    explicit Toggle_impl(Widget_ptr img);
    explicit Toggle_impl(Widget_ptr img, unsigned radius);
    explicit Toggle_impl(Widget_ptr img, unsigned rtop_left, unsigned rtop_right, unsigned rbottom_right, unsigned rbottom_left);

    explicit Toggle_impl(Widget_ptr img, const ustring & label);
    explicit Toggle_impl(Widget_ptr img, const ustring & label, unsigned radius);
    explicit Toggle_impl(Widget_ptr img, const ustring & label, unsigned rtop_left, unsigned rtop_right, unsigned rbottom_right, unsigned rbottom_left);

    explicit Toggle_impl(int icon_size, const ustring & icon_name);
    explicit Toggle_impl(int icon_size, const ustring & icon_name, unsigned radius);
    explicit Toggle_impl(int icon_size, const ustring & icon_name, unsigned rtop_left, unsigned rtop_right, unsigned rbottom_right, unsigned rbottom_left);

    explicit Toggle_impl(const ustring & label, int icon_size, const ustring & icon_name);
    explicit Toggle_impl(const ustring & label, int icon_size, const ustring & icon_name, unsigned radius);
    explicit Toggle_impl(const ustring & label, int icon_size, const ustring & icon_name, unsigned rtop_left, unsigned rtop_right, unsigned rbottom_right, unsigned rbottom_left);

    explicit Toggle_impl(Toggle_action & action, Action::Flags items=Action::ALL);
    explicit Toggle_impl(Toggle_action & action, unsigned radius, Action::Flags items=Action::ALL);
    explicit Toggle_impl(Toggle_action & action, unsigned rtop_left, unsigned rtop_right, unsigned rbottom_right, unsigned rbottom_left, Action::Flags items=Action::ALL);

    explicit Toggle_impl(Toggle_action & action, int icon_size, Action::Flags items=Action::ALL);
    explicit Toggle_impl(Toggle_action & action, int icon_size, unsigned radius, Action::Flags items=Action::ALL);
    explicit Toggle_impl(Toggle_action & action, int icon_size, unsigned rtop_left, unsigned rtop_right, unsigned rbottom_right, unsigned rbottom_left, Action::Flags items=Action::ALL);

    explicit Toggle_impl(Master_action & action, Action::Flags items=Action::ALL);
    explicit Toggle_impl(Master_action & action, unsigned radius, Action::Flags items=Action::ALL);
    explicit Toggle_impl(Master_action & action, unsigned rtop_left, unsigned rtop_right, unsigned rbottom_right, unsigned rbottom_left, Action::Flags items=Action::ALL);

    explicit Toggle_impl(Master_action & action, int icon_size, Action::Flags items=Action::ALL);
    explicit Toggle_impl(Master_action & action, int icon_size, unsigned radius, Action::Flags items=Action::ALL);
    explicit Toggle_impl(Master_action & action, int icon_size, unsigned rtop_left, unsigned rtop_right, unsigned rbottom_right, unsigned rbottom_left, Action::Flags items=Action::ALL);

    void setup(bool state);
    void set(bool state);
    bool get() const { return state_; }
    void toggle();
    void set_action(Toggle_action & action, Action::Flags items=Action::ALL);
    void set_action(Toggle_action & action, int icon_size, Action::Flags items=Action::ALL);
    void select(Toggle_action & action);
    connection connect(const slot<void(bool)> & slot, bool prepend=false) { return signal_toggle_.connect(slot, prepend); }
    connection connect(slot<void(bool)> && slot, bool prepend=false) { return signal_toggle_.connect(slot, prepend); }

protected:

    // Overrides Button_base_impl.
    void on_release() override;

private:

    bool                state_          { false };
    connection          toggle_cx_      { true  };
    signal<void(bool)>  signal_toggle_;

private:

    void on_select(Toggle_action & action);
    void on_unselect(Toggle_action & action);
};

} // namespace tau

#endif // __TAU_BUTTON_IMPL_HH__

// ----------------------------------------------------------------------------
// SPDX-License-Identifier: BSD-2-Clause
// Copyright © 2014-2024 Konstantin Shmelkov <mrcashe@gmail.com>.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
// THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
// PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
// CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
// EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
// PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
// OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
// WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
// OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
// EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// ----------------------------------------------------------------------------

#include <tau/brush.hh>
#include <tau/font.hh>
#include <tau/painter.hh>
#include "loop-impl.hh"
#include "spinner-impl.hh"
#include <numbers>
#include <iostream>

namespace tau {

Spinner_impl::Spinner_impl():
    Widget_impl()
{
    init();
}

Spinner_impl::Spinner_impl(Style spinner_style):
    sstyle_(spinner_style)
{
    init();
}

void Spinner_impl::init() {
    require_size(8);
    require_aspect_ratio(1.0);
    signal_display_in().connect(fun(this, &Spinner_impl::on_display));
    signal_paint().connect(fun(this, &Spinner_impl::on_paint));
    conf().item(tau::Conf::FOREGROUND).signal_changed().connect(fun(this, &Spinner_impl::on_foreground_changed));
}

void Spinner_impl::start() {
    cx_ = Loop_impl::this_ptr()->alarm(fun(this, &Spinner_impl::on_timer), 232, true);
}

void Spinner_impl::stop() {
    cx_.drop();
}

void Spinner_impl::on_timer() {
    invalidate();
    if (8 == ++step_) { step_ = 0; }
}

void Spinner_impl::on_display() {
    if (Painter pr = painter()) {
        if (Font font = pr.select_font(conf().font().spec())) {
            require_size(std::max(8u, font.height()));
        }
    }

    on_foreground_changed();
}

bool Spinner_impl::on_paint(Painter pr, const Rect & inval) {
    if (Size z = size()) {
        pr.matrix().translate(z.width()/2, z.height()/2);
        pr.matrix().scale(z.min()/2);
        pr.matrix().rotate(-step_*std::numbers::pi/4.0);
        Color c(color_);

        switch (sstyle_) {
            case Spinner::LINE:
                pr.rectangle(-0.12, -0.75, 0.12, 0.75, 0.12);
                pr.set_brush(c);
                pr.fill();
                break;

            // Assume SPINNER_BALL.
            default:
                for (int i = 0; i < 8; ++i) {
                    pr.circle(0.0, 0.76, 0.17);
                    pr.set_brush(c);
                    pr.fill();
                    double value = c.value()+vstep_;
                    if (value >= 1.0) { value = ivalue_; }
                    c.set_value(value);
                    pr.matrix().rotate(std::numbers::pi/4.0);
                }
        }
    }

    return false;
}

void Spinner_impl::spinner_style(Style spinner_style) {
    if (sstyle_ != spinner_style) {
        sstyle_ = spinner_style;
        invalidate();
    }
}

void Spinner_impl::on_foreground_changed() {
    color_ = conf().color(tau::Conf::FOREGROUND);
    ivalue_ = color_.value();
    vstep_ = (1.0-ivalue_)/8;
    if (vstep_ < 0.07) { vstep_ = 0.1, ivalue_ = 0.2; }
    invalidate();
}

} // namespace tau

//END

// ----------------------------------------------------------------------------
// SPDX-License-Identifier: BSD-2-Clause
// Copyright © 2014-2024 Konstantin Shmelkov <mrcashe@gmail.com>.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
// THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
// PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
// CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
// EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
// PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
// OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
// WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
// OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
// EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// ----------------------------------------------------------------------------

#include <tau/accel.hh>
#include <tau/brush.hh>
#include <tau/input.hh>
#include <tau/toplevel.hh>
#include <tau/painter.hh>
#include <tau/widget.hh>
#include <display-impl.hh>
#include <frame-impl.hh>
#include <label-impl.hh>
#include <loop-impl.hh>
#include <painter-impl.hh>
#include <popup-impl.hh>
#include <scroller-impl.hh>
#include <theme-impl.hh>
#include <window-impl.hh>
#include <iostream>

namespace tau {

Widget_impl::Widget_impl() {
    ++widgets_;
    signal_parent().connect(fun(this, &Widget_impl::on_parent), true);
    conf_.signal_changed(Conf::BACKGROUND).connect(bind_back(fun(this, &Widget_impl::invalidate), Rect()));
    signal_origin_changed_.connect(bind_back(fun(this, &Widget_impl::invalidate), Rect()));
    signal_size_changed_.connect(fun(this, &Widget_impl::on_size_changed));
    signal_show_.connect(fun(this, &Widget_impl::on_show));
    signal_hide_.connect(fun(this, &Widget_impl::on_hide));
    signal_enable_.connect(fun(this, &Widget_impl::on_enable));
    signal_disable_.connect(fun(this, &Widget_impl::on_disable));
    signal_display_in_.connect(fun(this, &Widget_impl::on_display_in));
    signal_display_out_.connect(fun(this, &Widget_impl::hide_tooltip));
    signal_select_.connect(fun(this, &Widget_impl::on_select));
    signal_unselect_.connect(fun(this, &Widget_impl::on_unselect));
}

Widget_impl::~Widget_impl() {
    --widgets_;
    destroy();
    if (signal_accel_) { delete signal_accel_; }
    if (signal_offset_changed_) { delete signal_offset_changed_; }
    if (signal_key_down_) { delete signal_key_down_; }
    if (signal_key_up_) { delete signal_key_up_; }
    if (signal_input_) { delete signal_input_; }
    if (signal_mouse_down_) { delete signal_mouse_down_; }
    if (signal_mouse_double_click_) { delete signal_mouse_double_click_; }
    if (signal_mouse_up_) { delete signal_mouse_up_; }
    if (signal_mouse_wheel_) { delete signal_mouse_wheel_; }
    if (signal_mouse_motion_) { delete signal_mouse_motion_; }
    if (signal_mouse_enter_) { delete signal_mouse_enter_; }
    if (signal_mouse_leave_) { delete signal_mouse_leave_; }
    if (signal_paint_) { delete signal_paint_; }
    if (signal_backpaint_) { delete signal_backpaint_; }
    if (signal_run_) { delete signal_run_; }
    if (signal_unchain_) { delete signal_unchain_; }
}

void Widget_impl::on_parent(Object * oparent) {
    if (oparent) {
        if (!container_) {
            if ((container_ = dynamic_cast<Container_impl *>(oparent))) {
                conf_.set_parent(container_->conf());
                if (auto scroller = dynamic_cast<Scroller_impl *>(container_)) { pan_cx_ = scroller->signal_pan_changed().connect(fun(this, &Widget_impl::on_pan_changed)); }
                shutdown(false);
            }
        }
    }

    // Unparent.
    else {
        selected_ = false;
        pan_cx_.drop();
        hide_tooltip();

        if (container_) {
            container_ = nullptr;
            handle_display(false);
            handle_visibility(false);
        }

        shutdown(true);
        conf_.unparent();
        origin_.set(INT_MIN, INT_MIN);
        worigin_.set(INT_MIN, INT_MIN);
        poffset_.set(INT_MIN, INT_MIN);
        size_.reset();
        required_size_.reset();
    }
}

void Widget_impl::enable() {
    if (disabled_) {
        disabled_ = false;

        if (/*!in_shutdown() &&*/ !frozen_) {
            // if (sensitive_) { signal_enable_(); }
            signal_enable_();
        }
    }
}

void Widget_impl::thaw() {
    if (frozen_) {
        frozen_ = false;

        if (/*!in_shutdown() &&*/ !disabled_) {
            // if (sensitive_) { signal_enable_(); }
            signal_enable_();
        }
    }
}

void Widget_impl::disable() {
    if (!disabled_) {
        disabled_ = true;

        if (/*!in_shutdown() &&*/ !frozen_) {
            // if (sensitive_) { signal_disable_(); }
            signal_disable_();
        }
    }
}

void Widget_impl::freeze() {
    if (!frozen_) {
        frozen_ = true;

        if (/*!in_shutdown() &&*/ !disabled_) {
            // if (sensitive_) { signal_disable_(); }
            signal_disable_();
        }
    }
}

void Widget_impl::par_enable(bool yes) {
    if (yes) { enable(); }
    else { disable(); }
}

void Widget_impl::show() {
    if (hidden_) {
        hidden_ = false;

        if (!in_shutdown() && !disappeared_) {
            signal_hints_changed_(Hints::SHOW);
            if (visible()) { signal_show_(); }
        }
    }
}

void Widget_impl::appear() {
    if (disappeared_) {
        disappeared_ = false;

        if (!in_shutdown() && !hidden_) {
            signal_hints_changed_(Hints::SHOW);
            if (visible()) { signal_show_(); }
        }
    }
}

void Widget_impl::hide() {
    if (!hidden_) {
        bool was_visible = visible();
        hidden_ = true;

        if (!in_shutdown() && !disappeared_) {
            signal_hints_changed_(Hints::HIDE);
            if (was_visible) { signal_hide_(); }
        }
    }
}

void Widget_impl::disappear() {
    if (!disappeared_) {
        bool was_visible = visible();
        disappeared_ = true;

        if (!in_shutdown() && !hidden_) {
            signal_hide_();
            signal_hints_changed_(Hints::HIDE);
            if (was_visible) { signal_hide_(); }
        }
    }
}

void Widget_impl::par_show(bool yes) {
    if (yes) { show(); }
    else { hide(); }
}

// Overridden by Window_impl.
void Widget_impl::invalidate(const Rect & r) {
    if (!in_shutdown() && container_) {
        Rect va { offset(), size() }, inval { r ? r : va };
        inval.intersect(va);
        inval.translate(origin_-offset());
        if (inval) { container_->invalidate(inval); }
    }
}

// Overridden by Container_impl.
bool Widget_impl::hover() const noexcept {
    return !in_shutdown() && container_ && (this == container_->mouse_grabber() || this == container_->mouse_owner());
}

bool Widget_impl::hint_margin_left(unsigned left) {
    auto m = margin_hint_;
    m.left = left;
    return hint_margin(m);
}

bool Widget_impl::hint_margin_right(unsigned right) {
    auto m = margin_hint_;
    m.right = right;
    return hint_margin(m);
}

bool Widget_impl::hint_margin_top(unsigned top) {
    auto m = margin_hint_;
    m.top = top;
    return hint_margin(m);
}

bool Widget_impl::hint_margin_bottom(unsigned bottom) {
    auto m = margin_hint_;
    m.bottom = bottom;
    return hint_margin(m);
}

bool Widget_impl::hint_margin(const Margin & margin) {
    if (margin_hint_ != margin) {
        margin_hint_ = margin;
        if (!in_shutdown()) { signal_hints_changed_(Hints::SIZE); }
        return true;
    }

    return false;
}

bool Widget_impl::hint_size(const Size & sz) {
    if (size_hint_.update(sz) && !in_shutdown()) { signal_hints_changed_(Hints::SIZE); return true; }
    return false;
}

bool Widget_impl::hint_min_size(const Size & sz) {
    if (min_size_hint_.update(sz) && !in_shutdown()) { signal_hints_changed_(Hints::SIZE); return true; }
    return false;
}

bool Widget_impl::hint_max_size(const Size & sz) {
    if (max_size_hint_.update(sz) && !in_shutdown()) { signal_hints_changed_(Hints::SIZE); return true; }
    return false;
}

bool Widget_impl::require_size(const Size & sz) {
    if (required_size_.update(sz) && !in_shutdown()) { signal_hints_changed_(Hints::SIZE); return true; }
    return false;
}

bool Widget_impl::update_origin(const Point & pt) {
    bool changed = origin_.update(pt);

    if (!in_shutdown()) {
        if (changed) { signal_origin_changed_(); }
        else { update_pdata(); }
    }

    return changed;
}

bool Widget_impl::update_size(const Size & size) {
    Size was_size = size_;
    bool changed = size_.update(size);

    if (!in_shutdown()) {
        update_pdata();

        if (changed) {
            signal_size_changed_();
            if (!was_size && visible()) { signal_show_(); }
            else if (!size_) { signal_hide_(); }
        }
    }

    return changed;
}

// Overridden by Window_xxx.
Painter Widget_impl::painter() {
    if (!in_shutdown()) {
        if (auto wnd = window()) {
            if (auto pr = wnd->painter()) {
                if (auto pi = pr.ptr()) {
                    pi->capture(this);
                    signal_show_.connect(bind_back(fun(pi, &Painter_impl::capture), this));
                    signal_hide_.connect(bind_back(fun(pi, &Painter_impl::capture), this));
                    signal_pdata_changed_.connect(bind_back(fun(pi, &Painter_impl::capture), this), true);
                }

                return pr;
            }
        }
    }

    return Painter();
}

// Overridden by Container_impl.
// Overridden by Window_impl.
bool Widget_impl::grab_mouse_up(Widget_impl *) {
    if (!in_shutdown() && has_display() && enabled() && container_) {
        if (this == container_->mouse_grabber()) { return true; }
        return container_->grab_mouse_up(this);
    }

    return false;
}

bool Widget_impl::grab_mouse() {
    bool had_mouse = hover();

    if (grab_mouse_up(this)) {
        if (!had_mouse) {
            handle_mouse_enter(where_mouse());
        }

        return true;
    }

    return false;
}

// Overridden by Container_impl.
// Overridden by Window_impl.
bool Widget_impl::ungrab_mouse_up(Widget_impl *) {
    return !in_shutdown() && container_ && container_->ungrab_mouse_up(this);
}

bool Widget_impl::ungrab_mouse() {
    if (grabs_mouse()) {
        bool had_mouse = hover();

        if (ungrab_mouse_up(this)) {
            if (!hover() && had_mouse) { handle_mouse_leave(); }
            return true;
        }
    }

    return false;
}

// Overridden by Container_impl.
// Overridden by Window_impl.
bool Widget_impl::grabs_mouse() const noexcept {
    return container_ && this == container_->mouse_grabber();
}

// Overridden by Window_impl.
Point Widget_impl::where_mouse() const noexcept {
    return !in_shutdown() && container_ ? container_->where_mouse()-origin() : Point(INT_MIN, INT_MIN);
}

// Overridden by Window_impl.
Point Widget_impl::to_screen(const Point & pt) const noexcept {
    return !in_shutdown() && container_ ? container_->to_screen(pt+origin())-container_->offset() : pt;
}

// Overridden by Popup_impl.
// Overridden by Toplevel_impl.
Window_impl * Widget_impl::toplevel() noexcept {
    return !in_shutdown() && container_ ? container_->toplevel() : nullptr;
}

// Overridden by Popup_impl.
// Overridden by Toplevel_impl.
const Window_impl * Widget_impl::toplevel() const noexcept {
    return !in_shutdown() && container_ ? container_->toplevel() : nullptr;
}

// Overridden by Window_impl.
// Overridden by Container_impl.
Window_impl * Widget_impl::window() noexcept {
    return !in_shutdown() && container_ ? container_->window() : nullptr;
}

// Overridden by Window_impl.
// Overridden by Container_impl.
const Window_impl * Widget_impl::window() const noexcept {
    return !in_shutdown() && container_ ? container_->window() : nullptr;
}

// Overridden by Popup_impl.
// Overridden by Container_impl.
Point Widget_impl::to_parent(const Container_impl * ci, const Point & pt) const noexcept {
    Point cpt(pt);

    if (!in_shutdown() && container_) {
        cpt += origin_-container_->offset();
        cpt += container_->to_parent(ci);
    }

    return cpt;
}

void Widget_impl::set_tooltip(const ustring & s, Align align) {
    unset_tooltip();
    tooltip_text_ = s;
    tooltip_align_ = align;
}

void Widget_impl::set_tooltip(Widget_ptr tooltip_widget) {
    unset_tooltip();
    tooltip_widget_ = tooltip_widget;
}

bool Widget_impl::has_tooltip() const noexcept {
    return !tooltip_text_.empty() || (nullptr != tooltip_widget_);
}

void Widget_impl::unset_tooltip() {
    hide_tooltip();
    tooltip_widget_.reset();
    tooltip_text_.clear();
}

Widget_ptr Widget_impl::show_tooltip(const ustring & s, Align align) {
    auto tp = std::make_shared<Label_impl>(s, align);
    tp->hint_margin(2);
    show_tooltip(tp);
    return tp;
}

Widget_ptr Widget_impl::show_tooltip(const ustring & s, const Point & pt, Gravity gravity, unsigned time_ms) {
    auto tp = std::make_shared<Label_impl>(s);
    tp->hint_margin(2);
    show_tooltip(tp, pt, gravity, time_ms);
    return tp;
}

Widget_ptr Widget_impl::show_tooltip(const ustring & s, Align align, const Point & pt, Gravity gravity, unsigned time_ms) {
    auto tp = std::make_shared<Label_impl>(s, align);
    tp->hint_margin(2);
    show_tooltip(tp, pt, gravity, time_ms);
    return tp;
}

void Widget_impl::show_tooltip(Widget_ptr twp) {
    if (auto wip = window()) {
        if (auto dp = wip->display()) {
            Point       pt = wip->where_mouse();
            Gravity     gravity;
            Rect        cr = dp->cursor_area();
            Size        sz = wip->size();
            int         x = pt.x(), y = pt.y(), zx = sz.width()/3, zy = sz.height()/3;

            if (x < zx) {
                if (y < zy) {
                    x += cr.right();
                    y += cr.bottom();
                    gravity = Gravity::TOP_LEFT;
                }

                else if (pt.y() < zy+zy) {
                    x += cr.right();
                    y += cr.bottom();
                    gravity = Gravity::LEFT;
                }

                else {
                    y += cr.top();
                    gravity = Gravity::BOTTOM_LEFT;
                }
            }

            else if (x < zx+zx) {
                if (y < zy) {
                    y += cr.bottom();
                    gravity = Gravity::TOP_LEFT;
                }

                else if (pt.y() < zy+zy) {
                    gravity = Gravity::BOTTOM;
                }

                else {
                    y += cr.top();
                    gravity = Gravity::BOTTOM_LEFT;
                }
            }

            else {
                x += cr.left();

                if (y < zy) {
                    gravity = Gravity::TOP_RIGHT;
                }

                else if (pt.y() < zy+zy) {
                    gravity = Gravity::RIGHT;
                }

                else {
                    y += cr.top();
                    gravity = Gravity::BOTTOM_RIGHT;
                }
            }

            show_tooltip(twp, Point(x, y)-to_parent(wip), gravity);
        }
    }
}

void Widget_impl::show_tooltip(Widget_ptr twp, const Point & pt, Gravity gravity, unsigned time_ms) {
    hide_tooltip();

    if (auto wip = window()) {
        if (auto dp = wip->display()) {
            wip->signal_close_tooltip()();
            auto tip = dp->create_popup(wip, to_parent(wip, pt), gravity);

            // Prevent overwrite configuration items already set by user.
            if (!twp->conf().is_set(Conf::BACKGROUND)) {
                tip->conf().unset(Conf::BACKGROUND);
                tip->conf().set_from(Conf::BACKGROUND, Conf::HELP_BACKGROUND);
            }

            // Prevent overwrite configuration items already set by user.
            if (!twp->conf().is_set(Conf::FOREGROUND)) {
                tip->conf().unset(Conf::FOREGROUND);
                tip->conf().set_from(Conf::FOREGROUND, Conf::TOOLTIP_FOREGROUND);
            }

            // Prevent overwrite configuration items already set by user.
            if (!twp->conf().is_set(Conf::FONT)) {
                tip->conf().unset(Conf::FONT);
                tip->conf().redirect(Conf::FONT, Conf::TOOLTIP_FONT);
            }

            // Prevent overwrite configuration items already set by user.
            if (!twp->conf().is_set(Conf::EDIT_FONT)) {
                tip->conf().unset(Conf::EDIT_FONT);
                tip->conf().redirect(Conf::EDIT_FONT, Conf::TOOLTIP_FONT);
            }

            wip->signal_close().connect(fun(tip, &Window_impl::close));
            wip->signal_close_tooltip().connect(fun(tip, &Window_impl::close));

            Color c = twp->conf().color(Conf::FOREGROUND);
            auto frame = std::make_shared<Frame_impl>(Border::SOLID, c);
            tip->insert(frame);
            frame->insert(twp);

            tip->show();
            tip->signal_close().connect(fun(this, &Widget_impl::on_tooltip_close));
            tip->signal_mouse_down().connect(bind_front(fun(this, &Widget_impl::on_tooltip_mouse), true));  // FIXME before?
            tip->signal_mouse_wheel().connect(bind_front(fun(this, &Widget_impl::on_tooltip_mouse), true)); // FIXME before?
            tooltip1_cx_ = wip->signal_mouse_down().connect(bind_front(fun(this, &Widget_impl::on_tooltip_mouse), false));    // FIXME before?
            tooltip2_cx_ = wip->signal_mouse_wheel().connect(bind_front(fun(this, &Widget_impl::on_tooltip_mouse), false));   // FIXME before?

            wip->open_tooltip(this, time_ms ? time_ms : 7398);
            tooltip_exposed_ = true;
        }
    }
}

// Handles both mouse_down and mouse_wheel.
bool Widget_impl::on_tooltip_mouse(bool result, int, int, const Point &) {
    hide_tooltip();
    return result;
}

void Widget_impl::on_tooltip_close() {
    tooltip_exposed_ = false;
    tooltip1_cx_.drop();
    tooltip2_cx_.drop();
}

void Widget_impl::hide_tooltip() {
    tooltip_timer_cx_.drop();

    if (tooltip_exposed_) {
        if (auto wip = window()) {
            wip->signal_close_tooltip()();
        }
    }

    tooltip1_cx_.drop();
    tooltip2_cx_.drop();
}

void Widget_impl::allow_tooltip() {
    tooltip_allowed_ = true;
    trigger_tooltip_timer();
}

void Widget_impl::disallow_tooltip() {
    hide_tooltip();
    tooltip_allowed_ = false;
}

void Widget_impl::expose_tooltip() {
    if (hover()) {
        if (tooltip_widget_) { show_tooltip(tooltip_widget_); }
        else if (!tooltip_text_.empty()) { show_tooltip(tooltip_text_, tooltip_align_); }
        tooltip_delay_ = std::min(3015U, tooltip_delay_+212U);
    }
}

void Widget_impl::trigger_tooltip_timer() {
    if (enabled() && has_tooltip() && !tooltip_exposed_ && tooltip_allowed_ && tooltip_timer_cx_.empty()) {
        if (auto dp = display()) {
            if (auto loop = dp->loop()) {
                tooltip_timer_cx_ = loop->alarm(fun(this, &Widget_impl::on_tooltip_timer), tooltip_delay_);
            }
        }
    }
}

void Widget_impl::on_enable() {
    if (enabled() && hover()) {
        enter_cursor();
    }
}

void Widget_impl::on_disable() {
    if (!enabled()) {
        drop_focus();
        ungrab_mouse();
        hide_tooltip();
        if (hover()) { leave_cursor(); }
    }
}

void Widget_impl::offset(const Point & pt) {
    if (auto scroller = dynamic_cast<Scroller_impl *>(container_)) {
        scroller->pan(pt);
    }
}

void Widget_impl::offset(int x, int y) {
    offset(Point(x, y));
}

Point Widget_impl::offset() const noexcept {
    Point pt;
    if (auto scroller = dynamic_cast<Scroller_impl *>(container_)) { pt = scroller->pan(); }
    return pt;
}

signal<void()> & Widget_impl::signal_offset_changed() {
    if (!signal_offset_changed_) { signal_offset_changed_ = new signal<void()>; }
    return *signal_offset_changed_;
}

// Connected to owning Scroller::signal_pan_changed().
void Widget_impl::on_pan_changed() {
    update_pdata();
    invalidate();
    if (signal_offset_changed_) { (*signal_offset_changed_)(); }
}

// Overridden by Window_impl.
bool Widget_impl::has_modal() const noexcept {
    return container_ && container_->modal_child() == this;
}

// Overridden by Container_impl.
// Overridden by Window_impl.
bool Widget_impl::grab_modal_up(Widget_impl * caller) {
    return container_ && container_->grab_modal_up(this);
}

bool Widget_impl::grab_modal() {
    if (focusable() && grab_modal_up(this)) {
        handle_focus_in();  // FIXME remove this?
        return true;
    }

    return false;
}

// Overridden by Container_impl.
// Overridden by Window_impl.
bool Widget_impl::end_modal_up(Widget_impl * ) {
    return container_ && container_->end_modal_up(this);
}

bool Widget_impl::end_modal() {
    return end_modal_up(this);
}

// Overridden by Window_impl.
bool Widget_impl::grabs_modal() const noexcept {
    return has_modal();
}

void Widget_impl::handle_focus_in() {
    if (!focused_ && focusable()) {
        focused_ = true;
        hide_tooltip();
        signal_focus_in_();
        track_pan(this);
    }
}

void Widget_impl::handle_focus_out() {
    if (focused_) {
        focused_ = false;
        signal_focus_out_();
    }
}

// Overridden by Container_impl.
// Overridden by Window_impl.
int Widget_impl::grab_focus_up(Widget_impl *) {
    return focusable() && container_ ? container_->grab_focus_up(this) : -1;
}

bool Widget_impl::grab_focus() {
    dropped_ = false;               // TODO REWORK? Set by drop_focus().
    int res = -1;
    if (!in_shutdown()) { res = grab_focus_up(this); }
    if (dropped_) { res = -1, dropped_ = false; }
    return res >= 0;
}

bool Widget_impl::focusable() const noexcept {
    return focusable_ && enabled() && !hidden();
}

bool Widget_impl::take_focus() {
    return focusable() && signal_take_focus_() && focused();
}

// Overridden by Container_impl.
// Overridden by Window_impl.
void Widget_impl::drop_focus_up(Widget_impl *) {
    if (!in_shutdown() && container_) {
        container_->drop_focus_up(this);
    }
}

void Widget_impl::drop_focus() {
    drop_focus_up(this);
    dropped_ = true;                // TODO REWORK? Cleared by grab_focus().
}

// Overridden by Container_impl.
void Widget_impl::resume_focus() {}

// Overridden by Container_impl.
void Widget_impl::suspend_focus() {}

// Overridden by Container_impl.
void Widget_impl::clear_focus() {
    handle_focus_out();
}

// Overridden by Container_impl.
void Widget_impl::clear_modal() {
    handle_focus_out();
}

void Widget_impl::allow_focus() {
    if (signal_unchain_ && signal_unchain_->empty()) { delete signal_unchain_; signal_unchain_ = nullptr; }
    focusable_ = true;
}

void Widget_impl::disallow_focus() {
    if (focusable_) {
        focusable_ = false;
        clear_focus();
        if (signal_unchain_) { signal_unchain_->operator()(); }
    }
}

// Overridden by Window_impl.
Display_impl * Widget_impl::display() noexcept {
    return !in_shutdown() && container_ && has_display_ ? container_->display() : nullptr;
}

// Overridden by Window_impl.
const Display_impl * Widget_impl::display() const noexcept {
    return !in_shutdown() && container_ && has_display_ ? container_->display() : nullptr;
}

void Widget_impl::update_cursor() {
    if (!cursor_name_.empty()) {
        if (auto cursor = Theme_impl::root()->find_cursor(cursor_name_, cursor_size_)) {
            set_cursor(cursor);
        }
    }
}

// Overridden by Container_impl.
// Overridden by Window_impl.
void Widget_impl::set_cursor_up(Cursor_ptr cursor, Widget_impl * wip) {
    if (!in_shutdown() && container_) {
        container_->set_cursor_up(cursor, wip);
    }
}

// Overridden by Container_impl.
// Overridden by Window_impl.
void Widget_impl::unset_cursor_up(Widget_impl * wip) {
    if (!in_shutdown() && container_) {
        container_->unset_cursor_up(wip);
    }
}

void Widget_impl::set_cursor(Cursor_ptr cursor) {
    if (cursor) {
        cursor_ = cursor;
        if (hover() && cursor_visible() && has_display() && enabled()) { set_cursor_up(cursor_, this); }
    }
}

void Widget_impl::set_cursor(const ustring & name, unsigned size) {
    if (!name.empty()) {
        cursor_name_ = name;
        cursor_size_ = size;

        if (auto cursor = Theme_impl::root()->find_cursor(name, size)) {
            set_cursor(cursor);
            cursor_theme_cx_ = Theme_impl::root()->signal_cursors_changed().connect(fun(this, &Widget_impl::update_cursor));
        }
    }
}

void Widget_impl::unset_cursor() {
    if (cursor_) { cursor_.reset(); unset_cursor_up(this); }
    cursor_theme_cx_.drop();
    cursor_name_.clear();
    cursor_size_ = 0;
}

// Overridden by Container_impl.
// Overridden by Window_impl.
void Widget_impl::show_cursor_up() {
    if (!in_shutdown() && container_ && hover()) {
        container_->show_cursor_up();
    }
}

void Widget_impl::show_cursor() {
    if (cursor_hidden_) {
        cursor_hidden_ = false;
        show_cursor_up();
    }
}

// Overridden by Window_impl.
void Widget_impl::hide_cursor_up() {
    if (!in_shutdown() && container_ && hover()) {
        container_->hide_cursor_up();
    }
}

void Widget_impl::hide_cursor() {
    if (!cursor_hidden_) {
        cursor_hidden_ = true;
        hide_cursor_up();
    }
}

// Overridden by Window_impl.
bool Widget_impl::cursor_visible() const noexcept {
    return !cursor_hidden() && visible() && !in_shutdown() && container_ && container_->cursor_visible();
}

// Overridden by Window_impl.
void Widget_impl::quit_dialog() {
    if (!in_shutdown() && container_) { container_->quit_dialog(); }
}

Accel * Widget_impl::allocate_accel(char32_t kc, int km, bool prepend) {
    auto & acc = accels_.emplace_front(kc, km);
    connect_accel(acc, prepend);
    return &acc;
}

Accel * Widget_impl::allocate_accel(const ustring & spec, bool prepend) {
    auto & acc = accels_.emplace_front(spec);
    connect_accel(acc, prepend);
    return &acc;
}

connection Widget_impl::connect_accel(Accel & accel, bool prepend) {
    auto cx = signal_accel().connect(fun(accel, &Accel::handle_accel), prepend);
    accel.handle_cx_ = cx;
    return cx;
}

void Widget_impl::connect_action(Action_base & action, bool prepend) {
    for (auto * acc: action.accels()) { connect_accel(*acc, prepend); }
    action.signal_accel_added().connect(fun(this, &Widget_impl::on_action_accel_added));
    signal_select_.connect(fun(action.signal_select()));
    signal_unselect_.connect(fun(action.signal_unselect()));
    if (selected()) { action.signal_select()(); }
}

void Widget_impl::on_action_accel_added(const Accel & accel) {
    connect_accel(const_cast<Accel &>(accel));
}

// Overridden by Container_impl.
bool Widget_impl::handle_accel(char32_t kc, int km) {
    return enabled() && signal_accel_ && (*signal_accel_)(kc, km);
}

// Overridden by Container_impl.
bool Widget_impl::handle_key_down(char32_t kc, int km) {
    return enabled() && signal_key_down_ && (*signal_key_down_)(kc, km);
}

// Overridden by Container_impl.
bool Widget_impl::handle_key_up(char32_t kc, int km) {
    return enabled() && signal_key_up_ && (*signal_key_up_)(kc, km);
}

// Overridden by Container_impl.
bool Widget_impl::handle_input(const ustring & s, int src) {
    return enabled() && signal_input_ && (*signal_input_)(std::cref(s), src);
}

// Overridden by Container_impl.
bool Widget_impl::handle_mouse_down(int mbt, int mm, const Point & pt) {
    return enabled() && signal_mouse_down_ && (*signal_mouse_down_)(mbt, mm, pt);
}

// Overridden by Container_impl.
bool Widget_impl::handle_mouse_up(int mbt, int mm, const Point & pt) {
    return enabled() && signal_mouse_up_ && (*signal_mouse_up_)(mbt, mm, pt);
}

// Overridden by Container_impl.
bool Widget_impl::handle_mouse_double_click(int mbt, int mm, const Point & pt) {
    return enabled() && signal_mouse_double_click_ && (*signal_mouse_double_click_)(mbt, mm, pt);
}

// Overridden by Container_impl.
void Widget_impl::handle_mouse_motion(int mm, const Point & pt) {
    if (enabled() && signal_mouse_motion_) { (*signal_mouse_motion_)(mm, pt); }
}

void Widget_impl::enter_cursor() {
    if (cursor_hidden_) {
        hide_cursor_up();
    }

    else if (cursor_) {
        set_cursor_up(cursor_, this);
    }
}

void Widget_impl::leave_cursor() {
    if (cursor_hidden_) {
        show_cursor_up();
    }

    else if (cursor_) {
        unset_cursor_up(this);
    }
}

// Overridden by Container_impl.
void Widget_impl::handle_mouse_enter(const Point & pt) {
    enter_cursor();
    trigger_tooltip_timer();
    if (signal_mouse_enter_) { (*signal_mouse_enter_)(pt); }
}

// Overridden by Container_impl.
void Widget_impl::handle_mouse_leave() {
    leave_cursor();
    if (signal_mouse_leave_) { (*signal_mouse_leave_)(); }
}

// Overridden by Container_impl.
bool Widget_impl::handle_mouse_wheel(int delta, int mm, const Point & pt) {
    return enabled() && signal_mouse_wheel_ && (*signal_mouse_wheel_)(delta, mm, pt);
}

// Overriden by Frame_impl.
// Overriden by Table_impl.
// Overriden by Window_impl.
// Return false to allow user append it's own slot after this one.
bool Widget_impl::on_backpaint(Painter pr, const Rect & inval) {
    auto & ci = conf_.item(Conf::BACKGROUND);

    if (ci.is_set()) {
        pr.set_brush(Brush(ci.str()));
        pr.paint();
    }

    return false;
}

// Overridden by Flat_container.
// Overridden by Layered_container.
void Widget_impl::handle_paint(Painter pr, const Rect & inval) {
    if (signal_paint_) { signal_paint_->operator()(pr, inval.translated(offset())); }
}

// Overridden by Flat_container.
// Overridden by Layered_container.
void Widget_impl::handle_backpaint(Painter pr, const Rect & inval) {
    auto r = inval.translated(offset());
    if (!signal_backpaint_ || signal_backpaint_->empty()) { on_backpaint(pr, r); }
    else { signal_backpaint_->operator()(pr, r); }
}

bool Widget_impl::has_parent(const Widget_impl * cwp) const noexcept {
    for (auto cp = container_; cp; cp = cp->container()) {
        if (cp == cwp) {
            return true;
        }
    }

    return false;
}

bool Widget_impl::scrollable() const noexcept {
    return dynamic_cast<Scroller_impl *>(container_);
}

Size Widget_impl::logical_size() const noexcept {
    Size sz(size_);
    if (scrollable()) { sz |= required_size(); }
    return sz;
}

void Widget_impl::handle_sensitivity(bool sens) {
    if (sensitive_ != sens) {
        bool was = enabled();
        sensitive_ = sens;

        if (!in_shutdown()) {
            if (was != enabled()) {
                if (sensitive_) { signal_enable_(); }
                else { signal_disable_(); }
            }
        }
    }
}

bool Widget_impl::visible() const noexcept {
    return visible_ && size() && !hidden();
}

void Widget_impl::handle_display(bool in) {
    if (in) {
        if (!has_display_) {
            has_display_ = true;
            if (!in_shutdown()) { signal_display_in_(); }
            if (visible_ && !hidden()) { signal_show_(); }
        }
    }

    else {
        if (has_display_) {
            has_display_ = false;
            if (!in_shutdown()) { signal_display_out_(); }
        }
    }
}

void Widget_impl::handle_visibility(bool visible) {
    if (visible_ != visible) {
        visible_ = visible;

        if (has_display_ && !hidden()) {
            if (visible) { signal_show_(); }
            else { signal_hide_(); }
        }
    }
}

void Widget_impl::on_size_changed() {
    refresh_track_pan();
    invalidate();
}

void Widget_impl::on_show() {
    worigin_.set(INT_MIN, INT_MIN);
    update_pdata();
    refresh_track_pan();
    invalidate();
}

void Widget_impl::on_hide() {
    update_pdata();
    hide_tooltip();
    drop_focus();
}

void Widget_impl::update_pdata() {
    Point po(INT_MIN, INT_MIN);
    worigin_ = po;

    if (auto wip = window()) {
        Rect  cr;

        if (visible()) {
            po = offset()-origin_;
            cr.set(size_);

            if (!in_shutdown() && container_) {
                po += container_->poffset();
                cr &= container_->viewable_area().translated(container_->offset()-origin_);
            }
        }

        bool changed = false, viewable_changed = false;

        if (worigin_.update(to_parent(wip))) {
            changed = true;
        }

        if (poffset_.update(po)) {
            changed = true;
        }

        if (viewable_area_ != cr) {
            changed = true;
            viewable_changed = cr.empty() || viewable_area_.empty();
            viewable_area_ = cr;
        }

        if (viewable_changed && container_) {
            container_->on_child_viewable(this, !cr.empty());
        }

        if (changed) {
            signal_pdata_changed_();
        }
    }
}

void Widget_impl::rise() {
    if (container_) { container_->rise(this); }
}

void Widget_impl::lower() {
    if (container_) { container_->lower(this); }
}

int Widget_impl::level() const noexcept {
    return container_ ? container_->level(this) : 0;
}

bool Widget_impl::hint_aspect_ratio(double ratio) {
    if (0.001 <= ratio && ratio <= 2000.0) {
        double was = required_aspect_ratio();
        aspect_ratio_hint_ = ratio;
        if (was != required_aspect_ratio()) { signal_hints_changed_(Hints::SIZE); }
    }

    return false;
}

double Widget_impl::required_aspect_ratio() const noexcept {
    return  0.001 <= required_aspect_ratio_ && required_aspect_ratio_ <= 2000.0 ? required_aspect_ratio_ :
            (0.001 <= aspect_ratio_hint_ && aspect_ratio_hint_ <= 2000.0 ? aspect_ratio_hint_ : 0.0);
}

bool Widget_impl::require_aspect_ratio(double ratio) {
    if (0.001 <= ratio && ratio <= 2000.0) {
        double was = required_aspect_ratio();
        required_aspect_ratio_ = ratio;
        if (was != required_aspect_ratio()) { signal_hints_changed_(Hints::SIZE); }
    }

    return false;
}

void Widget_impl::facade_in(Widget * w) {
    if (!facade_) {
        facade_ = w;
    }
}

void Widget_impl::facade_out(Widget * w) {
    if (facade_ == w) {
        facade_ = nullptr;
    }
}

void Widget_impl::on_display_in() {
    update_cursor();
    update_pdata();
    if (hidden()) { signal_hide_(); }   // FIXME Is it workaround?
    refresh_track_pan();

    // Try to connect signal_run_ to the loop.
    if (signal_run_ && !run2_cx_.empty() && run1_cx_.empty()) {
        if (auto dp = display()) {
            if (auto lp = dp->loop()) {
                run1_cx_ = lp->signal_run().connect(fun(signal_run_));
            }
        }
    }
}

void Widget_impl::on_run() {
    if (!signal_run_ || signal_run_->size() < 2) {
        run1_cx_.drop(), run2_cx_.drop();
        if (signal_run_) { delete signal_run_; signal_run_ = nullptr; }
    }
}

connection Widget_impl::connect_run(slot<void()> slot, bool prepend) {
    if (!signal_run_) { signal_run_ = new signal<void()>; }

    if (run1_cx_.empty()) {
        // Trying to connect to the loop.
        // If unsuccessful, on_display_in() will do it.
        if (auto dp = display()) {
            if (auto lp = dp->loop()) {
                run1_cx_ = lp->signal_run().connect(fun(signal_run_));
            }
        }
    }

    // Connect sentinel.
    run2_cx_ = signal_run_->connect(fun(this, &Widget_impl::on_run));
    return signal_run_->connect(slot, prepend);
}

void Widget_impl::on_select() {
    selected_ = true;
    refresh_track_pan();
}

void Widget_impl::on_unselect() {
    selected_ = false;
}

// Overridden by Scroller_impl.
void Widget_impl::track_pan(Widget_impl * wp) {
    if (container_ && !in_shutdown()) {
        container_->track_pan(wp);
    }
}

void Widget_impl::refresh_track_pan() {
    if (container_ && !in_shutdown() && has_display_ && visible() && (focused() || selected())) { container_->track_pan(this); }
}

// Overridden by Container_impl.
signal<bool(int, int, Point)> & Widget_impl::signal_mouse_down() {
    if (!signal_mouse_down_) { signal_mouse_down_ = new signal<bool(int, int, Point)>; }
    return *signal_mouse_down_;
}

// Overridden by Container_impl.
signal<bool(int, int, Point)> & Widget_impl::signal_mouse_double_click() {
    if (!signal_mouse_double_click_) { signal_mouse_double_click_ = new signal<bool(int, int, Point)>; }
    return *signal_mouse_double_click_;
}

// Overridden by Container_impl.
signal<bool(int, int, Point)> & Widget_impl::signal_mouse_up() {
    if (!signal_mouse_up_) { signal_mouse_up_ = new signal<bool(int, int, Point)>; }
    return *signal_mouse_up_;
}

// Overridden by Container_impl.
signal<void(int, Point)> & Widget_impl::signal_mouse_motion() {
    if (!signal_mouse_motion_) { signal_mouse_motion_ = new signal<void(int, Point)>; }
    return *signal_mouse_motion_;
}

// Overridden by Container_impl.
signal<void(Point)> & Widget_impl::signal_mouse_enter() {
    if (!signal_mouse_enter_) { signal_mouse_enter_ = new signal<void(Point)>; }
    return *signal_mouse_enter_;
}

// Overridden by Container_impl.
signal<void()> & Widget_impl::signal_mouse_leave() {
    if (!signal_mouse_leave_) { signal_mouse_leave_ = new signal<void()>; }
    return *signal_mouse_leave_;
}

// Overridden by Container_impl.
signal<bool(int, int, Point)> & Widget_impl::signal_mouse_wheel() {
    if (!signal_mouse_wheel_) { signal_mouse_wheel_ = new signal<bool(int, int, Point)>; }
    return *signal_mouse_wheel_;
}

signal<bool(Painter, Rect)> & Widget_impl::signal_paint() {
    if (!signal_paint_) { signal_paint_ = new signal<bool(Painter, Rect)>; }
    return *signal_paint_;
}

signal<bool(Painter, Rect)> & Widget_impl::signal_backpaint() {
    if (!signal_backpaint_) {
        signal_backpaint_ = new signal<bool(Painter, Rect)>;
        signal_backpaint_->connect(fun(this, &Widget_impl::on_backpaint));
    }

    return *signal_backpaint_;
}

signal<bool(char32_t, int)> & Widget_impl::signal_accel() {
    if (!signal_accel_) { signal_accel_ = new signal<bool(char32_t, int)>; }
    return *signal_accel_;
}

// Overridden by Container_impl.
signal<bool(char32_t, int)> & Widget_impl::signal_key_down() {
    if (!signal_key_down_) { signal_key_down_ = new signal<bool(char32_t, int)>; }
    return *signal_key_down_;
}

// Overridden by Container_impl.
signal<bool(char32_t, int)> & Widget_impl::signal_key_up() {
    if (!signal_key_up_) { signal_key_up_ = new signal<bool(char32_t, int)>; }
    return *signal_key_up_;
}

// Overridden by Container_impl.
signal<bool(const ustring &, int)> & Widget_impl::signal_input() {
    if (!signal_input_) { signal_input_ = new signal<bool(const ustring &, int)>; }
    return *signal_input_;
}

signal<void()> & Widget_impl::signal_unchain() {
    if (!signal_unchain_) { signal_unchain_ = new signal<void()>; }
    return *signal_unchain_;
}

} // namespace tau

//END

// ----------------------------------------------------------------------------
// SPDX-License-Identifier: BSD-2-Clause
// Copyright © 2014-2024 Konstantin Shmelkov <mrcashe@gmail.com>.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
// THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
// PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
// CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
// EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
// PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
// OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
// WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
// OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
// EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// ----------------------------------------------------------------------------

#include <sys-impl.hh>
#include <loop-impl.hh>
#include <pixmap-impl.hh>
#include <tau/exception.hh>
#include <tau/file.hh>
#include <tau/key-file.hh>
#include <tau/locale.hh>
#include <tau/string.hh>
#include <tau/timer.hh>
#include <errno.h>
#include <algorithm>
#include <atomic>
#include <cstdlib>
#include <filesystem>
#include <fstream>
#include <iomanip>
#include <iostream>
#include <ostream>
#include <mutex>

namespace fs = std::filesystem;

namespace {

using Mutex = std::recursive_mutex;

Mutex           mx_;
tau::ustring    prefix_;
tau::ustring    share_;
tau::ustring    program_name_;

} // anonymous namespace

namespace tau {

Key_file        kache_;
unsigned        timers_;
unsigned        widgets_;
unsigned        signals_;
unsigned        slots_;
unsigned        trackables_;
unsigned        tracks_;

void boot() {
    static std::atomic_flag inited;

    if (!inited.test_and_set()) {
        kache_.create_from_file(path_build(path_user_cache_dir(), str_format("tau-", sysinfo_.Major, '.', sysinfo_.Minor), "cache.ini"));
        auto & sect = kache_.section(uri_escape(path_self()));
        kache_.set_integer(sect, "rating", 1+kache_.get_integer(sect, "rating"));
        boot_linkage();
        boot_sys();
        boot_plat();
        pixmap_xpm_init();
        pixmap_bmp_init();
        pixmap_ico_init();
    }
}

ustring exception::what() const { return msg_; }

app_error::app_error(const ustring & msg)  { msg_ = program_name()+": "+msg; }
app_error::app_error(const ustring & domain, const ustring & msg) { msg_ = domain+": "+msg; }

internal_error::internal_error(const ustring & msg) { msg_ = msg; }

user_error::user_error(const ustring & msg) { msg_ = "user error: "+msg; }

int sys_error::gerror() const noexcept { return gerror_; }

graphics_error::graphics_error(const ustring & msg): internal_error("graphics error: "+msg) {}

bad_doc::bad_doc(const ustring & msg): internal_error(msg) {}

bad_font::bad_font(const ustring & msg): internal_error(msg) {}

bad_pixmap::bad_pixmap(const ustring & path): internal_error("bad pixmap: "+path) {}

Sysinfo sysinfo() noexcept { return Loop_impl::this_ptr()->sysinfo(); }

ustring str_sysinfo() {
    ustring s;

    s += str_format("tau Major:      ", sysinfo_.Major,     '\n');
    s += str_format("tau Minor:      ", sysinfo_.Minor,     '\n');
    s += str_format("tau Micro:      ", sysinfo_.Micro,     '\n');
    s += str_format("Platform:       ", sysinfo_.plat,      '\n');
    s += str_format("System:         ", sysinfo_.uname,     '\n');
    s += str_format("System Major:   ", sysinfo_.osmajor,   '\n');
    s += str_format("System Minor:   ", sysinfo_.osminor,   '\n');

    if (!sysinfo_.distrib.empty()) {
        s += str_format("Distrib:        ", sysinfo_.distrib, '\n');
        s += str_format("Distrib Major:  ", sysinfo_.distrib_major, '\n');
        s += str_format("Distrib Minor:  ", sysinfo_.distrib_minor, '\n');
        if (!sysinfo_.distrib_codename.empty()) { s += str_format("Codename:       ", sysinfo_.distrib_codename, '\n'); }
        if (!sysinfo_.distrib_description.empty()) { s += str_format("Description:    ", sysinfo_.distrib_description, '\n'); }
    }

    s += str_format("Host:           ", sysinfo_.host,       '\n');
    s += str_format("Address Bits:   ", sysinfo_.abits,      '\n');
    s += str_format("int Bits:       ", sysinfo_.ibits,      '\n');
    s += str_format("long Bits:      ", sysinfo_.lbits,      '\n');
    s += str_format("long long Bits: ", sysinfo_.llbits,     '\n');
    s += str_format("intmax_t Bits:  ", sysinfo_.mbits,      '\n');
    s += str_format("wchar_t Bits:   ", sysinfo_.wcbits,     '\n');
    s += str_format("Linkage:        ", (sysinfo_.shared ? "shared" : "static"), '\n');
    if (sysinfo_.shared) { s += str_format("Shared path:    ", (sysinfo_.sopath.empty() ? "NOT FOUND" : sysinfo_.sopath), '\n'); }
    s += str_format("lib_prefix:     ", sysinfo_.lib_prefix, '\n');
    s += str_format("Locale:         ", sysinfo_.locale,     '\n');
    s += str_format("I/O charset:    ", sysinfo_.iocharset,  '\n');
    s += str_format("CPU cores:      ", sysinfo_.cores,      '\n');

    return s;
}

ustring path_build(const ustring & s1, const ustring & s2, char32_t slash) {
    return path_build(s1, path_explode(s2), slash);
}

ustring path_build(const ustring & s1, const std::vector<ustring> & v2, char32_t slash) {
    bool v2_abs = !v2.empty() && ("\\" == v2.front() || "/" == v2.front());

    if (path_is_absolute(s1) && v2_abs) {
        throw user_error(str_format(__func__, ": paths '", s1, "' and '", path_implode(v2, slash), "' are absolute"));
    }

    auto v1 = path_explode(s1);
    std::vector<ustring> v3(v1.size()+v2.size());

    if (v2_abs) {
        std::copy(v2.begin(), v2.end(), v3.begin());
        std::copy(v1.begin(), v1.end(), v3.begin()+v2.size());
    }

    else {
        std::copy(v1.begin(), v1.end(), v3.begin());
        std::copy(v2.begin(), v2.end(), v3.begin()+v1.size());
    }

    return path_implode(v3, slash);
}

ustring path_build(const ustring & s1, const ustring & s2, const ustring & s3, char32_t slash) {
    return path_build(path_build(s1, s2, slash), s3, slash);
}

bool path_is_absolute(const ustring & path) {
    if (path.starts_with('/') || path.starts_with('\\')) { return true; }

    if (path.size() > 2 && ':' == path[1]) {
        char c = toupper(path[0]);
        return 'A' <= c && c <= 'Z';
    }

    return false;
}

ustring path_basename(const ustring & path) {
    std::size_t begin = path.find_last_of("/\\:"), end;
    begin = ustring::npos == begin ? 0 : 1+begin;
    end = path.find_first_of('.', begin);
    if (ustring::npos == end) { end = path.size(); }
    return path.substr(begin, end-begin);
}

ustring path_suffix(const ustring & path) {
    ustring fn = path_notdir(path);
    std::size_t pos = fn.find_last_of(".");
    return pos != ustring::npos ? fn.substr(pos+1) : ustring();
}

ustring path_notdir(const ustring & path) {
    std::size_t size = path.size();

    if (size > 1) {
        if (3 == size && ':' == path[1] && ('\\' == path[2] || '/' == path[2])) {
            return path.substr(0, 2);
        }

        else {
            ustring::size_type pos = path.find_last_of("/\\");
            return pos != ustring::npos ? path.substr(pos+1) : path;
        }
    }

    return path;
}

ustring path_prefix() {
    std::unique_lock lock(mx_);

    if (prefix_.empty()) {
        auto v = path_explode(path_self());

        if (!v.empty()) {
            v.pop_back();

            for (auto j = v.rbegin(); j != v.rend(); ++j) {
                if (str_similar(*j, "bin:lib", ':')) {
                    prefix_ = path_implode({ v.begin(), (++j).base() });
                    return prefix_;
                }
            }
        }

        prefix_ = path_dirname(path_self());
    }

    return prefix_;
}

ustring path_share() {
    std::unique_lock lock(mx_);

    if (share_.empty()) {
        ustring pfx = path_prefix(), s = path_build(pfx, "share", program_name());
        if (file_is_dir(s)) { share_ = s; return share_; }
        auto si = sysinfo();
        s = path_build(pfx, "share", str_format("tau-", si.Major, '.', si.Minor));
        if (file_is_dir(s)) { share_ = s; return share_; }
        s = path_build(pfx, "share");
        if (file_is_dir(s)) { share_ = s; return share_; }
        share_ = pfx;
    }

    return share_;
}

ustring program_name() {
    std::unique_lock lock(mx_);

    if (program_name_.empty()) {
        program_name_ = path_basename(path_self());
        if (program_name_.ends_with("32") || program_name_.ends_with("64")) { program_name_.erase(program_name_.size()-2); }
    }

    return program_name_;
}

ustring program_name(const ustring & name) {
    std::unique_lock lock(mx_);
    auto res = program_name_;

    if (!name.empty()) {
        program_name_ = name;
        share_.clear();
    }

    return res;
}

bool file_exists(const ustring & path) {
    if (path.npos != path.find(path_sep())) {
        for (auto & s: str_explode(path, path_sep())) {
            if (fs::exists(std::wstring(s))) {
                return true;
            }
        }

        return false;
    }

    return fs::exists(std::wstring(path));
}

bool file_is_dir(const ustring & path) {
    return fs::is_directory(std::wstring(path));
}

bool file_is_regular(const ustring & path) {
    return fs::is_regular_file(std::wstring(path));
}

std::vector<ustring> file_find(const ustring & dir, const ustring & mask) {
    std::vector<ustring> v;

    for (const ustring & p: file_glob(path_build(dir, "*"))) {
        if (file_is_dir(p)) {
            for (const ustring & s: file_find(p, mask)) {
                v.emplace_back(s);
            }
        }
    }

    for (const ustring & p: file_glob(path_build(dir, path_notdir(mask)))) {
        if (!file_is_dir(p)) {
            v.emplace_back(p);
        }
    }

    return v;
}

/// FIXME This variant does not checking for a exeecutable flag!
ustring path_which(const ustring & filename, const std::vector<ustring> & paths) {
    for (auto & s: paths) {
        ustring path = path_build(s, filename);
        if (file_exists(path)) { return path; }
    }

    return ustring();
}

std::vector<ustring> path_explode(const ustring & path) {
    std::vector<ustring> v;
    std::size_t m = 0, n = path.size(), k;

    k = path.find_first_of(":/\\");
    if (0 == k && ('/' == path[0] || '\\' == path[0])) { v.emplace_back(1, path[0]); ++m; }
    else if (1 == k && ':' == path[1]) { v.emplace_back(path, 0, 2); m += 2; }
    m = path.find_first_not_of("/\\", m);

    for (; m < n; m = k) {
        k = path.find_first_of("/\\", m); if (path.npos == k) { k = n; }
        if (k > m) { v.emplace_back(path, m, k-m); }
        ++k;
    }

    return v;
}

ustring path_implode(const std::vector<ustring> & v, char32_t slash) {
    ustring s;
    auto i = v.cbegin();

    if (i != v.cend() && s.npos != i->find_first_of("/\\")) { s += *i++; }

    while (i != v.cend()) {
        s += *i++;
        if (i != v.cend()) { s += slash; }
    }

    return s;
}

ustring uri_escape(const ustring & s) {
    std::string es(" !\"#$%&'()*+,\\/:;=?@[]"); ustring res;

    for (char32_t wc: s) {
        if (wc < 0x80 && std::string::npos != es.find(wc)) { res += str_format('%', std::hex, std::setfill('0'), std::setw(2), std::uppercase, unsigned(wc)); }
        else { res.push_back(wc); }
    }

    return res;
}

ustring uri_unescape(const ustring & s) {
    ustring res; std::size_t pos = 0, next, len = s.size();

    try {
        while (pos < len) {
            next = s.find('%', pos);
            if (s.npos == next) { res += s.substr(pos); break; }
            if (next > pos) { res += s.substr(pos, next-pos); }
            ++next; pos = next;
            char32_t wc = std::stoi(s.substr(pos, 2).raw(), nullptr, 16);
            res.push_back(wc);
            pos += 2;
        }
    }

    catch (...) {
        std::cerr << "** uri_unescape(" << s << "): an exception thrown" << std::endl;
    }

    return res;
}

bool uri_has_scheme(const ustring & uri, const ustring & scheme) {
    return str_similar(scheme, uri_scheme(uri));
}

std::optional<std::vector<uint8_t>> shared_file(const ustring & resource_name) {
    auto p = path_which(str_trimleft(resource_name, "\\/:"), { path_share(), path_build(sysinfo_.lib_prefix, "share"),
        path_build(sysinfo_.lib_prefix, "share", str_format("tau-", sysinfo_.Major, '.', sysinfo_.Minor)) });

    if (file_is_regular(p)) {
        try {
            fs::path fsp(p);

            if (auto bytes = fs::file_size(fsp)) {
                std::ifstream is(fsp, std::ios::binary);

                if (is.good()) {
                    std::vector<uint8_t> v(bytes);
                    is.read(reinterpret_cast<char *>(v.data()), bytes);
                    return v;
                }
            }
        }

        catch (...) {}
    }

    return std::nullopt;
}

intmax_t metric(Metric met) {
    switch (met) {
        case Metric::TIMERS:        return timers_;
        case Metric::WIDGETS:       return widgets_;
        case Metric::SIGNALS:       return signals_;
        case Metric::SLOTS:         return slots_;
        case Metric::TRACKABLES:    return trackables_;
        case Metric::TRACKS:        return tracks_;

        default: return 0;
    }
}

ustring str_envsubst(const ustring & s) {
    ustring t;
    std::size_t i = 0, j, z = s.size();

    while (i < z) {
        j = std::min(z, s.find('$', i)); t.append(s, i, j-i);

        if (j < z) {
            i = j; j = std::min(z, s.find_first_of(")}/\\", i+1)); if (j < z && ('}' == s[j] || ')' == s[j])) { ++j; }
            t.append(str_getenv(s.substr(i, j-i)));
        }

        i = j;
    }

    return t;
}

} // namespace tau

//END

// ----------------------------------------------------------------------------
// SPDX-License-Identifier: BSD-2-Clause
// Copyright © 2014-2024 Konstantin Shmelkov <mrcashe@gmail.com>.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
// THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
// PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
// CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
// EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
// PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
// OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
// WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
// OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
// EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// ----------------------------------------------------------------------------

#ifndef __TAU_NOTEBOOK_IMPL_HH__
#define __TAU_NOTEBOOK_IMPL_HH__

#include <card-impl.hh>
#include <tau/notebook.hh>
#include <tau/timeval.hh>
#include <box-impl.hh>
#include <frame-impl.hh>

namespace tau {

class Notebook_impl: public Box_impl {
public:

    Notebook_impl(Side tab_pos=Side::TOP);
   ~Notebook_impl() { destroy(); }

    int  append_page(Widget_ptr wp);
    int  append_page(Widget_ptr wp, const ustring & title);
    int  append_page(Widget_ptr wp, const ustring & title, Align align);
    int  append_page(Widget_ptr wp, Widget_ptr title);

    void prepend_page(Widget_ptr wp);
    void prepend_page(Widget_ptr wp, const ustring & title);
    void prepend_page(Widget_ptr wp, const ustring & title, Align align);
    void prepend_page(Widget_ptr wp, Widget_ptr tp);

    int insert_page(Widget_ptr wp, int nth_page);
    int insert_page(Widget_ptr wp, int nth_page, const ustring & title);
    int insert_page(Widget_ptr wp, int nth_page, const ustring & title, Align align);
    int insert_page(Widget_ptr wp, int nth_page, Widget_ptr tp);

    int insert_page_after(Widget_ptr wp, Widget_ptr after_this);
    int insert_page_after(Widget_ptr wp, Widget_ptr after_this, const ustring & title);
    int insert_page_after(Widget_ptr wp, Widget_ptr after_this, const ustring & title, Align align);
    int insert_page_after(Widget_ptr wp, Widget_ptr after_this, Widget_ptr tp);

    int insert_page_before(Widget_ptr wp, Widget_ptr before_this);
    int insert_page_before(Widget_ptr wp, Widget_ptr before_this, const ustring & title);
    int insert_page_before(Widget_ptr wp, Widget_ptr before_this, const ustring & title, Align align);
    int insert_page_before(Widget_ptr wp, Widget_ptr before_this, Widget_ptr tp);

    int  remove_page(Widget_cptr cwp);
    int  remove_page(int page);
    void clear_pages();
    int  page_number(Widget_cptr cwp) const;
    Widget_ptr widget_at(int page);
    Widget_cptr widget_at(int page) const;
    Widget_ptr tab_at(int page);
    Widget_cptr tab_at(int page) const;

    void append_widget(Widget_ptr wp, bool shrink=false);
    void prepend_widget(Widget_ptr wp, bool shrink=false);
    void append_tab(Widget_ptr wp, bool shrink=false);
    void prepend_tab(Widget_ptr wp, bool shrink=false);
    void remove(Widget_impl * wp);

    int  show_next();
    int  show_previous();
    int  show_page(int nth_page, bool take_focus=false);
    int  show_page(Widget_cptr cwp, bool take_focus=false);

    void reorder_page(Widget_ptr wp, int nth_page);
    void reorder_page(int old_page, int new_page);

    void allow_reorder();
    void disallow_reorder();
    bool reorder_allowed() const noexcept { return reorder_allowed_; }
    void allow_rollover() { rollover_allowed_ = true; }
    void disallow_rollover() { rollover_allowed_ = false; }
    bool rollover_allowed() const noexcept { return rollover_allowed_; }

    void show_tabs();
    void hide_tabs();
    bool tabs_visible() const noexcept { return tabs_visible_; }
    int current_page() const;
    std::size_t page_count() const noexcept { return pages_.size(); }
    bool empty() const noexcept { return pages_.empty(); }
    void set_homogeneous_tabs();
    void unset_homogeneous_tabs();
    bool homogeneous_tabs() const noexcept { return homogeneous_tabs_; }

    void set_border(unsigned px, int radius=0) { frame_->set_border(px); frame_->set_border_radius(radius); }
    void set_border(unsigned px, Border bs, int radius=0) { frame_->set_border(px, bs); frame_->set_border_radius(radius); }
    void set_border(unsigned px, Border bs, const Color & color, int radius=0) { frame_->set_border(px, bs, color); frame_->set_border_radius(radius); }
    void set_border_style(Border bs) { frame_->set_border_style(bs); }
    Border border_style() const noexcept { return frame_->border_left_style(); }
    void set_border_color(const Color & color) { frame_->set_border_color(color); }
    void unset_border_color() { frame_->unset_border_color(); }
    Color border_color() const noexcept { return frame_->border_left_color(); }
    void set_border_radius(int radius) { frame_->set_border_radius(radius); }
    int border_radius() const noexcept { return frame_->border_top_left_radius(); }

    signal<void(int)> & signal_page_added() { return signal_page_added_; }
    signal<void(int)> & signal_page_removed() { return signal_page_removed_; }
    signal<void(int)> & signal_page_changed() { return signal_page_changed_; }
    signal<void(int, int)> & signal_page_reordered() { return signal_page_reordered_; }

    Container_impl * compound() noexcept override { return card_; };
    const Container_impl * compound() const noexcept override { return card_; };

private:

    struct Page {
        Widget_ptr          wp_;
        Widget_ptr          title_;
        Frame_ptr           frame_;

        connection          size1_cx_;
        connection          size2_cx_;
        connection          requisition_cx_;
        connection          hints_cx_;
        connection          mouse_down_cx_;
        connection          mouse_up_cx_;
        connection          mouse_motion_cx_;

        connection          show_cx_;
        connection          hide_cx_;

        void select() {
            if (wp_ && !wp_->selected()) {
                wp_->signal_select()();
            }
        }

        void unselect() {
            if (wp_ && wp_->selected()) {
                wp_->signal_unselect()();
            }
        }
    };

    Roller_impl *           roller_;
    Box_impl *              tabbox_;
    Absolute_impl *         abs_;
    Box_impl *              mainbox_;
    Frame_impl *            frame_;
    Card_impl *             card_;

    std::vector<Page>       pages_;
    std::size_t             spc_                = 2;        // Tab spacing.
    Widget_ptr              drag_;                          // Currently dragging frame.
    int                     last_               = -1;
    bool                    reorder_allowed_    = true;
    bool                    rollover_allowed_   = true;
    bool                    in_arrange_tabs_    = false;
    bool                    tabs_visible_       = true;
    bool                    homogeneous_tabs_   = false;
    Timeval                 drag_tv_;

    signal<void(int)>       signal_page_added_;
    signal<void(int)>       signal_page_removed_;
    signal<void(int)>       signal_page_changed_;
    signal<void(int, int)>  signal_page_reordered_;
    connection              undrag_cx_;

private:

    int  find_current();
    Color sel_color();
    void init_page(unsigned nth_page, Widget_ptr wp, Widget_ptr tp);
    void update_tabs(Hints op=Hints::SIZE);
    void update_current();
    void update_frame_border();
    Size child_requisition(Widget_impl * wp);
    void undrag();

    void on_widget_show(Widget_impl * wp);
    void on_widget_hide(Widget_impl * wp);
    bool on_tab_mouse_down(int mbt, int mm, const Point & pt, Widget_impl * wi);
    bool on_tab_mouse_up(int mbt, int mm, const Point & pt, Widget_impl * wi);
    void on_tab_mouse_motion(int mm, const Point & pt, Widget_impl * wi);
    bool on_mouse_wheel(int d, int mm, const Point & pt);
    void on_frame_background_changed();
    void on_abs_size();
};

} // namespace tau

#endif // __TAU_NOTEBOOK_IMPL_HH__

// ----------------------------------------------------------------------------
// SPDX-License-Identifier: BSD-2-Clause
// Copyright © 2014-2024 Konstantin Shmelkov <mrcashe@gmail.com>.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
// THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
// PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
// CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
// EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
// PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
// OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
// WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
// OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
// EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// ----------------------------------------------------------------------------

#include <tau/icon.hh>
#include <button-impl.hh>
#include <menu-impl.hh>
#include <menu-item-impl.hh>
#include <iostream>

namespace tau {

Action_menu_impl::Action_menu_impl(Action & action, Align accel_align):
    Menu_item_impl(action.label()),
    Menu_image(action.icon_name()),
    Menu_accel(accel_align)
{
    if (!action.enabled()) { freeze(); }
    if (!action.visible()) { disappear(); }
    enable_cx_ = action.signal_enable().connect(fun(this, &Action_menu_impl::thaw));
    disable_cx_ = action.signal_disable().connect(fun(this, &Action_menu_impl::freeze));
    show_cx_ = action.signal_show().connect(fun(this, &Action_menu_impl::appear));
    hide_cx_ = action.signal_hide().connect(fun(this, &Action_menu_impl::disappear));
    action.signal_label_changed().connect(fun(this, static_cast<void(Action_menu_impl::*)(const ustring &)>(&Action_menu_impl::assign)));
    action.signal_accel_added().connect(bind_back(fun(this, &Action_menu_impl::on_accel_added), std::cref(action)));
    action.signal_accel_changed().connect(bind_back(fun(this, &Action_menu_impl::on_accel_changed), std::cref(action)));
    action.signal_accel_removed().connect(bind_back(fun(this, &Action_menu_impl::on_accel_removed), std::cref(action)));
    action.signal_icon_changed().connect(bind_back(fun(this, static_cast<void(Action_menu_impl::*)(const ustring &, int)>(&Action_menu_impl::assign_icon)), Icon::SMALL));
    update_accels(action);
    buffer().signal_changed().connect(fun(this, &Action_menu_impl::on_label_changed));
    exec_cx_ = signal_activate().connect(fun(action, &Action::exec));
}

Action_menu_impl::Action_menu_impl(Master_action & action, Align accel_align):
    Menu_item_impl(action.label()),
    Menu_image(action.icon_name()),
    Menu_accel(accel_align)
{
    freeze();
    if (!action.visible()) { disappear(); }
    action.signal_enable().connect(fun(this, &Action_menu_impl::thaw));
    action.signal_disable().connect(fun(this, &Action_menu_impl::freeze));
    action.signal_hide().connect(fun(this, &Action_menu_impl::disappear));
    action.signal_show().connect(fun(this, &Action_menu_impl::appear));
    action.signal_label_changed().connect(fun(this, static_cast<void(Action_menu_impl::*)(const ustring &)>(&Action_menu_impl::assign)));
    action.signal_accel_added().connect(bind_back(fun(this, &Action_menu_impl::on_master_accel_added), std::cref(action)));
    action.signal_accel_removed().connect(bind_back(fun(this, &Action_menu_impl::on_master_accel_removed), std::cref(action)));
    action.signal_icon_changed().connect(bind_back(fun(this, static_cast<void(Action_menu_impl::*)(const ustring &, int)>(&Action_menu_impl::assign_icon)), Icon::SMALL));
    update_master_accels(action);
    buffer().signal_changed().connect(fun(this, &Action_menu_impl::on_label_changed));
}

Action_menu_impl::Action_menu_impl(const ustring & label, std::string_view icon_name, Align accel_align):
    Menu_item_impl(label),
    Menu_image(icon_name),
    Menu_accel(accel_align)
{
    thaw();
    buffer().signal_changed().connect(fun(this, &Action_menu_impl::on_label_changed));
}

void Action_menu_impl::on_label_changed() {
    if (buffer().empty()) { disappear(); }
    else { appear(); }
}

void Action_menu_impl::update_accels(const Action & action) {
    auto accels = action.accels();
    if (!accels.empty()) { accel_label_->show(), accel_label_->assign(accels.front()->label()); }
    else { accel_label_->clear(), accel_label_->hide(); }
}

void Action_menu_impl::update_master_accels(const Master_action & action) {
    auto accels = action.accels();
    if (!accels.empty()) { accel_label_->show(), accel_label_->assign(accels.front()->label()); }
    else { accel_label_->clear(), accel_label_->hide(); }
}

void Action_menu_impl::on_accel_changed(const Accel &, const Accel &, const Action & action) {
    update_accels(action);
}

void Action_menu_impl::on_accel_added(const Accel & accel, const Action & action) {
    update_accels(action);
}

void Action_menu_impl::on_master_accel_added(const Accel & accel, const Master_action & action) {
    update_master_accels(action);
}

void Action_menu_impl::on_accel_removed(const Accel & accel, const Action & action) {
    update_accels(action);
}

void Action_menu_impl::on_master_accel_removed(const Accel & accel, const Master_action & action) {
    update_master_accels(action);
}

void Action_menu_impl::select(Action & action) {
    action.signal_select().connect(bind_back(fun(this, &Action_menu_impl::on_select), std::ref(action)));
    action.signal_unselect().connect(bind_back(fun(this, &Action_menu_impl::on_unselect), std::ref(action)));
}

void Action_menu_impl::on_select(Action & action) {
    if (action.enabled()) thaw(); else freeze();
    if (!action.visible()) { disappear(); } else { appear(); }
    enable_cx_ = action.signal_enable().connect(fun(this, &Action_menu_impl::thaw));
    disable_cx_ = action.signal_disable().connect(fun(this, &Action_menu_impl::freeze));
    show_cx_ = action.signal_show().connect(fun(this, &Action_menu_impl::appear));
    hide_cx_ = action.signal_hide().connect(fun(this, &Action_menu_impl::disappear));
    exec_cx_ = signal_activate().connect(fun(action, &Action::exec));
}

void Action_menu_impl::on_unselect(Action & action) {
    enable_cx_.drop(), disable_cx_.drop(), show_cx_.drop(), hide_cx_.drop(), exec_cx_.drop();
    freeze();
}

// ----------------------------------------------------------------------------
// ----------------------------------------------------------------------------

Toggle_menu_impl::Toggle_menu_impl(const ustring & label, std::string_view icon_name):
    Menu_item_impl(label)
{
    init_button(icon_name);
    init();
    freeze();
}

Toggle_menu_impl::Toggle_menu_impl(Toggle_action & toggle_action):
    Menu_item_impl(toggle_action.label())
{
    init_button(toggle_action.icon_name());
    init();
    init(toggle_action);
}

Toggle_menu_impl::Toggle_menu_impl(Master_action & master_action):
    Menu_item_impl(master_action.label())
{
    init_button(master_action.icon_name());
    init();
    init(master_action);
}

Toggle_menu_impl::Toggle_menu_impl(const ustring & label, std::string_view icon_name, Align accel_align):
    Menu_item_impl(label),
    Menu_accel(accel_align)
{
    init_button(icon_name);
    init();
    freeze();
}

Toggle_menu_impl::Toggle_menu_impl(Toggle_action & toggle_action, Align accel_align):
    Menu_item_impl(toggle_action.label()),
    Menu_accel(accel_align)
{
    init_button(toggle_action.icon_name());
    init();
    init(toggle_action);
}

Toggle_menu_impl::Toggle_menu_impl(Master_action & master_action, Align accel_align):
    Menu_item_impl(master_action.label()),
    Menu_accel(accel_align)
{
    init_button(master_action.icon_name());
    init();
    init(master_action);
}

Toggle_menu_impl::Toggle_menu_impl(const ustring & label, Check::Style check_style, Border border_style):
    Menu_item_impl(label)
{
    img_ = check_ = std::make_shared<Check_impl>(check_style, border_style);
    init();
    freeze();
}

Toggle_menu_impl::Toggle_menu_impl(Toggle_action & toggle_action, Check::Style check_style, Border border_style):
    Menu_item_impl(toggle_action.label())
{
    img_ = check_ = std::make_shared<Check_impl>(check_style, border_style);
    init();
    init(toggle_action);
}

Toggle_menu_impl::Toggle_menu_impl(Master_action & master_action, Check::Style check_style, Border border_style):
    Menu_item_impl(master_action.label())
{
    img_ = check_ = std::make_shared<Check_impl>(check_style, border_style);
    init();
    init(master_action);
}

Toggle_menu_impl::Toggle_menu_impl(const ustring & label, Align accel_align, Check::Style check_style, Border border_style):
    Menu_item_impl(label),
    Menu_accel(accel_align)
{
    img_ = check_ = std::make_shared<Check_impl>(check_style, border_style);
    init();
}

Toggle_menu_impl::Toggle_menu_impl(Toggle_action & toggle_action, Align accel_align, Check::Style check_style, Border border_style):
    Menu_item_impl(toggle_action.label()),
    Menu_accel(accel_align)
{
    img_ = check_ = std::make_shared<Check_impl>(check_style, border_style);
    init();
    init(toggle_action);
}

Toggle_menu_impl::Toggle_menu_impl(Master_action & master_action, Align accel_align, Check::Style check_style, Border border_style):
    Menu_item_impl(master_action.label()),
    Menu_accel(accel_align)
{
    img_ = check_ = std::make_shared<Check_impl>(check_style, border_style);
    init();
    init(master_action);
}

void Toggle_menu_impl::init_button(std::string_view icon_name) {
    img_ = toggle_ = std::make_shared<Toggle_impl>(Icon::DEFAULT, icon_name);
    toggle_->hide_relief();
    toggle_->connect(fun(this, &Toggle_menu_impl::on_button_toggle));
}

void Toggle_menu_impl::init() {
    conf().boolean(Conf::BUTTON_LABEL) = false;
    buffer().signal_changed().connect(fun(this, &Toggle_menu_impl::on_label_changed));
}

void Toggle_menu_impl::init(Toggle_action & action) {
    if (!action.enabled()) { freeze(); }
    if (!action.visible()) { disappear(); }
    action.signal_enable().connect(fun(this, &Toggle_menu_impl::thaw));
    action.signal_disable().connect(fun(this, &Toggle_menu_impl::freeze));
    action.signal_hide().connect(fun(this, &Toggle_menu_impl::disappear));
    action.signal_show().connect(fun(this, &Toggle_menu_impl::appear));
    action.signal_label_changed().connect(fun(this, static_cast<void(Toggle_menu_impl::*)(const ustring &)>(&Toggle_menu_impl::assign)));
    action.signal_accel_added().connect(bind_back(fun(this, &Toggle_menu_impl::on_accel_added), std::cref(action)));
    action.signal_accel_changed().connect(bind_back(fun(this, &Toggle_menu_impl::on_accel_changed), std::cref(action)));
    action.signal_accel_removed().connect(bind_back(fun(this, &Toggle_menu_impl::on_accel_removed), std::cref(action)));
    action.connect(fun(this, &Toggle_menu_impl::on_action_toggle));

    if (check_) {
        check_->setup(action.get());
        check_cx_ = check_->signal_check().connect(bind_back(fun(action, &Toggle_action::set), true));
        uncheck_cx_ = check_->signal_uncheck().connect(bind_back(fun(action, &Toggle_action::set), false));
        action.connect(fun(check_, &Check_impl::setup));
        action.signal_setup().connect(fun(check_, &Check_impl::setup));
    }

    else if (toggle_) {
        toggle_->setup(action.get());
        action.signal_setup().connect(fun(toggle_, &Toggle_impl::setup));
    }

    update_accels(action);
    signal_activate().connect(fun(action, &Toggle_action::toggle));
}

void Toggle_menu_impl::init(Master_action & action) {
    freeze();
    if (!action.visible()) { disappear(); }
    action.signal_enable().connect(fun(this, &Toggle_menu_impl::thaw));
    action.signal_disable().connect(fun(this, &Toggle_menu_impl::freeze));
    action.signal_hide().connect(fun(this, &Toggle_menu_impl::disappear));
    action.signal_show().connect(fun(this, &Toggle_menu_impl::appear));
    action.signal_label_changed().connect(fun(this, static_cast<void(Toggle_menu_impl::*)(const ustring &)>(&Toggle_menu_impl::assign)));
    action.signal_accel_added().connect(bind_back(fun(this, &Toggle_menu_impl::on_master_accel_added), std::cref(action)));
    action.signal_accel_removed().connect(bind_back(fun(this, &Toggle_menu_impl::on_master_accel_removed), std::cref(action)));
    if (toggle_) { action.signal_icon_changed().connect(bind_back(fun(toggle_, &Toggle_impl::set_icon), Icon::DEFAULT)); }
    update_master_accels(action);
}

void Toggle_menu_impl::set_check_style(Check::Style check_style) {
    if (check_) {
        check_->set_check_style(check_style);
    }
}

Check::Style Toggle_menu_impl::check_style() const noexcept {
    return check_ ? check_->check_style() : Check::XSTYLE;
}

void Toggle_menu_impl::set_border_style(Border border_style) {
    if (check_) {
        check_->set_border_style(border_style);
    }
}

Border Toggle_menu_impl::border_style() const noexcept {
    return check_ ? check_->border_style() : Border::NONE;
}

void Toggle_menu_impl::set_border_width(unsigned npx) {
    if (check_) {
        check_->set_border_width(npx);
    }
}

unsigned Toggle_menu_impl::border_width() const noexcept {
    return check_ ? check_->border_width() : 0;
}

void Toggle_menu_impl::update_accels(const Toggle_action & action) {
    auto accels = action.accels();
    if (!accels.empty()) { accel_label_->show(); accel_label_->assign(accels.front()->label()); }
    else { accel_label_->clear(); accel_label_->hide(); }
}

void Toggle_menu_impl::update_master_accels(const Master_action & action) {
    auto accels = action.accels();
    if (!accels.empty()) { accel_label_->show(); accel_label_->assign(accels.front()->label()); }
    else { accel_label_->clear(); accel_label_->hide(); }
}

void Toggle_menu_impl::on_label_changed() {
    if (buffer().empty()) { disappear(); }
    else { appear(); }
}

void Toggle_menu_impl::on_accel_added(const Accel &, const Toggle_action & action) {
    update_accels(action);
}

void Toggle_menu_impl::on_master_accel_added(const Accel &, const Master_action & action) {
    update_master_accels(action);
}

void Toggle_menu_impl::on_accel_changed(const Accel &, const Accel &, const Toggle_action & action) {
    update_accels(action);
}

void Toggle_menu_impl::on_accel_removed(const Accel &, const Toggle_action & action) {
    update_accels(action);
}

void Toggle_menu_impl::on_master_accel_removed(const Accel &, const Master_action & action) {
    update_master_accels(action);
}

void Toggle_menu_impl::on_action_toggle(bool state) {
    if (check_) { check_->setup(state); }
    else if (toggle_) { toggle_->setup(state); }
}

bool Toggle_menu_impl::get() const noexcept {
    if (check_) { return check_->checked(); }
    else if (toggle_) { return toggle_->get(); }
    return false;
}

void Toggle_menu_impl::select(Toggle_action & action) {
    action.signal_select().connect(bind_back(fun(this, &Toggle_menu_impl::on_select), std::ref(action)));
    action.signal_unselect().connect(bind_back(fun(this, &Toggle_menu_impl::on_unselect), std::ref(action)));
}

void Toggle_menu_impl::on_select(Toggle_action & action) {
    if (action.enabled()) thaw(); else freeze();
    if (!action.visible()) { disappear(); } else { appear(); }
    enable_cx_ = action.signal_enable().connect(fun(this, &Toggle_menu_impl::thaw));
    disable_cx_ = action.signal_disable().connect(fun(this, &Toggle_menu_impl::freeze));
    show_cx_ = action.signal_show().connect(fun(this, &Toggle_menu_impl::appear));
    hide_cx_ = action.signal_hide().connect(fun(this, &Toggle_menu_impl::disappear));
    toggle_cx_ = signal_activate().connect(fun(action, &Toggle_action::toggle));

    if (toggle_) {
        setup_cx_ = action.signal_setup().connect(fun(toggle_, &Toggle_impl::setup));
        toggle_->setup(action.get());
    }

    else if (check_) {
        check_->setup(action.get());
        setup_cx_ = action.signal_setup().connect(fun(check_, &Check_impl::setup));
        check_cx_ = check_->signal_check().connect(bind_back(fun(action, &Toggle_action::set), true));
        uncheck_cx_ = check_->signal_uncheck().connect(bind_back(fun(action, &Toggle_action::set), false));
    }
}

void Toggle_menu_impl::on_unselect(Toggle_action & action) {
    enable_cx_.drop(), disable_cx_.drop(), show_cx_.drop(), hide_cx_.drop(), toggle_cx_.drop(), setup_cx_.drop();
    check_cx_.drop(), uncheck_cx_.drop();
    freeze();
}

// ----------------------------------------------------------------------------
// ----------------------------------------------------------------------------

void Submenu_impl::set_menu(Menu_ptr mp) {
    menu_ = mp;
    if (mp->disabled() || !mp->has_enabled_items()) { freeze(); }
    mp->signal_enable().connect(fun(this, &Submenu_impl::thaw));
    mp->signal_disable().connect(fun(this, &Submenu_impl::freeze));
}

void Submenu_impl::init() {
    arrow_ = std::make_shared<Icon_impl>("go-next", Icon::TINY);
    arrow_->par_enable(!disabled());
    signal_enable().connect(fun(arrow_, &Widget_impl::enable));
    signal_disable().connect(fun(arrow_, &Widget_impl::disable));
    signal_display_in_.connect(fun(this, &Submenu_impl::on_display_in));
}

void Submenu_impl::on_display_in() {
    if (!menu_ || menu_->disabled() || !menu_->has_enabled_items()) { freeze(); }
    else { thaw(); }
}

// ----------------------------------------------------------------------------
// ----------------------------------------------------------------------------

Check_menu_impl::Check_menu_impl(const ustring & label, bool checked):
    Menu_item_impl(label),
    check_(std::make_shared<Check_impl>(checked))
{
    init();
}

Check_menu_impl::Check_menu_impl(const ustring & label, Check::Style check_style, bool checked):
    Menu_item_impl(label),
    check_(std::make_shared<Check_impl>(check_style, checked))
{
    init();
}

Check_menu_impl::Check_menu_impl(const ustring & label, Border border_style, bool checked):
    Menu_item_impl(label),
    check_(std::make_shared<Check_impl>(border_style, checked))
{
    init();
}

Check_menu_impl::Check_menu_impl(const ustring & label, Check::Style check_style, Border border_style, bool checked):
    Menu_item_impl(label),
    check_(std::make_shared<Check_impl>(check_style, border_style, checked))
{
    init();
}

void Check_menu_impl::init() {
    check_->signal_check().connect(fun(signal_activate()));
    check_->signal_uncheck().connect(fun(signal_activate()));
    signal_enable_.connect(fun(this, &Check_menu_impl::on_enable));
    signal_disable_.connect(fun(this, &Check_menu_impl::on_disable));
    check_->signal_enable().connect(fun(this, &Check_menu_impl::on_check_enable));
    check_->signal_disable().connect(fun(this, &Check_menu_impl::on_check_disable));
    signal_display_in_.connect(fun(this, &Check_menu_impl::on_display_in));
}

void Check_menu_impl::join(Check_menu_ptr other) {
    check_->join(other->check_ptr());
}

void Check_menu_impl::on_display_in() {
    check_->par_enable(!disabled());
}

void Check_menu_impl::on_enable() {
    if (!disabled()) {
        check_->enable();
    }
}

void Check_menu_impl::on_disable() {
    if (disabled() && !frozen()) {
        check_->disable();
    }
}

void Check_menu_impl::on_check_enable() {
    if (!check_->disabled()) {
        thaw();
    }
}

void Check_menu_impl::on_check_disable() {
    if (check_->frozen()) {
        freeze();
    }
}

} // namespace tau

//END

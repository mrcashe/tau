// ----------------------------------------------------------------------------
// SPDX-License-Identifier: BSD-2-Clause
// Copyright © 2014-2024 Konstantin Shmelkov <mrcashe@gmail.com>.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
// THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
// PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
// CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
// EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
// PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
// OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
// WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
// OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
// EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// ----------------------------------------------------------------------------

/// @file frame.cc The Frame (Frame_impl wrapper) class implementation.
/// The header is frame.hh.

#include <tau/frame.hh>
#include <tau/painter.hh>
#include <frame-impl.hh>

namespace tau {

#define FRAME_IMPL (std::static_pointer_cast<Frame_impl>(impl))

Frame::Frame():
    Container(std::make_shared<Frame_impl>())
{
}

Frame::Frame(const Frame & other):
    Container(other.impl)
{
}

Frame & Frame::operator=(const Frame & other) {
    Container::operator=(other);
    return *this;
}

Frame::Frame(Frame && other):
    Container(other.impl)
{
}

Frame & Frame::operator=(Frame && other) {
    Container::operator=(other);
    return *this;
}

Frame::Frame(Widget_ptr wp):
    Container(std::dynamic_pointer_cast<Frame_impl>(wp))
{
}

Frame & Frame::operator=(Widget_ptr wp) {
    Container::operator=(std::dynamic_pointer_cast<Frame_impl>(wp));
    return *this;
}

Frame::Frame(const ustring & label, Align align):
    Container(std::static_pointer_cast<Widget_impl>(std::make_shared<Frame_impl>(label, align)))
{
}

Frame::Frame(Border bs, unsigned border_width, unsigned border_radius):
    Container(std::static_pointer_cast<Widget_impl>(std::make_shared<Frame_impl>(bs, border_width, border_radius)))
{
}

Frame::Frame(Border bs, unsigned border_width, unsigned rtop_left, unsigned rtop_right, unsigned rbottom_right, unsigned rbottom_left):
    Container(std::static_pointer_cast<Widget_impl>(std::make_shared<Frame_impl>(bs, border_width, rtop_left, rtop_right, rbottom_right, rbottom_left)))
{
}

Frame::Frame(Border bs, const Color & border_color, unsigned border_width, unsigned border_radius):
    Container(std::static_pointer_cast<Widget_impl>(std::make_shared<Frame_impl>(bs, border_color, border_width, border_radius)))
{
}

Frame::Frame(Border bs, const Color & border_color, unsigned border_width, unsigned rtop_left, unsigned rtop_right, unsigned rbottom_right, unsigned rbottom_left):
    Container(std::static_pointer_cast<Widget_impl>(std::make_shared<Frame_impl>(bs, border_color, border_width, rtop_left, rtop_right, rbottom_right, rbottom_left)))
{
}

Frame::Frame(const ustring & label, Border bs, unsigned border_width, unsigned border_radius):
    Container(std::static_pointer_cast<Widget_impl>(std::make_shared<Frame_impl>(label, bs, border_width, border_radius)))
{
}

Frame::Frame(const ustring & label, Border bs, unsigned border_width, unsigned rtop_left, unsigned rtop_right, unsigned rbottom_right, unsigned rbottom_left):
    Container(std::static_pointer_cast<Widget_impl>(std::make_shared<Frame_impl>(label, bs, border_width, rtop_left, rtop_right, rbottom_right, rbottom_left)))
{
}

Frame::Frame(const ustring & label, Border bs, const Color & border_color, unsigned border_width, unsigned border_radius):
    Container(std::static_pointer_cast<Widget_impl>(std::make_shared<Frame_impl>(label, bs, border_color, border_width, border_radius)))
{
}

Frame::Frame(const ustring & label, Border bs, const Color & border_color, unsigned border_width, unsigned rtop_left, unsigned rtop_right, unsigned rbottom_right, unsigned rbottom_left):
    Container(std::static_pointer_cast<Widget_impl>(std::make_shared<Frame_impl>(label, bs, border_color, border_width, rtop_left, rtop_right, rbottom_right, rbottom_left)))
{
}

Frame::Frame(const ustring & label, Align align, Border bs, unsigned border_width, unsigned border_radius):
    Container(std::static_pointer_cast<Widget_impl>(std::make_shared<Frame_impl>(label, align, bs, border_width, border_radius)))
{
}

Frame::Frame(const ustring & label, Align align, Border bs, unsigned border_width, unsigned rtop_left, unsigned rtop_right, unsigned rbottom_right, unsigned rbottom_left):
    Container(std::static_pointer_cast<Widget_impl>(std::make_shared<Frame_impl>(label, align, bs, border_width, rtop_left, rtop_right, rbottom_right, rbottom_left)))
{
}

Frame::Frame(const ustring & label, Align align, Border bs, const Color & border_color, unsigned border_width, unsigned border_radius):
    Container(std::static_pointer_cast<Widget_impl>(std::make_shared<Frame_impl>(label, align, bs, border_color, border_width, border_radius)))
{
}

Frame::Frame(const ustring & label, Align align, Border bs, const Color & border_color, unsigned border_width, unsigned rtop_left, unsigned rtop_right, unsigned rbottom_right, unsigned rbottom_left):
    Container(std::static_pointer_cast<Widget_impl>(std::make_shared<Frame_impl>(label, align, bs, border_color, border_width, rtop_left, rtop_right, rbottom_right, rbottom_left)))
{
}

void Frame::insert(Widget & w) {
    FRAME_IMPL->insert(w.ptr());
}

void Frame::clear() {
    FRAME_IMPL->clear();
}

bool Frame::empty() const noexcept {
    return FRAME_IMPL->empty();
}

void Frame::set_label(const ustring & label, Align align) {
    FRAME_IMPL->set_label(label, align);
}

void Frame::set_label(const ustring & label, Side label_pos, Align align) {
    FRAME_IMPL->set_label(label, label_pos, align);
}

void Frame::set_label(Widget & w, Side label_pos, Align align) {
    FRAME_IMPL->set_label(w.ptr(), label_pos, align);
}

void Frame::set_label(Widget & w, Align align) {
    FRAME_IMPL->set_label(w.ptr(), align);
}

void Frame::align_label(Align align) {
    FRAME_IMPL->align_label(align);
}

Align Frame::label_align() const noexcept {
    return FRAME_IMPL->label_align();
}

void Frame::move_label(Side label_pos) {
    FRAME_IMPL->move_label(label_pos);
}

Side Frame::where_label() const noexcept {
    return FRAME_IMPL->where_label();
}

Widget_ptr Frame::widget() noexcept {
    return FRAME_IMPL->widget();
}

Widget_cptr Frame::widget() const noexcept {
    return FRAME_IMPL->widget();
}

bool Frame::has_label() const noexcept {
    return FRAME_IMPL->has_label();
}

Widget_ptr Frame::label() noexcept {
    return FRAME_IMPL->label();
}

Widget_cptr Frame::label() const noexcept {
    return FRAME_IMPL->label();
}

bool Frame::border_visible() const noexcept {
    return FRAME_IMPL->border_visible();
}

unsigned Frame::border_left() const noexcept {
    return FRAME_IMPL->border_left();
}

unsigned Frame::border_right() const noexcept {
    return FRAME_IMPL->border_right();
}

unsigned Frame::border_top() const noexcept {
    return FRAME_IMPL->border_top();
}

unsigned Frame::border_bottom() const noexcept {
    return FRAME_IMPL->border_bottom();
}

void Frame::set_border(unsigned npx) {
    FRAME_IMPL->set_border(npx, npx, npx, npx);
}

void Frame::set_border(unsigned left, unsigned right, unsigned top, unsigned bottom) {
    FRAME_IMPL->set_border(left, right, top, bottom);
}

void Frame::set_border(unsigned px, Border bs) {
    FRAME_IMPL->set_border(px, bs);
}

void Frame::set_border(unsigned px, Border bs, unsigned radius) {
    FRAME_IMPL->set_border(px, bs, radius);
}

void Frame::set_border(unsigned px, Border bs, unsigned rtop_left, unsigned rtop_right, unsigned rbottom_right, unsigned rbottom_left) {
    FRAME_IMPL->set_border(px, bs, rtop_left, rtop_right, rbottom_right, rbottom_left);
}

void Frame::set_border(unsigned px, Border bs, const Color & color) {
    FRAME_IMPL->set_border(px, bs, color);
}

void Frame::set_border(unsigned px, Border bs, const Color & color, unsigned radius) {
    FRAME_IMPL->set_border(px, bs, color, radius);
}

void Frame::set_border(unsigned px, Border bs, const Color & color, unsigned rtop_left, unsigned rtop_right, unsigned rbottom_right, unsigned rbottom_left) {
    FRAME_IMPL->set_border(px, bs, color, rtop_left, rtop_right, rbottom_right, rbottom_left);
}

void Frame::set_border_left(unsigned npx) {
    FRAME_IMPL->set_border_left(npx);
}

void Frame::set_border_right(unsigned npx) {
    FRAME_IMPL->set_border_right(npx);
}

void Frame::set_border_top(unsigned npx) {
    FRAME_IMPL->set_border_top(npx);
}

void Frame::set_border_bottom(unsigned npx) {
    FRAME_IMPL->set_border_bottom(npx);
}

void Frame::set_border_left(unsigned px, Border bs) {
    FRAME_IMPL->set_border_left(px, bs);
}

void Frame::set_border_right(unsigned px, Border bs) {
    FRAME_IMPL->set_border_right(px, bs);
}

void Frame::set_border_top(unsigned px, Border bs) {
    FRAME_IMPL->set_border_top(px, bs);
}

void Frame::set_border_bottom(unsigned px, Border bs) {
    FRAME_IMPL->set_border_bottom(px, bs);
}

void Frame::set_border_left(unsigned px, Border bs, const Color & color) {
    FRAME_IMPL->set_border_left(px, bs, color);
}

void Frame::set_border_right(unsigned px, Border bs, const Color & color) {
    FRAME_IMPL->set_border_right(px, bs, color);
}

void Frame::set_border_top(unsigned px, Border bs, const Color & color) {
    FRAME_IMPL->set_border_top(px, bs, color);
}

void Frame::set_border_bottom(unsigned px, Border bs, const Color & color) {
    FRAME_IMPL->set_border_bottom(px, bs, color);
}

void Frame::set_border_left_style(Border bs) {
    FRAME_IMPL->set_border_left_style(bs);
}

void Frame::set_border_right_style(Border bs) {
    FRAME_IMPL->set_border_right_style(bs);
}

void Frame::set_border_top_style(Border bs) {
    FRAME_IMPL->set_border_top_style(bs);
}

void Frame::set_border_bottom_style(Border bs) {
    FRAME_IMPL->set_border_bottom_style(bs);
}

void Frame::set_border_style(Border bs) {
    FRAME_IMPL->set_border_style(bs);
}

void Frame::set_border_style(Border left, Border right, Border top, Border bottom) {
    FRAME_IMPL->set_border_style(left, right, top, bottom);
}

Border Frame::border_left_style() const noexcept {
    return FRAME_IMPL->border_left_style();
}

Border Frame::border_right_style() const noexcept {
    return FRAME_IMPL->border_right_style();
}

Border Frame::border_top_style() const noexcept {
    return FRAME_IMPL->border_top_style();
}

Border Frame::border_bottom_style() const noexcept {
    return FRAME_IMPL->border_bottom_style();
}

void Frame::set_border_color(const Color & left, const Color & right, const Color & top, const Color & bottom) {
    FRAME_IMPL->set_border_color(left, right, top, bottom);
}

void Frame::set_border_color(const Color & color) {
    FRAME_IMPL->set_border_color(color);
}

void Frame::set_border_left_color(const Color & color) {
    FRAME_IMPL->set_border_left_color(color);
}

void Frame::set_border_right_color(const Color & color) {
    FRAME_IMPL->set_border_right_color(color);
}

void Frame::set_border_top_color(const Color & color) {
    FRAME_IMPL->set_border_top_color(color);
}

void Frame::set_border_bottom_color(const Color & color) {
    FRAME_IMPL->set_border_bottom_color(color);
}

Color Frame::border_left_color() const noexcept {
    return FRAME_IMPL->border_left_color();
}

Color Frame::border_right_color() const noexcept {
    return FRAME_IMPL->border_right_color();
}

Color Frame::border_top_color() const noexcept {
    return FRAME_IMPL->border_top_color();
}

Color Frame::border_bottom_color() const noexcept {
    return FRAME_IMPL->border_bottom_color();
}

void Frame::unset_border_color() {
    FRAME_IMPL->unset_border_color();
}

void Frame::unset_border_left_color() {
    FRAME_IMPL->unset_border_left_color();
}

void Frame::unset_border_right_color() {
    FRAME_IMPL->unset_border_right_color();
}

void Frame::unset_border_top_color() {
    FRAME_IMPL->unset_border_top_color();
}

void Frame::unset_border_bottom_color() {
    FRAME_IMPL->unset_border_bottom_color();
}

void Frame::set_border_top_left_radius(unsigned radius) {
    FRAME_IMPL->set_border_top_left_radius(radius);
}

void Frame::set_border_top_right_radius(unsigned radius) {
    FRAME_IMPL->set_border_top_right_radius(radius);
}

void Frame::set_border_bottom_right_radius(unsigned radius) {
    FRAME_IMPL->set_border_bottom_right_radius(radius);
}

void Frame::set_border_bottom_left_radius(unsigned radius) {
    FRAME_IMPL->set_border_bottom_left_radius(radius);
}

void Frame::set_border_radius(unsigned radius) {
    FRAME_IMPL->set_border_radius(radius);
}

void Frame::set_border_radius(unsigned top_left, unsigned top_right, unsigned bottom_right, unsigned bottom_left) {
    FRAME_IMPL->set_border_radius(top_left, top_right, bottom_right, bottom_left);
}

unsigned Frame::border_top_left_radius() const noexcept {
    return FRAME_IMPL->border_top_left_radius();
}

unsigned Frame::border_top_right_radius() const noexcept {
    return FRAME_IMPL->border_top_right_radius();
}

unsigned Frame::border_bottom_left_radius() const noexcept {
    return FRAME_IMPL->border_bottom_left_radius();
}

unsigned Frame::border_bottom_right_radius() const noexcept {
    return FRAME_IMPL->border_bottom_right_radius();
}

signal<void()> & Frame::signal_border_changed() {
    return FRAME_IMPL->signal_border_changed();
}

} // namespace tau

//END

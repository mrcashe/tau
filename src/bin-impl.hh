// ----------------------------------------------------------------------------
// SPDX-License-Identifier: BSD-2-Clause
// Copyright © 2014-2024 Konstantin Shmelkov <mrcashe@gmail.com>.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
// THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
// PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
// CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
// EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
// PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
// OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
// WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
// OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
// EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// ----------------------------------------------------------------------------

#ifndef __TAU_BIN_IMPL_HH__
#define __TAU_BIN_IMPL_HH__

#include <flat-container.hh>

namespace tau {

class Bin_impl: public Flat_container {
public:

    Bin_impl();
   ~Bin_impl() { destroy(); }

    void insert(Widget_ptr wp);

    // FIXME Redefined by Roller_impl.
    void clear();

    bool empty() const noexcept { return nullptr == cp_; }
    Container_impl * compound() noexcept override { return this; };
    const Container_impl * compound() const noexcept override { return this; };
    Widget_ptr widget() { return chptr(cp_); }
    Widget_cptr widget() const { return chptr(cp_); }

private:

    Widget_impl *   cp_     = nullptr;
    connection      cx_     { true };

private:

    void update_requisition();
    void arrange();
    void on_hints(Hints op);
    bool on_take_focus();
};

} // namespace tau

#endif // __TAU_BIN_IMPL_HH__

// ----------------------------------------------------------------------------
// SPDX-License-Identifier: BSD-2-Clause
// Copyright © 2014-2024 Konstantin Shmelkov <mrcashe@gmail.com>.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
// THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
// PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
// CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
// EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
// PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
// OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
// WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
// OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
// EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// ----------------------------------------------------------------------------

#include <tau/event.hh>
#include <tau/exception.hh>
#include <event-impl.hh>
#include <loop-impl.hh>

namespace {

void throw_if(tau::Event_ptr p, const char * s) { if (!p) throw tau::user_error(tau::str_format("tau::Event::", s, " called on pure tau::Loop")); }

} // anonymous namespace

namespace tau {

Event::Event() {}

Event::Event(slot<void()> slot_ready) {
    auto loop = Loop_impl::this_ptr();
    impl = loop->event();
    impl->signal_ready().connect(slot_ready);
}

Event::Event(Event_ptr evp):
    impl(evp)
{
}

Event_ptr Event::ptr() {
    return impl;
}

Event_cptr Event::ptr() const {
    return impl;
}

Event::operator bool() const noexcept {
    return nullptr != impl;
}

void Event::emit() {
    throw_if(impl, __func__);
    if (impl) { impl->emit(); }
}

signal<void()> & Event::signal_ready() {
    throw_if(impl, __func__);
    return impl->signal_ready();
}

} // namespace tau

//END

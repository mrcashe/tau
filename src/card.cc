// ----------------------------------------------------------------------------
// SPDX-License-Identifier: BSD-2-Clause
// Copyright © 2014-2024 Konstantin Shmelkov <mrcashe@gmail.com>.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
// THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
// PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
// CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
// EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
// PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
// OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
// WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
// OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
// EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// ----------------------------------------------------------------------------

#include <tau/card.hh>
#include <card-impl.hh>

#define CARD_IMPL (std::static_pointer_cast<Card_impl>(impl))

namespace tau {

Card::Card():
    Container(std::static_pointer_cast<Widget_impl>(std::make_shared<Card_impl>()))
{
}

Card::Card(const Card & other):
    Container(other.impl)
{
}

Card & Card::operator=(const Card & other) {
    Container::operator=(other);
    return *this;
}

Card::Card(Card && other):
    Container(other.impl)
{
}

Card & Card::operator=(Card && other) {
    Container::operator=(other);
    return *this;
}

Card::Card(Widget_ptr wp):
    Container(std::static_pointer_cast<Widget_impl>(std::dynamic_pointer_cast<Card_impl>(wp)))
{
}

Card & Card::operator=(Widget_ptr wp) {
    Container::operator=(std::dynamic_pointer_cast<Card_impl>(wp));
    impl = wp;
    return *this;
}

void Card::insert(Widget & w, bool take_focus) {
    CARD_IMPL->insert(w.ptr(), take_focus);
}

std::size_t Card::remove_current() {
    return CARD_IMPL->remove_current();
}

std::size_t Card::remove_others() {
    return CARD_IMPL->remove_others();
}

std::size_t Card::remove(Widget & w) {
    return CARD_IMPL->remove(w.ptr().get());
}

void Card::clear() {
    CARD_IMPL->clear();
}

void Card::show_next() {
    CARD_IMPL->show_next();
}

void Card::show_previous() {
    CARD_IMPL->show_previous();
}

std::size_t Card::count() const noexcept {
    return CARD_IMPL->count();
}

Widget_ptr Card::current() {
    return CARD_IMPL->chptr(CARD_IMPL->current());
}

Widget_cptr Card::current() const {
    return CARD_IMPL->chptr(CARD_IMPL->current());
}

bool Card::empty() const noexcept {
    return CARD_IMPL->empty();
}

} // namespace tau

//END

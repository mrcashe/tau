// ----------------------------------------------------------------------------
// SPDX-License-Identifier: BSD-2-Clause
// Copyright © 2014-2024 Konstantin Shmelkov <mrcashe@gmail.com>.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
// THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
// PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
// CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
// EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
// PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
// OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
// WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
// OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
// EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// ----------------------------------------------------------------------------

#include <tau/dialog.hh>
#include <dialog-impl.hh>
#include <display-impl.hh>

namespace tau {

#define DIALOG_IMPL (std::static_pointer_cast<Dialog_impl>(impl))

Dialog::Dialog(Widget & w, const Rect & bounds):
    Toplevel(Display_impl::this_ptr()->create_dialog(w.toplevel().get(), bounds))
{
}

Dialog::Dialog(Widget & w, const ustring & title, const Rect & bounds):
    Toplevel(Display_impl::this_ptr()->create_dialog(w.toplevel().get(), bounds))
{
    DIALOG_IMPL->set_title(title);
}

Dialog::Dialog(Toplevel & wnd, const Rect & bounds):
    Toplevel(Display_impl::this_ptr()->create_dialog(wnd.ptr().get(), bounds))
{
}

Dialog::Dialog(Toplevel & wnd, const ustring & title, const Rect & bounds):
    Toplevel(Display_impl::this_ptr()->create_dialog(wnd.ptr().get(), bounds))
{
    DIALOG_IMPL->set_title(title);
}

Dialog::Dialog(const Dialog & other):
    Toplevel(other.impl)
{
}

Dialog & Dialog::operator=(const Dialog & other) {
    Toplevel::operator=(other);
    return *this;
}

Dialog::Dialog(Dialog && other):
    Toplevel(other.impl)
{
}

Dialog & Dialog::operator=(Dialog && other) {
    Toplevel::operator=(other);
    return *this;
}

Dialog::Dialog(Widget_ptr wp):
    Toplevel(std::dynamic_pointer_cast<Dialog_impl>(wp))
{
}

Dialog & Dialog::operator=(Widget_ptr wp) {
    Toplevel::operator=(std::dynamic_pointer_cast<Dialog_impl>(wp));
    return *this;
}

void Dialog::run() {
    DIALOG_IMPL->run();
}

void Dialog::quit() {
    DIALOG_IMPL->quit();
}

bool Dialog::running() const {
    return DIALOG_IMPL->running();
}

} // namespace tau

//END

// ----------------------------------------------------------------------------
// SPDX-License-Identifier: BSD-2-Clause
// Copyright © 2014-2024 Konstantin Shmelkov <mrcashe@gmail.com>.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
// THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
// PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
// CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
// EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
// PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
// OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
// WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
// OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
// EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// ----------------------------------------------------------------------------

#ifndef __TAU_NAVIGATOR_IMPL_HH__
#define __TAU_NAVIGATOR_IMPL_HH__

#include <bin-impl.hh>
#include <gettext-impl.hh>
#include <tau/file.hh>
#include <set>

namespace tau {

class Navigator_impl: public Bin_impl {
public:

    Navigator_impl(const ustring & uri=ustring());

    ustring uri() const { return uri_; }
    void set_uri(const ustring & uri);
    void refresh();

    void sort_by(const ustring & col);
    ustring sorted_by() const { return sort_; }

    void set_option(std::string_view opt);
    void reset_option(std::string_view opt);
    bool has_option(std::string_view opt) const;
    std::string options(char32_t sep=U':') const;

    void set_filter(const ustring & patterns);
    ustring filter() const;

    signal<void(const ustring &)> & signal_file_select() { return signal_file_select_; }
    signal<void(const ustring &)> & signal_file_unselect() { return signal_file_unselect_; }
    signal<void(const ustring &)> & signal_file_activate() { return signal_file_activate_; }
    signal<void(const ustring &)> & signal_dir_changed() { return signal_dir_changed_; }

    // TODO 0.8 Public it at facade!
    Action & action_cancel() noexcept { return cancel_ax_; }

    // TODO 0.8 Public it at facade!
    Action & action_zoom_in() noexcept { return zoom_in_ax_; }

    // TODO 0.8 Public it at facade!
    Action & action_zoom_out() noexcept { return zoom_out_ax_; }

private:

    // The record holding single file information.
    struct Rec {
        ustring                 name_;          // File name (without path component).
        std::string             mime_;          // MIME type (not for dirs).
        ustring                 type_;          // Localized file type.
        File                    file_;
        Timeval                 mtime_;         // Modify time.
        uintmax_t               size_           = 0;    // Bytes for file, #objects for dir.
        Pixmap_cptr             icon_;
        ustring                 tooltip_;       // Tooltip for icon.
        bool                    hidden_: 1      = false;
        bool                    dir_: 1         = false;
        bool                    filtered_: 1    = false;
        bool                    link_: 1        = false;
        int                     y_              = INT_MIN;
    };

    using Records   = std::vector<Rec>;
    using Files     = std::vector<ustring>;

    // The structure holding directory entry information.
    struct Holder {
        Files                   files_;         // **tmp
        ustring                 path_;          // Dir path.
        ustring                 name_;          // File name.
        Records                 dirs_;          // Dir records.
        Records                 recs_;          // File records.
        File                    file_;
        unsigned                mask_           { File::NOTHING };  // File monitor event mask.
        connection              wcx_            { true };           // Watch connection.
        connection              tcx_            { true };           // Watch timer connection.
    };

    using Holders   = std::list<Holder>;
    using Filer     = Files::iterator;
    using Recorder  = Records::iterator;

    Theme_ptr                   theme_;
    Header_ptr                  hdr_;
    Box_ptr                     box_;
    Box_ptr                     ostatus_;       // Outer status box.
    Box_ptr                     status_;        // Inner status box.
    Counter_ptr                 file_counter_;
    Counter_ptr                 dir_counter_;
    Counter_ptr                 sel_counter_;
    Progress_ptr                progress_;
    Label_ptr                   all_value_;
    Label_ptr                   dir_label_;
    Label_ptr                   file_label_;
    Label_ptr                   sel_label_;
    List_ptr                    list_;          // File panel as list.
    Label_ptr                   empty_;
    Spinner_ptr                 spinner_;
    Holders                     holders_;
    Filer                       fi_;            // File iterator.
    Recorder                    ri_;            // Recs iterator.
    ustring                     uri_;
    std::vector<ustring>        filters_;
    std::string                 sort_           { "name" };
    std::set<std::string>       options_;
    Timeval                     t1_;
    connection                  hide_cx_        { true };   // Tick hide timer.
    connection                  tick_cx_        { true };
    connection                  run_cx_         { true };
    Action                      cancel_ax_      { "Escape Cancel", "", "dialog-cancel", lgettext("Cancel"), fun(this, &Navigator_impl::end_show) };
    Action                      zoom_in_ax_     { U'=', KM_CONTROL, lgettext("Zoom In"), "zoom-in", lgettext("Increase icon size"), fun(this, &Navigator_impl::zoom_in) }; // FIXME Ctrl+= -> Ctrl++?
    Action                      zoom_out_ax_    { U'-', KM_CONTROL, lgettext("Zoom Out"), "zoom-out", lgettext("Decrease icon size"), fun(this, &Navigator_impl::zoom_out) };

    signal<void(const ustring &)> signal_file_select_;
    signal<void(const ustring &)> signal_file_unselect_;
    signal<void(const ustring &)> signal_file_activate_;
    signal<void(const ustring &)> signal_dir_changed_;

private:

    int  find_row(const ustring & name);
    ustring name_from_row(int y);
    ustring format_file_time(Timeval & mtime) const;    // TODO 0.8 const!
    void limit_name_column();
    void zoom_in();
    void zoom_out();
    void update_counters();
    void update_sort_marker();
    void update_sel_counter();
    void update_file_counter();
    void update_dir_counter();
    bool compare(const Rec & r1, const Rec & r2) const noexcept;
    std::vector<ustring> list_dir(const ustring & uri) const;

    Rec & make_record(const ustring & filename);
    void list_record(Rec & rec, int y=INT_MAX);
    void list_objects(Rec & rec);

    void begin_any();
    void begin_read();
    void run_read();
    void begin_sort();
    void run_sort_dirs();
    void begin_sort_files();
    void run_sort();
    void begin_filter();
    void run_filter();
    void begin_show();
    void show_dirs();
    void show_files();
    void end_show();
    void show_objects();

    void on_file_select(const ustring & filename);
    void on_file_unselect(const ustring & filename);
    void on_list_activate(int y);
    bool on_list_mark_validate(int y);
    void on_list_move_row(int o, int n);
    void on_header_click(int x);
    void on_header_width_changed(int x);
    void on_tick();
    void on_icon_size();
    bool on_mouse_wheel(int delta, int mm, const Point & pt);
    void on_cancel_disable();
    void on_watch(unsigned event, const ustring & path);
    void on_watch_timer(const ustring & path);
};

} // namespace tau

#endif // __TAU_NAVIGATOR_IMPL_HH__

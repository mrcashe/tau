// ----------------------------------------------------------------------------
// SPDX-License-Identifier: BSD-2-Clause
// Copyright © 2014-2024 Konstantin Shmelkov <mrcashe@gmail.com>.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
// THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
// PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
// CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
// EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
// PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
// OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
// WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
// OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
// EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// ----------------------------------------------------------------------------

#include <tau/buffer.hh>
#include <tau/container.hh>
#include <tau/cursor.hh>
#include <tau/exception.hh>
#include <tau/painter.hh>
#include <tau/string.hh>
#include <tau/toplevel.hh>
#include <tau/widget.hh>
#include <container-impl.hh>
#include <cursor-impl.hh>
#include <display-impl.hh>
#include <window-impl.hh>

namespace tau {

Widget::Widget():
    impl(std::make_shared<Widget_impl>())
{
    impl->facade_in(this);
}

Widget::Widget(const Widget & other):
    impl(other.impl)
{
    impl->facade_in(this);
}

Widget & Widget::operator=(const Widget & other) {
    if (this != &other) {
        if (impl) { impl->facade_out(this); }
        impl = other.impl;
        if (!impl) { throw user_error(str_format(this, " Widget::operator=(const Widget &): assignment from pure or incompatible copy")); }
    }

    return *this;
}

Widget::Widget(Widget && other):
    impl(other.impl)
{
    impl->facade_in(this);
}

Widget & Widget::operator=(Widget && other) {
    if (impl) { impl->facade_out(this); }
    impl = other.impl;
    if (!impl) { throw user_error(str_format(this, " Widget::operator=(Widget &&): assignment from pure or incompatible copy")); }
    return *this;
}

Widget::Widget(Widget_ptr wp):
    impl(wp)
{
    if (!impl) { throw user_error(str_format(this, " Widget::Widget(Widget_ptr): construct from pure or incompatible implementation pointer")); }
    impl->facade_in(this);
}

Widget & Widget::operator=(Widget_ptr wp) {
    if (impl) { impl->facade_out(this); }
    impl = wp;
    if (!impl) { throw user_error(str_format(this, " Widget::operator=(Widget_ptr): assignment from pure or incompatible implementation pointer")); }
    impl->facade_in(this);
    return *this;
}

Widget::~Widget() {
    impl->facade_out(this);
}

Widget_ptr Widget::ptr() noexcept {
    return impl;
}

Widget_cptr Widget::ptr() const noexcept {
    return impl;
}

void Widget::show() {
    impl->show();
}

void Widget::hide() {
    impl->hide();
}

void Widget::appear() {
    impl->appear();
}

void Widget::disappear() {
    impl->disappear();
}

void Widget::par_show(bool yes) {
    impl->par_show(yes);
}

Rect Widget::viewable_area() const noexcept {
    return impl->viewable_area();
}

Point Widget::origin() const noexcept {
    return impl->origin();
}

Size Widget::size() const noexcept {
    return impl->size();
}

bool Widget::require_size(const Size & sz) {
    return impl->require_size(sz);
}

bool Widget::require_size(unsigned width, unsigned height) {
    return impl->require_size(width, height);
}

Size Widget::required_size() const noexcept {
    return impl->required_size();
}

Size Widget::logical_size() const noexcept {
    return impl->logical_size();
}

bool Widget::hint_size(const Size & sz) {
    return impl->hint_size(sz);
}

bool Widget::hint_size(unsigned width, unsigned height) {
    return impl->hint_size(width, height);
}

Size Widget::size_hint() const noexcept {
    return impl->size_hint();
}

bool Widget::hint_min_size(const Size & sz) {
    return impl->hint_min_size(sz);
}

bool Widget::hint_min_size(unsigned width, unsigned height) {
    return impl->hint_min_size(width, height);
}

Size Widget::min_size_hint() const noexcept {
    return impl->min_size_hint();
}

bool Widget::hint_max_size(const Size & sz) {
    return impl->hint_max_size(sz);
}

bool Widget::hint_max_size(unsigned width, unsigned height) {
    return impl->hint_max_size(width, height);
}

Size Widget::max_size_hint() const noexcept {
    return impl->max_size_hint();
}

bool Widget::hint_margin_left(unsigned left) {
    return impl->hint_margin_left(left);
}

bool Widget::hint_margin_right(unsigned right) {
    return impl->hint_margin_right(right);
}

bool Widget::hint_margin_top(unsigned top) {
    return impl->hint_margin_top(top);
}

bool Widget::hint_margin_bottom(unsigned bottom) {
    return impl->hint_margin_bottom(bottom);
}

bool Widget::hint_margin(unsigned left, unsigned right, unsigned top, unsigned bottom) {
    return impl->hint_margin(left, right, top, bottom);
}

bool Widget::hint_margin(unsigned width) {
    return impl->hint_margin(width);
}

bool Widget::hint_margin(const Margin & margin) {
    return impl->hint_margin(margin);
}

Margin Widget::margin_hint() const noexcept {
    return impl->margin_hint();
}

void Widget::enable() {
    impl->enable();
}

void Widget::disable() {
    impl->disable();
}

void Widget::freeze() {
    impl->freeze();
}

void Widget::thaw() {
    impl->thaw();
}

void Widget::par_enable(bool yes) {
    impl->par_enable(yes);
}

bool Widget::enabled() const noexcept {
    return impl->enabled();
}

bool Widget::disabled() const noexcept {
    return impl->disabled();
}

bool Widget::frozen() const noexcept {
    return impl->frozen();
}

bool Widget::hover() const noexcept {
    return impl->hover();
}

Conf & Widget::conf() noexcept {
    return impl->conf();
}

const Conf & Widget::conf() const noexcept {
    return impl->conf();
}

bool Widget::hidden() const noexcept {
    return impl->hidden();
}

bool Widget::visible() const noexcept {
    return impl->visible();
}

bool Widget::grab_modal() {
    return impl->grab_modal();
}

bool Widget::end_modal() {
    return impl->end_modal();
}

bool Widget::grabs_modal() const noexcept {
    return impl->grabs_modal();
}

void Widget::allow_focus() {
    impl->allow_focus();
}

void Widget::disallow_focus() {
    impl->disallow_focus();
}

bool Widget::focused() const noexcept {
    return impl->focused();
}

bool Widget::focusable() const noexcept {
    return impl->focusable();
}

bool Widget::grab_focus() {
    return impl->grab_focus();
}

void Widget::drop_focus() {
    impl->drop_focus();
}

bool Widget::take_focus() {
    return impl->take_focus();
}

bool Widget::grab_mouse() {
    return impl->grab_mouse();
}

Point Widget::where_mouse() const noexcept {
    return impl->where_mouse();
}

bool Widget::grabs_mouse() const noexcept {
    return impl->grabs_mouse();
}

bool Widget::ungrab_mouse() {
    return impl->ungrab_mouse();
}

void Widget::set_cursor(Cursor cursor) {
    impl->set_cursor(cursor.ptr());
}

void Widget::set_cursor(const ustring & name, unsigned size) {
    impl->set_cursor(name, size);
}

Cursor Widget::cursor() noexcept {
    return impl->cursor();
}

void Widget::unset_cursor() {
    impl->unset_cursor();
}

void Widget::show_cursor() {
    impl->show_cursor();
}

void Widget::hide_cursor() {
    impl->hide_cursor();
}

bool Widget::cursor_hidden() const noexcept {
    return impl->cursor_hidden();
}

bool Widget::cursor_visible() const noexcept {
    return impl->cursor_visible();
}

void Widget::invalidate(const Rect & r) {
    impl->invalidate(r);
}

Painter Widget::painter() {
    return impl->painter();
}

void Widget::quit_dialog() {
    impl->quit_dialog();
}

Point Widget::to_screen(const Point & pt) const noexcept {
    return impl->to_screen(pt);
}

Rect Widget::to_screen(const Rect & r) const noexcept {
    return impl->to_screen(r);
}

Point Widget::to_parent(const Container & cont, const Point & pt) const noexcept {
    return impl->to_parent(static_cast<const Container_impl *>(cont.ptr().get()), pt);
}

Rect Widget::to_parent(const Container & cont, const Rect & r) const noexcept {
    return impl->to_parent(static_cast<const Container_impl *>(cont.ptr().get()), r);
}

connection Widget::connect_accel(Accel & accel, bool prepend) {
    return impl->connect_accel(accel, prepend);
}

Accel * Widget::allocate_accel(char32_t kc, int km, bool prepend) {
    return impl->allocate_accel(kc, km, prepend);
}

Accel * Widget::allocate_accel(const ustring & spec, bool prepend) {
    return impl->allocate_accel(spec, prepend);
}

void Widget::connect_action(Action_base & action, bool prepend) {
    impl->connect_action(action, prepend);
}

void Widget::set_tooltip(const ustring & s, Align align) {
    impl->set_tooltip(s, align);
}

void Widget::set_tooltip(Widget & w) {
    impl->set_tooltip(w.ptr());
}

bool Widget::has_tooltip() const noexcept {
    return impl->has_tooltip();
}

void Widget::unset_tooltip() {
    impl->unset_tooltip();
}

void Widget::allow_tooltip() {
    impl->allow_tooltip();
}

void Widget::disallow_tooltip() {
    impl->disallow_tooltip();
}

bool Widget::tooltip_allowed() const noexcept {
    return impl->tooltip_allowed();
}

Widget_ptr Widget::show_tooltip(const ustring & s, Align align) {
    return impl->show_tooltip(s, align);
}

Widget_ptr Widget::show_tooltip(const ustring & s, const Point & pt, Gravity gravity, unsigned time_ms) {
    return impl->show_tooltip(s, pt, gravity, time_ms);
}

Widget_ptr Widget::show_tooltip(const ustring & s, Align align, const Point & pt, Gravity gravity, unsigned time_ms) {
    return impl->show_tooltip(s, align, pt, gravity, time_ms);
}

void Widget::show_tooltip(Widget & w) {
    impl->show_tooltip(w.ptr());
}

void Widget::show_tooltip(Widget & w, const Point & pt, Gravity gravity, unsigned time_ms) {
    impl->show_tooltip(w.ptr(), pt, gravity, time_ms);
}

void Widget::hide_tooltip() {
    impl->hide_tooltip();
}

Point Widget::offset() const noexcept {
    return impl->offset();
}

void Widget::offset(const Point & pt) {
    impl->offset(pt);
}

void Widget::offset(int x, int y) {
    impl->offset(x, y);
}

Display Widget::display() noexcept {
    auto dp = Display_impl::ptr(impl->display());
    if (!dp) { dp = Display_impl::this_ptr(); }
    return dp;
}

const Display Widget::display() const noexcept {
    auto dp = Display_impl::ptr(impl->display());
    if (!dp) { dp = Display_impl::this_ptr(); }
    return dp;
}

Widget_ptr Widget::container() noexcept {
    auto pp = impl->container();
    return pp && pp->container() ? pp->container()->chptr(pp) : nullptr;
}

Widget_cptr Widget::container() const noexcept {
    auto pp = impl->container();
    return pp && pp->container() ? pp->container()->chptr(pp) : nullptr;
}

Widget_ptr Widget::window() noexcept {
    auto wip = impl->window();
    return wip && wip->display() ? wip->display()->winptr(wip) : nullptr;
}

Widget_cptr Widget::window() const noexcept {
    auto wip = impl->window();
    return wip && wip->display() ? wip->display()->winptr(wip) : nullptr;
}

Widget_ptr Widget::toplevel() noexcept {
    auto wip = impl->toplevel();
    return wip && wip->display() ? wip->display()->winptr(wip) : nullptr;
}

Widget_cptr Widget::toplevel() const noexcept {
    auto wip = impl->toplevel();
    return wip && wip->display() ? wip->display()->winptr(wip) : nullptr;
}

bool Widget::has_parent() const noexcept {
    return impl->has_parent();
}

bool Widget::has_parent(const Widget & w) const noexcept {
    return impl->has_parent(w.ptr().get());
}

bool Widget::has_window() const noexcept {
    return impl->has_window();
}

bool Widget::has_display() const noexcept {
    return impl->has_display();
}

bool Widget::scrollable() const noexcept {
    return impl->scrollable();
}

void Widget::rise() {
    impl->rise();
}

void Widget::lower() {
    impl->lower();
}

int Widget::level() const noexcept {
    return impl->level();
}

bool Widget::hint_aspect_ratio(double ratio) {
    return impl->hint_aspect_ratio(ratio);
}

double Widget::required_aspect_ratio() const noexcept {
    return impl->required_aspect_ratio();
}

bool Widget::has_aspect_ratio_hint() const noexcept {
    return impl->has_aspect_ratio_hint();
}

bool Widget::require_aspect_ratio(double ratio) {
    return impl->require_aspect_ratio(ratio);
}

bool Widget::running() const noexcept {
    return impl->running();
}

bool Widget::in_shutdown() const noexcept {
    return impl->in_shutdown();
}

ustring Widget::epath() const {
    return impl->epath();
}

void Widget::set_string(const ustring & s) {
    impl->set_string(s);
}

ustring Widget::get_string() const {
    return impl->get_string();
}

void Widget::set_boolean(bool yes) {
    impl->set_boolean(yes);
}

bool Widget::get_boolean() const {
    return impl->get_boolean();
}

void Widget::set_integer(intmax_t i) {
    impl->set_integer(i);
}

intmax_t Widget::get_integer() const {
    return impl->get_integer();
}

void Widget::set_real(double d) {
    impl->set_real(d);
}

double Widget::get_real() const {
    return impl->get_real();
}

connection Widget::connect_run(slot<void()> slot, bool prepend) {
    return impl->connect_run(slot, prepend);
}

bool Widget::selected() const noexcept {
    return impl->selected();
}

Widget * Widget::facade() {
    return impl->facade();
}

const Widget * Widget::facade() const {
    return impl->facade();
}

signal<void()> & Widget::signal_unparent() {
    return impl->signal_unparent();
}

signal<void(Object *)> & Widget::signal_parent() {
    return impl->signal_parent();
}

signal<void()> & Widget::signal_destroy() {
    return impl->signal_destroy();
}

signal<void()> & Widget::signal_enable() {
    return impl->signal_enable();
}

signal<void()> & Widget::signal_disable() {
    return impl->signal_disable();
}

signal<void()> & Widget::signal_origin_changed() {
    return impl->signal_origin_changed();
}

signal<void()> & Widget::signal_size_changed() {
    return impl->signal_size_changed();
}

signal<void()> & Widget::signal_offset_changed() {
    return impl->signal_offset_changed();
}

signal<bool(Painter, Rect)> & Widget::signal_paint() {
    return impl->signal_paint();
}

signal<bool(Painter, Rect)> & Widget::signal_backpaint() {
    return impl->signal_backpaint();
}

signal<bool(char32_t, int)> & Widget::signal_key_down() {
    return impl->signal_key_down();
}

signal<bool(char32_t, int)> & Widget::signal_key_up() {
    return impl->signal_key_up();
}

signal<bool(const ustring &, int)> & Widget::signal_input() {
    return impl->signal_input();
}

signal<bool(int, int, Point)> & Widget::signal_mouse_down() {
    return impl->signal_mouse_down();
}

signal<bool(int, int, Point)> & Widget::signal_mouse_double_click() {
    return impl->signal_mouse_double_click();
}

signal<bool(int, int, Point)> & Widget::signal_mouse_up() {
    return impl->signal_mouse_up();
}

signal<void(int, Point)> & Widget::signal_mouse_motion() {
    return impl->signal_mouse_motion();
}

signal<void(Point)> & Widget::signal_mouse_enter() {
    return impl->signal_mouse_enter();
}

signal<void()> & Widget::signal_mouse_leave() {
    return impl->signal_mouse_leave();
}

signal<bool(int, int, Point)> & Widget::signal_mouse_wheel() {
    return impl->signal_mouse_wheel();
}

signal<void()> & Widget::signal_focus_in() {
    return impl->signal_focus_in();
}

signal<void()> & Widget::signal_focus_out() {
    return impl->signal_focus_out();
}

signal<void()> & Widget::signal_select() {
    return impl->signal_select();
}

signal<void()> & Widget::signal_unselect() {
    return impl->signal_unselect();
}

signal<void(Hints)> & Widget::signal_hints_changed() {
    return impl->signal_hints_changed();
}

signal<void()> & Widget::signal_show() {
    return impl->signal_show();
}

signal<void()> & Widget::signal_hide() {
    return impl->signal_hide();
}

signal<void()> & Widget::signal_display_in() {
    return impl->signal_display_in();
}

signal<void()> & Widget::signal_display_out() {
    return impl->signal_display_out();
}

signal<bool()> & Widget::signal_take_focus() {
    return impl->signal_take_focus();
}

} // namespace tau

//END

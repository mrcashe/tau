// ----------------------------------------------------------------------------
// SPDX-License-Identifier: BSD-2-Clause
// Copyright © 2014-2024 Konstantin Shmelkov <mrcashe@gmail.com>.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
// THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
// PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
// CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
// EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
// PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
// OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
// WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
// OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
// EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// ----------------------------------------------------------------------------

#include <file-impl.hh>
#include <pixmap-impl.hh>

namespace tau {

File::File() {
    impl = File_impl::create();
}

File::~File() {}

File::File(const ustring & uri) {
    impl = File_impl::create(uri);
}

bool File::exists() const noexcept {
    return impl->exists();
}

uint64_t File::bytes() const noexcept {
    return impl->bytes();
}

bool File::is_dir() const noexcept {
    return impl->is_dir();
}

bool File::is_link() const noexcept {
    return impl->is_link();
}

bool File::is_regular() const noexcept {
    return impl->is_regular();
}

bool File::is_char() const noexcept {
    return impl->is_char();
}

bool File::is_block() const noexcept {
    return impl->is_block();
}

bool File::is_fifo() const noexcept {
    return impl->is_fifo();
}

bool File::is_exec() const noexcept {
    return impl->is_exec();
}

bool File::is_hidden() const noexcept {
    return impl->is_hidden();
}

bool File::is_removable() const noexcept {
    return impl->is_removable();
}

Timeval File::atime() const noexcept {
    return impl->atime();
}

Timeval File::ctime() const noexcept {
    return impl->ctime();
}

Timeval File::mtime() const noexcept {
    return impl->mtime();
}

std::optional<ustring> File::read_link() const {
    return impl->read_link();
}

const Pixmap File::icon(int icon_size) const {
    return impl->icon(icon_size);
}

std::string File::mime() const {
    return impl->mime();
}

ustring File::type(const Language & lang) const {
    return impl->type();
}

void File::rm(int opts, slot<void(int)> slot_async) {
    impl->rm(opts, slot_async);
}

void File::cp(const ustring & dest, int opts) {
    impl->cp(dest, opts);
}

signal<void(int, const ustring &)> & File::signal_watch(int event_mask) {
    return impl->signal_watch(event_mask);
}

} // namespace tau

//END

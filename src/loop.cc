// ----------------------------------------------------------------------------
// SPDX-License-Identifier: BSD-2-Clause
// Copyright © 2014-2024 Konstantin Shmelkov <mrcashe@gmail.com>.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
// THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
// PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
// CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
// EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
// PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
// OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
// WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
// OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
// EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// ----------------------------------------------------------------------------

#include <tau/exception.hh>
#include <tau/event.hh>
#include <tau/loop.hh>
#include <tau/timer.hh>
#include <tau/ustring.hh>
#include <event-impl.hh>
#include <loop-impl.hh>
#include <iostream>

namespace tau {

Loop::Loop() {}
Loop::Loop(Loop_ptr lp): impl(lp) {}
Loop::~Loop() {}

Loop_ptr Loop::ptr() {
    return impl;
}

Loop_cptr Loop::ptr() const {
    return impl;
}

// static
Loop Loop::this_loop() {
    Loop lp;
    lp.impl = Loop_impl::this_ptr();
    return lp;
}

Loop::operator bool() const noexcept {
    return nullptr != impl;
}

void Loop::run() {
    if (!impl) { Loop_impl::this_ptr()->run(); }
    else { impl->run(); }
}

void Loop::quit() {
    if (!impl) { Loop_impl::this_ptr()->quit(); }
    else { impl->quit(); }
}

int Loop::id() const noexcept {
    if (!impl) { return Loop_impl::this_ptr()->id(); }
    return impl->id();
}

bool Loop::running() const noexcept {
    if (!impl) { return Loop_impl::this_ptr()->running(); }
    return impl->running();
}

// static
std::size_t Loop::count() noexcept {
    return Loop_impl::count();
}

// static
std::vector<Loop> Loop::list() {
    auto all = Loop_impl::list();
    std::vector<Loop> res;
    std::size_t n = 0;
    for (auto lp: all) { res[n++] = lp; }
    return res;
}

bool Loop::alive() const noexcept {
    if (!impl) { return Loop_impl::this_ptr()->alive(); }
    return impl->alive();
}

std::vector<ustring> Loop::mounts() const {
    if (!impl) { return Loop_impl::this_ptr()->mounts(); }
    return impl->mounts();
}

Event Loop::event(slot<void()> slot_ready) {
    auto ev = impl ? impl->event() : Loop_impl::this_ptr()->event();
    if (slot_ready) { ev->signal_ready().connect(slot_ready); }
    return Event_impl::wrap(ev);
}

connection Loop::alarm(slot<void()> slot_alarm, int timeout_ms, bool periodical) {
    if (!impl) { return Loop_impl::this_ptr()->alarm(slot_alarm, timeout_ms, periodical); }
    return impl->alarm(slot_alarm, timeout_ms, periodical);
}

signal<void()> & Loop::signal_start() {
    if (!impl) { return Loop_impl::this_ptr()->signal_start(); }
    return impl->signal_start();
}

signal<void()> & Loop::signal_idle() {
    if (!impl) { return Loop_impl::this_ptr()->signal_idle(); }
    return impl->signal_idle();
}

signal<void()> & Loop::signal_quit() {
    if (!impl) { return Loop_impl::this_ptr()->signal_quit(); }
    return impl->signal_quit();
}

signal<void()> & Loop::signal_run() {
    if (!impl) { return Loop_impl::this_ptr()->signal_run(); }
    return impl->signal_run();
}

signal<void(int, const ustring &)> & Loop::signal_watch() {
    if (!impl) { return Loop_impl::this_ptr()->signal_watch(); }
    return impl->signal_watch();
}

Loop::Signal_iterate & Loop::signal_iterate() {
    if (!impl) { return Loop_impl::this_ptr()->signal_iterate(); }
    return impl->signal_iterate();
}

} // namespace tau

//END

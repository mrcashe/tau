// ----------------------------------------------------------------------------
// SPDX-License-Identifier: BSD-2-Clause
// Copyright © 2014-2024 Konstantin Shmelkov <mrcashe@gmail.com>.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
// THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
// PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
// CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
// EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
// PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
// OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
// WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
// OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
// EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// ----------------------------------------------------------------------------

#include <tau/button.hh>
#include <button-impl.hh>

namespace tau {

#define BUTTON_BASE_IMPL (std::static_pointer_cast<Button_base_impl>(impl))
#define BUTTON_IMPL (std::static_pointer_cast<Button_impl>(impl))
#define TOGGLE_IMPL (std::static_pointer_cast<Toggle_impl>(impl))

Button_base::Button_base(Widget_ptr wp):
    Widget(wp)
{
}

Button_base & Button_base::operator=(Widget_ptr wp) {
    Widget::operator=(std::dynamic_pointer_cast<Button_base_impl>(wp));
    return *this;
}

void Button_base::set_image(Widget & w) {
    BUTTON_BASE_IMPL->set_image(w.ptr());
}

void Button_base::set_label(const ustring & text) {
    BUTTON_BASE_IMPL->set_label(text);
}

void Button_base::set_icon(const ustring & icon_name, int icon_size) {
    BUTTON_BASE_IMPL->set_icon(icon_name, icon_size);
}

int Button_base::icon_size() const noexcept {
    return BUTTON_BASE_IMPL->icon_size();
}

void Button_base::resize_icon(int icon_size) {
    BUTTON_BASE_IMPL->resize_icon(icon_size);
}

void Button_base::show_relief() {
    BUTTON_BASE_IMPL->show_relief();
}

void Button_base::hide_relief() {
    BUTTON_BASE_IMPL->hide_relief();
}

bool Button_base::relief_visible() const noexcept {
    return BUTTON_BASE_IMPL->relief_visible();
}

void Button_base::set_border_top_left_radius(unsigned radius) {
    BUTTON_BASE_IMPL->set_border_top_left_radius(radius);
}

void Button_base::set_border_top_right_radius(unsigned radius) {
    BUTTON_BASE_IMPL->set_border_top_right_radius(radius);
}

void Button_base::set_border_bottom_right_radius(unsigned radius) {
    BUTTON_BASE_IMPL->set_border_bottom_right_radius(radius);
}

void Button_base::set_border_bottom_left_radius(unsigned radius) {
    BUTTON_BASE_IMPL->set_border_bottom_left_radius(radius);
}

void Button_base::set_border_radius(unsigned radius) {
    BUTTON_BASE_IMPL->set_border_radius(radius);
}

void Button_base::set_border_radius(unsigned top_left, unsigned top_right, unsigned bottom_right, unsigned bottom_left) {
    BUTTON_BASE_IMPL->set_border_radius(top_left, top_right, bottom_right, bottom_left);
}

void Button_base::unset_border_top_left_radius() {
    BUTTON_BASE_IMPL->unset_border_top_left_radius();
}

void Button_base::unset_border_top_right_radius() {
    BUTTON_BASE_IMPL->unset_border_top_right_radius();
}

void Button_base::unset_border_bottom_right_radius() {
    BUTTON_BASE_IMPL->unset_border_bottom_right_radius();
}

void Button_base::unset_border_bottom_left_radius() {
    BUTTON_BASE_IMPL->unset_border_bottom_left_radius();
}

void Button_base::unset_border_radius() {
    BUTTON_BASE_IMPL->unset_border_radius();
}

unsigned Button_base::border_top_left_radius() const noexcept {
    return BUTTON_BASE_IMPL->border_top_left_radius();
}

unsigned Button_base::border_top_right_radius() const noexcept {
    return BUTTON_BASE_IMPL->border_top_right_radius();
}

unsigned Button_base::border_bottom_right_radius() const noexcept {
    return BUTTON_BASE_IMPL->border_bottom_right_radius();
}

unsigned Button_base::border_bottom_left_radius() const noexcept {
    return BUTTON_BASE_IMPL->border_bottom_left_radius();
}

// ----------------------------------------------------------------------------
// ----------------------------------------------------------------------------

Button::Button():
    Button_base(std::make_shared<Button_impl>())
{
}

Button::Button(unsigned radius):
    Button_base(std::make_shared<Button_impl>(radius))
{
}

Button::Button(unsigned rtop_left, unsigned rtop_right, unsigned rbottom_right, unsigned rbottom_left):
    Button_base(std::make_shared<Button_impl>(rtop_left, rtop_right, rbottom_right, rbottom_left))
{
}

Button::Button(const ustring & label):
    Button_base(std::static_pointer_cast<Widget_impl>(std::make_shared<Button_impl>(label)))
{
}

Button::Button(const ustring & label, unsigned radius):
    Button_base(std::static_pointer_cast<Widget_impl>(std::make_shared<Button_impl>(label, radius)))
{
}

Button::Button(const ustring & label, unsigned rtop_left, unsigned rtop_right, unsigned rbottom_right, unsigned rbottom_left):
    Button_base(std::static_pointer_cast<Widget_impl>(std::make_shared<Button_impl>(label, rtop_left, rtop_right, rbottom_right, rbottom_left)))
{
}

Button::Button(Widget & img):
    Button_base(std::static_pointer_cast<Widget_impl>(std::make_shared<Button_impl>(img.ptr())))
{
}

Button::Button(Widget & img, unsigned radius):
    Button_base(std::static_pointer_cast<Widget_impl>(std::make_shared<Button_impl>(img.ptr(), radius)))
{
}

Button::Button(Widget & img, unsigned rtop_left, unsigned rtop_right, unsigned rbottom_right, unsigned rbottom_left):
    Button_base(std::static_pointer_cast<Widget_impl>(std::make_shared<Button_impl>(img.ptr(), rtop_left, rtop_right, rbottom_right, rbottom_left)))
{
}

Button::Button(Widget & img, const ustring & label):
    Button_base(std::static_pointer_cast<Widget_impl>(std::make_shared<Button_impl>(img.ptr(), label)))
{
}

Button::Button(Widget & img, const ustring & label, unsigned radius):
    Button_base(std::static_pointer_cast<Widget_impl>(std::make_shared<Button_impl>(img.ptr(), label, radius)))
{
}

Button::Button(Widget & img, const ustring & label, unsigned rtop_left, unsigned rtop_right, unsigned rbottom_right, unsigned rbottom_left):
    Button_base(std::static_pointer_cast<Widget_impl>(std::make_shared<Button_impl>(img.ptr(), label, rtop_left, rtop_right, rbottom_right, rbottom_left)))
{
}

Button::Button(int icon_size, const ustring & icon_name):
    Button_base(std::static_pointer_cast<Widget_impl>(std::make_shared<Button_impl>(icon_size, icon_name)))
{
}

Button::Button(int icon_size, const ustring & icon_name, unsigned radius):
    Button_base(std::static_pointer_cast<Widget_impl>(std::make_shared<Button_impl>(icon_size, icon_name, radius)))
{
}

Button::Button(int icon_size, const ustring & icon_name, unsigned rtop_left, unsigned rtop_right, unsigned rbottom_right, unsigned rbottom_left):
    Button_base(std::static_pointer_cast<Widget_impl>(std::make_shared<Button_impl>(icon_size, icon_name, rtop_left, rtop_right, rbottom_right, rbottom_left)))
{
}

Button::Button(const ustring & label, int icon_size, const ustring & icon_name):
    Button_base(std::static_pointer_cast<Widget_impl>(std::make_shared<Button_impl>(label, icon_size, icon_name)))
{
}

Button::Button(Action & action, Action::Flags items):
    Button_base(std::static_pointer_cast<Widget_impl>(std::make_shared<Button_impl>(action, items)))
{
}

Button::Button(Action & action, unsigned radius, Action::Flags items):
    Button_base(std::static_pointer_cast<Widget_impl>(std::make_shared<Button_impl>(action, radius, items)))
{
}

Button::Button(Action & action, unsigned rtop_left, unsigned rtop_right, unsigned rbottom_right, unsigned rbottom_left, Action::Flags items):
    Button_base(std::static_pointer_cast<Widget_impl>(std::make_shared<Button_impl>(action, rtop_left, rtop_right, rbottom_right, rbottom_left, items)))
{
}

Button::Button(Action & action, int icon_size, Action::Flags items):
    Button_base(std::static_pointer_cast<Widget_impl>(std::make_shared<Button_impl>(action, icon_size, items)))
{
}

Button::Button(Action & action, int icon_size, unsigned radius, Action::Flags items):
    Button_base(std::static_pointer_cast<Widget_impl>(std::make_shared<Button_impl>(action, icon_size, radius, items)))
{
}

Button::Button(Action & action, int icon_size, unsigned rtop_left, unsigned rtop_right, unsigned rbottom_right, unsigned rbottom_left, Action::Flags items):
    Button_base(std::static_pointer_cast<Widget_impl>(std::make_shared<Button_impl>(action, icon_size, rtop_left, rtop_right, rbottom_right, rbottom_left, items)))
{
}

Button::Button(Master_action & action, Action::Flags items):
    Button_base(std::static_pointer_cast<Widget_impl>(std::make_shared<Button_impl>(action, items)))
{
}

Button::Button(Master_action & action, unsigned radius, Action::Flags items):
    Button_base(std::static_pointer_cast<Widget_impl>(std::make_shared<Button_impl>(action, radius, items)))
{
}

Button::Button(Master_action & action, unsigned rtop_left, unsigned rtop_right, unsigned rbottom_right, unsigned rbottom_left, Action::Flags items):
    Button_base(std::static_pointer_cast<Widget_impl>(std::make_shared<Button_impl>(action, rtop_left, rtop_right, rbottom_right, rbottom_left, items)))
{
}

Button::Button(Master_action & action, int icon_size, Action::Flags items):
    Button_base(std::static_pointer_cast<Widget_impl>(std::make_shared<Button_impl>(action, icon_size, items)))
{
}

Button::Button(Master_action & action, int icon_size, unsigned radius, Action::Flags items):
    Button_base(std::static_pointer_cast<Widget_impl>(std::make_shared<Button_impl>(action, icon_size, radius, items)))
{
}

Button::Button(Master_action & action, int icon_size, unsigned rtop_left, unsigned rtop_right, unsigned rbottom_right, unsigned rbottom_left, Action::Flags items):
    Button_base(std::static_pointer_cast<Widget_impl>(std::make_shared<Button_impl>(action, icon_size, rtop_left, rtop_right, rbottom_right, rbottom_left, items)))
{
}

Button::Button(const Button & other):
    Button_base(other.impl)
{
}

Button & Button::operator=(const Button & other) {
    Button_base::operator=(other);
    return *this;
}

Button::Button(Button && other):
    Button_base(other.impl)
{
}

Button & Button::operator=(Button && other) {
    Button_base::operator=(other);
    return *this;
}

Button::Button(Widget_ptr wp):
    Button_base(std::dynamic_pointer_cast<Button_impl>(wp))
{
}

Button & Button::operator=(Widget_ptr wp) {
    Button_base::operator=(std::dynamic_pointer_cast<Button_impl>(wp));
    return *this;
}

void Button::click() {
    BUTTON_IMPL->click();
}

void Button::enable_repeat() noexcept {
    BUTTON_IMPL->enable_repeat();
}

void Button::disable_repeat() noexcept {
    BUTTON_IMPL->disable_repeat();
}

bool Button::repeat_enabled() const noexcept {
    return BUTTON_IMPL->repeat_enabled();
}

void Button::set_repeat_delay(unsigned first, unsigned next) {
    BUTTON_IMPL->set_repeat_delay(first, next);
}

unsigned Button::repeat_delay() const noexcept {
    return BUTTON_IMPL->repeat_delay();
}

unsigned Button::repeat_interval() const noexcept {
    return BUTTON_IMPL->repeat_interval();
}

connection Button::connect(const slot<void()> & slot, bool prepend) {
    return BUTTON_IMPL->connect(slot, prepend);
}

connection Button::connect(slot<void()> && slot, bool prepend) {
    return BUTTON_IMPL->connect(slot, prepend);
}

void Button::set_action(Action & action, Action::Flags items) {
    BUTTON_IMPL->set_action(action, items);
}

void Button::select(Action & action) {
    BUTTON_IMPL->select(action);
}

// ----------------------------------------------------------------------------
// ----------------------------------------------------------------------------

Toggle::Toggle():
    Button_base(std::make_shared<Toggle_impl>())
{
}

Toggle::Toggle(unsigned radius):
    Button_base(std::make_shared<Toggle_impl>(radius))
{
}

Toggle::Toggle(unsigned rtop_left, unsigned rtop_right, unsigned rbottom_right, unsigned rbottom_left):
    Button_base(std::make_shared<Toggle_impl>(rtop_left, rtop_right, rbottom_right, rbottom_left))
{
}

Toggle::Toggle(const ustring & label):
    Button_base(std::static_pointer_cast<Widget_impl>(std::make_shared<Toggle_impl>(label)))
{
}

Toggle::Toggle(const ustring & label, unsigned radius):
    Button_base(std::static_pointer_cast<Widget_impl>(std::make_shared<Toggle_impl>(label, radius)))
{
}

Toggle::Toggle(const ustring & label, unsigned rtop_left, unsigned rtop_right, unsigned rbottom_right, unsigned rbottom_left):
    Button_base(std::static_pointer_cast<Widget_impl>(std::make_shared<Toggle_impl>(label, rtop_left, rtop_right, rbottom_right, rbottom_left)))
{
}

Toggle::Toggle(Widget & img):
    Button_base(std::static_pointer_cast<Widget_impl>(std::make_shared<Toggle_impl>(img.ptr())))
{
}

Toggle::Toggle(Widget & img, unsigned radius):
    Button_base(std::static_pointer_cast<Widget_impl>(std::make_shared<Toggle_impl>(img.ptr(), radius)))
{
}

Toggle::Toggle(Widget & img, unsigned rtop_left, unsigned rtop_right, unsigned rbottom_right, unsigned rbottom_left):
    Button_base(std::static_pointer_cast<Widget_impl>(std::make_shared<Toggle_impl>(img.ptr(), rtop_left, rtop_right, rbottom_right, rbottom_left)))
{
}

Toggle::Toggle(Widget & img, const ustring & label):
    Button_base(std::static_pointer_cast<Widget_impl>(std::make_shared<Toggle_impl>(img.ptr(), label)))
{
}

Toggle::Toggle(Widget & img, const ustring & label, unsigned radius):
    Button_base(std::static_pointer_cast<Widget_impl>(std::make_shared<Toggle_impl>(img.ptr(), label, radius)))
{
}

Toggle::Toggle(Widget & img, const ustring & label, unsigned rtop_left, unsigned rtop_right, unsigned rbottom_right, unsigned rbottom_left):
    Button_base(std::static_pointer_cast<Widget_impl>(std::make_shared<Toggle_impl>(img.ptr(), label, rtop_left, rtop_right, rbottom_right, rbottom_left)))
{
}

Toggle::Toggle(int icon_size, const ustring & icon_name):
    Button_base(std::static_pointer_cast<Widget_impl>(std::make_shared<Toggle_impl>(icon_size, icon_name)))
{
}

Toggle::Toggle(int icon_size, const ustring & icon_name, unsigned radius):
    Button_base(std::static_pointer_cast<Widget_impl>(std::make_shared<Toggle_impl>(icon_size, icon_name, radius)))
{
}

Toggle::Toggle(int icon_size, const ustring & icon_name, unsigned rtop_left, unsigned rtop_right, unsigned rbottom_right, unsigned rbottom_left):
    Button_base(std::static_pointer_cast<Widget_impl>(std::make_shared<Toggle_impl>(icon_size, icon_name, rtop_left, rtop_right, rbottom_right, rbottom_left)))
{
}

Toggle::Toggle(const ustring & label, int icon_size, const ustring & icon_name):
    Button_base(std::static_pointer_cast<Widget_impl>(std::make_shared<Toggle_impl>(label, icon_size, icon_name)))
{
}

Toggle::Toggle(Toggle_action & action, Action::Flags items):
    Button_base(std::static_pointer_cast<Widget_impl>(std::make_shared<Toggle_impl>(action, items)))
{
}

Toggle::Toggle(Toggle_action & action, unsigned radius, Action::Flags items):
    Button_base(std::static_pointer_cast<Widget_impl>(std::make_shared<Toggle_impl>(action, radius, items)))
{
}

Toggle::Toggle(Toggle_action & action, unsigned rtop_left, unsigned rtop_right, unsigned rbottom_right, unsigned rbottom_left, Action::Flags items):
    Button_base(std::static_pointer_cast<Widget_impl>(std::make_shared<Toggle_impl>(action, rtop_left, rtop_right, rbottom_right, rbottom_left, items)))
{
}

Toggle::Toggle(Toggle_action & action, int icon_size, Action::Flags items):
    Button_base(std::static_pointer_cast<Widget_impl>(std::make_shared<Toggle_impl>(action, icon_size, items)))
{
}

Toggle::Toggle(Toggle_action & action, int icon_size, unsigned radius, Action::Flags items):
    Button_base(std::static_pointer_cast<Widget_impl>(std::make_shared<Toggle_impl>(action, icon_size, radius, items)))
{
}

Toggle::Toggle(Toggle_action & action, int icon_size, unsigned rtop_left, unsigned rtop_right, unsigned rbottom_right, unsigned rbottom_left, Action::Flags items):
    Button_base(std::static_pointer_cast<Widget_impl>(std::make_shared<Toggle_impl>(action, icon_size, rtop_left, rtop_right, rbottom_right, rbottom_left, items)))
{
}

Toggle::Toggle(Master_action & action, Action::Flags items):
    Button_base(std::static_pointer_cast<Widget_impl>(std::make_shared<Toggle_impl>(action, items)))
{
}

Toggle::Toggle(Master_action & action, unsigned radius, Action::Flags items):
    Button_base(std::static_pointer_cast<Widget_impl>(std::make_shared<Toggle_impl>(action, radius, items)))
{
}

Toggle::Toggle(Master_action & action, unsigned rtop_left, unsigned rtop_right, unsigned rbottom_right, unsigned rbottom_left, Action::Flags items):
    Button_base(std::static_pointer_cast<Widget_impl>(std::make_shared<Toggle_impl>(action, rtop_left, rtop_right, rbottom_right, rbottom_left, items)))
{
}

Toggle::Toggle(Master_action & action, int icon_size, Action::Flags items):
    Button_base(std::static_pointer_cast<Widget_impl>(std::make_shared<Toggle_impl>(action, icon_size, items)))
{
}

Toggle::Toggle(Master_action & action, int icon_size, unsigned radius, Action::Flags items):
    Button_base(std::static_pointer_cast<Widget_impl>(std::make_shared<Toggle_impl>(action, icon_size, radius, items)))
{
}

Toggle::Toggle(Master_action & action, int icon_size, unsigned rtop_left, unsigned rtop_right, unsigned rbottom_right, unsigned rbottom_left, Action::Flags items):
    Button_base(std::static_pointer_cast<Widget_impl>(std::make_shared<Toggle_impl>(action, icon_size, rtop_left, rtop_right, rbottom_right, rbottom_left, items)))
{
}

Toggle::Toggle(const Toggle & other):
    Button_base(other.impl)
{
}

Toggle & Toggle::operator=(const Toggle & other) {
    Button_base::operator=(other);
    return *this;
}

Toggle::Toggle(Toggle && other):
    Button_base(other.impl)
{
}

Toggle & Toggle::operator=(Toggle && other) {
    Button_base::operator=(other);
    return *this;
}

Toggle::Toggle(Widget_ptr wp):
    Button_base(std::dynamic_pointer_cast<Toggle_impl>(wp))
{
}

Toggle & Toggle::operator=(Widget_ptr wp) {
    Button_base::operator=(std::dynamic_pointer_cast<Toggle_impl>(wp));
    return *this;
}

void Toggle::toggle() {
    TOGGLE_IMPL->toggle();
}

void Toggle::setup(bool state) {
    TOGGLE_IMPL->setup(state);
}

void Toggle::set(bool state) {
    TOGGLE_IMPL->set(state);
}

bool Toggle::get() const noexcept {
    return TOGGLE_IMPL->get();
}

connection Toggle::connect(const slot<void(bool)> & slot, bool prepend) {
    return TOGGLE_IMPL->connect(slot, prepend);
}

connection Toggle::connect(slot<void(bool)> && slot, bool prepend) {
    return TOGGLE_IMPL->connect(slot, prepend);
}

void Toggle::set_action(Toggle_action & action, Action::Flags items) {
    TOGGLE_IMPL->set_action(action, items);
}

void Toggle::select(Toggle_action & action) {
    TOGGLE_IMPL->select(action);
}

} // namespace tau

//END

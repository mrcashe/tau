// ----------------------------------------------------------------------------
// SPDX-License-Identifier: BSD-2-Clause
// Copyright © 2014-2024 Konstantin Shmelkov <mrcashe@gmail.com>.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
// THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
// PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
// CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
// EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
// PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
// OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
// WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
// OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
// EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// ----------------------------------------------------------------------------

#include <tau/geometry.hh>
#include <cmath>
#include <numbers>
#include <iostream>

namespace {

// Borrowed from Cairo Graphics project, see cairo-arc.c.
double arc_max_angle_for_tolerance_normalized(double tolerance) {
    // Use table lookup to reduce search time in most cases.
    static constexpr struct { double angle, error; } table[] = {
        { std::numbers::pi/1.0,   0.0185185185185185036127   },
        { std::numbers::pi/2.0,   0.000272567143730179811158 },
        { std::numbers::pi/3.0,   2.38647043651461047433e-05 },
        { std::numbers::pi/4.0,   4.2455377443222443279e-06  },
        { std::numbers::pi/5.0,   1.11281001494389081528e-06 },
        { std::numbers::pi/6.0,   3.72662000942734705475e-07 },
        { std::numbers::pi/7.0,   1.47783685574284411325e-07 },
        { std::numbers::pi/8.0,   6.63240432022601149057e-08 },
        { std::numbers::pi/9.0,   3.2715520137536980553e-08  },
        { std::numbers::pi/10.0,  1.73863223499021216974e-08 },
        { std::numbers::pi/11.0,  9.81410988043554039085e-09 },
    };

    double angle, error;
    std::size_t i = 0;

    for (; i < std::size(table); ++i) {
        if (table[i].error < tolerance) {
            return table[i].angle;
        }
    }

    ++i;

    do {
        angle = std::numbers::pi/i++;
        error = 2.0/27.0*std::pow(std::sin(angle/4), 6)/std::pow(std::cos(angle/4), 2);
    } while (error > tolerance);

    return angle;
}

void arc_segment(tau::Contour & ctr, double xc, double yc, double radius, double angle1, double angle2) {
    double rsin1 = radius*std::sin(angle1);
    double rcos1 = radius*std::cos(angle1);
    double rsin2 = radius*std::sin(angle2);
    double rcos2 = radius*std::cos(angle2);
    double h = 4.0/3.0*std::tan((angle2-angle1)/4.0);

    ctr.cubic_to(tau::Vector(xc+rcos1-h*rsin1, yc+rsin1+h*rcos1),
                 tau::Vector(xc+rcos2+h*rsin2, yc+rsin2-h*rcos2),
                 tau::Vector(xc+rcos2, yc+rsin2));
}

} // anonymous namespace

namespace tau {

Contour::Contour(const Vector & start):
    start_(start)
{
}

Contour::Contour(const Vector & start, const Vector & end):
    start_(start)
{
    line_to(end);
}

Contour::Contour(const Vector & start, const Vector & cp, const Vector & end):
    start_(start)
{
    conic_to(cp, end);
}

Contour::Contour(const Vector & start, const Vector & cp1, const Vector & cp2, const Vector & end):
    start_(start)
{
    cubic_to(cp1, cp2, end);
}

Contour::Contour(const Contour & ctr, const Matrix & mat):
    start_(ctr.start_*mat)
{
    for (auto & cv: ctr.curves_) {
        curves_.emplace_back(cv*mat);
    }
}

Contour::Contour(Contour && ctr):
    start_(ctr.start_),
    curves_(std::move(ctr.curves_))
{
}

Contour & Contour::operator=(Contour && ctr) {
    start_ = ctr.start_;
    curves_ = std::move(ctr.curves_);
    return *this;
}

Contour & Contour::rectangle(const Vector & v1, const Vector & v2, double radius) {
    double xmin = std::min(v1.x(), v2.x()), xmax = std::max(v1.x(), v2.x());
    double ymin = std::min(v1.y(), v2.y()), ymax = std::max(v1.y(), v2.y());

    if (radius <= 0.0) {
        start_.set(xmin, ymin);
        line_to(xmax, ymin);
        line_to(xmax, ymax);
        line_to(xmin, ymax);
        line_to(xmin, ymin);
    }

    else {
        double r = std::min(radius, std::min(std::fabs(xmax-xmin)/2, std::fabs(ymax-ymin)/2));
        start_.set(xmin+r, ymin);

        line_to(xmax-r, ymin);
        merge(Contour().arc({ r, r }, r, std::numbers::pi/2.0, 0.0));

        line_to(xmax, ymax-r);
        merge(Contour().arc({ r, r }, r, 0, -std::numbers::pi/2.0));

        line_to(xmin+r, ymax);
        merge(Contour().arc({ r, r }, r, 3*std::numbers::pi/2.0, std::numbers::pi));

        line_to(xmin, ymin+r);
        merge(Contour().arc({ r, r }, r, std::numbers::pi, std::numbers::pi/2.0));
    }

    return *this;
}

Contour & Contour::merge(const Contour & ctr) {
    Vector v = curves_.empty() ? start_ : curves_.back().end();
    v -= ctr.start();

    for (Curve cv: ctr.curves_) {
        cv.transform(Matrix(1, 1, 0, 0, v.x(), v.y()));
        curves_.emplace_back(cv);
    }

    return *this;
}

Contour & Contour::arc(const Vector & center, double radius, double angle1, double angle2, const Matrix & mat, double tolerance) {
    radius = std::fabs(radius);
    angle1 = -angle1;
    angle2 = -angle2;
    double major_axis = radius;

    // Borrowed from Cairo Graphics project, see cairo-matrix.c.
    if (!mat.unity()) {
        double a = mat.xx(), b = mat.yx(), c = mat.xy(), d = mat.yy();
        double i = a*a + b*b, j = c*c + d*d;
        double f = 0.5*(i+j), g = 0.5*(i-j);
        double h = a*c + b*d;
        major_axis = radius*std::sqrt(f+std::hypot(g, h));
    }

    double max_angle = arc_max_angle_for_tolerance_normalized(tolerance/major_axis);
    int nseg = std::ceil(std::fabs(angle2-angle1)/max_angle);

    if (nseg > 0) {
        start_.set(center.x()+radius*std::cos(angle1), center.y()+radius*std::sin(angle1));
        double step = (angle2-angle1)/nseg;
        for (; nseg > 1; --nseg, angle1 += step) { arc_segment(*this, center.x(), center.y(), radius, angle1, angle1+step); }
        arc_segment(*this, center.x(), center.y(), radius, angle1, angle2);
    }

    return *this;
}

Vector Contour::start() const {
    return start_;
}

std::vector<Curve> & Contour::curves() {
    return curves_;
}

const std::vector<Curve> & Contour::curves() const {
    return curves_;
}

Curve & Contour::operator[](std::size_t index) {
    return curves_[index];
}

const Curve & Contour::operator[](std::size_t index) const {
    return curves_[index];
}

void Contour::line_to(const Vector & end) {
    curves_.emplace_back(end);
}

void Contour::conic_to(const Vector & cp1, const Vector & end) {
    curves_.emplace_back(cp1, end);
}

void Contour::cubic_to(const Vector & cp1, const Vector & cp2, const Vector & end) {
    curves_.emplace_back(cp1, cp2, end);
}

std::size_t Contour::size() const noexcept {
    return curves_.size();
}

bool Contour::empty() const noexcept {
    return curves_.empty();
}

Contour::iterator Contour::begin() {
    return curves_.begin();
}

Contour::const_iterator Contour::begin() const {
    return curves_.begin();
}

Contour::iterator Contour::end() {
    return curves_.end();
}

Contour::const_iterator Contour::end() const {
    return curves_.end();
}

void Contour::transform(const Matrix & mat) {
    start_ *= mat;
    for (Curve & c: curves_) { c *= mat; }
}

unsigned Contour::order() const {
    unsigned o = 0;
    for (const Curve & c: curves_) { o = std::max(o, c.order()); }
    return o;
}

void Contour::revert() {
    if (!empty()) {
        Vector end = start_;
        std::vector<Curve> cvs;

        for (const Curve & cv: curves_) {
            if (3 == cv.order()) { cvs.emplace(cvs.begin(), cv.cp2(), cv.cp1(), end); }
            else if (2 == cv.order()) { cvs.emplace(cvs.begin(), cv.cp1(), end); }
            else { cvs.emplace(cvs.begin(), end); }
            end = cv.end();
        }

        start_ = end;
        curves_ = std::move(cvs);
    }
}

void Contour::line_to(double x, double y) {
    line_to({ x, y });
}

void Contour::conic_to(double cx, double cy, double ex, double ey) {
    conic_to({ cx, cy }, { ex, ey });
}

void Contour::cubic_to(double cx1, double cy1, double cx2, double cy2, double ex, double ey) {
    cubic_to({ cx1, cy1 }, { cx2, cy2 }, { ex, ey });
}

Contour & operator*=(Contour & ctr, const Matrix & mat) {
    ctr.transform(mat);
    return ctr;
}

} // namespace tau

//END


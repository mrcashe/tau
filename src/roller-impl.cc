// ----------------------------------------------------------------------------
// SPDX-License-Identifier: BSD-2-Clause
// Copyright © 2014-2024 Konstantin Shmelkov <mrcashe@gmail.com>.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
// THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
// PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
// CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
// EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
// PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
// OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
// WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
// OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
// EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// ----------------------------------------------------------------------------

#include <tau/icon.hh>
#include <button-impl.hh>
#include <display-impl.hh>
#include <loop-impl.hh>
#include <roller-impl.hh>
#include <slider-impl.hh>
#include <iostream>

namespace tau {

Roller_impl::Roller_impl(bool autohide):
    autohide_(autohide)
{
    auto table = std::make_shared<Table_impl>();
    table_ = table.get();
    Bin_impl::insert(table);

    auto scroller = std::make_shared<Scroller_impl>();
    scroller_ = scroller.get();
    table_->put(scroller, 0, 0);

    auto slider = std::make_shared<Slider_impl>(scroller.get(), Orientation::SOUTH, autohide);
    vslider_ = slider.get();
    table_->put(slider, Align::CENTER, Align::FILL, 1, 0, 1, 1, true);

    slider = std::make_shared<Slider_impl>(scroller.get(), Orientation::EAST, autohide);
    hslider_ = slider.get();
    table_->put(slider, Align::FILL, Align::CENTER, 0, 1, 1, 1, false, true);
}

Roller_impl::Roller_impl(Orientation orient, bool autohide):
    autohide_(autohide)
{
    horz_ = Orientation::RIGHT == orient || Orientation::LEFT == orient;

    auto box = std::make_shared<Box_impl>(orient, 3);
    box_ = box.get();
    Bin_impl::insert(box);

    auto start = std::make_shared<Button_impl>(tau::Icon::TINY, horz_ ? "go-previous" : "go-up");
    start_ = start.get();
    start_->par_show(!autohide);
    start_->enable_repeat();
    start_->connect(fun(this, &Roller_impl::on_start_button_click));
    start_->signal_mouse_leave().connect(fun(this, &Roller_impl::update_buttons));
    box_->append(start, true);

    auto scroller = std::make_shared<Scroller_impl>();
    scroller_ = scroller.get();
    scroller_->signal_pan_changed().connect(fun(this, &Roller_impl::update_buttons));
    scroller_->signal_size_changed().connect(fun(this, &Roller_impl::update_buttons));
    scroller_->signal_pan_size_changed().connect(fun(this, &Roller_impl::update_buttons));
    box_->append(scroller);

    auto end = std::make_shared<Button_impl>(tau::Icon::TINY, horz_ ? "go-next" : "go-down");
    end_ = end.get();
    end_->enable_repeat();
    end_->connect(fun(this, &Roller_impl::on_end_button_click));
    end_->signal_mouse_leave().connect(fun(this, &Roller_impl::update_buttons));
    end->par_show(!autohide);
    box_->append(end, true);

    signal_mouse_wheel().connect(fun(this, &Roller_impl::on_mouse_wheel), true);
}

void Roller_impl::insert(Widget_ptr wp) {
    scroller()->insert(wp);
}

void Roller_impl::clear() {
    scroller()->clear();
}

bool Roller_impl::empty() const noexcept {
    return scroller()->empty();
}

Container_impl * Roller_impl::compound() noexcept {
    return scroller();
};

const Container_impl * Roller_impl::compound() const noexcept {
    return scroller();
};

int Roller_impl::pan() const noexcept {
    if (box_) {
        Point pt = scroller()->pan();
        return horz_ ? pt.x() : pt.y();
    }

    return 0;
}

void Roller_impl::on_start_button_click() {
    if (horz_) { scroller()->action_pan_left().exec(); }
    else { scroller()->action_pan_up().exec(); }
}

void Roller_impl::on_end_button_click() {
    if (horz_) { scroller()->action_pan_right().exec(); }
    else { scroller()->action_pan_down().exec(); }
}

void Roller_impl::allow_autohide() {
    autohide_ = true;

    if (start_ && end_) {
        update_buttons();
    }

    else if (hslider_ && vslider_) {
        hslider_->allow_autohide();
        vslider_->allow_autohide();
    }
}

void Roller_impl::disallow_autohide() {
    autohide_ = false;

    if (start_ && end_) {
        update_buttons();
    }

    else if (hslider_ && vslider_) {
        hslider_->disallow_autohide();
        vslider_->disallow_autohide();
    }
}

void Roller_impl::set_step(int step) {
    if (box_) {
        scroller()->set_step(horz_ ? Point(step, 0) : Point(0, step));
    }
}

int Roller_impl::step() const noexcept {
    if (box_) {
        Point s = scroller()->step();
        return horz_ ? s.x() : s.y();
    }

    return 0;
}

void Roller_impl::pan(int pos) {
    if (box_) {
        if (horz_) { scroller()->pan_x(pos); }
        else { scroller()->pan_y(pos); }
    }
}

void Roller_impl::pan(Widget_impl * wp) {
    scroller()->pan(wp);
}

void Roller_impl::update_buttons() {
    if (auto dp = display()) {
        if (auto lp = dp->loop()) {
            timer_cx_ = lp->alarm(fun(this, &Roller_impl::on_timer), 21);
        }
    }
}

void Roller_impl::on_timer() {
    if (start_ && end_) {
        if (autohide_) {
            Size max = scroller()->pan_size()-scroller()->size();
            Point pn = scroller()->pan();

            if (horz_) {
                if (!start_->hover()) { start_->par_show(pn.x() > 0); }
                if (!end_->hover()) { end_->par_show(pn.x() < max.iwidth()); }
            }

            else {
                if (!start_->hover()) { start_->par_show(pn.y() > 0); }
                if (!end_->hover()) { end_->par_show(pn.y() < max.iheight()); }
            }
        }

        else {
            start_->show();
            end_->show();
        }
    }
}

bool Roller_impl::on_mouse_wheel(int d, unsigned mods, const Point & pt) {
    if (d > 0) {
        if (horz_) { scroller()->action_pan_right().exec(); }
        else { scroller()->action_pan_down().exec(); }
    }

    else if (d < 0) {
        if (horz_) { scroller()->action_pan_left().exec(); }
        else { scroller()->action_pan_up().exec(); }
    }

    return true;
}

} // namespace tau

//END

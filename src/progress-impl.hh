// ----------------------------------------------------------------------------
// SPDX-License-Identifier: BSD-2-Clause
// Copyright © 2014-2024 Konstantin Shmelkov <mrcashe@gmail.com>.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
// THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
// PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
// CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
// EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
// PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
// OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
// WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
// OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
// EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// ----------------------------------------------------------------------------

#ifndef __TAU_PROGRESS_IMPL_HH__
#define __TAU_PROGRESS_IMPL_HH__

#include <frame-impl.hh>

namespace tau {

class Progress_impl: public Frame_impl {
public:

    Progress_impl(Border bs=Border::INSET, bool vertical=false);

    Border border_style() const noexcept { return border_left_style(); }
    Color border_color() const noexcept { return border_left_color(); }
    unsigned border() const noexcept { return border_left(); }
    void text_align(Align align);
    Align text_align() const noexcept { return text_align_; }
    void set_value(double value);
    double value() const noexcept { return value_; }
    void set_min_value(double min_value);
    double min_value() const noexcept { return min_value_; }
    void set_max_value(double max_value);
    double max_value() const noexcept { return max_value_; }
    void set_precision(int prec);
    int precision() const noexcept { return precision_; }
    void set_format(const ustring & fmt);
    ustring format() const { return fmt_; }

private:

    bool        vertical_;
    double      value_      = 0.0;
    double      min_value_  = 0.0;
    double      max_value_  = 100.0;
    int         precision_  = 0;
    ustring     fmt_;
    ustring     msg_;
    Size        text_size_;
    Widget_ptr  area_;
    Align       text_align_ = Align::CENTER;

private:

    void paint_now();
    void redraw(Painter pr);
    void format_str();
    void calc_hints();

    bool on_paint(Painter pr, const Rect & inval);
    void on_display_in();
};

} // namespace tau

#endif // __TAU_PROGRESS_IMPL_HH__

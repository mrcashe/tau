// ----------------------------------------------------------------------------
// SPDX-License-Identifier: BSD-2-Clause
// Copyright © 2014-2024 Konstantin Shmelkov <mrcashe@gmail.com>.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
// THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
// PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
// CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
// EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
// PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
// OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
// WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
// OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
// EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// ----------------------------------------------------------------------------

/// @file table-impl.hh The Table_impl class declaration.
/// Implementation is in table-impl.cc file.

#ifndef __TAU_TABLE_IMPL_HH__
#define __TAU_TABLE_IMPL_HH__

#include <layered-container.hh>
#include <map>
#include <optional>

namespace tau {

class Table_impl: public Layered_container {
public:

    Table_impl();
    explicit Table_impl(unsigned spacing);
    explicit Table_impl(unsigned xspacing, unsigned yspacing);
    explicit Table_impl(Align xalign, Align yalign);
    explicit Table_impl(Align xalign, Align yalign, unsigned spacing);
    explicit Table_impl(Align xalign, Align yalign, unsigned xspacing, unsigned yspacing);
   ~Table_impl();

    void put(Widget_ptr wp, const Span & span, bool xsh=false, bool ysh=false);

    void put(Widget_ptr wp, Align xalign, Align yalign, const Span & span, bool xsh=false, bool ysh=false) {
        put(wp, span, xsh, ysh);
        align(wp.get(), xalign, yalign);
    }

    void put(Widget_ptr wp, int x=0, int y=0, unsigned xspan=1, unsigned yspan=1, bool xsh=false, bool ysh=false) {
        put(wp, make_span(x, y, xspan, yspan), xsh, ysh);
    }

    void put(Widget_ptr wp, Align xalign, Align yalign=Align::CENTER, int x=0, int y=0, unsigned xspan=1, unsigned yspan=1, bool xsh=false, bool ysh=false) {
        put(wp, x, y, xspan, yspan, xsh, ysh);
        align(wp.get(), xalign, yalign);
    }

    // Redefined by List_impl::remove(Widget_impl *).
    std::size_t remove(Widget_impl * wp) { return remove_priv({ wp }); }
    std::size_t remove(const Span & range);
    std::size_t remove(int x, int y, unsigned xspan=1, unsigned yspan=1) { return remove(make_span(x, y, xspan, yspan)); }

    bool empty() const noexcept { return holders_.empty(); }
    void clear();
    void respan(Widget_impl * wp, const Span & span);
    void respan(Widget_impl * wp, int x, int y, unsigned xspan, unsigned yspan) { respan(wp, make_span(x, y, xspan, yspan)); }
    void respan(Widget_impl * wp, const Span & span, bool xsh, bool ysh);
    void respan(Widget_impl * wp, int x, int y, unsigned xspan, unsigned yspan, bool xsh, bool ysh) { respan(wp, make_span(x, y, xspan, yspan), xsh, ysh); }

    std::list<Widget_ptr> widgets(const Span & rng) const;
    std::list<Widget_ptr> widgets(int x, int y, unsigned xspan, unsigned yspan) const { return widgets(make_span(x, y, xspan, yspan)); }

    void set_column_spacing(unsigned xspacing);
    void set_row_spacing(unsigned yspacing);
    void set_spacing(unsigned xspacing, unsigned yspacing);
    void set_spacing(unsigned spacing);
    std::pair<unsigned, unsigned> spacing() const noexcept { return std::make_pair(xspacing_, yspacing_); }

    void set_column_margin(int x, unsigned left, unsigned right) { set_column_margin(x, { left, right, 0, 0 }); }
    void set_column_margin(int x, const Margin & margin);
    void set_row_margin(int y, unsigned top, unsigned bottom) { set_row_margin(y, { 0, 0, top, bottom }); }
    void set_row_margin(int y, const Margin & margin);
    Margin column_margin(int x) const noexcept;
    Margin row_margin(int y) const noexcept;

    void set_columns_margin(unsigned left, unsigned right) { set_columns_margin({ left, right, 0, 0 }); }
    void set_columns_margin(const Margin & margin);
    void set_rows_margin(unsigned top, unsigned bottom) { set_rows_margin({ 0, 0, top, bottom }); }
    void set_rows_margin(const Margin & margin);
    Margin columns_margin() const noexcept { return margin_; }
    Margin rows_margin() const noexcept { return margin_; }

    void align_columns(Align xalign);
    std::pair<Align, Align> align() const { return std::make_pair(xalign_, yalign_); }

    void align(Align xalign, Align yalign);
    void align_rows(Align yalign);
    void align_column(int x, Align column_align);
    Align column_align(int x) const noexcept;
    void unalign_column(int x);

    void align_row(int y, Align row_align);
    Align row_align(int y) const noexcept;
    void unalign_row(int y);

    void align(Widget_impl * wp, Align xalign, Align yalign=Align::CENTER);
    std::pair<Align, Align> align(const Widget_impl * wp) const;
    void unalign(Widget_impl * wp);

    void insert_columns(int x, unsigned n_columns=1);
    void insert_rows(int y, unsigned n_rows=1);

    void remove_columns(int x, unsigned n_columns=1);
    void remove_rows(int y, unsigned n_rows=1);

    void set_column_width(int column, unsigned width);
    unsigned column_width(int column) const noexcept;

    void set_row_height(int row, unsigned height);
    unsigned row_height(int row) const noexcept;

    void set_min_column_width(int column, unsigned width);
    unsigned min_column_width(int column) const noexcept;

    void set_min_row_height(int row, unsigned height);
    unsigned min_row_height(int row) const noexcept;

    void set_max_column_width(int column, unsigned width);
    unsigned max_column_width(int column) const noexcept;

    void set_max_row_height(int row, unsigned height);
    unsigned max_row_height(int row) const noexcept;

    Span span() const noexcept { return span_; }
    Span span(const Widget_impl * wp) const noexcept;
    Span column_span(int col) const noexcept;
    Span row_span(int row) const noexcept;

    Rect bounds(const Span & span, bool with_margins=false) const noexcept;
    Rect bounds(int x, int y, unsigned xspan, unsigned yspan, bool with_margins=false) const noexcept { return bounds(make_span(x, y, xspan, yspan), with_margins); }

    Rect column_bounds(int col, bool with_margins=false) const noexcept;
    Rect row_bounds(int row, bool with_margins=false) const noexcept;

    void select(const Span & spn);
    void select(int x, int y, unsigned xspan=1, unsigned yspan=1) { select(make_span(x, y, xspan, yspan)); }

    // Redefined by List_impl::unselect().
    void unselect();

    Span selection() const noexcept { return sel_; }

    // Redefined by List_impl::has_selection().
    bool has_selection() const noexcept { return sel_.xmax > sel_.xmin && sel_.ymax >= sel_.ymin; }

    void mark_back(const Color & c, const Span & spn, bool with_margins=false);
    void mark_back(const Color & c, int x, int y, unsigned xspan, unsigned yspan, bool with_margins=false) { mark_back(c, make_span(x, y, xspan, yspan), with_margins); }
    void unmark(const Span & spn);
    void unmark(int x, int y, unsigned xspan, unsigned yspan) { unmark(make_span(x, y, xspan, yspan)); }
    void unmark();
    bool has_marks() const noexcept { return !marks_.empty(); }
    std::vector<Span> marks() const noexcept;

    int column_at_x(int x) const noexcept;
    int row_at_y(int y) const noexcept;

    signal<void(int)> & signal_column_bounds_changed();
    signal<void(int)> & signal_row_bounds_changed();
    signal<void()> & signal_selection_changed() { return signal_selection_changed_; }

    Container_impl * compound() noexcept override { return this; };
    const Container_impl * compound() const noexcept override { return this; };

protected:

    // Overrides Widget_impl.
    bool on_backpaint(Painter pr, const Rect & inval) override;

private:

    using  Opt_align    = std::optional<Align>;
    struct Holder;
    using  Holders      = std::map<Widget_impl *, Holder>;
    using  Hiter        = Holders::iterator;
    using  Hiters       = std::vector<Hiter>;

    struct Holder {
        Widget_ptr      wp_;
        Span            spn_;
        bool            xsh_ : 1;                           // Shrink widget horizontally.
        bool            ysh_ : 1;                           // Shrink widget vertically.
        bool            has_align_ : 1  = false;            // Align was set by user.
        bool            removal_ : 1    = false;            // Marked for removal.
        bool            marked_: 1      = false;            // Already marked by mark_back().
        bool            place_: 1       = true;             // Need to be placed.
        Align           xalign_;                            // Horizontal align.
        Align           yalign_;                            // Vertical align.
        Size            min_;                               // Calculated minimal size.
        Size            max_;                               // Calculated maximal size.
        connection      cx_             { true };           // Connection to the wp_->signal_hints_changed().
    };

    struct Col {
        int             x_              = 0;                // Origin.
        int             lx_             = INT_MIN;          // Last origin.
        unsigned        w_              = 0;                // Width.
        unsigned        lw_             = 0;                // Last width.
        unsigned        left_           = 0;                // User specified left margin.
        unsigned        right_          = 0;                // User specified right margin.
        unsigned        usize_          = 0;                // User specified exact size.
        unsigned        umin_           = 0;                // User specified minimal size.
        unsigned        umax_           = 0;                // User specified maximal size.
        unsigned        rmax_           = 0;                // Maximal requisition.
        unsigned        rmin_           = 0;                // Minimal requisition.
        unsigned        lr_             = 0;                // Last requisition.
        unsigned        shrank_         = 0;                // Shrank widget count.
        unsigned        visible_        = 0;                // Visible widget count.
        Opt_align       xalign_;                            // User specified align.
        mutable int     ymin_           = INT_MAX;          // Column span min (cached).
        mutable int     ymax_           = INT_MIN;          // Column span max (cached).
        Hiters          refs_;
    };

    struct Row {
        int             y_              = 0;                // Origin.
        int             ly_             = INT_MIN;          // Last origin.
        unsigned        h_              = 0;                // Height.
        unsigned        lh_             = 0;                // Last height.
        unsigned        top_            = 0;                // User specified top margin.
        unsigned        bottom_         = 0;                // User specified bottom margin.
        unsigned        usize_          = 0;                // User specified exact size.
        unsigned        umin_           = 0;                // User specified minimal size.
        unsigned        umax_           = 0;                // User specified maximal size.
        unsigned        rmax_           = 0;                // Maximal requisition.
        unsigned        rmin_           = 0;                // Minimal requisition.
        unsigned        lr_             = 0;                // Last requisition.
        unsigned        shrank_         = 0;                // Shrank widget count.
        unsigned        visible_        = 0;                // Visible widget count.
        Opt_align       yalign_;                            // User specified align.
        mutable int     xmin_           = INT_MAX;          // Row span min (cached).
        mutable int     xmax_           = INT_MIN;          // Row span max (cached).
        Hiters          refs_;
    };

    // Mark structure.
    struct Mark {
        Color c;        // Mark color.
        Span spn;       // Mark span.
        bool mg;        // Include margins.
        Mark(const Color & color, const Span & span, bool with_margins): c(color), spn(span), mg(with_margins) {}
    };

    using Cols          = std::map<int, Col>;
    using Rows          = std::map<int, Row>;
    using Col_iter      = Cols::iterator;
    using Col_citer     = Cols::const_iterator;
    using Row_iter      = Rows::iterator;
    using Row_citer     = Rows::const_iterator;
    using Marks         = std::list<Mark>;

    Holders             holders_;
    Cols                cols_;
    Rows                rows_;
    Span                sel_;                               // Current selection.
    Marks               marks_;                             // Marks

    Align               xalign_         = Align::CENTER;    // Overall column alignment.
    Align               yalign_         = Align::CENTER;    // Overall row alignment.

    unsigned            xspacing_       = 0;                // Overall column spacing.
    unsigned            yspacing_       = 0;                // Overall row spacing.
    Margin              margin_;

    unsigned            vis_cols_       = 0;                // Current visible columns count.
    unsigned            vis_rows_       = 0;                // Current visible rows count.
    unsigned            shrank_cols_    = 0;                // Current shrank columns count.
    unsigned            shrank_rows_    = 0;                // Current shrank rows count.
    unsigned            user_cols_      = 0;                // Current columns count having width set by user.
    unsigned            user_rows_      = 0;                // Current rows count having height set by user.
    unsigned            xspc_           = 0;                // Current X space value (calculated).
    unsigned            yspc_           = 0;                // Current Y space value (calculated).

    unsigned            xreq_           = 0;                // Requisition along X axis.
    unsigned            yreq_           = 0;                // Requisition along Y axis.
    unsigned            shrank_xreq_    = 0;                // Shrank columns requisition.
    unsigned            shrank_yreq_    = 0;                // Shrank rows requisition.

    unsigned            xavail_         = 0;                // Available extra space along X axis.
    unsigned            yavail_         = 0;                // Available extra space along Y axis.

    Span                span_;                              // Span.
    connection          once_cx_;                           // Connection to on_display_once().
    signal<void(int)> * signal_column_bounds_changed_       = nullptr;
    signal<void(int)> * signal_row_bounds_changed_          = nullptr;
    signal<void()>      signal_selection_changed_;

private:

    void init();
    void arrange();
    void recalc();
    unsigned req(Col & col) noexcept;
    unsigned req(Row & row) noexcept;
    void wipe(Hiter hi);
    void dist(Hiter hi);
    bool child_requisition(Holder & hol) noexcept;
    bool req_horz(Holder & hol);
    bool req_vert(Holder & hol);
    void alloc_cols();
    void alloc_rows();
    void update_requisition();
    void update_avail() noexcept;

    void place();
    void place(Holder & hol, Rect * inval=nullptr);
    void update_marks();
    std::list<Holder *> holders(const Span & spn);
    Span make_span(int x, int y, unsigned sx, unsigned sy) const noexcept;

    Col_iter new_col(int xx, const Col & src);
    Col_iter new_col(int xx) { return new_col(xx, Col()); }
    Row_iter new_row(int yy, const Row & src);
    Row_iter new_row(int yy) { return new_row(yy, Row()); }

    void unref(Col_iter i, Hiter hi);
    void unref(Row_iter i, Hiter hi);
    void erase(Col_iter i);
    void erase(Row_iter i);
    void trim(Col_iter i);
    void trim(Row_iter i);
    bool erasable(const Col & col) const noexcept;
    bool erasable(const Row & row) const noexcept;

    void show(Col_iter i, Holder & hol);
    void hide(Col_iter i, bool shrank);
    void hide(Col_iter i);

    void show(Row_iter i, Holder & hol);
    void hide(Row_iter i, bool shrank);
    void hide(Row_iter i);

    bool set_column_spacing_i(unsigned xspacing);
    bool set_row_spacing_i(unsigned yspacing);

    std::size_t remove_priv(const std::list<Widget_impl *> & wps);

    void on_size_changed();
    bool on_take_focus();
    void on_select_background();
    void on_display_in();
    void on_display_once();
    void on_hints(Hiter hi, Hints op);
};

} // namespace tau

#endif // __TAU_TABLE_IMPL_HH__

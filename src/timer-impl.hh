// ----------------------------------------------------------------------------
// SPDX-License-Identifier: BSD-2-Clause
// Copyright © 2014-2024 Konstantin Shmelkov <mrcashe@gmail.com>.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
// THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
// PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
// CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
// EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
// PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
// OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
// WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
// OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
// EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// ----------------------------------------------------------------------------

#ifndef __TAU_TIMER_IMPL_HH__
#define __TAU_TIMER_IMPL_HH__

#include <sys-impl.hh>

namespace tau {

struct Timer_impl {
    Timer_impl(int time_ms=0, bool periodical=false):
        time_ms_(time_ms),
        periodical_(periodical)
    {
        ++timers_;
    }

    ~Timer_impl() { --timers_; }

    Timer_impl(const Timer_impl & other) = delete;
    Timer_impl & operator=(const Timer_impl & other) = delete;
    Timer_impl(Timer_impl && other) = delete;
    Timer_impl & operator=(Timer_impl && other) = delete;

    void on_loop_quit() { loop_ = nullptr; quit_cx_.drop(); }

    Loop_impl *     loop_               = nullptr;
    uint64_t        time_point_         = 0;
    int             time_ms_            = 0;
    int             rem_                = 0;        // Remaining time while paused.
    bool            periodical_: 1      = false;
    bool            running_ : 1        = false;
    bool            pause_ : 1          = false;
    bool            timer_ : 1          = false;    // Created by Timer.
    signal<void()>  signal_alarm_;
    connection      cx_;
    connection      quit_cx_            { true };
};

} // namespace tau

#endif // __TAU_TIMER_IMPL_HH__

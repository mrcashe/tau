// ----------------------------------------------------------------------------
// SPDX-License-Identifier: BSD-2-Clause
// Copyright © 2014-2024 Konstantin Shmelkov <mrcashe@gmail.com>.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
// THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
// PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
// CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
// EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
// PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
// OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
// WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
// OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
// EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// ----------------------------------------------------------------------------

#ifndef __TAU_MENUBAR_IMPL_HH__
#define __TAU_MENUBAR_IMPL_HH__

#include <box-impl.hh>
#include <menu-impl.hh>

namespace tau {

class Menubar_impl: public Menu_impl {
public:

    Menubar_impl();

    // Overrides pure Menu_impl.
    void append(Widget_ptr wp, bool shrink=false) override;

    // Overrides pure Menu_impl.
    void prepend(Widget_ptr wp, bool shrink=false) override;

    // Overrides pure Menu_impl.
    void insert_before(Widget_ptr wp, const Widget_impl * other, bool shrink=false) override;

    // Overrides pure Menu_impl.
    void insert_after(Widget_ptr wp, const Widget_impl * other, bool shrink=false) override;

    // Overrides Menu_impl.
    void remove(Widget_impl * wp) override;

    // Overrides Menu_impl.
    void clear() override;

    // Overrides pure Menu_impl.
    bool empty() const noexcept override { return box_->empty(); }

    // Overrides pure Menu_impl.
    void child_menu_cancel() override;

    // Overrides pure Menu_impl.
    void child_menu_left() override;

    // Overrides pure Menu_impl.
    void child_menu_right() override;

    bool activate();

    Container_impl * compound() noexcept override { return box_; };
    const Container_impl * compound() const noexcept override { return box_; };

protected:

    // Overrides pure Menu_impl.
    void mark_item(Widget_impl * wp, bool select) override;

private:

    Box_impl *  box_;
    Action      action_left_ {"Left", fun(this, &Menubar_impl::on_left) };
    Action      action_right_ {"Right", fun(this, &Menubar_impl::on_right) };

private:

    void on_left();
    void on_right();
    void on_mouse_leave();
    bool on_mouse_down(int mbt, int mm, const Point & pt);
};

} // namespace tau

#endif // __TAU_MENUBAR_IMPL_HH__

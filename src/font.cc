// ----------------------------------------------------------------------------
// SPDX-License-Identifier: BSD-2-Clause
// Copyright © 2014-2024 Konstantin Shmelkov <mrcashe@gmail.com>.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
// THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
// PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
// CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
// EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
// PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
// OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
// WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
// OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
// EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// ----------------------------------------------------------------------------

#include <tau/font.hh>
#include <tau/glyph.hh>
#include <tau/locale.hh>
#include <tau/string.hh>
#include <font-impl.hh>
#include <display-impl.hh>
#include <gettext-impl.hh>
#include <algorithm>
#include <atomic>
#include <cstring>
#include <iostream>     // cerr
#include <sstream>

namespace {

// Indicates that Display_impl::signal_open() already connected.
std::atomic_flag    connected_;

const std::vector<tau::ustring> faces_  = {
    gettext_noop("Regular"),        gettext_noop("Normal"),     gettext_noop("Book"),       gettext_noop("Italic"),
    gettext_noop("Oblique"),        gettext_noop("Roman"),      gettext_noop("Medium"),     gettext_noop("Thin"),
    gettext_noop("Bold"),           gettext_noop("ExtraLight"), gettext_noop("Light"),      gettext_noop("Heavy"),
    gettext_noop("Plain"),          gettext_noop("Initials"),   gettext_noop("Demi"),       gettext_noop("Condensed"),
    gettext_noop("SemiBold"),       gettext_noop("ExtraBold"),  gettext_noop("BoldItalic"), gettext_noop("BoldOblique"),
    gettext_noop("LightOblique"),   gettext_noop("Mono")
};

const std::string digits_ = ".,-+=0123456789eE";

tau::ustring        normal_;
tau::ustring        mono_;

void on_display_open(tau::Display_impl * dp) {
    normal_ = dp->font_normal();
    mono_ = dp->font_mono();
}

void check_open() {
    if (!connected_.test_and_set()) {
        tau::Display_impl::signal_open().connect(fun(on_display_open));
    }
}

bool is_size(const std::string & s) {
    return s.npos == s.find_first_not_of(digits_);
}

bool is_face(const std::string & s) {
    std::size_t n = 0;
    for (n = 0; '=' == s[n]; ++n) {}
    auto sn = s.substr(n);
    return std::any_of(faces_.begin(), faces_.end(), [&sn](auto & s) { return s.size() == sn.size() && tau::ustring::npos != str_like(s, sn); });
}

void log(const char * s) {
    std::cerr << "tau::Font::" << s << "() called on empty Font" << std::endl;
}

} // anonymous namespace

namespace tau {

Font::Font() {}

Font::~Font() {}

Font::Font(Font_ptr fp):
    impl(fp)
{
}

bool Font::operator==(const Font & other) const noexcept {
    return impl && other.impl && impl.get() == other.impl.get();
}

bool Font::operator!=(const Font & other) const noexcept {
    return !impl || !other.impl || impl.get() != other.impl.get();
}

Font::operator bool() const noexcept {
    return nullptr != impl;
}

void Font::reset() {
    impl.reset();
}

Font_ptr Font::ptr() {
    return impl;
}

Font_cptr Font::ptr() const {
    return impl;
}

ustring Font::spec() const noexcept {
    if (impl) { return impl->spec_; }
    else { log(__func__); return ustring(); }
}

ustring Font::psname() const noexcept {
    if (impl) { return impl->psname(); }
    else { log(__func__); return ustring(); }
}

unsigned Font::dpi() const noexcept {
    if (impl) { return impl->dpi_; }
    else { log(__func__); return 0; }
}

double Font::ascent() const noexcept {
    if (impl) { return impl->ascent_; }
    else { log(__func__); return 0.0; }
}

int Font::iascent() const noexcept {
    if (impl) { return impl->iascent(); }
    else { log(__func__); return 0; }
}

double Font::descent() const noexcept {
    if (impl) { return impl->descent_; }
    else { log(__func__); return 0.0; }
}

int Font::idescent() const noexcept {
    if (impl) { return impl->idescent(); }
    else { log(__func__); return 0; }
}

double Font::linegap() const noexcept {
    if (impl) { return impl->linegap_; }
    else { log(__func__); return 0.0; }
}

Vector Font::min() const noexcept {
    if (impl) { return impl->min_; }
    else { log(__func__); return Vector(); }
}

Vector Font::max() const noexcept {
    if (impl) { return impl->max_; }
    else { log(__func__); return Vector(); }
}

Glyph Font::glyph(char32_t wc) const {
    if (impl) { return impl->glyph(wc); }
    else { log(__func__); return Glyph(); }
}

Glyph Font::master_glyph(char32_t wc) const {
    if (impl) { return impl->master_glyph(wc); }
    else { log(__func__); return Glyph(); }
}

bool Font::monospace() const {
    if (impl) { return impl->monospace(); }
    else { log(__func__); return false; }
}

unsigned Font::char_width() const {
    if (impl) { return impl->char_width(); }
    else { log(__func__); return 0; }
}

unsigned Font::height() const noexcept {
    if (impl) { return impl->height(); }
    else { log(__func__); return 0; }
}

// static
ustring Font::normal() {
    check_open();
    if (normal_.empty() && Display_impl::this_running()) { normal_ = Display_impl::this_ptr()->font_normal(); }
    return normal_;
}

// static
ustring Font::mono() {
    check_open();
    if (mono_.empty() && Display_impl::this_running()) { mono_ = Display_impl::this_ptr()->font_mono(); }
    return mono_;
}

// static
std::vector<ustring> Font::list_families() {
    return Display_impl::this_running() ? Display_impl::this_ptr()->list_families() : std::vector<ustring>();
}

// static
void Font::list_families_async(slot<void(ustring)> slot_next) {
    if (Display_impl::this_running()) {
        Display_impl::this_ptr()->list_families_async(slot_next);
    }
}

// static
std::vector<ustring> Font::list_faces(const ustring & family) {
    return Display_impl::this_running() ? Display_impl::this_ptr()->list_faces(family) : std::vector<ustring>();
}

// static
bool Font::same(const ustring & spec1, const ustring & spec2) noexcept {
    auto v1 = Font::explode(spec1), v2 = Font::explode(spec2);
    return std::equal(v1.begin(), v1.end(), v2.begin(), v2.end(), [](auto & p1, auto & p2) { return str_similar(p1, p2); } );

}

// static
std::vector<ustring> Font::explode(const ustring & spec) {
    return str_explode(str_trim(spec), ' ');
}

// static
ustring Font::family(const std::vector<ustring> & specv) {
    std::vector<ustring> rv;
    std::copy_if(specv.begin(), specv.end(), std::back_inserter(rv), [](const ustring & s) { return !is_size(s) && !is_face(s); } );
    return str_implode(rv, ' ');
}

// static
ustring Font::family(const ustring & spec) {
    return family(explode(spec));
}

// static
ustring Font::face(const std::vector<ustring> & specv) {
    std::vector<ustring> rv;
    bool mono = false;
    std::size_t pos;

    for (const ustring & s: specv) {
        if (is_face(s)) {
            for (pos = 0; pos < s.size() && U'=' == s[pos]; ++pos) {}
            if (str_similar("Mono", s.substr(pos))) { mono = true; }
        }
    }

    if (mono) { rv.push_back("Mono"); }

    for (const ustring & s: specv) {
        if (is_face(s)) {
            for (pos = 0; pos < s.size() && U'=' == s[pos]; ++pos) {}
            ustring t(s.substr(pos));
            if (!str_similar(t, "Mono Regular", U' ') && !str_similar(t, rv)) { rv.push_back(t); }
        }
    }

    return rv.empty() ? faces_[0] : str_implode(rv, ' ');
}

// static
ustring Font::face(const ustring & spec) {
    return face(explode(spec));
}

// static
ustring Font::label(const ustring & spec) {
    return label(explode(spec));
}

// static
ustring Font::label(const std::vector<ustring> & specv) {
    std::vector<ustring> v = specv;
    std::transform(v.begin(), v.end(), v.begin(), [](auto s) { return lgettext(s); });
    return str_implode(v, ' ');
}

// static
ustring Font::build(const std::vector<ustring> & specv) {
    double pts = points(specv);
    ustring fc = face(specv);
    if (fc == faces_[0]) { fc.clear(); }
    else { fc.insert(0, 1, ' '); }
    return family(specv)+fc+(pts >= 0.1 ? str_format(' ', pts) : "");
}

// static
ustring Font::build(const ustring & family, const ustring & fc, double pt) {
    std::vector<ustring> v;
    v.emplace_back(family);
    for (auto & s: str_explode(face(fc))) { v.emplace_back(s); }
    if (pt >= 0) { v.emplace_back(str_format(pt)); }
    return build(v);
}

// static
bool Font::has_face(const ustring & spec, const ustring & face_element) {
    return str_similar(face_element, str_explode(face(spec), Locale().blanks()));
}

// static
ustring Font::set_face(const ustring & spec, const ustring & fc) {
    double pt = points(spec);
    return build(family(spec), fc, pt);
}

// static
ustring Font::add_face(const ustring & spec, const ustring & faces) {
    auto specv = explode(spec);
    for (auto & s: str_explode(faces)) { specv.emplace_back(s); }
    return build(specv);
}

// static
ustring Font::make_bold(const ustring & spec) {
    return has_face(spec, "Bold") ? spec : add_face(spec, "Bold");
}

// static
ustring Font::make_italic(const ustring & spec) {
    return has_face(spec, "Italic") || has_face(spec, "Oblique") ? spec : add_face(spec, "Italic");
}

// static
double Font::points(const std::vector<ustring> & specv, double fallback) {
    static const char * signs = "+-=";
    double pts = 0.0;

    for (std::string s: specv) {
        if (is_size(s)) {
            std::size_t pos, rpos = 0, len = s.size();
            for (pos = s.find(','); pos < len; pos = s.find(',')) { s[pos] = '.'; }
            pos = std::strchr(signs, s[0]) ? 1 : 0;

            while (pos < len) {
                char sgn = s[rpos];
                std::size_t sign = s.find_first_of(signs, pos), end = std::min(sign, len);
                if ('=' == sgn) { ++rpos; }
                double d; std::istringstream is(s.substr(rpos, end-rpos)); is.imbue(std::locale::classic()); is >> d;
                if ('=' == sgn) { return d > 0.0 ? d : fallback; }
                pts += d;
                pos = end+1;
                rpos = sign;
            }
        }
    }

    return pts > 0.0 ? pts : fallback;
}

// static
double Font::points(const ustring & spec, double fallback) {
    return points(explode(spec), fallback);
}

// static
ustring Font::without_points(const ustring & spec) {
    auto v = explode(spec);
    return build(family(v), face(v));
}

// static
ustring Font::resize(const ustring & spec, double pts) {
    auto v = explode(spec);
    return build(family(v), face(v), pts);
}

// static
ustring Font::enlarge(const ustring & spec, double delta) {
    auto v = explode(spec);
    double pts = points(v);
    return pts >= 0.01 ? build(family(v), face(v), pts+delta) : spec;
}

// static
ustring Font::at_least(const ustring & spec, double min_size_pt) {
    double pts = points(spec);
    return pts < min_size_pt ? resize(spec, min_size_pt) : spec;
}

// static
ustring Font::as_max(const ustring & font_spec, double max_size_pt) {
    double pts = points(font_spec);
    return pts > max_size_pt ? resize(font_spec, max_size_pt) : font_spec;
}

} // namespace tau

//END

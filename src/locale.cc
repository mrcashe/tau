// ----------------------------------------------------------------------------
// SPDX-License-Identifier: BSD-2-Clause
// Copyright © 2014-2024 Konstantin Shmelkov <mrcashe@gmail.com>.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
// THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
// PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
// CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
// EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
// PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
// OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
// WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
// OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
// EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// ----------------------------------------------------------------------------

#include <locale-impl.hh>
#include <gettext-impl.hh>
#include <sys-impl.hh>
#include <tau/encoding.hh>
#include <tau/exception.hh>
#include <iostream>
#include <locale>

namespace {

const Hard_locale hard_[] = {
    {
        .code                   = "C",
        .blanks                 = "\u0009\u0020\u00A0\u2000\u2001\u2002\u2003\u2004\u2005\u2006\u2007\u2008\u2009\u200a\u200b\u202F\u205f\u2060\ufeff",
        .newlines               = "\u000A\u000D\u2028\u2029",
        .delimiters             = "\u0009\u000A\u000D\u0020\u00A0\u2007\u2028\u2029\u202F\u2060.,:;!?-+=|\\/<>(){}[]~`\"@#$%^&*",
        .int_curr_symbol        = "",
        .currency_symbol        = "",
        .mon_decimal_point      = "\u002e",
        .mon_thousands_sep      = "",
        .mon_grouping           = "",
        .positive_sign          = "",
        .negative_sign          = "",
        .int_frac_digits        = -1,
        .frac_digits            = -1,
        .p_cs_precedes          = -1,
        .p_sep_by_space         = -1,
        .n_cs_precedes          = -1,
        .n_sep_by_space         = -1,
        .p_sign_posn            = -1,
        .n_sign_posn            = -1,
        .decimal_point          = "\u002E",
        .thousands_sep          = "",
        .grouping               = "",
        .abday                  = "\u0053\u0075\u006E:"
                                  "\u004D\u006F\u006E:"
                                  "\u0054\u0075\u0065:"
                                  "\u0057\u0065\u0064:"
                                  "\u0054\u0068\u0075:"
                                  "\u0046\u0072\u0069:"
                                  "\u0053\u0061\u0074",
        .day                    = "\u0053\u0075\u006E\u0064\u0061\u0079:"
                                  "\u004D\u006F\u006E\u0064\u0061\u0079:"
                                  "\u0054\u0075\u0065\u0073\u0064\u0061\u0079:"
                                  "\u0057\u0065\u0064\u006E\u0065\u0073\u0064\u0061\u0079:"
                                  "\u0054\u0068\u0075\u0072\u0073\u0064\u0061\u0079:"
                                  "\u0046\u0072\u0069\u0064\u0061\u0079:"
                                  "\u0053\u0061\u0074\u0075\u0072\u0064\u0061\u0079",
        .abmon                  = "\u004A\u0061\u006E:"
                                  "\u0046\u0065\u0062:"
                                  "\u004D\u0061\u0072:"
                                  "\u0041\u0070\u0072:"
                                  "\u004D\u0061\u0079:"
                                  "\u004A\u0075\u006E:"
                                  "\u004A\u0075\u006C:"
                                  "\u0041\u0075\u0067:"
                                  "\u0053\u0065\u0070:"
                                  "\u004F\u0063\u0074:"
                                  "\u004E\u006F\u0076:"
                                  "\u0044\u0065\u0063",
        .mon                    = "\u004A\u0061\u006E\u0075\u0061\u0072\u0079:"
                                  "\u0046\u0065\u0062\u0072\u0075\u0061\u0072\u0079:"
                                  "\u004D\u0061\u0072\u0063\u0068:"
                                  "\u0041\u0070\u0072\u0069\u006C:"
                                  "\u004D\u0061\u0079:"
                                  "\u004A\u0075\u006E\u0065:"
                                  "\u004A\u0075\u006C\u0079:"
                                  "\u0041\u0075\u0067\u0075\u0073\u0074:"
                                  "\u0053\u0065\u0070\u0074\u0065\u006D\u0062\u0065\u0072:"
                                  "\u004F\u0063\u0074\u006F\u0062\u0065\u0072:"
                                  "\u004E\u006F\u0076\u0065\u006D\u0062\u0065\u0072:"
                                  "\u0044\u0065\u0063\u0065\u006D\u0062\u0065\u0072",
        .d_t_fmt                = "\u0025\u0061\u0020\u0025\u0062\u0020\u0025\u0065\u0020\u0025\u0048\u003A\u0025\u004D\u003A\u0025\u0053\u0020\u0025\u0059",
        .d_fmt                  = "\u0025\u006D\u002F\u0025\u0064\u002F\u0025\u0079",
        .t_fmt                  = "\u0025\u0048\u003A\u0025\u004D\u003A\u0025\u0053",
        .am_pm                  = "\u0041\u004D:\u0050\u004D",
        .t_fmt_ampm             = "\u0025\u0049\u003A\u0025\u004D\u003A\u0025\u0053\u0020\u0025\u0070",
        .date_fmt               = "\u0025\u0061\u0020\u0025\u0062\u0020\u0025\u0065\u0020\u0025\u0048\u003A\u0025\u004D\u003A\u0025\u0053\u0020\u0025\u005A\u0020\u0025\u0059",
        .first_weekday          = 2
    },
    {
        .code                   = "en_US",
        .int_curr_symbol        = "\u0055\u0053\u0044\u0020",
        .currency_symbol        = "\u0024",
        .mon_decimal_point      = "\u002e",
        .mon_thousands_sep      = "\u002c",
        .mon_grouping           = "3:3",
        .positive_sign          = nullptr,
        .negative_sign          = "\u002d",
        .int_frac_digits        = 2,
        .frac_digits            = 2,
        .p_cs_precedes          = 1,
        .p_sep_by_space         = 0,
        .n_cs_precedes          = 1,
        .n_sep_by_space         = 0,
        .p_sign_posn            = 1,
        .n_sign_posn            = 1,
        .decimal_point          = "\u002E",
        .thousands_sep          = "\u002C",
        .grouping               = "3:3",
        .abday                  = "\u0053\u0075\u006E:"
                                  "\u004D\u006F\u006E:"
                                  "\u0054\u0075\u0065:"
                                  "\u0057\u0065\u0064:"
                                  "\u0054\u0068\u0075:"
                                  "\u0046\u0072\u0069:"
                                  "\u0053\u0061\u0074",
        .day                    = "\u0053\u0075\u006E\u0064\u0061\u0079:"
                                  "\u004D\u006F\u006E\u0064\u0061\u0079:"
                                  "\u0054\u0075\u0065\u0073\u0064\u0061\u0079:"
                                  "\u0057\u0065\u0064\u006E\u0065\u0073\u0064\u0061\u0079:"
                                  "\u0054\u0068\u0075\u0072\u0073\u0064\u0061\u0079:"
                                  "\u0046\u0072\u0069\u0064\u0061\u0079:"
                                  "\u0053\u0061\u0074\u0075\u0072\u0064\u0061\u0079",
        .abmon                  = "\u004A\u0061\u006E:"
                                  "\u0046\u0065\u0062:"
                                  "\u004D\u0061\u0072:"
                                  "\u0041\u0070\u0072:"
                                  "\u004D\u0061\u0079:"
                                  "\u004A\u0075\u006E:"
                                  "\u004A\u0075\u006C:"
                                  "\u0041\u0075\u0067:"
                                  "\u0053\u0065\u0070:"
                                  "\u004F\u0063\u0074:"
                                  "\u004E\u006F\u0076:"
                                  "\u0044\u0065\u0063",
        .mon                    = "\u004A\u0061\u006E\u0075\u0061\u0072\u0079:"
                                  "\u0046\u0065\u0062\u0072\u0075\u0061\u0072\u0079:"
                                  "\u004D\u0061\u0072\u0063\u0068:"
                                  "\u0041\u0070\u0072\u0069\u006C:"
                                  "\u004D\u0061\u0079:"
                                  "\u004A\u0075\u006E\u0065:"
                                  "\u004A\u0075\u006C\u0079:"
                                  "\u0041\u0075\u0067\u0075\u0073\u0074:"
                                  "\u0053\u0065\u0070\u0074\u0065\u006D\u0062\u0065\u0072:"
                                  "\u004F\u0063\u0074\u006F\u0062\u0065\u0072:"
                                  "\u004E\u006F\u0076\u0065\u006D\u0062\u0065\u0072:"
                                  "\u0044\u0065\u0063\u0065\u006D\u0062\u0065\u0072",
        .d_t_fmt                = "\u0025\u0061\u0020\u0025\u0064\u0020\u0025\u0062\u0020\u0025\u0059\u0020\u0025\u0072\u0020\u0025\u005A",
        .d_fmt                  = "\u0025\u006D\u002F\u0025\u0064\u002F\u0025\u0059",
        .t_fmt                  = "\u0025\u0072",
        .am_pm                  = "\u0041\u004D:\u0050\u004D",
        .t_fmt_ampm             = "\u0025\u0049\u003A\u0025\u004D\u003A\u0025\u0053\u0020\u0025\u0070",
        .date_fmt               = "\u0025\u0061\u0020\u0025\u0065\u0020\u0025\u0062"
                                  "\u0020\u0025\u0048\u003A\u0025\u004D\u003A\u0025\u0053\u0020"
                                  "\u0025\u005A\u0020\u0025\u0059",
        .first_weekday          = 2
    },
    {
        .code                   = "en_AU",
        .int_curr_symbol        = "\u0041\u0055\u0044\u0020",
        .currency_symbol        = "\u0024",
        .mon_decimal_point      = "\u002e",
        .mon_thousands_sep      = "\u002c",
        .mon_grouping           = "3:3",
        .positive_sign          = "",
        .negative_sign          = "\u002d",
        .int_frac_digits        = 2,
        .frac_digits            = 2,
        .p_cs_precedes          = 1,
        .p_sep_by_space         = 0,
        .n_cs_precedes          = 1,
        .n_sep_by_space         = 0,
        .p_sign_posn            = 1,
        .n_sign_posn            = 1,
        .decimal_point          = "\u002E",
        .thousands_sep          = "\u002C",
        .grouping               = "3:3",
        .abday                  = "\u0053\u0075\u006E:"
                                  "\u004D\u006F\u006E:"
                                  "\u0054\u0075\u0065:"
                                  "\u0057\u0065\u0064:"
                                  "\u0054\u0068\u0075:"
                                  "\u0046\u0072\u0069:"
                                  "\u0053\u0061\u0074",
        .day                    = "\u0053\u0075\u006E\u0064\u0061\u0079:"
                                  "\u004D\u006F\u006E\u0064\u0061\u0079:"
                                  "\u0054\u0075\u0065\u0073\u0064\u0061\u0079:"
                                  "\u0057\u0065\u0064\u006E\u0065\u0073\u0064\u0061\u0079:"
                                  "\u0054\u0068\u0075\u0072\u0073\u0064\u0061\u0079:"
                                  "\u0046\u0072\u0069\u0064\u0061\u0079:"
                                  "\u0053\u0061\u0074\u0075\u0072\u0064\u0061\u0079",
        .abmon                  = "\u004A\u0061\u006E:"
                                  "\u0046\u0065\u0062:"
                                  "\u004D\u0061\u0072:"
                                  "\u0041\u0070\u0072:"
                                  "\u004D\u0061\u0079:"
                                  "\u004A\u0075\u006E:"
                                  "\u004A\u0075\u006C:"
                                  "\u0041\u0075\u0067:"
                                  "\u0053\u0065\u0070:"
                                  "\u004F\u0063\u0074:"
                                  "\u004E\u006F\u0076:"
                                  "\u0044\u0065\u0063",
        .mon                    = "\u004A\u0061\u006E\u0075\u0061\u0072\u0079:"
                                  "\u0046\u0065\u0062\u0072\u0075\u0061\u0072\u0079:"
                                  "\u004D\u0061\u0072\u0063\u0068:"
                                  "\u0041\u0070\u0072\u0069\u006C:"
                                  "\u004D\u0061\u0079:"
                                  "\u004A\u0075\u006E\u0065:"
                                  "\u004A\u0075\u006C\u0079:"
                                  "\u0041\u0075\u0067\u0075\u0073\u0074:"
                                  "\u0053\u0065\u0070\u0074\u0065\u006D\u0062\u0065\u0072:"
                                  "\u004F\u0063\u0074\u006F\u0062\u0065\u0072:"
                                  "\u004E\u006F\u0076\u0065\u006D\u0062\u0065\u0072:"
                                  "\u0044\u0065\u0063\u0065\u006D\u0062\u0065\u0072",
        .d_t_fmt                = "\u0025\u0061\u0020\u0025\u0064\u0020\u0025\u0062\u0020\u0025\u0059\u0020\u0025\u0054\u0020\u0025\u005A",
        .d_fmt                  = "\u0025\u0064\u002F\u0025\u006D\u002F\u0025\u0079",
        .t_fmt                  = "\u0025\u0054",
        .am_pm                  = "\u0041\u004D:\u0050\u004D",
        .t_fmt_ampm             = "\u0025\u0049\u003A\u0025\u004D\u003A\u0025\u0053\u0020",
        .date_fmt               = "\u0025\u0061\u0020\u0025\u0065\u0020\u0025\u0062"
                                  "\u0020\u0025\u0048\u003A\u0025\u004D\u003A\u0025\u0053\u0020"
                                  "\u0025\u005A\u0020\u0025\u0059",
        .first_weekday          = 2
    },
    {
        .code                   = "en_CA",
        .int_curr_symbol        = "\u0043\u0041\u0044\u0020",
        .currency_symbol        = "\u0024",
        .mon_decimal_point      = "\u002e",
        .mon_thousands_sep      = "\u002c",
        .mon_grouping           = "3:3",
        .positive_sign          = "",
        .negative_sign          = "\u002d",
        .int_frac_digits        = 2,
        .frac_digits            = 2,
        .p_cs_precedes          = 1,
        .p_sep_by_space         = 0,
        .n_cs_precedes          = 1,
        .n_sep_by_space         = 0,
        .p_sign_posn            = 1,
        .n_sign_posn            = 1,
        .decimal_point          = "\u002E",
        .thousands_sep          = "\u002C",
        .grouping               = "3:3",
        .abday                  = "\u0053\u0075\u006E:"
                                  "\u004D\u006F\u006E:"
                                  "\u0054\u0075\u0065:"
                                  "\u0057\u0065\u0064:"
                                  "\u0054\u0068\u0075:"
                                  "\u0046\u0072\u0069:"
                                  "\u0053\u0061\u0074",
        .day                    = "\u0053\u0075\u006E\u0064\u0061\u0079:"
                                  "\u004D\u006F\u006E\u0064\u0061\u0079:"
                                  "\u0054\u0075\u0065\u0073\u0064\u0061\u0079:"
                                  "\u0057\u0065\u0064\u006E\u0065\u0073\u0064\u0061\u0079:"
                                  "\u0054\u0068\u0075\u0072\u0073\u0064\u0061\u0079:"
                                  "\u0046\u0072\u0069\u0064\u0061\u0079:"
                                  "\u0053\u0061\u0074\u0075\u0072\u0064\u0061\u0079",
        .abmon                  = "\u004A\u0061\u006E:"
                                  "\u0046\u0065\u0062:"
                                  "\u004D\u0061\u0072:"
                                  "\u0041\u0070\u0072:"
                                  "\u004D\u0061\u0079:"
                                  "\u004A\u0075\u006E:"
                                  "\u004A\u0075\u006C:"
                                  "\u0041\u0075\u0067:"
                                  "\u0053\u0065\u0070:"
                                  "\u004F\u0063\u0074:"
                                  "\u004E\u006F\u0076:"
                                  "\u0044\u0065\u0063",
        .mon                    = "\u004A\u0061\u006E\u0075\u0061\u0072\u0079:"
                                  "\u0046\u0065\u0062\u0072\u0075\u0061\u0072\u0079:"
                                  "\u004D\u0061\u0072\u0063\u0068:"
                                  "\u0041\u0070\u0072\u0069\u006C:"
                                  "\u004D\u0061\u0079:"
                                  "\u004A\u0075\u006E\u0065:"
                                  "\u004A\u0075\u006C\u0079:"
                                  "\u0041\u0075\u0067\u0075\u0073\u0074:"
                                  "\u0053\u0065\u0070\u0074\u0065\u006D\u0062\u0065\u0072:"
                                  "\u004F\u0063\u0074\u006F\u0062\u0065\u0072:"
                                  "\u004E\u006F\u0076\u0065\u006D\u0062\u0065\u0072:"
                                  "\u0044\u0065\u0063\u0065\u006D\u0062\u0065\u0072",
        .d_t_fmt                = "\u0025\u0061\u0020\u0025\u0064\u0020\u0025\u0062\u0020\u0025\u0059\u0020\u0025\u0072\u0020\u0025\u005A",
        .d_fmt                  = "\u0025\u0059\u002D\u0025\u006D\u002D\u0025\u0064",
        .t_fmt                  = "\u0025\u0072",
        .am_pm                  = "\u0041\u004D:\u0050\u004D",
        .t_fmt_ampm             = "\u0025\u0049\u003A\u0025\u004D\u003A\u0025\u0053\u0020\u0025\u0070",
        .date_fmt               = "\u0025\u0061\u0020\u0025\u0065\u0020\u0025\u0062"
                                  "\u0020\u0025\u0048\u003A\u0025\u004D\u003A\u0025\u0053\u0020"
                                  "\u0025\u005A\u0020\u0025\u0059",
        .first_weekday          = 2
    },
    {
        .code                   = "en_GB",
        .int_curr_symbol        = "\u0047\u0042\u0050\u0020",
        .currency_symbol        = "\u00a3",
        .mon_decimal_point      = "\u002e",
        .mon_thousands_sep      = "\u002c",
        .mon_grouping           = "3:3",
        .positive_sign          = "",
        .negative_sign          = "\u002d",
        .int_frac_digits        = 2,
        .frac_digits            = 2,
        .p_cs_precedes          = 1,
        .p_sep_by_space         = 0,
        .n_cs_precedes          = 1,
        .n_sep_by_space         = 0,
        .p_sign_posn            = 1,
        .n_sign_posn            = 1,
        .decimal_point          = "\u002E",
        .thousands_sep          = "\u002C",
        .grouping               = "3:3",
        .abday                  = "\u0053\u0075\u006E:"
                                  "\u004D\u006F\u006E:"
                                  "\u0054\u0075\u0065:"
                                  "\u0057\u0065\u0064:"
                                  "\u0054\u0068\u0075:"
                                  "\u0046\u0072\u0069:"
                                  "\u0053\u0061\u0074",
        .day                    = "\u0053\u0075\u006E\u0064\u0061\u0079:"
                                  "\u004D\u006F\u006E\u0064\u0061\u0079:"
                                  "\u0054\u0075\u0065\u0073\u0064\u0061\u0079:"
                                  "\u0057\u0065\u0064\u006E\u0065\u0073\u0064\u0061\u0079:"
                                  "\u0054\u0068\u0075\u0072\u0073\u0064\u0061\u0079:"
                                  "\u0046\u0072\u0069\u0064\u0061\u0079:"
                                  "\u0053\u0061\u0074\u0075\u0072\u0064\u0061\u0079",
        .abmon                  = "\u004A\u0061\u006E:"
                                  "\u0046\u0065\u0062:"
                                  "\u004D\u0061\u0072:"
                                  "\u0041\u0070\u0072:"
                                  "\u004D\u0061\u0079:"
                                  "\u004A\u0075\u006E:"
                                  "\u004A\u0075\u006C:"
                                  "\u0041\u0075\u0067:"
                                  "\u0053\u0065\u0070:"
                                  "\u004F\u0063\u0074:"
                                  "\u004E\u006F\u0076:"
                                  "\u0044\u0065\u0063",
        .mon                    = "\u004A\u0061\u006E\u0075\u0061\u0072\u0079:"
                                  "\u0046\u0065\u0062\u0072\u0075\u0061\u0072\u0079:"
                                  "\u004D\u0061\u0072\u0063\u0068:"
                                  "\u0041\u0070\u0072\u0069\u006C:"
                                  "\u004D\u0061\u0079:"
                                  "\u004A\u0075\u006E\u0065:"
                                  "\u004A\u0075\u006C\u0079:"
                                  "\u0041\u0075\u0067\u0075\u0073\u0074:"
                                  "\u0053\u0065\u0070\u0074\u0065\u006D\u0062\u0065\u0072:"
                                  "\u004F\u0063\u0074\u006F\u0062\u0065\u0072:"
                                  "\u004E\u006F\u0076\u0065\u006D\u0062\u0065\u0072:"
                                  "\u0044\u0065\u0063\u0065\u006D\u0062\u0065\u0072",
        .d_t_fmt                = "\u0025\u0061\u0020\u0025\u0064\u0020\u0025\u0062\u0020\u0025\u0059\u0020\u0025\u0054\u0020\u0025\u005A",
        .d_fmt                  = "\u0025\u0064\u002F\u0025\u006D\u002F\u0025\u0079",
        .t_fmt                  = "\u0025\u0054",
        .am_pm                  = "\u0061\u006D:\u0070\u006D",
        .t_fmt_ampm             = "\u0025\u006C\u003A\u0025\u004D\u003A\u0025\u0053\u0020\u0025\u0050\u0020\u0025\u005A",
        .date_fmt               = "\u0025\u0061\u0020\u0025\u0065\u0020\u0025\u0062"
                                  "\u0020\u0025\u0048\u003A\u0025\u004D\u003A\u0025\u0053\u0020"
                                  "\u0025\u005A\u0020\u0025\u0059",
        .first_weekday          = 2
    },
    {
        .code                   = "en_NZ",
        .int_curr_symbol        = "\u004e\u005a\u0044\u0020",
        .currency_symbol        = "\u0024",
        .mon_decimal_point      = "\u002e",
        .mon_thousands_sep      = "\u002c",
        .mon_grouping           = "3:3",
        .positive_sign          = "",
        .negative_sign          = "\u002d",
        .int_frac_digits        = 2,
        .frac_digits            = 2,
        .p_cs_precedes          = 1,
        .p_sep_by_space         = 0,
        .n_cs_precedes          = 1,
        .n_sep_by_space         = 0,
        .p_sign_posn            = 1,
        .n_sign_posn            = 1,
        .decimal_point          = "\u002E",
        .thousands_sep          = "\u002C",
        .grouping               = "3:3",
        .abday                  = "\u0053\u0075\u006E:"
                                  "\u004D\u006F\u006E:"
                                  "\u0054\u0075\u0065:"
                                  "\u0057\u0065\u0064:"
                                  "\u0054\u0068\u0075:"
                                  "\u0046\u0072\u0069:"
                                  "\u0053\u0061\u0074",
        .day                    = "\u0053\u0075\u006E\u0064\u0061\u0079:"
                                  "\u004D\u006F\u006E\u0064\u0061\u0079:"
                                  "\u0054\u0075\u0065\u0073\u0064\u0061\u0079:"
                                  "\u0057\u0065\u0064\u006E\u0065\u0073\u0064\u0061\u0079:"
                                  "\u0054\u0068\u0075\u0072\u0073\u0064\u0061\u0079:"
                                  "\u0046\u0072\u0069\u0064\u0061\u0079:"
                                  "\u0053\u0061\u0074\u0075\u0072\u0064\u0061\u0079",
        .abmon                  = "\u004A\u0061\u006E:"
                                  "\u0046\u0065\u0062:"
                                  "\u004D\u0061\u0072:"
                                  "\u0041\u0070\u0072:"
                                  "\u004D\u0061\u0079:"
                                  "\u004A\u0075\u006E:"
                                  "\u004A\u0075\u006C:"
                                  "\u0041\u0075\u0067:"
                                  "\u0053\u0065\u0070:"
                                  "\u004F\u0063\u0074:"
                                  "\u004E\u006F\u0076:"
                                  "\u0044\u0065\u0063",
        .mon                    = "\u004A\u0061\u006E\u0075\u0061\u0072\u0079:"
                                  "\u0046\u0065\u0062\u0072\u0075\u0061\u0072\u0079:"
                                  "\u004D\u0061\u0072\u0063\u0068:"
                                  "\u0041\u0070\u0072\u0069\u006C:"
                                  "\u004D\u0061\u0079:"
                                  "\u004A\u0075\u006E\u0065:"
                                  "\u004A\u0075\u006C\u0079:"
                                  "\u0041\u0075\u0067\u0075\u0073\u0074:"
                                  "\u0053\u0065\u0070\u0074\u0065\u006D\u0062\u0065\u0072:"
                                  "\u004F\u0063\u0074\u006F\u0062\u0065\u0072:"
                                  "\u004E\u006F\u0076\u0065\u006D\u0062\u0065\u0072:"
                                  "\u0044\u0065\u0063\u0065\u006D\u0062\u0065\u0072",
        .d_t_fmt                = "\u0025\u0061\u0020\u0025\u0064\u0020\u0025\u0062\u0020\u0025\u0059\u0020\u0025\u0054\u0020\u0025\u005A",
        .d_fmt                  = "\u0025\u0064\u002F\u0025\u006D\u002F\u0025\u0079",
        .t_fmt                  = "\u0025\u0054",
        .am_pm                  = "\u0041\u004D:\u0050\u004D",
        .t_fmt_ampm             = "\u0025\u0049\u003A\u0025\u004D\u003A\u0025\u0053\u0020\u0025\u0070",
        .date_fmt               = "\u0025\u0061\u0020\u0025\u0065\u0020\u0025\u0062"
                                  "\u0020\u0025\u0048\u003A\u0025\u004D\u003A\u0025\u0053\u0020"
                                  "\u0025\u005A\u0020\u0025\u0059",
        .first_weekday          = 2
    },
    {
        .code                   = "de_DE",
        .int_curr_symbol        = "\u0045\u0055\u0052\u0020",
        .currency_symbol        = "\u20ac",
        .mon_decimal_point      = "\u002c",
        .mon_thousands_sep      = "\u002e",
        .mon_grouping           = "3:3",
        .positive_sign          = "",
        .negative_sign          = "\u002d",
        .int_frac_digits        = 2,
        .frac_digits            = 2,
        .p_cs_precedes          = 0,
        .p_sep_by_space         = 1,
        .n_cs_precedes          = 0,
        .n_sep_by_space         = 1,
        .p_sign_posn            = 1,
        .n_sign_posn            = 1,
        .decimal_point          = "\u002c",
        .thousands_sep          = "\u002e",
        .grouping               = "3:3",
        .abday                  = "\u0053\u006F:"
                                  "\u004D\u006F:"
                                  "\u0044\u0069:"
                                  "\u004D\u0069:"
                                  "\u0044\u006F:"
                                  "\u0046\u0072:"
                                  "\u0053\u0061",
        .day                    = "\u0053\u006F\u006E\u006E\u0074\u0061\u0067:"
                                  "\u004D\u006F\u006E\u0074\u0061\u0067:"
                                  "\u0044\u0069\u0065\u006E\u0073\u0074\u0061\u0067:"
                                  "\u004D\u0069\u0074\u0074\u0077\u006F\u0063\u0068:"
                                  "\u0044\u006F\u006E\u006E\u0065\u0072\u0073\u0074\u0061\u0067:"
                                  "\u0046\u0072\u0065\u0069\u0074\u0061\u0067:"
                                  "\u0053\u0061\u006D\u0073\u0074\u0061\u0067",
        .abmon                  = "\u004A\u0061\u006E:"
                                  "\u0046\u0065\u0062:"
                                  "\u004D\u00E4\u0072:"
                                  "\u0041\u0070\u0072:"
                                  "\u004D\u0061\u0069:"
                                  "\u004A\u0075\u006E:"
                                  "\u004A\u0075\u006C:"
                                  "\u0041\u0075\u0067:"
                                  "\u0053\u0065\u0070:"
                                  "\u004F\u006B\u0074:"
                                  "\u004E\u006F\u0076:"
                                  "\u0044\u0065\u007A",
        .mon                    = "\u004A\u0061\u006E\u0075\u0061\u0072:"
                                  "\u0046\u0065\u0062\u0072\u0075\u0061\u0072:"
                                  "\u004D\u00E4\u0072\u007A:"
                                  "\u0041\u0070\u0072\u0069\u006C:"
                                  "\u004D\u0061\u0069:"
                                  "\u004A\u0075\u006E\u0069:"
                                  "\u004A\u0075\u006C\u0069:"
                                  "\u0041\u0075\u0067\u0075\u0073\u0074:"
                                  "\u0053\u0065\u0070\u0074\u0065\u006D\u0062\u0065\u0072:"
                                  "\u004F\u006B\u0074\u006F\u0062\u0065\u0072:"
                                  "\u004E\u006F\u0076\u0065\u006D\u0062\u0065\u0072:"
                                  "\u0044\u0065\u007A\u0065\u006D\u0062\u0065\u0072:",
        .d_t_fmt                = "\u0025\u0061\u0020\u0025\u0064\u0020\u0025\u0062\u0020\u0025\u0059\u0020\u0025\u0054\u0020\u0025\u005A",
        .d_fmt                  = "\u0025\u0064\u002E\u0025\u006D\u002E\u0025\u0059",
        .t_fmt                  = "\u0025\u0054",
        .am_pm                  = "",
        .t_fmt_ampm             = "",
        .date_fmt               = "\u0025\u0061\u0020\u0025\u002D\u0064\u002E\u0020"
                                  "\u0025\u0062\u0020\u0025\u0048\u003A\u0025\u004D\u003A\u0025\u0053"
                                  "\u0020\u0025\u005A\u0020\u0025\u0059",
        .first_weekday          = 2
    },
    {
        .code                   = "ru_RU",
        .int_curr_symbol        = "\u0052\u0055\u0042\u0020",
        .currency_symbol        = "\u20bd",
        .mon_decimal_point      = "\u002e",
        .mon_thousands_sep      = "\u00a0",
        .mon_grouping           = "3:3",
        .positive_sign          = nullptr,
        .negative_sign          = "\u002d",
        .int_frac_digits        = 2,
        .frac_digits            = 2,
        .p_cs_precedes          = 0,
        .p_sep_by_space         = 1,
        .n_cs_precedes          = 0,
        .n_sep_by_space         = 1,
        .p_sign_posn            = 1,
        .n_sign_posn            = 1,
        .decimal_point          = "\u002c",
        .thousands_sep          = "\u00a0",
        .grouping               = "3:3",
        .abday                  = "\u0412\u0441:"
                                  "\u041f\u043d:"
                                  "\u0412\u0442:"
                                  "\u0421\u0440:"
                                  "\u0427\u0442:"
                                  "\u041f\u0442:"
                                  "\u0421\u0431",
        .day                    = "\u0412\u043E\u0441\u043A\u0440\u0435\u0441\u0435\u043D\u044C\u0435:"
                                  "\u041F\u043E\u043D\u0435\u0434\u0435\u043B\u044C\u043D\u0438\u043A:"
                                  "\u0412\u0442\u043E\u0440\u043D\u0438\u043A:"
                                  "\u0421\u0440\u0435\u0434\u0430:"
                                  "\u0427\u0435\u0442\u0432\u0435\u0440\u0433:"
                                  "\u041F\u044F\u0442\u043D\u0438\u0446\u0430:"
                                  "\u0421\u0443\u0431\u0431\u043E\u0442\u0430",
        .abmon                  = "\u044F\u043D\u0432:"
                                  "\u0444\u0435\u0432:"
                                  "\u043C\u0430\u0440:"
                                  "\u0430\u043F\u0440:"
                                  "\u043C\u0430\u0439:"
                                  "\u0438\u044E\u043D:"
                                  "\u0438\u044E\u043B:"
                                  "\u0430\u0432\u0433:"
                                  "\u0441\u0435\u043D:"
                                  "\u043E\u043A\u0442:"
                                  "\u043D\u043E\u044F:"
                                  "\u0434\u0435\u043A",
        .mon                    = "\u042F\u043D\u0432\u0430\u0440\u044C:"
                                  "\u0424\u0435\u0432\u0440\u0430\u043B\u044C:"
                                  "\u041C\u0430\u0440\u0442:"
                                  "\u0410\u043F\u0440\u0435\u043B\u044C:"
                                  "\u041C\u0430\u0439:"
                                  "\u0418\u044E\u043D\u044C:"
                                  "\u0418\u044E\u043B\u044C:"
                                  "\u0410\u0432\u0433\u0443\u0441\u0442:"
                                  "\u0421\u0435\u043D\u0442\u044F\u0431\u0440\u044C:"
                                  "\u041E\u043A\u0442\u044F\u0431\u0440\u044C:"
                                  "\u041D\u043E\u044F\u0431\u0440\u044C:"
                                  "\u0414\u0435\u043A\u0430\u0431\u0440\u044C",
        .d_t_fmt                = "\u0025\u0061\u0020\u0025\u0064\u0020\u0025\u0062\u0020\u0025\u0059\u0020\u0025\u0054",
        .d_fmt                  = "\u0025\u0064\u002E\u0025\u006D\u002E\u0025\u0059",
        .t_fmt                  = "\u0025\u0054",
        .am_pm                  = nullptr,
        .t_fmt_ampm             = nullptr,
        .date_fmt               = nullptr,
        .first_weekday          = 2
    }
};

const Hard_locale * hardcoded(const tau::Language & lang, const tau::Territory & terr) {
    std::string code = lang.code()+"_"+terr.code();

    for (const Hard_locale * p = hard_; p < hard_+std::size(hard_); ++p) {
        if (code == p->code) {
            return p;
        }
    }

    return hard_;
}

tau::Locale_impl sys_locale_ = {
    .spec       = "C",
    .lang       = tau::Language("C"),
    .terr       = tau::Territory(""),
    .enc        = tau::Encoding("ASCII"),
    .iocharset  = tau::Encoding("ASCII")
};

} // anonymous namespace

// ----------------------------------------------------------------------------
// ----------------------------------------------------------------------------

namespace tau {

Locale_impl * sys_locale_ptr_   = nullptr;
Locale_impl * locale_ptr_       = nullptr;

struct Locale_deleter {
    ~Locale_deleter() {
        if (sys_locale_ptr_ && sys_locale_ptr_ != &sys_locale_) { delete sys_locale_ptr_; }
        if (locale_ptr_ && locale_ptr_ != sys_locale_ptr_) { delete locale_ptr_; }
        std::locale::global(std::locale::classic());
    }
};

Locale_deleter locale_deleter_;

// static
void Locale_impl::boot() {
    if (!sys_locale_ptr_) {
        sys_locale_.init1();
        sys_locale_.init2();
        sys_locale_ptr_ = &sys_locale_;

        locale_ptr_ = new Locale_impl;
        locale_ptr_->spec = "C";
        locale_ptr_->lang = Language("C");
        locale_ptr_->terr = Territory("");
        locale_ptr_->enc = sys_locale_.enc;
        locale_ptr_->iocharset = sys_locale_.iocharset;
        locale_ptr_->init();
    }
}

void Locale_impl::init2() {
    auto beg = spec.find_first_of("-_"), end = beg;

    if (std::string::npos != beg) {
        lang = Language(spec.substr(0, beg));
        end = spec.find('.', ++beg);
        terr = Territory(spec.substr(beg, end-beg));
    }

    else {
        lang = Language(spec);
        terr = Territory("");
    }

    if (!enc) {
        beg = spec.find('.');

        if (std::string::npos != beg) {
            ++beg;
            end = spec.find('@');
            enc = Encoding(spec.substr(beg, end-beg));
        }
    }

    beg = spec.find('@');
    if (std::string::npos != beg) { mod = spec.substr(1+beg); }
    if (!iocharset) { iocharset = enc; }
    init();
}

void Locale_impl::init() {
    if (auto * hd = hardcoded(lang, terr)) {
        code.assign(hd->code ? hd->code : hard_->code);
        int_curr_symbol.assign(hd->int_curr_symbol ? hd->int_curr_symbol : hard_->int_curr_symbol);
        currency_symbol.assign(hd->currency_symbol ? hd->currency_symbol : hard_->currency_symbol);
        mon_decimal_point.assign(hd->mon_decimal_point ? hd->mon_decimal_point : hard_->mon_decimal_point);
        mon_thousands_sep.assign(hd->mon_thousands_sep ? hd->mon_thousands_sep : hard_->mon_thousands_sep);
        mon_grouping.assign(hd->mon_grouping ? hd->mon_grouping : hard_->mon_grouping);
        positive_sign.assign(hd->positive_sign ? hd->positive_sign : hard_->positive_sign);
        negative_sign.assign(hd->negative_sign ? hd->negative_sign : hard_->negative_sign);
        decimal_point.assign(hd->decimal_point ? hd->decimal_point : hard_->decimal_point);
        thousands_sep.assign(hd->thousands_sep ? hd->thousands_sep : hard_->thousands_sep);
        grouping.assign(hd->grouping ? hd->grouping : hard_->grouping);
        abday.assign(hd->abday ? hd->abday : hard_->abday);
        day.assign(hd->day ? hd->day : hard_->day);
        abmon.assign(hd->abmon ? hd->abmon : hard_->abmon);
        mon.assign(hd->mon ? hd->mon : hard_->mon);
        d_t_fmt.assign(hd->d_t_fmt ? hd->d_t_fmt : hard_->d_t_fmt);
        d_fmt.assign(hd->d_fmt ? hd->d_fmt : hard_->d_fmt);
        t_fmt.assign(hd->t_fmt ? hd->t_fmt : hard_->t_fmt);
        am_pm.assign(hd->am_pm ? hd->am_pm : hard_->am_pm);
        t_fmt_ampm.assign(hd->t_fmt_ampm ? hd->t_fmt_ampm : hard_->t_fmt_ampm);
        date_fmt.assign(hd->date_fmt ? hd->date_fmt : hard_->date_fmt);

        int_frac_digits = hd->int_frac_digits;
        frac_digits     = hd->frac_digits;
        p_cs_precedes   = hd->p_cs_precedes;
        p_sep_by_space  = hd->p_sep_by_space;
        n_cs_precedes   = hd->n_cs_precedes;
        n_sep_by_space  = hd->n_sep_by_space;
        p_sign_posn     = hd->p_sign_posn;
        n_sign_posn     = hd->n_sign_posn;
        first_weekday   = hd->first_weekday;

        blanks.assign(hd->blanks ? hd->blanks : hard_->blanks);
        wblanks = blanks;
        newlines.assign(hd->newlines ? hd->newlines : hard_->newlines);
        wnewlines = newlines;
        delimiters.assign(hd->delimiters ? hd->delimiters : hard_->delimiters);
        wdelimiters = delimiters;
    }
}

// ----------------------------------------------------------------------------
// ----------------------------------------------------------------------------

Locale::Locale() {
    if (!sys_locale_ptr_) { Locale_impl::boot(); }
    impl = locale_ptr_;
}

Locale::Locale(const std::string & spec) {
    if (!sys_locale_ptr_) { Locale_impl::boot(); }

    if (spec.empty()) {
        impl = sys_locale_ptr_;
    }

    else {
        impl = new Locale_impl;
        impl->spec = spec;
        impl->iocharset = sys_locale_.iocharset;
        impl->init2();
    }
}

Locale::Locale(Locale_impl * pimpl):
    impl(pimpl)
{
    if (!sys_locale_ptr_) { Locale_impl::boot(); }
}

Locale::Locale(const Language & lang, const Territory & terr, std::string_view modifier):
    impl(new Locale_impl)
{
    if (!sys_locale_ptr_) { Locale_impl::boot(); }
    impl->lang = lang;
    impl->terr = terr;
    impl->mod = modifier;
    impl->iocharset = sys_locale_.iocharset;
    impl->init();
}

Locale::Locale(const Language & lang, const Territory & terr, const Encoding & enc, std::string_view modifier):
    impl(new Locale_impl)
{
    if (!sys_locale_ptr_) { Locale_impl::boot(); }
    impl->lang = lang;
    impl->terr = terr;
    impl->enc = enc;
    impl->iocharset = sys_locale_.iocharset;
    impl->mod = modifier;
    impl->init();
}

Locale::Locale(const Locale & other):
    impl(new Locale_impl)
{
    impl->lang = other.impl->lang;
    impl->terr = other.impl->terr;
    impl->enc = other.impl->enc;
    impl->iocharset = sys_locale_.iocharset;
    impl->mod = other.impl->mod;
    impl->init();
}

Locale::~Locale() {
    if (impl != &sys_locale_ && impl != locale_ptr_) { delete impl; }
}

Locale & Locale::operator=(const Locale & other) {
    if (this != &other) {
        if (impl == sys_locale_ptr_) { impl = new Locale_impl; }
        impl->lang = other.impl->lang;
        impl->terr = other.impl->terr;
        impl->enc = other.impl->enc;
        impl->iocharset = sys_locale_.iocharset;
        impl->mod = other.impl->mod;
        impl->init();
    }

    return *this;
}

bool Locale::operator==(const Locale & other) const noexcept {
    return impl->lang == other.impl->lang && impl->terr == other.impl->terr &&
           impl->enc == other.impl->enc && impl->mod == other.impl->mod;
}

bool Locale::operator!=(const Locale & other) const noexcept {
    return impl->lang != other.impl->lang || impl->terr != other.impl->terr ||
           impl->enc != other.impl->enc || impl->mod != other.impl->mod;
}

// static
std::string Locale::set(std::string_view svname) {
    boot();
    std::string name(svname);
    if (!sys_locale_ptr_) { Locale_impl::boot(); }
    std::string prev, now;

    try {
        prev = std::locale::global(std::locale(name)).name();
        now  = std::locale().name();
    }

    catch (...) {
        prev = setlocale(LC_ALL, NULL);
        now  = setlocale(LC_ALL, name.c_str());
    }

    if (locale_ptr_) { delete locale_ptr_; }

    if (!name.empty()) {
        locale_ptr_ = new Locale_impl;
        locale_ptr_->spec = name;
        locale_ptr_->init2();
    }

    else {
        locale_ptr_ = new Locale_impl(sys_locale_);
        locale_ptr_->init2();
    }

    gettext_update();        // Calls bindtextdomain() from gettext package.
    return Locale().name();
}

std::string Locale::name() const noexcept {
    std::string spec = impl->code;

    if (impl->enc) {
        spec += '.';
        spec += impl->enc.name();
    }

    if (!impl->mod.empty()) {
        spec += '@';
        spec += impl->mod;
    }

    return spec;
}

const Language & Locale::language() const noexcept {
    return impl->lang;
}

const Territory & Locale::territory() const noexcept {
    return impl->terr;
}

const Encoding & Locale::encoding() const noexcept {
    return impl->enc;
}

const Encoding & Locale::iocharset() const noexcept {
    return impl->iocharset;
}

std::string Locale::modifier() const {
    return impl->mod;
}

std::vector<Locale> Locale::variants() const {
    std::vector<Locale> v;

    std::string lang = impl->lang.code2();
    std::string terr = impl->lang.name();
    std::string enc = impl->enc.name();
    std::string mod = impl->mod;

    if (!lang.empty() && !terr.empty() && !enc.empty() && !mod.empty()) {
        v.emplace_back(lang+'_'+terr+'.'+enc+mod);
    }

    if (!lang.empty() && !terr.empty() && !enc.empty()) {
        v.emplace_back(lang+'_'+terr+'.'+enc);
    }

    if (!lang.empty() && !terr.empty()) {
        v.emplace_back(lang+'_'+terr);
    }

    if (!lang.empty()) {
        v.emplace_back(lang);
    }

    return v;
}

ustring Locale::int_curr_symbol() const {
    return impl->int_curr_symbol;
}

ustring Locale::currency_symbol() const {
    return impl->currency_symbol;
}

ustring Locale::mon_decimal_point() const {
    return impl->mon_decimal_point;
}

ustring Locale::mon_thousands_sep() const {
    return impl->mon_thousands_sep;
}

std::vector<int> Locale::mon_grouping() const {
    std::vector<int> v;

    for (auto & s: str_explode(impl->mon_grouping, ':')) {
        v.emplace_back(s[0]-'\0');
    }

    return v;
}

ustring Locale::positive_sign() const {
    return impl->positive_sign;
}

ustring Locale::negative_sign() const {
    return impl->negative_sign;
}

int Locale::int_frac_digits() const noexcept {
    return impl->int_frac_digits;
}

int Locale::frac_digits() const noexcept {
    return impl->frac_digits;
}

int Locale::p_cs_precedes() const noexcept {
    return impl->p_cs_precedes;
}

int Locale::p_sep_by_space() const noexcept {
    return impl->p_sep_by_space;
}

int Locale::n_cs_precedes() const noexcept {
    return impl->n_cs_precedes;
}

int Locale::n_sep_by_space() const noexcept {
    return impl->n_sep_by_space;
}

int Locale::p_sign_posn() const noexcept {
    return impl->p_sign_posn;
}

int Locale::n_sign_posn() const noexcept {
    return impl->n_sign_posn;
}

ustring Locale::decimal_point() const {
    return impl->decimal_point;
}

ustring Locale::thousands_sep() const {
    return impl->thousands_sep;
}

std::vector<int> Locale::grouping() const {
    std::vector<int> v;

    for (auto & s: str_explode(impl->grouping, ':')) {
        v.emplace_back(s[0]-'\0');
    }

    return v;
}

std::vector<ustring> Locale::abday() const {
    return str_explode(impl->abday, ':');
}

std::vector<ustring> Locale::day() const {
    return str_explode(impl->day, ':');
}

std::vector<ustring> Locale::abmon() const {
    return str_explode(impl->abmon, ':');
}

std::vector<ustring> Locale::mon() const {
    return str_explode(impl->mon, ':');
}

ustring Locale::d_t_fmt() const {
    return impl->d_t_fmt;
}

ustring Locale::d_fmt() const {
    return impl->d_fmt;
}

ustring Locale::t_fmt() const {
    return impl->t_fmt;
}

std::vector<ustring> Locale::am_pm() const {
    return str_explode(impl->am_pm, ':');
}

ustring Locale::t_fmt_ampm() const {
    return impl->t_fmt_ampm;
}

ustring Locale::date_fmt() const {
    return impl->date_fmt;
}

int Locale::first_weekday() const noexcept {
    return impl->first_weekday;
}

ustring Locale::blanks() const {
    return impl->blanks;
}

std::u32string Locale::wblanks() const {
    return impl->wblanks;
}

ustring Locale::newlines() const {
    return impl->newlines;
}

std::u32string Locale::wnewlines() const {
    return impl->wnewlines;
}

ustring Locale::delimiters() const {
    return impl->delimiters;
}

std::u32string Locale::wdelimiters() const {
    return impl->wdelimiters;
}

} // namespace tau

//END

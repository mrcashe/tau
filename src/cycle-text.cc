// ----------------------------------------------------------------------------
// SPDX-License-Identifier: BSD-2-Clause
// Copyright © 2014-2024 Konstantin Shmelkov <mrcashe@gmail.com>.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
// THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
// PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
// CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
// EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
// PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
// OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
// WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
// OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
// EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// ----------------------------------------------------------------------------

#include <tau/cycle.hh>
#include <cycle-text-impl.hh>

namespace tau {

#define CYCLE_IMPL (std::static_pointer_cast<Cycle_impl>(impl))
#define CYCLE_TEXT_IMPL (std::static_pointer_cast<Cycle_text_impl>(impl))

Cycle_text::Cycle_text(Border bs):
    Cycle_base(std::make_shared<Cycle_text_impl>(bs))
{
}

Cycle_text::Cycle_text(Align align, Border bs):
    Cycle_base(std::make_shared<Cycle_text_impl>(align, bs))
{
}

Cycle_text::Cycle_text(const std::vector<ustring> & sv, Border bs):
    Cycle_base(std::make_shared<Cycle_text_impl>(sv, bs))
{
}

Cycle_text::Cycle_text(const std::vector<ustring> & sv, Align align, Border bs):
    Cycle_base(std::make_shared<Cycle_text_impl>(sv, align, bs))
{
}

Cycle_text::Cycle_text(Widget_ptr wp):
    Cycle_base(std::dynamic_pointer_cast<Cycle_text_impl>(wp))
{
}

Cycle_text::Cycle_text(const Cycle_text & other):
    Cycle_base(other.impl)
{
}

Cycle_text & Cycle_text::operator=(const Cycle_text & other) {
    Cycle_base::operator=(other);
    return *this;
}

Cycle_text::Cycle_text(Cycle_text && other):
    Cycle_base(other.impl)
{
}

Cycle_text & Cycle_text::operator=(Cycle_text && other) {
    Cycle_base::operator=(other);
    return *this;
}

Cycle_text & Cycle_text::operator=(Widget_ptr wp) {
    Cycle_base::operator=(std::dynamic_pointer_cast<Cycle_text_impl>(wp));
    return *this;
}

void Cycle_text::text_align(Align align) {
    CYCLE_TEXT_IMPL->text_align(align);
}

Align Cycle_text::text_align() const noexcept {
    return CYCLE_TEXT_IMPL->text_align();
}

void Cycle_text::allow_edit() {
    CYCLE_TEXT_IMPL->allow_edit();
}

void Cycle_text::disallow_edit() {
    CYCLE_TEXT_IMPL->disallow_edit();
}

bool Cycle_text::editable() const noexcept {
    return CYCLE_TEXT_IMPL->editable();
}

int Cycle_text::add(const ustring & text, const ustring & tooltip) {
    return CYCLE_TEXT_IMPL->add(text, tooltip);
}

int Cycle_text::add(const ustring & text, Align align, const ustring & tooltip) {
    return CYCLE_TEXT_IMPL->add(text, align, tooltip);
}

int Cycle_text::add(const std::vector<ustring> & sv, Align align) {
    return CYCLE_TEXT_IMPL->add(sv, align);
}

std::size_t Cycle_text::remove(Widget & w) {
    return CYCLE_IMPL->remove(w.ptr().get());
}

std::size_t Cycle_text::remove(int id) {
    return CYCLE_IMPL->remove(id);
}

std::size_t Cycle_text::remove(const ustring & text) {
    return CYCLE_TEXT_IMPL->remove(text);
}

void Cycle_text::wrap(Wrap wm) {
    CYCLE_TEXT_IMPL->wrap(wm);
}

Wrap Cycle_text::wrap() const noexcept {
    return CYCLE_TEXT_IMPL->wrap();
}

Widget_ptr Cycle_text::widget(const ustring & s) {
    return CYCLE_TEXT_IMPL->widget(s);
}

Widget_cptr Cycle_text::widget(const ustring & s) const {
    return CYCLE_TEXT_IMPL->widget(s);
}

int Cycle_text::select(Widget & w, bool fix) {
    return CYCLE_IMPL->select(w.ptr().get(), fix);
}

int Cycle_text::select(int id, bool fix) {
    return CYCLE_IMPL->select(id, fix);
}

int Cycle_text::select(const ustring & s, bool fix) {
    return CYCLE_TEXT_IMPL->select(s, fix);
}

int Cycle_text::select_similar(const ustring & s, bool fix) {
    return CYCLE_TEXT_IMPL->select_similar(s, fix);
}

int Cycle_text::setup(Widget & w, bool fix) {
    return CYCLE_IMPL->setup(w.ptr().get(), fix);
}

int Cycle_text::setup(int id, bool fix) {
    return CYCLE_IMPL->setup(id, fix);
}

int Cycle_text::setup(const ustring & s, bool fix) {
    return CYCLE_TEXT_IMPL->setup(s, fix);
}

int Cycle_text::setup_similar(const ustring & s, bool fix) {
    return CYCLE_TEXT_IMPL->setup_similar(s, fix);
}

ustring Cycle_text::str() const {
    return CYCLE_TEXT_IMPL->str();
}

ustring Cycle_text::str(int id) const {
    return CYCLE_TEXT_IMPL->str(id);
}

std::u32string Cycle_text::wstr() const {
    return CYCLE_TEXT_IMPL->wstr();
}

std::u32string Cycle_text::wstr(int id) const {
    return CYCLE_TEXT_IMPL->wstr(id);
}

std::vector<ustring> Cycle_text::strings() const {
    return CYCLE_TEXT_IMPL->strings();
}

std::vector<std::u32string> Cycle_text::wstrings() const {
    return CYCLE_TEXT_IMPL->wstrings();
}

Widget_ptr Cycle_text::edit() {
    return CYCLE_TEXT_IMPL->edit();
}

Widget_ptr Cycle_text::editor() {
    return CYCLE_TEXT_IMPL->editor();
}

Widget_cptr Cycle_text::editor() const {
    return CYCLE_TEXT_IMPL->editor();
}

signal<void(int, const std::u32string &)> & Cycle_text::signal_activate_edit() {
    return CYCLE_TEXT_IMPL->signal_activate_edit();
}

signal<void(int)> & Cycle_text::signal_cancel_edit() {
    return CYCLE_TEXT_IMPL->signal_cancel_edit();
}

signal_all<const std::u32string &> & Cycle_text::signal_validate() {
    return CYCLE_TEXT_IMPL->signal_validate();
}

signal_all<const std::u32string &> & Cycle_text::signal_approve() {
    return CYCLE_TEXT_IMPL->signal_approve();
}

signal<void(const std::u32string &)> & Cycle_text::signal_new_text() {
    return CYCLE_TEXT_IMPL->signal_new_text();
}

signal<void(const std::u32string &)> & Cycle_text::signal_remove_text() {
    return CYCLE_TEXT_IMPL->signal_remove_text();
}

signal<void(const std::u32string &)> & Cycle_text::signal_select_text() {
    return CYCLE_TEXT_IMPL->signal_select_text();
}

signal<void(int, const std::u32string &)> & Cycle_text::signal_activate_text() {
    return CYCLE_TEXT_IMPL->signal_activate_text();
}

signal<void(int, const std::u32string &)> & Cycle_text::signal_text_changed() {
    return CYCLE_TEXT_IMPL->signal_text_changed();
}

} // namespace tau

//END

// ----------------------------------------------------------------------------
// SPDX-License-Identifier: BSD-2-Clause
// Copyright © 2014-2024 Konstantin Shmelkov <mrcashe@gmail.com>.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
// THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
// PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
// CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
// EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
// PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
// OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
// WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
// OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
// EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// ----------------------------------------------------------------------------

#include <tau/menu.hh>
#include <menubar-impl.hh>

namespace tau {

#define MENUBAR_IMPL (std::static_pointer_cast<Menubar_impl>(impl))

Menubar::Menubar():
    Menu(std::make_shared<Menubar_impl>())
{
}

Menubar::Menubar(const Menubar & other):
    Menu(other.impl)
{
}

Menubar & Menubar::operator=(const Menubar & other) {
    Menu::operator=(other);
    return *this;
}

Menubar::Menubar(Menubar && other):
    Menu(other.impl)
{
}

Menubar & Menubar::operator=(Menubar && other) {
    Menu::operator=(other);
    return *this;
}

Menubar::Menubar(Widget_ptr wp):
    Menu(std::dynamic_pointer_cast<Menubar_impl>(wp))
{
}

Menubar & Menubar::operator=(Widget_ptr wp) {
    Menu::operator=(std::dynamic_pointer_cast<Menubar_impl>(wp));
    impl = wp;
    return *this;
}

bool Menubar::activate() {
    return MENUBAR_IMPL->activate();
}

} // namespace tau

//END

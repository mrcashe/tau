// ----------------------------------------------------------------------------
// SPDX-License-Identifier: BSD-2-Clause
// Copyright © 2014-2024 Konstantin Shmelkov <mrcashe@gmail.com>.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
// THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
// PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
// CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
// EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
// PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
// OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
// WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
// OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
// EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// ----------------------------------------------------------------------------

#ifndef __TAU_TWINS_IMPL_HH__
#define __TAU_TWINS_IMPL_HH__

#include <flat-container.hh>

namespace tau {

class Twins_impl: public Flat_container {
public:

    Twins_impl(Orientation orient, double ratio=0.5);
   ~Twins_impl() { destroy(); }

    void insert_first(Widget_ptr wp);
    void insert_second(Widget_ptr wp);
    void insert(Widget_ptr first, Widget_ptr second);
    bool has_first() const noexcept { return nullptr != first_; }
    bool has_second() const noexcept { return nullptr != second_; }
    bool empty() const noexcept { return nullptr == first_ && nullptr == second_; }
    void remove_first();
    void remove_second();
    void clear();
    Widget_ptr first() noexcept { return chptr(first_); }
    Widget_ptr second() noexcept { return chptr(second_); }
    Widget_cptr first() const noexcept { return chptr(first_); }
    Widget_cptr second() const noexcept { return chptr(second_); }
    void set_ratio(double ratio);
    double ratio() const noexcept { return ratio_; }
    signal<void(double)> & signal_ratio_changed() { return signal_ratio_changed_; }

    Container_impl * compound() noexcept override { return this; };
    const Container_impl * compound() const noexcept override { return this; };

private:

    Separator_impl *     sep_;
    Widget_impl *        first_                 = nullptr;
    Widget_impl *        second_                = nullptr;
    double               ratio_;
    Orientation          orient_;
    connection           first_cx_              { true };
    connection           second_cx_             { true };
    connection           sep_mouse_motion_cx_   { true };
    signal<void(double)> signal_ratio_changed_;

private:

    Size child_requisition(Widget_impl * wp);
    void update_requisition();
    void rm_first();
    void rm_second();
    void arrange();

    void on_hints(Widget_impl * wp, Hints op);
    bool on_sep_mouse_down(int mbt, int mm, const Point & pt);
    bool on_sep_mouse_up(int mbt, int mm, const Point & pt);
    void on_sep_mouse_motion(int mm, const Point & pt);
    bool on_take_focus();
};

} // namespace tau

#endif // __TAU_TWINS_IMPL_HH__

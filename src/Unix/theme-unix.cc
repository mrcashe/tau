// ----------------------------------------------------------------------------
// SPDX-License-Identifier: BSD-2-Clause
// Copyright © 2014-2024 Konstantin Shmelkov <mrcashe@gmail.com>.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
// THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
// PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
// CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
// EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
// PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
// OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
// WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
// OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
// EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// ----------------------------------------------------------------------------

#include <tau/exception.hh>
#include <tau/string.hh>
#include <tau/sys.hh>
#include "theme-unix.hh"
#include <mutex>

namespace {

std::recursive_mutex    mx_;
tau::Theme_unix_ptr     root_;

} // anonymous namespace

namespace tau {

// Overrides Theme_impl.
void Theme_unix::boot() {
    add_pixmap_dir("/usr/share/pixmaps");
    add_icon_dir("/usr/share/icons");

    add_icon_dir(path_build(path_home(), ".icons"));
    add_cursor_dir(path_build(path_home(), ".icons"));

    add_pixmap_dir("/usr/local/share/pixmaps");
    add_icon_dir("/usr/local/share/icons");

    add_cursor_dir(path_build(path_share(), "cursors"));
    add_pixmap_dir(path_build(path_share(), "pixmaps"));
    add_icon_dir(path_build(path_share(), "icons"));

    boot_cursor_themes("Oxygen Blue:Oxygen Yellow:Breeze:KDE Classic:Adwaita");

    // FIXME Trying to obtain cursor size from environment. What about Wayland here?
    int cs = std::atoi(str_getenv("XCURSOR_SIZE").c_str());
    if (cs) { cursor_size_ = std::max(12, cs); }

    // TODO Add support for "XCURSOR_THEME" environment variable here?

    Theme_impl::boot();
}

// static
Theme_unix_ptr Theme_unix::root_unix() {
    std::unique_lock lock(mx_);

    if (!root_) {
        root_ = std::make_shared<Theme_unix>();
        root_->boot();
    }

    return root_;
}

// static
Theme_ptr Theme_impl::root() {
    return Theme_unix::root_unix();
}

} // namespace tau

//END

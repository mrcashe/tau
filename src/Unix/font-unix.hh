// ----------------------------------------------------------------------------
// SPDX-License-Identifier: BSD-2-Clause
// Copyright © 2014-2024 Konstantin Shmelkov <mrcashe@gmail.com>.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
// THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
// PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
// CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
// EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
// PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
// OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
// WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
// OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
// EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// ----------------------------------------------------------------------------

#ifndef __TAU_FONT_UNIX_HH__
#define __TAU_FONT_UNIX_HH__

#include "defs-unix.hh"
#include <font-impl.hh>
#include <glyph-impl.hh>
#include <map>
#if __has_include(<memory_resource>)
#include <memory_resource>
#endif

namespace tau {

class Font_unix: public Font_impl {
public:

    Font_unix(Font_face_ptr fface, const ustring & spec, double pts, unsigned dpi);

    // Overrides pure Font_impl.
    ustring psname() const override;

    // Overrides pure Font_impl.
    Glyph_ptr glyph(char32_t wc) const override;

    // Overrides pure Font_impl.
    Glyph_ptr master_glyph(char32_t wc) const override;

protected:

#if __has_include(<memory_resource>)
    uint8_t             mem_[16384];
    std::pmr::monotonic_buffer_resource pool_ { std::data(mem_), std::size(mem_) };
    using Glyphs = std::pmr::map<char32_t, Glyph_ptr>;
    mutable Glyphs      gmap_ { &pool_ };
#else
    using Glyphs = std::map<char32_t, Glyph_ptr>;
    mutable Glyphs      gmap_;
#endif
    Font_face_ptr       face_;
    Matrix              mat_;
};

} // namespace tau

#endif // __TAU_FONT_UNIX_HH__

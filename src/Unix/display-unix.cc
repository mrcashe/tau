// ----------------------------------------------------------------------------
// SPDX-License-Identifier: BSD-2-Clause
// Copyright © 2014-2024 Konstantin Shmelkov <mrcashe@gmail.com>.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
// THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
// PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
// CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
// EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
// PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
// OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
// WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
// OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
// EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// ----------------------------------------------------------------------------

#include <tau/exception.hh>
#include <tau/sys.hh>
#include "display-unix.hh"
#include <iostream>
#include <map>
#include <mutex>

namespace {

using Displays = std::map<std::thread::id, tau::Display_unix_ptr>;

std::recursive_mutex    smx_;
Displays                dps_;
std::atomic_int         dpcnt_;

tau::Display_unix_ptr new_display(std::thread::id tid, const tau::ustring & args) {
    std::unique_lock lk(smx_);
    auto [ i, succeed ] = dps_.emplace(tid, std::make_shared<tau::Display_unix>(tid, args));
    ++dpcnt_;
    tau::Display_impl::signal_open()(i->second.get());
    return i->second;
}

} // anonymous namespace

// ----------------------------------------------------------------------------
// ----------------------------------------------------------------------------

namespace tau {

Display_unix::Display_unix(std::thread::id tid, const ustring & args):
    loop_(Loop_unix::this_unix_loop())
{
    if (!loop_->alive()) { throw user_error(ustring(__func__)+": an attempt to run Display on dead Loop"); }
    signal_quit_.connect(fun(this, &Display_unix::on_quit));
    loop_->signal_quit().connect(fun(signal_quit_));
    tid_ = tid;
    dpid_ = dpcnt_;
    open(args);
}

void Display_unix::on_quit() {
    Display_ptr dp;     // Keep our shptr.
    std::unique_lock lock(smx_);
    dp = dps_[tid_];
    dps_.erase(tid_);
}

// ----------------------------------------------------------------------------
// ----------------------------------------------------------------------------

// static
Display_unix_ptr Display_unix::this_unix_display() {
    std::thread::id tid = std::this_thread::get_id();
    std::unique_lock lk(smx_);
    auto i = dps_.find(tid);
    if (dps_.end() != i) { return i->second; }
    smx_.unlock();
    return new_display(tid, ustring());
}

// static
// Platform-specific.
Display_ptr Display_impl::open(const ustring & args) {
    std::thread::id tid = std::this_thread::get_id();
    std::unique_lock lk(smx_);
    auto i = dps_.find(tid);
    if (dps_.end() != i) { return i->second; }
    smx_.unlock();
    return new_display(tid, args);
}

// static
// Platform-specific.
bool Display_impl::this_running() noexcept {
    std::unique_lock lock(smx_);
    return dps_.contains(std::this_thread::get_id());
}

// static
// Platform-specific.
Display_ptr Display_impl::this_ptr() {
    return Display_unix::this_unix_display();
}

// static
Display_xcb_ptr Display_xcb::this_xcb_display() {
    return Display_unix::this_unix_display();
}

// static
// Platform-specific.
Display_ptr Display_impl::ptr(Display_impl * dp) {
    std::unique_lock lock(smx_);
    auto i = std::find_if(dps_.begin(), dps_.end(), [dp](auto & p) { return p.second.get() == dp; } );
    return i != dps_.end() ? i->second : nullptr;
}

} // namespace tau

//END

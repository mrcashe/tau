// ----------------------------------------------------------------------------
// SPDX-License-Identifier: BSD-2-Clause
// Copyright © 2014-2024 Konstantin Shmelkov <mrcashe@gmail.com>.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
// THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
// PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
// CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
// EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
// PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
// OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
// WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
// OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
// EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// ----------------------------------------------------------------------------

#ifndef __TAU_LOOP_UNIX_HH__
#define __TAU_LOOP_UNIX_HH__

#include <tau/file.hh>
#include <tau/ustring.hh>
#include <event-impl.hh>
#include <loop-impl.hh>
#include "defs-unix.hh"
#include "watcher-impl.hh"
#include <poll.h>

namespace tau {

class Poller_base: public trackable {
public:

    virtual ~Poller_base() = default;
    virtual int fd() const = 0;
    virtual signal<void()> & signal_poll() = 0;

    bool on_poll(int pfd) {
        if (fd() == pfd) {
            signal_poll()();
            return true;
        }

        return false;
    }

    signal<void()> & signal_destroy() {
        return signal_destroy_;
    }

protected:

    signal<void()> signal_destroy_;
};

class Poller_unix: public Poller_base {
    int fd_ = -1;
    signal<void()> signal_poll_;

public:

    explicit Poller_unix(int fd): fd_(fd) {}
   ~Poller_unix() { signal_destroy_(); }
    int fd() const override { return fd_; }
    signal<void()> & signal_poll() override { return signal_poll_; }
};

// ----------------------------------------------------------------------------
// ----------------------------------------------------------------------------

class Event_unix: public Event_impl, public Poller_base {
public:

    Event_unix();
   ~Event_unix();

    // Overrides Event_impl.
    void emit() override;

    // Overrides pure Event_impl.
    void release() override;

    int fd() const override { return fds_[0]; }
    signal<void()> & signal_poll() override { return signal_ready_; }

private:

    using Mutex = std::recursive_mutex;
    using Lock = std::lock_guard<Mutex>;

    int         fds_[2] { -1, -1 };
    Mutex       mx_;
};

// ----------------------------------------------------------------------------
// ----------------------------------------------------------------------------

class File_monitor_unix: public trackable {
public:

    File_monitor_unix(int wd, const ustring & path):
        wd_(wd),
        path_(path)
    {
    }

   ~File_monitor_unix() {
        signal_destroy_();
    }

    signal<void()> & signal_destroy() {
        return signal_destroy_;
    }

    signal<void(int, ustring)> & signal_notify() {
        return signal_notify_;
    }

    int wd() const {
        return wd_;
    }

    bool on_inotify(int wd, const ustring & p, int mask) {
        if (wd == wd_) {
            ustring s = p.empty() ? path_ : (path_is_absolute(p) ? p : path_build(path_, p));
            signal_notify()(mask, s);
            return true;
        }

        return false;
    }

private:

    int             wd_;
    ustring         path_;
    signal<void()>  signal_destroy_;
    signal<void(int, ustring)> signal_notify_;
};

// ----------------------------------------------------------------------------
// ----------------------------------------------------------------------------

class Loop_unix: public Loop_impl {
public:

    Loop_unix();
    static Loop_unix_ptr this_unix_loop();

    bool is_removable(const ustring & mp);
    void on_mounts();
    void add_poller(Poller_base * ppi, short events);

    // Overrides pure Loop_impl.
    Event_ptr event() override;

    File_monitor_unix_ptr create_file_monitor(const ustring & path, int mask);

protected:

    connection      watcher_cx_;
    Watcher_ptr     wt_;
    Event_ptr       wte_;

protected:

    // Overrides pure Loop_impl.
    bool iterate(int timeout_ms) override;

    // Overrides Loop_impl.
    void init_mounts() override;

    // Overrides Loop_impl.
    void done_mounts() override;

    // Overrides Loop_impl.
    void on_quit() override;

    void on_watcher();

private:

    using Fds = std::vector<struct pollfd>;

    Fds             fds_;
    int             infd_           = -1;
    unsigned        nmonitors_      = 0;            // File monitor count.
    Poller_unix *   infd_poller_    = nullptr;
    Timeval         wttv_;

    signal<bool(int)> signal_chain_poll_;
    signal<bool(int, ustring, int)> signal_chain_notify_;

private:

    void on_poller_destroy(int fd);
    void on_inotify();
    void on_file_monitor_destroy(int wd);
};

} // namespace tau

#endif // __TAU_LOOP_UNIX_HH__

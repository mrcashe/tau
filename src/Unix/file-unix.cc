// ----------------------------------------------------------------------------
// SPDX-License-Identifier: BSD-2-Clause
// Copyright © 2014-2024 Konstantin Shmelkov <mrcashe@gmail.com>.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
// THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
// PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
// CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
// EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
// PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
// OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
// WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
// OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
// EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// ----------------------------------------------------------------------------

#include <tau/exception.hh>
#include <tau/locale.hh>
#include <tau/string.hh>
#include <tau/sys.hh>
#include "file-unix.hh"
#include <sys/stat.h>
#include <errno.h>
#include <unistd.h>
#include <filesystem>
#include <map>
#include <mutex>
#include <iostream>

namespace fs = std::filesystem;

namespace {

using User_icons = std::map<std::string, std::string_view>;

std::recursive_mutex    smx_;
User_icons              uicons_;

} // anonymous namespace

// ----------------------------------------------------------------------------
// ----------------------------------------------------------------------------

namespace tau {

File_unix::File_unix(const ustring & uri) {
    uri_ = uri;
    update_stat();
}

void File_unix::update_stat() {
    flags_ = 0;
    atime_ = ctime_ = mtime_ = 0;
    exists_ = false;
    bytes_ = 0;

    if (uri_is_file(uri_)) {
        try {
            std::wstring ws(path_real(uri_));
            exists_ = fs::exists(ws);

            if (exists_) {
                // if (fs::is_symlink(ws)) { flags_ |= IS_LNK; }    -> doesn't work on libstdc++-13!
                if (fs::is_directory(ws)) { flags_ |= IS_DIR; }
                if (fs::is_fifo(ws)) { flags_ |= IS_FIFO; }
                if (fs::is_regular_file(ws)) { flags_ |= IS_REG; bytes_ = fs::file_size(ws); }
                if (fs::is_block_file(ws)) { flags_ |= IS_BLK; }
                if (fs::is_character_file(ws)) { flags_ |= IS_CHR; }
                if (fs::is_socket(ws)) { flags_ |= IS_SOCK; }

                auto & io = Locale().iocharset();
                std::string path = io.is_utf8() ? std::string(uri_) : io.encode(uri_);
                struct stat st;

                if (0 <= lstat(path.c_str(), &st)) {
                    if (S_ISLNK(st.st_mode)) { flags_ |= IS_LNK; }
                    atime_ = Timeval(uint64_t(1000000)*uint64_t(st.st_atime));
                    ctime_ = Timeval(uint64_t(1000000)*uint64_t(st.st_ctime));
                    mtime_ = Timeval(uint64_t(1000000)*uint64_t(st.st_mtime));
                }
            }
        }

        catch (std::exception & x) {
            std::cerr << "** File: " << x.what() << std::endl;
        }

        catch (...) {
            std::cerr << "** File: unknown exception" << std::endl;
        }
    }
}

// Overrides pure File_impl.
bool File_unix::is_hidden() const noexcept {
    return path_notdir(uri_).starts_with('.');
}

// Overrides pure File_impl.
bool File_unix::is_removable() const noexcept {
    auto loop = Loop_unix::this_unix_loop();

    if (!uri_.empty()) {
        for (const ustring & s: loop->mounts()) {
            for (ustring p = path_real(uri_); "/" != p; p = path_dirname(p)) {
                if (p == s) {
                    return loop->is_removable(p);
                }
            }
        }
    }

    return false;
}

// Overrides pure File_impl.
bool File_unix::is_exec() const noexcept {
    if (exists_) {
        std::wstring ws(path_real(uri_));
        return fs::perms::none != (fs::status(ws).permissions() & (fs::perms::owner_exec|fs::perms::group_exec|fs::perms::others_exec));
    }

    return false;
}

// Overrides pure File_impl.
void File_unix::rm(int opts, slot<void(int)> slot_async) {
    fs::remove_all(std::wstring(uri_));
    exists_ = false;
}

// Overrides pure File_impl.
std::string_view File_unix::user_icon() const {
    std::unique_lock lock(smx_);

    if (uicons_.empty()) {
        uicons_[tau::path_home()] = "folder-home";
        uicons_[tau::path_user_documents_dir()] = "folder-documents";
        uicons_[tau::path_user_desktop_dir()] = "user-desktop";
        uicons_[tau::path_user_downloads_dir()] = "folder-downloads:folder-download";
        uicons_[tau::path_user_music_dir()] = "folder-sound:folder-music";
        uicons_[tau::path_user_pictures_dir()] = "folder-image:folder-images";
        uicons_[tau::path_user_videos_dir()] = "folder-video:folder-videos";
        uicons_[tau::path_user_templates_dir()] = "folder-templates";
    }

    return uicons_[uri_];
}

} // namespace tau

//END

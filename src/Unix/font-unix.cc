// ----------------------------------------------------------------------------
// SPDX-License-Identifier: BSD-2-Clause
// Copyright © 2014-2024 Konstantin Shmelkov <mrcashe@gmail.com>.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
// THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
// PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
// CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
// EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
// PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
// OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
// WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
// OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
// EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// ----------------------------------------------------------------------------

#include <tau/display.hh>
#include "font-face-unix.hh"
#include "font-unix.hh"

namespace tau {

Font_unix::Font_unix(Font_face_ptr face, const ustring & spec, double pts, unsigned dpi):
    face_(face)
{
    dpi_ = dpi;
    double sx = pts*dpi/(72*face->upm());
    mat_.scale(sx);
    ascent_ = sx*face->ascent();
    descent_ = sx*face->descent();
    linegap_ = sx*face->linegap();

    Rect bbox = face->bounds();
    min_.set(sx*bbox.left(), sx*bbox.top());
    max_.set(sx*bbox.right(), sx*bbox.bottom());
    spec_.assign(Font::resize(spec, pts));
}

// Overrides pure Font_impl.
ustring Font_unix::psname() const {
    return face_->psname();
}

// Overrides pure Font_impl.
Glyph_ptr Font_unix::glyph(char32_t wc) const {
    Glyph_ptr g = gmap_[wc];
    if (g) { return g; }
    g = master_glyph(wc)->glyph(mat_);
    g->hint();
    gmap_[wc] = g;
    return g;
}

Glyph_ptr Font_unix::master_glyph(char32_t wc) const {
    return face_->master(wc);
}

} // namespace tau

//END


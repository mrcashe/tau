// ----------------------------------------------------------------------------
// SPDX-License-Identifier: BSD-2-Clause
// Copyright © 2014-2024 Konstantin Shmelkov <mrcashe@gmail.com>.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
// THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
// PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
// CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
// EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
// PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
// OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
// WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
// OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
// EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// ----------------------------------------------------------------------------

#include <tau/exception.hh>
#include "loop-unix.hh"
#include <unistd.h>
#include <fcntl.h>

namespace tau {

Event_unix::Event_unix():
    Event_impl()
{
    if (-1 == ::pipe2(fds_, O_NONBLOCK)) {
        throw sys_error();
    }
}

Event_unix::~Event_unix() {
    close(fds_[1]);
    close(fds_[0]);
    signal_destroy_();
}

// Overrides pure Event_impl.
void Event_unix::emit() {
    Lock lock(mx_);
    char c = '1';
    ssize_t res = write(fds_[1], &c, 1);
    (void)res;
}

// Overrides pure Event_impl.
void Event_unix::release() {
    char buffer[16];
    ssize_t n = sizeof buffer;
    Lock lock(mx_);
    while (n == sizeof buffer) { n = read(fds_[0], buffer, sizeof buffer); }
}

} // namespace tau

//END

// ----------------------------------------------------------------------------
// SPDX-License-Identifier: BSD-2-Clause
// Copyright © 2014-2024 Konstantin Shmelkov <mrcashe@gmail.com>.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
// THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
// PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
// CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
// EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
// PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
// OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
// WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
// OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
// EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// ----------------------------------------------------------------------------

#include <tau/exception.hh>
#include "loop-unix.hh"
#include "sys-unix.hh"
#include <unistd.h>
#include <sys/inotify.h>
#include <cstring>
#include <iostream>

namespace tau {

thread_local tau::Loop_unix_ptr this_loop_;

} // anonymous namespace

namespace tau {

Loop_unix::Loop_unix() {
    wttv_ = Timeval::now();
}

// Overrides Loop_impl.
void Loop_unix::on_quit() {
    if (infd_poller_) { delete infd_poller_; infd_poller_ = nullptr; }
    if (-1 != infd_) { close(infd_); }
    watcher_cx_.drop();
    wte_.reset();
    // this_loop_.reset();
}

// Overrides Loop_impl.
void Loop_unix::init_mounts() {
    wt_ = Watcher_impl::ptr();
    wte_ = event();
    wte_->signal_ready().connect(fun(this, &Loop_unix::on_watcher));
    watcher_cx_ = wt_->signal_event().connect(fun(wte_, &Event_impl::emit));
}

// Overrides Loop_impl.
void Loop_unix::done_mounts() {
    watcher_cx_.drop();
    wt_.reset();
}

// Overrides pure Loop_impl.
bool Loop_unix::iterate(int timeout_ms) {
    int res = poll(fds_.data(), fds_.size(), std::max(1, timeout_ms));

    if (0 < res) {
        for (auto & pfd: fds_) {
            if (pfd.revents) {
                signal_chain_poll_(pfd.fd);
            }
        }
    }

    else if (0 > res) {
        std::cerr << "** " << __func__ << ": " << strerror(errno) << std::endl;
    }

    return res > 0;
}

void Loop_unix::add_poller(Poller_base * ppi, short events) {
    fds_.emplace_back();
    fds_.back().fd = ppi->fd();
    fds_.back().events = events;
    ppi->signal_destroy().connect(bind_back(fun(this, &Loop_unix::on_poller_destroy), ppi->fd()));
    signal_chain_poll_.connect(fun(ppi, &Poller_base::on_poll));
}

// Overrides pure Loop_impl.
Event_ptr Loop_unix::event() {
    auto evp = std::make_shared<Event_unix>();
    evp->signal_ready().connect(fun(evp, &Event_unix::release));
    add_poller(evp.get(), POLLIN);
    return evp;
}

void Loop_unix::on_poller_destroy(int fd) {
    std::erase_if(fds_, [fd](auto & p) { return p.fd == fd; });
}

void Loop_unix::on_watcher() {
    auto now = Timeval::now();
    for (auto & ev: wt_->since(wttv_)) { signal_watch_(ev.flags, ev.mpoint); }
    wttv_ = now;
}

bool Loop_unix::is_removable(const ustring & mp) {
    auto mounts = list_mounts();
    auto i = std::find_if(mounts.begin(), mounts.end(), [mp](auto & m) { return mp == m.mpoint; } );
    return i != mounts.end() && i->removable;
}

void Loop_unix::on_inotify() {
    char buffer[16384];

    for (;;) {
        ssize_t n_read = read(infd_, buffer, sizeof(buffer));
        if (n_read < 1) { break; }
        std::size_t offset = 0, n_bytes = n_read;

        while (offset < n_bytes) {
            inotify_event * kevent = reinterpret_cast<inotify_event *>(buffer+offset);
            std::size_t event_size = sizeof(inotify_event)+kevent->len;

            int mask = File::NOTHING;
            if (IN_ACCESS & kevent->mask) { mask = File::ACCESSED; }
            if (IN_MODIFY & kevent->mask) { mask = File::CHANGED; }
            if (IN_ATTRIB & kevent->mask) { mask = File::ATTRIB; }
            if (IN_CLOSE & kevent->mask) { mask = File::CLOSED; }
            if (IN_OPEN & kevent->mask) { mask = File::OPENED; }
            if (IN_MOVED_FROM & kevent->mask) { mask = File::MOVED_OUT; }
            if (IN_MOVED_TO & kevent->mask) { mask = File::MOVED_IN; }
            if (IN_CREATE & kevent->mask) { mask = File::CREATED; }
            if (IN_DELETE & kevent->mask) { mask = File::DELETED; }
            if (IN_DELETE_SELF & kevent->mask) { mask = File::SELF_DELETED; }
            if (IN_MOVE_SELF & kevent->mask) { mask = File::SELF_MOVED; }
            // Omit IN_UNMOUNT, IN_Q_OVERFLOW and IN_IGNORED.

            if (0 != mask) {
                ustring name;

                if (0 != kevent->len) {
                    std::string s(kevent->name, kevent->len);
                    auto io = Locale().iocharset();
                    name.assign(io.is_utf8() ? ustring(s) : io.decode(s));
                }

                signal_chain_notify_(kevent->wd, name, mask);
            }

            offset += event_size;
        }
    }
}

// Overrides pure Loop.
File_monitor_unix_ptr Loop_unix::create_file_monitor(const ustring & path, int mask) {
    uint32_t umask = 0;
    if (File::ACCESSED & mask) { umask |= IN_ACCESS; }
    if (File::ATTRIB & mask) { umask |= IN_ATTRIB; }
    if (File::CHANGED & mask) { umask |= IN_MODIFY; }
    if (File::OPENED & mask) { umask |= IN_OPEN; }
    if (File::CLOSED & mask) { umask |= IN_CLOSE; }
    if (File::CREATED & mask) { umask |= IN_CREATE; }
    if (File::DELETED & mask) { umask |= IN_DELETE; }
    if (File::MOVED_IN & mask) { umask |= IN_MOVED_TO; }
    if (File::MOVED_OUT & mask) { umask |= IN_MOVED_FROM; }
    if (File::SELF_MOVED & mask) { umask |= IN_MOVE_SELF; }
    if (File::SELF_DELETED & mask) { umask |= IN_DELETE_SELF; }

    int fd = infd_;

    if (fd < 0) {
        fd = inotify_init1(IN_NONBLOCK|IN_CLOEXEC);
        if (fd < 0) { throw sys_error("inotify_init1(): "+path); }
    }

    Encoding enc;
    std::string lfp = enc.is_utf8() ? std::string(path) : enc.encode(path);
    int wd = inotify_add_watch(fd, lfp.c_str(), umask);

    if (wd < 0) {
        sys_error x("inotify_add_watch(): "+path);
        if (infd_ < 0) { close(fd); }
        throw x;
    }

    if (infd_ < 0) {
        infd_ = fd;
        infd_poller_ = new Poller_unix(infd_);
        infd_poller_->signal_poll().connect(fun(this, &Loop_unix::on_inotify));
        add_poller(infd_poller_, POLLIN);
    }

    auto fm = std::make_shared<File_monitor_unix>(wd, path);
    ++nmonitors_;
    signal_chain_notify_.connect(fun(fm, &File_monitor_unix::on_inotify));
    fm->signal_destroy().connect(bind_back(fun(this, &Loop_unix::on_file_monitor_destroy), wd));
    return fm;
}

void Loop_unix::on_file_monitor_destroy(int wd) {
    inotify_rm_watch(infd_, wd);

    if (nmonitors_ && 0 == --nmonitors_) {
        if (infd_poller_) { delete infd_poller_; infd_poller_ = nullptr; }
        close(infd_); infd_ = -1;
    }
}

// static
Loop_ptr Loop_impl::this_ptr() {
    return this_loop_ ? this_loop_ : Loop_unix::this_unix_loop();
}

// static
Loop_unix_ptr Loop_unix::this_unix_loop() {
    if (this_loop_) { return this_loop_; }
    this_loop_ = std::static_pointer_cast<Loop_unix>(Loop_impl::that_ptr(std::this_thread::get_id()));
    if (this_loop_) { return this_loop_; }
    this_loop_ = std::make_shared<Loop_unix>();
    new_loop(this_loop_);
    return this_loop_;
}

} // namespace tau

//END

// ----------------------------------------------------------------------------
// SPDX-License-Identifier: BSD-2-Clause
// Copyright © 2014-2024 Konstantin Shmelkov <mrcashe@gmail.com>.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
// THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
// PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
// CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
// EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
// PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
// OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
// WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
// OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
// EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// ----------------------------------------------------------------------------

/// @file watcher-impl.hh The Watcher_impl class declaration.

#ifndef __TAU_WATCHER_IMPL_HH__
#define __TAU_WATCHER_IMPL_HH__

#include <tau/timeval.hh>
#include <loop-impl.hh>
#include "defs-unix.hh"
#include <mutex>

namespace tau {

struct Watcher_event {
    int             flags;
    ustring         mpoint;
    Watcher_event(int f, const ustring & s): flags(f), mpoint(s) {}
};

/// Watching thread (singleton).
class Watcher_impl: public trackable {
public:

    static Watcher_ptr ptr();
    virtual ~Watcher_impl() = default;
    signal<void()> & signal_event() { return signal_event_; }
    std::vector<Watcher_event> since(tau::Timeval tv);

protected:

    using Events    = std::multimap<uint64_t, Watcher_event>;       // Mount events.

    Mounts          mounts_;
    Events          events_;
    Loop_ptr        lp_;

protected:

    Watcher_impl();
    static Watcher_ptr create();
    void on_mounts();
    void init_pro();

    // Overriden by Watcher_poll.
    // Overriden by Watcher_linux.
    virtual void init() { init_pro(); }

private:

    std::vector<connection> cxs_;
    signal<void()>          signal_event_;
    Timeval                 tv_;
    std::mutex              mmx_;

private:

    void run();
    void on_idle();
    void on_quit();
};

} // namespace tau

#endif // __TAU_WATCHER_IMPL_HH__

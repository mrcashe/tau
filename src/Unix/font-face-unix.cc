// ----------------------------------------------------------------------------
// SPDX-License-Identifier: BSD-2-Clause
// Copyright © 2014-2024 Konstantin Shmelkov <mrcashe@gmail.com>.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
// THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
// PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
// CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
// EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
// PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
// OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
// WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
// OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
// EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// ----------------------------------------------------------------------------

#include "font-face-unix.hh"
#include <true-type-font.hh>

namespace tau {

Font_face::Font_face(True_type_ptr ttf):
    file_(ttf)
{
    zero_ = ttf->zero();
    set_family(ttf->family());
    set_facename(ttf->face());
    set_fontname(ttf->fontname());
    set_psname(ttf->psname());
    set_bounds(ttf->bounds());
    set_upm(ttf->upm());
    set_ascent(ttf->ascent());
    set_descent(ttf->descent());
    set_linegap(ttf->linegap());
    set_max_advance(ttf->max_advance());

    auto [ min_lsb, min_rsb ] = ttf->sidebearing();
    set_min_lsb(min_lsb);
    set_min_rsb(min_rsb);

    set_max_x_extent(ttf->max_x_extent());

    auto [ rise, run ] = ttf->caret_slope();
    set_caret_slope_rise(rise);
    set_caret_slope_run(run);
}

void Font_face::set_family(const ustring & family) {
    family_ = family;
}

void Font_face::set_facename(const ustring & facename) {
    facename_ = facename;
}

void Font_face::set_fontname(const ustring & name) {
    fontname_ = name;
}

void Font_face::set_psname(const ustring & psname) {
    psname_ = psname;
}

void Font_face::set_bounds(const Rect & bbox) {
    bbox_ = bbox;
}

void Font_face::set_upm(unsigned upm) {
    upm_ = upm;
}

void Font_face::set_ascent(int16_t asc) {
    ascent_ = asc;
}

void Font_face::set_descent(int16_t desc) {
    descent_ = desc;
}

void Font_face::set_linegap(int16_t linegap) {
    linegap_ = linegap;
}

void Font_face::set_max_advance(uint16_t adv) {
    max_advance_ = adv;
}

void Font_face::set_min_lsb(int16_t lsb) {
    min_lsb_ = lsb;
}

void Font_face::set_min_rsb(int16_t rsb) {
    min_rsb_ = rsb;
}

void Font_face::set_max_x_extent(int16_t extent) {
    max_x_extent_ = extent;
}

void Font_face::set_caret_slope_rise(bool rise) {
    caret_slope_rise_ = rise;
}

void Font_face::set_caret_slope_run(bool run) {
    caret_slope_run_ = run;
}

void Font_face::preload(char32_t first, char32_t last) {
    std::u32string s(last-first+1, U' ');
    std::size_t i = 0;
    for (char32_t c = first; c <= last; ++c) { s[i++] = c; }
    i = 0;

    for (auto g: file_->glyphs(s)) {
        if (g) {
            glyphs_[s[i++]] = g;
        }
    }
}

Glyph_ptr Font_face::master(char32_t wc) {
    // Preload ASCII glyphs.
    if (glyphs_.empty()) {
        preload(0x0020, 0x007e);
    }

    // Try to uncache.
    auto iter = glyphs_.find(wc);
    if (iter != glyphs_.end()) { return iter->second; }

    // Test if it needed to preload Latin1 extended page.
    if (wc >= 0x00a0 && wc <= 0x00ff && !xlatin_) {
        preload(0x00a0, 0x00ff);
        xlatin_ = true;
    }

    // Test if it needed to preload Unicode pages.
    if (wc >= 0x000100 && wc <= 0x10ffff) {
        if (preloaded_.end() == preloaded_.find(wc & 0xffffff00)) {
            preloaded_.insert(wc & 0xffffff00);
            preload(wc & 0xffffff00, wc | 0x000000ff);
        }
    }

    // Not found, load glyph from file.
    auto gs = file_->glyphs(std::u32string(1, wc));

    // Return fallback glyph.
    if (gs.empty()) { return zero_; }

    // Cache it.
    glyphs_[wc] = gs.front();
    return gs.front();
}

} // namespace tau

//END

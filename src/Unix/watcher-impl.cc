// ----------------------------------------------------------------------------
// SPDX-License-Identifier: BSD-2-Clause
// Copyright © 2014-2024 Konstantin Shmelkov <mrcashe@gmail.com>.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
// THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
// PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
// CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
// EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
// PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
// OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
// WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
// OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
// EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// ----------------------------------------------------------------------------

/// @file watcher-impl.cc The Watcher_impl class implementation.

#include <tau/file.hh>
#include "watcher-impl.hh"
#include <iostream>

namespace tau {

std::mutex          mx_;
tau::Watcher_ptr    wt_;

} // anonymous namespace

// ----------------------------------------------------------------------------
// ----------------------------------------------------------------------------

namespace tau {

Watcher_impl::Watcher_impl() {
    for (auto lp: Loop_impl::list()) {
        auto cx = lp->signal_quit().connect(fun(this, &Watcher_impl::on_quit));
        cx.set_autodrop();
        cxs_.push_back(cx);
    }

    auto thr = std::thread([this] { this->run(); });
    thr.detach();
}

void Watcher_impl::init_pro() {
    lp_ = Loop_impl::this_ptr();
    lp_->signal_idle().connect(fun(this, &Watcher_impl::on_idle));
}

void Watcher_impl::run() {
    init();
    lp_->run();
    std::unique_lock lock(mx_);
    wt_.reset();
}

void Watcher_impl::on_idle() {
    if (wt_ && lp_) {
        if (wt_.use_count() < 2 || Loop_impl::count() < 2) {
            lp_->quit();
        }

        else {
            auto tv = Timeval::now();

            if (tv-tv_ >= 2000000) {
                std::unique_lock lock(mmx_);
                events_.erase(events_.begin(), events_.lower_bound(tv_));
                tv_ = tv;
            }
        }
    }
}

void Watcher_impl::on_quit() {
    if (2 > Loop_impl::count()) { lp_->quit(); }
}

void Watcher_impl::on_mounts() {
    std::unique_lock lock(mmx_);
    auto mounts = list_mounts();

    for (auto & m: mounts) {
        auto i = std::find_if(mounts_.begin(), mounts_.end(), [m](auto & mm) { return m.dev == mm.dev; } );

        if (i == mounts_.end()) {
            mounts_.emplace_back(m);
            int flags = File::MOUNT | (m.removable ? File::REMOVABLE : 0);
            events_.emplace(Timeval::now(), Watcher_event(flags, m.mpoint));
            signal_event_();
        }
    }

    bool umount;

    do {
        umount = false;

        for (auto i = mounts_.begin(); i != mounts_.end(); ++i) {
            auto & mm = *i;
            auto j = std::find_if(mounts.begin(), mounts.end(), [mm](auto & m) { return mm.dev == m.dev; } );

            if (j == mounts.end()) {
                const ustring mpoint(mm.mpoint);
                int flags = File::UMOUNT | (mm.removable ? File::REMOVABLE : 0);
                mounts_.erase(i);
                events_.emplace(Timeval::now(), Watcher_event(flags, mpoint));
                signal_event_();
                umount = true;
                break;
            }
        }
    } while (umount);
}

std::vector<Watcher_event> Watcher_impl::since(tau::Timeval tv) {
    std::vector<Watcher_event> v;
    std::unique_lock lock(mmx_);
    for (auto i = events_.lower_bound(tv); i != events_.end(); ++i) { v.push_back(i->second); }
    return v;
}

// static
Watcher_ptr Watcher_impl::ptr() {
    std::unique_lock lock(mx_);
    if (!wt_) { wt_ = create(); }
    return wt_;
}

} // namespace tau

//END

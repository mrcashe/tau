// ----------------------------------------------------------------------------
// SPDX-License-Identifier: BSD-2-Clause
// Copyright © 2014-2024 Konstantin Shmelkov <mrcashe@gmail.com>.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
// THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
// PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
// CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
// EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
// PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
// OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
// WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
// OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
// EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// ----------------------------------------------------------------------------

#include <tau/exception.hh>
#include <tau/file.hh>
#include <tau/font.hh>
#include <tau/key-file.hh>
#include <tau/string.hh>
#include <tau/timeval.hh>
#include <gettext-impl.hh>
#include <locale-impl.hh>
#include <sys-impl.hh>
#include "sys-unix.hh"
#include "theme-unix.hh"
#include <cerrno>
#include <cstdlib>
#include <cstring>
#include <iostream>
#include <dirent.h>
#include <fnmatch.h>
#include <glob.h>
#include <pwd.h>
#include <unistd.h>
#include <sys/stat.h>
#include <sys/utsname.h>
#include <filesystem>
#include <fstream>
#include <iostream>

namespace tau {

const char32_t keyfile_comment_separator_ = U'#';

char32_t path_slash() noexcept { return U'/'; }
char32_t path_sep() noexcept { return U':'; }

sys_error::sys_error(const ustring & extra_msg) {
    gerror_ = errno;
    msg_ = str_format("system error, code is ", gerror_, ": ", strerror(gerror_));
    if (!extra_msg.empty()) { msg_ += " (" + extra_msg + ")"; }
}

struct tm Timeval::gmtime() const noexcept {
    time_t t(usec_ / 1000000);
    struct tm res;
    gmtime_r(&t, &res);
    return res;
}

struct tm Timeval::localtime() const noexcept {
    time_t t(usec_ / 1000000);
    struct tm res;
    localtime_r(&t, &res);
    return res;
}

ustring path_home() {
    ustring home(str_getenv("HOME"));
    if (!home.empty()) { return home; }

    Encoding enc;
    struct passwd pwd;
    struct passwd* pw = nullptr;
    char buffer[16384];
    const char* logname = getenv("LOGNAME");

    if (!logname || '\0' == *logname) {
        int result = getpwnam_r(logname, &pwd, buffer, sizeof(buffer), &pw);
        if (0 == result && pw && pw->pw_uid == getuid()) { return enc.is_utf8() ? ustring(pw->pw_dir) : enc.decode(pw->pw_dir); }
    }

    int result = getpwuid_r(getuid(), &pwd, buffer, sizeof(buffer), &pw);
    if (0 == result && pw) { return enc.is_utf8() ? ustring(pw->pw_dir) : enc.decode(pw->pw_dir); }
    return ustring(); // FIXME What to do in that case?
}

ustring login_name() {
    struct passwd pwd;
    struct passwd* pw = nullptr;
    char buffer[16384];
    const char* logname = getenv("LOGNAME");
    auto io = Locale().iocharset();

    if (logname) {
        int result = getpwnam_r(logname, &pwd, buffer, sizeof(buffer), &pw);
        if (0 == result && pw && pw->pw_uid == getuid()) { return io.is_utf8() ? ustring(pw->pw_name) : io.decode(pw->pw_name); }
    }

    int result = getpwuid_r(getuid(), &pwd, buffer, sizeof(buffer), &pw);
    if (0 == result && pw) { return io.is_utf8() ? ustring(pw->pw_name) : io.decode(pw->pw_name); }
    return "somebody";
}

ustring path_user_data_dir() {
    ustring dir = str_getenv("XDG_DATA_HOME");
    if (path_is_absolute(dir)) {
        return dir;
    }
    dir = path_home();
    if (!dir.empty()) {
        return path_build(dir, ".local", "share");
    }
    return path_build(path_build(path_tmp(), login_name()), ".local", "share");
}

ustring path_user_config_dir() {
    ustring dir = str_getenv("XDG_CONFIG_HOME");
    if (path_is_absolute(dir)) { return dir; }
    dir = path_home();
    if (!dir.empty()) { return path_build(dir, ".config"); }
    return path_build(path_tmp(), login_name(), ".config");
}

ustring path_user_cache_dir() {
    ustring dir = str_getenv("XDG_CACHE_HOME");
    if (path_is_absolute(dir)) { return dir; }
    dir = path_home();
    if (!dir.empty()) { return path_build(dir, ".cache"); }
    return path_build(path_tmp(), login_name(), ".cache");
}

ustring path_user_runtime_dir() {
    ustring dir = str_getenv("XDG_RUNTIME_DIR");
    if (path_is_absolute(dir)) { return dir; }
    return path_user_cache_dir();
}

ustring path_user_desktop_dir() {
    auto h = path_home(), s = kache_.get_string(kache_.section("XDG User Dirs"), "XDG_DESKTOP_DIR", h);
    return s.empty() || h == s ? path_build(h, lgettext("Desktop")) : s;
}

ustring path_user_documents_dir() {
    auto h = path_home(), s = kache_.get_string(kache_.section("XDG User Dirs"), "XDG_DOCUMENTS_DIR", h);
    return s.empty() || h == s ? path_build(h, lgettext("Documents")) : s;
}

ustring path_user_downloads_dir() {
    auto h = path_home(), s = kache_.get_string(kache_.section("XDG User Dirs"), "XDG_DOWNLOAD_DIR", h);
    return s.empty() || h == s ? path_build(h, lgettext("Downloads")) : s;
}

ustring path_user_music_dir() {
    auto h = path_home(), s = kache_.get_string(kache_.section("XDG User Dirs"), "XDG_MUSIC_DIR", h);
    return s.empty() || h == s ? path_build(h, lgettext("Music")) : s;
}

ustring path_user_pictures_dir() {
    auto h = path_home(), s = kache_.get_string(kache_.section("XDG User Dirs"), "XDG_PICTURES_DIR", h);
    return s.empty() || h == s ? path_build(h, lgettext("Pictures")) : s;
}

ustring path_user_videos_dir() {
    auto h = path_home(), s = kache_.get_string(kache_.section("XDG User Dirs"), "XDG_VIDEOS_DIR", h);
    return s.empty() || h == s ? path_build(h, lgettext("Videos")) : s;
}

ustring path_user_templates_dir() {
    auto h = path_home(), s = kache_.get_string(kache_.section("XDG User Dirs"), "XDG_TEMPLATES_DIR", h);
    return s.empty() || h == s ? path_build(h, lgettext("Templates")) : s;
}

ustring path_tmp() {
    ustring dir = str_getenv("TMPDIR");
    if (path_is_absolute(dir)) { return dir; }
    return "/tmp";
}

ustring path_cwd() {
    ustring dir = str_getenv("PWD");

    if (dir.empty()) {
        char wd[1 + PATH_MAX];

        if (getcwd(wd, PATH_MAX)) {
            Encoding enc;
            dir.assign(enc.is_utf8() ? ustring(wd) : enc.decode(wd));
        }
    }

    return dir;
}

std::vector<ustring> file_list(const ustring & path) {
    auto io = Locale().iocharset();
    std::string lfp = io.is_utf8() ? std::string(path) : io.encode(path);
    DIR* d = opendir(lfp.c_str());
    if (!d) { throw sys_error(); }
    struct dirent* de;
    std::vector<ustring> v;

    do {
        de = readdir(d);
        if (de) {
            v.push_back(io.is_utf8() ? ustring(de->d_name) : io.decode(de->d_name));
        }
    } while (de);

    closedir(d);
    return v;
}

std::vector<ustring> file_glob(const ustring & mask) {
    std::vector<ustring> v;
    glob_t gl;
    auto io = Locale().iocharset();
    std::string lm = io.is_utf8() ? std::string(mask) : io.encode(mask);
    int result = glob(lm.c_str(), GLOB_NOSORT | GLOB_TILDE, 0, &gl);

    if (GLOB_NOSPACE == result) {
        throw std::bad_alloc();
    }

    else if (GLOB_ABORTED == result) {
        throw internal_error("glob(): GLOB_ABORTED returned");
    }

    else if (0 == result) {
        for (std::size_t i = 0; i < gl.gl_pathc; ++i) {
            v.push_back(io.is_utf8() ? ustring(gl.gl_pathv[i]) : io.decode(gl.gl_pathv[i]));
        }
    }

    globfree(&gl);
    return v;
}

void file_mkdir(const ustring & path) {
    if (!file_exists(path)) {
        const ustring parent = path_dirname(path);
        if (file_exists(parent) && !file_is_dir(parent)) { throw user_error(str_format("file '", parent, "' exists but not a directory")); }
        else { file_mkdir(parent); }
        auto io = Locale().iocharset();
        std::string lfp = io.is_utf8() ? std::string(path) : io.encode(path);
        if (0 != mkdir(lfp.c_str(), 0755)) { throw sys_error(path); }
    }

    else if (!file_is_dir(path)) {
        throw user_error(str_format("file '", path, "' exists but not a directory"));
    }
}

ustring path_dirname(const ustring & path) {
    ustring::size_type pos = path.find_last_of("/\\");
    if (ustring::npos != pos) { return 0 != pos ? path.substr(0, pos) : "/"; }
    return ".";
}

ustring path_real(const ustring & path) {
    auto io = Locale().iocharset();
    std::string lfp = io.encode(path);
    char * res_path = realpath(lfp.c_str(), nullptr);

    if (res_path) {
        ustring res = io.decode(res_path);
        std::free(res_path);
        return res;
    }

    return path;
}

bool path_match(const ustring & pattern, const ustring & path) {
    Locale loc;
    auto io = Locale().iocharset();
    std::string lp = io.encode(pattern);
    std::string lfp = io.encode(path);
    return 0 == fnmatch(lp.c_str(), lfp.c_str(), FNM_PATHNAME | FNM_PERIOD);
}

ustring path_which(const ustring & cmd) {
    if (cmd.npos == cmd.find(path_slash())) {
        for (auto & s : str_explode(str_getenv("PATH"), path_sep())) {
            ustring path = path_build(s, cmd);
            if (File(path).is_exec()) { return path; }
        }
    }

    return ustring();
}

void msleep(unsigned time_ms) {
    usleep(1000 * time_ms);
}

ustring pipe_in(const ustring & cmd) {
    auto enc = Locale().encoding();

    if (FILE * is = popen(enc.encode(cmd).data(), "r")) {
        char buffer[256]; std::size_t n; std::string os;

        do {
            n = std::fread(buffer, 1, sizeof buffer, is);
            os.append(buffer, n);
        } while (n == sizeof buffer);

        pclose(is); return enc.decode(os);
    }

    throw sys_error();
}

void Locale_impl::init1() {
    const char * s = getenv("LANG");
    if (!s) { s = getenv("LANGUAGE"); }
    if (!s) { s = "C"; }

    spec = s;
    auto begin = spec.find('.');

    if (spec.npos != begin) {
        ++begin;
        auto end = spec.find('@');
        iocharset = enc = Encoding(spec.substr(begin, end-begin));
    }
}

ustring::operator std::wstring() const {
    std::wstring ws;
    for (const char* p = str_.c_str(); '\0' != *p; p = utf8_next(p)) { ws.push_back(char32_from_pointer(p)); }
    return ws;
}

ustring & ustring::assign(const std::wstring & src) {
    str_from_utf32(*this, reinterpret_cast<const char32_t*>(src.c_str()), src.size());
    return *this;
}

ustring & ustring::assign(const wchar_t * src) {
    str_from_utf32(*this, reinterpret_cast<const char32_t*>(src), wcslen(src));
    return *this;
}

ustring & ustring::assign(const wchar_t * src, size_type n) {
    str_from_utf32(*this, reinterpret_cast<const char32_t*>(src), n);
    return *this;
}

bool path_same(const ustring & first_path, const ustring & second_path) {
    auto v1 = path_explode(path_real(first_path)), v2 = path_explode(path_real(second_path));
    return std::equal(v1.begin(), v1.end(), v2.begin(), v2.end());
}

ustring uri_scheme(const ustring & s) {
    std::size_t pos = s.find_first_of(":/?#");
    return pos != s.npos && ':' == s[pos] ? s.substr(0, pos) : ustring();
}

ustring uri_notscheme(const ustring & uri) {
    std::size_t pos = uri.find_first_of(":/?#");
    return pos != uri.npos && ':' == uri[pos] ? uri.substr(':' == uri[pos] ? 1+pos : pos) : ustring();
}

bool uri_is_file(const ustring & uri) {
    auto sch = uri_scheme(uri);
    return !uri.empty() && (sch.empty() || str_similar("file", sch));
}

// Declared in sys-impl.hh
std::vector<ustring> list_mount_points() {
    auto ms = list_mounts();
    std::vector<ustring> v(ms.size());
    std::size_t n = 0;
    for (auto & m: ms) { v[n++] = m.mpoint; }
    return v;
}

// Called from boot_sys() of certain platform.
void boot_unix() {
    struct utsname un;

    if (0 == uname(&un)) {
        sysinfo_.uname = str_format(un.sysname, ' ', un.release, ' ', un.version);
    }

    else {
        sysinfo_.uname = pipe_in("uname -a");
        auto v = str_explode(sysinfo_.uname);

        if (v.size() > 2) {
            auto w = str_explode(v[2], '.');
            if (w.size() > 0) { sysinfo_.osmajor = std::atoi(w[0].c_str()); }
            if (w.size() > 1) { sysinfo_.osminor = std::atoi(w[1].c_str()); }
        }
    }

    sysinfo_.locale = Locale().name();
    sysinfo_.iocharset = Locale().iocharset().name();

    // XDG User Dirs
    auto path = path_build(path_home(), ".config", "user-dirs.dirs");
    File fi(path);
    auto & sect = kache_.section("XDG User Dirs");
    uint64_t last = kache_.get_integer(sect, "mtime"), mtime = fi.mtime();

    if (last < mtime && 0 != mtime) {
        Key_file kf(path);
        kache_.set_string(sect, "XDG_DESKTOP_DIR", str_envsubst(str_trimright(kf.get_string("XDG_DESKTOP_DIR"), "/")));
        kache_.set_string(sect, "XDG_DOWNLOAD_DIR", str_envsubst(str_trimright(kf.get_string("XDG_DOWNLOAD_DIR"), "/")));
        kache_.set_string(sect, "XDG_TEMPLATES_DIR", str_envsubst(str_trimright(kf.get_string("XDG_TEMPLATES_DIR"), "/")));
        kache_.set_string(sect, "XDG_DOCUMENTS_DIR", str_envsubst(str_trimright(kf.get_string("XDG_DOCUMENTS_DIR"), "/")));
        kache_.set_string(sect, "XDG_MUSIC_DIR", str_envsubst(str_trimright(kf.get_string("XDG_MUSIC_DIR"), "/")));
        kache_.set_string(sect, "XDG_PICTURES_DIR", str_envsubst(str_trimright(kf.get_string("XDG_PICTURES_DIR"), "/")));
        kache_.set_string(sect, "XDG_VIDEOS_DIR", str_envsubst(str_trimright(kf.get_string("XDG_VIDEOS_DIR"), "/")));
        kache_.set_integer(sect, "mtime", mtime);
    }
}

ustring str_getenv(const ustring & env, const ustring & fallback) {
    ustring res = fallback;

    if (char * val = getenv(str_replace(env, "$(){}", "").data())) {
        res.assign(str_trimleft(str_trimright((Locale().iocharset().decode(val)))));
    }

    return res;
}

std::optional<std::vector<uint8_t>> shared_resource(const ustring & resource_name) {
    return shared_file(resource_name);
}

} // namespace tau

// END

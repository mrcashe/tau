// ----------------------------------------------------------------------------
// SPDX-License-Identifier: BSD-2-Clause
// Copyright © 2014-2024 Konstantin Shmelkov <mrcashe@gmail.com>.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
// THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
// PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
// CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
// EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
// PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
// OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
// WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
// OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
// EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// ----------------------------------------------------------------------------

#ifndef __TAU_FONT_FACE_HH__
#define __TAU_FONT_FACE_HH__

#include <tau/font.hh>
#include <glyph-impl.hh>
#include "true-type-font.hh"
#include <map>
#include <set>

namespace tau {

class Font_face {
public:

    Font_face(True_type_ptr ttf);
    Font_face(const Font_face & other) = delete;
    Font_face(Font_face && other) = delete;
    Font_face & operator=(const Font_face & other) = delete;
    Font_face & operator=(Font_face && other) = delete;
   ~Font_face() {}

    True_type_ptr font_file() { return file_; }
    True_type_cptr font_file() const { return file_; }

    ustring family() const {
        return family_;
    }

    ustring facename() const {
        return facename_;
    }

    ustring fontname() const {
        return fontname_;
    }

    ustring psname() const {
        return psname_;
    }

    Rect bounds() const {
        return bbox_;
    }

    unsigned upm() const {
        return upm_;
    }

    int ascent() const {
        return ascent_;
    }

    int descent() const {
        return descent_;
    }

    int linegap() const {
        return linegap_;
    }

    int max_advance() const {
        return max_advance_;
    }

    int min_lsb() const {
        return min_lsb_;
    }

    int min_rsb() const {
        return min_rsb_;
    }

    int max_x_extent() const {
        return max_x_extent_;
    }

    bool caret_slope_rise() const {
        return caret_slope_rise_;
    }

    bool caret_slope_run() const {
        return caret_slope_run_;
    }

    Glyph_ptr master(char32_t wc);

    void set_family(const ustring & family);
    void set_facename(const ustring & facename);
    void set_fontname(const ustring & name);
    void set_psname(const ustring & psname);
    void set_bounds(const Rect & bounds);
    void set_upm(unsigned upm);
    void set_ascent(int16_t asc);
    void set_descent(int16_t desc);
    void set_linegap(int16_t lgap);
    void set_max_advance(uint16_t adv);
    void set_min_lsb(int16_t lsb);
    void set_min_rsb(int16_t rsb);
    void set_max_x_extent(int16_t ext);
    void set_caret_slope_rise(bool rise);
    void set_caret_slope_run(bool run);

private:

    void preload(char32_t first, char32_t last);

private:

    using Glyphs        = std::map<char32_t, Glyph_ptr>;

    ustring             family_;
    ustring             facename_;
    ustring             fontname_;
    ustring             psname_;

    Rect                bbox_;
    unsigned            upm_                = 0;
    int                 ascent_             = 0;
    int                 descent_            = 0;
    int                 linegap_            = 0;
    int                 max_advance_        = 0;
    int                 min_lsb_            = 0;
    int                 min_rsb_            = 0;
    int                 max_x_extent_       = 0;
    bool                caret_slope_rise_   = false;
    bool                caret_slope_run_    = false;
    bool                xlatin_             = false;
    Glyphs              glyphs_;
    True_type_ptr       file_;
    Glyph_ptr           zero_;
    std::set<char32_t>  preloaded_;
};

} // namespace tau

#endif // __TAU_FONT_FACE_HH__

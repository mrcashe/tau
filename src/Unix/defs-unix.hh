// ----------------------------------------------------------------------------
// SPDX-License-Identifier: BSD-2-Clause
// Copyright © 2014-2024 Konstantin Shmelkov <mrcashe@gmail.com>.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
// THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
// PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
// CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
// EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
// PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
// OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
// WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
// OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
// EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// ----------------------------------------------------------------------------

#ifndef __TAU_DEFS_UNIX_HH__
#define __TAU_DEFS_UNIX_HH__

#include <defs-impl.hh>

namespace tau {

class Display_unix;
using Display_unix_ptr = std::shared_ptr<Display_unix>;
using Display_unix_cptr = std::shared_ptr<const Display_unix>;

class File_monitor_unix;
using File_monitor_unix_ptr = std::shared_ptr<File_monitor_unix>;
using File_monitor_unix_cptr = std::shared_ptr<const File_monitor_unix>;

class Font_face;
using Font_face_ptr = std::shared_ptr<Font_face>;
using Font_face_cptr = std::shared_ptr<const Font_face>;

class Loop_unix;
using Loop_unix_ptr = std::shared_ptr<Loop_unix>;
using Loop_unix_cptr = std::shared_ptr<const Loop_unix>;

class Theme_unix;
using Theme_unix_ptr = std::shared_ptr<Theme_unix>;
using Theme_unix_cptr = std::shared_ptr<const Theme_unix>;

class Watcher_impl;
using Watcher_ptr = std::shared_ptr<Watcher_impl>;
using Watcher_cptr = std::shared_ptr<const Watcher_impl>;

class Watcher_poll;
using Watcher_poll_ptr = std::shared_ptr<Watcher_poll>;
using Watcher_poll_cptr = std::shared_ptr<const Watcher_poll>;

} // namespace tau

#endif // __TAU_DEFS_UNIX_HH__

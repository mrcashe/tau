// ----------------------------------------------------------------------------
// SPDX-License-Identifier: BSD-2-Clause
// Copyright © 2014-2024 Konstantin Shmelkov <mrcashe@gmail.com>.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
// THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
// PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
// CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
// EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
// PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
// OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
// WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
// OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
// EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// ----------------------------------------------------------------------------

/// @file frame-impl.hh The Frame_impl class (wrapped Frame) declaration.
/// Implementation is in frame-impl.cc file.

#ifndef __TAU_FRAME_IMPL_HH__
#define __TAU_FRAME_IMPL_HH__

#include <tau/enums.hh>
#include <tau/color.hh>
#include <flat-container.hh>

namespace tau {

class Frame_impl: public Flat_container {
public:

    Frame_impl();
    Frame_impl(const ustring & label, Align align=Align::CENTER);
    Frame_impl(Border bs, unsigned border_width=0, unsigned border_radius=0);
    Frame_impl(Border bs, unsigned border_width, unsigned rtop_left, unsigned rtop_right, unsigned rbottom_right, unsigned rbottom_left);
    Frame_impl(Border bs, const Color & border_color, unsigned border_width=0, unsigned border_radius=0);
    Frame_impl(Border bs, const Color & border_color, unsigned border_width, unsigned rtop_left, unsigned rtop_right, unsigned rbottom_right, unsigned rbottom_left);
    Frame_impl(const ustring & label, Border bs, unsigned border_width=0, unsigned border_radius=0);
    Frame_impl(const ustring & label, Border bs, unsigned border_width, unsigned rtop_left, unsigned rtop_right, unsigned rbottom_right, unsigned rbottom_left);
    Frame_impl(const ustring & label, Border bs, const Color & border_color, unsigned border_width=0, unsigned border_radius=0);
    Frame_impl(const ustring & label, Border bs, const Color & border_color, unsigned border_width, unsigned rtop_left, unsigned rtop_right, unsigned rbottom_right, unsigned rbottom_left);
    Frame_impl(const ustring & label, Align align, Border bs, unsigned border_width=0, unsigned border_radius=0);
    Frame_impl(const ustring & label, Align align, Border bs, unsigned border_width, unsigned rtop_left, unsigned rtop_right, unsigned rbottom_right, unsigned rbottom_left);
    Frame_impl(const ustring & label, Align align, Border bs, const Color & border_color, unsigned border_width=0, unsigned border_radius=0);
    Frame_impl(const ustring & label, Align align, Border bs, const Color & border_color, unsigned border_width, unsigned rtop_left, unsigned rtop_right, unsigned rbottom_right, unsigned rbottom_left);
   ~Frame_impl();

    void insert(Widget_ptr wp);
    void clear();
    bool empty() const noexcept { return nullptr == cp_; }

    void set_label(const ustring & text, Align align=Align::CENTER);
    void set_label(const ustring & label, Side label_pos, Align align=Align::CENTER);
    void set_label(Widget_ptr wp, Align align=Align::CENTER);
    void set_label(Widget_ptr wp, Side label_pos, Align align=Align::CENTER);

    void align_label(Align align);
    Align label_align() const noexcept { return align_; }
    void unset_label();
    void move_label(Side label_pos);
    Side where_label() const noexcept { return lpos_; }
    bool has_label() const noexcept { return nullptr != label_; }
    Widget_ptr label() noexcept { return chptr(label_); }
    Widget_cptr label() const noexcept { return chptr(label_); };
    Widget_ptr widget() noexcept { return chptr(cp_); }
    Widget_cptr widget() const noexcept { return chptr(cp_); };

    bool border_visible() const noexcept;

    void set_border_left(unsigned npx);
    void set_border_right(unsigned npx);
    void set_border_top(unsigned npx);
    void set_border_bottom(unsigned npx);

    void set_border_left(unsigned px, Border bs);
    void set_border_right(unsigned px, Border bs);
    void set_border_top(unsigned px, Border bs);
    void set_border_bottom(unsigned px, Border bs);

    void set_border_left(unsigned px, Border bs, const Color & color);
    void set_border_right(unsigned px, Border bs, const Color & color);
    void set_border_top(unsigned px, Border bs, const Color & color);
    void set_border_bottom(unsigned px, Border bs, const Color & color);

    void set_border(unsigned px);
    void set_border(unsigned left, unsigned right, unsigned top, unsigned bottom);
    void set_border(unsigned px, Border bs);
    void set_border(unsigned px, Border bs, unsigned radius) { set_border(px, bs, radius, radius, radius, radius); }
    void set_border(unsigned px, Border bs, unsigned rtop_left, unsigned rtop_right, unsigned rbottom_right, unsigned rbottom_left);
    void set_border(unsigned px, Border bs, const Color & color);
    void set_border(unsigned px, Border bs, const Color & color, unsigned radius) { set_border(px, bs, color, radius, radius, radius, radius); }
    void set_border(unsigned px, Border bs, const Color & color, unsigned rtop_left, unsigned rtop_right, unsigned rbottom_right, unsigned rbottom_left);

    // Overridden by Counter_impl.
    virtual void set_border_style(Border bs) { set_border_style_priv(bs); }

    void set_border_style(Border left, Border right, Border top, Border bottom);
    void set_border_left_style(Border bs);
    void set_border_right_style(Border bs);
    void set_border_top_style(Border bs);
    void set_border_bottom_style(Border bs);

    void set_border_color(const Color & color);
    void set_border_color(const Color & left, const Color & right, const Color & top, const Color & bottom);
    void set_border_left_color(const Color & color);
    void set_border_right_color(const Color & color);
    void set_border_top_color(const Color & color);
    void set_border_bottom_color(const Color & color);

    void unset_border_color();
    void unset_border_left_color();
    void unset_border_right_color();
    void unset_border_top_color();
    void unset_border_bottom_color();

    unsigned border_left() const noexcept { return eb_.left; }
    unsigned border_right() const noexcept { return eb_.right; }
    unsigned border_top() const noexcept { return eb_.top; }
    unsigned border_bottom() const noexcept { return eb_.left; }

    Color border_left_color() const noexcept { return *border_left_color_; }
    Color border_right_color() const noexcept { return *border_right_color_; }
    Color border_top_color() const noexcept { return *border_top_color_; }
    Color border_bottom_color() const noexcept { return *border_bottom_color_; }

    Border border_left_style() const noexcept { return border_left_style_; }
    Border border_right_style() const noexcept { return border_right_style_; }
    Border border_top_style() const noexcept { return border_top_style_; }
    Border border_bottom_style() const noexcept { return border_bottom_style_; }

    // Overriden by Button_base_impl.
    virtual void set_border_radius(unsigned radius);

    // Overriden by Button_base_impl.
    virtual void set_border_radius(unsigned top_left, unsigned top_right, unsigned bottom_right, unsigned bottom_left);

    // Overriden by Button_base_impl.
    virtual void set_border_top_left_radius(unsigned radius);

    // Overriden by Button_base_impl.
    virtual void set_border_top_right_radius(unsigned radius);

    // Overriden by Button_base_impl.
    virtual void set_border_bottom_right_radius(unsigned radius);

    // Overriden by Button_base_impl.
    virtual void set_border_bottom_left_radius(unsigned radius);

    // Overriden by Button_base_impl.
    virtual unsigned border_top_left_radius() const noexcept { return rtop_left_; }

    // Overriden by Button_base_impl.
    virtual unsigned border_top_right_radius() const noexcept { return rtop_right_; }

    // Overriden by Button_base_impl.
    virtual unsigned border_bottom_right_radius() const noexcept { return rbottom_right_; }

    // Overriden by Button_base_impl.
    virtual unsigned border_bottom_left_radius() const noexcept { return rbottom_left_; }

    signal<void()> & signal_border_changed();

    Container_impl * compound() noexcept override { return this; };
    const Container_impl * compound() const noexcept override { return this; };

protected:

    Widget_impl *   cp_                         = nullptr;

protected:

    // Overrides Widget_impl.
    bool on_backpaint(Painter pr, const Rect & inval) override;

private:

    Widget_impl *   label_                      = nullptr;
    Align           align_                      = Align::CENTER;
    Side            lpos_                       = Side::TOP;
    Rect            lb_;                        // Label bounds.

    // Border sizes set by user.
    unsigned        left_                       = 0;
    unsigned        right_                      = 0;
    unsigned        top_                        = 0;
    unsigned        bottom_                     = 0;

    Margin          eb_;                        // Actual borders.

    // Border sizes + label width/height.
    // Available only after arrange() call.
    unsigned        lleft_                      = 0;
    unsigned        lright_                     = 0;
    unsigned        ltop_                       = 0;
    unsigned        lbottom_                    = 0;

    // Border radius.
    unsigned        rtop_left_                  = 0;
    unsigned        rtop_right_                 = 0;
    unsigned        rbottom_right_              = 0;
    unsigned        rbottom_left_               = 0;

    Opt_color       border_left_color_;
    Opt_color       border_right_color_;
    Opt_color       border_top_color_;
    Opt_color       border_bottom_color_;

    Border          border_left_style_          = Border::NONE;
    Border          border_right_style_         = Border::NONE;
    Border          border_top_style_           = Border::NONE;
    Border          border_bottom_style_        = Border::NONE;

    signal<void()> * signal_border_changed_     = nullptr;

    connection      label_hints_cx_             { true };
    connection      child_hints_cx_             { true };
    connection      child_focus_cx_             { true };
    connection      child_back_cx_              { true };

private:

    void init();
    void init_border_style(Border bs, unsigned width, unsigned rtop_left, unsigned rtop_right, unsigned rbottom_right, unsigned rbottom_left);
    void set_border_style_priv(Border bs);
    void update_requisition(Hints op=Hints::SIZE);
    Size child_requisition(Widget_impl * wp);
    unsigned min_border_size(Border bs);
    unsigned ceil_border(unsigned px, Border bs);
    void update_label_foreground();

    // Returns true if changed.
    bool set_left_style(Border bs);
    bool set_right_style(Border bs);
    bool set_top_style(Border bs);
    bool set_bottom_style(Border bs);
    bool set_style(Border bs);

    // Returns true if changed.
    bool set_left_border(unsigned px);
    bool set_right_border(unsigned px);
    bool set_top_border(unsigned px);
    bool set_bottom_border(unsigned px);
    bool set_borders(unsigned px);

    // Returns true if changed.
    bool set_radius(unsigned rtop_left, unsigned rtop_right, unsigned rbottom_right, unsigned rbottom_left);
    bool set_radius_top_left(unsigned radius);
    bool set_radius_top_right(unsigned radius);
    bool set_radius_bottom_right(unsigned radius);
    bool set_radius_bottom_left(unsigned radius);

    // Returns true if invalidate() call needed.
    bool set_left_color(const Color & color);
    bool set_right_color(const Color & color);
    bool set_top_color(const Color & color);
    bool set_bottom_color(const Color & color);
    bool set_colors(const Color & color);

    void on_label_requisition(Hints op);
    bool on_paint(Painter pr, const Rect & inval);

    void render_border(Painter pr);
    void render_background(Painter pr, const Rect & inval);
    void arrange();

    Margin eborders() const;                    // Calculates effective border width.
    Margin placement(const Margin & eb) const;  // Calculates child placement.

    // Gets effective radius.
    unsigned rtop_left() const;
    unsigned rtop_right() const;
    unsigned rbottom_right() const;
    unsigned rbottom_left() const;
};

} // namespace tau

#endif // __TAU_FRAME_IMPL_HH__

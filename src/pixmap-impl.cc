// ----------------------------------------------------------------------------
// SPDX-License-Identifier: BSD-2-Clause
// Copyright © 2014-2024 Konstantin Shmelkov <mrcashe@gmail.com>.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
// THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
// PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
// CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
// EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
// PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
// OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
// WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
// OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
// EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// ----------------------------------------------------------------------------

#include <tau/color.hh>
#include <tau/exception.hh>
#include <tau/sys.hh>
#include <tau/string.hh>
#include <pixmap-impl.hh>
#include <iostream>
#include <set>

namespace {

tau::signal<tau::Pixmap_ptr (const tau::ustring & path)> signal_load_from_file_;
tau::signal<tau::Pixmap_ptr (const uint8_t * raw, std::size_t nbytes)> signal_load_from_memory_;
tau::signal<bool(const tau::ustring & path)> signal_file_supported_;

} // anonymous namespace

namespace tau {

void Pixmap_impl::copy(const Pixmap_impl * other) {
    int iwidth = other->size().width(), iheight = other->size().height();

    for (int y = 0; y < iheight; ++y) {
        for (int x = 0; x < iwidth; ++x) {
            put_pixel({ x, y }, other->get_pixel(Point(x, y)));
        }
    }
}

Pixmap_ptr Pixmap_impl::dup() const {
    Pixmap_ptr pix = create(depth(), size());
    pix->copy(this);
    return pix;
}

// static
Pixmap_ptr Pixmap_impl::create(const uint8_t * raw, std::size_t nbytes) {
    try {
        if (auto pix = signal_load_from_memory_(raw, nbytes)) {
            return pix;
        }
    }

    catch (bad_pixmap & x) {
        std::cerr << "** Pixmap_impl::create(): " << x.what() << std::endl;
    }

    throw bad_pixmap("Pixmap_impl::create(): can't detect pixmap format");
}

// static
Pixmap_ptr Pixmap_impl::load_from_file(const ustring & path) {
    try {
        if (auto pix = signal_load_from_file_(path)) {
            return pix;
        }
    }

    catch (bad_pixmap & x) {
        std::cerr << "** Pixmap_impl::create(): " << x.what() << std::endl;
    }

    throw bad_pixmap(str_format("Pixmap_impl::load_from_file(): ", path, ": can't detect pixmap format"));
}

// static
signal<bool(const ustring & path)> & Pixmap_impl::signal_file_supported() {
    return signal_file_supported_;
}

// static
signal<Pixmap_ptr (const uint8_t * raw, std::size_t nbytes)> & Pixmap_impl::signal_load_from_memory() {
    return signal_load_from_memory_;
}

// static
signal<Pixmap_ptr (const ustring & path)> & Pixmap_impl::signal_load_from_file() {
    return signal_load_from_file_;
}

} // namespace tau

//END

// ----------------------------------------------------------------------------
// SPDX-License-Identifier: BSD-2-Clause
// Copyright © 2014-2024 Konstantin Shmelkov <mrcashe@gmail.com>.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
// THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
// PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
// CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
// EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
// PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
// OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
// WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
// OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
// EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// ----------------------------------------------------------------------------

#include <tau/header.hh>
#include <tau/list.hh>
#include <header-impl.hh>
#include <list-impl.hh>

namespace tau {

#define HEADER_IMPL (std::static_pointer_cast<Header_impl>(impl))

Header::Header(List_base & list, bool resize_allowed):
    Widget(std::make_shared<Header_impl>(std::dynamic_pointer_cast<List_impl>(list.ptr()), resize_allowed))
{
}

Header::Header(Separator::Style sep_style, List_base & list, bool resize_allowed):
    Widget(std::make_shared<Header_impl>(sep_style, std::dynamic_pointer_cast<List_impl>(list.ptr()), resize_allowed))
{
}

Header::Header(const Header & other):
    Widget(other.impl)
{
}

Header & Header::operator=(const Header & other) {
    Widget::operator=(other);
    return *this;
}

Header::Header(Header && other):
    Widget(other.impl)
{
}

Header & Header::operator=(Header && other) {
    Widget::operator=(other);
    return *this;
}

Header::Header(Widget_ptr wp):
    Widget(std::dynamic_pointer_cast<Header_impl>(wp))
{
}

Header & Header::operator=(Widget_ptr wp) {
    Widget::operator=(std::dynamic_pointer_cast<Header_impl>(wp));
    return *this;
}

void Header::show_column(int column) {
    HEADER_IMPL->show_column(column);
}

void Header::show_column(int column, const ustring & title, Align align) {
    HEADER_IMPL->show_column(column, title, align);
}

void Header::show_column(int column, Widget & title) {
    HEADER_IMPL->show_column(column, title.ptr());
}

void Header::hide_column(int column) {
    HEADER_IMPL->hide_column(column);
}

void Header::allow_resize() {
    HEADER_IMPL->allow_resize();
}

void Header::disallow_resize() {
    HEADER_IMPL->disallow_resize();
}

bool Header::resize_allowed() const noexcept {
    return HEADER_IMPL->resize_allowed();
}

void Header::show_sort_marker(int column, bool descend) {
    HEADER_IMPL->show_sort_marker(column, descend);
}

void Header::hide_sort_marker() {
    HEADER_IMPL->hide_sort_marker();
}

void Header::set_separator_style(Separator::Style sep_style) {
    HEADER_IMPL->set_separator_style(sep_style);
}

Separator::Style Header::separator_style() const noexcept {
    return HEADER_IMPL->separator_style();
}

signal<void(int)> & Header::signal_click() {
    return HEADER_IMPL->signal_click();
}

signal<void(int)> & Header::signal_width_changed() {
    return HEADER_IMPL->signal_width_changed();
}

} // namespace tau

//END

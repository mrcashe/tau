// ----------------------------------------------------------------------------
// SPDX-License-Identifier: BSD-2-Clause
// Copyright © 2014-2024 Konstantin Shmelkov <mrcashe@gmail.com>.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
// THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
// PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
// CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
// EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
// PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
// OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
// WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
// OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
// EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// ----------------------------------------------------------------------------

#include <tau/buffer.hh>
#include <tau/enums.hh>
#include <tau/exception.hh>
#include <tau/file.hh>
#include <tau/sys.hh>
#include <tau/key-file.hh>
#include <tau/string.hh>
#include <gettext-impl.hh>
#include <sys-impl.hh>
#include <algorithm>
#include <atomic>
#include <cstdlib>
#include <fstream>
#include <iostream>
#include <mutex>

namespace {

void chk_name(const void * p, std::string_view name) {
    if (name.empty()) {
        throw tau::user_error(tau::str_format(p, " Key_file: got empty section or key name"));
    }
}

} // anonymous namespace

// ----------------------------------------------------------------------------
// ----------------------------------------------------------------------------

namespace tau {

struct Key_file_impl;
using Mutex = std::recursive_mutex;

struct Key_section {
    ustring comment_;
    using Pair                  = std::pair<std::string, ustring>;
    using Keys                  = std::list<Pair>;
    Keys elems_;
    const Key_file_impl * kf_   = nullptr;
    std::string name_;

    Key_section(const Key_file_impl * kf, std::string_view name): kf_(kf), name_(name) {}
    Key_section() = default;
    Key_section(const Key_section & other) = default;
    Key_section(Key_section && other) = default;
    Key_section & operator=(const Key_section & other) = default;
    Key_section & operator=(Key_section && other) = default;

    std::vector<std::string> list_keys() const {
        std::vector<std::string> v(elems_.size());
        auto i = v.begin();
        for (auto & p: elems_) { *i++ = p.first; }
        return v;
    }

    // Return true if changed.
    bool set_value(std::string_view name, const ustring & val) {
        auto i = std::find_if(elems_.begin(), elems_.end(), [name](auto & p) { return name == p.first; } );

        if (i == elems_.end()) {
            elems_.emplace_back(name, val);
            return true;
        }

        else {
            if (i->second != val) {
                if (!val.empty()) { i->second = val; }  // FIXME test it! maybe it's bad idea to erase empty key?
                else { elems_.erase(i); }
                return true;
            }
        }

        return false;
    }

    bool has_key(std::string_view key_name, bool similar) const {
        return similar
            ? std::any_of(elems_.begin(), elems_.end(), [key_name](auto & p) { return str_similar(key_name, p.first); } )
            : elems_.end() != std::find_if(elems_.begin(), elems_.end(), [key_name](auto & p) { return key_name == p.first; } );
    }

    Keys::iterator find(std::string_view key, bool similar=false) {
        if (similar) {
            return std::find_if(elems_.begin(), elems_.end(), [key](auto & p) { return str_similar(key, p.first); } );
        }

        else {
            auto i = std::find_if(elems_.begin(), elems_.end(), [key](auto & p) { return key == p.first; } );
            return i;
        }
    }

    Keys::const_iterator find(std::string_view key, bool similar=false) const {
        if (similar) {
            return std::find_if(elems_.begin(), elems_.end(), [key](auto & p) { return str_similar(key, p.first); } );
        }

        else {
            auto i = std::find_if(elems_.begin(), elems_.end(), [key](auto & p) { return key == p.first; } );
            return i;
        }
    }

    std::string key_name(std::string_view name) const {
        auto i = std::find_if(elems_.begin(), elems_.end(), [name](auto & p) { return str_similar(name, p.first); } );
        return i != elems_.end() ? i->first : std::string();
    }

    ustring str(char csep) const {
        ustring os;

        if (!comment_.empty()) {
            for (auto & s: str_explode(comment_, Locale().newlines())) {
                os += str_format(csep, ' ', s, '\n');
            }
        }

        if (!elems_.empty()) {
            for (auto & e: elems_) {
                os += str_format(e.first, '=', e.second, '\n');
            }

            os += '\n';
        }

        return os;
    }

    void clear() {
        elems_.clear();
        comment_.clear();
    }

    bool empty() const noexcept {
        return comment_.empty() && elems_.empty();
    }
};

// ----------------------------------------------------------------------------
// ----------------------------------------------------------------------------

struct Key_file_impl: public trackable {
    using Pair              = std::pair<std::string, Key_section>;
    using Sections          = std::list<Pair>;

    mutable Mutex           mx_;
    std::atomic_char32_t    csep_ { '#' };
    std::atomic_char32_t    lsep_ { path_sep() };
    Key_section             root_;
    Sections                sections_;
    ustring                 path_;
    std::atomic_bool        locked_ { false };
    std::atomic_bool        changed_ { false };
    signal<void()> *        signal_changed_ = nullptr;
    signal<void(std::string_view, std::string_view, const ustring &)> * signal_modified_ = nullptr;

   ~Key_file_impl() {
        if (signal_changed_) { delete signal_changed_; }
        if (signal_modified_) { delete signal_modified_; }
    }

    Key_file_impl():
        root_(this, "root")
    {
    }

    Key_file_impl(const Key_file_impl & other) {
        {   std::unique_lock lock(other.mx_);
            csep_ = other.csep_.load();
            lsep_ = other.lsep_.load();
            root_ = other.root_;
            sections_ = other.sections_;
            path_ = other.path_;
            locked_= other.locked_.load();
            changed_ = other.changed_.load();
        }

        own();
    }

    Key_file_impl(Key_file_impl && other) {
        {   std::unique_lock lock(other.mx_);
            csep_ = other.csep_.load();
            lsep_ = other.lsep_.load();
            root_ = std::move(other.root_);
            sections_ = std::move(other.sections_);
            path_ = std::move(other.path_);
            locked_= other.locked_.load();
            changed_ = other.changed_.load();
        }

        own();
    }

    Key_file_impl & operator=(const Key_file_impl & other) {
        if (this != &other) {
            {   std::unique_lock lock(mx_), lock2(other.mx_);
                csep_ = other.csep_.load();
                lsep_ = other.lsep_.load();
                root_ = other.root_;
                sections_ = other.sections_;
                path_ = other.path_;
                locked_= other.locked_.load();
                changed_ = other.changed_.load();
                own();
            }
        }

        return *this;
    }

    Key_file_impl & operator=(Key_file_impl && other) {
        {   std::unique_lock lock(mx_), lock2(other.mx_);
            csep_ = other.csep_.load();
            lsep_ = other.lsep_.load();
            root_ = std::move(other.root_);
            sections_ = std::move(other.sections_);
            path_ = std::move(other.path_);
            locked_= other.locked_.load();
            changed_ = other.changed_.load();
            own();
        }

        return *this;
    }

    signal<void()> & signal_changed() {
        std::unique_lock lock(mx_);
        if (!signal_changed_) { signal_changed_ = new signal<void()>; }
        return *signal_changed_;
    }

    signal<void(std::string_view, std::string_view, const ustring &)> & signal_modified() {
        std::unique_lock lock(mx_);
        if (!signal_modified_) { signal_modified_ = new signal<void(std::string_view, std::string_view, const ustring &)>; }
        return *signal_modified_;
    }

    void set_changed_nolock(std::string_view sect_name, std::string_view key, const ustring & value) {
        if (!changed_) {
            changed_ = true;
            if (signal_changed_) { (*signal_changed_)(); }
        }

        if (signal_modified_) {
            signal_modified_->operator()(sect_name, key, value);
        }
    }

    void set_changed(std::string_view sect_name, std::string_view key, const ustring & value) {
        std::unique_lock lock(mx_);
        set_changed_nolock(sect_name, key, value);
    }

    void own() {
        root_.kf_ = this;
        for (auto & p: sections_) { p.second.kf_ = this; }
    }

    ustring str() const {
        ustring os;
        std::unique_lock lock(mx_);
        os += root_.str(csep_);

        for (auto & p: sections_) {
            if (!p.second.empty()) {
                os += str_format('[', p.first, "]\n");
                os += p.second.str(csep_);
            }
        }

        return os;
    }

    std::vector<std::string> list_sections() const {
        std::unique_lock lock(mx_);
        std::vector<std::string> v(sections_.size());
        auto i = v.begin();
        for (auto & p: sections_) { *i++ = p.first; }
        return v;
    }

    Sections::iterator find(std::string_view sect_name, bool similar=false) {
        if (similar) {
            return std::find_if(sections_.begin(), sections_.end(), [sect_name](auto & p) { return str_similar(p.first, sect_name); } );
        }

        else {
            return std::find_if(sections_.begin(), sections_.end(), [sect_name](auto & p) { return p.first == sect_name; } );
        }
    }

    Sections::const_iterator find(std::string_view sect_name, bool similar=false) const {
        if (similar) {
            return std::find_if(sections_.begin(), sections_.end(), [sect_name](auto & p) { return str_similar(p.first, sect_name); } );
        }

        else {
            return std::find_if(sections_.begin(), sections_.end(), [sect_name](auto & p) { return p.first == sect_name; } );
        }
    }

    bool has_section(std::string_view sect_name, bool similar) const {
        std::unique_lock lock(mx_);
        return find(sect_name, similar) != sections_.end();
    }

    bool has_key(const Key_section & sect, std::string_view key_name, bool similar) const {
        if (this != sect.kf_) { throw user_error(str_format(this, " Key_file: foreign section '", sect.name_, '\'')); }
        std::unique_lock lock(mx_);
        return sect.has_key(key_name, similar);
    }
    
    bool has_key(std::string_view key_name, bool similar) const {
        return has_key(root_, key_name, similar);
    }
    
    Key_section & section(std::string_view sect_name, bool similar) {
        std::unique_lock lock(mx_);
        auto i = find(sect_name, similar);
        if (i != sections_.end()) { return i->second; }
        set_changed_nolock(sect_name, "", "");
        return sections_.emplace_back(sect_name, Key_section(this, sect_name)).second;
    }

    Key_section & section_nochange(std::string_view sect_name, bool similar) {
        std::unique_lock lock(mx_);
        auto i = find(sect_name, similar);
        if (i != sections_.end()) { return i->second; }
        return sections_.emplace_back(sect_name, Key_section(this, sect_name)).second;
    }

    const Key_section & section(std::string_view sect_name, bool similar) const {
        std::unique_lock lock(mx_);
        auto i = find(sect_name, similar);
        if (i != sections_.end()) { return i->second; }
        throw user_error(str_format("Key_file: section ", sect_name, " does not exist"));
    }

    bool empty() const {
        std::unique_lock lock(mx_);
        return root_.empty() && sections_.empty();
    }

    bool clear() {
        bool changed = false;

        if (!locked_) {
            std::unique_lock lock(mx_);
            if (!root_.empty()) { root_.clear(); changed = true; }
            if (!sections_.empty()) { sections_.clear(); changed = true; }
            changed_ = false;
        }

        return changed;
    }

    void rename_section(const Key_section & sect, std::string_view new_name) {
        chk_sect(sect);
        std::unique_lock lock(mx_);

        if (sections_.end() != find(new_name, false)) {
            throw user_error(str_format("Key_file: section ", new_name, " already exists"));
        }

        auto i = std::find_if(sections_.begin(), sections_.end(), [sect](auto& p) { return &p.second == &sect; });

        if (i == sections_.end()) {
            throw internal_error("Key_file: failed to find section iterator");
        }

        i->first = new_name;
        set_changed_nolock(new_name, "", "");
    }

    void swap_sections(Key_section & sect1, Key_section & sect2) {
        chk_sect(sect1);
        chk_sect(sect2);
        std::unique_lock lock(mx_);
        std::swap(sect1.comment_, sect2.comment_);
        sect1.elems_.swap(sect2.elems_);

        if (sect1.comment_ != sect2.comment_ || !(sect1.elems_.empty() && sect2.elems_.empty())) {
            set_changed_nolock(sect1.name_, "", "");
            set_changed_nolock(sect2.name_, "", "");
        }
    }

    void remove_section(std::string_view sect_name, bool similar) {
        if (!locked_) {
            std::unique_lock lock(mx_);
            auto i = find(sect_name, similar);
            if (i != sections_.end()) { sections_.erase(i); set_changed_nolock(sect_name, "", ""); }
        }
    }

    void chk_sect(const Key_section & sect) const {
        if (this != sect.kf_) {
            throw user_error(str_format(this, " Key_file: foreign section '", sect.name_, '\''));
        }
    }

    void remove_key(Key_section & sect, std::string_view key, bool similar) {
        chk_sect(sect);

        if (!locked_) {
            std::unique_lock lock(mx_);
            auto iter = sect.find(key, similar);
            if (iter != sect.elems_.end()) { sect.elems_.erase(iter); set_changed_nolock(sect.name_, key, ""); }
        }
    }

    std::vector<double> get_doubles(const Key_section & sect, std::string_view key, std::size_t min_size, double fallback) const {
        chk_sect(sect);
        std::vector<double> v;

        {
            std::unique_lock lock(mx_);
            auto iter = sect.find(key);

            if (iter != sect.elems_.end()) {
                for (auto & s: str_explode(iter->second, lsep_)) {
                    double d; std::istringstream is(str_replace(s, ',', '.')); is.imbue(std::locale::classic()); is >> d;
                    v.push_back(d);
                }
            }
        }

        while (v.size() < min_size) { v.push_back(fallback); }
        return v;
    }

    double get_double(const Key_section & sect, std::string_view key, double fallback) const {
        chk_sect(sect);
        std::unique_lock lock(mx_);
        auto iter = sect.find(key);
        if (iter == sect.elems_.end()) { return fallback; }
        double d; std::istringstream is(str_replace(iter->second, ',', '.')); is.imbue(std::locale::classic()); is >> d;
        return d;
    }

    std::vector<intmax_t> get_integers(const Key_section & sect, std::string_view key, std::size_t min_size, intmax_t fallback) const {
        chk_sect(sect);
        std::vector<intmax_t> v;

        {
            std::unique_lock lock(mx_);
            auto iter = sect.find(key);

            if (iter != sect.elems_.end()) {
                for (std::string s: str_explode(iter->second, lsep_)) {
                    try { v.push_back(std::stoll(s, nullptr, 0)); }
                    catch (std::exception & x) { std::cerr << "** " << __func__ << ": " << x.what() << std::endl; }
                }
            }
        }

        while (v.size() < min_size) { v.emplace_back(fallback); }
        return v;
    }

    intmax_t get_integer(const Key_section & sect, std::string_view key, intmax_t fallback) const {
        chk_sect(sect);
        std::unique_lock lock(mx_);
        auto i = sect.find(key);

        if (i != sect.elems_.end()) {
            try { return std::stoll(i->second.raw(), nullptr, 0); }
            catch (std::exception & x) { std::cerr << "** " << __func__ << ": " << x.what() << std::endl; }
        }

        return fallback;
    }

    std::vector<bool> get_booleans(const Key_section & sect, std::string_view key, std::size_t min_size, bool fallback) const {
        chk_sect(sect);
        std::vector<bool> v;

        {
            std::unique_lock lock(mx_);
            auto iter = sect.find(key);

            if (iter != sect.elems_.end()) {
                for (auto & s: str_explode(iter->second, lsep_)) {
                    v.emplace_back(str_boolean(s));
                }
            }
        }

        while (v.size() < min_size) { v.emplace_back(fallback); }
        return v;
    }

    bool get_boolean(const Key_section & sect, std::string_view key, bool fallback) const {
        chk_sect(sect);
        std::unique_lock lock(mx_);
        auto iter = sect.find(key);
        return iter != sect.elems_.end() ? str_boolean(iter->second) : fallback;
    }

    std::vector<ustring> get_strings(const Key_section & sect, std::string_view key, std::size_t min_size, const ustring & fallback) const {
        chk_sect(sect);
        std::vector<ustring> v;

        {
            std::unique_lock lock(mx_);
            auto iter = sect.find(key);
            if (iter != sect.elems_.end()) { v = str_explode(iter->second, lsep_); }
        }

        while (v.size() < min_size) { v.emplace_back(fallback); }
        return v;
    }

    ustring get_string(const Key_section & sect, std::string_view key, const ustring & fallback) const {
        chk_sect(sect);
        std::unique_lock lock(mx_);
        auto iter = sect.find(key);
        return iter == sect.elems_.end() ? fallback : iter->second;
    }

    ustring comment(const Key_section & sect) const {
        std::unique_lock lock(mx_);
        return sect.comment_;
    }

    void set_comment(Key_section & sect, const ustring & comment) {
        chk_sect(sect);

        if (!locked_) {
            std::unique_lock lock(mx_);

            if (comment != sect.comment_) {
                sect.comment_ = comment;
                set_changed_nolock(sect.name_, "", "");
            }
        }
    }

    void set_string(Key_section & sect, std::string_view key, const ustring & value) {
        chk_sect(sect);

        if (!locked_) {
            std::unique_lock lock(mx_);
            if (sect.set_value(key, value)) { set_changed_nolock(sect.name_, key, value); }
        }
    }

    void set_strings(Key_section & sect, std::string_view key, const std::vector<ustring> & vec) {
        chk_sect(sect);

        if (!locked_) {
            auto s = str_implode(vec, lsep_);
            std::unique_lock lock(mx_);
            if (sect.set_value(key, s)) { set_changed_nolock(sect.name_, key, s); }
        }
    }

    void set_boolean(Key_section & sect, std::string_view key, bool value) {
        chk_sect(sect);

        if (!locked_) {
            ustring s = value ? "true" : "false";
            std::unique_lock lock(mx_);
            if (sect.set_value(key, s)) { set_changed_nolock(sect.name_, key, s); }
        }
    }

    void set_booleans(Key_section & sect, std::string_view key, const std::vector<bool> & vec) {
        chk_sect(sect);

        if (!locked_) {
            std::vector<ustring> sv;
            for (bool b: vec) { sv.push_back(b ? "true" : "false"); }
            auto s = str_implode(sv, lsep_);
            std::unique_lock lock(mx_);
            if (sect.set_value(key, s)) { set_changed_nolock(sect.name_, key, s); }
        }
    }

    void set_integer(Key_section & sect, std::string_view key, intmax_t value) {
        chk_sect(sect);

        if (!locked_) {
            auto s = str_format(value);
            std::unique_lock lock(mx_);
            if (sect.set_value(key, s)) { set_changed_nolock(sect.name_, key, s); }
        }
    }

    void set_integers(Key_section & sect, std::string_view key, const std::vector<intmax_t> & vec) {
        chk_sect(sect);

        if (!locked_) {
            std::vector<ustring> sv;
            for (intmax_t ll: vec) { sv.push_back(str_format(ll)); }
            auto s = str_implode(sv, lsep_);
            std::unique_lock lock(mx_);
            if (sect.set_value(key, s)) { set_changed_nolock(sect.name_, key, s); }
        }
    }

    void set_double(Key_section & sect, std::string_view key, double value) {
        chk_sect(sect);

        if (!locked_) {
            auto s = str_format(value);
            std::unique_lock lock(mx_);
            if (sect.set_value(key, s)) { set_changed_nolock(sect.name_, key, s); }
        }
    }

    void set_doubles(Key_section & sect, std::string_view key, const std::vector<double> & vec) {
        chk_sect(sect);

        if (!locked_) {
            std::vector<ustring> sv;
            for (double d: vec) { sv.push_back(str_format(d)); }
            auto s = str_implode(sv, lsep_);
            std::unique_lock lock(mx_);
            if (sect.set_value(key, s)) { set_changed_nolock(sect.name_, key, s); }
        }
    }

    std::vector<std::string> list_keys(const Key_section & sect) const {
        chk_sect(sect);
        std::unique_lock lock(mx_);
        return sect.list_keys();
    }

    ustring key_name(const Key_section & sect, std::string_view similar_name) const {
        chk_sect(sect);
        std::unique_lock lock(mx_);
        return sect.key_name(similar_name);
    }

    bool load(Buffer buf) {
        bool changed = false;

        if (!locked_) {
            std::string sect_name;

            for (auto i = buf.cbegin(); !i.eof(); i.forward_line()) {
                i.skip_blanks();
                char32_t wc = *i;

                if (U'#' != wc && U';' != wc) {
                    auto ii(i), j(i); ii.move_to_eol();

                    if (U'[' == wc) {
                        if ((++i).find(U']', ii)) {
                            sect_name = str_trimboth((++j).str(i));
                        }
                    }

                    else {
                        if (i.find(U'=', ii)) {
                            std::string key = str_trimboth(j.str(i));
                            auto val = str_trimboth(str_trimboth((++i).str(ii)), "\"");

                            if (!key.empty() && !val.empty()) {
                                bool exists = has_section(sect_name, false);
                                if (!exists) { changed = true; }
                                Key_section & sect = sect_name.empty() ? root_ : section_nochange(sect_name, false);
                                if (!exists && signal_modified_) { (*signal_modified_)(sect_name, "", ""); }

                                if (sect.set_value(key, val)) {
                                    changed = true;
                                    if (signal_modified_) { (*signal_modified_)(sect_name, key, val); }
                                }
                            }
                        }
                    }
                }
            }
        }

        return changed;
    }

    bool load(std::istream & is) {
        if (is.good()) {
            Buffer buf;
            buf.insert(buf.cend(), is);
            return load(buf);
        }

        return false;
    }
};

// ----------------------------------------------------------------------------
// ----------------------------------------------------------------------------

Key_file::Key_file(char32_t lsep, char32_t csep) noexcept:
    impl(new Key_file_impl)
{
    set_list_separator(lsep);
    set_comment_separator(csep);
}

Key_file::Key_file(std::istream & is, char32_t lsep, char32_t csep) noexcept:
    impl(new Key_file_impl)
{
    set_list_separator(lsep);
    set_comment_separator(csep);
    try { impl->load(is); } catch (...) {}
}

Key_file::Key_file(Buffer buf, char32_t lsep, char32_t csep) noexcept:
    impl(new Key_file_impl)
{
    set_list_separator(lsep);
    set_comment_separator(csep);
    try { impl->load(buf); } catch (...) {}
}

Key_file::Key_file(const ustring & path, char32_t lsep, char32_t csep) noexcept:
    impl(new Key_file_impl)
{
    set_list_separator(lsep);
    set_comment_separator(csep);

    if (file_exists(path)) {
        impl->path_ = path;

        try {
            Buffer buf = Buffer::load_from_file(path);
            impl->load(buf);
        }

        catch (...) {}
    }
}

Key_file::Key_file(const Key_file & other):
    impl(new Key_file_impl(*other.impl))
{
}

Key_file & Key_file::operator=(const Key_file & other) {
    if (this != &other) { impl = new Key_file_impl(*other.impl); }
    return *this;
}

Key_file::Key_file(Key_file && other):
    impl(new Key_file_impl(std::move(*other.impl)))
{
}

Key_file & Key_file::operator=(Key_file && other) {
    *impl = std::move(*other.impl);
    return *this;
}

Key_file::~Key_file() {
    flush();
    delete impl;
}

void Key_file::load(std::istream & is) {
    bool changed = impl->load(is);

    {   std::unique_lock lock(impl->mx_);
        if (changed) { impl->set_changed_nolock("", "", ""); }
    }
}

void Key_file::load_from_buffer(Buffer buf) {
    bool changed = impl->load(buf);

    {   std::unique_lock lock(impl->mx_);
        if (changed) { impl->set_changed_nolock("", "", ""); }
    }
}

void Key_file::load_from_file(const ustring & path) {
    Buffer buf = Buffer::load_from_file(path);

    {   std::unique_lock lock(impl->mx_);
        impl->path_ = path;
        impl->changed_ = false;
    }

    impl->load(buf);
}

void Key_file::reload() noexcept {
    ustring path;
    { std::unique_lock lock(impl->mx_); path = impl->path_; }

    if (!path.empty()) {
        if (File(path).is_regular()) {
            try {
                Buffer buf = Buffer::load_from_file(path);
                impl->changed_ = false;
                impl->clear();
                if (impl->load(buf)) { impl->set_changed_nolock("", "", ""); }
            }

            catch (exception & x) {
                std::cerr << "** Key_file::reload(): " << path << ": " << x.what() << std::endl;
            }

            catch (std::exception & x) {
                std::cerr << "** Key_file::reload(): " << path << ": " << x.what() << std::endl;
            }

            catch (...) {
                std::cerr << "** Key_file::reload(): " << path << std::endl;
            }
        }
    }
}

void Key_file::save(std::ostream & os) const {
    if (os.good()) {
        Buffer buf;

        {
            std::unique_lock lock(impl->mx_);
            buf.insert(buf.cend(), impl->str());
            impl->changed_ = false;
        }

        os << buf;
    }
}

void Key_file::save_to_file(const ustring & path) {
    file_mkdir(path_dirname(path));
    Buffer buf;

    {
        std::unique_lock lock(impl->mx_);
        buf.insert(buf.cend(), impl->str());
        impl->changed_ = false;
    }

    buf.save_to_file(path);
    { std::unique_lock lock(impl->mx_); impl->path_ = path; }
}

void Key_file::flush() noexcept {
    if (impl->changed_) {
        ustring path;

        {   std::unique_lock lock(impl->mx_);
            if (!impl->locked_) { path = impl->path_; }
        }

        if (!path.empty()) {
            try {
                save_to_file(path);
            }

            catch (exception & x) {
                std::cerr << "** Key_file::flush(): " << path << ": " << x.what() << std::endl;
            }

            catch (std::exception & x) {
                std::cerr << "** Key_file::flush(): " << path << ": " << x.what() << std::endl;
            }

            catch (...) {
                std::cerr << "** Key_file::flush(): " << path << std::endl;
            }
        }
    }
}

void Key_file::create_from_file(const ustring & path) {
    if (file_exists(path)) { load_from_file(path); }
    else { file_mkdir(path_dirname(path)); save_to_file(path); }
}

void Key_file::set_comment_separator(char32_t comment_sep) noexcept {
    impl->csep_ = U'\0' != comment_sep ? comment_sep : keyfile_comment_separator_;
}

void Key_file::set_list_separator(char32_t list_sep) noexcept {
    impl->lsep_ = U'\0' != list_sep ? list_sep : path_sep();
}

char32_t Key_file::list_separator() const noexcept {
    return impl->lsep_;
}

char32_t Key_file::comment_separator() const noexcept {
    return impl->csep_;
}

std::vector<std::string> Key_file::list_sections() const {
    return impl->list_sections();
}

std::vector<std::string> Key_file::list_keys(const Key_section & sect) const {
    return impl->list_keys(sect);
}

bool Key_file::has_section(std::string_view sect_name, bool similar) const noexcept {
    return impl->has_section(sect_name, similar);
}

bool Key_file::has_key(const Key_section & sect, std::string_view key_name, bool similar) const {
    return impl->has_key(sect, key_name, similar);
}

bool Key_file::has_key(std::string_view key_name, bool similar) const noexcept {
    return impl->has_key(key_name, similar);
}

std::string Key_file::key_name(const Key_section & sect, std::string_view similar_name) const {
    chk_name(this, similar_name);
    return impl->key_name(sect, similar_name);
}

std::string Key_file::key_name(std::string_view similar_name) const {
    chk_name(this, similar_name);
    return impl->key_name(impl->root_, similar_name);
}

Key_section & Key_file::root() noexcept {
    return impl->root_;
}

const Key_section & Key_file::root() const noexcept {
    return impl->root_;
}

Key_section & Key_file::section(std::string_view sect_name, bool similar) {
    return impl->section(sect_name, similar);
}

const Key_section & Key_file::section(std::string_view sect_name, bool similar) const {
    return impl->section(sect_name, similar);
}

void Key_file::set_comment(Key_section & sect, const ustring & comment) {
    impl->set_comment(sect, comment);
}

void Key_file::set_comment(const ustring & comment) {
    impl->set_comment(root(), comment);
}

void Key_file::set_string(Key_section & sect, std::string_view key_name, const ustring & value) {
    chk_name(this, key_name);
    impl->set_string(sect, key_name, value);
}

void Key_file::set_string(std::string_view key_name, const ustring & value) {
    chk_name(this, key_name);
    impl->set_string(root(), key_name, value);
}

void Key_file::set_strings(Key_section & sect, std::string_view key_name, const std::vector<ustring> & vec) {
    chk_name(this, key_name);
    impl->set_strings(sect, key_name, vec);
}

void Key_file::set_strings(std::string_view key_name, const std::vector<ustring> & vec) {
    chk_name(this, key_name);
    impl->set_strings(root(), key_name, vec);
}

void Key_file::set_boolean(Key_section & sect, std::string_view key_name, bool value) {
    chk_name(this, key_name);
    impl->set_boolean(sect, key_name, value);
}

void Key_file::set_boolean(std::string_view key_name, bool value) {
    chk_name(this, key_name);
    impl->set_boolean(root(), key_name, value);
}

void Key_file::set_booleans(Key_section & sect, std::string_view key_name, const std::vector<bool> & vec) {
    chk_name(this, key_name);
    impl->set_booleans(sect, key_name, vec);
}

void Key_file::set_booleans(std::string_view key_name, const std::vector<bool> & vec) {
    chk_name(this, key_name);
    impl->set_booleans(root(), key_name, vec);
}

void Key_file::set_integer(Key_section & sect, std::string_view key_name, intmax_t value) {
    chk_name(this, key_name);
    impl->set_integer(sect, key_name, value);
}

void Key_file::set_integer(std::string_view key_name, intmax_t value) {
    chk_name(this, key_name);
    impl->set_integer(root(), key_name, value);
}

void Key_file::set_integers(Key_section & sect, std::string_view key_name, const std::vector<intmax_t> & vec) {
    chk_name(this, key_name);
    impl->set_integers(sect, key_name, vec);
}

void Key_file::set_integers(std::string_view key_name, const std::vector<intmax_t> & vec) {
    chk_name(this, key_name);
    impl->set_integers(root(), key_name, vec);
}

void Key_file::set_double(Key_section & sect, std::string_view key_name, double value) {
    impl->set_double(sect, key_name, value);
}

void Key_file::set_double(std::string_view key_name, double value) {
    chk_name(this, key_name);
    impl->set_double(root(), key_name, value);
}

void Key_file::set_doubles(Key_section & sect, std::string_view key_name, const std::vector<double> & vec) {
    chk_name(this, key_name);
    impl->set_doubles(sect, key_name, vec);
}

void Key_file::set_doubles(std::string_view key_name, const std::vector<double> & vec) {
    chk_name(this, key_name);
    impl->set_doubles(root(), key_name, vec);
}

ustring Key_file::comment(const Key_section & sect) const {
    return impl->comment(sect);
}

ustring Key_file::comment() const {
    return impl->comment(root());
}

ustring Key_file::get_string(const Key_section & sect, std::string_view key_name, const ustring & fallback) const {
    return impl->get_string(sect, key_name, fallback);
}

ustring Key_file::get_string(std::string_view key_name, const ustring & fallback) const {
    chk_name(this, key_name);
    return impl->get_string(root(), key_name, fallback);
}

std::vector<ustring> Key_file::get_strings(const Key_section & sect, std::string_view key_name, std::size_t min_size, const ustring & fallback) const {
    chk_name(this, key_name);
    return impl->get_strings(sect, key_name, min_size, fallback);
}

std::vector<ustring> Key_file::get_strings(std::string_view key_name, std::size_t min_size, const ustring & fallback) const {
    chk_name(this, key_name);
    return impl->get_strings(root(), key_name, min_size, fallback);
}

bool Key_file::get_boolean(const Key_section & sect, std::string_view key_name, bool fallback) const {
    chk_name(this, key_name);
    return impl->get_boolean(sect, key_name, fallback);
}

bool Key_file::get_boolean(std::string_view key_name, bool fallback) const {
    chk_name(this, key_name);
    return impl->get_boolean(root(), key_name, fallback);
}

std::vector<bool> Key_file::get_booleans(const Key_section & sect, std::string_view key_name, std::size_t min_size, bool fallback) const {
    chk_name(this, key_name);
    return impl->get_booleans(sect, key_name, min_size, fallback);
}

std::vector<bool> Key_file::get_booleans(std::string_view key_name, std::size_t min_size, bool fallback) const {
    chk_name(this, key_name);
    return impl->get_booleans(root(), key_name, min_size, fallback);
}

intmax_t Key_file::get_integer(const Key_section & sect, std::string_view key_name, intmax_t fallback) const {
    chk_name(this, key_name);
    return impl->get_integer(sect, key_name, fallback);
}

intmax_t Key_file::get_integer(std::string_view key_name, intmax_t fallback) const {
    chk_name(this, key_name);
    return impl->get_integer(root(), key_name, fallback);
}

std::vector<intmax_t> Key_file::get_integers(const Key_section & sect, std::string_view key_name, std::size_t min_size, intmax_t fallback) const {
    chk_name(this, key_name);
    return impl->get_integers(sect, key_name, min_size, fallback);
}

std::vector<intmax_t> Key_file::get_integers(std::string_view key_name, std::size_t min_size, intmax_t fallback) const {
    chk_name(this, key_name);
    return impl->get_integers(root(), key_name, min_size, fallback);
}

double Key_file::get_double(const Key_section & sect, std::string_view key_name, double fallback) const {
    chk_name(this, key_name);
    return impl->get_double(sect, key_name, fallback);
}

double Key_file::get_double(std::string_view key_name, double fallback) const {
    chk_name(this, key_name);
    return impl->get_double(root(), key_name, fallback);
}

std::vector<double> Key_file::get_doubles(const Key_section & sect, std::string_view key_name, std::size_t min_size, double fallback) const {
    chk_name(this, key_name);
    return impl->get_doubles(sect, key_name, min_size, fallback);
}

std::vector<double> Key_file::get_doubles(std::string_view key_name, std::size_t min_size, double fallback) const {
    chk_name(this, key_name);
    return impl->get_doubles(root(), key_name, min_size, fallback);
}

void Key_file::remove_key(Key_section & sect, std::string_view key_name, bool similar) {
    chk_name(this, key_name);
    impl->remove_key(sect, key_name, similar);
}

void Key_file::remove_key(std::string_view key_name, bool similar) {
    chk_name(this, key_name);
    impl->remove_key(root(), key_name, similar);
}

void Key_file::rename_section(const Key_section & sect, std::string_view new_name) {
    chk_name(this, new_name);
    impl->rename_section(sect, new_name);
}

void Key_file::swap_sections(Key_section & sect1, Key_section & sect2) {
    impl->swap_sections(sect1, sect2);
}

void Key_file::remove_section(std::string_view sect_name, bool similar) {
    chk_name(this, sect_name);
    impl->remove_section(sect_name, similar);
}

void Key_file::clear() {
    if (impl->clear()) { impl->set_changed_nolock("", "", ""); }
}

bool Key_file::empty() const noexcept {
    return impl->empty();
}

void Key_file::lock() noexcept {
    impl->locked_ = true;
}

void Key_file::unlock() noexcept {
    impl->locked_ = false;
}

bool Key_file::locked() const noexcept {
    return impl->locked_;
}

bool Key_file::changed() const noexcept {
    return impl->changed_;
}

signal<void()> & Key_file::signal_changed() {
    return impl->signal_changed();
}

signal<void(std::string_view, std::string_view, const ustring &)> & Key_file::signal_modified() {
    return impl->signal_modified();
}

std::istream & operator>>(std::istream & is, Key_file & kf) {
    kf.load(is); return is;
}

std::ostream & operator<<(std::ostream & os, const Key_file & kf) {
    kf.save(os); return os;
}

} // namespace tau

//END

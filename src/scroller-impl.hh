// ----------------------------------------------------------------------------
// SPDX-License-Identifier: BSD-2-Clause
// Copyright © 2014-2024 Konstantin Shmelkov <mrcashe@gmail.com>.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
// THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
// PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
// CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
// EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
// PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
// OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
// WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
// OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
// EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// ----------------------------------------------------------------------------

#ifndef __TAU_SCROLLER_IMPL_HH__
#define __TAU_SCROLLER_IMPL_HH__

#include <tau/action.hh>
#include <tau/timer.hh>
#include <flat-container.hh>
#include <forward_list>

namespace tau {

class Scroller_impl: public Flat_container {
public:

    Scroller_impl();
   ~Scroller_impl() { destroy(); }

    void insert(Widget_ptr wp);
    void clear();
    Widget_impl * widget() { return cp_; }
    const Widget_impl * widget() const { return cp_; }
    bool empty() const noexcept { return nullptr != cp_; }

    Size pan_size() const noexcept;
    Point pan() const noexcept;

    void gravity(Gravity g);
    Gravity gravity() const noexcept { return gravity_; }

    void pan_x(int x);
    void pan_y(int y);
    void pan(const Point & pos);
    void pan(int x, int y);
    void pan(Widget_impl * wp);

    Action & action_pan_left() { return action_pan_left_; }
    Action & action_pan_right() { return action_pan_right_; }
    Action & action_pan_up() { return action_pan_up_; }
    Action & action_pan_down() { return action_pan_down_; }
    Action & action_previous_page() { return action_previous_page_; }
    Action & action_next_page() { return action_next_page_; }
    Action & action_home() { return action_home_; }
    Action & action_end() { return action_end_; }

    void set_step(const Point & step) { step_ = step; }
    void set_step(int xstep, int ystep) { step_.set(xstep, ystep); }
    Point step() const { return step_; }

    signal<void()> & signal_pan_changed() { return signal_pan_changed_; }
    signal<void()> & signal_pan_size_changed() { return signal_pan_size_changed_; }

    Container_impl * compound() noexcept override { return this; };
    const Container_impl * compound() const noexcept override { return this; };

    // Overrides Widget_impl.
    void track_pan(Widget_impl * wp) override;

private:

    Widget_impl *   cp_                     { nullptr };
    connection      cx_                     { true };
    Widget_impl *   tracked_                { nullptr };
    Point           pan_;
    Point           pending_;               // Pending pan.
    Point           deferred_               { -1, -1 };             // Deferred pan.
    Point           step_;
    Gravity         gravity_                { Gravity::NONE };
    Timer           tmr_                    { fun(this, &Scroller_impl::on_timer) };

    // -- Actions & Signals.
    Action          action_pan_left_        { "<Ctrl><Alt>Left",    fun(this, &Scroller_impl::pan_left)  };
    Action          action_pan_right_       { "<Ctrl><Alt>Right",   fun(this, &Scroller_impl::pan_right) };
    Action          action_pan_up_          { "<Ctrl>Up",           fun(this, &Scroller_impl::pan_up)    };
    Action          action_pan_down_        { "<Ctrl>Down",         fun(this, &Scroller_impl::pan_down)  };
    Action          action_previous_page_   { "PageUp",             fun(this, &Scroller_impl::page_up)   };
    Action          action_next_page_       { "PageDown",           fun(this, &Scroller_impl::page_down) };
    Action          action_home_            { "Home",               fun(this, &Scroller_impl::home)      };
    Action          action_end_             { "End",                fun(this, &Scroller_impl::end)       };

    connection      gravity_cx_;                        // Gravity slot blocker.
    connection      tracked_cx_             { true };   // Deferred track connection.
    signal<void()>  signal_pan_changed_;
    signal<void()>  signal_pan_size_changed_;

private:

    void arrange();
    void check_pan();
    void check_gravity();
    void update_pan(int x, int y);
    void update_requisition();
    void pan_to(Widget_impl * wp);

    void pan_left();
    void pan_right();
    void pan_down();
    void pan_up();
    void page_down();
    void page_up();
    void home();
    void end();

    void on_hints(Hints op);
    bool on_mouse_down(int mbt, int mm, const Point & pt);
    bool on_mouse_wheel(int delta, int mm, const Point & pt);
    bool on_take_focus();
    void on_timer();
    void on_tracked_unparent(Object * o);
    void on_size_changed();
    void on_pan_size_changed();
    void on_display_in();
};

} // namespace tau

#endif // __TAU_SCROLLER_IMPL_HH__

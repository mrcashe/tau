// ----------------------------------------------------------------------------
// SPDX-License-Identifier: BSD-2-Clause
// Copyright © 2014-2024 Konstantin Shmelkov <mrcashe@gmail.com>.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
// THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
// PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
// CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
// EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
// PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
// OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
// WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
// OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
// EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// ----------------------------------------------------------------------------

#include <bin-impl.hh>
#include <iostream>

namespace tau {

Bin_impl::Bin_impl() {
    signal_arrange_.connect(fun(this, &Bin_impl::arrange), true);
    signal_size_changed_.connect(fun(this, &Bin_impl::arrange));
    signal_display_in_.connect(fun(this, &Bin_impl::update_requisition));
    signal_take_focus_.connect(fun(this, &Bin_impl::on_take_focus));
}

void Bin_impl::insert(Widget_ptr wp) {
    bool has_focus = focused();
    chk_parent(wp);

    if (wp.get() != cp_) {
        clear();
        cp_ = wp.get();
        cx_ = wp->signal_hints_changed().connect(fun(this, &Bin_impl::on_hints));
        update_child_bounds(cp_, INT_MIN, INT_MIN);
        make_child(wp);
        update_requisition();
        arrange();
    }

    if (has_focus) { cp_->take_focus(); }
}

void Bin_impl::clear() {
    if (auto wp = cp_) {
        cx_.drop();
        cp_->invalidate();
        cp_ = nullptr;
        unparent( { wp } );
        update_requisition();
    }
}

void Bin_impl::update_requisition() {
    Size rs;

    if (cp_ && !cp_->hidden()) {
        Margin m = cp_->margin_hint();
        rs.increase(m.left+m.right, m.top+m.bottom);
        Size rq = cp_->required_size();
        if (rq) { rs += rq; }
    }

    require_size(rs);
}

void Bin_impl::arrange() {
    if (cp_ && !cp_->hidden()) {
        Margin m = cp_->margin_hint(); Size z { size() };
        z.decrease(m.left+m.right, m.top+m.bottom);
        if (update_child_bounds(cp_, m.left, m.top, z)) { invalidate(); }
    }
}

void Bin_impl::on_hints(Hints op) {
    if (!in_shutdown()) {
        if (Hints::SHOW == op) {
            update_requisition();
            arrange();
            if (focused() && cp_) { cp_->take_focus(); }
        }

        else if (Hints::HIDE == op) {
            update_child_bounds(cp_, INT_MIN, INT_MIN);
            update_requisition();
            invalidate();
        }

        else {
            update_requisition();
        }
    }
}

bool Bin_impl::on_take_focus() {
    return cp_ && cp_->take_focus();
}

} // namespace tau

//END

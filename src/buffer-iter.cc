// ----------------------------------------------------------------------------
// SPDX-License-Identifier: BSD-2-Clause
// Copyright © 2014-2024 Konstantin Shmelkov <mrcashe@gmail.com>.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
// THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
// PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
// CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
// EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
// PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
// OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
// WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
// OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
// EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// ----------------------------------------------------------------------------

#include <tau/string.hh>
#include <buffer-impl.hh>
#include <array>

namespace tau {

Buffer_citer_impl * Buffer_citer_impl::create() {
    static std::array<Buffer_citer_impl, 1024> v;

    for (auto & i: v) {
        if (!i.busy_) {
            i.busy_ = true;
            i.row_ = 0;
            i.col_ = 0;
            return &i;
        }
    }

    Buffer_citer_impl * tp = new Buffer_citer_impl;
    tp->heap_ = true;
    tp->busy_ = true;
    return tp;
}

Buffer_citer_impl * Buffer_citer_impl::create(Buffer_ptr buf, std::size_t row, std::size_t col) {
    Buffer_citer_impl * tp = create();
    tp->buf_ = buf;
    tp->row_ = row;
    tp->col_ = col;
    return tp;
}

// ----------------------------------------------------------------------------
// ----------------------------------------------------------------------------

Buffer_citer::Buffer_citer():
    impl(Buffer_citer_impl::create())
{
}

Buffer_citer::Buffer_citer(const Buffer_citer & other):
    impl(Buffer_citer_impl::create())
{
    impl->buf_ = other.impl->buf_;
    impl->row_ = other.impl->row_;
    impl->col_ = other.impl->col_;
}

Buffer_citer::Buffer_citer(const Buffer_citer & other, std::size_t row, std::size_t col):
    impl(Buffer_citer_impl::create())
{
    impl->buf_ = other.impl->buf_;
    impl->row_ = row;
    impl->col_ = col;
}

Buffer_citer & Buffer_citer::operator=(const Buffer_citer & other) {
    if (this != &other) {
        impl->buf_ = other.impl->buf_;
        impl->row_ = other.impl->row_;
        impl->col_ = other.impl->col_;
    }

    return *this;
}

Buffer_citer::Buffer_citer(Buffer_citer_impl * p):
    impl(p)
{
}

Buffer_citer::~Buffer_citer() {
    impl->buf_.reset();
    impl->busy_ = false;
    if (impl->heap_) { delete impl; }
}

void Buffer_citer::set(const Buffer_citer & other, std::size_t row, std::size_t col) {
    impl->buf_ = other.impl->buf_;
    impl->row_ = row;
    impl->col_ = col;
}

char32_t Buffer_citer::operator*() const noexcept {
    if (impl->buf_ && row() < impl->buf_->rows() && col() < impl->buf_->length(row())) {
        return impl->buf_->at(row(), col());
    }

    return 0;
}

char32_t Buffer_citer::operator[](ssize_t offset) const noexcept {
    Buffer_citer i(*this);

    while (0 != offset) {
        if (offset > 0) { ++i, --offset; }
        else { --i, ++offset; }
    }

    return *i;
}

ustring Buffer_citer::str(Buffer_citer other) const {
    return wstr(other);
}

std::size_t Buffer_citer::length(Buffer_citer other) const {
    std::size_t res = 0;

    if (impl->buf_ && other.impl->buf_ == impl->buf_) {
        res = impl->buf_->length(*this, other);
    }

    return res;
}

ustring Buffer_citer::str(std::size_t nchars) const {
    return wstr(nchars);
}

std::u32string Buffer_citer::wstr(Buffer_citer other) const {
    std::u32string res;

    if (impl->buf_ && other.impl->buf_ == impl->buf_) {
        res = impl->buf_->wstr(*this, other);
    }

    return res;
}

std::u32string Buffer_citer::wstr(std::size_t nchars) const {
    std::u32string res;

    if (impl->buf_ && !eof()) {
        std::size_t r = row(), c = col(), nlines = impl->buf_->rows(), len = impl->buf_->length(r);

        while (0 != nchars--) {
            res += impl->buf_->at(r, c++);

            if (c >= len) {
                if (++r >= nlines) { break; }
                c = 0;
                len = impl->buf_->length(r);
            }
        }
    }

    return res;
}

Buffer_citer & Buffer_citer::operator++() {
    if (impl->buf_) {
        std::size_t len = impl->buf_->length(row());

        do {
            if (1+col() < len) {
                ++impl->col_;
            }

            else if (1+row() < impl->buf_->rows()) {
                ++impl->row_;
                impl->col_ = 0;
            }

            else {
                impl->col_ = len;
                len = impl->buf_->length(row());
            }
        } while (!eof() && char32_is_modifier(operator*()));
    }

    return *this;
}

Buffer_citer Buffer_citer::operator++(int) {
    Buffer_citer result(*this);
    operator++();
    return result;
}

Buffer_citer & Buffer_citer::operator--() {
    if (impl->buf_) {
        do {
            if (0 != col()) {
                --impl->col_;
            }

            else if (0 != row()) {
                --impl->row_;
                impl->col_ = impl->buf_->length(row());
                if (0 != col()) { --impl->col_; }
            }
        } while (!sof() && char32_is_modifier(operator*()));
    }

    return *this;
}

Buffer_citer Buffer_citer::operator--(int) {
    Buffer_citer result(*this);
    operator--();
    return result;
}

Buffer_citer & Buffer_citer::operator+=(ssize_t n) {
    if (n > 0) { while (n--) operator++(); }
    else if (n < 0) { while (n++) operator--(); }
    return *this;
}

Buffer_citer & Buffer_citer::operator-=(ssize_t n) {
    if (n > 0) { while (n--) operator--(); }
    else if (n < 0) { while (n++) operator++(); }
    return *this;
}

bool Buffer_citer::operator==(const Buffer_citer & other) const noexcept {
    if (impl->buf_ && impl->buf_ == other.impl->buf_) {
        if (row() == other.row()) {
            return col() == other.col();
        }

        if (eof() && other.eof()) {
            return true;
        }
    }

    return false;
}

bool Buffer_citer::operator!=(const Buffer_citer & other) const noexcept {
    return !operator==(other);
}

bool Buffer_citer::operator<(const Buffer_citer & other) const noexcept {
    if (impl->buf_ && impl->buf_ == other.impl->buf_) {
        if (row() < other.row()) {
            return true;
        }

        if (row() == other.row()) {
            return col() < other.col();
        }
    }

    return false;
}

Buffer_citer::operator bool() const noexcept {
    return nullptr != impl->buf_;
}

size_t Buffer_citer::row() const noexcept {
    return impl->row_;
}

size_t Buffer_citer::col() const noexcept {
    return impl->col_;
}

bool Buffer_citer::eol() const noexcept {
    return eof() || char32_is_newline(operator*());
}

bool Buffer_citer::eof() const noexcept {
    if (impl->buf_) {
        std::size_t nrows = impl->buf_->rows();

        if (0 == nrows) {
            return true;
        }

        if (row() >= nrows) {
            return true;
        }

        if (row() == nrows-1) {
            if (col() >= impl->buf_->length(row())) {
                return true;
            }
        }

        return false;
    }

    return true;
}

bool Buffer_citer::sof() const noexcept {
    if (impl->buf_) {
        return 0 == row() && 0 == col();
    }

    return true;
}

void Buffer_citer::move_to(Buffer_citer other) noexcept {
    move_to(other.row(), other.col());
}

void Buffer_citer::move_to(size_t row, size_t col) noexcept {
    if (impl->buf_) {
        if (impl->buf_->empty()) {
            impl->row_ = 0;
            impl->col_ = 0;
        }

        else if (row < impl->buf_->rows()) {
            impl->row_ = row;
            impl->col_ = std::min(col, impl->buf_->length(row));
        }

        else {
            impl->row_ = impl->buf_->rows()-1;
            impl->col_ = impl->buf_->length(impl->row_);
        }
    }
}

void Buffer_citer::move_to(size_t col) noexcept {
    if (impl->buf_) {
        if (impl->row_ < impl->buf_->rows()) {
            impl->col_ = std::min(col, impl->buf_->length(impl->row_));
        }
    }
}

void Buffer_citer::move_to_sol() noexcept {
    if (impl->buf_) {
        impl->col_ = 0;
    }
}

void Buffer_citer::move_to_eol() noexcept {
    if (impl->buf_) {
        while (!eof() && !char32_is_newline(operator*())) {
            operator++();
        }
    }
}

Buffer_citer Buffer_citer::at_eol() {
    Buffer_citer i(*this);
    i.move_to_eol();
    return i;
}

void Buffer_citer::backward_line() noexcept {
    if (impl->buf_) {
        impl->col_ = 0;
        if (0 != impl->row_) { --impl->row_; }
    }
}

void Buffer_citer::forward_line() noexcept {
    if (impl->buf_) {
        size_t nrows = impl->buf_->rows();

        if (0 != nrows) {
            if (1+row() < nrows) {
                impl->col_ = 0;
                ++impl->row_;
            }

            else {
                impl->row_ = nrows-1;
                impl->col_ = impl->buf_->length(impl->row_);
            }
        }
    }
}

void Buffer_citer::backward_word() noexcept {
    if (impl->buf_) {
        std::size_t col = impl->col_;

        if (0 == col) {
            operator--();
        }

        else {
            operator--();

            if (char32_is_delimiter(operator*())) {
                while (0 != impl->col_ && char32_is_delimiter(operator*())) { operator--(); }
                if (0 == impl->col_) { return; }
                while (0 != impl->col_ && !char32_is_delimiter(operator*())) { operator--(); }
                if (impl->col_ < col && char32_is_delimiter(operator*())) { operator++(); }
            }

            else {
                while (0 != impl->col_ && !char32_is_delimiter(operator*())) { operator--(); }
                if (0 == impl->col_) { return; }
                if (impl->col_ < col && !char32_is_delimiter(operator*())) { operator++(); }
            }
        }
    }
}

void Buffer_citer::forward_word() noexcept {
    if (impl->buf_) {
        if (eol()) {
            operator++();
        }

        else if (char32_is_delimiter(operator*())) {
            while (!eol() && char32_is_delimiter(operator*())) { operator++(); }
        }

        else {
            while (!eol() && !char32_is_delimiter(operator*())) { operator++(); }
            while (!eol() && char32_is_delimiter(operator*())) { operator++(); }
        }
    }
}

void Buffer_citer::skip_blanks() noexcept {
    if (impl->buf_) {
        while (!eol() && char32_isblank(operator*())) {
            operator++();
        }
    }
}

void Buffer_citer::skip_whitespace() noexcept {
    if (impl->buf_) {
        while (!eof()) {
            char32_t c = operator*();
            if (!char32_isblank(c) && !char32_is_newline(c)) { break; }
            operator++();
        }
    }
}

void Buffer_citer::reset() {
    impl->buf_.reset();
    impl->row_ = 0;
    impl->col_ = 0;
}

bool Buffer_citer::find(char32_t wc) noexcept {
    if (impl->buf_ && 0x000000 != wc) {
        while (!eof()) {
            if (wc == operator*()) { return true; }
            operator++();
        }
    }

    return false;
}

bool Buffer_citer::find(char32_t wc, Buffer_citer other) noexcept {
    if (impl->buf_ && operator<(other) && 0x000000 != wc) {
        while (!eof() && operator<(other)) {
            if (wc == operator*()) { return true; }
            operator++();
        }
    }

    return false;
}

bool Buffer_citer::find(const ustring & text) noexcept {
    return find(std::u32string(text));
}

bool Buffer_citer::find(const ustring & text, Buffer_citer other) noexcept {
    return find(std::u32string(text), other);
}

bool Buffer_citer::find(const std::u32string & text) noexcept {
    std::size_t len = text.size();

    if (impl->buf_ && 0 != len) {
        while (!eof()) {
            if (text == wstr(len)) { return true; }
            operator++();
        }
    }

    return false;
}

bool Buffer_citer::find(const std::u32string & text, Buffer_citer other) noexcept {
    if (impl->buf_ && operator<(other)) {
        std::size_t len = text.size();

        if (0 != len) {
            while (!eof() && operator<(other)) {
                if (text == wstr(len)) { return true; }
                operator++();
            }
        }
    }

    return false;
}

bool Buffer_citer::find_first_of(const ustring & chars) noexcept {
    if (impl->buf_ && !chars.empty()) {
        while (!eof()) {
            char32_t wc = operator*();

            for (const char * p = chars.c_str(); '\0' != *p; p = utf8_next(p)) {
                if (char32_from_pointer(p) == wc) {
                    return true;
                }
            }

            operator++();
        }
    }

    return false;
}

bool Buffer_citer::find_first_of(const ustring & chars, Buffer_citer other) noexcept {
    if (impl->buf_ && operator<(other) && !chars.empty()) {
        while (!eof() && operator<(other)) {
            char32_t wc = operator*();

            for (const char * p = chars.c_str(); '\0' != *p; p = utf8_next(p)) {
                if (char32_from_pointer(p) == wc) {
                    return true;
                }
            }

            operator++();
        }
    }

    return false;
}

bool Buffer_citer::find_first_of(const std::u32string & chars) noexcept {
    if (impl->buf_ && !chars.empty()) {
        while (!eof()) {
            char32_t wc = operator*();

            for (const char32_t * p = chars.c_str(); U'\0' != *p; ++p) {
                if (*p == wc) {
                    return true;
                }
            }

            operator++();
        }
    }

    return false;
}

bool Buffer_citer::find_first_of(const std::u32string & chars, Buffer_citer other) noexcept {
    if (impl->buf_ && operator<(other) && !chars.empty()) {
        while (!eof() && operator<(other)) {
            char32_t wc = operator*();

            for (const char32_t * p = chars.c_str(); U'\0' != *p; ++p) {
                if (*p == wc) {
                    return true;
                }
            }

            operator++();
        }
    }

    return false;
}

bool Buffer_citer::find_first_not_of(const ustring & chars) noexcept {
    if (impl->buf_ && !chars.empty()) {
        while (!eof()) {
            char32_t wc = operator*();

            for (const char * p = chars.c_str(); '\0' != *p; p = utf8_next(p)) {
                if (char32_from_pointer(p) == wc) {
                    goto next;
                }
            }

            return true;

            next:
            operator++();
        }
    }

    return false;
}

bool Buffer_citer::find_first_not_of(const ustring & chars, Buffer_citer other) noexcept {
    if (impl->buf_ && operator<(other) && !chars.empty()) {
        while (!eof() && operator<(other)) {
            char32_t wc = operator*();

            for (const char * p = chars.c_str(); '\0' != *p; p = utf8_next(p)) {
                if (char32_from_pointer(p) == wc) {
                    goto next;
                }
            }

            return true;

            next:
            operator++();
        }
    }

    return false;
}

bool Buffer_citer::find_first_not_of(const std::u32string & chars) noexcept {
    if (impl->buf_ && !chars.empty()) {
        while (!eof()) {
            char32_t wc = operator*();

            for (const char32_t * p = chars.c_str(); U'\0' != *p; ++p) {
                if (*p == wc) {
                    goto next;
                }
            }

            return true;

            next:
            operator++();
        }
    }

    return false;
}

bool Buffer_citer::find_first_not_of(const std::u32string & chars, Buffer_citer other) noexcept {
    if (impl->buf_ && operator<(other) && !chars.empty()) {
        while (!eof() && operator<(other)) {
            char32_t wc = operator*();

            for (const char32_t * p = chars.c_str(); U'\0' != *p; ++p) {
                if (*p == wc) {
                    goto next;
                }
            }

            return true;

            next:
            operator++();
        }
    }

    return false;
}

bool Buffer_citer::equals(const ustring & text, bool advance) noexcept {
    return equals(std::u32string(text), advance);
}

bool Buffer_citer::equals(const std::u32string & text, bool advance) noexcept {
    std::size_t len = text.size();

    if (impl->buf_ && 0 != len) {
        if (wstr(len) == text) {
            if (advance) { operator+=(len); }
            return true;
        }
    }

    return false;
}

/// @since 0.7.0
Buffer_citer operator+(const Buffer_citer & i, int n) {
    auto j(i);
    j += n;
    return j;
}

/// @since 0.7.0
Buffer_citer operator-(const Buffer_citer & i, int n) {
    auto j(i);
    j -= n;
    return j;
}

/// @since 0.7.0
ssize_t operator-(const Buffer_citer & i, const Buffer_citer & j) {
    ssize_t n = i.length(j);
    return i >= j ? n : -n;
}

} // namespace tau

//END

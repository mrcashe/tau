// ----------------------------------------------------------------------------
// SPDX-License-Identifier: BSD-2-Clause
// Copyright © 2014-2024 Konstantin Shmelkov <mrcashe@gmail.com>.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
// THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
// PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
// CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
// EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
// PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
// OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
// WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
// OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
// EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// ----------------------------------------------------------------------------

#include <tau/roller.hh>
#include <tau/scroller.hh>
#include <roller-impl.hh>
#include <scroller-impl.hh>

namespace tau {

#define ROLLER_IMPL (std::static_pointer_cast<Roller_impl>(impl))

Roller::Roller(bool autohide):
    Container(std::make_shared<Roller_impl>(autohide))
{
}

Roller::Roller(Orientation orient, bool autohide):
    Container(std::make_shared<Roller_impl>(orient, autohide))
{
}

Roller::Roller(const Roller & other):
    Container(other.impl)
{
}

Roller & Roller::operator=(const Roller & other) {
    Container::operator=(other);
    return *this;
}

Roller::Roller(Roller && other):
    Container(other.impl)
{
}

Roller & Roller::operator=(Roller && other) {
    Container::operator=(other);
    return *this;
}

Roller::Roller(Widget_ptr wp):
    Container(std::dynamic_pointer_cast<Roller_impl>(wp))
{
}

Roller & Roller::operator=(Widget_ptr wp) {
    Container::operator=(std::dynamic_pointer_cast<Roller_impl>(wp));
    return *this;
}

void Roller::insert(Widget & w) {
    ROLLER_IMPL->insert(w.ptr());
}

void Roller::clear() {
    ROLLER_IMPL->clear();
}

bool Roller::empty() const noexcept {
    return ROLLER_IMPL->empty();
}

Scroller Roller::scroller() {
    return ROLLER_IMPL->scroller_ptr();
}

const Scroller Roller::scroller() const {
    return ROLLER_IMPL->scroller_ptr();
}

void Roller::pan(int pos) {
    ROLLER_IMPL->pan(pos);
}

int Roller::pan() const noexcept {
    return ROLLER_IMPL->pan();
}

void Roller::allow_autohide() noexcept {
    ROLLER_IMPL->allow_autohide();
}

void Roller::disallow_autohide() noexcept {
    ROLLER_IMPL->disallow_autohide();
}

bool Roller::autohide_allowed() const noexcept {
    return ROLLER_IMPL->autohide_allowed();
}

void Roller::set_step(int step) {
    ROLLER_IMPL->set_step(step);
}

int Roller::step() const noexcept {
    return ROLLER_IMPL->step();
}

} // namespace tau

//END

// ----------------------------------------------------------------------------
// SPDX-License-Identifier: BSD-2-Clause
// Copyright © 2014-2024 Konstantin Shmelkov <mrcashe@gmail.com>.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
// THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
// PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
// CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
// EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
// PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
// OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
// WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
// OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
// EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// ----------------------------------------------------------------------------

/// @file table-impl.cc The Table_impl class implementation.
/// The header is table-impl.hh.

#include <table-impl.hh>
#include <tau/brush.hh>
#include <tau/painter.hh>
#include <iostream>

namespace tau {

Table_impl::Table_impl() {
    init();
}

Table_impl::Table_impl(unsigned xspacing, unsigned yspacing):
    xspacing_(xspacing),
    yspacing_(yspacing)
{
    init();
}

Table_impl::Table_impl(unsigned spacing):
    xspacing_(spacing),
    yspacing_(spacing)
{
    init();
}

Table_impl::Table_impl(Align xalign, Align yalign):
    xalign_(xalign),
    yalign_(yalign)
{
    init();
}

Table_impl::Table_impl(Align xalign, Align yalign, unsigned spacing):
    xalign_(xalign),
    yalign_(yalign),
    xspacing_(spacing),
    yspacing_(spacing)
{
    init();
}

Table_impl::Table_impl(Align xalign, Align yalign, unsigned xspacing, unsigned yspacing):
    xalign_(xalign),
    yalign_(yalign),
    xspacing_(xspacing),
    yspacing_(yspacing)
{
    init();
}

Table_impl::~Table_impl() {
    destroy();
    if (signal_column_bounds_changed_) { delete signal_column_bounds_changed_; }
    if (signal_row_bounds_changed_) { delete signal_row_bounds_changed_; }
}

void Table_impl::init() {
    conf().signal_changed(Conf::SELECT_BACKGROUND).connect(fun(this, &Table_impl::on_select_background));
    signal_arrange_.connect(fun(this, &Table_impl::arrange));
    signal_size_changed_.connect(fun(this, &Table_impl::on_size_changed));
    signal_display_in_.connect(fun(this, &Table_impl::on_display_in));
    once_cx_ = signal_display_in_.connect(fun(this, &Table_impl::on_display_once));
    signal_take_focus_.connect(fun(this, &Table_impl::on_take_focus), true);
}

void Table_impl::put(Widget_ptr wp, const Span & spn, bool xsh, bool ysh) {
    if (wp) {
        chk_parent(wp);
        update_child_bounds(wp.get(), INT_MIN, INT_MIN);
        auto [hi, succeed] = holders_.emplace(wp.get(), Holder());
        auto & hol = hi->second;
        hol.wp_ = wp;
        hol.spn_ = spn;
        hol.xsh_ = xsh;
        hol.ysh_ = ysh;
        make_child(wp);
        dist(hi);
        if (!wp->hidden()) { on_hints(hi, Hints::SIZE); }
        hol.cx_ = wp->signal_hints_changed().connect(tau::bind_front(fun(this, &Table_impl::on_hints), hi));
    }
}

std::size_t Table_impl::remove_priv(const std::list<Widget_impl *> & wps) {
    std::size_t n = 0;
    invalidate_children(wps);

    for (auto wp: wps) {
        auto i = holders_.find(wp);
        if (i != holders_.end()) { ++n; wipe(i); holders_.erase(i); }
    }

    unparent(wps);
    recalc(), queue_arrange(), invalidate();
    return n;
}

std::size_t Table_impl::remove(const Span & rng) {
    auto v = widgets(rng);
    std::list<Widget_impl *> w(v.size());
    std::transform(v.begin(), v.end(), w.begin(), [](auto & wp) { return wp.get(); } );
    return remove_priv(w);
}

void Table_impl::clear() {
    unmark();
    unselect();
    holders_.clear();
    vis_cols_ = vis_rows_ = user_cols_ = user_rows_ = shrank_cols_ = shrank_rows_ = 0;
    xspc_ = yspc_ = shrank_xreq_ = shrank_yreq_ = xreq_ = yreq_ = 0;
    span_ = Span();

    if (!rows_.empty()) {
        int yy = rows_.begin()->first, ym = rows_.rbegin()->first;

        for (; yy <= ym; ++yy) {
            auto j = rows_.find(yy);

            if (j != rows_.end()) {
                erase(j);
                if (!in_shutdown() && signal_row_bounds_changed_) { signal_row_bounds_changed_->operator()(yy); }
            }
        }
    }

    if (!cols_.empty()) {
        int xx = cols_.begin()->first, xm = cols_.rbegin()->first;

        for (; xx <= xm; ++xx) {
            auto i = cols_.find(xx);

            if (i != cols_.end()) {
                erase(i);
                if (!in_shutdown() && signal_column_bounds_changed_) { signal_column_bounds_changed_->operator()(xx); }
            }
        }
    }

    unparent_all(), recalc(), invalidate();
}

void Table_impl::on_hints(Hiter hi, Hints op) {
    if (!in_shutdown()) {
        auto & hol = hi->second;

        if (Hints::SHOW == op) {
            auto ci = cols_.find(hol.spn_.xmin), ce = cols_.find(hol.spn_.xmax-1);
            do { show(ci, hol); } while (ci++ != ce);

            auto ri = rows_.find(hol.spn_.ymin), re = rows_.find(hol.spn_.ymax-1);
            do { show(ri, hol); } while (ri++ != re);

            recalc(), queue_arrange(), invalidate();
        }

        else if (Hints::HIDE == op) {
            update_child_bounds(hol.wp_.get(), INT_MIN, INT_MIN);

            auto ci = cols_.find(hol.spn_.xmin), ce = cols_.find(hol.spn_.xmax-1);
            do { hide(ci, hol.xsh_); } while (ci++ != ce);

            auto ri = rows_.find(hol.spn_.ymin), re = rows_.find(hol.spn_.ymax-1);
            do { hide(ri, hol.ysh_); } while (ri++ != re);

            recalc(), queue_arrange(), invalidate();
        }

        else if (Hints::SIZE == op) {
            recalc(), queue_arrange();
            // Size max = hi->second.max_;
            // auto i = cols_.find(hi->second.spn_.xmin), ii = cols_.find(hi->second.spn_.xmax-1);
            // auto j = rows_.find(hi->second.spn_.ymin), jj = rows_.find(hi->second.spn_.ymax-1);
            //
            // if (child_requisition(hi->second)) {
            //     if (ii == i && jj == j) {
            //         if (max.width() < i->second.rmax_ && max.height() < j->second.rmax_) {
            //             if (hi->second.min_.width() >= i->second.rmin_ && hi->second.min_.height() >= j->second.rmin_ && hi->second.max_.width() <= i->second.rmax_ && hi->second.max_.height() <= j->second.rmax_) {
            //                 place(hi->second);
            //                 return;
            //             }
            //         }
            //     }
            //
            //     int x1 = INT_MAX, x2 = INT_MIN, y1 = INT_MAX, y2 = INT_MIN;
            //     auto in = i;
            //
            //     do {
            //         for (auto h: in->second.refs_) {
            //             x1 = std::min(x1, h->second.spn_.xmin), x2 = std::max(x2, h->second.spn_.xmax);
            //             y1 = std::min(y1, h->second.spn_.ymin), y2 = std::max(y2, h->second.spn_.ymax);
            //         }
            //     } while (in++ != ii);
            //
            //     auto jn = j;
            //
            //     do {
            //         for (auto h: jn->second.refs_) {
            //             x1 = std::min(x1, h->second.spn_.xmin), x2 = std::max(x2, h->second.spn_.xmax);
            //             y1 = std::min(y1, h->second.spn_.ymin), y2 = std::max(y2, h->second.spn_.ymax);
            //         }
            //     } while (jn++ != jj);
            //
            //     i = cols_.find(x1), ii = cols_.find(x2-1); do { req(i->second); } while (i++ != ii);
            //     j = rows_.find(y1), jj = rows_.find(y2-1); do { req(j->second); } while (j++ != jj);
            //     update_requisition(), alloc_cols(), alloc_rows(), queue_arrange();
            // }
        }
    }
}

bool Table_impl::child_requisition(Holder & hol) noexcept {
    bool changed = false;

    if (!hol.wp_->hidden()) {
        Size req = hol.wp_->required_size();
        Size min = hol.wp_->min_size_hint();
        Size max = hol.wp_->max_size_hint();
        Size ex  = hol.wp_->size_hint();

        req.update_max(min);
        req.update_min(max, true);
        req.update(ex, true);
        min.update(ex, true);
        if (hol.wp_->has_aspect_ratio_hint()) { req.apply_aspect_ratio(hol.wp_->required_aspect_ratio()); }

        Margin m = hol.wp_->margin_hint();
        req.increase(m.left+m.right, m.top+m.bottom);
        if (hol.max_.update(req)) { changed = true; }
        if (hol.min_.update(hol.xsh_ ? req.width() : min.width(), hol.ysh_ ? req.height() : min.height())) { changed = true; }
    }

    else {
        hol.max_.reset(), hol.min_.reset();
    }

    return changed;
}

bool Table_impl::req_horz(Holder & hol) {
    bool changed = false;
    auto cb = cols_.find(hol.spn_.xmin), ce = cols_.find(hol.spn_.xmax-1), i = cb;
    unsigned nc = 1, wmax = hol.max_.width(), wmin = hol.min_.width();
    bool sh = wmin && wmax == wmin;

    while (i++ != ce) {
        unsigned spc = xspacing_+i->second.right_;
        if (i != cb) { spc += i->second.left_; }
        wmax = wmax > spc ? wmax-spc : 0;
        wmin = wmin > spc ? wmin-spc : 0;
        if (sh || !i->second.shrank_) { ++nc; }
    }

    unsigned wpc_max = wmax/nc, rem_max = wmax%nc;  // Calculate width per cell and remainder per cell.
    unsigned wpc_min = wmin/nc, rem_min = wmin%nc;
    i = cb;

    do {
        unsigned w1 = wpc_max, w2 = wpc_min, nv;
        if (w1 && rem_max) { ++w1, --rem_max; }
        if (w2 && rem_min) { ++w2, --rem_min; }

        if (sh || (!i->second.shrank_ && cb == ce)) {
            nv = std::max(i->second.rmax_, w1);
            if (i->second.rmax_ != nv) { i->second.rmax_ = nv; changed = true; }
        }

        nv = std::max(i->second.rmin_, w2);
        if (i->second.rmin_ != nv) { i->second.rmin_ = nv; changed = true; }
    } while (i++ != ce);

    return changed;
}

bool Table_impl::req_vert(Holder & hol) {
    bool changed = false;
    auto rb = rows_.find(hol.spn_.ymin), re = rows_.find(hol.spn_.ymax-1), i = rb;
    unsigned nc = 1, hmax = hol.max_.height(), hmin = hol.min_.height();
    bool sh = hmin && hmax == hmin;

    while (i++ != re) {
        unsigned spc = yspacing_+i->second.bottom_;
        if (i != rb) { spc += i->second.top_; }
        hmax = hmax > spc ? hmax-spc : 0;
        hmin = hmin > spc ? hmin-spc : 0;
        if (sh || !i->second.shrank_) { ++nc; }
    }

    unsigned hpc_max = hmax/nc, rem_max = hmax%nc;  // Calculate width per cell and remainder per cell.
    unsigned hpc_min = hmin/nc, rem_min = hmin%nc;
    i = rb;

    do {
        unsigned h1 = hpc_max, h2 = hpc_min, nv;
        if (h1 && rem_max) { ++h1; --rem_max; }
        if (h2 && rem_min) { ++h2; --rem_min; }

        if (sh || (!i->second.shrank_ && rb == re)) {
            nv = std::max(i->second.rmax_, h1);
            if (i->second.rmax_ != nv) { i->second.rmax_ = nv; changed = true; }
        }

        nv = std::max(i->second.rmin_, h2);
        if (i->second.rmin_ != nv) { i->second.rmin_ = nv; changed = true; }
    } while (i++ != re);

    return changed;
}

unsigned Table_impl::req(Col & col) noexcept {
    col.rmin_ = col.rmax_ = 0;

    if (col.visible_) {
        for (auto hi: col.refs_) { req_horz(hi->second); }
        unsigned px = std::max(col.umin_, (col.usize_ ? col.usize_ : std::max(col.rmin_, col.rmax_)));
        if (col.umax_) { px = std::min(px, col.umax_); }
        int d = px-col.lr_;
        col.lr_ = px;

        if (d > 0) {
            (col.shrank_ ? shrank_xreq_ : xreq_) += d;
        }

        else if (d < 0) {
            unsigned ud = std::abs(d);
            if (col.shrank_) { shrank_xreq_ = shrank_xreq_ > ud ? shrank_xreq_-ud : 0; }
            else { xreq_ = xreq_ > ud ? xreq_-ud : 0; }
        }
    }

    return col.lr_;
}

unsigned Table_impl::req(Row & row) noexcept {
    row.rmin_ = row.rmax_ = 0;

    if (row.visible_) {
        for (auto hi: row.refs_) { req_vert(hi->second); }
        unsigned px = std::max(row.umin_, (row.usize_ ? row.usize_ : std::max(row.rmin_, row.rmax_)));
        if (row.umax_) { px = std::min(px, row.umax_); }
        int d = px-row.lr_;
        row.lr_ = px;

        if (d > 0) {
            (row.shrank_ ? shrank_yreq_ : yreq_) += d;
        }

        else if (d < 0) {
            unsigned ud = std::abs(d);
            if (row.shrank_) { shrank_yreq_ = shrank_yreq_ > ud ? shrank_yreq_-ud : 0; }
            else { yreq_ = yreq_ > ud ? yreq_-ud : 0; }
        }
    }

    return row.lr_;
}

void Table_impl::recalc() {
    for (auto & p: holders_) { child_requisition(p.second); p.second.place_ = true; }
    for (auto & p: cols_) { req(p.second); }
    for (auto & p: rows_) { req(p.second); }
    update_requisition();
}

void Table_impl::arrange() {
    alloc_cols();
    alloc_rows();
    place();
}

void Table_impl::update_avail() noexcept {
    xavail_ = logical_size().width();
    unsigned req = xspc_+shrank_xreq_;
    xavail_ = xavail_ > req ? xavail_-req : 0;
    yavail_ = logical_size().height();
    req = yspc_+shrank_yreq_;
    yavail_ = yavail_ > req ? yavail_-req : 0;
}

void Table_impl::update_requisition() {
    require_size(xspc_+shrank_xreq_+xreq_, yspc_+shrank_yreq_+yreq_);
    update_avail();
}

void Table_impl::alloc_cols() {
    int x = 0;

    for (auto i = cols_.begin(); i != cols_.end(); ++i) {
        auto & col = i->second;

        if (col.visible_) {
            unsigned nexp = vis_cols_-user_cols_-shrank_cols_;
            unsigned nextra = nexp ? nexp : shrank_cols_;
            unsigned xextra = nextra ? xavail_/nextra : 0;
            unsigned px;
            x += col.left_;
            col.x_ = x;

            if (col.usize_) {
                px = col.usize_;
                px = std::max(px, col.umin_);
                if (col.umax_) { px = std::min(px, col.umax_); }
            }

            // Shrank column.
            else if (col.shrank_) {
                px = std::max(col.rmin_, col.rmax_);

                if (!nexp && !user_cols_ && (Align::FILL == col.xalign_ || Align::FILL == xalign_)) {
                    px += xextra;
                    // TODO remainder
                    //if (xavail_%nextra) { ++px; }
                }

                px = std::max(px, col.umin_);
                if (col.umax_) { px = std::min(px, col.umax_); }
            }

            // Expanded column.
            else {
                px = std::max(xextra, col.rmin_);
                if (!px) { px = col.rmin_ ? col.rmin_ : col.rmax_; }
                px = std::max(px, col.umin_);
                if (col.umax_) { px = std::min(px, col.umax_); }
                // TODO remainder
                // else if (xavail_%nextra) { ++px; }
            }

            x += px;
            col.w_ = x-col.x_;
            x += col.right_+xspacing_;

            if (col.lx_ != col.x_ || col.lw_ != col.w_) {
                col.lx_ = col.x_, col.lw_ = col.w_;
                if (!in_shutdown() && signal_column_bounds_changed_) { (*signal_column_bounds_changed_)(i->first); }
                for (auto hi: col.refs_) { hi->second.place_ = true; }
            }
        }
    }
}

void Table_impl::alloc_rows() {
    int y = 0;

    for (auto i = rows_.begin(); i != rows_.end(); ++i) {
        auto & row = i->second;

        if (row.visible_) {
            unsigned nexp = vis_rows_-user_rows_-shrank_rows_;
            unsigned nextra = nexp ? nexp : shrank_rows_;
            unsigned yextra = nextra ? yavail_/nextra : 0;
            unsigned px;
            y += row.top_;
            row.y_ = y;

            if (row.usize_) {
                px = row.usize_;
                px = std::max(px, row.umin_);
                if (row.umax_) { px = std::min(px, row.umax_); }
            }

            else if (row.shrank_) {
                px = std::max(row.rmin_, row.rmax_);

                if (!nexp && !user_rows_ && (Align::FILL == row.yalign_ || Align::FILL == yalign_)) {
                    px += yextra;
                    // TODO remainder
                    // if (yavail_%nextra) { ++px; }
                }

                px = std::max(px, row.umin_);
                if (row.umax_) { px = std::min(px, row.umax_); }
            }

            else {
                px = std::max(yextra, row.rmin_);
                if (!px) { px = row.rmin_ ? row.rmin_ : row.rmax_; }
                px = std::max(px, row.umin_);
                if (row.umax_) { px = std::min(px, row.umax_); }
                // TODO remainder
                // else if (yavail_%nextra) { ++px; }
            }

            y += px;
            row.h_ = y-row.y_;
            y += row.bottom_+yspacing_;

            if (row.ly_ != row.y_ || row.lh_ != row.h_) {
                row.ly_ = row.y_, row.lh_ = row.h_;
                if (!in_shutdown() && signal_row_bounds_changed_) { (*signal_row_bounds_changed_)(i->first); }
                for (auto hi: row.refs_) { hi->second.place_ = true; }
            }
        }
    }
}

void Table_impl::place(Holder & hol, Rect * inval) {
    if (!hol.wp_->hidden() && size()) {
        Align xalign = xalign_, yalign = yalign_; Point o; Size  s;

        auto col = cols_.find(hol.spn_.xmin), ecol = cols_.find(hol.spn_.xmax-1);
        if (col->second.xalign_) { xalign = *col->second.xalign_; }
        o.translate(col->second.x_, 0);

        do {
            s.increase(col->second.w_, 0);
            if (col->first > hol.spn_.xmin) { s.increase(xspacing_+col->second.left_, 0); }
            if (1 < hol.spn_.xmax-col->first) { s.increase(col->second.right_, 0); }
        } while (col++ != ecol);

        auto row = rows_.find(hol.spn_.ymin), erow = rows_.find(hol.spn_.ymax-1);
        if (row->second.yalign_) { yalign = *row->second.yalign_; }
        o.translate(0, row->second.y_);

        do {
            s.increase(0, row->second.h_);
            if (row->first > hol.spn_.ymin) { s.increase(0, yspacing_+row->second.top_); }
            if (1 < hol.spn_.ymax-row->first) { s.increase(0, row->second.bottom_); }
        } while (row++ != erow);

        Size xs = hol.max_;
        if (hol.wp_->has_aspect_ratio_hint()) { xs.apply_aspect_ratio(hol.wp_->required_aspect_ratio()); }
        xs = s-xs;
        if (hol.has_align_) { xalign = hol.xalign_, yalign = hol.yalign_; }

        // Align widgets inside cell bounds. FIXME Is it workaround with aspect ratio?
        if (hol.wp_->has_aspect_ratio_hint() || (Align::FILL != xalign && hol.max_.width() && hol.max_.width() == hol.min_.width())) {
            s.update_width(hol.max_.width());
            if (Align::END == xalign) { o.translate(xs.width(), 0); }
            else if (Align::CENTER == xalign) { o.translate(xs.width()/2, 0); }
        }

        if (hol.wp_->has_aspect_ratio_hint() || (Align::FILL != yalign && hol.max_.height() && hol.max_.height() == hol.min_.height())) {
            s.update_height(hol.max_.height());
            if (Align::END == yalign) { o.translate(0, xs.height()); }
            else if (Align::CENTER == yalign) { o.translate(0, xs.height()/2); }
        }

        Margin m = hol.wp_->margin_hint();
        o.translate(m.left, m.top);
        s.decrease(m.left+m.right, m.top+m.bottom);
        Rect r(hol.wp_->origin(), hol.wp_->size());

        if (update_child_bounds(hol.wp_.get(), o, s)) {
            if (inval) { *inval |= (r|Rect(o, s)); }
            else { invalidate(r|Rect(o, s)); }
        }
    }
}

void Table_impl::place() {
    Rect inval;

    for (auto & r: rows_) {
        for (auto hi: r.second.refs_) {
            // if (hi->second.place_) {
                // hi->second.place_ = false;
                place(hi->second, &inval);
            // }
        }
    }

    if (inval) { invalidate(inval); }
}

std::list<Table_impl::Holder *> Table_impl::holders(const Span & spn) {
    std::list<Holder *> v;

    if (spn.xmax > spn.xmin && spn.ymax > spn.ymin) {
        for (auto & p: holders_) {
            auto & hol = p.second;
            if (hol.spn_.xmax > spn.xmin && hol.spn_.xmin < spn.xmax && hol.spn_.ymax > spn.ymin && hol.spn_.ymin < spn.ymax) { v.push_back(&hol); }
        }
    }

    return v;
}

std::list<Widget_ptr> Table_impl::widgets(const Span & spn) const {
    std::list<Widget_ptr> v;

    // Filter out invalid input data.
    if (spn.xmax > spn.xmin && spn.ymax > spn.ymin) {
        for (auto & p: holders_) {
            auto & hol = p.second;
            if (hol.spn_.xmax > spn.xmin && hol.spn_.xmin < spn.xmax && hol.spn_.ymax > spn.ymin && hol.spn_.ymin < spn.ymax) { v.push_back(hol.wp_); }
        }
    }

    return v;
}

Span Table_impl::span(const Widget_impl * wp) const noexcept {
    Span spn;
    auto i = holders_.find(const_cast<Widget_impl *>(wp));
    if (holders_.end() != i) { spn = i->second.spn_; }
    return spn;
}

bool Table_impl::erasable(const Col & col) const noexcept {
    return !col.xalign_ && !col.left_ && !col.right_ && !col.umin_ && !col.umax_ && !col.usize_;
}

void Table_impl::trim(Col_iter i) {
    if (span_.xmax == i->first+1) {  // trim span right?
        Cols::reverse_iterator j(i);
        while (j != cols_.rend() && ++j != cols_.rend() && j->second.refs_.empty());
        span_.xmax = j == cols_.rend() ? INT_MIN : 1+j.base()->first;
    }

    if (span_.xmin == i->first) {   // trim span left?
        auto j = i;
        while (j != cols_.end() && ++j != cols_.end() && j->second.refs_.empty());
        span_.xmin = j == cols_.end() ? INT_MAX : j->first;
    }
}

void Table_impl::erase(Col_iter i) {
    if (i != cols_.end()) {
        trim(i);

        if (i->second.visible_) {
            i->second.visible_ = 0;
            if (i->second.w_) { i->second.w_ = 0; if (!in_shutdown() && signal_column_bounds_changed_) (*signal_column_bounds_changed_)(i->first); }
            if (vis_cols_) { --vis_cols_; }
        }

        i->second.lr_ = i->second.x_ = i->second.lx_ = i->second.w_ = i->second.lw_ = i->second.shrank_ = 0;
        i->second.refs_.clear();
        if (erasable(i->second)) { cols_.erase(i); }
    }
}

void Table_impl::unref(Col_iter i, Hiter hi) {
    if (i != cols_.end() && !i->second.refs_.empty()) {
        trim(i);
        i->second.ymin_ = INT_MAX, i->second.ymax_ = INT_MIN;
        auto k = std::find(begin(i->second.refs_), end(i->second.refs_), hi);
        if (k != i->second.refs_.end()) { i->second.refs_.erase(k); }
        if (i->second.refs_.empty()) { erase(i); }
    }
}

bool Table_impl::erasable(const Row & row) const noexcept {
    return !row.yalign_ && !row.top_ && !row.bottom_ && !row.umin_ && !row.umax_ && !row.usize_;
}

void Table_impl::trim(Row_iter i) {
    if (span_.ymax == i->first+1) {  // trim span bottom?
        Rows::reverse_iterator j(i);
        while (j != rows_.rend() && ++j != rows_.rend() && j->second.refs_.empty()) { }
        span_.ymax = j == rows_.rend() ? INT_MIN : 1+j.base()->first;
    }

    if (span_.ymin == i->first) {   // trim span top?
        auto j = i;
        while (j != rows_.end() && ++j != rows_.end() && j->second.refs_.empty());
        span_.ymin = j == rows_.end() ? INT_MAX : j->first;
    }
}

void Table_impl::erase(Row_iter i) {
    if (i != rows_.end()) {
        trim(i);

        if (i->second.visible_) {
            i->second.visible_ = 0;
            if (i->second.h_) { i->second.h_ = 0; if (!in_shutdown() && signal_row_bounds_changed_) (*signal_row_bounds_changed_)(i->first); }
            if (vis_rows_) { --vis_rows_; }
        }

        i->second.lr_ = i->second.y_ = i->second.ly_ = i->second.h_ = i->second.lh_ = i->second.shrank_ = 0;
        i->second.refs_.clear();
        if (erasable(i->second)) { rows_.erase(i); }
    }
}

void Table_impl::unref(Row_iter i, Hiter hi) {
    if (i != rows_.end() && !i->second.refs_.empty()) {
        trim(i);
        i->second.xmin_ = INT_MAX, i->second.xmax_ = INT_MIN;
        auto k = std::find(begin(i->second.refs_), end(i->second.refs_), hi);
        if (k != i->second.refs_.end()) { i->second.refs_.erase(k); }
        if (i->second.refs_.empty()) { erase(i); }
    }
}

Table_impl::Col_iter Table_impl::new_col(int xx, const Col & src) {
    cols_.erase(xx);
    auto i = cols_.emplace(xx, src).first;
    i->second.ymin_ = INT_MAX, i->second.ymax_ = INT_MIN;
    if (!i->second.refs_.empty()) { span_.xmin = std::min(span_.xmin, xx), span_.xmax = std::max(span_.xmax, xx+1); }
    return i;
}

Table_impl::Row_iter Table_impl::new_row(int yy, const Row & src) {
    rows_.erase(yy);
    auto i = rows_.emplace(yy, src).first;
    i->second.xmin_ = INT_MAX, i->second.xmax_ = INT_MIN;
    if (!i->second.refs_.empty()) { span_.ymin = std::min(span_.ymin, yy), span_.ymax = std::max(span_.ymax, yy+1); }
    return i;
}

void Table_impl::show(Col_iter i, Holder & hol) {
    unsigned px = req(i->second);

    if (hol.xsh_ && 0 == i->second.shrank_++) {
        ++shrank_cols_;
        xreq_ = xreq_ > px ? xreq_-px : 0;
        shrank_xreq_ += px;
    }

    if (0 == i->second.visible_++) {
        if (++vis_cols_ > 1) { xspc_ += xspacing_; }
        xspc_ += i->second.left_+i->second.right_;
        if (i->second.usize_) { ++user_cols_; }
        else if (!i->second.shrank_) { xreq_ += px; }
    }
}

void Table_impl::hide(Col_iter i) {
    if (i->second.visible_) { i->second.visible_ = 1; }
    if (i->second.shrank_) { i->second.shrank_ = 1; }
    hide(i, i->second.shrank_);
}

void Table_impl::hide(Col_iter i, bool shrank) {
    unsigned px = req(i->second);

    if (i->second.visible_ && --i->second.visible_ == 0) {
        if (1 < vis_cols_--) { xspc_ -= xspacing_; }
        xspc_ = xspc_ > i->second.left_+i->second.right_ ? xspc_-i->second.left_-i->second.right_ : 0;
        if (i->second.usize_ && user_cols_) { --user_cols_; }
        else if (i->second.shrank_) { shrank_xreq_ = shrank_xreq_ > px ? shrank_xreq_-px : 0; }
        else { xreq_ = xreq_ > px ? xreq_-px : 0; }
    }

    if (shrank && i->second.shrank_ && --i->second.shrank_ == 0) {
        if (shrank_cols_) { --shrank_cols_; }
        if (i->second.visible_) { shrank_xreq_ = shrank_xreq_ > px ? shrank_xreq_-px : 0; xreq_ += px; }
    }
}

void Table_impl::show(Row_iter i, Holder & hol) {
    unsigned px = req(i->second);

    if (hol.ysh_ && 0 == i->second.shrank_++) {
        yreq_ = yreq_ > px ? yreq_-px : 0;
        shrank_yreq_ += px;
        ++shrank_rows_;
    }

    if (0 == i->second.visible_++) {
        if (++vis_rows_ > 1) { yspc_ += yspacing_; }
        yspc_ += i->second.top_+i->second.bottom_;
        if (i->second.usize_) { ++user_rows_; }
        else if (!i->second.shrank_) {  yreq_ += px; }
    }
}

void Table_impl::hide(Row_iter i) {
    if (i->second.visible_) { i->second.visible_ = 1; }
    if (i->second.shrank_) { i->second.shrank_ = 1; }
    hide(i, i->second.shrank_);
}

void Table_impl::hide(Row_iter i, bool shrank) {
    unsigned px = req(i->second);

    if (i->second.visible_ && --i->second.visible_ == 0) {
        if (1 < vis_rows_--) { yspc_ -= yspacing_; }
        yspc_ = yspc_ > i->second.top_+i->second.bottom_ ? yspc_-i->second.top_-i->second.bottom_ : 0;
        if (i->second.usize_ && user_rows_) { --user_rows_; }
        else if (i->second.shrank_) { shrank_yreq_ = shrank_yreq_ > px ? shrank_yreq_-px : 0; }
        else { yreq_ = yreq_ > px ? yreq_-px : 0; }
    }

    if (shrank && 0 != i->second.shrank_ && --i->second.shrank_ == 0) {
        if (shrank_rows_) { --shrank_rows_; }
        if (i->second.visible_) { shrank_yreq_ = shrank_yreq_ > px ? shrank_yreq_-px : 0; yreq_ += px; }
    }
}

void Table_impl::dist(Hiter hi) {
    Holder & hol = hi->second;
    bool marks = false;     // Marks need to be updated.

    for (int xx = hol.spn_.xmin; xx < hol.spn_.xmax; ++xx) {
        auto i = cols_.find(xx);
        if (i == cols_.end()) { i = new_col(xx); }
        i->second.refs_.push_back(hi);
        i->second.ymin_ = std::min(i->second.ymin_, hol.spn_.ymin);
        i->second.ymax_ = std::max(i->second.ymax_, hol.spn_.ymax);
        span_.xmin = std::min(span_.xmin, xx);
        span_.xmax = std::max(span_.xmax, xx+1);
        if (1 == i->second.refs_.size()) { marks = true; }
        if (!hi->second.wp_->hidden()) { show(i, hol); }
    }

    for (int yy = hol.spn_.ymin; yy < hol.spn_.ymax; ++yy) {
        auto i = rows_.find(yy);
        if (i == rows_.end()) { i = new_row(yy); }
        i->second.refs_.push_back(hi);
        i->second.xmin_ = std::min(i->second.xmin_, hol.spn_.xmin);
        i->second.xmax_ = std::max(i->second.xmax_, hol.spn_.xmax);
        span_.ymin = std::min(span_.ymin, yy);
        span_.ymax = std::max(span_.ymax, yy+1);
        if (1 == i->second.refs_.size()) { marks = true; }
        if (!hi->second.wp_->hidden()) { show(i, hol); }
    }

    if (marks) { update_marks(); }

    // Change background, if inserted into selection...
    if (has_selection() && hol.spn_.xmin >= sel_.xmin && hol.spn_.ymin >= sel_.ymin && hol.spn_.xmax <= sel_.xmax && hol.spn_.ymax <= sel_.ymax) {
        hol.wp_->signal_select()();
        hol.wp_->conf().set_from(Conf::BACKGROUND, Conf::SELECT_BACKGROUND, true);
        hol.wp_->conf().set_from(Conf::WHITESPACE_BACKGROUND, Conf::SELECT_BACKGROUND, true);
        if (!in_shutdown()) { signal_selection_changed_(); }
    }

    // ..or into mark...
    for (auto & m: marks_) {
        if (hol.spn_.xmin >= m.spn.xmin && hol.spn_.ymin >= m.spn.ymin && hol.spn_.xmax <= m.spn.xmax && hol.spn_.ymax <= m.spn.ymax) {
            hol.wp_->conf().push(Conf::BACKGROUND, m.c.html());
            hol.wp_->conf().push(Conf::WHITESPACE_BACKGROUND, m.c.html());
            break;
        }
    }
}

void Table_impl::wipe(Hiter hi) {
    bool hidden = hi->second.wp_->hidden(), chk_selection = false;

    // Restore background, if inserted into selection...
    if (has_selection() && hi->second.spn_.xmin >= sel_.xmin && hi->second.spn_.ymin >= sel_.ymin && hi->second.spn_.xmax <= sel_.xmax && hi->second.spn_.ymax <= sel_.ymax) {
        hi->second.wp_->signal_unselect()();
        hi->second.wp_->conf().unset(Conf::BACKGROUND);
        hi->second.wp_->conf().unset(Conf::WHITESPACE_BACKGROUND);
        chk_selection = true;
    }

    // ..or into mark...
    for (auto & m: marks_) {
        if (hi->second.spn_.xmin >= m.spn.xmin && hi->second.spn_.ymin >= m.spn.ymin && hi->second.spn_.xmax <= m.spn.xmax && hi->second.spn_.ymax <= m.spn.ymax) {
            hi->second.wp_->conf().unset(Conf::BACKGROUND);
            hi->second.wp_->conf().unset(Conf::WHITESPACE_BACKGROUND);
            break;
        }
    }

    for (int xx = hi->second.spn_.xmin; xx < hi->second.spn_.xmax; ++xx) {
        auto i = cols_.find(xx);
        if (!hidden) { hide(i, hi->second.xsh_); }
        unref(i, hi);
    }

    for (int yy = hi->second.spn_.ymin; yy < hi->second.spn_.ymax; ++yy) {
        auto i = rows_.find(yy);
        if (!hidden) { hide(i, hi->second.ysh_); }
        unref(i, hi);
    }

    update_child_bounds(hi->second.wp_.get(), INT_MIN, INT_MIN);

    if (chk_selection) {
        auto wps = widgets(sel_);

        if (1 == wps.size() && wps.front() == hi->second.wp_) {
            sel_ = Span();
            if (!in_shutdown()) { signal_selection_changed_(); }
        }
    }
}

void Table_impl::respan(Widget_impl * wp, const Span & spn, bool xsh, bool ysh) {
    auto i = holders_.find(wp);

    if (i != holders_.end()) {
        if (i->second.spn_ != spn || i->second.xsh_ != xsh || i->second.ysh_ != ysh) {
            wipe(i);
            i->second.spn_ = spn;
            i->second.xsh_ = xsh;
            i->second.ysh_ = ysh;
            dist(i);
            recalc(), queue_arrange();
        }
    }
}

void Table_impl::respan(Widget_impl * wp, const Span & spn) {
    if (spn.xmax > spn.xmin && spn.ymax > spn.ymin) {
        auto i = holders_.find(wp);
        if (i != holders_.end()) { respan(wp, spn, i->second.xsh_, i->second.ysh_); }
    }
}

Rect Table_impl::bounds(const Span & rng, bool with_margins) const noexcept {
    Rect bounds;
    int xmin = INT_MAX, ymin = INT_MAX, xmax = INT_MIN, ymax = INT_MIN;

    if (rng.xmax > rng.xmin) {
        auto i = std::find_if(cols_.begin(), cols_.end(), [&rng](auto & p) { return p.second.visible_ && p.first >= rng.xmin; } );

        if (i != cols_.end()) {
            xmin = i->second.x_-(with_margins ? i->second.left_ : 0);

            do {
                if (i->second.visible_) {
                    xmax = i->second.x_+i->second.w_+(with_margins ? i->second.right_ : 0);
                }
            } while (++i != cols_.end() && i->first < rng.xmax);
        }
    }

    if (rng.ymax > rng.ymin) {
        auto i = std::find_if(rows_.begin(), rows_.end(), [&rng](auto & p) { return p.second.visible_ && p.first >= rng.ymin; } );

        if (i != rows_.end()) {
            ymin = i->second.y_-(with_margins ? i->second.top_ : 0);

            do {
                if (i->second.visible_) {
                    ymax = i->second.y_+i->second.h_+(with_margins ? i->second.bottom_ : 0);
                }
            } while (++i != rows_.end() && i->first < rng.ymax);
        }
    }

    if (xmax > xmin && ymax > ymin) { bounds.set(xmin, ymin, xmax, ymax); }
    return bounds;
}

Rect Table_impl::column_bounds(int xx, bool with_margins) const noexcept {
    auto i = cols_.find(xx);
    return i != cols_.end() ? Rect(i->second.x_-(with_margins ? i->second.left_ : 0), 0, Size(i->second.w_+(with_margins ? i->second.left_+i->second.right_ : 0), size().height())) : Rect();
}

Rect Table_impl::row_bounds(int yy, bool with_margins) const noexcept {
    auto i = rows_.find(yy);
    return i != rows_.end() ? Rect(0, i->second.y_-(with_margins ? i->second.top_ : 0), Size(size().width(), i->second.h_+(with_margins ? i->second.top_+i->second.bottom_ : 0))) : Rect();
}

int Table_impl::column_at_x(int x) const noexcept {
    for (auto & p: cols_) {
        if (x >= p.second.x_ && x < p.second.x_+int(p.second.w_)) {
            return p.first;
        }
    }

    return INT_MIN;
}

int Table_impl::row_at_y(int y) const noexcept {
    for (auto & p: rows_) {
        if (y >= p.second.y_ && y < p.second.y_+int(p.second.h_)) {
            return p.first;
        }
    }

    return INT_MIN;
}

void Table_impl::set_column_width(int xx, unsigned usize) {
    auto i = cols_.find(xx);

    if (i != cols_.end()) {
        Col & col = i->second;

        if (col.usize_ != usize) {
            std::swap(col.usize_, usize);

            if (col.visible_) {
                unsigned sold = std::max(usize ? usize : std::max(col.rmin_, col.rmax_), col.umin_), snew = std::max(col.usize_, col.umin_), ud;
                if (col.umax_) { sold = std::min(sold, col.umax_), snew = std::min(snew, col.umax_); }
                int d = snew-sold;
                ud = abs(d);

                if (d > 0) {
                    if (col.shrank_) { shrank_xreq_ += ud; }
                    else { xreq_ += ud; }
                }

                else if (d < 0) {
                    if (col.shrank_) { shrank_xreq_ = shrank_xreq_ > ud ? shrank_xreq_-ud : 0; }
                    else  { xreq_ = xreq_ > ud ? xreq_-ud : 0; }
                }

                if (!usize && col.usize_) { ++user_cols_; }
                else if (!col.usize_ && usize && user_cols_) { --user_cols_; }
                update_requisition(); queue_arrange();
            }
        }
    }

    else {
        i = new_col(xx);
        i->second.usize_ = usize;
    }
}

void Table_impl::set_min_column_width(int xx, unsigned umin) {
    auto i = cols_.find(xx);

    if (i != cols_.end()) {
        Col & col = i->second;

        if (col.umin_ != umin) {
            std::swap(col.umin_, umin);

            if (col.visible_) {
                unsigned sold = std::max(umin, col.usize_), snew = std::max(col.umin_, col.usize_), ud;
                if (col.shrank_ && !col.usize_) { sold = std::max(sold, std::max(col.rmin_, col.rmax_)), snew = std::max(snew, std::max(col.rmin_, col.rmax_)); }
                if (col.umax_) { sold = std::min(sold, col.umax_), snew = std::min(snew, col.umax_); }
                int d = snew-sold; ud = abs(d);

                if (d > 0) {
                    if (col.shrank_) { shrank_xreq_ += ud; }
                    else { xreq_ += ud; }
                }

                else if (d < 0) {
                    if (col.shrank_) { shrank_xreq_ = shrank_xreq_ > ud ? shrank_xreq_-ud : 0; }
                    else  { xreq_ = xreq_ > ud ? xreq_-ud : 0; }
                }

                if (!umin || col.w_ < umin) { update_requisition(); queue_arrange(); }
            }
        }
    }

    else {
        i = new_col(xx);
        i->second.umin_ = umin;
    }
}

void Table_impl::set_max_column_width(int xx, unsigned umax) {
    auto i = cols_.find(xx);

    if (i != cols_.end()) {
        Col & col = i->second;

        if (col.umax_ != umax) {
            std::swap(umax, col.umax_);

            if (col.visible_) {
                unsigned sold = std::max(col.umin_, col.usize_), snew = sold, ud;
                if (col.shrank_ && !col.usize_) { sold = std::max(sold, std::max(col.rmin_, col.rmax_)), snew = std::max(snew, std::max(col.rmin_, col.rmax_)); }
                if (col.umax_) { snew = std::min(sold, col.umax_), sold = std::min(snew, umax); }
                int d = snew-sold; ud = abs(d);

                if (d > 0) {
                    if (col.shrank_) { shrank_xreq_ += ud; }
                    else { xreq_ += ud; }
                }

                else if (d < 0) {
                    if (col.shrank_) { shrank_xreq_ = shrank_xreq_ > ud ? shrank_xreq_-ud : 0; }
                    else  { xreq_ = xreq_ > ud ? xreq_-ud : 0; }
                }

                if (0 == umax || col.w_ > umax) { update_requisition(); queue_arrange(); }
            }
        }
    }

    else {
        i = new_col(xx);
        i->second.umax_ = umax;
    }
}

unsigned Table_impl::column_width(int xx) const noexcept {
    auto i = cols_.find(xx);
    return i != cols_.end() ? i->second.usize_ : 0;
}

unsigned Table_impl::min_column_width(int xx) const noexcept {
    auto i = cols_.find(xx);
    return i != cols_.end() ? i->second.umin_ : 0;
}

unsigned Table_impl::max_column_width(int xx) const noexcept {
    auto i = cols_.find(xx);
    return i != cols_.end() ? i->second.umax_ : 0;
}

void Table_impl::set_row_height(int yy, unsigned usize) {
    auto i = rows_.find(yy);

    if (i != rows_.end()) {
        Row & row = i->second;

        if (row.usize_ != usize) {
            std::swap(row.usize_, usize);

            if (row.visible_) {
                unsigned sold = std::max(usize ? usize : std::max(row.rmin_, row.rmax_), row.umin_), snew = std::max(row.usize_, row.umin_), ud;
                if (row.umax_) { sold = std::min(sold, row.umax_), snew = std::min(snew, row.umax_); }
                int d = snew-sold;
                ud = abs(d);

                if (d > 0) {
                    if (row.shrank_) { shrank_yreq_ += ud; }
                    else { yreq_ += ud; }
                }

                else if (d < 0) {
                    if (row.shrank_) { shrank_yreq_ = shrank_yreq_ > ud ? shrank_yreq_-ud : 0; }
                    else  { yreq_ = yreq_ > ud ? yreq_-ud : 0; }
                }

                if (!usize && row.usize_) { ++user_rows_; }
                else if (!row.usize_ && usize && user_rows_) { --user_rows_; }
                update_requisition(); queue_arrange();
            }
        }
    }

    else {
        i = new_row(yy);
        i->second.usize_ = usize;
    }
}

void Table_impl::set_min_row_height(int yy, unsigned umin) {
    auto i = rows_.find(yy);

    if (i != rows_.end()) {
        Row & row = i->second;

        if (row.umin_ != umin) {
            std::swap(row.umin_, umin);

            if (row.visible_) {
                unsigned sold = std::max(umin, row.usize_), snew = std::max(row.umin_, row.usize_), ud;
                if (row.shrank_ && !row.usize_) { sold = std::max(sold, std::max(row.rmin_, row.rmax_)), snew = std::max(snew, std::max(row.rmin_, row.rmax_)); }
                if (row.umax_) { sold = std::min(sold, row.umax_), snew = std::min(snew, row.umax_); }
                int d = snew-sold; ud = std::abs(d);

                if (d > 0) {
                    (row.shrank_ ? shrank_yreq_ : yreq_) += ud;
                }

                else if (d < 0) {
                    if (row.shrank_) { shrank_yreq_ = shrank_yreq_ > ud ? shrank_yreq_-ud : 0; }
                    else  { yreq_ = yreq_ > ud ? yreq_-ud : 0; }
                }

                if (!umin || row.h_ < umin) { update_requisition(); queue_arrange(); }
            }
        }
    }

    else {
        i = new_row(yy);
        i->second.umin_ = umin;
    }
}

void Table_impl::set_max_row_height(int yy, unsigned umax) {
    auto i = rows_.find(yy);

    if (i != rows_.end()) {
        Row & row = i->second;

        if (row.umax_ != umax) {
            std::swap(umax, row.umax_);

            if (row.visible_) {
                unsigned sold = std::max(row.umin_, row.usize_), snew = sold, ud;
                if (row.shrank_ && !row.usize_) { sold = std::max(sold, std::max(row.rmin_, row.rmax_)), snew = std::max(snew, std::max(row.rmin_, row.rmax_)); }
                if (row.umax_) { snew = std::min(sold, row.umax_), sold = std::min(snew, umax); }
                int d = snew-sold; ud = std::abs(d);

                if (d > 0) {
                    (row.shrank_ ? shrank_yreq_ : yreq_) += ud;
                }

                else if (d < 0) {
                    if (row.shrank_) { shrank_yreq_ = shrank_yreq_ > ud ? shrank_yreq_-ud : 0; }
                    else  { yreq_ = yreq_ > ud ? yreq_-ud : 0; }
                }

                if (0 == umax|| row.h_ > umax) { update_requisition(); queue_arrange(); }
            }
        }
    }

    else {
        i = new_row(yy);
        i->second.umax_ = umax;
    }
}

unsigned Table_impl::row_height(int yy) const noexcept {
    auto i = rows_.find(yy);
    return i != rows_.end() ? i->second.usize_ : 0;
}

unsigned Table_impl::min_row_height(int yy) const noexcept {
    auto i = rows_.find(yy);
    return i != rows_.end() ? i->second.umin_ : 0;
}

unsigned Table_impl::max_row_height(int yy) const noexcept {
    auto i = rows_.find(yy);
    return i != rows_.end() ? i->second.umax_ : 0;
}

void Table_impl::insert_columns(int xmin, unsigned n) {
    if (0 == n || cols_.empty()) { return; }
    if (xmin >= cols_.rbegin()->first) { return; }
    int xmax = xmin+n;

    // Process selection.
    if (sel_.xmax > sel_.xmin && sel_.ymax > sel_.ymin) {
        if (sel_.xmin >= xmax) { sel_.xmin += n, sel_.xmax += n; }
        else if (sel_.xmax > xmin) { unselect(); }
    }

    // Process marks.
    for (auto & m: marks_) {
        if (m.spn.xmin >= xmax) {
            m.spn.xmin += n, m.spn.xmax += n;
        }
    }

    // Trim or move children.
    for (auto i = holders_.begin(); i != holders_.end(); ++i) {
        // Trim child.
        if (i->second.spn_.xmin < xmin && i->second.spn_.xmax > xmin) {
            if (i->second.spn_.xmax > xmax) {
                for (int xx = xmax; xx < i->second.spn_.xmax; ++xx) {
                    auto j = cols_.find(xx);

                    if (j != cols_.end()) {
                        if (!i->second.wp_->hidden()) { hide(j, i->second.xsh_); }
                        unref(j, i);
                    }
                }
            }

            i->second.spn_.xmax = xmin;
        }

        // Move the child to the right.
        else if (i->second.spn_.xmin >= xmin) {
            i->second.spn_.xmin += n, i->second.spn_.xmax += n;
        }
    }

    // Copy columns right.
    for (int x = cols_.rbegin()->first; x >= xmin; --x) {
        auto i = cols_.find(x);

        if (i != cols_.end()) {
            Col col = i->second;
            new_col(x+n, col);
        }
    }

    for (int xx = xmin; xx < xmin+int(n); ++xx) { erase(cols_.find(xx)); }
    recalc(), queue_arrange();
}

void Table_impl::insert_rows(int ymin, unsigned n) {
    if (0 == n || rows_.empty()) { return; }
    if (ymin >= rows_.rbegin()->first) { return; }
    int ymax = ymin+n;

    // Process selection.
    if (has_selection()) {
        if (sel_.ymin >= ymax) { sel_.ymin += n, sel_.ymax += n; }
        else if (sel_.ymax > ymin) { unselect(); }
    }

    // Process marks.
    for (auto & m: marks_) {
        if (m.spn.ymin >= ymax) {
            m.spn.ymin += n, m.spn.ymax += n;
        }
    }

    // Trim or move children.
    for (auto i = holders_.begin(); i != holders_.end(); ++i) {
        // Trim the child.
        if (i->second.spn_.ymin < ymin && i->second.spn_.ymax > ymin) {
            if (i->second.spn_.ymax > ymax) {
                for (int yy = ymax; yy < i->second.spn_.ymax; ++yy) {
                    auto j = rows_.find(yy);

                    if (j != rows_.end()) {
                        if (!i->second.wp_->hidden()) { hide(j, i->second.ysh_); }
                        unref(j, i);
                    }
                }
            }

            i->second.spn_.ymax = ymin;
        }

        // Move child down.
        else if (i->second.spn_.ymin >= ymin) {
            i->second.spn_.ymin += n, i->second.spn_.ymax += n;
        }
    }

    // Copy rows down, moving downward up.
    for (int yy = rows_.rbegin()->first; yy >= ymin; --yy) {
        auto i = rows_.find(yy);

        if (i != rows_.end()) {
            Row row = i->second;
            new_row(yy+n, row);
        }
    }

    for (int yy = ymin; yy < ymin+int(n); ++yy) { erase(rows_.find(yy)); }
    recalc(), queue_arrange();
}

void Table_impl::remove_columns(int xmin, unsigned n_columns) {
    if (0 != n_columns && !cols_.empty()) {
        std::list<Widget_impl *> l;
        int last = cols_.rbegin()->first;

        if (xmin <= last) {
            unselect();
            int xmax = xmin+n_columns;

            // Trim and move children.
            for (auto i = holders_.begin(); i != holders_.end(); ++i) {
                auto & hol = i->second;

                // Trim the child.
                if (hol.spn_.xmin < xmin && hol.spn_.xmax > xmin) {
                    if (hol.spn_.xmax > xmax) {
                        for (int xx = xmax; xx < hol.spn_.xmax; ++xx) {
                            auto j = cols_.find(xx);

                            if (j != cols_.end()) {
                                if (!hol.wp_->hidden()) { hide(j, hol.xsh_); }
                                unref(j, i);
                            }
                        }
                    }

                    hol.spn_.xmax = xmin;
                }

                // Remove child.
                else if (hol.spn_.xmin >= xmin && hol.spn_.xmin < xmax) {
                    l.push_back(hol.wp_.get());
                    hol.removal_ = true;
                }

                // Move the child to the left.
                else if (hol.spn_.xmin >= xmax) {
                    hol.spn_.xmin -= n_columns;
                    hol.spn_.xmax -= n_columns;
                }
            }

            if (!cols_.empty()) {   // Test again after trimming and deletion.
                for (int xx = xmin; xx < xmax; ++xx) {
                    auto i = cols_.find(xx);
                    if (i != cols_.end()) { hide(i), erase(i); }
                }

                for (int xx = xmax; xx <= last; ++xx) {     // Move columns lefter.
                    auto i = cols_.find(xx);
                    if (i != cols_.end()) { cols_[xx-n_columns] = i->second; }
                }
            }

            invalidate_children(l), unparent(l);
            std::erase_if(holders_, [](auto & p) { return p.second.removal_; } );
            recalc(), queue_arrange();
        }
    }
}

void Table_impl::remove_rows(int ymin, unsigned n_rows) {
    if (0 != n_rows && !rows_.empty()) {
        std::list<Widget_impl *> l;
        int last = rows_.rbegin()->first;

        if (ymin <= last) {
            unselect();
            int ymax = ymin+n_rows;

            // Trim and move children.
            for (auto i = holders_.begin(); i != holders_.end(); ++i) {
                auto & hol = i->second;

                // Trim the child.
                if (hol.spn_.ymin < ymin && hol.spn_.ymax > ymin) {
                    if (hol.spn_.ymax > ymax) {
                        for (int yy = ymax; yy < hol.spn_.ymax; ++yy) {
                            auto j = rows_.find(yy);

                            if (j != rows_.end()) {
                                if (!hol.wp_->hidden()) { hide(j, hol.ysh_); }
                                unref(j, i);
                            }
                        }
                    }

                    hol.spn_.ymax = ymin;
                }

                // Remove child.
                else if (hol.spn_.ymin >= ymin && hol.spn_.ymin < ymax) {
                    l.push_back(hol.wp_.get());
                    hol.removal_ = true;
                }

                // Move the child above.
                else if (hol.spn_.ymin >= ymax) {
                    hol.spn_.ymin -= n_rows;
                    hol.spn_.ymax -= n_rows;
                }
            }

            // Test again after trimming and deletion.
            if (!rows_.empty()) {
                for (int yy = ymin; yy < ymax; ++yy) {
                    auto i = rows_.find(yy);
                    if (i != rows_.end()) { hide(i), erase(i); }
                }

                for (int yy = ymax; yy <= last; ++yy) {     // Move rows above.
                    auto i = rows_.find(yy);
                    if (i != rows_.end()) { rows_[yy-n_rows] = i->second; }
                }
            }

            invalidate_children(l), unparent(l);
            std::erase_if(holders_, [](auto & p) { return p.second.removal_; } );
            recalc(), queue_arrange();
        }
    }
}

void Table_impl::select(const Span & spn) {
    if (!empty() && spn != sel_ && spn.xmax > spn.xmin && spn.ymax > spn.ymin) {
        unselect();
        sel_ = spn;
        auto w = widgets(sel_);
        for (auto wp: w) { wp->signal_select()(); }
        if (!in_shutdown()) { signal_selection_changed_(); }
        for (auto wp: w) { wp->conf().set_from(Conf::BACKGROUND, Conf::SELECT_BACKGROUND, true), wp->conf().set_from(Conf::WHITESPACE_BACKGROUND, Conf::SELECT_BACKGROUND, true); }
        invalidate(bounds(sel_, true));
    }
}

// Overridden by List_impl.
void Table_impl::unselect() {
    if (sel_.xmax > sel_.xmin && sel_.ymax > sel_.ymin) {
        invalidate(bounds(sel_, true));

        for (auto wp: widgets(sel_)) {
            wp->conf().unset(Conf::WHITESPACE_BACKGROUND);
            wp->conf().unset(Conf::BACKGROUND);
            wp->signal_unselect()();
        }

        sel_ = Span();
        if (!in_shutdown()) { signal_selection_changed_(); }
    }
}

// Overrides Widget_impl.
bool Table_impl::on_backpaint(Painter pr, const Rect & inval) {
    Widget_impl::on_backpaint(pr, inval);

    for (auto & m: marks_) {
        if (auto r = bounds(m.spn, m.mg)) {
            pr.rectangle(r.left(), r.top(), r.right(), r.bottom());
            pr.set_brush(m.c);
            pr.fill();
        }
    }

    if (auto r = bounds(sel_, true)) {
        pr.rectangle(r.left(), r.top(), r.right(), r.bottom());
        pr.set_brush(conf().brush(Conf::SELECT_BACKGROUND));
        pr.fill();
    }

    return false;
}

Span Table_impl::make_span(int x, int y, unsigned sx, unsigned sy) const noexcept {
    return { x, y, UINT_MAX != sx ? x+int(std::max(1U, sx)) : INT_MAX, UINT_MAX != sy ? y+int(std::max(1U, sy)) : INT_MAX };
}

void Table_impl::on_display_in() {
    recalc();
    arrange();
}

void Table_impl::on_display_once() {
    once_cx_.drop();
    update_marks();
    auto w = widgets(sel_);
    Brush br(conf().brush(Conf::SELECT_BACKGROUND));
    for (auto wp: w) { wp->conf().brush(Conf::BACKGROUND) = br, wp->conf().brush(Conf::WHITESPACE_BACKGROUND) = br; }
    invalidate(bounds(sel_, true));
}

void Table_impl::update_marks() {
    bool changed = false;

    for (auto & m: marks_) {
        auto hols = holders(m.spn);

        for (auto * hol: hols) {
            if (!hol->marked_) {
                hol->marked_ = true, changed = true;
                hol->wp_->conf().brush(Conf::BACKGROUND) = Brush(m.c);
            }
        }
    }

    if (changed) {
        invalidate();
        signal_selection_changed_();
    }
}

void Table_impl::mark_back(const Color & c, const Span & spn, bool with_margins) {
    if (spn.xmax > spn.xmin && spn.ymax > spn.ymin) {
        auto i = std::find_if(marks_.begin(), marks_.end(), [&spn](auto & m) { return spn == m.spn; });

        // Already marked -> change color.
        if (i != marks_.end()) {
            i->mg = with_margins; i->c = c;
            auto hols = holders(spn);

            if (!hols.empty()) {
                for (auto hol: hols) { hol->wp_->conf().brush(Conf::BACKGROUND) = Brush(c); }
                invalidate(bounds(spn, with_margins));
            }
        }

        // Add new mark.
        else {
            marks_.emplace_front(c, spn, with_margins);

            if (once_cx_.empty()) {
                auto hols = holders(spn);

                if (!hols.empty()) {
                    for (auto * hol: hols) { hol->marked_ = true; hol->wp_->conf().push(Conf::BACKGROUND, c.html()); }
                    invalidate(bounds(spn, with_margins));
                }
            }
        }
    }
}

void Table_impl::unmark(const Span & spn) {
    auto i = std::find_if(marks_.begin(), marks_.end(), [&spn](auto & m) { return spn == m.spn; });

    if (i != marks_.end()) {
        for (auto hol: holders(spn)) { hol->marked_ = false; hol->wp_->conf().unset(Conf::BACKGROUND); }
        invalidate(bounds(spn, true));
        marks_.remove_if([spn](auto & m) { return spn == m.spn; });
    }
}

void Table_impl::unmark() {
    if (!marks_.empty()) {
        std::list<Holder *> v; Rect r;
        for (auto & m: marks_) { v.merge(holders(m.spn)); r |= bounds(m.spn, true); }
        if (r) { invalidate(r); }
        for (auto hol: v) { hol->marked_ = false; hol->wp_->conf().unset(Conf::BACKGROUND); }
        marks_.clear();
    }
}

std::vector<Span> Table_impl::marks() const noexcept {
    std::vector<Span> v;
    for (auto & m: marks_) { v.push_back(m.spn); }
    return v;
}

Span Table_impl::column_span(int xx) const noexcept {
    auto j = cols_.find(xx);

    if (cols_.end() != j) {
        if (INT_MAX == j->second.ymin_ || INT_MIN == j->second.ymax_) {
            for (auto & p: holders_) {
                auto & hol = p.second;

                if (hol.spn_.xmin <= xx && hol.spn_.xmax > xx) {
                    j->second.ymin_ = std::min(j->second.ymin_, hol.spn_.ymin);
                    j->second.ymax_ = std::max(j->second.ymax_, hol.spn_.ymax);
                }
            }
        }

        return { xx, j->second.ymin_, 1+xx, j->second.ymax_ };
    }

    return { xx, INT_MAX, 1+xx, INT_MIN };
}

Span Table_impl::row_span(int yy) const noexcept {
    auto j = rows_.find(yy);

    if (rows_.end() != j) {
        if (INT_MAX == j->second.xmin_ || INT_MIN == j->second.xmax_) {
            for (auto & p: holders_) {
                auto & hol = p.second;

                if (hol.spn_.ymin <= yy && hol.spn_.ymax > yy) {
                    j->second.xmin_ = std::min(j->second.xmin_, hol.spn_.xmin);
                    j->second.xmax_ = std::max(j->second.xmax_, hol.spn_.xmax);
                }
            }
        }

        return { j->second.xmin_, yy, j->second.xmax_, 1+yy };
    }

    return { INT_MAX, yy, INT_MIN, 1+yy };
}

void Table_impl::align(Widget_impl * wp, Align xalign, Align yalign) {
    bool changed = false;
    auto i = holders_.find(wp);

    if (i != holders_.end()) {
        auto & hol = i->second;
        changed = !hol.has_align_;

        if (hol.xalign_ != xalign) {
            hol.xalign_ = xalign;
            changed = true;
        }

        if (hol.yalign_ != yalign) {
            hol.yalign_ = yalign;
            changed = true;
        }

        hol.has_align_ = true;
    }

    if (changed) {
        queue_arrange();
    }
}

std::pair<Align, Align> Table_impl::align(const Widget_impl * wp) const {
    auto i = holders_.find(const_cast<Widget_impl *>(wp));
    return i != holders_.cend() ? std::make_pair(i->second.xalign_, i->second.yalign_) : std::make_pair(xalign_, yalign_);
}

void Table_impl::unalign(Widget_impl * wp) {
    auto i = holders_.find(wp);

    if (i != holders_.end() && i->second.has_align_) {
        i->second.has_align_ = false;
        i->second.xalign_ = i->second.xalign_ = Align::CENTER;
        queue_arrange();
    }
}

void Table_impl::align(Align xalign, Align yalign) {
    if (xalign_ != xalign || yalign_ != yalign) {
        std::swap(xalign_, xalign);
        std::swap(yalign_, yalign);
        if (xalign_ != Align::FILL && xalign != Align::FILL && yalign_ != Align::FILL && yalign != Align::FILL) { place(); }
        else { recalc(); queue_arrange(); }
    }
}

void Table_impl::align_columns(Align xalign) {
    if (xalign_ != xalign) {
        std::swap(xalign_, xalign);
        if (xalign_ != Align::FILL && xalign != Align::FILL) { place(); }
        else { recalc(); queue_arrange(); }
    }
}

void Table_impl::align_rows(Align yalign) {
    if (yalign_ != yalign) {
        std::swap(yalign_, yalign);
        if (yalign_ != Align::FILL && yalign != Align::FILL) { place(); }
        else { recalc(); queue_arrange(); }
    }
}

void Table_impl::align_column(int xx, Align xalign) {
    bool changed = false, arrange = false;
    auto i = cols_.find(xx);

    if (i != cols_.end()) {
        if (!i->second.xalign_ || *i->second.xalign_ != xalign) {
            changed = true;
            arrange = i->second.xalign_ || *i->second.xalign_ == Align::FILL;
            i->second.xalign_ = xalign;
        }

        if (changed) {
            if (arrange || xalign == Align::FILL) { recalc(); queue_arrange(); }
            else { place(); }
        }
    }

    else {
        i = new_col(xx);
        i->second.xalign_ = xalign;
    }
}

Align Table_impl::column_align(int xx) const noexcept {
    auto i = cols_.find(xx);
    return i != cols_.end() && i->second.xalign_ ? *i->second.xalign_ : xalign_;
}

void Table_impl::unalign_column(int xx) {
    auto i = cols_.find(xx);

    if (i != cols_.end()) {
        if (i->second.xalign_) {
            Align was = *i->second.xalign_;
            i->second.xalign_.reset();

            if (i->second.visible_ && (was != xalign_)) {
                if (xalign_ != Align::FILL && was != Align::FILL) { place(); }
                else { recalc(); queue_arrange(); }
            }
        }
    }
}

void Table_impl::align_row(int yy, Align yalign) {
    bool changed = false, arrange = false;
    auto i = rows_.find(yy);

    if (i != rows_.end()) {
        if (!i->second.yalign_ || i->second.yalign_ != yalign) {
            changed = true;
            arrange = i->second.yalign_ && *i->second.yalign_ != Align::FILL;
            i->second.yalign_ = yalign;
        }

        if (changed) {
            if (arrange || yalign == Align::FILL) { recalc(); queue_arrange(); }
            else { place(); }
        }
    }

    else {
        i = new_row(yy);
        i->second.yalign_ = yalign;
    }
}

Align Table_impl::row_align(int yy) const noexcept {
    auto i = rows_.find(yy);
    return i != rows_.end() && i->second.yalign_ ? *i->second.yalign_ : yalign_;
}

void Table_impl::unalign_row(int yy) {
    auto i = rows_.find(yy);

    if (i != rows_.end()) {
        if (i->second.yalign_) {
            Align was = *i->second.yalign_;
            i->second.yalign_.reset();

            if (i->second.visible_ && (was != yalign_)) {
                if (yalign_ != Align::FILL && was != Align::FILL) { place(); }
                else { recalc(); queue_arrange(); }
            }
        }
    }
}

bool Table_impl::set_column_spacing_i(unsigned xspacing) {
    int d = xspacing-xspacing_;

    if (0 != d) {
        if (vis_cols_ > 1) {
            unsigned ud = abs(d)*(vis_cols_-1);
            if (d > 0) { xspc_ += ud; }
            else { xspc_ = xspc_ > ud ? xspc_-ud : 0; }
        }

        xspacing_ = xspacing;
        return true;
    }

    return false;
}

void Table_impl::set_column_spacing(unsigned xspacing) {
    if (set_column_spacing_i(xspacing)) { update_requisition(); queue_arrange(); }
}

bool Table_impl::set_row_spacing_i(unsigned yspacing) {
    int d = yspacing-yspacing_;

    if (0 != d) {
        if (vis_rows_ > 1) {
            unsigned ud = abs(d)*(vis_rows_-1);
            if (d > 0) { yspc_ += ud; }
            else { yspc_ = yspc_ > ud ? yspc_-ud : 0; }
        }

        yspacing_ = yspacing;
        return true;
    }

    return false;
}

void Table_impl::set_row_spacing(unsigned yspacing) {
    if (set_row_spacing_i(yspacing)) { update_requisition(); queue_arrange(); }
}

void Table_impl::set_spacing(unsigned xspacing, unsigned yspacing) {
    bool changed = set_column_spacing_i(xspacing);
    if (set_row_spacing_i(yspacing)) { changed = true; }
    if (changed) { update_requisition(); queue_arrange(); }
}

void Table_impl::set_spacing(unsigned spacing) {
    set_spacing(spacing, spacing);
}

void Table_impl::set_column_margin(int xx, const Margin & mg) {
    auto i = cols_.find(xx);

    if (i != cols_.end()) {
        int dl = mg.left-i->second.left_, dr = mg.right-i->second.right_, d = dl+dr;
        i->second.left_ = mg.left;
        i->second.right_ = mg.right;

        if (i->second.visible_ && (0 != dl || 0 != dr)) {
            unsigned ud = abs(d);
            if (d > 0) { xspc_ += ud; }
            else { xspc_ = xspc_ > ud ? xspc_-ud : 0; }
            update_requisition(); queue_arrange();
        }
    }

    else {
        if (0 != mg.left || 0 != mg.right) {
            i = new_col(xx);
            i->second.left_ = mg.left;
            i->second.right_ = mg.right;
        }
    }
}

void Table_impl::set_row_margin(int yy, const Margin & mg) {
    auto i = rows_.find(yy);

    if (i != rows_.end()) {
        int dt = mg.top-i->second.top_, db = mg.bottom-i->second.bottom_, d = dt+db;
        i->second.top_ = mg.top;
        i->second.bottom_ = mg.bottom;

        if (i->second.visible_ && (0 != dt || 0 != db)) {
            unsigned ud = abs(d);
            if (d > 0) { yspc_ += ud; }
            else { yspc_ = yspc_ > ud ? yspc_-ud : 0; }
            update_requisition(); queue_arrange();
        }
    }

    else {
        if (0 != mg.top || 0 != mg.bottom) {
            i = new_row(yy);
            i->second.top_ = mg.top;
            i->second.bottom_ = mg.bottom;
        }
    }
}

void Table_impl::set_columns_margin(const Margin & margin) {
    auto mg = margin_;
    mg.left = margin.left;
    mg.right = margin.right;

    if (margin_ != mg) {
        margin_ = mg;
        bool changed = false;

        for (auto & p: cols_) {
            int dl = mg.left-p.second.left_, dr = mg.right-p.second.right_, d = dl+dr;
            p.second.left_ = mg.left;
            p.second.right_ = mg.right;

            if (0 != d && p.second.visible_) {
                changed = true;
                unsigned ud = abs(d);
                if (d > 0) { xspc_ += ud; }
                else { xspc_ = xspc_ > ud ? xspc_-ud : 0; }
            }
        }

        if (changed) { update_requisition(); queue_arrange(); }
    }
}

void Table_impl::set_rows_margin(const Margin & margin) {
    auto mg = margin_;
    mg.left = margin.top;
    mg.right = margin.bottom;

    if (margin_ != mg) {
        margin_ = mg;
        bool changed = false;

        for (auto & p: rows_) {
            int dt = mg.top-p.second.top_, db = mg.bottom-p.second.bottom_, d = dt+db;
            p.second.top_ = mg.top;
            p.second.bottom_ = mg.bottom;

            if (0 != d && p.second.visible_) {
                changed = true;
                unsigned ud = abs(d);
                if (d > 0) { yspc_ += ud; }
                else { yspc_ = yspc_ > ud ? yspc_-ud : 0; }
            }
        }

        if (changed) { update_requisition(); queue_arrange(); }
    }
}

Margin Table_impl::column_margin(int xx) const noexcept {
    Margin mg;
    auto i = cols_.find(xx);
    if (i != cols_.end()) { mg.left = i->second.left_, mg.right = i->second.right_; }
    return mg;
}

Margin Table_impl::row_margin(int yy) const noexcept {
    Margin mg;
    auto i = rows_.find(yy);
    if (i != rows_.end()) { mg.top = i->second.top_, mg.bottom = i->second.bottom_; }
    return mg;
}

bool Table_impl::on_take_focus() {
    if (focused_child_ && focused_child_->take_focus()) { return true; }
    if (std::any_of(holders_.begin(), holders_.end(), [](auto & p) { return p.second.wp_->take_focus(); })) { return true; }
    return grab_focus();
}

void Table_impl::on_size_changed() {
    update_requisition();
    for (auto & p: holders_) { p.second.place_ = true; }
    arrange();
}

void Table_impl::on_select_background() {
    for (auto wp: widgets(sel_)) { wp->conf().set_from(Conf::BACKGROUND, Conf::SELECT_BACKGROUND); }
    if (has_selection()) { invalidate(); }
}

signal<void(int)> & Table_impl::signal_column_bounds_changed() {
    if (!signal_column_bounds_changed_) { signal_column_bounds_changed_ = new signal<void(int)>; }
    return *signal_column_bounds_changed_;
}

signal<void(int)> & Table_impl::signal_row_bounds_changed() {
    if (!signal_row_bounds_changed_) { signal_row_bounds_changed_ = new signal<void(int)>; }
    return *signal_row_bounds_changed_;
}

} // namespace tau

//END

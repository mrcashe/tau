// ----------------------------------------------------------------------------
// SPDX-License-Identifier: BSD-2-Clause
// Copyright © 2014-2024 Konstantin Shmelkov <mrcashe@gmail.com>.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
// THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
// PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
// CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
// EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
// PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
// OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
// WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
// OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
// EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// ----------------------------------------------------------------------------

/// @file table.cc The Table class interface.
/// The header is table.hh.

#include <tau/table.hh>
#include <table-impl.hh>

namespace tau {

#define TABLE_IMPL (std::static_pointer_cast<Table_impl>(impl))

Table::Table():
    Container(std::make_shared<Table_impl>())
{
}

Table::Table(unsigned xspacing, unsigned yspacing):
    Container(std::make_shared<Table_impl>(xspacing, yspacing))
{
}

Table::Table(unsigned spacing):
    Container(std::make_shared<Table_impl>(spacing))
{
}

Table::Table(Align xalign, Align yalign):
    Container(std::make_shared<Table_impl>(xalign, yalign))
{
}

Table::Table(Align xalign, Align yalign, unsigned spacing):
    Container(std::make_shared<Table_impl>(xalign, yalign, spacing))
{
}

Table::Table(Align xalign, Align yalign, unsigned xspacing, unsigned yspacing):
    Container(std::make_shared<Table_impl>(xalign, yalign, xspacing, yspacing))
{
}

Table::Table(const Table & other):
    Container(other.impl)
{
}

Table & Table::operator=(const Table & other) {
    Container::operator=(other);
    return *this;
}

Table::Table(Table && other):
    Container(other.impl)
{
}

Table & Table::operator=(Table && other) {
    Container::operator=(other);
    return *this;
}

Table::Table(Widget_ptr wp):
    Container(std::dynamic_pointer_cast<Table_impl>(wp))
{
}

Table & Table::operator=(Widget_ptr wp) {
    Container::operator=(std::dynamic_pointer_cast<Table_impl>(wp));
    return *this;
}

void Table::put(Widget & w, int x, int y, unsigned xspan, unsigned yspan, bool xsh, bool ysh) {
    TABLE_IMPL->put(w.ptr(), x, y, xspan, yspan, xsh, ysh);
}

void Table::put(Widget & w, const Span & span, bool xsh, bool ysh) {
    TABLE_IMPL->put(w.ptr(), span, xsh, ysh);
}

void Table::put(Widget & w, Align xalign, Align yalign, int x, int y, unsigned xspan, unsigned yspan, bool xsh, bool ysh) {
    TABLE_IMPL->put(w.ptr(), xalign, yalign, x, y, xspan, yspan, xsh, ysh);
}

void Table::put(Widget & w, Align xalign, Align yalign, const Span & span, bool xsh, bool ysh) {
    TABLE_IMPL->put(w.ptr(), xalign, yalign, span, xsh, ysh);
}

Span Table::span() const noexcept {
    return TABLE_IMPL->span();
}

Span Table::span(const Widget & w) const noexcept {
    return TABLE_IMPL->span(w.ptr().get());
}

Span Table::column_span(int row) const noexcept {
    return TABLE_IMPL->column_span(row);
}

Span Table::row_span(int col) const noexcept {
    return TABLE_IMPL->row_span(col);
}

void Table::set_column_spacing(unsigned xspacing) {
    TABLE_IMPL->set_column_spacing(xspacing);
}

void Table::set_row_spacing(unsigned yspacing) {
    TABLE_IMPL->set_row_spacing(yspacing);
}

void Table::set_spacing(unsigned xspacing, unsigned yspacing) {
    TABLE_IMPL->set_spacing(xspacing, yspacing);
}

void Table::set_spacing(unsigned spacing) {
    TABLE_IMPL->set_spacing(spacing);
}

std::pair<unsigned, unsigned> Table::spacing() const noexcept {
    return TABLE_IMPL->spacing();
}

std::size_t Table::remove(Widget & w) {
    return TABLE_IMPL->remove(w.ptr().get());
}

std::size_t Table::remove(int xmin, int ymin, unsigned xspan, unsigned yspan) {
    return TABLE_IMPL->remove(xmin, ymin, xspan, yspan);
}

std::size_t Table::remove(const Span & range) {
    return TABLE_IMPL->remove(range);
}

void Table::respan(Widget & w, int x, int y, unsigned xspan, unsigned yspan) {
    TABLE_IMPL->respan(w.ptr().get(), x, y, xspan, yspan);
}

void Table::respan(Widget & w, int x, int y, unsigned xspan, unsigned yspan, bool xsh, bool ysh) {
    TABLE_IMPL->respan(w.ptr().get(), x, y, xspan, yspan, xsh, ysh);
}

void Table::respan(Widget & w, const Span & span) {
    TABLE_IMPL->respan(w.ptr().get(), span);
}

void Table::respan(Widget & w, const Span & span, bool xsh, bool ysh) {
    TABLE_IMPL->respan(w.ptr().get(), span, xsh, ysh);
}

void Table::insert_columns(int x, unsigned n_columns) {
    TABLE_IMPL->insert_columns(x, n_columns);
}

void Table::insert_rows(int y, unsigned n_rows) {
    TABLE_IMPL->insert_rows(y, n_rows);
}

void Table::remove_columns(int x, unsigned n_columns) {
    TABLE_IMPL->remove_columns(x, n_columns);
}

void Table::remove_rows(int y, unsigned n_rows) {
    TABLE_IMPL->remove_rows(y, n_rows);
}

void Table::align(Align xalign, Align yalign) {
    TABLE_IMPL->align(xalign, yalign);
}

void Table::align_columns(Align xalign) {
    TABLE_IMPL->align_columns(xalign);
}

void Table::align_rows(Align yalign) {
    TABLE_IMPL->align_columns(yalign);
}

std::pair<Align, Align> Table::align() const {
    return TABLE_IMPL->align();
}

void Table::align(Widget & w, Align xalign, Align yalign) {
    TABLE_IMPL->align(w.ptr().get(), xalign, yalign);
}

void Table::unalign(Widget & w) noexcept {
    TABLE_IMPL->unalign(w.ptr().get());
}

void Table::align_column(int x, Align column_align) {
    TABLE_IMPL->align_column(x, column_align);
}

Align Table::column_align(int x) const noexcept {
    return TABLE_IMPL->column_align(x);
}

void Table::unalign_column(int x) {
    TABLE_IMPL->unalign_column(x);
}

void Table::align_row(int y, Align column_align) {
    TABLE_IMPL->align_row(y, column_align);
}

Align Table::row_align(int y) const noexcept {
    return TABLE_IMPL->row_align(y);
}

void Table::unalign_row(int y) {
    TABLE_IMPL->unalign_row(y);
}

std::pair<Align, Align> Table::align(const Widget & w) const {
    return TABLE_IMPL->align(w.ptr().get());
}

bool Table::empty() const noexcept {
    return TABLE_IMPL->empty();
}

void Table::clear() {
    TABLE_IMPL->clear();
}

void Table::select(const Span & spn) {
    TABLE_IMPL->select(spn);
}

void Table::select(int x, int y, unsigned xspan, unsigned yspan) {
    TABLE_IMPL->select(x, y, xspan, yspan);
}

void Table::unselect() {
    TABLE_IMPL->unselect();
}

bool Table::has_selection() const noexcept {
    return TABLE_IMPL->has_selection();
}

Span Table::selection() const noexcept {
    return TABLE_IMPL->selection();
}

void Table::mark_back(const Color & c, const Span & spn, bool with_margins) {
    TABLE_IMPL->mark_back(c, spn, with_margins);
}

void Table::mark_back(const Color & c, int x, int y, unsigned width, unsigned height, bool with_margins) {
    TABLE_IMPL->mark_back(c, x, y, width, height, with_margins);
}

void Table::unmark(const Span & spn) {
    TABLE_IMPL->unmark(spn);
}

void Table::unmark(int x, int y, unsigned xspan, unsigned yspan) {
    TABLE_IMPL->unmark(x, y, xspan, yspan);
}

void Table::unmark() {
    TABLE_IMPL->unmark();
}

bool Table::has_marks() const noexcept {
    return TABLE_IMPL->has_marks();
}

std::vector<Span> Table::marks() const {
    return TABLE_IMPL->marks();
}

void Table::set_column_margin(int x, unsigned left, unsigned right) {
    TABLE_IMPL->set_column_margin(x, left, right);
}

void Table::set_column_margin(int x, const Margin & margin) {
    TABLE_IMPL->set_column_margin(x, margin);
}

void Table::set_row_margin(int y, unsigned top, unsigned bottom) {
    TABLE_IMPL->set_row_margin(y, top, bottom);
}

void Table::set_row_margin(int y, const Margin & margin) {
    TABLE_IMPL->set_row_margin(y, margin);
}

Margin Table::column_margin(int x) const noexcept{
    return TABLE_IMPL->column_margin(x);
}

Margin Table::row_margin(int y) const noexcept {
    return TABLE_IMPL->row_margin(y);
}

void Table::set_columns_margin(unsigned left, unsigned right) {
    TABLE_IMPL->set_columns_margin(left, right);
}

void Table::set_columns_margin(const Margin & margin) {
    TABLE_IMPL->set_columns_margin(margin);
}

void Table::set_rows_margin(unsigned top, unsigned bottom) {
    TABLE_IMPL->set_rows_margin(top, bottom);
}

void Table::set_rows_margin(const Margin & margin) {
    TABLE_IMPL->set_rows_margin(margin);
}

Margin Table::columns_margin() const noexcept {
    return TABLE_IMPL->columns_margin();
}

Margin Table::rows_margin() const noexcept {
    return TABLE_IMPL->rows_margin();
}

Rect Table::bounds(int x, int y, unsigned xspan, unsigned yspan, bool with_margins) const noexcept {
    return TABLE_IMPL->bounds(x, y, xspan, yspan, with_margins);
}

Rect Table::bounds(const Span & span, bool with_margins) const noexcept {
    return TABLE_IMPL->bounds(span, with_margins);
}

Rect Table::column_bounds(int col, bool with_margins) const noexcept {
    return TABLE_IMPL->column_bounds(col, with_margins);
}

Rect Table::row_bounds(int row, bool with_margins) const noexcept {
    return TABLE_IMPL->row_bounds(row, with_margins);
}

void Table::set_column_width(int x, unsigned width) {
    TABLE_IMPL->set_column_width(x, width);
}

unsigned Table::column_width(int x) const noexcept {
    return TABLE_IMPL->column_width(x);
}

void Table::set_row_height(int y, unsigned height) {
    TABLE_IMPL->set_row_height(y, height);
}

unsigned Table::row_height(int y) const noexcept {
    return TABLE_IMPL->row_height(y);
}

void Table::set_min_column_width(int x, unsigned width) {
    TABLE_IMPL->set_min_column_width(x, width);
}

unsigned Table::min_column_width(int x) const noexcept {
    return TABLE_IMPL->min_column_width(x);
}

void Table::set_min_row_height(int y, unsigned height) {
    TABLE_IMPL->set_min_row_height(y, height);
}

unsigned Table::min_row_height(int y) const noexcept {
    return TABLE_IMPL->min_row_height(y);
}

void Table::set_max_column_width(int column, unsigned width) {
    TABLE_IMPL->set_max_column_width(column, width);
}

unsigned Table::max_column_width(int column) const noexcept {
    return TABLE_IMPL->max_column_width(column);
}

void Table::set_max_row_height(int row, unsigned height) {
    TABLE_IMPL->set_max_row_height(row, height);
}

unsigned Table::max_row_height(int row) const noexcept {
    return TABLE_IMPL->max_row_height(row);
}

std::list<Widget_ptr> Table::widgets(int x, int y, std::size_t xspan, std::size_t yspan) const {
    return TABLE_IMPL->widgets(x, y, xspan, yspan);
}

std::list<Widget_ptr> Table::widgets(const Span & rng) const {
    return TABLE_IMPL->widgets(rng);
}

signal<void(int)> & Table::signal_column_bounds_changed() {
    return TABLE_IMPL->signal_column_bounds_changed();
}

signal<void(int)> & Table::signal_row_bounds_changed() {
    return TABLE_IMPL->signal_row_bounds_changed();
}

signal<void()> & Table::signal_selection_changed() {
    return TABLE_IMPL->signal_selection_changed();
}

} // namespace tau

//END

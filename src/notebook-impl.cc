// ----------------------------------------------------------------------------
// SPDX-License-Identifier: BSD-2-Clause
// Copyright © 2014-2024 Konstantin Shmelkov <mrcashe@gmail.com>.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
// THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
// PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
// CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
// EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
// PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
// OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
// WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
// OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
// EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// ----------------------------------------------------------------------------

#include <tau/brush.hh>
#include <tau/string.hh>
#include <absolute-impl.hh>
#include <loop-impl.hh>
#include <notebook-impl.hh>
#include <roller-impl.hh>
#include <text-impl.hh>
#include <algorithm>
#include <iostream>

namespace {

// Convert Side to Orientation.
tau::Orientation or_from_tab_pos(tau::Side tab_pos) {
    switch (tab_pos) {
        case tau::Side::LEFT: return tau::Orientation::RIGHT;
        case tau::Side::RIGHT: return tau::Orientation::LEFT;
        case tau::Side::BOTTOM: return tau::Orientation::UP;
        default: return tau::Orientation::DOWN;
    }
}

// Convert Side to orthogonal Orientation.
tau::Orientation orth_or_from_tab_pos(tau::Side tab_pos) {
    switch (tab_pos) {
        case tau::Side::LEFT: return tau::Orientation::DOWN;
        case tau::Side::RIGHT: return tau::Orientation::DOWN;
        case tau::Side::BOTTOM: return tau::Orientation::RIGHT;
        default: return tau::Orientation::RIGHT;
    }
}

} // anonymous namespace

namespace tau {

Notebook_impl::Notebook_impl(Side tab_pos):
    Box_impl(or_from_tab_pos(tab_pos))
{
    auto roller = std::make_shared<Roller_impl>(horizontal() ? Orientation::DOWN : Orientation::RIGHT);
    append(roller, true);
    roller_ = roller.get();

    auto tabbox = std::make_shared<Box_impl>(orth_or_from_tab_pos(tab_pos));
    roller_->insert(tabbox);
    tabbox_ = tabbox.get();

    auto abs = std::make_shared<Absolute_impl>();
    tabbox_->append(abs, true);
    abs_ = abs.get();
    abs_->signal_size_changed().connect(fun(this, &Notebook_impl::on_abs_size));

    auto frame = std::make_shared<Frame_impl>(Border::SOLID, sel_color());
    append(frame);
    frame_ = frame.get();

    auto mainbox = std::make_shared<Box_impl>(orth_or_from_tab_pos(tab_pos));
    frame_->insert(mainbox);
    mainbox_ = mainbox.get();

    auto card = std::make_shared<Card_impl>();
    mainbox_->append(card);
    card_ = card.get();

    signal_size_changed_.connect(bind_back(fun(this, &Notebook_impl::update_tabs), Hints::SIZE));
    signal_take_focus_.connect(fun(card_, &Card_impl::take_focus), true);
    signal_display_in_.connect(fun(this, &Notebook_impl::update_current));
    frame_->conf().signal_changed(Conf::BUTTON_BACKGROUND).connect(fun(this, &Notebook_impl::on_frame_background_changed));
    roller_->signal_mouse_wheel().connect(fun(this, &Notebook_impl::on_mouse_wheel), true);
}

Color Notebook_impl::sel_color() {
    Color c = conf().brush(Conf::BUTTON_BACKGROUND).value().color();
    c.darker(0.18);
    return c;
}

void Notebook_impl::on_frame_background_changed() {
    frame_->set_border_color(sel_color());
}

int Notebook_impl::remove_page(Widget_cptr cwp) {
    return remove_page(page_number(cwp));
}

int Notebook_impl::page_number(Widget_cptr cwp) const {
    std::size_t current = 0;

    for (auto & p: pages_) {
        if (cwp == p.wp_ || cwp == p.title_ || cwp->has_parent(p.wp_.get()) || cwp->has_parent(p.title_.get())) { return current; }
        ++current;
    }

    return -1;
}

int Notebook_impl::remove_page(int page) {
    if (page >= 0) {
        std::size_t n = page;
        int current = current_page();

        if (n < pages_.size()) {
            auto & p = pages_[n];
            if (page == current) { p.unselect(); }
            auto wp = p.wp_;
            abs_->remove(p.frame_.get());
            card_->remove(p.wp_.get());
            p.size1_cx_.drop();
            p.size2_cx_.drop();
            p.requisition_cx_.drop();
            p.hints_cx_.drop();
            p.mouse_down_cx_.drop();
            p.mouse_up_cx_.drop();
            p.mouse_motion_cx_.drop();
            p.show_cx_.drop();
            p.hide_cx_.drop();
            pages_.erase(pages_.begin()+n);
            signal_page_removed_(page);
            for (int n = pages_.size(); n > page; --n) { signal_page_reordered_(n-1, n); }
            roller_->par_show(!pages_.empty());
            update_frame_border();
            update_tabs();
            if (page == current) { update_current(); }
            invalidate();
            return n;
        }
    }

    return -1;
}

int Notebook_impl::current_page() const {
    auto wp = card_->current();
    auto i = std::find_if(pages_.begin(), pages_.end(), [wp](auto & p) { return wp == p.wp_.get(); } );
    return i != pages_.end() ? i-pages_.begin() : -1;
}

void Notebook_impl::clear_pages() {
    unsigned n = pages_.size();

    if (n) {
        int current = current_page();
        if (current >= 0) { pages_[current].unselect(); }
        pages_.clear();
        for (; n > 0; --n) { signal_page_removed_(n-1); }
        card_->clear();
        abs_->clear();
        update_frame_border();
        update_tabs();
    }
}

int Notebook_impl::append_page(Widget_ptr wp, Widget_ptr tp) {
    int current = current_page(), nth_page = pages_.size();
    pages_.emplace_back();
    init_page(nth_page, wp, tp);
    if (!nth_page) { update_frame_border(); }
    update_tabs();
    signal_page_added_(nth_page);

    if (current < 0) {
        update_current();
        int current = current_page();
        signal_page_changed_(current);
        pages_[current].select();
    }

    return nth_page;
}

int Notebook_impl::append_page(Widget_ptr wp) {
    return append_page(wp, str_format("Page ", 1+pages_.size()));
}

int Notebook_impl::append_page(Widget_ptr wp, const ustring & title, Align align) {
    auto tp = std::make_shared<Label_impl>(title, align);
    tp->hint_margin(2);
    return append_page(wp, tp);
}

int Notebook_impl::append_page(Widget_ptr wp, const ustring & title) {
    return append_page(wp, title, Align::CENTER);
}

void Notebook_impl::prepend_page(Widget_ptr wp, Widget_ptr tp) {
    int current = current_page();
    pages_.emplace(pages_.begin());
    init_page(0, wp, tp);
    signal_page_added_(0);
    if (current < 0) { update_frame_border(); }
    update_tabs();
    for (std::size_t n = pages_.size()-1; n > 0; --n) { signal_page_reordered_(n, n-1); }

    if (current < 0) {
        update_current();
        int current = current_page();
        signal_page_changed_(current);
        pages_[current].select();
    }
}

void Notebook_impl::prepend_page(Widget_ptr wp) {
    prepend_page(wp, str_format("Page ", 1+pages_.size()));
}

void Notebook_impl::prepend_page(Widget_ptr wp, const ustring & title, Align align) {
    auto tp = std::make_shared<Label_impl>(title, align);
    tp->hint_margin(2);
    prepend_page(wp, tp);
}

void Notebook_impl::prepend_page(Widget_ptr wp, const ustring & title) {
    prepend_page(wp, title, Align::CENTER);
}

int Notebook_impl::insert_page(Widget_ptr wp, int nth_page, Widget_ptr tp) {
    int current = current_page();
    if (nth_page < 0) { nth_page = 0; }
    if (std::size_t(nth_page) >= pages_.size()) { nth_page = pages_.empty() ? 0 : pages_.size(); }
    pages_.emplace(pages_.begin()+nth_page);
    init_page(nth_page, wp, tp);
    for (std::size_t n = pages_.size()-1; n > std::size_t(nth_page); --n) { signal_page_reordered_(n, n-1); }
    if (current < 0) { update_frame_border(); }
    update_tabs();
    signal_page_added_(nth_page);

    if (current < 0) {
        update_current();
        int current = current_page();
        signal_page_changed_(current);
        pages_[current].select();
    }

    return nth_page;
}

int Notebook_impl::insert_page(Widget_ptr wp, int nth_page) {
    return insert_page(wp, nth_page, str_format("Page ", 1+pages_.size()));
}

int Notebook_impl::insert_page(Widget_ptr wp, int nth_page, const ustring & title, Align align) {
    auto tp = std::make_shared<Label_impl>(title, align);
    tp->hint_margin(2);
    return insert_page(wp, nth_page, tp);
}

int Notebook_impl::insert_page(Widget_ptr wp, int nth_page, const ustring & title) {
    return insert_page(wp, nth_page, title, Align::CENTER);
}

int Notebook_impl::insert_page_after(Widget_ptr wp, Widget_ptr after_this, Widget_ptr tp) {
    int nth_page = page_number(after_this);
    if (nth_page < 0) { nth_page = pages_.size(); }
    return insert_page(wp, nth_page, tp);
}

int Notebook_impl::insert_page_before(Widget_ptr wp, Widget_ptr before_this, Widget_ptr tp) {
    int nth_page = page_number(before_this);
    if (nth_page < 0) { nth_page = 0; }
    if (nth_page > 0) { --nth_page; }
    return insert_page(wp, nth_page, tp);
}

int Notebook_impl::insert_page_after(Widget_ptr wp, Widget_ptr after_this) {
    return insert_page_after(wp, after_this, str_format("Page ", 1+pages_.size()));
}

int Notebook_impl::insert_page_after(Widget_ptr wp, Widget_ptr after_this, const ustring & title, Align align) {
    auto tp = std::make_shared<Label_impl>(title, align);
    tp->hint_margin(2);
    return insert_page_after(wp, after_this, tp);
}

int Notebook_impl::insert_page_after(Widget_ptr wp, Widget_ptr after_this, const ustring & title) {
    return insert_page_after(wp, after_this, title, Align::CENTER);
}

int Notebook_impl::insert_page_before(Widget_ptr wp, Widget_ptr before_this) {
    return insert_page_before(wp, before_this, str_format("Page ", 1+pages_.size()));
}

int Notebook_impl::insert_page_before(Widget_ptr wp, Widget_ptr before_this, const ustring & title, Align align) {
    auto tp = std::make_shared<Label_impl>(title, align);
    tp->hint_margin(2);
    return insert_page_before(wp, before_this, tp);
}

int Notebook_impl::insert_page_before(Widget_ptr wp, Widget_ptr before_this, const ustring & title) {
    return insert_page_before(wp, before_this, title, Align::CENTER);
}

void Notebook_impl::update_frame_border() {
    if (!empty() && tabs_visible_) {
        switch (orientation()) {
            case Orientation::RIGHT:
                roller_->hint_margin_left(2);
                frame_->set_border_left(spc_);
                break;

            case Orientation::LEFT:
                roller_->hint_margin_right(2);
                frame_->set_border_right(spc_);
                break;

            case Orientation::UP:
                roller_->hint_margin_bottom(2);
                frame_->set_border_bottom(spc_);
                break;

            case Orientation::DOWN:
                roller_->hint_margin_top(2);
                frame_->set_border_top(spc_);
                break;
        }
    }

    else {
        roller_->hint_margin(0);
        frame_->set_border(0);
    }
}

void Notebook_impl::init_page(unsigned nth_page, Widget_ptr wp, Widget_ptr tp) {
    Page & p = pages_[nth_page];
    p.wp_ = wp;
    p.title_ = tp;
    p.frame_ = std::make_shared<Frame_impl>();

    if (horizontal()) {
        if (Orientation::RIGHT == orientation()) {
            p.frame_->set_border_left(1);
            p.frame_->set_border_left_style(Border::OUTSET);
        }

        else {
            p.frame_->set_border_right(1);
            p.frame_->set_border_right_style(Border::OUTSET);
        }

        p.frame_->set_border_top(1);
        p.frame_->set_border_bottom(1);
        p.frame_->set_border_top_style(Border::OUTSET);
        p.frame_->set_border_bottom_style(Border::OUTSET);
    }

    else {
        if (Orientation::DOWN == orientation()) {
            p.frame_->set_border_top(1);
            p.frame_->set_border_top_style(Border::OUTSET);
        }

        else {
            p.frame_->set_border_bottom(1);
            p.frame_->set_border_bottom_style(Border::OUTSET);
        }

        p.frame_->set_border_left(1);
        p.frame_->set_border_right(1);
        p.frame_->set_border_left_style(Border::OUTSET);
        p.frame_->set_border_right_style(Border::OUTSET);
    }

    p.frame_->insert(tp);
    p.size1_cx_ = p.frame_->signal_size_changed().connect(bind_back(fun(this, &Notebook_impl::update_tabs), Hints::SIZE));
    p.hints_cx_ = p.frame_->signal_hints_changed().connect(fun(this, &Notebook_impl::update_tabs));
    p.mouse_down_cx_ = p.frame_->signal_mouse_down().connect(bind_back(fun(this, &Notebook_impl::on_tab_mouse_down), p.frame_.get()));
    p.mouse_up_cx_ = p.frame_->signal_mouse_up().connect(bind_back(fun(this, &Notebook_impl::on_tab_mouse_up), p.frame_.get()));
    p.mouse_motion_cx_ = p.frame_->signal_mouse_motion().connect(bind_back(fun(this, &Notebook_impl::on_tab_mouse_motion), p.frame_.get()));
    p.show_cx_ = p.wp_->signal_show().connect(bind_back(fun(this, &Notebook_impl::on_widget_show), p.wp_.get()));
    p.hide_cx_ = p.wp_->signal_hide().connect(bind_back(fun(this, &Notebook_impl::on_widget_hide), p.wp_.get()));

    card_->insert(wp);
    abs_->put(p.frame_, Point());
    update_frame_border();
    roller_->par_show(tabs_visible_);
    update_tabs();
    invalidate();
}

void Notebook_impl::update_current() {
    int current = -1;
    Color c = conf().brush(Conf::BUTTON_BACKGROUND).value().color();

    for (std::size_t i = 0; i < pages_.size(); ++i) {
        Page & pg = pages_[i];

        if (pg.wp_.get() != card_->current()) {
            pg.frame_->conf().brush(Conf::BACKGROUND) = Brush(c.darken(0.1));
        }

        else {
            current = i;
            pg.frame_->conf().brush(Conf::BACKGROUND) = Brush(c.lighten(0.1));
            roller_->pan(pg.frame_.get());
        }
    }

    if (current >= 0 && current != last_) { last_ = current; signal_page_changed_(current); }
}

void Notebook_impl::update_tabs(Hints) {
    if (!in_shutdown() && ! in_arrange_tabs_ && tabs_visible_) {
        in_arrange_tabs_ = true;
        unsigned aw = 0, ah = 0, wmax = 0, hmax = 0, spc = 0;

        for (auto & p: pages_) {
            Size req = child_requisition(p.frame_.get());
            wmax = std::max(wmax, req.width());
            hmax = std::max(hmax, req.height());
            ++spc;
        }

        spc = spc > 1 ? (spc-1)*spc_ : 0;
        aw = horizontal() ? wmax : spc;
        ah = horizontal() ? spc : hmax;

        for (auto & p: pages_) {
            if (homogeneous_tabs_) {
                abs_->resize(p.frame_.get(), wmax, hmax);
                if (horizontal()) { ah += hmax; aw = std::max(aw, wmax); }
                else { aw += wmax; ah = std::max(ah, hmax); }
            }

            else {
                Size req = child_requisition(p.frame_.get());
                if (horizontal()) { ah += req.height(), abs_->resize(p.frame_.get(), wmax, req.height()); }
                else { aw += req.width(), abs_->resize(p.frame_.get(), req.width(), hmax); }
            }
        }

        abs_->show();
        abs_->hint_min_size(aw, ah);
        int offset = 0;

        for (auto & p: pages_) {
            abs_->show();

            if (horizontal()) {
                if (p.frame_ != drag_) { abs_->move(p.frame_.get(), 0, offset); }
                offset += p.frame_->size().height();
            }

            else {
                if (p.frame_ != drag_) { abs_->move(p.frame_.get(), offset, 0); }
                offset += p.frame_->size().width();
            }

            offset += spc_;
        }

        roller_->invalidate();
        in_arrange_tabs_ = false;
    }
}

Size Notebook_impl::child_requisition(Widget_impl * wp) {
    Size req = wp->required_size();
    req.update(wp->size_hint(), true);
    req.update_max(wp->min_size_hint());
    req.update_max(16, 8);
    req.update_min(wp->max_size_hint(), true);
    return req;
}

int Notebook_impl::show_next() {
    if (!pages_.empty()) {
        unsigned current = std::max(0, current_page());

        if (pages_.size() > 1) {
            if (1+current < pages_.size() || rollover_allowed_) {
                if (++current >= pages_.size()) { current = 0; }
                pages_[current].wp_->show();
            }
        }

        return current;
    }

    return -1;
}

int Notebook_impl::show_previous() {
    if (!pages_.empty()) {
        unsigned current = std::max(0, current_page());

        if (pages_.size() > 1) {
            if (current > 0 || rollover_allowed_) {
                current = current > 0 ? current-1 : pages_.size()-1;
                pages_[current].wp_->show();
            }
        }

        return current;
    }

    return -1;
}

int Notebook_impl::show_page(int nth_page, bool take_focus) {
    if (nth_page >= 0) {
        unsigned n = nth_page;

        if (n < pages_.size()) {
            pages_[n].wp_->show();
            if (take_focus) { show_page(n); pages_[n].wp_->take_focus(); }
            return n;
        }
    }

    return -1;
}

int Notebook_impl::show_page(Widget_cptr cwp, bool take_focus) {
    return show_page(page_number(cwp));
}

void Notebook_impl::undrag() {
    undrag_cx_.drop();
    drag_tv_ = 0;

    for (auto & p: pages_) {
        p.frame_->ungrab_mouse();
    }

    if (drag_) {
        while (drag_->level()) { drag_->lower(); }
        drag_.reset();
        update_tabs();
    }
}

void Notebook_impl::on_tab_mouse_motion(int mm, const Point & pt, Widget_impl * wp) {
    if (0 != drag_tv_ && Timeval::now() >= drag_tv_) {
        if (mm & MM_LEFT && 0 != drag_tv_ && Timeval::now() >= drag_tv_) {
            Point ppt = wp->to_parent(wp->container(), pt);
            int moving = -1, moved = -1, current = current_page();

            auto j = std::find_if(pages_.begin(), pages_.end(), [wp](auto & p) { return wp == p.frame_.get(); } );
            if (j != pages_.end()) { moving = j-pages_.begin(); }

            if (moving >= 0) {
                if (drag_) {
                    Rect va { abs_->offset(), abs_->size() };

                    if (horizontal()) {
                        abs_->move(drag_.get(), 0, ppt.y());
                        if (ppt.y() > va.bottom()) { roller_->pan(ppt.y()-va.height()); }
                        else if (ppt.y() < va.top()) { roller_->pan(ppt.y()); }
                        ppt = Rect(drag_->origin(), drag_->size()).center();

                        for (std::size_t i = 0; i < pages_.size(); ++i) {
                            if (pages_[i].frame_.get() != wp) {
                                Point origin = pages_[i].frame_->origin();
                                Size sz(va.width(), pages_[i].frame_->size().height());
                                if (Rect(origin, sz).contains(ppt)) { moved = i; break; }
                            }
                        }
                    }

                    else {
                        abs_->move(drag_.get(), ppt.x(), 0);
                        if (ppt.x() > va.right()) { roller_->pan(ppt.x()-va.width()); }
                        else if (ppt.x() < va.left()) { roller_->pan(ppt.x()); }
                        ppt = Rect(drag_->origin(), drag_->size()).center();

                        for (std::size_t i = 0; i < pages_.size(); ++i) {
                            if (pages_[i].frame_.get() != wp) {
                                Point origin = pages_[i].frame_->origin();
                                Size sz(va.height(), pages_[i].frame_->size().width());
                                if (Rect(origin, sz).contains(ppt)) { moved = i; break; }
                            }
                        }
                    }

                    if (moving != moved) {
                        if (moved >= 0) {
                            std::swap(pages_[moving], pages_[moved]);
                            signal_page_reordered_(moving, moved);
                            update_tabs();
                            if (std::min(moved, moving) <= current) { update_current(); }
                        }
                    }
                }

                else if (reorder_allowed_ && pages_.size() > 1) {
                    Page & p = pages_[moving];

                    if (!undrag_cx_.empty()) {
                        undrag_cx_.drop();
                        drag_ = p.frame_;
                        drag_->grab_mouse();
                        if (0 == drag_->level()) { drag_->rise(); }
                    }
                }
            }
        }

        else {
            undrag();
        }
    }
}

bool Notebook_impl::on_tab_mouse_down(int mbt, int mm, const Point & pt, Widget_impl * wi) {
    if (MBT_LEFT == mbt) {
        auto i = std::find_if(pages_.begin(), pages_.end(), [wi](auto & pg) { return wi == pg.frame_.get(); } );

        if (i != pages_.end()) {
            if (undrag_cx_.empty()) {
                undrag_cx_ = Loop_impl::this_ptr()->alarm(fun(this, &Notebook_impl::undrag), 443);
                drag_tv_ = Timeval::future(223447);
            }

            else {
                undrag_cx_.drop();
                drag_tv_ = 0;
            }

            return true;
        }
    }

    return false;
}

bool Notebook_impl::on_tab_mouse_up(int mbt, int mm, const Point & pt, Widget_impl * wi) {
    if (MBT_LEFT == mbt) {
        if (!drag_) {
            auto i = std::find_if(pages_.begin(), pages_.end(), [wi](auto & pg) { return wi == pg.frame_.get(); } );
            if (i != pages_.end()) { i->wp_->show(); }
        }

        else {
            undrag();
        }

        return true;
    }

    return false;
}

bool Notebook_impl::on_mouse_wheel(int d, int mm, const Point & pt) {
    if (d > 0) { show_next(); }
    else { show_previous(); }
    return true;
}

void Notebook_impl::on_widget_show(Widget_impl * wp) {
    update_current();
    if (wp->visible() && !wp->selected()) { wp->signal_select()(); }
}

void Notebook_impl::on_widget_hide(Widget_impl * wp) {
    update_current();
    if (wp->hidden() && wp->selected()) { wp->signal_unselect()(); }
}

void Notebook_impl::reorder_page(Widget_ptr wp, int nth_page) {
    reorder_page(page_number(wp), nth_page);
}

void Notebook_impl::reorder_page(int old_page, int new_page) {
    if (reorder_allowed_) {
        int size = pages_.size();

        if (old_page >= 0 && new_page >= 0 && old_page < size && new_page < size && old_page != new_page) {
            std::swap(pages_[old_page], pages_[new_page]);
            signal_page_reordered_(new_page, old_page);
            if (old_page == last_ || new_page == last_) { last_ = -1; }
            update_tabs();
            update_current();
        }
    }
}

void Notebook_impl::allow_reorder() {
    reorder_allowed_ = true;
}

void Notebook_impl::disallow_reorder() {
    reorder_allowed_ = false;
}

void Notebook_impl::show_tabs() {
    if (!tabs_visible_) {
        tabs_visible_ = true;
        update_frame_border();
        roller_->show();
    }
}

void Notebook_impl::hide_tabs() {
    if (tabs_visible_) {
        tabs_visible_ = false;
        update_frame_border();
        roller_->hide();
    }
}

void Notebook_impl::set_homogeneous_tabs() {
    if (!homogeneous_tabs_) {
        homogeneous_tabs_ = true;
        update_tabs();
    }
}

void Notebook_impl::unset_homogeneous_tabs() {
    if (homogeneous_tabs_) {
        homogeneous_tabs_ = false;
        update_tabs();
    }
}

Widget_ptr Notebook_impl::widget_at(int page) {
    return page >= 0 && unsigned(page) < pages_.size() ? pages_[page].wp_ : nullptr;
}

Widget_cptr Notebook_impl::widget_at(int page) const {
    return page >= 0 && unsigned(page) < pages_.size() ? pages_[page].wp_ : nullptr;
}

Widget_ptr Notebook_impl::tab_at(int page) {
    return page >= 0 && unsigned(page) < pages_.size() ? pages_[page].title_ : nullptr;
}

Widget_cptr Notebook_impl::tab_at(int page) const {
    return page >= 0 && unsigned(page) < pages_.size() ? pages_[page].title_ : nullptr;
}

void Notebook_impl::append_widget(Widget_ptr wp, bool shrink) {
    mainbox_->append(wp, shrink);
}

void Notebook_impl::prepend_widget(Widget_ptr wp, bool shrink) {
    mainbox_->prepend(wp, shrink);
}

void Notebook_impl::append_tab(Widget_ptr wp, bool shrink) {
    tabbox_->append(wp, shrink);
}

void Notebook_impl::prepend_tab(Widget_ptr wp, bool shrink) {
    tabbox_->prepend(wp, shrink);
}

void Notebook_impl::remove(Widget_impl * wp) {
    tabbox_->remove(wp);
    mainbox_->remove(wp);
}

void Notebook_impl::on_abs_size() {
    for (auto wp: tabbox_->children()) {
        if (wp.get() != abs_) {
            if (tabbox_->horizontal()) { wp->hint_max_size(0, abs_->size().height()); }
            else { wp->hint_max_size(abs_->size().width(), 0); }
        }
    }
}

} // namespace tau

//END

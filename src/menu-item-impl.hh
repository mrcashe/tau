// ----------------------------------------------------------------------------
// SPDX-License-Identifier: BSD-2-Clause
// Copyright © 2014-2024 Konstantin Shmelkov <mrcashe@gmail.com>.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
// THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
// PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
// CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
// EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
// PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
// OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
// WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
// OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
// EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// ----------------------------------------------------------------------------

#ifndef __TAU_MENU_ITEM_IMPL_HH__
#define __TAU_MENU_ITEM_IMPL_HH__

#include <check-impl.hh>
#include <icon-impl.hh>
#include <separator-impl.hh>
#include <label-impl.hh>

namespace tau {

class Menu_item_impl: public Label_impl {
    signal<void()>      signal_activate_;

public:

    Menu_item_impl(const ustring & label=ustring()):
        Label_impl(label, Align::START)
    {
    }

    void activate() {
        if (enabled()) {
            signal_activate_();
        }
    }

    signal<void()> & signal_activate() { return signal_activate_; }
};

// ----------------------------------------------------------------------------
// ----------------------------------------------------------------------------

struct Menu_image {
    Widget_ptr img_;

    Menu_image() = default;

    Menu_image(Widget_ptr img):
        img_(img)
    {
    }

    Menu_image(const ustring & icon_name):
        img_(std::make_shared<Icon_impl>(icon_name, Icon::SMALL))
    {
    }

    Menu_image(Pixmap_cptr pix):
        img_(std::make_shared<Icon_impl>(pix))
    {
    }

    void assign_icon(const ustring & icon_name, int icon_size=Icon::SMALL) {
        if (auto ip = std::dynamic_pointer_cast<Icon_impl>(img_)) {
            ip->assign(icon_name, icon_size);
        }

        else {
            img_ = std::make_shared<Icon_impl>(icon_name, icon_size);
        }
    }

    void assign_icon(Pixmap_cptr pix) {
        if (auto ip = std::dynamic_pointer_cast<Icon_impl>(img_)) {
            ip->assign(pix);
        }

        else {
            img_ = std::make_shared<Icon_impl>(pix);
        }
    }
};

// ----------------------------------------------------------------------------
// ----------------------------------------------------------------------------

struct Menu_accel {
    Label_ptr    accel_label_;

    Menu_accel(Align align=Align::END) {
        accel_label_ = std::make_shared<Label_impl>(align);
        accel_label_->conf().redirect(Conf::FOREGROUND, Conf::ACCEL_FOREGROUND);
    }
};

// ----------------------------------------------------------------------------
// ----------------------------------------------------------------------------

class Submenu_impl: public Menu_item_impl, public Menu_image {
public:

    Submenu_impl(const ustring & label, Menu_ptr mp, const ustring & icon_name=ustring()):
        Menu_item_impl(label),
        Menu_image(icon_name)
    {
        set_menu(mp);
        init();
    }

    Submenu_impl(const ustring & label, const ustring & icon_name=ustring()):
        Menu_item_impl(label),
        Menu_image(icon_name)
    {
        init();
    }

    Submenu_impl(const ustring & label, Menu_ptr mp, Pixmap_cptr pix):
        Menu_item_impl(label),
        Menu_image(pix)
    {
        set_menu(mp);
        init();
    }

    Submenu_impl(const ustring & label, Pixmap_cptr pix):
        Menu_item_impl(label),
        Menu_image(pix)
    {
        init();
    }

    Menu_ptr menu() { return menu_; }
    Menu_cptr menu() const { return menu_; }
    Widget_ptr arrow() { return arrow_; }
    Widget_cptr arrow() const { return arrow_; }
    void set_menu(Menu_ptr mp);

private:

    Menu_ptr    menu_;
    Widget_ptr  arrow_;

private:

    void init();
    void on_display_in();
};

// ----------------------------------------------------------------------------
// ----------------------------------------------------------------------------

class Action_menu_impl: public Menu_item_impl, public Menu_image, public Menu_accel {
public:

    explicit Action_menu_impl(const ustring & label, std::string_view icon_name, Align accel_align=Align::END);
    explicit Action_menu_impl(Action & action, Align accel_align=Align::END);
    explicit Action_menu_impl(Master_action & action, Align accel_align=Align::END);
    void select(Action & action);

private:

    connection  enable_cx_      { true };
    connection  disable_cx_     { true };
    connection  show_cx_        { true };
    connection  hide_cx_        { true };
    connection  exec_cx_        { true };

private:

    void update_accels(const Action & action);
    void update_master_accels(const Master_action & action);
    void on_accel_added(const Accel & accel, const Action & action);
    void on_master_accel_added(const Accel & accel, const Master_action & action);
    void on_accel_changed(const Accel &, const Accel &, const Action & action);
    void on_accel_removed(const Accel &, const Action & action);
    void on_master_accel_removed(const Accel &, const Master_action & action);
    void on_label_changed();
    void on_select(Action & action);
    void on_unselect(Action & action);
};

// ----------------------------------------------------------------------------
// ----------------------------------------------------------------------------

class Toggle_menu_impl: public Menu_item_impl, public Menu_image, public Menu_accel {
public:

    Toggle_menu_impl(const ustring & label, std::string_view icon_name);
    Toggle_menu_impl(Master_action & master_action);
    Toggle_menu_impl(Toggle_action & toggle_action);
    Toggle_menu_impl(const ustring & label, std::string_view icon_name, Align accel_align);
    Toggle_menu_impl(Master_action & master_action, Align accel_align);
    Toggle_menu_impl(Toggle_action & toggle_action, Align accel_align);
    Toggle_menu_impl(const ustring & label, Check::Style check_style, Border border_style=Border::SOLID);
    Toggle_menu_impl(Master_action & master_action, Check::Style check_style, Border border_style=Border::SOLID);
    Toggle_menu_impl(Toggle_action & toggle_action, Check::Style check_style, Border border_style=Border::SOLID);
    Toggle_menu_impl(const ustring & label, Align accel_align, Check::Style check_style, Border border_style=Border::SOLID);
    Toggle_menu_impl(Master_action & master_action, Align accel_align, Check::Style check_style, Border border_style=Border::SOLID);
    Toggle_menu_impl(Toggle_action & toggle_action, Align accel_align, Check::Style check_style, Border border_style=Border::SOLID);

    void set_check_style(Check::Style check_style);
    Check::Style check_style() const noexcept;

    void set_border_style(Border border_style);
    Border border_style() const noexcept;

    void set_border_width(unsigned npx);
    unsigned border_width() const noexcept;
    bool get() const noexcept;

    void select(Toggle_action & action);

private:

    Check_ptr   check_;
    Toggle_ptr  toggle_;
    connection  enable_cx_      { true };
    connection  disable_cx_     { true };
    connection  show_cx_        { true };
    connection  hide_cx_        { true };
    connection  toggle_cx_      { true };
    connection  setup_cx_       { true };
    connection  check_cx_       { true };
    connection  uncheck_cx_     { true };

private:

    void init();
    void init_button(std::string_view icon_name);
    void init(Toggle_action & action);
    void init(Master_action & action);
    void update_accels(const Toggle_action & action);
    void update_master_accels(const Master_action & action);
    void on_label_changed();
    void on_accel_added(const Accel &, const Toggle_action & action);
    void on_master_accel_added(const Accel &, const Master_action & action);
    void on_accel_changed(const Accel &, const Accel &, const Toggle_action & action);
    void on_accel_removed(const Accel &, const Toggle_action & action);
    void on_master_accel_removed(const Accel &, const Master_action & action);
    void on_action_toggle(bool toggled);
    void on_button_toggle(bool) { activate(); }
    void on_select(Toggle_action & action);
    void on_unselect(Toggle_action & action);
};

// ----------------------------------------------------------------------------
// ----------------------------------------------------------------------------

class Check_menu_impl: public Menu_item_impl {
public:

    Check_menu_impl(const ustring & label, bool checked=false);
    Check_menu_impl(const ustring & label, Check::Style check_style, bool checked=false);
    Check_menu_impl(const ustring & label, Border border_style, bool checked=false);
    Check_menu_impl(const ustring & label, Check::Style check_style, Border border_style, bool checked=false);

    Check_ptr check_ptr() { return check_; }
    Check_cptr check_ptr() const { return check_; }

    void set_check_style(Check::Style check_style) { check_->set_check_style(check_style); }
    Check::Style check_style() const noexcept { return check_->check_style(); }
    void set_border_style(Border border_style) { check_->set_border_style(border_style); }
    Border border_style() const noexcept { return check_->border_style(); }
    void set_border_width(unsigned npx) { check_->set_border_width(npx); }
    unsigned border_width() const noexcept { return check_->border_width(); }

    void check() { check_->check(); }
    void uncheck() { check_->uncheck(); }
    void set(bool state) { check_->set(state); }
    void setup(bool state) { check_->setup(state); }
    void toggle() { check_->toggle(); }
    bool checked() const noexcept { return check_->checked(); }
    void join(Check_menu_ptr other);
    bool joined() const noexcept { return check_->joined(); }

    signal<void()> & signal_check() { return check_->signal_check(); }
    signal<void()> & signal_uncheck() { return check_->signal_uncheck(); }

private:

    Check_ptr   check_;

private:

    void init();
    void on_display_in();
    void on_enable();
    void on_disable();
    void on_check_enable();
    void on_check_disable();
};

// ----------------------------------------------------------------------------
// ----------------------------------------------------------------------------

class Slot_menu_impl: public Menu_item_impl, public Menu_image {
public:

    Slot_menu_impl(const ustring & label, const slot<void()> & slot_activate, const ustring & icon_name=ustring()):
        Menu_item_impl(label),
        Menu_image(icon_name)
    {
        signal_activate().connect(slot_activate);
    }

    Slot_menu_impl(const ustring & label, const slot<void()> & slot_activate, Pixmap_cptr pix):
        Menu_item_impl(label),
        Menu_image(pix)
    {
        signal_activate().connect(slot_activate);
    }
};

} // namespace tau

#endif // __TAU_MENU_ITEM_IMPL_HH__

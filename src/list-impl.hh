// ----------------------------------------------------------------------------
// SPDX-License-Identifier: BSD-2-Clause
// Copyright © 2014-2024 Konstantin Shmelkov <mrcashe@gmail.com>.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
// THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
// PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
// CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
// EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
// PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
// OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
// WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
// OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
// EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// ----------------------------------------------------------------------------

#ifndef __TAU_LIST_IMPL_HH__
#define __TAU_LIST_IMPL_HH__

#include <tau/action.hh>
#include <table-impl.hh>
#include <set>

namespace tau {

class List_impl: public Table_impl {
public:

    List_impl();
    List_impl(unsigned spacing);
    List_impl(unsigned xspacing, unsigned yspacing);

    int  prepend_row(Widget_ptr wp, bool shrink=false);
    int  prepend_row(Widget_ptr wp, Align align, bool shrink=false);
    int  insert_row(Widget_ptr wp, int position, bool shrink=false);
    int  insert_row(Widget_ptr wp, int position, Align align, bool shrink=false);
    int  append_row(Widget_ptr wp, bool shrink=false);
    int  append_row(Widget_ptr wp, Align align, bool shrink=false);

    int  prepend(Widget_ptr wp, bool shrink=false);
    int  prepend(Widget_ptr wp, Align align, bool shrink=false);
    int  insert(Widget_ptr wp, int position, bool shrink=false);
    int  insert(Widget_ptr wp, int position, Align align, bool shrink=false);
    int  append(Widget_ptr wp, bool shrink=false);
    int  append(Widget_ptr wp, Align align, bool shrink=false);

    int  prepend(int y, Widget_ptr wp, bool shrink=false);
    int  prepend(int y, Widget_ptr wp, Align align, bool shrink=false);
    int  insert(int y, Widget_ptr wp, int position, bool shrink=false);
    int  insert(int y, Widget_ptr wp, int position, Align align, bool shrink=false);
    int  append(int y, Widget_ptr wp, bool shrink=false);
    int  append(int y, Widget_ptr wp, Align align, bool shrink=false);

    std::size_t remove(int y);

    // FIXME Redefinition of Table_impl::remove(Widget_impl *).
    std::size_t remove(Widget_impl * wp);

    std::list<Widget_ptr> widgets(int y) const { return Table_impl::widgets({ INT_MIN, y, INT_MAX, 1+y}); }
    Widget_ptr widget_at(int y, int col) noexcept;
    Widget_cptr widget_at(int y, int col) const noexcept;
    std::size_t count() const noexcept { return sels_.size(); }

    int select(int y, bool fix=false);
    int select_next(bool fix=false);
    int select_previous(bool fix=false);
    int select_front(bool fix=false);
    int select_back(bool fix=false);

    int  current() const noexcept;
    bool fixed() const noexcept { return INT_MIN != fixed_; }
    void unfix() { fixed_ = INT_MIN; }

    // FIXME Hides Table_impl::selection().
    std::vector<int> selection() const;

    // TODO 0.8 Copy to facade.
    std::size_t sel_count() const;

    // FIXME Redefinition of Table_impl::has_selection().
    bool has_selection() const noexcept { return Table_impl::has_selection() || has_marks(); }

    // FIXME Redefinition of Table_impl::unselect().
    void unselect();

    // TODO 0.8 Copy to facade.
    // NOTE We can not make it noexcept!
    bool row_visible(int y) const;

    void allow_multiple_select() { allow_multiple_select(true); }
    void disallow_multiple_select()  { allow_multiple_select(false); }
    bool multiple_select_allowed() const noexcept { return multiple_select_allowed_; }

    Action & action_cancel() { return action_cancel_; }
    Action & action_activate() { return action_activate_; }
    Action & action_previous() { return action_previous_; }
    Action & action_next() { return action_next_; }
    Action & action_previous_page() { return action_previous_page_; }
    Action & action_next_page() { return action_next_page_; }
    Action & action_home() { return action_home_; }
    Action & action_end() { return action_end_; }
    Action & action_select_previous() { return action_select_previous_; }
    Action & action_select_next() { return action_select_next_; }
    Action & action_select_previous_page() { return action_select_previous_page_; }
    Action & action_select_next_page() { return action_select_next_page_; }
    Action & action_select_home() { return action_select_home_; }
    Action & action_select_end() { return action_select_end_; }
    Action & action_select_all() { return action_select_all_; }

    signal<void(int)> & signal_select_row() { return signal_select_row_; }
    signal<void(int)> & signal_activate_row() { return signal_activate_row_; }
    signal<void(int)> & signal_remove_row() { return signal_remove_row_; }
    signal<void(int, int)> & signal_move_row() { return signal_move_row_; }
    signal_all<int> & signal_mark_validate() { return signal_mark_validate_; }

protected:

    bool on_fake_mouse(int mbt, int mm, const Point & pt) { return true; }

private:

    Action              action_cancel_                  { "Escape Cancel", fun(this, &List_impl::drop_focus) };
    Action              action_activate_                { KC_ENTER, KM_NONE, fun(this, &List_impl::activate_current) };
    Action              action_previous_                { KC_UP, KM_NONE, fun(this, &List_impl::on_prev) };
    Action              action_next_                    { KC_DOWN, KM_NONE, fun(this, &List_impl::on_next) };
    Action              action_previous_page_           { KC_PAGE_UP, KM_NONE, fun(this, &List_impl::on_page_up) };
    Action              action_next_page_               { KC_PAGE_DOWN, KM_NONE, fun(this, &List_impl::on_page_down) };
    Action              action_home_                    { KC_HOME, KM_NONE, fun(this, &List_impl::on_home) };
    Action              action_end_                     { KC_END, KM_NONE, fun(this, &List_impl::on_end) };
    Action              action_select_previous_         { KC_UP, KM_SHIFT, fun(this, &List_impl::on_select_prev) };
    Action              action_select_next_             { KC_DOWN, KM_SHIFT, fun(this, &List_impl::on_select_next) };
    Action              action_select_previous_page_    { KC_PAGE_UP, KM_SHIFT, fun(this, &List_impl::on_select_page_up) };
    Action              action_select_next_page_        { KC_PAGE_DOWN, KM_SHIFT, fun(this, &List_impl::on_select_page_down) };
    Action              action_select_home_             { KC_HOME, KM_SHIFT, fun(this, &List_impl::on_select_home) };
    Action              action_select_end_              { KC_END, KM_SHIFT, fun(this, &List_impl::on_select_end) };
    Action              action_select_all_              { U'A', KM_CONTROL, fun(this, &List_impl::on_select_all) };

    Action *            sel_actions_[7]                 { &action_select_previous_,         &action_select_next_,
                                                          &action_select_previous_page_,    &action_select_next_page_,
                                                          &action_select_home_,             &action_select_end_,
                                                          &action_select_all_
                                                        };

    signal<void(int)>   signal_select_row_;
    signal<void(int)>   signal_activate_row_;
    signal<void(int)>   signal_remove_row_;
    signal<void(int, int)> signal_move_row_;
    signal_all<int>     signal_mark_validate_;

    using Selectables   = std::set<int>;
    using Frees         = std::set<Widget_impl *>;

    bool                multiple_select_allowed_        = false;
    int                 activated_                      = INT_MIN;
    int                 fixed_                          = INT_MIN;
    Selectables         sels_;
    Frees               frees_;

private:

    void init();
    void activate_current();
    void adjust();
    int  prev_row();
    int  next_row();
    int  page_up_row();
    int  page_down_row();
    void unselect_only();
    void allow_multiple_select(bool yes);
    void reset_activated();
    void unmark_all();
    bool row_marked(int y);
    void unmark_row(int y);
    void mark_row(int y);
    void flip_mark(int y);

    bool on_mouse_down(int mbt, int mm, const Point & pt);
    bool on_mouse_double_click(int mbt, int mm, const Point & pt);
    void on_column_bounds_changed(int row);
    void on_next();
    void on_prev();
    void on_page_up();
    void on_page_down();
    void on_home();
    void on_end();
    void on_select_next();
    void on_select_prev();
    void on_select_page_up();
    void on_select_page_down();
    void on_select_home();
    void on_select_end();
    void on_select_all();
    bool on_take_focus();
    void on_selection_changed();
    void on_select_background();
    void on_children_changed();
    void on_hints_changed(Widget_impl * wp, Hints hints);
};

} // namespace tau

#endif // __TAU_LIST_IMPL_HH__

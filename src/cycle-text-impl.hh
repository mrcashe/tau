// ----------------------------------------------------------------------------
// SPDX-License-Identifier: BSD-2-Clause
// Copyright © 2014-2024 Konstantin Shmelkov <mrcashe@gmail.com>.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
// THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
// PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
// CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
// EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
// PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
// OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
// WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
// OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
// EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// ----------------------------------------------------------------------------

#ifndef __TAU_CYCLE_TEXT_IMPL_HH__
#define __TAU_CYCLE_TEXT_IMPL_HH__

#include <cycle-impl.hh>

namespace tau {

class Cycle_text_impl: public Cycle_impl {
public:

    Cycle_text_impl(Border bs=Border::INSET);
    Cycle_text_impl(Align align, Border bs=Border::INSET);
    Cycle_text_impl(const std::vector<ustring> & sv, Border bs=Border::INSET);
    Cycle_text_impl(const std::vector<ustring> & sv, Align align, Border bs=Border::INSET);

    int add(const ustring & s, Align align, const ustring & tooltip=ustring());
    int add(const ustring & s, const ustring & tooltip=ustring()) { return add(s, align_, tooltip); }
    int add(const std::vector<ustring> & sv, Align align=Align::CENTER);

    std::size_t remove(const ustring & text);
    std::size_t remove(int id);

    // Overrides Cycle_impl.
    void clear() override;

    int select(const ustring & s, bool fix=false);
    int select_similar(const ustring & s, bool fix=false);
    int setup(const ustring & s, bool fix=false);
    int setup_similar(const ustring & s, bool fix=false);

    ustring str() const;
    ustring str(int id) const;

    Widget_ptr widget(const ustring & s);
    Widget_cptr widget(const ustring & s) const;

    std::u32string wstr() const;
    std::u32string wstr(int id) const;

    std::vector<ustring> strings() const;
    std::vector<std::u32string> wstrings() const;

    void text_align(Align align);
    Align text_align() const noexcept { return align_; }

    void wrap(Wrap wm);
    Wrap wrap() const noexcept { return wrap_; }

    void allow_edit();
    void disallow_edit();
    bool editable() const noexcept { return editable_; }

    Widget_ptr edit();
    Widget_ptr editor();
    Widget_cptr editor() const;

    signal<void(const std::u32string &)> & signal_new_text()            { return signal_new_text_;      }
    signal<void(const std::u32string &)> & signal_remove_text()         { return signal_remove_text_;   }
    signal<void(const std::u32string &)> & signal_select_text()         { return signal_select_text_;   }
    signal<void(int, const std::u32string &)> & signal_activate_text()  { return signal_activate_text_; }
    signal<void(int, const std::u32string &)> & signal_text_changed()   { return signal_text_changed_;  }
    signal_all<const std::u32string &> & signal_validate()              { return signal_validate_;      }
    signal_all<const std::u32string &> & signal_approve()               { return signal_approve_;       }

    signal<void(int, const std::u32string &)> & signal_activate_edit()  { return signal_activate_edit_; }
    signal<void(int)> & signal_cancel_edit()                            { return signal_cancel_edit_;   }

private:

    using Holders = std::list<Entry_impl *>;
    Holders                 holders_;
    Align                   align_;
    Wrap                    wrap_           = Wrap::NONE;
    bool                    editable_       = false;
    Action                  action_edit_    { "Space"_tu };     // For internal use only.

    signal<void(const std::u32string &)>        signal_new_text_;
    signal<void(const std::u32string &)>        signal_remove_text_;
    signal<void(const std::u32string &)>        signal_select_text_;
    signal<void(int, const std::u32string &)>   signal_activate_text_;
    signal<void(int, const std::u32string &)>   signal_text_changed_;
    signal_all<const std::u32string &>          signal_validate_;
    signal_all<const std::u32string &>          signal_approve_;

    signal<void(int, const std::u32string &)>   signal_activate_edit_;
    signal<void(int)>                           signal_cancel_edit_;

    // Edit mode connections.
    connection              edit_disallow_cx_   { true };
    connection              edit_activate_cx_   { true };
    connection              edit_cancel_cx_     { true };
    connection              edit_focus_out_cx_  { true };

private:

    void init();
    void end_edit(int id);
    void on_select_id(int id);
    void on_changed(int id, const Entry_impl * entry);
    void on_edit_cancel(int id);
    bool on_mouse_double_click(int mbt, int mm, const Point & pt);
    bool on_mouse_down(int mbt, int mm, const Point & pt);
    void on_edit_activate(int id, const std::u32string &);
};

} // namespace tau

#endif // __TAU_CYCLE_TEXT_IMPL_HH__

// ----------------------------------------------------------------------------
// SPDX-License-Identifier: BSD-2-Clause
// Copyright © 2014-2024 Konstantin Shmelkov <mrcashe@gmail.com>.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
// THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
// PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
// CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
// EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
// PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
// OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
// WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
// OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
// EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// ----------------------------------------------------------------------------

#ifndef __TAU_LIST_TEXT_IMPL_HH__
#define __TAU_LIST_TEXT_IMPL_HH__

#include <list-impl.hh>
#include <entry-impl.hh>
#include <algorithm>

namespace tau {

class List_text_impl: public List_impl {
public:

    List_text_impl(Align align=Align::START);
    List_text_impl(unsigned xspacing, Align align=Align::START);
    List_text_impl(unsigned xspacing, unsigned yspacing, Align align=Align::START);
    List_text_impl(const std::vector<ustring> & sv, Align align=Align::START);
    List_text_impl(const std::vector<ustring> & sv, unsigned xspacing, Align align=Align::START);
    List_text_impl(const std::vector<ustring> & sv, unsigned xspacing, unsigned yspacing, Align align=Align::START);

    int         append(const ustring & str, bool shrink=false);
    int         append(const ustring & str, Align align, bool shrink=true);
    int         append(const std::vector<ustring> & sv, bool shrink=false);
    int         append(const std::vector<ustring> & sv, Align align, bool shrink=false);

    int         prepend(const ustring & str, bool shrink=false);
    int         prepend(const ustring & str, Align align, bool shrink=true);
    int         prepend(const std::vector<ustring> & sv, bool shrink=false);
    int         prepend(const std::vector<ustring> & sv, Align align, bool shrink=false);

    int         insert(const ustring & str, int row, bool shrink=false);
    int         insert(const ustring & str, int row, Align align, bool shrink=true);
    int         insert(const std::vector<ustring> & sv, int row, bool shrink=false);
    int         insert(const std::vector<ustring> & sv, int row, Align align, bool shrink=true);

    int         insert_before(const ustring & str, const ustring & other, bool shrink=false);
    int         insert_before(const ustring & str, const ustring & other, Align align, bool shrink=true);
    int         insert_before(const std::vector<ustring> & sv, const ustring & other, bool shrink=false);
    int         insert_before(const std::vector<ustring> & sv, const ustring & other, Align align, bool shrink=true);

    int         insert_after(const ustring & str, const ustring & other, bool shrink=false);
    int         insert_after(const ustring & str, const ustring & other, Align align, bool shrink=true);
    int         insert_after(const std::vector<ustring> & sv, const ustring & other, bool shrink=false);
    int         insert_after(const std::vector<ustring> & sv, const ustring & other, Align align, bool shrink=true);

    int         insert(int row, const ustring & str, int col, bool shrink=false);
    int         insert(int row, const ustring & str, int col, Align align, bool shrink=true);

    std::size_t remove(const ustring & s, bool similar=false);

    int         select(const ustring & str, bool fix=false);
    int         select_similar(const ustring & str, bool fix=false);
    ustring     str() const;
    std::u32string wstr() const;
    std::vector<ustring> strings() const;
    std::vector<std::u32string> wstrings() const;

    ustring     str(int row) const;
    std::u32string wstr(int row) const;
    int         find(const ustring & s, int ymin=INT_MIN+1, int ymax=INT_MAX) const;
    int         similar(const ustring & s, int ymin=INT_MIN+1, int ymax=INT_MAX) const;
    int         like(const ustring & s, int ymin=INT_MIN+1, int ymax=INT_MAX) const;
    bool        contains(const ustring & str, bool similar=false) const;

    void        text_align(Align align);
    Align       text_align() const noexcept { return align_; }
    void        wrap(Wrap wrap);
    Wrap        wrap() const noexcept { return wrap_; }

    void        allow_edit();
    void        disallow_edit();
    bool        editable() const noexcept { return editable_; }
    void        allow_edit(int row);
    void        disallow_edit(int row);
    bool        editable(int row) const noexcept;
    void        allow_edit(const ustring & text);
    void        disallow_edit(const ustring & text);
    bool        editable(const ustring & text) const noexcept;
    Widget_ptr  edit(int row);
    Widget_ptr  editor();
    Widget_cptr editor() const;

    signal<void(const std::u32string &)> & signal_select_text() { return signal_select_text_; }
    signal<void(const std::u32string &)> & signal_activate_text() { return signal_activate_text_; }
    signal<void(int, const std::u32string &)> & signal_remove_text() { return signal_remove_text_; }
    signal<void(int, const std::u32string &)> & signal_text_changed() { return signal_text_changed_; }
    signal_all<const std::u32string &> & signal_validate() { return signal_validate_; }
    signal_all<const std::u32string &> & signal_approve() { return signal_approve_; }

    signal<void(int, const std::u32string &)> & signal_activate_edit() { return signal_activate_edit_; }
    signal<void(int)> & signal_cancel_edit() { return signal_cancel_edit_; }

private:

    using Holders       = std::map<int, Entry_impl *>;
    Align               align_;
    Wrap                wrap_ = Wrap::NONE;
    Holders             holders_;
    bool                editable_ = false;

    Action              action_edit_ { "Space Enter", fun(this, &List_text_impl::on_edit) };

    signal<void(const std::u32string &)>            signal_select_text_;
    signal<void(const std::u32string &)>            signal_activate_text_;
    signal<void(int, const std::u32string &)>       signal_remove_text_;
    signal<void(int, const std::u32string &)>       signal_text_changed_;
    signal_all<const std::u32string &>              signal_validate_;
    signal_all<const std::u32string &>              signal_approve_;

    signal<void(int, const std::u32string &)>       signal_activate_edit_;
    signal<void(int)>                               signal_cancel_edit_;

    connection          edit_disallow_cx_           { true };
    connection          edit_activate_cx_           { true };
    connection          edit_cancel_cx_             { true };
    connection          edit_focus_out_cx_          { true };

private:

    void        init();
    Entry_ptr   new_holder(const std::u32string & ws);
    Widget_ptr  edit(Entry_impl * tp, int y);

    void        on_row_selected(int row);
    void        on_row_activated(int row);
    void        on_row_removed(int row);
    void        on_row_moved(int old_row, int new_row);

    void        on_entry_changed(Entry_impl * tp);
    void        end_edit();
    void        on_entry_cancel(int row);
    void        on_edit();
    void        on_parent(Entry_impl * ep, Object * op);
    void        on_children_changed();
};

} // namespace tau

#endif // __TAU_LIST_TEXT_IMPL_HH__

// ----------------------------------------------------------------------------
// SPDX-License-Identifier: BSD-2-Clause
// Copyright © 2014-2024 Konstantin Shmelkov <mrcashe@gmail.com>.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
// THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
// PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
// CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
// EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
// PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
// OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
// WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
// OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
// EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// ----------------------------------------------------------------------------

#ifndef __TAU_GLYPH_IMPL_HH__
#define __TAU_GLYPH_IMPL_HH__

#include <tau/geometry.hh>
#include <defs-impl.hh>
#include <cmath>
#include <iterator>

namespace tau {

struct Glyph_impl {
    using Contours = std::vector<Contour>;

    Vector      min_, max_;     // Bounds ((xmin:ymin), (xmax:ymax)).
    Vector      advance_;       // Advance x & y.
    Vector      bearing_;       // Bearing left (x) & top (y).
    Contours    contours_;
    Rect        bbox_;

    void set_bounds(const Point & top_left, const Point & bottom_right) {
        bbox_.set(top_left, bottom_right);
    }

    Rect bounds() const;

    void add_contour(const Contour & gctr) {
        if (!gctr.empty()) {
            contours_.push_back(gctr);
        }
    }

    void merge(const Glyph_impl & other) {
        std::copy(other.contours_.begin(), other.contours_.end(), std::back_inserter(contours_));
    }

    void transform(const Matrix & mat) {
        for (auto & ctr: contours_) {
            ctr.transform(mat);
        }
    }

    // Get scaled glyph.
    Glyph_ptr glyph(const Matrix & mat);

    // Perform automatic hinting.
    void hint();
};

} // namespace tau

#endif // __TAU_GLYPH_IMPL_HH__

// ----------------------------------------------------------------------------
// SPDX-License-Identifier: BSD-2-Clause
// Copyright © 2014-2024 Konstantin Shmelkov <mrcashe@gmail.com>.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
// THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
// PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
// CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
// EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
// PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
// OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
// WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
// OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
// EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// ----------------------------------------------------------------------------

#include <tau/exception.hh>
#include <tau/font.hh>
#include <brush-impl.hh>
#include <loop-impl.hh>
#include <pen-impl.hh>
#include "font-xcb.hh"
#include "painter-xcb.hh"
#include "pixmap-xcb.hh"
#include "winface-xcb.hh"
#include <cstring>
#include <numbers>
#include <iostream>

// ----------------------------------------------------------------------------
// ----------------------------------------------------------------------------

namespace tau {

Painter_xcb::Painter_xcb(Winface_xcb * wf):
    dp_(wf->xdp()),
    wf_(wf),
    cx_(wf->conn()),
    xid_(wf->xid()),
    xpicture_(wf->xpicture()),
    gc_(dp_, wf->xid())
{
    wstate().viewable_.set(wf->self()->size());
    wf->self()->signal_destroy().connect(fun(this, &Painter_xcb::on_window_destroy));
    select_font_priv(Font::normal());
    update_clip_priv();
    dp_->loop()->signal_quit().connect(fun(this, &Painter_xcb::on_display_quit));

    // Handle possible buffer pixmap change.
    wf->self()->signal_size_changed().connect(fun(this, &Painter_xcb::on_window_size));
}

Painter_xcb::Painter_xcb(Pixmap_xcb * pixmap):
    pixmap_(pixmap)
{
    if (pixmap_) {
        dp_ = pixmap_->display() ? pixmap_->display() : Display_xcb::this_xcb_display().get();
        pixmap_->set_display(dp_);
        cx_ = dp_->conn();
        pixmap_->signal_destroy().connect(fun(this, &Painter_xcb::on_pixmap_destroy));
        wstate().viewable_.set(pixmap_->size());
    }

    if (dp_) { dp_->loop()->signal_quit().connect(fun(this, &Painter_xcb::on_display_quit)); }
    select_font_priv(Font::normal());
    update_clip_priv();
}

void Painter_xcb::set_clip() {
    if (cx_) {
        gc_.flush();
        xcb_set_clip_rectangles(cx_, XCB_CLIP_ORDERING_UNSORTED, gc_.xid(), 0, 0, 1, &cr_);
        xcb_render_set_picture_clip_rectangles(cx_, xpicture_, 0, 0, 1, &cr_);
    }
}

void Painter_xcb::update_clip_priv() {
    Rect clip = wstate().viewable_;
    if (clip_ != clip) { cr_ = to_xcb_rectangle(clip); set_clip(); }
}

void Painter_xcb::begin_stroke() {
    gc_.set_foreground(state().pen_->color);
    double dlw = state().pen_->line_width*matrix().max_factor();
    uint8_t lw = dlw > 0.0 ? dlw : 1;;
    gc_.set_line_width(lw);
    Line lstyle = state().pen_->line_style;

    if (Line::DOT == lstyle) {
        const uint8_t dashes[] { lw, lw };
        gc_.set_line_style(XCB_LINE_STYLE_ON_OFF_DASH);
        gc_.set_dashes(0, sizeof dashes, dashes);
    }

    else if (Line::DASH == lstyle) {
        const uint8_t dashes[] { uint8_t(3*lw), lw };
        gc_.set_line_style(XCB_LINE_STYLE_ON_OFF_DASH);
        gc_.set_dashes(0, sizeof dashes, dashes);
    }

    else if (Line::DASH_DOT == lstyle) {
        const uint8_t dashes[] { uint8_t(3*lw), lw, lw, lw };
        gc_.set_line_style(XCB_LINE_STYLE_ON_OFF_DASH);
        gc_.set_dashes(0, sizeof dashes, dashes);
    }

    else if (Line::DASH_DOT_DOT == lstyle) {
        const uint8_t dashes[] { uint8_t(3*lw), lw, lw, lw, lw, lw };
        gc_.set_line_style(XCB_LINE_STYLE_ON_OFF_DASH);
        gc_.set_dashes(0, sizeof dashes, dashes);
    }

    else {
        gc_.set_line_style(XCB_LINE_STYLE_SOLID);
    }

    gc_.set_cap_style(xcb_cap_style(state().pen_->cap_style));
    gc_.set_join_style(xcb_join_style(state().pen_->join_style));
    gc_.set_func(gx_oper(state().op_));
    gc_.flush();
}

// Overrides pure Painter_impl.
void Painter_xcb::draw_pixmap(Pixmap_cptr pix, const Point & origin, const Size & size, const Point & pt, bool transparent) {
    if (Pixmap_xcb_cptr xpix = std::dynamic_pointer_cast<const Pixmap_xcb>(pix)) {
        if (pixmap_) {
            int dy = pt.y();

            for (int y = origin.y(); y < origin.y()+size.iheight(); ++y, ++dy) {
                int dx = pt.x();

                for (int x = origin.x(); x < origin.x()+size.iwidth(); ++x, ++dx) {
                    Color c = pix->get_pixel({ x, y });
                    if (!transparent || c.alpha() > 0.0) { pixmap_->put_pixel({ dx, dy }, c); }
                }
            }
        }

        else if (dp_) {
            auto ncpix = std::const_pointer_cast<Pixmap_xcb>(xpix);
            ncpix->set_display(dp_);
            xpix->draw(xid_, xpicture_, state().op_, origin, size, pt, transparent);
            damage();
        }
    }
}

// Overrides pure Painter_impl.
void Painter_xcb::fill_rectangles(const Rect * rs, std::size_t nrs, const Color & c) {
    if (pixmap_) {
        pixmap_->fill_rectangles(rs, nrs, c);
    }

    else if (cx_) {
        Rect dmg;
        xcb_rectangle_t xr[nrs], *p = xr;

        for (; nrs; --nrs, rs++) {
            if (*rs) {
                *p++ = to_xcb_rectangle(*rs);
                dmg |= *rs;
            }
        }

        if (p > xr) {
            gc_.set_foreground(c);
            gc_.set_func(gx_oper(state().op_));
            gc_.flush();
            xcb_poly_fill_rectangle(cx_, xid_, gc_.xid(), p-xr, xr);
            xcb_flush(cx_);
            damage(dmg);
        }
    }
}

// Overrides Painter_impl.
void Painter_xcb::fill_polygon(const Point * pts, std::size_t npts, const Color & color) {
    if (cx_) {
        int x1 = INT_MAX, x2 = INT_MIN, y1 = INT_MAX, y2 = INT_MIN;
        xcb_point_t xpts[npts];

        for (unsigned n = 0; n < npts; ++n) {
            xpts[n] = to_xcb_point(pts[n]);
            x1 = std::min(x1, pts[n].x()), x2 = std::max(x2, pts[n].x());
            y1 = std::min(y1, pts[n].y()), y2 = std::max(y2, pts[n].y());
        }

        gc_.set_foreground(color);
        gc_.set_func(gx_oper(state().op_));
        gc_.flush();
        xcb_fill_poly(cx_, xid_, gc_.xid(), XCB_POLY_SHAPE_COMPLEX, XCB_COORD_MODE_ORIGIN, npts, xpts);
        xcb_flush(cx_);
        damage(Rect(x1, y1, x2, y2));
    }
}

// Overrides Painter_impl.
void Painter_xcb::stroke_prim_text(const Prim_text & o) {
    if (dp_ && !wstate().viewable_.empty()) {
        if (Font_xcb_ptr fp = std::dynamic_pointer_cast<Font_xcb>(state().font_)) {
            Point pt(matrix()*o.pos);
            xcb_render_picture_t src = dp_->solid_fill(o.color);
            uint8_t op = xrender_oper(state().op_);
            set_clip();
            Point opt = pt-woffset();
            fp->render_glyphs(o.str, opt, op, src, xpicture_);
            damage({ opt.x(), opt.y()-fp->iascent(), Size(16384, fp->height()) });
        }
    }
}

// Overrides pure Painter_impl.
void Painter_xcb::stroke_rectangle(const Rect & r) {
    if (cx_) {
        xcb_rectangle_t xr = to_xcb_rectangle(r);
        begin_stroke();
        xcb_poly_rectangle(cx_, xid_, gc_.xid(), 1, &xr);
        xcb_flush(cx_);
        unsigned lw = gc_.line_width();
        damage(r.increased(lw, lw));
    }
}

// Overrides pure Painter_impl.
void Painter_xcb::stroke_polyline(const Point * pts, std::size_t npts) {
    if (cx_) {
        std::size_t n = npts;
        xcb_point_t xpts[npts];
        int x1 = INT_MAX, x2 = INT_MIN, y1 = INT_MAX, y2 = INT_MIN;

        for (xcb_point_t * p = xpts; n; n--, ++pts) {
            *p++ = to_xcb_point(*pts);
            x1 = std::min(x1, pts->x()), x2 = std::max(x2, pts->x());
            y1 = std::min(y1, pts->y()), y2 = std::max(y2, pts->y());
        }

        begin_stroke();
        xcb_poly_line(cx_, XCB_COORD_MODE_ORIGIN, xid_, gc_.xid(), npts, xpts);
        xcb_flush(cx_);
        unsigned lw = gc_.line_width();
        damage(Rect(x1, y1, x2, y2).increased(lw));
    }
}

// Overrides Painter_impl.
void Painter_xcb::stroke_prim_arc(const Prim_arc & o) {
    if (cx_) {
        if (matrix().identity()) {
            xcb_arc_t arc;
            arc.x = o.center.x()-woffset().x()-o.radius;
            arc.y = o.center.y()-woffset().y()-o.radius;
            arc.width = 2*o.radius;
            arc.height = 2*o.radius;
            constexpr double pi2 = 2.0*std::numbers::pi;
            double a1 = std::min(o.angle1, o.angle2); while (std::fabs(a1) > pi2) { a1 -= a1 > 0 ? pi2 : -pi2; }
            double a2 = std::max(o.angle1, o.angle2); while (std::fabs(a2) > pi2) { a2 -= a2 > 0 ? pi2 : -pi2; }
            arc.angle1 = 3666.93*a1;
            arc.angle2 = 3666.93*(a2-a1);
            begin_stroke();
            xcb_poly_arc(cx_, xid_, gc_.xid(), 1, &arc);
            xcb_flush(cx_);
            damage();
        }

        else {
            Painter_impl::stroke_prim_arc(o);
        }
    }
}

// Overrides Painter_impl.
void Painter_xcb::fill_prim_arc(const Prim_arc & o) {
    if (cx_) {
        if (matrix().identity()) {
            xcb_arc_t arc;
            arc.x = o.center.x()-woffset().x()-o.radius;
            arc.y = o.center.y()-woffset().y()-o.radius;
            arc.width = 2*o.radius;
            arc.height = 2*o.radius;
            constexpr double pi2 = 2.0*std::numbers::pi;
            double a1 = std::min(o.angle1, o.angle2); while (std::fabs(a1) > pi2) { a1 -= a1 > 0 ? pi2 : -pi2; }
            double a2 = std::max(o.angle1, o.angle2); while (std::fabs(a2) > pi2) { a2 -= a2 > 0 ? pi2 : -pi2; }
            arc.angle1 = 3666.93*a1;
            arc.angle2 = 3666.93*(a2-a1);
            gc_.set_foreground(state().brush_->color());
            gc_.set_func(gx_oper(state().op_));
            gc_.set_arc_mode(o.pie ? XCB_ARC_MODE_PIE_SLICE : XCB_ARC_MODE_CHORD);
            gc_.flush();
            xcb_poly_fill_arc(cx_, xid_, gc_.xid(), 1, &arc);
            xcb_flush(cx_);
            damage();
        }

        else {
            Painter_impl::fill_prim_arc(o);
        }
    }
}

// Overrides Painter_impl.
void Painter_xcb::fill_round_rectangle(const Prim_rect & o) {
    if (matrix().identity()) {
        gc_.set_foreground(state().brush_->color());
        gc_.set_func(gx_oper(state().op_));
        gc_.set_arc_mode(XCB_ARC_MODE_PIE_SLICE);
        gc_.flush();

        int r = o.radius;
        int x = o.v1.x()-woffset().x();
        int y = o.v1.y()-woffset().y();
        int w = o.v2.x()-o.v1.x();
        int h = o.v2.y()-o.v1.y();

        xcb_rectangle_t xr[3];
        xr[0].x = x+r;
        xr[0].y = y;
        xr[0].width = std::max(0, w-r-r);
        xr[0].height = std::min(h, r);

        xr[1].x = x+r;
        xr[1].y = y+h-r;
        xr[1].width = std::max(0, w-r-r);
        xr[1].height = std::min(h, r);

        xr[2].x = x;
        xr[2].y = y+r;
        xr[2].width = w;
        xr[2].height = h-r-r;

        xcb_arc_t arc[4];
        arc[0].x = x;
        arc[0].y = y;
        arc[0].width = r+r;
        arc[0].height = r+r;
        arc[0].angle1 = 3666.93*std::numbers::pi/2.0;
        arc[0].angle2 = 3666.93*std::numbers::pi/2.0;

        arc[1].x = x;
        arc[1].y = y+h-r-r;
        arc[1].width = r+r;
        arc[1].height = r+r;
        arc[1].angle1 = 3666.93*std::numbers::pi;
        arc[1].angle2 = 3666.93*std::numbers::pi/2.0;

        arc[2].x = x+w-r-r;
        arc[2].y = y;
        arc[2].width = r+r;
        arc[2].height = r+r;
        arc[2].angle1 = 0;
        arc[2].angle2 = 3666.93*std::numbers::pi/2.0;

        arc[3].x = x+w-r-r;
        arc[3].y = y+h-r-r;
        arc[3].width = r+r;
        arc[3].height = r+r;
        arc[3].angle1 = 3.0*3666.93*std::numbers::pi/2.0;
        arc[3].angle2 = 3666.93*std::numbers::pi/2.0;

        xcb_poly_fill_arc(cx_, xid_, gc_.xid(), 4, arc);
        xcb_poly_fill_rectangle(cx_, xid_, gc_.xid(), 3, xr);
        xcb_flush(cx_);
        damage({ x, y, Size(w, h) });
    }

    else {
        Painter_impl::fill_round_rectangle(o);
    }
}

// Overrides pure Painter.
void Painter_xcb::set_font(Font_ptr fp) {
    if (fp) {
        state().font_ = fp;
        state().fontspec_ = fp->spec_;
    }
}

// Overrides pure Painter.
Font_ptr Painter_xcb::select_font_priv(const ustring & spec) {
    if (dp_ && !Font::same(spec, state().fontspec_)) {
        state().font_ = dp_->font(spec);
        state().fontspec_ = spec;
    }

    return state().font_;
}

// Overrides pure Painter.
Vector Painter_xcb::text_size(const ustring & s) const {
    return text_size(std::u32string(s));
}

// Overrides pure Painter_impl.
Vector Painter_xcb::text_size(const std::u32string & ws) const {
    Vector v;

    if (!ws.empty()) {
        if (auto fp = font()) {
            v.y(std::ceil(fp->ascent_-fp->descent_+fp->linegap_));
            int w = 0;

            for (char32_t wc: ws) {
                if (auto gp = fp->glyph(wc)) {
                    w += std::ceil(gp->advance_.x());
                }
            }

            v.x(w);
        }
    }

    return v;
}

// Overrides pure Painter_impl.
std::vector<int> Painter_xcb::offsets(const ustring & s) const {
    return offsets(std::u32string(s));
}

// Overrides pure Painter_impl.
std::vector<int> Painter_xcb::offsets(const std::u32string & ws) const {
    std::vector<int> v(1+ws.size());
    int i = 0, pos = 0;

    if (auto fp = font()) {
        for (char32_t wc: ws) {
            if (auto gp = fp->glyph(wc)) {
                v[i++] = pos;
                pos += std::ceil(gp->advance_.x());
            }
        }
    }

    v.back() = pos;
    return v;
}

void Painter_xcb::damage(const Rect & dmg) {
    if (wf_) {
        wf_->damage(dmg);
    }
}

// Handle possible buffer pixmap change.
void Painter_xcb::on_window_size() {
    if (wf_) {
        xid_ = wf_->xid();
        xpicture_ = wf_->xpicture();
        gc_.assign(dp_, xid_);
    }
}

void Painter_xcb::on_window_destroy() {
    xpicture_ = XCB_NONE;
    xid_ = XCB_NONE;
}

} // namespace tau

//END

// ----------------------------------------------------------------------------
// SPDX-License-Identifier: BSD-2-Clause
// Copyright © 2014-2024 Konstantin Shmelkov <mrcashe@gmail.com>.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
// THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
// PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
// CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
// EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
// PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
// OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
// WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
// OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
// EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// ----------------------------------------------------------------------------

#ifndef __TAU_XCB_FONT_HH__
#define __TAU_XCB_FONT_HH__

#include <glyph-impl.hh>
#include <sys-impl.hh>
#include <Unix/font-unix.hh>
#include "display-xcb.hh"
#include <set>
#if __has_include(<memory_resource>)
#include <memory_resource>
#endif

namespace tau {

class Font_xcb: public Font_unix {
public:

    Font_xcb(Font_face_ptr fface, const ustring & spec, double size_pt, Display_xcb * dp);
   ~Font_xcb();

    void render_glyphs(const std::wstring & str, Point pt, uint8_t oper, xcb_render_picture_t src, xcb_render_picture_t dst);

private:

    Display_xcb *           dp_     = nullptr;
    xcb_connection_t *      cx_     = nullptr;
    xcb_render_glyphset_t   east_   = XCB_NONE;
    const uint8_t           format_ = 8;
    std::vector<uint8_t>    bits_;
    std::vector<xcb_render_glyphinfo_t> ginfos_;
#if __has_include(<memory_resource>)
    uint8_t                 mem_[16384];
    std::pmr::monotonic_buffer_resource pool_ { std::data(mem_), std::size(mem_) };
    std::pmr::set<uint32_t> chars_ { &pool_ };
#else
    std::set<uint32_t>      chars_;
#endif

private:

    void on_display_quit() { dp_ = nullptr; cx_ = nullptr; }
};

} // namespace tau

#endif // __TAU_XCB_FONT_HH__

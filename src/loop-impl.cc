// ----------------------------------------------------------------------------
// SPDX-License-Identifier: BSD-2-Clause
// Copyright © 2014-2024 Konstantin Shmelkov <mrcashe@gmail.com>.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
// THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
// PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
// CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
// EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
// PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
// OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
// WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
// OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
// EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// ----------------------------------------------------------------------------

#include <event-impl.hh>
#include <loop-impl.hh>
#include <timer-impl.hh>
#include <tau/event.hh>
#include <tau/exception.hh>
#include <tau/key-file.hh>
#include <tau/timer.hh>
#include <tau/timeval.hh>
#include <algorithm>
#include <iomanip>
#include <atomic>
#include <map>
#include <iostream>

namespace {

using Mutex = std::recursive_mutex;
using Loops = std::map<std::thread::id, tau::Loop_ptr>;

Mutex           smx_;
Loops           loops_;
std::atomic_int loopcnt_;

} // anonymous namespace

// ----------------------------------------------------------------------------
// ----------------------------------------------------------------------------

namespace tau {

// static
void Loop_impl::new_loop(Loop_ptr loop) {
    boot();
    std::unique_lock lock(smx_);
    loops_[std::this_thread::get_id()] = loop;
    loop->id_ = loopcnt_;

    for (auto & p: loops_) {
        if (loop != p.second) {
            p.second->signal_quit().connect(fun(loop, &Loop_impl::chk_kache));
        }
    }
}

// static
Loop_ptr Loop_impl::that_ptr(std::thread::id tid) {
    std::unique_lock lock(smx_);
    auto i = loops_.find(tid);
    return loops_.end() != i ? i->second : nullptr;
}

// ----------------------------------------------------------------------------
// ----------------------------------------------------------------------------

Loop_impl::Loop_impl() {
    signal_iterate_.connect(fun(this, &Loop_impl::on_iterate));
    signal_start_.connect(fun(this, &Loop_impl::chk_kache));
    taken_.reserve(512);
}

bool Loop_impl::on_iterate(const Slot_iterate & slot) {
    slot();
    return false;
}

Timer_ptr Loop_impl::make_timer(int time_ms, bool periodical) {
    if (taken_.empty()) {
        auto tp = std::make_shared<Timer_impl>(time_ms, periodical);
        tp->loop_ = this;
        tp->quit_cx_ = signal_quit().connect(fun(tp, &Timer_impl::on_loop_quit));
        return tp;
    }

    auto tp = taken_.back();
    taken_.pop_back();
    tp->time_ms_ = time_ms;
    tp->periodical_ = periodical;
    return tp;
}

void Loop_impl::take_timer(Timer_ptr tp) {
    if (tp.use_count() < 3) {
        tp->cx_.drop();
        if (tp->signal_alarm_.empty() && taken_.size() < taken_.capacity()) { taken_.push_back(tp); }
    }
}

void Loop_impl::start_timer(Timer_ptr tp) {
    if (runlevel_ >= 0 && !tp->running_) {
        tp->time_point_ = Timeval::future(1000*(tp->pause_ ? tp->rem_ : tp->time_ms_));
        tp->running_ = true;
        tp->pause_ = false;
        timers_.emplace(tp->time_point_, tp);
        earliest_ = timers_.begin()->first;
        if (top_ == tp) { top_.reset(); }
    }
}

void Loop_impl::stop_timer(Timer_ptr tp) {
    if (std::erase_if(timers_, [tp](auto & p) { return tp == p.second; })) {
        tp->running_ = false;
        earliest_ = timers_.empty() ? UINT64_MAX : timers_.begin()->first;
    }
}

connection Loop_impl::alarm(slot<void()> slot_alarm, int timeout_ms, bool periodical) {
    if (runlevel_ < 0) { throw user_error("Loop_impl::signal_alarm(): dead loop"); }

    // if (!periodical) {
    //     uint64_t timepoint = Timeval::future(1000*(timeout_ms));
    //
    //     for (auto i = timers_.lower_bound(timepoint); i != timers_.end() && i->first == timepoint; ++i) {
    //         if (!i->second->timer_ && !i->second->periodical_) {
    //             std::cout << i->second->signal_alarm_.size() << std::endl;
    //             return i->second->signal_alarm_.connect(slot_alarm);
    //         }
    //     }
    // }

    auto tp = make_timer(std::max(1, timeout_ms), periodical);
    start_timer(tp);
    tp->cx_ = tp->signal_alarm_.connect(slot_alarm);
    return tp->cx_;
}

void Loop_impl::outer_iterate() {
    uint64_t now = Timeval::now(), ts = std::max(now, std::min(std::min(earliest_, run_), idle_));
    int dts = std::max(1, int((ts-now)/1000));
    bool res = iterate(dts);
    now = Timeval::now();

    while (!timers_.empty() && now >= timers_.begin()->first) {
        auto i = timers_.begin();
        top_ = i->second;
        timers_.erase(i);
        top_->running_ = false;
        top_->signal_alarm_();

        // Prevent double timer insertion during signal emission.
        if (top_) {
            if (top_->periodical_ && !top_->signal_alarm_.empty()) { start_timer(top_); }
            else { take_timer(top_); }
            top_.reset();
        }
    }

    earliest_ = !timers_.empty() ? timers_.begin()->first : UINT64_MAX;

    if (!signal_run_.empty()) {
        signal_run_();
        now = Timeval::now();
        run_ = now+2000;
    }

    else {
        run_ = UINT64_MAX;
    }

    if (!res && ts >= idle_) {
        idle_ = now+uidle_;
        signal_idle_();
    }
}

void Loop_impl::run() {
    if (runlevel_ < 0) {
        ustring msg = "Loop_impl::run(): attempt to rerun dead loop";
        std::cerr << "** " << msg << std::endl;
        throw user_error(msg);
    }

    idle_ = Timeval::future(uidle_);
    int runlevel = ++runlevel_;
    if (1 == runlevel) { signal_iterate_(fun(signal_start_)); }
    auto s = fun(this, &Loop_impl::outer_iterate);

    while (runlevel_ >= runlevel) {
        signal_iterate_(s);
    }

    if (1 == runlevel) {
        runlevel_ = -1;
        if (1 == runlevel) { signal_iterate_(fun(signal_quit_)); }
        timers_.clear();
        taken_.clear();
        on_quit();
        std::unique_lock lock(smx_);
        loops_.erase(std::this_thread::get_id());
    }
}

void Loop_impl::chk_kache() {
    if (kache_.signal_changed().empty()) {
        kache_event_ = event();
        kache_event_->signal_ready().connect(fun(this, &Loop_impl::on_kache_changed));
        kache_cx_ = kache_.signal_changed().connect(fun(kache_event_, &Event_impl::emit));
        signal_quit_.connect(fun(this, &Loop_impl::done_kache));
        kache_.flush();
    }
}

void Loop_impl::done_kache() {
    kache_cx_.drop();
    kache_event_.reset();
    kache_.flush();
}

void Loop_impl::on_kache_changed() {
    alarm(fun(kache_, &Key_file::flush), 16954);
}

void Loop_impl::on_idle() {
    if (signal_watch_.empty()) {
        done_mounts();
        idle_cx_.drop();
    }
}

void Loop_impl::quit() {
    std::unique_lock lock(mmx_);
    if (runlevel_ > 0) { --runlevel_; }
}

signal<void(int, const ustring &)> & Loop_impl::signal_watch() {
    if (signal_watch_.empty()) { init_mounts(); idle_cx_ = signal_idle_.connect(fun(this, &Loop_impl::on_idle)); }
    return signal_watch_;
}

// static
std::size_t Loop_impl::count() noexcept {
    std::unique_lock lock(smx_);
    return std::count_if(loops_.begin(), loops_.end(), [](auto & p) { return p.second->alive(); });
}

// static
std::vector<Loop_ptr> Loop_impl::list() {
    std::unique_lock lock(smx_);
    std::vector<Loop_ptr> res(loops_.size());
    std::size_t n = 0;
    for (auto & p: loops_) { res[n++] = p.second; }
    return res;
}

} // namespace tau

//END

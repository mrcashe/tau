// ----------------------------------------------------------------------------
// SPDX-License-Identifier: BSD-2-Clause
// Copyright © 2014-2024 Konstantin Shmelkov <mrcashe@gmail.com>.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
// THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
// PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
// CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
// EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
// PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
// OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
// WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
// OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
// EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// ----------------------------------------------------------------------------

/// @file date.cc The Date class implementation.

#include <tau/date.hh>
#include <tau/locale.hh>
#include <tau/string.hh>
#include <tau/timeval.hh>
#include <algorithm>
#include <iomanip>
#include <set>

namespace tau {

Date::Date() noexcept {}

Date::Date(const ustring & s) {
    assign(s);
}

Date::Date(int y, int m, int d) noexcept {
    assign(y, m, d);
}

Date::operator bool() const noexcept {
    return has_year();
}

void Date::reset() noexcept {
    year_ = mon_ = day_ = 0;
}

bool Date::has_year() const noexcept {
    return 0 != year_;
}

bool Date::has_month() const noexcept {
    return 0 != mon_;
}

bool Date::has_day() const noexcept {
    return 0 != day_;
}

unsigned Date::year() const noexcept {
    return year_;
}

unsigned Date::month() const noexcept {
    return mon_;
}

unsigned Date::day() const noexcept {
    return day_;
}

bool Date::certain() const noexcept {
    return has_year() && has_month() && has_day();
}

bool Date::leap() const noexcept {
    return 0 == year_%4 && (0 != year_%100 || 0 == year_%400);
}

unsigned Date::year_days() const noexcept {
    return has_year() ? (leap() ? 366 : 365) : 0;
}

void Date::year(int y) noexcept {
    if (y > 0) { year_ = y; }
    else { reset(); }
}

void Date::month(int m) noexcept {
    mon_ = m > 0 && m < 13 ? m : 0;
}

unsigned Date::month_days() const {
    if (mon_) {
        if (std::set<int>( { 1, 3, 5, 7, 8, 10, 12 }).contains(mon_)) { return 31; }
        if (std::set<int>( { 4, 6, 9, 11 }).contains(mon_)) { return 30; }
        if (2 == mon_) { return leap() ? 29 : 28; }
    }

    return 0;
}

void Date::day(int d) noexcept {
    day_ = d && unsigned(d) <= month_days() ? d : 0;
}

void Date::assign(int y, int m, int d) noexcept {
    reset();
    year(y);
    month(m);
    day(d);
}

void Date::assign(const ustring & s) {
    // YYYY-mm-dd format always supported.
    if (2 <= std::count(s.begin(), s.end(), U'-')) {
        auto v = str_explode(s, '-');
        assign(std::atoi(v[0].data()), std::atoi(v[1].data()), std::atoi(v[2].data()));
    }

    // Use current locale settings.
    else {
        auto d_fmt = Locale().d_fmt(), sep = str_replace(d_fmt, "%Ymd", ""), fmt = str_replace(d_fmt, "%.-", "");

        if (!d_fmt.empty() && !sep.empty() && 3 == fmt.size()) {
            auto v = str_explode(s, sep[0]);

            if (3 == v.size()) {
                if ("dmY" == fmt) { std::reverse(v.begin(), v.end()); }
                assign(std::atoi(v[0].data()), std::atoi(v[1].data()), std::atoi(v[2].data()));
            }
        }
    }
}

ustring Date::str(const Locale & lc) const {
    if (operator bool()) {
        auto d_fmt = lc.d_fmt(), sep = str_replace(d_fmt, "%Ymd", ""), fmt = str_replace(d_fmt, "%.-", "");

        // Try to format using given locale.
        if (!d_fmt.empty() && !sep.empty() && 3 == fmt.size()) {
            if ("dmY" == fmt) {
                return str_format(std::setw(2), std::setfill('0'), day_)+sep[0]+str_format(std::setw(2), std::setfill('0'), mon_)+sep[0]+str_format(std::setw(4), std::setfill('0'), year_);
            }

            else {
                return str_format(std::setw(4), std::setfill('0'), year_)+sep[0]+str_format(std::setw(2), std::setfill('0'), mon_)+sep[0]+str_format(std::setw(2), std::setfill('0'), day_);
            }
        }

        // Default format is YYYY-mm-dd.
        else {
            return str_format(std::setw(4), std::setfill('0'), year_)+'-'+str_format(std::setw(2), std::setfill('0'), mon_)+'-'+str_format(std::setw(2), std::setfill('0'), day_);
        }
    }

    return ustring();
}

ustring Date::str() const {
    return str(Locale());
}

// static
Date Date::today() {
    return tau::Timeval::now().date();
}

// static
bool Date::validate(const std::u32string & ws) {
    if (!ws.empty()) {
        std::u32string d_fmt = Locale().d_fmt(), sep = str_replace(d_fmt, U"%Ymd", U"")+U'-', num = str_replace(ws, sep, U"");
        if (num.size() > 8) { return false; }
        if (U'0' > ws.front() || ws.front() > U'9') { return false; }
        if (2 < std::count_if(ws.begin(), ws.end(), [&sep](auto wc) { return sep.npos != sep.find(wc); })) { return false; }
        if (!str_is_numeric(num)) { return false; }
    }

    return true;
}

bool operator==(const Date & d1, const Date & d2) noexcept {
    return d1.certain() && d2.certain() && d1.year() == d2.year() && d1.month() == d2.month() && d1.day() == d2.day();
}

bool operator!=(const Date & d1, const Date & d2) noexcept {
    return d1.certain() && d2.certain() ? (d1.year() != d2.year() || d1.month() != d2.month() || d1.day() != d2.day()) : true;
}

} // namespace tau

//END

// ----------------------------------------------------------------------------
// SPDX-License-Identifier: BSD-2-Clause
// Copyright © 2014-2024 Konstantin Shmelkov <mrcashe@gmail.com>.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
// THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
// PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
// CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
// EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
// PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
// OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
// WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
// OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
// EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// ----------------------------------------------------------------------------

#include <tau/menu.hh>
#include <tau/toplevel.hh>
#include <pixmap-impl.hh>
#include <toplevel-impl.hh>
#include <menu-impl.hh>
#include <menu-item-impl.hh>

namespace tau {

#define MENU_IMPL (std::static_pointer_cast<Menu_impl>(impl))

Menu::Menu(Widget_ptr wp):
    Container(wp)
{
}

Menu & Menu::operator=(Widget_ptr wp) {
    Container::operator=(std::dynamic_pointer_cast<Menu_impl>(wp));
    return *this;
}

void Menu::append(Widget & w) {
    MENU_IMPL->append(w.ptr());
}

Action_menu_item Menu::append(Action & action, Align accel_align) {
    Action_menu_item item(action, accel_align);
    append(item);
    return item;
}

Toggle_menu_item Menu::append(Toggle_action & action) {
    Toggle_menu_item item(action);
    append(item);
    return item;
}

Toggle_menu_item Menu::append(Toggle_action & action, Align accel_align) {
    Toggle_menu_item item(action, accel_align);
    append(item);
    return item;
}

Toggle_menu_item Menu::append(Toggle_action & action, Check::Style check_style, Border border_style) {
    Toggle_menu_item item(action, check_style, border_style);
    append(item);
    return item;
}

Toggle_menu_item Menu::append(Toggle_action & action, Align accel_align, Check::Style check_style, Border border_style) {
    Toggle_menu_item item(action, accel_align, check_style, border_style);
    append(item);
    return item;
}

Slot_menu_item Menu::append(const ustring & label, const slot<void()> & slot_activate, const ustring & icon_name) {
    Slot_menu_item item(label, slot_activate, icon_name);
    append(item);
    return item;
}

Slot_menu_item Menu::append(const ustring & label, const slot<void()> & slot_activate, const Pixmap pix) {
    Slot_menu_item item(label, slot_activate, pix);
    append(item);
    return item;
}

Submenu_item Menu::append(const ustring & label, Menu & menu, const ustring & icon_name) {
    Submenu_item submenu(label, menu, icon_name);
    append(submenu);
    return submenu;
}

Submenu_item Menu::append(const ustring & label, Menu & menu, const Pixmap pix) {
    Submenu_item submenu(label, menu, pix);
    append(submenu);
    return submenu;
}

Separator Menu::append_separator(Separator::Style separator_style) {
    Separator sep(separator_style);
    append(sep);
    return sep;
}

void Menu::prepend(Widget & w) {
    MENU_IMPL->prepend(w.ptr());
}

Action_menu_item Menu::prepend(Action & action, Align accel_align) {
    Action_menu_item item(action, accel_align);
    prepend(item);
    return item;
}

Toggle_menu_item Menu::prepend(Toggle_action & action) {
    Toggle_menu_item item(action);
    prepend(item);
    return item;
}

Toggle_menu_item Menu::prepend(Toggle_action & action, Align accel_align) {
    Toggle_menu_item item(action, accel_align);
    prepend(item);
    return item;
}

Toggle_menu_item Menu::prepend(Toggle_action & action, Check::Style check_style, Border border_style) {
    Toggle_menu_item item(action, check_style, border_style);
    prepend(item);
    return item;
}

Toggle_menu_item Menu::prepend(Toggle_action & action, Align accel_align, Check::Style check_style, Border border_style) {
    Toggle_menu_item item(action, accel_align, check_style, border_style);
    prepend(item);
    return item;
}

Slot_menu_item Menu::prepend(const ustring & label, const slot<void()> & slot_activate, const ustring & icon_name) {
    Slot_menu_item item(label, slot_activate, icon_name);
    prepend(item);
    return item;
}

Slot_menu_item Menu::prepend(const ustring & label, const slot<void()> & slot_activate, const Pixmap pix) {
    Slot_menu_item item(label, slot_activate, pix);
    prepend(item);
    return item;
}

Submenu_item Menu::prepend(const ustring & label, Menu & menu, const ustring & icon_name) {
    Submenu_item submenu(label, menu, icon_name);
    prepend(submenu);
    return submenu;
}

Submenu_item Menu::prepend(const ustring & label, Menu & menu, const Pixmap pix) {
    Submenu_item submenu(label, menu, pix);
    prepend(submenu);
    return submenu;
}

Separator Menu::prepend_separator(Separator::Style separator_style) {
    Separator sep(separator_style);
    prepend(sep);
    return sep;
}

void Menu::insert_before(Widget & w, const Widget & other) {
    MENU_IMPL->insert_before(w.ptr(), other.ptr().get());
}

Action_menu_item Menu::insert_before(Action & action, const Widget & other, Align accel_align) {
    Action_menu_item item(action, accel_align);
    insert_before(item, other);
    return item;
}

Toggle_menu_item Menu::insert_before(Toggle_action & action, const Widget & other) {
    Toggle_menu_item item(action);
    insert_before(item, other);
    return item;
}

Toggle_menu_item Menu::insert_before(Toggle_action & action, Align accel_align, const Widget & other) {
    Toggle_menu_item item(action, accel_align);
    insert_before(item, other);
    return item;
}

Toggle_menu_item Menu::insert_before(Toggle_action & action, const Widget & other, Check::Style check_style, Border border_style) {
    Toggle_menu_item item(action, check_style, border_style);
    insert_before(item, other);
    return item;
}

Toggle_menu_item Menu::insert_before(Toggle_action & action, Align accel_align, const Widget & other, Check::Style check_style, Border border_style) {
    Toggle_menu_item item(action, accel_align, check_style, border_style);
    insert_before(item, other);
    return item;
}

Slot_menu_item Menu::insert_before(const ustring & label, const slot<void()> & slot_activate, const Widget & other, const ustring & icon_name) {
    Slot_menu_item item(label, slot_activate, icon_name);
    insert_before(item, other);
    return item;
}

Slot_menu_item Menu::insert_before(const ustring & label, const slot<void()> & slot_activate, const Widget & other, const Pixmap pix) {
    Slot_menu_item item(label, slot_activate, pix);
    insert_before(item, other);
    return item;
}

Submenu_item Menu::insert_before(const ustring & label, Menu & menu, const Widget & other, const ustring & icon_name) {
    Submenu_item item(label, menu, icon_name);
    insert_before(item, other);
    return item;
}

Submenu_item Menu::insert_before(const ustring & label, Menu & menu, const Widget & other, const Pixmap pix) {
    Submenu_item item(label, menu, pix);
    insert_before(item, other);
    return item;
}

Separator Menu::insert_separator_before(const Widget & other, Separator::Style separator_style) {
    Separator sep (separator_style);
    insert_before(sep, other);
    return sep;
}

void Menu::insert_after(Widget & w, const Widget & other) {
    MENU_IMPL->insert_after(w.ptr(), other.ptr().get());
}

Action_menu_item Menu::insert_after(Action & action, const Widget & other, Align accel_align) {
    Action_menu_item item(action, accel_align);
    insert_after(item, other);
    return item;
}

Toggle_menu_item Menu::insert_after(Toggle_action & action, Align accel_align, const Widget & other) {
    Toggle_menu_item item(action, accel_align);
    insert_after(item, other);
    return item;
}

Toggle_menu_item Menu::insert_after(Toggle_action & action, const Widget & other) {
    Toggle_menu_item item(action);
    insert_after(item, other);
    return item;
}

Toggle_menu_item Menu::insert_after(Toggle_action & action, Align accel_align, const Widget & other, Check::Style check_style, Border border_style) {
    Toggle_menu_item item(action, accel_align, check_style, border_style);
    insert_after(item, other);
    return item;
}

Toggle_menu_item Menu::insert_after(Toggle_action & action, const Widget & other, Check::Style check_style, Border border_style) {
    Toggle_menu_item item(action, check_style, border_style);
    insert_after(item, other);
    return item;
}

Slot_menu_item Menu::insert_after(const ustring & label, const slot<void()> & slot_activate, const Widget & other, const ustring & icon_name) {
    Slot_menu_item item(label, slot_activate, icon_name);
    insert_after(item, other);
    return item;
}

Slot_menu_item Menu::insert_after(const ustring & label, const slot<void()> & slot_activate, const Widget & other, const Pixmap pix) {
    Slot_menu_item item(label, slot_activate, pix);
    insert_after(item, other);
    return item;
}

Submenu_item Menu::insert_after(const ustring & label, Menu & menu, const Widget & other, const ustring & icon_name) {
    Submenu_item item(label, menu, icon_name);
    insert_after(item, other);
    return item;
}

Submenu_item Menu::insert_after(const ustring & label, Menu & menu, const Widget & other, const Pixmap pix) {
    Submenu_item item(label, menu, pix);
    insert_after(item, other);
    return item;
}

Separator Menu::insert_separator_after(const Widget & other, Separator::Style separator_style) {
    Separator sep(separator_style);
    insert_after(sep, other);
    return sep;
}

bool Menu::empty() const noexcept {
    return MENU_IMPL->empty();
}

void Menu::remove(Widget & w) {
    MENU_IMPL->remove(w.ptr().get());
}

void Menu::clear() {
    MENU_IMPL->clear();
}

void Menu::quit() {
    MENU_IMPL->pass_quit();
}

signal<void()> & Menu::signal_quit() {
    return MENU_IMPL->signal_quit();
}

// ----------------------------------------------------------------------------
// ----------------------------------------------------------------------------

#define MENU_ITEM_IMPL (std::static_pointer_cast<Menu_item_impl>(impl))

Menu_item::Menu_item(Widget_ptr wp):
    Widget(wp)
{
}

Menu_item & Menu_item::operator=(Widget_ptr wp) {
    Widget::operator=(std::dynamic_pointer_cast<Menu_item_impl>(wp));
    return *this;
}

ustring Menu_item::label() const {
    return MENU_ITEM_IMPL->str();
}

// ----------------------------------------------------------------------------
// ----------------------------------------------------------------------------

#define SUBMENU_IMPL (std::static_pointer_cast<Submenu_impl>(impl))

Submenu_item::Submenu_item(const Submenu_item & other):
    Menu_item(other.impl)
{
}

Submenu_item & Submenu_item::operator=(const Submenu_item & other) {
    Menu_item::operator=(other);
    return *this;
}

Submenu_item::Submenu_item(Submenu_item && other):
    Menu_item(other.impl)
{
}

Submenu_item & Submenu_item::operator=(Submenu_item && other) {
    Menu_item::operator=(other);
    return *this;
}

Submenu_item::Submenu_item(Widget_ptr wp):
    Menu_item(std::dynamic_pointer_cast<Submenu_impl>(wp))
{
}

Submenu_item & Submenu_item::operator=(Widget_ptr wp) {
    Menu_item::operator=(std::dynamic_pointer_cast<Submenu_impl>(wp));
    return *this;
}

Submenu_item::Submenu_item(const ustring & label, Menu & menu, const ustring & icon_name):
    Menu_item(std::make_shared<Submenu_impl>(label, std::static_pointer_cast<Menu_impl>(menu.ptr()), icon_name))
{
}

Submenu_item::Submenu_item(const ustring & label, const ustring & icon_name):
    Menu_item(std::make_shared<Submenu_impl>(label, icon_name))
{
}

Submenu_item::Submenu_item(const ustring & label, Menu & menu, const Pixmap pix):
    Menu_item(std::make_shared<Submenu_impl>(label, std::static_pointer_cast<Menu_impl>(menu.ptr()), pix.ptr()))
{
}

Submenu_item::Submenu_item(const ustring & label, const Pixmap pix):
    Menu_item(std::make_shared<Submenu_impl>(label, pix.ptr()))
{
}

void Submenu_item::set_label(const ustring & label) {
    MENU_ITEM_IMPL->assign(label);
}

void Submenu_item::set_icon(const ustring & icon_name, int icon_size) {
    SUBMENU_IMPL->assign_icon(icon_name, icon_size);
}

void Submenu_item::set_icon(const Pixmap pix) {
    SUBMENU_IMPL->assign_icon(pix.ptr());
}

void Submenu_item::set_menu(Menu & m) {
    SUBMENU_IMPL->set_menu(std::static_pointer_cast<Menu_impl>(m.ptr()));
}

Widget_ptr Submenu_item::menu() {
    return SUBMENU_IMPL->menu();
}

Widget_cptr Submenu_item::menu() const {
    return SUBMENU_IMPL->menu();
}

// ----------------------------------------------------------------------------
// ----------------------------------------------------------------------------

#define SLOT_MENU_IMPL (std::static_pointer_cast<Slot_menu_impl>(impl))

Slot_menu_item::Slot_menu_item(const Slot_menu_item & other):
    Menu_item(other.impl)
{
}

Slot_menu_item & Slot_menu_item::operator=(const Slot_menu_item & other) {
    Menu_item::operator=(other);
    return *this;
}

Slot_menu_item::Slot_menu_item(Slot_menu_item && other):
    Menu_item(other.impl)
{
}

Slot_menu_item & Slot_menu_item::operator=(Slot_menu_item && other) {
    Menu_item::operator=(other);
    return *this;
}

Slot_menu_item::Slot_menu_item(Widget_ptr wp):
    Menu_item(std::dynamic_pointer_cast<Slot_menu_impl>(wp))
{
}

Slot_menu_item & Slot_menu_item::operator=(Widget_ptr wp) {
    Menu_item::operator=(std::dynamic_pointer_cast<Slot_menu_impl>(wp));
    return *this;
}

Slot_menu_item::Slot_menu_item(const ustring & label, const slot<void()> & slot_activate, const ustring & image_name):
    Menu_item(std::make_shared<Slot_menu_impl>(label, slot_activate, image_name))
{
}

Slot_menu_item::Slot_menu_item(const ustring & label, const slot<void()> & slot_activate, const Pixmap pix):
    Menu_item(std::make_shared<Slot_menu_impl>(label, slot_activate, pix.ptr()))
{
}

void Slot_menu_item::set_label(const ustring & label) {
    MENU_ITEM_IMPL->assign(label);
}

void Slot_menu_item::set_icon(const ustring & icon_name, int icon_size) {
    SLOT_MENU_IMPL->assign_icon(icon_name, icon_size);
}

void Slot_menu_item::set_icon(const Pixmap pix) {
    SLOT_MENU_IMPL->assign_icon(pix.ptr());
}

// ----------------------------------------------------------------------------
// ----------------------------------------------------------------------------

#define ACTION_MENU_IMPL (std::static_pointer_cast<Action_menu_impl>(impl))

Action_menu_item::Action_menu_item(const ustring & label, std::string_view icon_name, Align accel_align):
    Menu_item(std::make_shared<Action_menu_impl>(label, icon_name, accel_align))
{
}

Action_menu_item::Action_menu_item(Master_action & action, Align accel_align):
    Menu_item(std::make_shared<Action_menu_impl>(action, accel_align))
{
}

Action_menu_item::Action_menu_item(Action & action, Align accel_align):
    Menu_item(std::make_shared<Action_menu_impl>(action, accel_align))
{
}

Action_menu_item::Action_menu_item(const Action_menu_item & other):
    Menu_item(other.impl)
{
}

Action_menu_item & Action_menu_item::operator=(const Action_menu_item & other) {
    Menu_item::operator=(other);
    return *this;
}

Action_menu_item::Action_menu_item(Action_menu_item && other):
    Menu_item(other.impl)
{
}

Action_menu_item & Action_menu_item::operator=(Action_menu_item && other) {
    Menu_item::operator=(other);
    return *this;
}

Action_menu_item::Action_menu_item(Widget_ptr wp):
    Menu_item(std::dynamic_pointer_cast<Action_menu_impl>(wp))
{
}

Action_menu_item & Action_menu_item::operator=(Widget_ptr wp) {
    Menu_item::operator=(std::dynamic_pointer_cast<Action_menu_impl>(wp));
    return *this;
}

void Action_menu_item::set_icon(const ustring & icon_name, int icon_size) {
    ACTION_MENU_IMPL->assign_icon(icon_name, icon_size);
}

void Action_menu_item::set_icon(const Pixmap pix) {
    ACTION_MENU_IMPL->assign_icon(pix.ptr());
}

void Action_menu_item::select(Action & action) {
    ACTION_MENU_IMPL->select(action);
}

// ----------------------------------------------------------------------------
// ----------------------------------------------------------------------------

#define TOGGLE_MENU_IMPL (std::static_pointer_cast<Toggle_menu_impl>(impl))

Toggle_menu_item::Toggle_menu_item(const ustring & label, std::string_view icon_name):
    Menu_item(std::make_shared<Toggle_menu_impl>(label, icon_name))
{
}

Toggle_menu_item::Toggle_menu_item(Master_action & master_action):
    Menu_item(std::make_shared<Toggle_menu_impl>(master_action))
{
}

Toggle_menu_item::Toggle_menu_item(Toggle_action & toggle_action):
    Menu_item(std::make_shared<Toggle_menu_impl>(toggle_action))
{
}

Toggle_menu_item::Toggle_menu_item(const ustring & label, std::string_view icon_name, Align accel_align):
    Menu_item(std::make_shared<Toggle_menu_impl>(label, icon_name, accel_align))
{
}

Toggle_menu_item::Toggle_menu_item(Master_action & master_action, Align accel_align):
    Menu_item(std::make_shared<Toggle_menu_impl>(master_action, accel_align))
{
}

Toggle_menu_item::Toggle_menu_item(Toggle_action & toggle_action, Align accel_align):
    Menu_item(std::make_shared<Toggle_menu_impl>(toggle_action, accel_align))
{
}

Toggle_menu_item::Toggle_menu_item(const ustring & label, Check::Style check_style, Border border_style):
    Menu_item(std::make_shared<Toggle_menu_impl>(label, check_style, border_style))
{
}

Toggle_menu_item::Toggle_menu_item(Master_action & master_action, Check::Style check_style, Border border_style):
    Menu_item(std::make_shared<Toggle_menu_impl>(master_action, check_style, border_style))
{
}

Toggle_menu_item::Toggle_menu_item(Toggle_action & toggle_action, Check::Style check_style, Border border_style):
    Menu_item(std::make_shared<Toggle_menu_impl>(toggle_action, check_style, border_style))
{
}

Toggle_menu_item::Toggle_menu_item(const ustring & label, Align accel_align, Check::Style check_style, Border border_style):
    Menu_item(std::make_shared<Toggle_menu_impl>(label, accel_align, check_style, border_style))
{
}

Toggle_menu_item::Toggle_menu_item(Toggle_action & toggle_action, Align accel_align, Check::Style check_style, Border border_style):
    Menu_item(std::make_shared<Toggle_menu_impl>(toggle_action, accel_align, check_style, border_style))
{
}

Toggle_menu_item::Toggle_menu_item(Master_action & master_action, Align accel_align, Check::Style check_style, Border border_style):
    Menu_item(std::make_shared<Toggle_menu_impl>(master_action, accel_align, check_style, border_style))
{
}

Toggle_menu_item::Toggle_menu_item(Widget_ptr wp):
    Menu_item(std::dynamic_pointer_cast<Toggle_menu_impl>(wp))
{
}

Toggle_menu_item::Toggle_menu_item(const Toggle_menu_item & other):
    Menu_item(other.impl)
{
}

Toggle_menu_item & Toggle_menu_item::operator=(const Toggle_menu_item & other) {
    Menu_item::operator=(other);
    return *this;
}

Toggle_menu_item::Toggle_menu_item(Toggle_menu_item && other):
    Menu_item(other.impl)
{
}

Toggle_menu_item & Toggle_menu_item::operator=(Toggle_menu_item && other) {
    Menu_item::operator=(other);
    return *this;
}

Toggle_menu_item & Toggle_menu_item::operator=(Widget_ptr wp) {
    Menu_item::operator=(std::dynamic_pointer_cast<Toggle_menu_impl>(wp));
    return *this;
}

void Toggle_menu_item::set_check_style(Check::Style check_style) {
    TOGGLE_MENU_IMPL->set_check_style(check_style);
}

Check::Style Toggle_menu_item::check_style() const noexcept {
    return TOGGLE_MENU_IMPL->check_style();
}

void Toggle_menu_item::set_border_style(Border border_style) {
    TOGGLE_MENU_IMPL->set_border_style(border_style);
}

Border Toggle_menu_item::border_style() const noexcept {
    return TOGGLE_MENU_IMPL->border_style();
}

void Toggle_menu_item::set_border_width(unsigned npx) {
    TOGGLE_MENU_IMPL->set_border_width(npx);
}

unsigned Toggle_menu_item::border_width() const noexcept {
    return TOGGLE_MENU_IMPL->border_width();
}

bool Toggle_menu_item::get() const noexcept {
    return TOGGLE_MENU_IMPL->get();
}

void Toggle_menu_item::select(Toggle_action & action) {
    TOGGLE_MENU_IMPL->select(action);
}

// ----------------------------------------------------------------------------
// ----------------------------------------------------------------------------

#define CHECK_MENU_IMPL (std::static_pointer_cast<Check_menu_impl>(impl))

Check_menu_item::Check_menu_item(const ustring & label, bool checked):
    Menu_item(std::make_shared<Check_menu_impl>(label, checked))
{
}

Check_menu_item::Check_menu_item(const Check_menu_item & other):
    Menu_item(other.impl)
{
}

Check_menu_item & Check_menu_item::operator=(const Check_menu_item & other) {
    Menu_item::operator=(other);
    return *this;
}

Check_menu_item::Check_menu_item(Check_menu_item && other):
    Menu_item(other.impl)
{
}

Check_menu_item & Check_menu_item::operator=(Check_menu_item && other) {
    Menu_item::operator=(other);
    return *this;
}

Check_menu_item::Check_menu_item(Widget_ptr wp):
    Menu_item(std::dynamic_pointer_cast<Check_menu_impl>(wp))
{
}

Check_menu_item & Check_menu_item::operator=(Widget_ptr wp) {
    Menu_item::operator=(std::dynamic_pointer_cast<Check_menu_impl>(wp));
    return *this;
}

Check_menu_item::Check_menu_item(const ustring & label, Check::Style check_style, bool checked):
    Menu_item(std::make_shared<Check_menu_impl>(label, check_style, checked))
{
}

Check_menu_item::Check_menu_item(const ustring & label, Border border_style, bool checked):
    Menu_item(std::make_shared<Check_menu_impl>(label, border_style, checked))
{
}

Check_menu_item::Check_menu_item(const ustring & label, Check::Style check_style, Border border_style, bool checked):
    Menu_item(std::make_shared<Check_menu_impl>(label, check_style, border_style, checked))
{
}

void Check_menu_item::set_label(const ustring & label) {
    CHECK_MENU_IMPL->assign(label);
}

void Check_menu_item::set_check_style(Check::Style check_style) {
    CHECK_MENU_IMPL->set_check_style(check_style);
}

Check::Style Check_menu_item::check_style() const noexcept {
    return CHECK_MENU_IMPL->check_style();
}

void Check_menu_item::set_border_style(Border border_style) {
    CHECK_MENU_IMPL->set_border_style(border_style);
}

Border Check_menu_item::border_style() const noexcept {
    return CHECK_MENU_IMPL->border_style();
}

void Check_menu_item::set_border_width(unsigned npx) {
    CHECK_MENU_IMPL->set_border_width(npx);
}

unsigned Check_menu_item::border_width() const noexcept {
    return CHECK_MENU_IMPL->border_width();
}

void Check_menu_item::check() {
    CHECK_MENU_IMPL->check();
}

void Check_menu_item::uncheck() {
    CHECK_MENU_IMPL->uncheck();
}

void Check_menu_item::toggle() {
    CHECK_MENU_IMPL->toggle();
}

void Check_menu_item::set(bool state) {
    CHECK_MENU_IMPL->set(state);
}

void Check_menu_item::setup(bool state) {
    CHECK_MENU_IMPL->setup(state);
}

bool Check_menu_item::checked() const noexcept {
    return CHECK_MENU_IMPL->checked();
}

void Check_menu_item::join(Check_menu_item & other) {
    CHECK_MENU_IMPL->join(std::static_pointer_cast<Check_menu_impl>(other.ptr()));
}

bool Check_menu_item::joined() const noexcept {
    return CHECK_MENU_IMPL->joined();
}

signal<void()> & Check_menu_item::signal_check() {
    return CHECK_MENU_IMPL->signal_check();
}

signal<void()> & Check_menu_item::signal_uncheck() {
    return CHECK_MENU_IMPL->signal_uncheck();
}

} // namespace tau

//END

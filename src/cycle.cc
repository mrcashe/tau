// ----------------------------------------------------------------------------
// SPDX-License-Identifier: BSD-2-Clause
// Copyright © 2014-2024 Konstantin Shmelkov <mrcashe@gmail.com>.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
// THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
// PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
// CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
// EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
// PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
// OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
// WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
// OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
// EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// ----------------------------------------------------------------------------

#include <tau/cycle.hh>
#include <cycle-impl.hh>

namespace tau {

#define CYCLE_IMPL (std::static_pointer_cast<Cycle_impl>(impl))

Cycle::Cycle(Border bs):
    Cycle_base(std::make_shared<Cycle_impl>(bs))
{
}

Cycle::Cycle(const Cycle & other):
    Cycle_base(other.impl)
{
}

Cycle & Cycle::operator=(const Cycle & other) {
    Cycle_base::operator=(other);
    return *this;
}

Cycle::Cycle(Cycle && other):
    Cycle_base(other.impl)
{
}

Cycle & Cycle::operator=(Cycle && other) {
    Cycle_base::operator=(other);
    return *this;
}

Cycle::Cycle(Widget_ptr wp):
    Cycle_base(std::dynamic_pointer_cast<Cycle_impl>(wp))
{
}

Cycle & Cycle::operator=(Widget_ptr wp) {
    Cycle_base::operator=(std::dynamic_pointer_cast<Cycle_impl>(wp));
    return *this;
}

int Cycle::add(Widget & w) {
    return CYCLE_IMPL->add(w.ptr());
}

std::size_t Cycle::remove(Widget & w) {
    return CYCLE_IMPL->remove(w.ptr().get());
}

std::size_t Cycle::remove(int id) {
    return CYCLE_IMPL->remove(id);
}

int Cycle::select(Widget & w, bool fix) {
    return CYCLE_IMPL->select(w.ptr().get(), fix);
}

int Cycle::select(int id, bool fix) {
    return CYCLE_IMPL->select(id, fix);
}

int Cycle::setup(Widget & w, bool fix) {
    return CYCLE_IMPL->setup(w.ptr().get(), fix);
}

int Cycle::setup(int id, bool fix) {
    return CYCLE_IMPL->setup(id, fix);
}

} // namespace tau

//END

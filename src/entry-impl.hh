// ----------------------------------------------------------------------------
// SPDX-License-Identifier: BSD-2-Clause
// Copyright © 2014-2024 Konstantin Shmelkov <mrcashe@gmail.com>.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
// THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
// PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
// CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
// EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
// PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
// OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
// WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
// OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
// EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// ----------------------------------------------------------------------------

/// @file entry-impl.hh The Entry_impl (wrapped Entry) class declaration.
/// Implementation is in entry-impl.cc file.

#ifndef __TAU_ENTRY_IMPL_HH__
#define __TAU_ENTRY_IMPL_HH__

#include <tau/action.hh>
#include <edit-impl.hh>
#include <frame-impl.hh>
#include <scroller-impl.hh>

namespace tau {

class Entry_impl: public Frame_impl {
public:

    Entry_impl(Border border_style=Border::INSET);
    Entry_impl(Align text_align, Border border_style=Border::INSET);
    Entry_impl(const ustring & s, Border border_style=Border::INSET);
    Entry_impl(const std::u32string & ws, Border border_style=Border::INSET);
    Entry_impl(const ustring & s, Align text_align, Border border_style=Border::INSET);
    Entry_impl(const std::u32string & ws, Align text_align, Border border_style=Border::INSET);
   ~Entry_impl();

    void allow_edit();
    void disallow_edit();
    bool editable() const noexcept;

    // Overrides Frame_impl.
    void set_border_style(Border bs) override;

    Border border_style() const noexcept { return border_left_style(); }
    void text_align(Align align);
    Align text_align() const noexcept;
    void wrap(Wrap wmode) { edit_->wrap(wmode); }
    Wrap wrap() const noexcept { return edit_->wrap(); }

    void assign(const ustring & s);
    void assign(const std::u32string & ws);

    ustring str() const { return edit_->str(); }
    std::u32string wstr() const { return edit_->wstr(); }

    bool empty() const noexcept { return edit_->empty(); }
    void clear() { edit_->clear(); }
    void select(std::size_t begin, std::size_t end);
    void select_all() { edit_->select_all(); }
    bool has_selection() const noexcept { return edit_->has_selection(); }
    void unselect();

    void move_to(std::size_t col);
    std::size_t caret() const;
    Size text_size(const ustring & s) { return edit_->text_size(s); }

    void append(Widget_ptr wp, bool shrink=false);
    Widget_ptr append(const ustring & text, unsigned margin_left=0, unsigned margin_right=0);
    Widget_ptr append(const ustring & text, const Color & color, unsigned margin_left=0, unsigned margin_right=0);
    void prepend(Widget_ptr wp, bool shrink=false);
    Widget_ptr prepend(const ustring & text, unsigned margin_left=0, unsigned margin_right=0);
    Widget_ptr prepend(const ustring & text, const Color & color, unsigned margin_left=0, unsigned margin_right=0);

    Action & action_cancel() noexcept { return edit_->action_cancel(); }
    Action & action_activate() noexcept { return action_enter_; }

    signal<void(const std::u32string &)> & signal_activate() { return signal_activate_; }
    signal<void()> & signal_changed() { return signal_changed_; }
    signal_all<const std::u32string &> & signal_validate();
    signal_all<const std::u32string &> & signal_approve();

private:

    Box_ptr         box_;
    Edit_ptr        edit_;
    Scroller_ptr    scroller_;
    std::u32string  shadow_;
    std::u32string  backup_;
    Buffer          buffer_;
    Action          action_enter_           { KC_ENTER, KM_NONE, fun(this, &Entry_impl::on_activate) };
    connection      changed_cx_             { true };

    signal<void()>                          signal_changed_;
    signal_all<const std::u32string &> *    signal_validate_ = nullptr;
    signal_all<const std::u32string &> *    signal_approve_ = nullptr;
    signal<void(const std::u32string &)>    signal_activate_;

private:

    void init(Align text_align);
    void init_box();
    bool approve(const std::u32string & ws);

    bool on_approve();
    void on_activate();
    void on_cancel();
    void on_buffer_changed();
    void on_border_changed();
    bool on_edit_mouse_down(int mbt, int mm, const Point & where);
    bool on_mouse_double_click(int mbt, int mm, const Point & where);
    void on_focus_in();
    void on_focus_out();
    void on_conf_radius();
};

} // namespace tau

#endif // __TAU_ENTRY_IMPL_HH__

// ----------------------------------------------------------------------------
// SPDX-License-Identifier: BSD-2-Clause
// Copyright © 2014-2024 Konstantin Shmelkov <mrcashe@gmail.com>.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
// THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
// PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
// CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
// EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
// PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
// OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
// WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
// OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
// EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// ----------------------------------------------------------------------------

#include <tau/file.hh>
#include <tau/key-file.hh>
#include <tau/sys.hh>
#include <xcb/cursor-xcb.hh>
#include <xcb/pixmap-xcb.hh>
#include <Unix/loop-unix.hh>
#include <Unix/sys-unix.hh>
#include <sys/utsname.h>
#include <iostream>

namespace tau {

ustring path_self() {
    if (auto lnk = File("/proc/self/exe").read_link()) { return *lnk; }
    return path_build(path_cwd(), "a.out");
}

// Declared in sys-impl.hh.
Mounts list_mounts() {
    Mounts mounts;
    std::ifstream is("/proc/self/mounts");

    if (is.good()) {
        std::map<ustring, ustring> devs;
        ustring where_root;
        char s[2048];

        while (!is.eof()) {
            is.getline(s, sizeof(s)-1);
            auto v = str_explode(s, ' ');

            if (v.size() > 1 && v[0].starts_with("/dev/")) {
                if ("/" == v[1]) { where_root = v[0]; }
                devs.try_emplace(v[0], v[1]);
            }
        }

        for (auto & pair: devs) {
            Mount mnt;
            mnt.dev = pair.first;
            mnt.mpoint = pair.second;

            if (pair.first != where_root && pair.first.starts_with("/dev/")) {
                ustring file = path_notdir(pair.first);
                while (!file.empty() && std::isdigit(file.back())) { file.erase(file.size()-1); }
                ustring p = path_build("/sys/block", file);

                if (file_is_dir(p)) {
                    std::ifstream iss(path_build(p, "removable"));

                    if (iss.good()) {
                        char ss[16];
                        iss.getline(ss, 15);
                        mnt.removable = 0 != std::atoi(ss);
                    }
                }
            }

            mounts.push_back(mnt);
        }
    }

    return mounts;
}

// Declared in sys-impl.hh.
void boot_sys() {
    boot_unix();

    if (sysinfo_.uname != kache_.get_string("uname")) {
        kache_.set_string("uname", sysinfo_.uname);
        Key_file k("/etc/lsb-release");
        sysinfo_.distrib = k.get_string("DISTRIB_ID", "Linux");
        kache_.set_string("DISTRIB_ID", sysinfo_.distrib);
        const ustring rel = k.get_string("DISTRIB_RELEASE");
        kache_.set_string("DISTRIB_RELEASE", rel);
        auto v = str_explode(rel, '.');
        if (v.size() > 0) { sysinfo_.distrib_major = std::atoi(v[0].c_str()); }
        if (v.size() > 1) { sysinfo_.distrib_minor = std::atoi(v[1].c_str()); }
        sysinfo_.distrib_codename = k.get_string("DISTRIB_CODENAME");
        kache_.set_string("DISTRIB_CODENAME", sysinfo_.distrib_codename);
        sysinfo_.distrib_description = k.get_string("DISTRIB_DESCRIPTION");
        kache_.set_string("DISTRIB_DESCRIPTION", sysinfo_.distrib_description);
    }

    else {
        sysinfo_.distrib = kache_.get_string("DISTRIB_ID", "Linux");
        auto v = str_explode(kache_.get_string("DISTRIB_RELEASE"), '.');
        if (v.size() > 0) { sysinfo_.distrib_major = std::atoi(v[0].c_str()); }
        if (v.size() > 1) { sysinfo_.distrib_minor = std::atoi(v[1].c_str()); }
        sysinfo_.distrib_codename = kache_.get_string("DISTRIB_CODENAME");
        sysinfo_.distrib_description = kache_.get_string("DISTRIB_DESCRIPTION");
    }

    struct utsname un;

    if (0 == uname(&un)) {
        std::string s = un.release;
        auto i = s.find_first_not_of("0123456789.");
        if (i != s.npos) { s.erase(i); }
        auto v = str_explode(s, '.');
        if (!v.empty()) { sysinfo_.osmajor = std::atoi(v[0].data()); }
        if (v.size() > 1) { sysinfo_.osminor = std::atoi(v[1].data()); }
    }

    sysinfo_.cores = sysconf(_SC_NPROCESSORS_ONLN);
}

// static
Pixmap_ptr Pixmap_impl::create(int depth, const Size & sz) {
    return std::make_shared<Pixmap_xcb>(depth, sz);
}

// static
Cursor_ptr Cursor_impl::create() {
    return std::make_shared<Cursor_xcb>();
}

} // namespace tau

//END

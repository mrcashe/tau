// ----------------------------------------------------------------------------
// SPDX-License-Identifier: BSD-2-Clause
// Copyright © 2014-2024 Konstantin Shmelkov <mrcashe@gmail.com>.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
// THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
// PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
// CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
// EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
// PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
// OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
// WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
// OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
// EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// ----------------------------------------------------------------------------

/// @file watcher-linux.cc The Watcher_linux class implementation.

#include "watcher-linux.hh"
#include <fcntl.h>
#include <cstring>
#include <iostream>

namespace tau {

Watcher_linux::~Watcher_linux() {
    if (mnt_poller_) { delete mnt_poller_; }
    if (-1 != mntfd_) { close(mntfd_); }
}

// Overrides Watcher_impl.
void Watcher_linux::init() {
    init_pro();
    mounts_ = list_mounts();
    mntfd_ = open("/proc/self/mounts", O_RDONLY, 0);

    if (mntfd_ < 0) {
        std::cerr << "** Loop_linux: unable to init mounts: " << strerror(errno) << std::endl;
    }

    else {
        mnt_poller_ = new Poller_unix(mntfd_);
        mnt_poller_->signal_poll().connect(fun(this, &Watcher_linux::on_mounts));
        std::static_pointer_cast<Loop_unix>(lp_)->add_poller(mnt_poller_, POLLERR|POLLPRI);
    }
}

// static
Watcher_ptr Watcher_impl::create() {
    return std::make_shared<Watcher_linux>();
}

} // namespace tau

//END

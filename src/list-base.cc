// ----------------------------------------------------------------------------
// SPDX-License-Identifier: BSD-2-Clause
// Copyright © 2014-2024 Konstantin Shmelkov <mrcashe@gmail.com>.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
// THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
// PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
// CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
// EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
// PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
// OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
// WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
// OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
// EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// ----------------------------------------------------------------------------

#include <tau/list.hh>
#include <list-impl.hh>

#define LIST_IMPL (std::static_pointer_cast<List_impl>(impl))

namespace tau {

List_base::List_base(Widget_ptr wp):
    Container(wp)
{
}

List_base & List_base::operator=(Widget_ptr wp) {
    Container::operator=(std::dynamic_pointer_cast<List_impl>(wp));
    return *this;
}

int List_base::select(int row) {
    return LIST_IMPL->select(row);
}

int List_base::select_next() {
    return LIST_IMPL->select_next();
}

int List_base::select_previous() {
    return LIST_IMPL->select_previous();
}

int List_base::select_front() {
    return LIST_IMPL->select_front();
}

int List_base::select_back() {
    return LIST_IMPL->select_back();
}

int List_base::current() const noexcept {
    return LIST_IMPL->current();
}

std::vector<int> List_base::selection() const {
    return LIST_IMPL->selection();
}

bool List_base::has_selection() const noexcept {
    return LIST_IMPL->has_selection();
}

Span List_base::span() const noexcept {
    return LIST_IMPL->span();
}

bool List_base::empty() const noexcept {
    return LIST_IMPL->empty();
}

std::size_t List_base::count() const noexcept {
    return LIST_IMPL->count();
}

void List_base::clear() {
    LIST_IMPL->clear();
}

void List_base::unselect() {
    LIST_IMPL->unselect();
}

bool List_base::fixed() const noexcept {
    return LIST_IMPL->fixed();
}

void List_base::unfix() {
    LIST_IMPL->unfix();
}

void List_base::allow_multiple_select() noexcept {
    LIST_IMPL->allow_multiple_select();
}

void List_base::disallow_multiple_select() noexcept {
    LIST_IMPL->disallow_multiple_select();
}

bool List_base::multiple_select_allowed() const noexcept {
    return LIST_IMPL->multiple_select_allowed();
}

void List_base::set_column_spacing(unsigned spacing) {
    LIST_IMPL->set_column_spacing(spacing);
}

void List_base::set_row_spacing(unsigned spacing) {
    LIST_IMPL->set_row_spacing(spacing);
}

std::pair<unsigned, unsigned> List_base::spacing() const noexcept {
    return LIST_IMPL->spacing();
}

void List_base::align_column(int x, Align xalign) {
    LIST_IMPL->align_column(x, xalign);
}

Align List_base::column_align(int x) const noexcept {
    return LIST_IMPL->column_align(x);
}

void List_base::unalign_column(int x) {
    LIST_IMPL->unalign_column(x);
}

void List_base::align_row(int y, Align yalign) {
    LIST_IMPL->align_row(y, yalign);
}

Align List_base::row_align(int y) const noexcept {
    return LIST_IMPL->row_align(y);
}

void List_base::unalign_row(int y) {
    LIST_IMPL->unalign_row(y);
}

void List_base::set_column_width(int column, unsigned width) {
    LIST_IMPL->set_column_width(column, width);
}

unsigned List_base::column_width(int column) const {
    return LIST_IMPL->column_width(column);
}

void List_base::set_row_height(int row, unsigned height) {
    LIST_IMPL->set_row_height(row, height);
}

unsigned List_base::row_height(int row) const noexcept {
    return LIST_IMPL->row_height(row);
}

void List_base::set_min_column_width(int column, unsigned width) {
    LIST_IMPL->set_min_column_width(column, width);
}

unsigned List_base::min_column_width(int column) const noexcept {
    return LIST_IMPL->min_column_width(column);
}

void List_base::set_min_row_height(int row, unsigned height) {
    LIST_IMPL->set_min_row_height(row, height);
}

unsigned List_base::min_row_height(int row) const noexcept {
    return LIST_IMPL->min_row_height(row);
}

void List_base::set_max_column_width(int column, unsigned width) {
    LIST_IMPL->set_max_column_width(column, width);
}

unsigned List_base::max_column_width(int column) const noexcept {
    return LIST_IMPL->max_column_width(column);
}

void List_base::set_max_row_height(int row, unsigned height) {
    LIST_IMPL->set_max_row_height(row, height);
}

unsigned List_base::max_row_height(int row) const noexcept {
    return LIST_IMPL->max_row_height(row);
}

void List_base::set_column_margin(int x, unsigned left, unsigned right) {
    LIST_IMPL->set_column_margin(x, left, right);
}

void List_base::set_column_margin(int x, const Margin & margin) {
    LIST_IMPL->set_column_margin(x, margin);
}

void List_base::set_row_margin(int y, unsigned top, unsigned bottom) {
    LIST_IMPL->set_row_margin(y, top, bottom);
}

void List_base::set_row_margin(int y, const Margin & margin) {
    LIST_IMPL->set_row_margin(y, margin);
}

Margin List_base::column_margin(int x) const noexcept {
    return LIST_IMPL->column_margin(x);
}

Margin List_base::row_margin(int y) const noexcept {
    return LIST_IMPL->row_margin(y);
}

Margin List_base::columns_margin() const noexcept {
    return LIST_IMPL->columns_margin();
}

Margin List_base::rows_margin() const noexcept {
    return LIST_IMPL->rows_margin();
}

Rect List_base::column_bounds(int col) const noexcept {
    return LIST_IMPL->column_bounds(col);
}

Rect List_base::row_bounds(int row) const noexcept {
    return LIST_IMPL->row_bounds(row);
}

std::list<Widget_ptr> List_base::widgets(int row) const {
    return LIST_IMPL->widgets(row);
}

Widget_ptr List_base::widget_at(int row, int col) noexcept {
    return LIST_IMPL->widget_at(row, col);
}

Widget_cptr List_base::widget_at(int row, int col) const noexcept {
    return LIST_IMPL->widget_at(row, col);
}

Action & List_base::action_cancel() {
    return LIST_IMPL->action_cancel();
}

Action & List_base::action_activate() {
    return LIST_IMPL->action_activate();
}

Action & List_base::action_previous() {
    return LIST_IMPL->action_previous();
}

Action & List_base::action_next() {
    return LIST_IMPL->action_next();
}

Action & List_base::action_previous_page() {
    return LIST_IMPL->action_previous_page();
}

Action & List_base::action_next_page() {
    return LIST_IMPL->action_next_page();
}

Action & List_base::action_home() {
    return LIST_IMPL->action_home();
}

Action & List_base::action_end() {
    return LIST_IMPL->action_end();
}

Action & List_base::action_select_previous() {
    return LIST_IMPL->action_select_previous();
}

Action & List_base::action_select_next() {
    return LIST_IMPL->action_select_next();
}

Action & List_base::action_select_previous_page() {
    return LIST_IMPL->action_select_previous_page();
}

Action & List_base::action_select_next_page() {
    return LIST_IMPL->action_select_next_page();
}

Action & List_base::action_select_home() {
    return LIST_IMPL->action_select_home();
}

Action & List_base::action_select_end() {
    return LIST_IMPL->action_select_end();
}

Action & List_base::action_select_all() {
    return LIST_IMPL->action_select_all();
}

signal<void(int)> & List_base::signal_select_row() {
    return LIST_IMPL->signal_select_row();
}

signal<void(int)> & List_base::signal_activate_row() {
    return LIST_IMPL->signal_activate_row();
}

signal<void(int)> & List_base::signal_remove_row() {
    return LIST_IMPL->signal_remove_row();
}

signal<void(int, int)> & List_base::signal_move_row() {
    return LIST_IMPL->signal_move_row();
}

signal_all<int> & List_base::signal_mark_validate() {
    return LIST_IMPL->signal_mark_validate();
}

signal<void(int)> & List_base::signal_column_bounds_changed() {
    return LIST_IMPL->signal_column_bounds_changed();
}

} // namespace tau

//END

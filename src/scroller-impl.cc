// ----------------------------------------------------------------------------
// SPDX-License-Identifier: BSD-2-Clause
// Copyright © 2014-2024 Konstantin Shmelkov <mrcashe@gmail.com>.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
// THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
// PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
// CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
// EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
// PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
// OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
// WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
// OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
// EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// ----------------------------------------------------------------------------

/// @file scroller-impl.cc The Scroller implementation (Scroller_impl).
/// Headers are:
/// - scroller.hh
/// - scroller-impl.hh

#include <scroller-impl.hh>
#include <theme-impl.hh>
#include <window-impl.hh>
#include <iostream>

namespace tau {

Scroller_impl::Scroller_impl() {
    signal_arrange_.connect(fun(this, &Scroller_impl::arrange));
    signal_size_changed_.connect(fun(this, &Scroller_impl::on_size_changed));
    gravity_cx_ = signal_size_changed_.connect(fun(this, &Scroller_impl::check_gravity));
    signal_pan_size_changed_.connect(fun(this, &Scroller_impl::on_pan_size_changed));
    signal_display_in_.connect(fun(this, &Scroller_impl::on_display_in));
    signal_take_focus_.connect(fun(this, &Scroller_impl::on_take_focus));

    // Connect these signals after to allow child to handle them.
    signal_mouse_down().connect(fun(this, &Scroller_impl::on_mouse_down));
    signal_mouse_wheel().connect(fun(this, &Scroller_impl::on_mouse_wheel));

    auto theme = Theme_impl::root();
    action_pan_left_.set_master_action(theme->find_action("pan-left"));
    action_pan_right_.set_master_action(theme->find_action("pan-right"));
    action_pan_up_.set_master_action(theme->find_action("pan-up"));
    action_pan_down_.set_master_action(theme->find_action("pan-down"));

    connect_action(action_pan_left_);
    connect_action(action_pan_right_);
    connect_action(action_pan_up_);
    connect_action(action_pan_down_);
    connect_action(action_previous_page_);
    connect_action(action_next_page_);
    connect_action(action_home_);
    connect_action(action_end_);
}

void Scroller_impl::insert(Widget_ptr wp) {
    chk_parent(wp);
    clear();
    cp_ = wp.get();
    cx_ = wp->signal_hints_changed().connect(fun(this, &Scroller_impl::on_hints));
    make_child(wp);
    update_child_bounds(cp_, INT_MIN, INT_MIN);
    on_hints(Hints::SIZE);
    queue_arrange();
}

void Scroller_impl::clear() {
    if (auto wp = cp_) {
        cx_.drop();
        cp_ = nullptr;
        tracked_cx_.drop();
        unparent({ wp });
        update_child_bounds(wp, INT_MIN, INT_MIN);
        on_hints(Hints::SIZE);
        invalidate();
    }
}

void Scroller_impl::update_requisition() {
    Size req;

    if (cp_ && !cp_->hidden()) {
        req = cp_->required_size();
        req.update(cp_->size_hint(), true);
        req.update_max(cp_->min_size_hint());
        req.update_min(cp_->max_size_hint(), true);
    }

    if (!in_shutdown() && require_size(req)) {
        signal_pan_size_changed_();
    }
}

void Scroller_impl::on_hints(Hints op) {
    if (!in_shutdown()) {
        if (Hints::SHOW == op) {
            pan(0, 0);
            update_requisition();
            queue_arrange();
        }

        else if (Hints::HIDE == op) {
            update_child_bounds(cp_, INT_MIN, INT_MIN);
            pan(0, 0);
            update_requisition();
            queue_arrange();
        }

        else {
            update_requisition();
        }
    }
}

void Scroller_impl::arrange() {
    if (cp_ && !cp_->hidden() && update_child_bounds(cp_, Point(), size())) { invalidate(); }
    gravity_cx_.unblock();
}

// Gravity connection blocked during action execution.
// arrange() call unblocks that connection at exit.
void Scroller_impl::check_gravity() {
    Size max = pan_size()-size();
    int xm = max.iwidth(), ym = max.iheight();
    Point p = pan_;

    switch (gravity_) {
        case Gravity::LEFT:             p.set(0, ym/2);     break;
        case Gravity::RIGHT:            p.set(xm, ym/2);    break;
        case Gravity::TOP:              p.set(xm/2, 0);     break;
        case Gravity::BOTTOM:           p.set(xm/2, ym);    break;
        case Gravity::TOP_RIGHT:        p.set(xm, 0);       break;
        case Gravity::BOTTOM_LEFT:      p.set(0, ym);       break;
        case Gravity::BOTTOM_RIGHT:     p.set(xm, ym);      break;
        case Gravity::CENTER:           p.set(xm/2, ym/2);  break;
        case Gravity::TOP_LEFT:                             return;
    }

    p.set(std::min(p.x(), xm), std::min(p.y(), ym));
    update_pan(p.x(), p.y());
}

void Scroller_impl::check_pan() {
    if (pan_ == pending_) {
        update_pan(pan_.x(), pan_.y());
    }
}

void Scroller_impl::on_timer() {
    if (cp_ && !in_shutdown()) {
        Size max = pan_size()-size();
        pending_.set(std::clamp(pending_.x(), 0, max.iwidth()), std::clamp(pending_.y(), 0, max.iheight()));
        if (pan_.update(pending_)) { signal_pan_changed_(); }
    }
}

void Scroller_impl::update_pan(int x, int y) {
    Size max = pan_size()-size();
    pending_.set(std::clamp(x, 0, max.iwidth()), std::clamp(y, 0, max.iheight()));
    if (pending_ != pan_) { tmr_.start(PAN_TIMEOUT); }
}

void Scroller_impl::pan(const Point & pt) {
    if (has_display()) { update_pan(pt.x(), pt.y()); }
    else { deferred_ = pt; }
}

void Scroller_impl::gravity(Gravity g) {
    if (gravity_ != g) {
        gravity_ = g;
        check_pan();
    }
}

Size Scroller_impl::pan_size() const noexcept {
    return cp_ ? cp_->required_size() : Size();
}

Point Scroller_impl::pan() const noexcept {
    return cp_ ? pan_ : Point();
}

void Scroller_impl::pan(int x, int y) {
    pan(Point(x, y));
}

void Scroller_impl::pan_x(int x) {
    pan(x, pan_.y());
}

void Scroller_impl::pan_y(int y) {
    pan(pan_.x(), y);
}

void Scroller_impl::pan_left() {
    if (pan_.x() > 0) {
        int stp = 0 != step_.x() ? step_.x() : logical_size().width()/32, d = std::min(stp, pan_.x());
        gravity_cx_.block();
        pan_x(pan_.x()-d);
    }
}

void Scroller_impl::pan_right() {
    Size max = pan_size()-size();

    if (0 != max.width()) {
        int stp = 0 != step_.x() ? step_.x() : logical_size().width()/32, d = std::min(stp, max.iwidth());
        gravity_cx_.block();
        pan_x(pan_.x()+d);
    }
}

void Scroller_impl::pan_up() {
    if (pan_.y() > 0) {
        int stp = 0 != step_.y() ? step_.y() : logical_size().height()/32, d = std::min(stp, pan_.y());
        gravity_cx_.block();
        pan_y(pan_.y()-d);
    }
}

void Scroller_impl::pan_down() {
    Size max = pan_size()-size();

    if (0 != max.height()) {
        int stp = 0 != step_.y() ? step_.y() : logical_size().height()/32, d = std::min(stp, max.iheight());
        gravity_cx_.block();
        pan_y(pan_.y()+d);
    }
}

void Scroller_impl::page_up() {
    if (pan_.y() > 0) {
        int d = std::min((4*size().iheight())/5, pan_.y());
        gravity_cx_.block();
        pan_y(pan_.y()-d);
    }
}

void Scroller_impl::page_down() {
    Size max = pan_size()-size();

    if (0 != max.height()) {
        int d = std::min((4*size().iheight())/5, max.iheight());
        gravity_cx_.block();
        pan_y(pan_.y()+d);
    }
}

void Scroller_impl::home() {
    if (pan_.y() > 0) {
        gravity_cx_.block();
        pan_y(0);
    }
}

void Scroller_impl::end() {
    Size max = pan_size()-size();

    if (0 != max.height()) {
        gravity_cx_.block();
        pan_y(max.height());
    }
}

bool Scroller_impl::on_mouse_down(int mbt, int mm, const Point & pt) {
    if (MBT_LEFT == mbt && MM_NONE == mm && !focused()) { grab_focus(); }
    return false;
}

bool Scroller_impl::on_mouse_wheel(int delta, int mm, const Point & pt) {
    Size max = pan_size()-size();

    if (delta < 0) {
        if (max.width() && ((MM_CONTROL|MM_SHIFT) & mm)) { pan_left(); return true; }
        else if (max.height()) { pan_up(); return true; }
    }

    else {
        if (max.width() && ((MM_CONTROL|MM_SHIFT) & mm)) { pan_right(); return true; }
        else if (max.height()) { pan_down(); return true; }
    }

    return false;
}

bool Scroller_impl::on_take_focus() {
    return cp_ && cp_->take_focus() ? true : grab_focus();
}

void Scroller_impl::pan(Widget_impl * wp) {
    if (wp && !wp->hidden() && wp->has_parent(this)) {
        pan_to(wp);
    }
}

void Scroller_impl::pan_to(Widget_impl * wp) {
    if (wp != cp_) {        // FIXME Filter children?
        Size z = size();
        Rect wa(wp->to_parent(this), wp->size()), r(z);

        if (r) {
            if (wa) {
                Point pt = pan_;
                constexpr int gap = 2;

                if (wa.bottom() > r.bottom()) {
                    if (wa.height() < z.height()) { pt.translate(0, gap+wa.bottom()-z.iheight()); }
                    else { pt.translate(0, wa.top()-gap-z.iheight()); }
                    pan(pt);
                }

                else if (wa.top() < r.top()) {
                    pt.translate(0, wa.top()-gap);
                    pan(pt);
                }

                if (wa.right() > r.right()) {
                    if (wa.width() < z.width()) { pt.translate(gap+wa.right()-z.iwidth(), 0); }
                    else { pt.translate(wa.left()-gap-z.iwidth(), 0); }
                    pan(pt);
                }

                else if (wa.left() < r.left()) {
                    pt.translate(wa.left()-gap, 0);
                    pan(pt);
                }
            }
        }

        else {
            tracked_ = wp;
            tracked_cx_ = wp->signal_parent().connect(fun(this, &Scroller_impl::on_tracked_unparent));
        }
    }
}

// Overrides Widget_impl.
void Scroller_impl::track_pan(Widget_impl * wp) {
    pan_to(wp);
}

void Scroller_impl::on_size_changed() {
    arrange(), check_pan();

    if (size()) {
        tracked_cx_.drop();
        if (tracked_) { pan_to(tracked_); }
        tracked_ = nullptr;
    }
}

void Scroller_impl::on_pan_size_changed() {
    check_pan(), check_gravity();
}

void Scroller_impl::on_display_in() {
    update_requisition();
    if (deferred_.x() >= 0) { pan(deferred_); deferred_.reset(); }
}

void Scroller_impl::on_tracked_unparent(Object * o) {
    if (!o) {
        tracked_cx_.drop();
        tracked_ = nullptr;
    }
}

} // namespace tau

//END

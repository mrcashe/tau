// ----------------------------------------------------------------------------
// SPDX-License-Identifier: BSD-2-Clause
// Copyright © 2014-2024 Konstantin Shmelkov <mrcashe@gmail.com>.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
// THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
// PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
// CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
// EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
// PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
// OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
// WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
// OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
// EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// ----------------------------------------------------------------------------

#ifndef __TAU_DISPLAY_IMPL_HH__
#define __TAU_DISPLAY_IMPL_HH__

#include <tau/display.hh>
#include <tau/object.hh>
#include <tau/timer.hh>
#include <tau/timeval.hh>
#include <timer-impl.hh>
#include <window-impl.hh>
#include <forward_list>
#include <map>
#include <thread>

namespace tau {

class Display_impl: public Object {
public:

    /// Platform-specific.
    static Display_ptr open(const ustring & args);

    /// Platform-specific.
    /// TODO Make thread-local variable for current.
    static Display_ptr this_ptr();

    /// Platform-specific.
    /// Test if current display running.
    static bool this_running() noexcept;

    /// Platform-specific.
    static Display_ptr ptr(Display_impl * dp);

    // Emitted when new display opened.
    static signal<void(Display_impl *)> & signal_open();

    int id() const noexcept { return dpid_; }
    Size size_px() const noexcept { return size_px_; }
    Size size_mm() const noexcept { return size_mm_; }
    unsigned dpi() const noexcept { return dpi_; }
    bool screensaver_allowed() const noexcept { return 0 == screensaver_counter_; }
    std::vector<ustring> list_families() const { return famv_; }
    void list_families_async(slot<void(ustring)> slot_next);

    // Overridden by Display_xcb.
    // Overridden by Display_win.
    virtual std::vector<ustring> list_faces(const ustring & family) const = 0;

    // Overridden by Display_xcb.
    // Overridden by Display_win.
    virtual ustring font_normal() const = 0;

    // Overridden by Display_xcb.
    // Overridden by Display_win.
    virtual ustring font_mono() const = 0;

    // Overridden by Display_xcb.
    // Overridden by Display_win.
    virtual int depth() const noexcept = 0;

    // Overridden by Display_xcb.
    // Overridden by Display_win.
    virtual bool can_paste_text() const noexcept = 0;

    // Overridden by Display_xcb.
    // Overridden by Display_win.
    virtual void paste_text() = 0;

    // Overridden by Display_xcb.
    // Overridden by Display_win.
    virtual void copy_text(const ustring & s) = 0;

    // Overridden by Display_xcb.
    virtual void allow_screensaver();

    // Overridden by Display_xcb.
    virtual void disallow_screensaver();

    // Overridden by Display_unix.
    // Overridden by Display_win.
    virtual Loop_ptr loop() = 0;

    // Overridden by Display_unix.
    // Overridden by Display_win.
    virtual Loop_cptr loop() const = 0;

    // Overridden by Display_xcb.
    // Overridden by Display_win.
    virtual void grab_mouse(Window_impl * wip) = 0;

    // Overridden by Display_xcb.
    // Overridden by Display_win.
    virtual void ungrab_mouse(Window_impl * wip) = 0;

    // Overridden by Display_xcb.
    // Overridden by Display_win.
    virtual bool grab_modal(Window_impl * wip) = 0;

    // Overridden by Display_xcb.
    // Overridden by Display_win.
    virtual bool end_modal(Window_impl * wii) = 0;

    // Overridden by Display_xcb.
    // Overridden by Display_win.
    /// @return @b true on success.
    virtual bool grab_focus(Window_impl * wii) = 0;

    // Overridden by Display_xcb.
    // Overridden by Display_win.
    virtual Point where_mouse() const noexcept = 0;

    // Overridden by Display_xcb.
    // Overridden by Display_win.
    virtual Rect cursor_area() const noexcept = 0;

    // Overridden by Display_xcb.
    // Overridden by Display_win.
    virtual Toplevel_ptr create_toplevel(const Rect & bounds) = 0;

    // Overridden by Display_xcb.
    // Overridden by Display_win.
    virtual Dialog_ptr create_dialog(Widget_impl * tpl, const Rect & bounds=Rect()) = 0;

    // Overridden by Display_xcb.
    // Overridden by Display_win.
    virtual Popup_ptr create_popup(Widget_impl * wpp, const Point & origin, Gravity gravity) = 0;

    Window_ptr winptr(Widget_impl * wp) noexcept;
    Window_cptr winptr(const Widget_impl * wp) const noexcept;

    Window_impl * modal_window() noexcept { return modal_window_; }
    const Window_impl * modal_window() const noexcept { return modal_window_; }
    Window_impl * focused_window() noexcept { return focused_; }
    const Window_impl * focused_window() const noexcept { return focused_; }
    Window_impl * mouse_grabber() noexcept { return mouse_grabber_; }
    const Window_impl * mouse_grabber() const noexcept { return mouse_grabber_; }
    Window_impl * mouse_owner() noexcept { return mouse_owner_; }
    const Window_impl * mouse_owner() const noexcept { return mouse_owner_; }

    Widget_ptr focus_endpoint() noexcept;
    Widget_cptr focus_endpoint() const noexcept;

    signal<void()> & signal_clipboard_changed() { return signal_clipboard_changed_; }
    signal<void()> & signal_quit() { return signal_quit_; }

protected:

    Size                    size_px_;
    Size                    size_mm_;
    unsigned                dpi_                    = 96;
    unsigned                screensaver_counter_    = 0;
    std::thread::id         tid_;
    int                     dpid_                   = -1;

    Window_impl *           mouse_grabber_          = nullptr;
    Window_impl *           mouse_owner_            = nullptr;
    Window_impl *           modal_window_           = nullptr;
    Window_impl *           focused_                = nullptr;

    // TODO Replace this signal by loop()->signal_quit().
    signal<void()>          signal_quit_;
    signal<void()>          signal_clipboard_changed_;

    using Families          = std::vector<ustring>;
    Families                famv_;

protected:

    Display_impl();
    void on_window_close(Window_impl * wip);
    void add_window(Window_ptr wip);
    Window_impl * set_mouse_owner(Window_impl * wip, const Point & pt);
    void reset_mouse_owner();
    void focus_window(Window_impl * wip);

    /// @return true if focus was removed.
    bool unfocus_window(Window_impl * wip);

private:

    using Windows           = std::forward_list<Window_ptr>;
    Windows                 windows_;
    Windows                 sanitize_;

    using Fam_iter          = Families::const_iterator;

    struct Family_lister {
        slot<void(ustring)> s;
        Fam_iter            iter;
        connection          cx      { true };
    };

    using Family_listers    = std::forward_list<Family_lister>;
    Family_listers          family_listers_;

private:

    void on_window_disable(Window_impl * wip);
    void on_sanitize();
    void on_family_run(Family_lister & lst);
    void on_quit();
};

} // namespace tau

#endif // __TAU_DISPLAY_IMPL_HH__

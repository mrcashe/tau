// ----------------------------------------------------------------------------
// SPDX-License-Identifier: BSD-2-Clause
// Copyright © 2014-2024 Konstantin Shmelkov <mrcashe@gmail.com>.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
// THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
// PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
// CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
// EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
// PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
// OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
// WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
// OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
// EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// ----------------------------------------------------------------------------

#include <tau/fontsel.hh>
#include <fontsel-impl.hh>

#define FONTSEL_IMPL (std::static_pointer_cast<Fontsel_impl>(impl))

namespace tau {

Fontsel::Fontsel():
    Widget(std::make_shared<Fontsel_impl>())
{
}

Fontsel::Fontsel(std::string_view spec, const ustring & sample):
    Widget(std::make_shared<Fontsel_impl>(spec, sample))
{
}

Fontsel::Fontsel(const Fontsel & other):
    Widget(other.impl)
{
}

Fontsel & Fontsel::operator=(const Fontsel & other) {
    Widget::operator=(other);
    return *this;
}

Fontsel::Fontsel(Fontsel && other):
    Widget(other.impl)
{
}

Fontsel & Fontsel::operator=(Fontsel && other) {
    Widget::operator=(other);
    return *this;
}

Fontsel::Fontsel(Widget_ptr wp):
    Widget(std::dynamic_pointer_cast<Fontsel_impl>(wp))
{
}

Fontsel & Fontsel::operator=(Widget_ptr wp) {
    Widget::operator=(std::dynamic_pointer_cast<Fontsel_impl>(wp));
    return *this;
}

void Fontsel::show_monospace_only(bool yes) {
    FONTSEL_IMPL->set_show_monospace_only(yes);
}

bool Fontsel::monospace_only_visible() const noexcept {
    return FONTSEL_IMPL->monospace_only_visible();
}

void Fontsel::select(std::string_view spec) {
    FONTSEL_IMPL->select(spec);
}

void Fontsel::set_sample(const ustring & sample) {
    FONTSEL_IMPL->set_sample(sample);
}

ustring Fontsel::spec() const {
    return FONTSEL_IMPL->spec();
}

ustring Fontsel::sample() const {
    return FONTSEL_IMPL->sample();
}

Action & Fontsel::action_apply() {
    return FONTSEL_IMPL->action_apply();
}

Action & Fontsel::action_cancel() {
    return FONTSEL_IMPL->action_cancel();
}

signal<void(std::string_view)> & Fontsel::signal_selection_changed() {
    return FONTSEL_IMPL->signal_selection_changed();
}

signal<void(std::string_view)> & Fontsel::signal_font_activated() {
    return FONTSEL_IMPL->signal_font_activated();
}

} // namespace tau

//END

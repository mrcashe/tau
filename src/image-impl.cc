// ----------------------------------------------------------------------------
// SPDX-License-Identifier: BSD-2-Clause
// Copyright © 2014-2024 Konstantin Shmelkov <mrcashe@gmail.com>.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
// THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
// PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
// CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
// EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
// PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
// OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
// WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
// OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
// EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// ----------------------------------------------------------------------------

#include <display-impl.hh>
#include <image-impl.hh>
#include <pixmap-impl.hh>
#include <theme-impl.hh>
#include <tau/brush.hh>
#include <tau/painter.hh>
#include <iostream>

namespace tau {

Image_impl::Image_impl() {
    init();
}

Image_impl::Image_impl(Pixmap_cptr pix, bool transparent) {
    init();
    set_pixmap(pix, transparent);
}

Image_impl::Image_impl(Pixmap_ptr pix, bool transparent) {
    init();
    set_pixmap(pix, transparent);
}

Image_impl::Image_impl(const ustring & pixmap_name, bool transparent):
    transparent_(transparent),
    pixmap_name_(pixmap_name)
{
    init();
}

void Image_impl::init() {
    signal_paint().connect(fun(this, &Image_impl::on_paint));
    signal_display_in_.connect(fun(this, &Image_impl::on_display));
    signal_enable_.connect(fun(this, &Image_impl::on_enable));
    signal_disable_.connect(fun(this, &Image_impl::on_disable));
}

Image_impl::~Image_impl() {
    destroy();
}

void Image_impl::add_pixmap(Pixmap_cptr pix, unsigned delay) {
    if (pix) {
        auto & ff = cani_.emplace_back();
        ff.pix = pix, ff.delay = delay;
        gray_.reset(); if (!enabled()) { create_gray(); }
        if (!update_requisition()) { redraw(); start_timer_if_needed(); }
    }
}

void Image_impl::add_pixmap(Pixmap_ptr pix, unsigned delay) {
    if (pix) {
        auto & ff = ani_.emplace_back();
        ff.pix = pix, ff.delay = delay;
        ff.changed_cx = pix->signal_changed().connect(bind_back(fun(this, &Image_impl::on_pix_changed), ani_.size()-1));
        gray_.reset(); if (!enabled()) { create_gray(); }
        if (!update_requisition()) { redraw(); start_timer_if_needed(); }
    }
}

void Image_impl::set_pixmap(Pixmap_cptr pix, bool transparent) {
    transparent_ = transparent;
    ani_.clear();
    cani_.clear();
    add_pixmap(pix);
}

void Image_impl::set_pixmap(Pixmap_ptr pix, bool transparent) {
    transparent_ = transparent;
    ani_.clear();
    cani_.clear();
    add_pixmap(pix);
}

void Image_impl::create_gray() {
    if (!in_shutdown() && !gray_ && (!cani_.empty() || !ani_.empty())) {
        Pixmap_cptr orig = !cani_.empty() ? cani_.front().pix : ani_.front().pix;

        if (32 == orig->depth() && transparent_) {
            gray_ = Pixmap_impl::create(32, orig->size());
            Color white(1.0, 1.0, 1.0, 0.33);   // FIXME What about alpha?

            for (int y = 0; y < orig->size().iheight(); ++y) {
                for (int x = 0; x < orig->size().iwidth(); ++x) {
                    Color c = orig->get_pixel(Point(x, y));
                    double a = c.alpha();
                    if (a > 0) { c.alpha_blend(white); }
                    double g = c.gray();
                    gray_->put_pixel(Point(x, y), Color(g, g, g, a));
                }
            }
        }

        else {
            gray_ = Pixmap_impl::create(8, orig);
        }

        if (!enabled()) { redraw(); }
    }
}

void Image_impl::on_timer() {
    std::size_t cur = cur_;

    if (!cani_.empty()) {
        if (cani_.size() > 1) {
            if (++cur_ >= cani_.size()) {
                cur_ = 0;
            }
        }
    }

    else if (ani_.size() > 1) {
        if (++cur_ >= ani_.size()) {
            cur_ = 0;
        }
    }

    if (cur != cur_) {
        timer_cx_ = Loop_impl::this_ptr()->alarm(fun(this, &Image_impl::on_timer), calc_delay());
        redraw();
    }
}

void Image_impl::redraw() {
    if (visible()) {
        if (1 == signal_paint().size()) {
            if (auto pr = painter()) {
                paint_pixmap(pr);
                return;
            }
        }

        invalidate();
    }
}

void Image_impl::on_pix_changed(std::size_t index) {
    if (!in_shutdown() && index < ani_.size() && cur_ == index) {
        gray_.reset();
        redraw();
    }
}

void Image_impl::paint_pixmap(Painter pr) {
    pr.set_brush(conf().brush(Conf::BACKGROUND)); pr.paint();

    Pixmap_cptr pix;
    if (!enabled() && gray_) { pix = gray_; }
    else if (!ani_.empty() && cur_ < ani_.size()) { pix = ani_[cur_].pix; }
    else if (!cani_.empty() && cur_ < cani_.size()) { pix = cani_[cur_].pix; }

    if (pix) {
        Rect r(pix->size());
        r.center_to(Rect(size()).center());
        pr.set_oper(oper_);
        pr.move_to(1+r.left(), 1+r.top());  // FIXME offset by 1?
        pr.pixmap(pix, transparent_);
        pr.fill();
    }
}

void Image_impl::set_delay(unsigned delay) {
    if (delay_ != delay) {
        delay_ = delay;
        start_timer_if_needed();
    }
}

void Image_impl::start_timer_if_needed() {
    if (cani_.size() > 1 || ani_.size() > 1) {
        Loop_impl::this_ptr()->alarm(fun(this, &Image_impl::on_timer), calc_delay());
    }
}

unsigned Image_impl::calc_delay() const {
    if (!cani_.empty()) {
        if (cur_ < cani_.size()) {
            const auto & ff = cani_[cur_];
            if (0 != ff.delay) { return ff.delay; }
        }
    }

    if (cur_ < ani_.size()) {
        const auto & ff = ani_[cur_];
        if (0 != ff.delay) { return ff.delay; }
    }

    if (0 != delay_) {
        return delay_;
    }

    return 1000;
}

void Image_impl::set_transparent() {
    if (!transparent_) {
        transparent_ = true;
        redraw();
    }
}

void Image_impl::unset_transparent() {
    if (transparent_) {
        transparent_ = false;
        redraw();
    }
}

void Image_impl::set_oper(Oper op) {
    if (oper_ != op) {
        oper_ = op;
        redraw();
    }
}

void Image_impl::clear() {
    pixmap_name_.clear();
    ani_.clear();
    cani_.clear();
    timer_cx_.drop();
    cur_ = 0;
    gray_.reset();
    update_requisition();
}

// Overriden by Icon_impl.
bool Image_impl::update_requisition() {
    Size z;
    for (auto & f: ani_) { z |= f.pix->size(); }
    for (auto & f: cani_) { z |= f.pix->size(); }
    return require_size(z);
}

bool Image_impl::on_paint(Painter pr, const Rect & inval) {
    paint_pixmap(pr);
    return false;
}

void Image_impl::on_display() {
    if (cani_.empty() && !pixmap_name_.empty()) {
        if (auto pix = Theme_impl::root()->find_pixmap(pixmap_name_)) {
            set_pixmap(pix, transparent_);
        }
    }

    if (enabled()) { on_enable(); }
    else { on_disable(); }
}

void Image_impl::on_enable() {
    if (enabled()) {
        if (!gray_cx_.empty()) { gray_cx_.drop(); }
        else { redraw(); }
    }
}

void Image_impl::on_disable() {
    if (!enabled()) {
        if (!gray_) { gray_cx_ = Loop_impl::this_ptr()->alarm(fun(this, &Image_impl::create_gray), IMAGE_GRAY_DELAY); }   // defs-impl.hh
        else { redraw(); }
    }
}

} // namespace tau

//END

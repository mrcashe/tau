// ----------------------------------------------------------------------------
// SPDX-License-Identifier: BSD-2-Clause
// Copyright © 2014-2024 Konstantin Shmelkov <mrcashe@gmail.com>.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
// THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
// PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
// CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
// EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
// PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
// OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
// WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
// OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
// EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// ----------------------------------------------------------------------------

#include <tau/exception.hh>
#include <tau/locale.hh>
#include <tau/string.hh>
#include <buffer-impl.hh>
#include <array>
#include <filesystem>
#include <fstream>
#include <iostream>

namespace tau {

Buffer_impl::Buffer_impl() {
    newlines_ = Locale().newlines();
}

Buffer_impl::~Buffer_impl() {
    if (signal_erase_) { delete signal_erase_; }
    if (signal_insert_) { delete signal_insert_; }
    if (signal_replace_) { delete signal_replace_; }
    if (signal_changed_) { delete signal_changed_; }
    if (signal_flush_) { delete signal_flush_; }
    if (signal_lock_) { delete signal_lock_; }
    if (signal_unlock_) { delete signal_unlock_; }
    if (signal_encoding_changed_) { delete signal_encoding_changed_; }
    if (signal_bom_changed_) { delete signal_bom_changed_; }
}

char32_t Buffer_impl::at(std::size_t row, std::size_t col) const {
    if (row < rows_.size()) {
        auto & s = rows_[row].s;
        if (col < s.size()) { return s[col]; }
    }

    return 0;
}

std::u32string Buffer_impl::wstr(std::size_t r1, std::size_t c1, std::size_t r2, std::size_t c2) const {
    if (r1 > r2) { std::swap(r1, r2); std::swap(c1, c2); }
    if (r1 == r2 && c1 > c2) { std::swap(c1, c2); }

    std::size_t nrows = rows_.size();
    std::u32string s;

    while (r1 < r2 && r1 < nrows) {
        const std::u32string & d = rows_[r1].s;
        s += d.substr(c1);
        ++r1, c1 = 0;
    }

    if (r1 == r2 && r1 < nrows) {
        const std::u32string & d = rows_[r1].s;

        if (c1 < d.size() && c2 > c1) {
            s += d.substr(c1, c2-c1);
        }
    }

    return s;
}

std::size_t Buffer_impl::length(std::size_t r1, std::size_t c1, std::size_t r2, std::size_t c2) const {
    if (r1 > r2) { std::swap(r1, r2); std::swap(c1, c2); }
    if (r1 == r2 && c1 > c2) { std::swap(c1, c2); }

    std::size_t nrows = rows_.size(), result = 0;

    while (r1 < r2 && r1 < nrows) {
        const std::u32string & d = rows_[r1].s;
        std::size_t len = d.size(), cm = std::min(len, c1);
        result += len-cm;
        ++r1, c1 = 0;
    }

    if (r1 == r2 && r1 < nrows) {
        const std::u32string & d = rows_[r1].s;
        std::size_t len = d.size();

        if (c1 < len && c2 > c1) {
            c2 = std::min(c2, len);
            result += c2-c1;
        }
    }

    return result;
}

Buffer_citer Buffer_impl::insert(Buffer_citer i, const std::u32string & str) {
    if (locked_ || str.empty()) {
        return i;
    }

    Buffer_citer e(i);
    std::size_t n = 0, len = str.size(), row, col;

    if (rows_.empty()) {
        row = 0;
        col = 0;
    }

    else if (i.row() < rows_.size()) {
        row = i.row();
        col = std::min(rows_[row].s.size(), i.col());
    }

    else {
        row = rows_.size()-1;
        col = rows_[row].s.size();
    }

    while (n < len) {
        std::size_t eol = str.find_first_of(newlines_, n);

        // No EOL character.
        // Add text at current position and exit.
        if (std::u32string::npos == eol) {
            if (rows_.empty()) { rows_.emplace_back(str.substr(n)); }
            else { rows_[row].s.insert(col, str, n, len-n); }
            col += len-n;
            n = len;
        }

        // Have EOL character.
        else {
            // Insert text between col and next at current position.
            std::u32string::size_type next, eeol;

            if (0x000a == str[eol] && eol+1 < len && 0x000d == str[eol+1]) { eeol = eol+2; }
            else if (0x000d == str[eol] && eol+1 < len && 0x000a == str[eol+1]) { eeol = eol+2; }
            else { eeol = eol+1; }

            std::u32string newline = std::u32string::npos == eeol ? str.substr(eol) : str.substr(eol, eeol-eol);
            next = eol+newline.size();

            if (rows_.empty()) {
                rows_.emplace_back(str.substr(n, next-n));
                rows_.emplace_back();
            }

            else {
                std::u32string right = rows_[row].s.substr(col);
                rows_[row].s.erase(col);
                rows_[row].s.insert(col, str, n, next-n);
                rows_.emplace(rows_.begin()+row+1, right);
            }

            n = next;
            ++row;
            col = 0;
        }
    }

    changed_ = true;
    e.move_to(row, col);
    if (signal_insert_) { (*signal_insert_)(i, e); }
    if (signal_changed_) { (*signal_changed_)(); }
    return e;
}

Buffer_citer Buffer_impl::erase(Buffer_citer b, Buffer_citer e) {
    if (e < b) { std::swap(b, e); }
    Buffer_citer ret(b);
    std::size_t row1 = b.row(), col1 = b.col(), row2 = e.row(), col2 = e.col(), nrows = rows_.size();

    if (!locked_ && nrows && b && e && b != e && row1 < nrows && col1 < rows_[row1].s.size()) {
        std::size_t erow2, ecol2;

        row2 = std::min(row2, nrows);
        if (row2 && row2 == nrows) { erow2 = row2-1, ecol2 = 0; }
        else { erow2 = row2; ecol2 = std::min(col2, rows_[erow2].s.size()); }
        std::size_t nlines = erow2-row1;
        std::u32string erased_text;
        if (signal_erase_) { erased_text.assign(wstr(b.row(), b.col(), e.row(), e.col())); }
        ret.move_to(row2, col2);

        if (0 == nlines) {
            if (ecol2 > col1) {
                rows_[row1].s.erase(col1, ecol2-col1);
            }
        }

        else {
            rows_[row1].s.erase(col1);
            rows_[row1].s.append(rows_[erow2].s.substr(ecol2));
            rows_[erow2].s.erase(0, ecol2);
            while (nlines--) { rows_.erase(rows_.begin()+row1+1); }
        }

        if (1 == rows_.size() && 0 == rows_[0].s.size()) { rows_.clear(); }
        changed_ = true;
        if (signal_erase_) { (*signal_erase_)(b, ret, erased_text); }
        if (signal_changed_) { (*signal_changed_)(); }
    }

    return ret;
}

void Buffer_impl::change_encoding(const Encoding & enc) {
    if (encoding_ != enc) {
        encoding_ = enc;
        if (signal_encoding_changed_) { (*signal_encoding_changed_)(encoding_); }
    }
}

void Buffer_impl::enable_bom() {
    if (!bom_) {
        bom_ = true;
        if (signal_bom_changed_) { (*signal_bom_changed_)(); }
    }
}

void Buffer_impl::disable_bom() {
    if (bom_) {
        bom_ = false;
        if (signal_bom_changed_) { (*signal_bom_changed_)(); }
    }
}

Buffer_citer Buffer_impl::insert(Buffer_citer iter, std::istream & is) {
    if (locked_ || !is.good()) { return iter; }

    acc_.clear();
    char buffer[2048];
    char32_t ob[10240];
    std::size_t fpos = 0, nbytes = 0;
    bool not8 = false;

    while (!is.eof()) {
        fpos += nbytes;
        is.read(buffer, sizeof(buffer));
        nbytes = is.gcount();
        if (0 == nbytes) { break; }
        std::size_t offset = 0, epos = nbytes;
        char32_t * obp = ob;

        if (0 == fpos) {
            if (nbytes >= 4 && '\0' == buffer[0] && '\0' == buffer[1] && '\xfe' == buffer[2] && '\xff' == buffer[3]) {
                offset += 4;
                nbytes -= 4;
                change_encoding(utf32be_);
                enable_bom();
            }

            else if (nbytes >= 4 && '\xff' == buffer[0] && '\xfe' == buffer[1] && '\0' == buffer[2] && '\0' == buffer[3]) {
                offset += 4;
                nbytes -= 4;
                change_encoding(utf32le_);
                enable_bom();
            }

            else if (nbytes >= 3 && '\xef' == buffer[0] && '\xbb' == buffer[1] && '\xbf' == buffer[2]) {
                offset += 3;
                nbytes -= 3;
                change_encoding(utf8_);
                enable_bom();
            }

            else if (nbytes >= 2 && '\xfe' == buffer[0] && '\xff' == buffer[1]) {
                offset += 2;
                nbytes -= 2;
                change_encoding(utf16be_);
                enable_bom();
            }

            else if (nbytes >= 2 && '\xff' == buffer[0] && '\xfe' == buffer[1]) {
                offset += 2;
                nbytes -= 2;
                change_encoding(utf16le_);
                enable_bom();
            }
        }

        if (utf32be_ == encoding_) {
            const char * p = buffer+offset;

            while (nbytes >= 4) {
                char32_t wc = uint8_t(*p++);
                wc <<= 8; wc += uint8_t(*p++);
                wc <<= 8; wc += uint8_t(*p++);
                wc <<= 8; wc += uint8_t(*p++);
                *obp++ = wc;
                nbytes -= 4;
            }
        }

        else if (utf32le_ == encoding_) {
            const char * p = buffer+offset;

            while (nbytes >= 4) {
                char32_t wc = uint8_t(p[3]);
                wc <<= 8; wc += uint8_t(p[2]);
                wc <<= 8; wc += uint8_t(p[1]);
                wc <<= 8; wc += uint8_t(p[0]);
                *obp++ = wc;
                p += 4;
                nbytes -= 4;
            }
        }

        else if (utf16be_ == encoding_) {
            const char * p = buffer+offset;
            char16_t sur = 0;

            while (nbytes >= 2) {
                char16_t wc = uint8_t(*p++);
                wc <<= 8; wc += uint8_t(*p++);

                if (0 != sur) {
                    if (char16_is_surrogate(wc)) {
                        *obp++ = char32_from_surrogate(sur, wc);
                    }

                    // FIXME Simply skip character on error? Is it correct?
                    else {
                        std::cerr << "** Buffer::insert(stream): (" << encoding_.name() << "), position "
                        << fpos+offset << ": skip surrogate (?) pair " << std::hex << std::showbase
                        << unsigned(sur) << ':' << std::hex << std::showbase << unsigned(wc) << std::dec << std::endl;
                    }

                    sur = 0;
                }

                else {
                    if (char16_is_surrogate(wc)) { sur = wc; }
                    else { *obp++ = char32_t(wc); }
                }

                nbytes -= 2;
                offset += 2;
            }
        }

        else if (utf16le_ == encoding_) {
            const char * p = buffer+offset;
            char16_t sur = 0;

            while (nbytes >= 2) {
                char16_t wc = uint8_t(p[1]);
                wc <<= 8; wc += uint8_t(p[0]);

                if (0 != sur) {
                    if (char16_is_surrogate(wc)) {
                        *obp++ = char32_from_surrogate(sur, wc);
                    }

                    // FIXME Simply skip character on error? Is it correct?
                    else {
                        std::cerr << "** Buffer::insert(stream): (" << encoding_.name() << "), position "
                        << fpos+offset << ": skip surrogate (?) pair " << std::hex << std::showbase
                        << unsigned(sur) << ':' << std::hex << std::showbase << unsigned(wc) << std::dec << std::endl;
                    }

                    sur = 0;
                }

                else {
                    if (char16_is_surrogate(wc)) { sur = wc; }
                    else { *obp++ = char32_t(wc); }
                }

                p += 2, nbytes -= 2, offset += 2;
            }
        }

        // Assume UTF-8?
        else {
            if (!acc_.empty()) {    // Incomplete sequence stored in acc.
                std::size_t more = utf8_charlen(acc_[0])-acc_.size();

                if (more <= nbytes) {
                    acc_.append(buffer+offset, more);
                    *obp++ = char32_from_pointer(acc_.c_str()); acc_.clear();
                    nbytes -= more; offset += more;
                }

                // Assume encoding is not UTF-8.
                else {
                    not8 = true;
                }
            }

            // Has incomplete sequence within buffer? Look at the buffer end.
            if (!not8 && nbytes > 4 && nbytes == sizeof buffer) {
                for (const char * p = buffer+nbytes-3; p < buffer+nbytes; ++p) {
                    if (*p >= -66 && *p <= -12) {
                        std::size_t n = buffer+nbytes-p;
                        if (utf8_charlen(*p) > n) { acc_.assign(p, n); epos = p-buffer; break; }
                    }
                }
            }

            // Read as (possibly) UTF-8.
            if (!not8) {
                const char * p = buffer+offset;

                // First try to read as ASCII.
                for (; *p > 0 && p < buffer+epos; ++p, ++offset) { *obp++ = char32_t(*p); }

                // Next try to read as UTF-8.
                for (; p < buffer+epos; p = utf8_next(p)) {
                    char32_t wc = char32_from_pointer(p);

                    if (!wc) {
                        not8 = true;
                        iter = insert(iter, std::u32string(ob, obp-ob));
                        obp = ob;
                        break;
                    }

                    *obp++ = wc;
                    offset += utf8_charlen(*p);
                    change_encoding(utf8_);
                }
            }

            // Not UTF-8, so far simply replace non UTF-8 characters by spaces.
            if (not8) {
                change_encoding(ascii_);
                for (const char * p = buffer+offset; p < buffer+nbytes; ++p) { *obp++ = *p > '\0' ? char32_t(*p) : U' '; }
            }
        }

        iter = insert(iter, std::u32string(ob, obp-ob));
    }

    return iter;
}

Buffer_citer Buffer_impl::replace(Buffer_citer i, char32_t uc, std::size_t count) {
    return replace(i, std::u32string(count, uc));
}

Buffer_citer Buffer_impl::replace(Buffer_citer i, const std::u32string & str) {
    if (locked_ || str.empty()) { return i; }
    std::size_t n = 0, len = str.size();

    while (n < len) {
        if (i.row() >= rows_.size()) {
            return insert(i, str);
        }

        if (i.row() == rows_.size()-1 && i.col() >= rows_[i.row()].s.size()) {
            return insert(i, str);
        }

        std::size_t eol = str.find_first_of(newlines_, n);
        if (ustring::npos == eol) { eol = len; }

        if (eol > n) {
            std::u32string & d = rows_[i.row()].s;
            std::size_t d_eol = d.find_first_of(newlines_);
            if (std::u32string::npos == d_eol) { d_eol = d.size(); }
            std::size_t n_repl = std::min(eol-n, (i.col() < d_eol ? d_eol-i.col() : 0));

            if (0 != n_repl) {
                std::u32string replaced_text;
                if (signal_replace_) { replaced_text.assign(d.substr(i.col(), n_repl)); }
                d.replace(i.col(), n_repl, str, n, n_repl);
                std::size_t col = i.col()+n_repl;
                auto j = i; j.move_to(col);
                if (signal_replace_) { (*signal_replace_)(i, j, replaced_text); }
                i.move_to(col);
            }

            if (eol-n > n_repl) {
                i = insert(i, str.substr(n+n_repl, eol-n-n_repl));
            }
        }

        if (eol < len) {
            if (U'\x000d' == str[eol] && eol+1 < len && U'\x000a' == str[eol+1]) { ++eol; }
            ++eol;
            i.forward_line();
        }

        n = eol;
    }

    changed_ = true;
    if (signal_changed_) { (*signal_changed_)(); }
    return i;
}

void Buffer_impl::flush() {
    if (!path_.empty()) {
        save_to_file(path_);
    }

    else {
        changed_ = false;
        if (signal_flush_) { (*signal_flush_)(); }
    }
}

void Buffer_impl::save(std::ostream & os) const {
    if (utf16be_ == encoding_) {
        if (bom_) { os.write("\xfe\xff", 2); }

        for (auto & row: rows_) {
            for (char32_t wc: row.s) {
                char16_t c1, c2;
                char32_to_surrogate(wc, c1, c2);
                os.put(c1);
                os.put(c1 >> 8);

                if (0 != c2) {
                    os.put(c2);
                    os.put(c2 >> 8);
                }
            }
        }
    }

    else if (utf16le_ == encoding_) {
        if (bom_) { os.write("\xff\xfe", 2); }

        for (auto & row: rows_) {
            for (char32_t wc: row.s) {
                char16_t c1, c2;
                char32_to_surrogate(wc, c1, c2);
                os.put(c1 >> 8);
                os.put(c1);

                if (0 != c2) {
                    os.put(c2 >> 8);
                    os.put(c2);
                }
            }
        }
    }

    else if (utf32be_ == encoding_) {
        if (bom_) { os.write("\x00\x00\xfe\xff", 4); }

        for (auto & row: rows_) {
            for (char32_t wc: row.s) {
                os.put(wc);
                os.put(wc >> 8);
                os.put(wc >> 16);
                os.put(wc >> 24);
            }
        }
    }

    else if (utf32le_ == encoding_) {
        if (bom_) { os.write("\xff\xfe\x00\x00", 4); }

        for (auto & row: rows_) {
            for (char32_t wc: row.s) {
                os.put(wc >> 24);
                os.put(wc >> 16);
                os.put(wc >> 8);
                os.put(wc);
            }
        }
    }

    else {
        if (utf8_ == encoding_) {
            if (bom_) {
                os.write("\xef\xbb\xbf", 3);
            }
        }

        for (auto & row: rows_) {
            ustring s(row.s);
            os.write(s.c_str(), s.bytes());
        }
    }

    changed_ = false;
    if (signal_flush_) { (*signal_flush_)(); }
}

void Buffer_impl::save_to_file(const ustring & path) {
    std::ofstream os(std::filesystem::path(std::wstring(path)), std::ios::binary);
    if (!os.good()) { throw sys_error(); }
    save(os);
    os.close();
}

void Buffer_impl::lock() {
    if (!locked_) {
        locked_ = true;
        if (signal_lock_) { (*signal_lock_)(); }
    }
}

void Buffer_impl::unlock() {
    if (locked_) {
        locked_ = false;
        if (signal_unlock_) { (*signal_unlock_)(); }
    }
}

signal<void(Buffer_citer, Buffer_citer, const std::u32string &)> & Buffer_impl::signal_erase() {
    if (!signal_erase_) { signal_erase_ = new signal<void(Buffer_citer, Buffer_citer, const std::u32string &)>; }
    return *signal_erase_;
}

signal<void(Buffer_citer, Buffer_citer)> & Buffer_impl::signal_insert() {
    if (!signal_insert_) { signal_insert_ = new signal<void(Buffer_citer, Buffer_citer)>; }
    return *signal_insert_;
}

signal<void(Buffer_citer, Buffer_citer, const std::u32string &)> & Buffer_impl::signal_replace() {
    if (!signal_replace_) { signal_replace_ = new signal<void(Buffer_citer, Buffer_citer, const std::u32string &)>; }
    return *signal_replace_;
}

signal<void()> & Buffer_impl::signal_changed() {
    if (!signal_changed_) { signal_changed_ = new signal<void()>; }
    return *signal_changed_;
}

signal<void()> & Buffer_impl::signal_flush() {
    if (!signal_flush_) { signal_flush_ = new signal<void()>; }
    return *signal_flush_;
}

signal<void()> & Buffer_impl::signal_lock() {
    if (!signal_lock_) { signal_lock_ = new signal<void()>; }
    return *signal_lock_;
}

signal<void()> & Buffer_impl::signal_unlock() {
    if (!signal_unlock_) { signal_unlock_ = new signal<void()>; }
    return *signal_unlock_;
}

signal<void(const Encoding &)> & Buffer_impl::signal_encoding_changed() {
    if (!signal_encoding_changed_) { signal_encoding_changed_ = new signal<void(const Encoding &)>; }
    return *signal_encoding_changed_;
}

signal<void()> & Buffer_impl::signal_bom_changed() {
    if (!signal_bom_changed_) {  signal_bom_changed_ = new signal<void()>; }
    return *signal_bom_changed_;
}

} // namespace tau

//END

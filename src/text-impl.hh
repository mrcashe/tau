// ----------------------------------------------------------------------------
// SPDX-License-Identifier: BSD-2-Clause
// Copyright © 2014-2024 Konstantin Shmelkov <mrcashe@gmail.com>.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
// THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
// PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
// CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
// EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
// PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
// OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
// WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
// OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
// EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// ----------------------------------------------------------------------------

#ifndef __TAU_TEXT_IMPL_HH__
#define __TAU_TEXT_IMPL_HH__

#include <tau/action.hh>
#include <label-impl.hh>

namespace tau {

class Text_impl: public Label_impl {
public:

    Text_impl();
    explicit Text_impl(Align xalign, Align yalign=Align::CENTER);
    explicit Text_impl(const ustring & s, Align xalign=Align::CENTER, Align yalign=Align::CENTER);
    explicit Text_impl(const std::u32string & ws, Align xalign=Align::CENTER, Align yalign=Align::CENTER);
    explicit Text_impl(Buffer buf, Align xalign=Align::CENTER, Align yalign=Align::CENTER);
   ~Text_impl();

    void allow_select();
    void disallow_select();
    bool select_allowed() const noexcept { return select_allowed_; }

    bool has_selection() const noexcept { return (sel_ && esel_); }
    std::pair<Buffer_citer, Buffer_citer> current() const { return { sel_, esel_ }; }
    ustring selection() const { return sel_ && esel_ ? sel_.str(esel_) : ustring(); }
    std::u32string wselection() const { return sel_ && esel_ ? sel_.wstr(esel_) : std::u32string(); }
    void select(Buffer_citer b, Buffer_citer e);
    void select(std::size_t row1, std::size_t col1, std::size_t row2, std::size_t col2);
    void select_all();
    void unselect();

    void move_to(const Buffer_citer pos);
    void move_to(std::size_t row, std::size_t col);
    Buffer_citer caret() const;

    void enable_caret();
    void disable_caret();
    bool caret_enabled() const noexcept { return caret_enabled_; }

    Action & action_previous() { return action_previous_; }
    Action & action_select_previous() { return action_select_previous_; }
    Action & action_next() { return action_next_; }
    Action & action_select_next() { return action_select_next_; }
    Action & action_previous_line() { return action_previous_line_; }
    Action & action_select_previous_line() { return action_select_previous_line_; }
    Action & action_next_line() { return action_next_line_; }
    Action & action_select_next_line() { return action_select_next_line_; }
    Action & action_previous_word() { return action_previous_word_; }
    Action & action_select_previous_word() { return action_select_previous_word_; }
    Action & action_next_word() { return action_next_word_; }
    Action & action_select_next_word() { return action_select_next_word_; }
    Action & action_home() { return action_home_; }
    Action & action_select_home() { return action_select_home_; }
    Action & action_end() { return action_end_; }
    Action & action_select_to_eol() { return action_select_to_eol_; }
    Action & action_sof() { return action_sof_; }
    Action & action_select_to_sof() { return action_select_to_sof_; }
    Action & action_eof() { return action_eof_; }
    Action & action_select_to_eof() { return action_select_to_eof_; }
    Action & action_previous_page() { return action_previous_page_; }
    Action & action_next_page() { return action_next_page_; }
    Action & action_select_previous_page() { return action_select_previous_page_; }
    Action & action_select_next_page() { return action_select_next_page_; }
    Action & action_select_all() { return action_select_all_; }
    Action & action_copy() { return action_copy_; }
    Action & action_cancel() { return action_cancel_; }

    signal<void()> & signal_caret_motion();
    signal<void()> & signal_click();
    signal<void()> & signal_selection_changed() { return signal_selection_changed_; }

protected:

    Buffer_citer        caret_;                 // Current caret position.
    bool                insert_ = true;
    signal<void()>      signal_selection_changed_;

protected:

    // Overrides Label_impl.
    // Overridden by Edit_impl.
    void init_buffer() override;

    // Overrides Label_impl.
    void render(Painter pr, const Rect & r) override;

    // Overrides Label_impl.
    void on_buffer_insert(Buffer_citer b, Buffer_citer e) override;

    // Overrides Label_impl.
    void on_buffer_replace(Buffer_citer b, Buffer_citer e, const std::u32string & replaced) override;

    // Overrides Label_impl.
    void on_buffer_erase(Buffer_citer b, Buffer_citer e, const std::u32string & erased) override;

    // Overrides Label_impl.
    void on_wrap() override;

    void scroll_to_caret();
    void hint_x();
    void refresh_caret();

    void pan_up();
    void pan_down();
    void left();
    void move_left();
    void select_left();
    void right();
    void move_right();
    void select_right();
    void up();
    void move_up();
    void select_up();
    void down();
    void move_down();
    void select_down();
    void backward_word();
    void select_word_left();
    void forward_word();
    void select_word_right();
    void home();
    void move_home();
    void select_home();
    void move_to_sof();
    void select_to_sof();
    void move_to_eol();
    void select_to_eol();
    void move_to_eof();
    void select_to_eof();
    void page_up();
    void move_page_up();
    void select_page_up();
    void page_down();
    void move_page_down();
    void select_page_down();
    void copy();

private:

    Action              action_previous_ { KC_LEFT, KM_NONE, fun(this, &Text_impl::move_left) };
    Action              action_select_previous_ { KC_LEFT, KM_SHIFT, fun(this, &Text_impl::select_left) };
    Action              action_next_ { KC_RIGHT, KM_NONE, fun(this, &Text_impl::move_right) };
    Action              action_select_next_ { KC_RIGHT, KM_SHIFT, fun(this, &Text_impl::select_right) };
    Action              action_previous_line_ { KC_UP, KM_NONE, fun(this, &Text_impl::move_up) };
    Action              action_select_previous_line_ { KC_UP, KM_SHIFT, fun(this, &Text_impl::select_up) };
    Action              action_next_line_ { KC_DOWN, KM_NONE, fun(this, &Text_impl::move_down) };
    Action              action_select_next_line_ { KC_DOWN, KM_SHIFT, fun(this, &Text_impl::select_down) };
    Action              action_previous_word_ { KC_LEFT, KM_CONTROL, fun(this, &Text_impl::backward_word) };
    Action              action_select_previous_word_ { KC_LEFT, KM_CONTROL+KM_SHIFT, fun(this, &Text_impl::select_word_left) };
    Action              action_next_word_ { KC_RIGHT, KM_CONTROL, fun(this, &Text_impl::forward_word) };
    Action              action_select_next_word_ { KC_RIGHT, KM_CONTROL+KM_SHIFT, fun(this, &Text_impl::select_word_right) };
    Action              action_home_ { KC_HOME, KM_NONE, fun(this, &Text_impl::move_home) };
    Action              action_select_home_ { KC_HOME, KM_SHIFT, fun(this, &Text_impl::select_home) };
    Action              action_end_ { KC_END, KM_NONE, fun(this, &Text_impl::move_to_eol) };
    Action              action_select_to_eol_ { KC_END, KM_SHIFT, fun(this, &Text_impl::select_to_eol) };
    Action              action_sof_ { KC_HOME, KM_CONTROL, fun(this, &Text_impl::move_to_sof) };
    Action              action_select_to_sof_ { KC_HOME, KM_CONTROL+KM_SHIFT, fun(this, &Text_impl::select_to_sof) };
    Action              action_eof_ { KC_END, KM_CONTROL, fun(this, &Text_impl::move_to_eof) };
    Action              action_select_to_eof_ { KC_END, KM_CONTROL+KM_SHIFT, fun(this, &Text_impl::select_to_eof) };
    Action              action_previous_page_ { KC_PAGE_UP, KM_NONE, fun(this, &Text_impl::move_page_up) };
    Action              action_next_page_ { KC_PAGE_DOWN, KM_NONE, fun(this, &Text_impl::move_page_down) };
    Action              action_select_previous_page_ { KC_PAGE_UP, KM_SHIFT, fun(this, &Text_impl::select_page_up) };
    Action              action_select_next_page_ { KC_PAGE_DOWN, KM_SHIFT, fun(this, &Text_impl::select_page_down) };
    Action              action_select_all_ { U'A', KM_CONTROL, fun(this, &Text_impl::on_select_all) };
    Action              action_copy_ { "<Ctrl>C <Ctrl>Insert", fun(this, &Text_impl::copy) };
    Action              action_cancel_ { "Escape Cancel", fun(this, &Widget_impl::drop_focus) };

    signal<void()> *    signal_caret_motion_        = nullptr;
    signal<void()> *    signal_click_               = nullptr;

    Buffer_citer        msel_;                      // Mouse selection start.
    Buffer_citer        emsel_;                     // Mouse selection last.
    bool                caret_visible_:  1          = false;
    bool                caret_exposed_:  1          = false;
    bool                caret_refresh_:  1          = false;
    bool                caret_enabled_:  1          = false;
    bool                select_allowed_: 1          = true;

    int                 xhint_                      = 0;    // Desired x offset for up/down caret navigation.
    int                 wcaret_                     = 2;    // Default caret size, in pixels.
    Rect                rcaret_;                            // Caret rectangle.
    Color               ccaret_;                            // Caret color.

    connection          caret_cx_                   { true };
    connection          take_cx_;

private:

    void init();

    void draw_caret(Painter pr);
    void expose_caret();
    void wipe_caret();
    void show_caret();
    void hide_caret();

    std::size_t hinted_pos(std::size_t ri);

    void update_caret();
    void update_selection(Buffer_citer i, Buffer_citer j);

    bool on_mouse_down(int mbt, int mm, const Point & pt);
    bool on_mouse_up(int mbt, int mm, const Point & pt);
    void on_mouse_motion(int mm, const Point & pt);
    void on_mouse_leave();

    void on_font_changed();
    void on_focus_in();
    void on_focus_out();

    void on_caret_timer();
    void on_size_changed();
    void on_display();
    void on_select_all();
    void on_selection_changed();
};

} // namespace tau

#endif // __TAU_TEXT_IMPL_HH__

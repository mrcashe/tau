// ----------------------------------------------------------------------------
// SPDX-License-Identifier: BSD-2-Clause
// Copyright © 2014-2024 Konstantin Shmelkov <mrcashe@gmail.com>.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
// THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
// PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
// CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
// EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
// PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
// OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
// WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
// OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
// EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// ----------------------------------------------------------------------------

/// @file container-impl.cc The Container_impl class implementation.
/// The header is container-impl.hh.

#include <tau/exception.hh>
#include <tau/painter.hh>
#include <tau/theme.hh>
#include <tau/widget.hh>
#include <container-impl.hh>
#include <display-impl.hh>
#include <loop-impl.hh>
#include <painter-impl.hh>
#include <window-impl.hh>
#include <algorithm>
#include <iostream>

namespace tau {

Container_impl::Container_impl() {
    allow_focus();
    signal_size_changed_.connect(fun(this, &Container_impl::update_mouse_owner));
    signal_pdata_changed_.connect(fun(this, &Container_impl::on_pdata));
    signal_show_.connect(fun(this, &Container_impl::on_show));
    signal_hide_.connect(fun(this, &Container_impl::on_hide));
    signal_enable_.connect(fun(this, &Container_impl::on_enable));
    signal_disable_.connect(fun(this, &Container_impl::on_disable));
    signal_parent().connect(fun(this, &Container_impl::on_parent));
    signal_display_in_.connect(bind_back(fun(this, &Container_impl::on_display), true));
    signal_display_out_.connect(bind_back(fun(this, &Container_impl::on_display), false));

    action_focus_next_.set_master_action("focus-next");
    action_focus_next_.connect_before(fun(this, &Container_impl::on_focus_next));
    connect_action(action_focus_next_);

    action_focus_previous_.set_master_action("focus-previous");
    action_focus_previous_.connect_before(fun(this, &Container_impl::on_focus_previous));
    connect_action(action_focus_previous_);
}

Container_impl::~Container_impl() {
    for (auto & fac: facades_) { delete fac.w; }
    destroy();
    unparent_all();
}

Widget_ptr Container_impl::chptr(Widget_impl * wp) noexcept {
    auto i = std::find_if(children_.begin(), children_.end(), [wp](auto wip) { return wp == wip.get(); });
    return i != children_.end() ? *i : nullptr;
}

Widget_cptr Container_impl::chptr(const Widget_impl * wp) const noexcept {
    auto i = std::find_if(children_.begin(), children_.end(), [wp](auto wip) { return wp == wip.get(); });
    return i != children_.end() ? *i : nullptr;
}

void Container_impl::chk_parent(Widget_ptr wp) {
    if (!wp) { throw internal_error("Container_impl::make_child(): got a pure widget pointer"); }
    if (wp->container() && this != wp->container()) { throw user_error(str_format("Container_impl::make_child(): widget ", wp, " already has parent")); }
}

// Overriden by Layered_container.
void Container_impl::make_child(Widget_ptr wp) {
    chk_parent(wp);
    children_.push_back(wp);
    if (auto * ci = dynamic_cast<Container_impl *>(wp.get())) { containers_.push_back(ci); }
    link(wp.get(), name(wp.get()));
    wp->handle_sensitivity(enabled());
    if (has_display()) { wp->handle_display(true); }
    wp->handle_visibility(visible());
    signal_child_(wp, 1);   // TODO 0.8 enum it!
    signal_children_changed_();
}

std::size_t Container_impl::unparent(const std::list<Widget_impl *> & wps) {
    std::size_t n = 0;

    for (auto wp: wps) {
        if (auto wip = chptr(wp)) {
            ++n;
            on_child_viewable(wp, false);
            sanitize(wip);
            auto i = std::find(children_.begin(), children_.end(), wip);
            if (i != children_.end()) { children_.erase(i); }
            wp->end_modal();
            wp->drop_focus();
            wp->ungrab_mouse();
            auto j = std::find(containers_.begin(), containers_.end(), static_cast<Container_impl *>(wp));
            if (j != containers_.end()) { containers_.erase(j); }
            unlink(wp);
            if (wp == modal_child_) { modal_child_ = nullptr; }
            if (wp == focused_child_) { focused_child_ = nullptr; }
            if (wp == mouse_grabber_) { mouse_grabber_ = nullptr; }
            if (wp == mouse_owner_) { mouse_owner_ = nullptr; }
            signal_child_(wip, 0);   // TODO 0.8 enum it!
        }
    }

    if (n) {
        update_mouse_owner();
        signal_children_changed_();
    }

    return n;
}

void Container_impl::unparent_all() {
    std::size_t n = children_.size();

    if (n) {
        if (!in_destroy()) {
            for (auto wip: children_) {
                on_child_viewable(wip.get(), false);
                sanitize(wip);
                signal_child_(wip, 0);   // TODO 0.8 enum it!
            }
        }

        unlink_all();
        children_.clear();
        containers_.clear();
    }

    auto mc = modal_child_;
    auto mg = mouse_grabber_;
    auto fc = focused_child_;
    modal_child_ = nullptr;
    mouse_grabber_ = nullptr;
    focused_child_ = nullptr;
    mouse_owner_ = nullptr;

    if (n && !in_shutdown()) {
        end_modal_up(mc);
        ungrab_mouse_up(mg);
        if (mc) { mc->clear_modal(); }
        if (fc) { fc->clear_focus(); }
        if (focused()) { grab_focus(); }
        signal_children_changed_();
    }
}

void Container_impl::invalidate_children(const std::list<Widget_impl *> & wps) {
    Rect r;

    for (auto wp: wps) {
        if (wp) {
            r |= Rect(wp->origin(), wp->size());
        }
    }

    if (r) { invalidate(r); }
}

std::list<Widget_ptr> Container_impl::children() const {
    std::list<Widget_ptr> l(children_.size());
    std::copy(children_.begin(), children_.end(), l.begin());
    return l;
}

// Overrides Widget_impl.
// Overridden by Popup_impl.
Point Container_impl::to_parent(const Container_impl * ci, const Point & pt) const noexcept {
    Point cpt(pt);

    if (!in_shutdown() && ci != this && container_) {
        cpt += origin()-container_->offset();
        cpt += container_->to_parent(ci);
    }

    return cpt;
}

// Overrides Widget_impl.
// Overridden by Window_impl.
Window_impl * Container_impl::window() noexcept {
    if (!in_shutdown()) {
        if (!window_  && container_) { window_ = container_->window(); }
        return window_;
    }

    return nullptr;
}

// Overrides Widget_impl.
// Overridden by Window_impl.
const Window_impl * Container_impl::window() const noexcept {
    if (!in_shutdown()) {
        if (!window_ && container_) { window_ = container_->window(); }
        return window_;
    }

    return nullptr;
}

// Overrides Widget_impl.
bool Container_impl::handle_accel(char32_t kc, int km) {
    if (enabled()) {
        if (modal_child_) {
            return modal_child_->handle_accel(kc, km);
        }

        if (focused_child_ && focused_child_->handle_accel(kc, km)) {
            return true;
        }

        return Widget_impl::handle_accel(kc, km);
    }

    return false;
}

bool Container_impl::on_input(const ustring & s, int src) {
    if (enabled()) {
        if (modal_child_) {
            return modal_child_->handle_input(s, src);
        }

        if (focused_child_ && focused_child_->handle_input(s, src)) {
            return true;
        }

        return !signal_input_ && Widget_impl::handle_input(s, src);
    }

    return false;
}

// Overrides Widget_impl.
bool Container_impl::handle_input(const ustring & s, int src) {
    return signal_input_ ? Widget_impl::handle_input(s, src) : on_input(s, src);
}

bool Container_impl::on_key_down(char32_t kc, int km) {
    if (enabled()) {
        if (modal_child_) {
            return modal_child_->handle_key_down(kc, km);
        }

        if (focused_child_) {
            if (focused_child_->handle_key_down(kc, km)) {
                return true;
            }
        }

        return !signal_key_down_ && Widget_impl::handle_key_down(kc, km);
    }

    return false;
}

// Overrides Widget_impl.
bool Container_impl::handle_key_down(char32_t kc, int km) {
    return signal_key_down_ ? Widget_impl::handle_key_down(kc, km) : on_key_down(kc, km);
}

bool Container_impl::on_key_up(char32_t kc, int km) {
    if (enabled()) {
        if (modal_child_) {
            return modal_child_->handle_key_up(kc, km);
        }

        if (focused_child_) {
            if (focused_child_->handle_key_up(kc, km)) {
                return true;
            }
        }

        return Widget_impl::handle_key_up(kc, km);
    }

    return false;
}

// Overrides Widget_impl.
bool Container_impl::handle_key_up(char32_t kc, int km) {
    return signal_key_up_ ? Widget_impl::handle_key_up(kc, km) : on_key_up(kc, km);
}

void Container_impl::on_display(bool in) {
    if (!in_destroy()) {
        for (auto wp: children_) {
            wp->handle_display(in);
            if (in) { wp->handle_sensitivity(enabled()); wp->handle_visibility(visible()); }
        }
    }

    if (!in) {
        window_ = nullptr;
    }
}

void Container_impl::on_parent(Object * o) {
    window_ = nullptr;

    if (!o) {
        focused_child_ = nullptr;
        modal_child_ = nullptr;
        mouse_grabber_ = nullptr;
        mouse_owner_ = nullptr;
    }
}

// Private.
Widget_impl * Container_impl::mouse_target_update(const Point & pt) {
    Widget_impl * wt = mouse_target(pt);
    set_mouse_owner(wt, pt);
    return wt;
}

// Private.
void Container_impl::set_mouse_owner(Widget_impl * wi, const Point & pt) {
    if (mouse_owner_ != wi) {
        if (auto mo = mouse_owner_) {
            mouse_owner_ = nullptr;
            if (mo != mouse_grabber_) { mo->handle_mouse_leave(); }
        }

        if (wi && wi != this) {
            mouse_owner_ = wi;
            wi->handle_mouse_enter(pt-wi->origin());
        }
    }
}

void Container_impl::update_mouse_owner() {
    if (!in_shutdown()) {
        Widget_impl * mt = nullptr;
        Point pt = where_mouse();
        if (hover()) { mt = mouse_target(pt); }
        set_mouse_owner(mt, pt);
    }
}

bool Container_impl::on_mouse_down(int mbt, int mm, const Point & pt) {
    if (auto wt = mouse_target_update(pt)) {
        if (wt->handle_mouse_down(mbt, mm, pt+wt->offset()-wt->origin())) {
            return true;
        }
    }

    if (!signal_mouse_down_ && Widget_impl::handle_mouse_down(mbt, mm, pt)) {
        return true;
    }

    return false;
}

bool Container_impl::on_mouse_up(int mbt, int mm, const Point & pt) {
    if (auto wt = mouse_target_update(pt)) {
        if (wt->handle_mouse_up(mbt, mm, pt+wt->offset()-wt->origin())) {
            return true;
        }
    }

    return !signal_mouse_up_ && Widget_impl::handle_mouse_up(mbt, mm, pt);
}

bool Container_impl::on_mouse_double_click(int mbt, int mm, const Point & pt) {
    if (auto wt = mouse_target_update(pt)) {
        if (wt->handle_mouse_double_click(mbt, mm, pt+wt->offset()-wt->origin())) {
            return true;
        }
    }

    if (!signal_mouse_double_click_ && Widget_impl::handle_mouse_double_click(mbt, mm, pt)) {
        return true;
    }

    return false;
}

bool Container_impl::on_mouse_wheel(int delta, int mm, const Point & pt) {
    if (auto wt = mouse_target_update(pt)) {
        if (wt->handle_mouse_wheel(delta, mm, pt+wt->offset()-wt->origin())) {
            return true;
        }
    }

    return !signal_mouse_wheel_ && Widget_impl::handle_mouse_wheel(delta, mm, pt);
}

void Container_impl::on_mouse_motion(int mm, const Point & pt) {
    if (auto wt = mouse_target_update(pt)) {
        wt->handle_mouse_motion(mm, pt+wt->offset()-wt->origin());
    }

    else if (!signal_mouse_motion_) {
        Widget_impl::handle_mouse_motion(mm, pt);
    }
}

void Container_impl::on_mouse_enter(const Point & pt) {
    if (!signal_mouse_enter_) { Widget_impl::handle_mouse_enter(pt); }
    mouse_target_update(pt);
}

void Container_impl::on_mouse_leave() {
    set_mouse_owner(nullptr);
    if (!signal_mouse_leave_) { Widget_impl::handle_mouse_leave(); }
}

// Overrides Widget_impl.
bool Container_impl::handle_mouse_down(int mbt, int mm, const Point & pt) {
    return signal_mouse_down_ ? Widget_impl::handle_mouse_down(mbt, mm, pt) : on_mouse_down(mbt, mm, pt);
}

// Overrides Widget_impl.
bool Container_impl::handle_mouse_up(int mbt, int mm, const Point & pt) {
    return signal_mouse_up_ ? Widget_impl::handle_mouse_up(mbt, mm, pt) : on_mouse_up(mbt, mm, pt);
}

// Overrides Widget_impl.
bool Container_impl::handle_mouse_double_click(int mbt, int mm, const Point & pt) {
    return signal_mouse_double_click_ ? Widget_impl::handle_mouse_double_click(mbt, mm, pt) : on_mouse_double_click(mbt, mm, pt);
}

// Overrides Widget_impl.
bool Container_impl::handle_mouse_wheel(int delta, int mm, const Point & pt) {
    return signal_mouse_wheel_ ? Widget_impl::handle_mouse_wheel(delta, mm, pt) : on_mouse_wheel(delta, mm, pt);
}

// Overrides Widget_impl.
void Container_impl::handle_mouse_motion(int mm, const Point & pt) {
    if (signal_mouse_motion_) { Widget_impl::handle_mouse_motion(mm, pt); }
    else { on_mouse_motion(mm, pt); }
}

// Overrides Widget_impl.
void Container_impl::handle_mouse_enter(const Point & pt) {
    if (signal_mouse_enter_) { Widget_impl::handle_mouse_enter(pt); }
    else { on_mouse_enter(pt); }
}

// Overrides Widget_impl.
void Container_impl::handle_mouse_leave() {
    if (signal_mouse_leave_) { Widget_impl::handle_mouse_leave(); }
    else { on_mouse_leave(); }
}

void Container_impl::on_enable() {
    if (!in_shutdown() && enabled()) {
        for (auto wp: children_) {
            wp->handle_sensitivity(true);
        }
    }
}

void Container_impl::on_disable() {
    if (!in_shutdown() && !enabled()) {
        for (auto wp: children_) {
            wp->handle_sensitivity(false);
        }
    }
}

// Overridden by Window_impl.
// Overrides Widget_impl.
bool Container_impl::grab_mouse_up(Widget_impl * caller) {
    if (enabled()) {
        if (caller == mouse_grabber_) {
            return true;
        }

        if (container_) {
            if (this == container_->mouse_grabber()) {
                return this == caller;
            }

            if (container_->grab_mouse_up(this)) {
                if (this == caller) { ungrab_mouse_down(); }
                else { mouse_grabber_ = caller; }
                return true;
            }
        }
    }

    return false;
}

// Overridden by Window_impl.
// Overrides Widget_impl.
bool Container_impl::ungrab_mouse_up(Widget_impl * caller) {
    if (caller) {
        if (caller == mouse_grabber_) {
            mouse_grabber_ = nullptr;
        }

        if (!in_shutdown() && !mouse_grabber_ && container_) {
            return container_->ungrab_mouse_up(this);
        }
    }

    return false;
}

void Container_impl::ungrab_mouse_down() {
    if (auto mg = mouse_grabber_) {
        mouse_grabber_ = nullptr;

        if (Container_impl * ci = dynamic_cast<Container_impl *>(mg)) {
            ci->ungrab_mouse_down();
        }

        else {
            mg->handle_mouse_leave();
        }
    }

    if (container_ && this != container_->mouse_owner()) {
        Widget_impl::handle_mouse_leave();
    }
}

// Overrides Widget_impl.
// Overridden by Window_impl.
bool Container_impl::grabs_mouse() const noexcept {
    if (container_ && !mouse_grabber_) { return this == container_->mouse_grabber(); }
    return false;
}

// Overrides Widget_impl.
bool Container_impl::grabs_modal() const noexcept {
    return has_modal() && !modal_child_;
}

// TODO Remove this BS!
void Container_impl::set_modal_child(Widget_impl * caller) {
    if (modal_child_ != caller) {
        if (auto mc = modal_child_) {
            modal_child_ = nullptr;
            mc->clear_modal();
        }

        if (focused_child_ && focused_child_ != caller) {
            focused_child_->handle_focus_out();
        }

        if (this != caller) {
            modal_child_ = caller;
            caller->handle_focus_in();
        }
    }
}

// TODO Rework this!
// Overrides Widget_impl.
// Overridden by Window_impl.
bool Container_impl::grab_modal_up(Widget_impl * caller) {
    bool res = false;

    if (!modal_child_ && container_) {
        if (grabs_modal()) {
            res = this == caller;
        }

        else {
            res = container_->grab_modal_up(this);
            if (res && this != caller) { set_modal_child(caller); }
        }
    }

    return res;
}

// Overridden by Window_impl.
// Overrides Widget_impl.
bool Container_impl::end_modal_up(Widget_impl * caller) {
    if (this == caller || modal_child_ == caller) {
        if (auto mc = modal_child_) {
            modal_child_ = nullptr;
            mc->clear_modal();
        }

        if (container_ && container_->end_modal_up(this)) {
            handle_focus_out();
            return true;
        }
    }

    return false;
}

// Overridden by Window_impl.
// Overrides Widget_impl.
int Container_impl::grab_focus_up(Widget_impl * caller) {
    int res = -1;

    if (caller && focusable() && !has_modal() && container_) {
        res = container_->grab_focus_up(this);

        if (res >= 0) {
            if (caller != focused_child_) {
                if (auto fc = focused_child_) {
                    focused_child_ = nullptr;
                    fc->clear_focus();
                }
            }

            // Check focus!
            if (container_->focused_child() == this) {
                if (this != caller) { focused_child_ = caller; }
                if (res > 0) { caller->handle_focus_in(); }
            }

            // Check focus again!
            if (container_->focused_child() != this) {
                res = -1;
            }
        }
    }

    return res;
}

// Overridden by Window_impl.
// Overrides Widget_impl.
void Container_impl::drop_focus_up(Widget_impl * caller) {
    if (!in_shutdown()) {
        if (auto fc = focused_child_) {
            if (fc == caller) {
                focused_child_ = nullptr;
                fc->clear_focus();
            }
        }

        if ((this == caller || !focusable()) && container_) {
            container_->drop_focus_up(this);
        }
    }
}

// Overrides Widget_impl.
void Container_impl::suspend_focus() {
    if (auto wp = modal_child_) {
        wp->handle_focus_out();
        wp->suspend_focus();
    }

    if (auto wp = focused_child_) {
        wp->handle_focus_out();
        wp->suspend_focus();
    }
}

// Overrides Widget_impl.
void Container_impl::resume_focus() {
    if (auto wp = modal_child_) {
        wp->handle_focus_in();
        wp->resume_focus();
    }

    else if (auto wp = focused_child_) {
        wp->handle_focus_in();
        wp->resume_focus();
    }
}

// Overrides Widget_impl.
void Container_impl::clear_focus() {
    if (auto wp = focused_child_) {
        focused_child_ = nullptr;
        wp->clear_focus();
    }

    Widget_impl::clear_focus();
}

// Overrides Widget_impl.
void Container_impl::clear_modal() {
    if (auto wp = modal_child_) {
        modal_child_ = nullptr;
        wp->clear_modal();
    }

    Widget_impl::clear_modal();
}

// Overridden by Window_impl.
// Overrides Widget_impl.
void Container_impl::set_cursor_up(Cursor_ptr cursor, Widget_impl * wip) {
    if (!in_shutdown() && (!cursor_ || this == wip)) {
        container_->set_cursor_up(cursor, wip);
    }
}

// Overridden by Window_impl.
// Overrides Widget_impl.
void Container_impl::unset_cursor_up(Widget_impl * wip) {
    if (!in_shutdown() && !cursor_hidden_ && container_) {
        if (cursor_ && this != wip) {
            container_->set_cursor_up(cursor_, this);
        }

        else {
            container_->unset_cursor_up(wip);
        }
    }
}

// Overridden by Window_impl.
// Overrides Widget_impl.
void Container_impl::show_cursor_up() {
    if (!in_shutdown() && container_ && !cursor_hidden_) {
        container_->show_cursor_up();
    }
}

// Overridden by Window_impl.
void Container_impl::queue_arrange_up() {
    if (!in_shutdown() && !in_arrange_ && container_) { container_->queue_arrange_up(); }
}

void Container_impl::queue_arrange() {
    arrange_ = true;
    queue_arrange_up();
}

void Container_impl::sync_arrange() {
    if (!in_arrange_) {
        in_arrange_ = true;
        while (arrange_ && !in_shutdown() && visible()) { arrange_ = false; signal_arrange_(); }
        for (auto ci: containers_) { ci->sync_arrange(); }
        in_arrange_ = false;
    }
}

void Container_impl::on_show() {
    for (auto wp: children_) {
        if (!std::dynamic_pointer_cast<Window_impl>(wp)) {
            wp->handle_visibility(true);
        }
    }
}

void Container_impl::on_hide() {
    drop_focus();
    for (auto wp: children_) { wp->handle_visibility(false); }
}

// Overridden by Window_impl.
Widget_ptr Container_impl::focus_endpoint() noexcept {
    if (focused()) {
        auto fc = modal_child_ ? modal_child_ : focused_child_;

        if (auto ci = dynamic_cast<Container_impl *>(fc)) {
            if (auto wp = ci->focus_endpoint()) {
                return wp;
            }
        }

        if (auto cp = chptr(fc)) { return cp; }
        if (container_) { return container_->chptr(this); }
    }

    return nullptr;
}

// Overridden by Window_impl.
Widget_cptr Container_impl::focus_endpoint() const noexcept {
    if (focused()) {
        auto fc = modal_child_ ? modal_child_ : focused_child_;

        if (auto ci = dynamic_cast<const Container_impl *>(fc)) {
            if (auto wp = ci->focus_endpoint()) {
                return wp;
            }
        }

        if (auto cp = chptr(fc)) { return cp; }
        if (container_) { return container_->chptr(this); }
    }

    return nullptr;
}

bool Container_impl::update_child_bounds(Widget_impl * wp, const Rect & bounds) {
    return update_child_bounds(wp, bounds.top_left(), bounds.size());
}

bool Container_impl::update_child_bounds(Widget_impl * wp, const Point & origin, const Size & sz) {
    bool changed = wp->update_origin(origin);
    if (wp->update_size(sz)) { changed = true; }
    return changed;
}

bool Container_impl::update_child_bounds(Widget_impl * wp, int x, int y, const Size & sz) {
    return update_child_bounds(wp, Point(x, y), sz);
}

bool Container_impl::update_child_bounds(Widget_impl * wp, int x, int y, unsigned w, unsigned h) {
    return update_child_bounds(wp, Point(x, y), Size(w, h));
}

void Container_impl::on_pdata() {
    for (auto wp: children_) { wp->update_pdata(); }
}

void Container_impl::paint_children(Painter pr, const Rect & inval, Widget_impl ** wps, std::size_t nwps, void (Widget_impl::*method)(Painter, const Rect &)) {
    auto pp = pr.ptr();
    Point wpos = worigin(), sc = offset();

    for (; nwps; --nwps, ++wps) {
        auto wp = *wps;
        Point worg = wp->origin();
        Rect  wbounds(worg-sc, wp->size());
        Rect  intersection = inval & wbounds;

        if (intersection) {
            pp->wpush();
            pp->poffset(wp->poffset());
            pp->set_viewable_area(intersection.translated(wpos));
            pp->push();
            pp->clear();
            Rect cinval(intersection.translated(sc-worg));
            (wp->*method)(pr, cinval);
            pp->pop();
            pp->wpop();
        }
    }
}

void Container_impl::on_focus_chain(Widget_impl * wp, bool in, bool before) {
    if (in) { chain_ = wp; }
    else if (wp == chain_) { chain_ = nullptr; }
}

void Container_impl::set_focus_chain(Widget_impl * wp, Widget_impl * other, bool before) {
    if (wp != other) {
        if (other->focused()) { chain_ = other; }

        {
            auto & fwd(before ? before_ : after_);
            auto in_cx = other->signal_focus_in().connect(bind_back(fun(this, &Container_impl::on_focus_chain), other, true, before));
            auto out_cx = other->signal_focus_out().connect(bind_back(fun(this, &Container_impl::on_focus_chain), other, false, before));
            auto unparent_cx = other->signal_display_out().connect(bind_back(fun(this, &Container_impl::unchain_focus), other));
            auto unchain_cx = other->signal_unchain().connect(bind_back(fun(this, &Container_impl::unchain_focus), other));
            fwd[other] = { wp, in_cx, out_cx, unparent_cx, unchain_cx };
        }

        {
            auto & rev(before ? after_ : before_);
            auto in_cx = wp->signal_focus_in().connect(bind_back(fun(this, &Container_impl::on_focus_chain), wp, true, !before));
            auto out_cx = wp->signal_focus_out().connect(bind_back(fun(this, &Container_impl::on_focus_chain), wp, false, !before));
            auto unparent_cx = wp->signal_display_out().connect(bind_back(fun(this, &Container_impl::unchain_focus), wp));
            auto unchain_cx = wp->signal_unchain().connect(bind_back(fun(this, &Container_impl::unchain_focus), wp));
            rev[wp] = { other, in_cx, out_cx, unparent_cx, unchain_cx };
        }
    }
}

void Container_impl::unchain_focus(Widget_impl * wp) {
    before_.erase(wp);
    after_.erase(wp);
    if (wp == chain_) { chain_ = nullptr; }
    std::erase_if(before_, [wp](auto & p) { return p.second.wp == wp; } );
    std::erase_if(after_, [wp](auto & p) { return p.second.wp == wp; } );
}

bool Container_impl::on_focus_next() {
    if (chain_) {
        auto i = after_.find(chain_);

        while (i != after_.end()) {
            if (i->second.wp->take_focus()) { return false; }
            i = after_.find(i->second.wp);
        }
    }

    return true;
}

bool Container_impl::on_focus_previous() {
    if (chain_) {
        auto i = before_.find(chain_);

        while (i != before_.end()) {
            if (i->second.wp->take_focus()) { return false; }
            i = before_.find(i->second.wp);
        }
    }

    return true;
}

void Container_impl::trigger_sanitize_if() {
    if (sanitize_.empty() && facades_.empty()) {
        trigger_sanitize();
    }
}

void Container_impl::trigger_sanitize() {
    sanitize_cx_.drop();
    Loop_impl::this_ptr()->alarm(fun(this, &Container_impl::on_sanitize), CONTAINER_SANITIZE);  // defs.hh
}

// Slot for signal_alarm().
void Container_impl::on_sanitize() {
    if (std::any_of(sanitize_.begin(), sanitize_.end(), [](auto & san) { return san.wp->running(); }) ||
        std::any_of(facades_.begin(), facades_.end(), [](auto & fac) { return fac.w->ptr().use_count() > 2 || fac.w->ptr()->running(); })) {
        trigger_sanitize();
    }

    else {
        sanitize_cx_ = connect_run(fun(this, &Container_impl::on_sanitize_run));
    }
}

void Container_impl::on_sanitize_run() {
    if (!sanitize_.empty()) { sanitize_.pop_back(); }
    if (!facades_.empty()) { auto w = facades_.back().w; facades_.pop_back(); delete w; }
    if (sanitize_.empty() && facades_.empty()) { sanitize_cx_.drop(); }
}

void Container_impl::sanitize(Widget_ptr wp) {
    if (container_) {
        container_->sanitize(wp);
    }

    else {
        trigger_sanitize_if();
        auto & san = sanitize_.emplace_back();
        san.wp = wp, san.cx = wp->signal_parent().connect(tau::bind_front(fun(this, &Container_impl::on_san_parent), wp.get()));
    }
}

void Container_impl::sanitize(Widget * w) {
    trigger_sanitize_if();
    auto i = std::find_if(mans_.begin(), mans_.end(), [w](auto & fac) { return fac.w == w; });
    if (i != mans_.end()) { mans_.erase(i); }
    auto & fac = facades_.emplace_back();
    fac.w = w, fac.cx = w->signal_parent().connect(tau::bind_front(fun(this, &Container_impl::on_managed_parent), w));
}

void Container_impl::on_tooltip_timer() {
    if (auto wip = window()) {
        auto wp = wip->tooltip_owner();
        if (!wp || !wp->has_parent(this)) { expose_tooltip(); }
    }
}

void Container_impl::on_managed_parent(Widget * w, Object * o) {
    // FIXME How to replace this to in_destroy()? Don't work at least on FreeBSD-13.1.
    if (!o) {
        if (w->has_parent()) {
            if (!in_shutdown()) {
                sanitize(w);
            }
        }
    }

    else {
        auto i = std::find_if(facades_.begin(), facades_.end(), [w](auto & fac) { return w == fac.w; });
        if (i != facades_.end()) { facades_.erase(i); }
        manage(w);
    }
}

void Container_impl::on_san_parent(Widget_impl * wp, Object *o ) {
    // FIXME How to replace this to in_destroy()? Don't work at least on FreeBSD-13.1.
    if (o) {
        if (!in_shutdown()) {
            auto i = std::find_if(sanitize_.begin(), sanitize_.end(), [wp](auto & san) { return wp == san.wp.get(); });
            if (i != sanitize_.end()) { sanitize_.erase(i); }
        }
    }
}

Widget & Container_impl::manage(Widget * w) {
    auto & man = mans_.emplace_back();
    man.w = w;
    man.cx = w->ptr()->signal_parent().connect(tau::bind_front(fun(this, &Container_impl::on_managed_parent), w));
    return *w;
}

// Overrides Widget_impl.
signal<bool(int, int, Point)> & Container_impl::signal_mouse_down() {
    auto & sig = Widget_impl::signal_mouse_down();
    if (sig.empty()) { sig.connect(fun(this, &Container_impl::on_mouse_down)); }
    return sig;
}

// Overrides Widget_impl.
signal<bool(int, int, Point)> & Container_impl::signal_mouse_double_click() {
    auto & sig = Widget_impl::signal_mouse_double_click();
    if (sig.empty()) { sig.connect(fun(this, &Container_impl::on_mouse_double_click)); }
    return sig;
}

// Overrides Widget_impl.
signal<bool(int, int, Point)> & Container_impl::signal_mouse_up() {
    auto & sig = Widget_impl::signal_mouse_up();
    if (sig.empty()) { sig.connect(fun(this, &Container_impl::on_mouse_up)); }
    return sig;
}

// Overrides Widget_impl.
signal<void(int, Point)> & Container_impl::signal_mouse_motion() {
    auto & sig = Widget_impl::signal_mouse_motion();
    if (sig.empty()) { sig.connect(fun(this, &Container_impl::on_mouse_motion)); }
    return sig;
}

// Overrides Widget_impl.
signal<void(Point)> & Container_impl::signal_mouse_enter() {
    auto & sig = Widget_impl::signal_mouse_enter();
    if (sig.empty()) { sig.connect(fun(this, &Container_impl::on_mouse_enter)); }
    return sig;
}

// Overrides Widget_impl.
signal<void()> & Container_impl::signal_mouse_leave() {
    auto & sig = Widget_impl::signal_mouse_leave();
    if (sig.empty()) { sig.connect(fun(this, &Container_impl::on_mouse_leave)); }
    return sig;
}

// Overrides Widget_impl.
signal<bool(int, int, Point)> & Container_impl::signal_mouse_wheel() {
    auto & sig = Widget_impl::signal_mouse_wheel();
    if (sig.empty()) { sig.connect(fun(this, &Container_impl::on_mouse_wheel)); }
    return sig;
}

// Overrides Widget_impl.
signal<bool(char32_t, int)> & Container_impl::signal_key_down() {
    auto & sig = Widget_impl::signal_key_down();
    if (sig.empty()) { sig.connect(fun(this, &Container_impl::on_key_down)); }
    return sig;
}

// Overrides Widget_impl.
signal<bool(char32_t, int)> & Container_impl::signal_key_up() {
    auto & sig = Widget_impl::signal_key_up();
    if (sig.empty()) { sig.connect(fun(this, &Container_impl::on_key_up)); }
    return sig;
}

// Overrides Widget_impl.
signal<bool(const ustring &, int)> & Container_impl::signal_input() {
    auto & sig = Widget_impl::signal_input();
    if (sig.empty()) { sig.connect(fun(this, &Container_impl::on_input)); }
    return sig;
}

} // namespace tau

//END

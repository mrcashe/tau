// ----------------------------------------------------------------------------
// SPDX-License-Identifier: BSD-2-Clause
// Copyright © 2014-2024 Konstantin Shmelkov <mrcashe@gmail.com>.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
// THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
// PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
// CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
// EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
// PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
// OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
// WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
// OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
// EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// ----------------------------------------------------------------------------

/// @file defs-impl.hh Miscellaneous defines.

#ifndef __TAU_DEFS_IMPL_HH__
#define __TAU_DEFS_IMPL_HH__

#include <tau/defs.hh>

/// Caret blink timeout, in milliseconds.
#define CARET_TIMEOUT           511

/// Timeout used for Window_impl::queue_arrange(), in millisecons.
#define ARRANGE_TIMEOUT         31

/// Timeout used for Scroller_impl::update_pan(), in milliseconds
#define PAN_TIMEOUT             17

/// Minimal Slider size, pixels.
#define MIN_SLIDER              5

/// Image_impl gray pixmap creation delay, in milliseconds.
#define IMAGE_GRAY_DELAY        37

/// Navigator cache size, elements.
#define NAVI_CACHE_SIZE         32

/// Container sanitize delay, in milliseconds.
#define CONTAINER_SANITIZE      33

/// File monitor delay, in milliseconds.
#define WATCH_DELAY             23

namespace tau {

class Absolute_impl;
using Absolute_ptr = std::shared_ptr<Absolute_impl>;
using Absolute_cptr = std::shared_ptr<const Absolute_impl>;

class Bin_impl;
using Bin_ptr = std::shared_ptr<Bin_impl>;
using Bin_cptr = std::shared_ptr<const Bin_impl>;

class Box_impl;
using Box_ptr = std::shared_ptr<Box_impl>;
using Box_cptr = std::shared_ptr<const Box_impl>;

class Button_base_impl;
using Button_base_ptr = std::shared_ptr<Button_base_impl>;
using Button_base_cptr = std::shared_ptr<const Button_base_impl>;

class Button_impl;
using Button_ptr = std::shared_ptr<Button_impl>;
using Button_cptr = std::shared_ptr<const Button_impl>;

class Card_impl;
using Card_ptr = std::shared_ptr<Card_impl>;
using Card_cptr = std::shared_ptr<const Card_impl>;

class Check_impl;
using Check_ptr = std::shared_ptr<Check_impl>;
using Check_cptr = std::shared_ptr<const Check_impl>;

class Check_menu_impl;
using Check_menu_ptr = std::shared_ptr<Check_menu_impl>;
using Check_menu_cptr = std::shared_ptr<const Check_menu_impl>;

class Colorsel_impl;
using Colorsel_ptr = std::shared_ptr<Colorsel_impl>;
using Colorsel_cptr = std::shared_ptr<const Colorsel_impl>;

class Container_impl;

class Counter_impl;
using Counter_ptr = std::shared_ptr<Counter_impl>;
using Counter_cptr = std::shared_ptr<const Counter_impl>;

class Cycle_impl;
using Cycle_ptr = std::shared_ptr<Cycle_impl>;
using Cycle_cptr = std::shared_ptr<const Cycle_impl>;

class Cycle_text_impl;
using Cycle_text_ptr = std::shared_ptr<Cycle_text_impl>;
using Cycle_text_cptr = std::shared_ptr<const Cycle_text_impl>;

class Dialog_impl;
using Dialog_ptr = std::shared_ptr<Dialog_impl>;
using Dialog_cptr = std::shared_ptr<const Dialog_impl>;

class Edit_impl;
using Edit_ptr = std::shared_ptr<Edit_impl>;
using Edit_cptr = std::shared_ptr<const Edit_impl>;

class Entry_impl;
using Entry_ptr = std::shared_ptr<Entry_impl>;
using Entry_cptr = std::shared_ptr<const Entry_impl>;

class Fontsel_impl;
using Fontsel_ptr = std::shared_ptr<Fontsel_impl>;
using Fontsel_cptr = std::shared_ptr<const Fontsel_impl>;

class Frame_impl;
using Frame_ptr = std::shared_ptr<Frame_impl>;
using Frame_cptr = std::shared_ptr<const Frame_impl>;

class Header_impl;
using Header_ptr = std::shared_ptr<Header_impl>;
using Header_cptr = std::shared_ptr<const Header_impl>;

class Icon_impl;
using Icon_ptr = std::shared_ptr<Icon_impl>;
using Icon_cptr = std::shared_ptr<const Icon_impl>;

class Image_impl;
using Image_ptr = std::shared_ptr<Image_impl>;
using Image_cptr = std::shared_ptr<const Image_impl>;

class Label_impl;
using Label_ptr = std::shared_ptr<Label_impl>;
using Label_cptr = std::shared_ptr<const Label_impl>;

class List_impl;
using List_ptr = std::shared_ptr<List_impl>;
using List_cptr = std::shared_ptr<const List_impl>;

class List_text_impl;
using List_text_ptr = std::shared_ptr<List_text_impl>;
using List_text_cptr = std::shared_ptr<const List_text_impl>;

class Menu_impl;
using Menu_wptr = std::weak_ptr<Menu_impl>;
using Menu_ptr = std::shared_ptr<Menu_impl>;
using Menu_cptr = std::shared_ptr<const Menu_impl>;

class Menubar_impl;
using Menubar_ptr = std::shared_ptr<Menubar_impl>;
using Menubar_cptr = std::shared_ptr<const Menubar_impl>;

class Menubox_impl;
using Menubox_ptr = std::shared_ptr<Menubox_impl>;
using Menubox_cptr = std::shared_ptr<const Menubox_impl>;

class Menu_item_impl;
using Menu_item_ptr = std::shared_ptr<Menu_item_impl>;
using Menu_item_cptr = std::shared_ptr<const Menu_item_impl>;

class Navigator_impl;
using Navigator_ptr = std::shared_ptr<Navigator_impl>;
using Navigator_cptr = std::shared_ptr<const Navigator_impl>;

class Observer_impl;
using Observer_ptr = std::shared_ptr<Observer_impl>;
using Observer_cptr = std::shared_ptr<const Observer_impl>;

class Popup_impl;
using Popup_ptr = std::shared_ptr<Popup_impl>;
using Popup_cptr = std::shared_ptr<const Popup_impl>;

class Progress_impl;
using Progress_ptr = std::shared_ptr<Progress_impl>;
using Progress_cptr = std::shared_ptr<const Progress_impl>;

class Roller_impl;
using Roller_ptr = std::shared_ptr<Roller_impl>;
using Roller_cptr = std::shared_ptr<const Roller_impl>;

class Scroller_impl;
using Scroller_ptr = std::shared_ptr<Scroller_impl>;
using Scroller_cptr = std::shared_ptr<const Scroller_impl>;

class Separator_impl;
using Separator_ptr = std::shared_ptr<Separator_impl>;
using Separator_cptr = std::shared_ptr<const Separator_impl>;

class Slider_impl;
using Slider_ptr = std::shared_ptr<Slider_impl>;
using Slider_cptr = std::shared_ptr<const Slider_impl>;

class Spinner_impl;
using Spinner_ptr = std::shared_ptr<Spinner_impl>;
using Spinner_cptr = std::shared_ptr<const Spinner_impl>;

class Submenu_impl;
using Submenu_ptr = std::shared_ptr<Submenu_impl>;
using Submenu_cptr = std::shared_ptr<const Submenu_impl>;

class Table_impl;
using Table_ptr = std::shared_ptr<Table_impl>;
using Table_cptr = std::shared_ptr<const Table_impl>;

class Text_impl;
using Text_ptr = std::shared_ptr<Text_impl>;
using Text_cptr = std::shared_ptr<const Text_impl>;

class Toggle_impl;
using Toggle_ptr = std::shared_ptr<Toggle_impl>;
using Toggle_cptr = std::shared_ptr<const Toggle_impl>;

class Toggle_action_impl;
using Toggle_action_ptr = std::shared_ptr<Toggle_action_impl>;
using Toggle_action_cptr = std::shared_ptr<const Toggle_action_impl>;

class Toplevel_impl;
using Toplevel_ptr = std::shared_ptr<Toplevel_impl>;
using Toplevel_cptr = std::shared_ptr<const Toplevel_impl>;

class True_type_font;
using True_type_ptr = std::shared_ptr<True_type_font>;
using True_type_cptr = std::shared_ptr<const True_type_font>;

class Twins_impl;
using Twins_ptr = std::shared_ptr<Twins_impl>;
using Twins_cptr = std::shared_ptr<const Twins_impl>;

class Window_impl;
using Window_ptr = std::shared_ptr<Window_impl>;
using Window_cptr = std::shared_ptr<const Window_impl>;

struct Winface;
using Winface_ptr = std::shared_ptr<Winface>;
using Winface_cptr = std::shared_ptr<const Winface>;

} // namespace tau

#endif // ifndef __TAU_DEFS_IMPL_HH__

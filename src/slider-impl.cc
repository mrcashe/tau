// ----------------------------------------------------------------------------
// SPDX-License-Identifier: BSD-2-Clause
// Copyright © 2014-2024 Konstantin Shmelkov <mrcashe@gmail.com>.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
// THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
// PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
// CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
// EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
// PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
// OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
// WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
// OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
// EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// ----------------------------------------------------------------------------

/// @file slider-impl.cc The Slider implementation (Slider_impl).
/// Headers are:
/// - slider.hh
/// - slider-impl.hh

#include <tau/brush.hh>
#include <tau/painter.hh>
#include <tau/pen.hh>
#include <tau/timeval.hh>
#include <button-impl.hh>
#include <icon-impl.hh>
#include <scroller-impl.hh>
#include <slider-impl.hh>
#include <iostream>

namespace tau {

Slider_impl::Slider_impl():
    Box_impl(Orientation::RIGHT),
    orient_auto_(true),
    autohide_(false)
{
    init(nullptr);
}

Slider_impl::Slider_impl(Orientation orient, bool autohide):
    Box_impl(orient),
    orient_auto_(false),
    autohide_(autohide)
{
    init(nullptr);
}

Slider_impl::Slider_impl(Scroller_impl * scroller):
    Box_impl(Orientation::RIGHT),
    orient_auto_(true),
    autohide_(false)
{
    init(scroller);
}

Slider_impl::Slider_impl(Scroller_impl * scroller, Orientation orient, bool autohide):
    Box_impl(orient),
    orient_auto_(false),
    autohide_(autohide)
{
    init(scroller);
}

void Slider_impl::init(Scroller_impl * scroller) {
    disallow_focus();

    start_ = std::make_shared<Button_impl>();
    start_->conf().integer(Conf::RADIUS) = 0;
    start_->enable_repeat();
    start_->connect(bind_back(fun(this, &Slider_impl::do_step), false));
    start_->signal_hints_changed().connect(fun(this, &Slider_impl::on_button_hints));
    append(start_, true);

    area_ = std::make_shared<Widget_impl>();
    area_->conf().signal_changed(Conf::SLIDER_FOREGROUND).connect(fun(this, &Slider_impl::paint_now));
    area_->conf().signal_changed(Conf::SLIDER_BACKGROUND).connect(fun(this, &Slider_impl::paint_now));
    area_->conf().redirect(Conf::BACKGROUND, Conf::SLIDER_BACKGROUND);
    area_->conf().redirect(Conf::FOREGROUND, Conf::SLIDER_FOREGROUND);
    area_->signal_paint().connect(fun(this, &Slider_impl::on_area_paint));
    area_->signal_mouse_down().connect(fun(this, &Slider_impl::on_area_mouse_down));
    area_->signal_mouse_up().connect(fun(this, &Slider_impl::on_area_mouse_up));
    area_->signal_mouse_motion().connect(fun(this, &Slider_impl::on_area_mouse_motion));
    area_->signal_mouse_wheel().connect(fun(this, &Slider_impl::on_area_mouse_wheel));
    area_->signal_mouse_enter().connect(fun(this, &Slider_impl::on_area_mouse_enter));
    area_->signal_mouse_leave().connect(fun(this, &Slider_impl::on_area_mouse_leave));
    area_->signal_size_changed().connect(fun(this, &Slider_impl::on_size_changed));
    append(area_, false);

    end_ = std::make_shared<Button_impl>();
    end_->conf().integer(Conf::RADIUS) = 0;
    end_->enable_repeat();
    end_->connect(bind_back(fun(this, &Slider_impl::do_step), true));
    end_->signal_hints_changed().connect(fun(this, &Slider_impl::on_button_hints));
    append(end_, true);

    signal_orientation_changed().connect(fun(this, &Slider_impl::update_arrows));

    if (autohide_) {
        disappear();
        autohide_cx_ = tmr_.connect(bind_back(fun(this, &Slider_impl::allow_autohide), true));
        tmr_.start(551);
    }

    if (scroller) { set_scroller(scroller, autohide_); }
    show_buttons();
    update_arrows();
}

void Slider_impl::update_arrows() {
    start_->set_image(std::make_shared<Icon_impl>(horizontal() ? "go-previous" : "go-up", Icon::NANO));
    end_->set_image(std::make_shared<Icon_impl>(horizontal() ? "go-next" : "go-down", Icon::NANO));
}

void Slider_impl::set_location(double loc) {
    if (loc_ != loc) {
        loc_ = loc;
        signal_location_changed_(loc);
        paint_now();

        if (horizontal()) {
            double rng = pansize_ ? double(size_.width())/pansize_.width() : 1.0;
            double d = pansize_.width()*std::clamp(loc_, 0.0, 1.0-rng);
            signal_pan_x_(d);
        }

        else {
            double rng = pansize_ ? double(size_.height())/pansize_.height() : 1.0;
            double d = pansize_.height()*std::clamp(loc_, 0.0, 1.0-rng);
            signal_pan_y_(d);
        }
    }
}

void Slider_impl::set_range(double range) {
    if (range_ != range) {
        range_ = range;
        paint_now();
    }
}

void Slider_impl::set(double loc, double range) {
    bool changed = false;

    if (range_ != range) {
        changed = true;
        range_ = range;
    }

    if (loc_ != loc) {
        loc_ = loc;
        changed = true;
        signal_location_changed_(loc);
    }

    if (changed) { paint_now(); }
}

// Do one step from mouse click on arrows up/left or down/right.
void Slider_impl::do_step(bool increase) {
    if (enabled()) {
        double loc = std::clamp(increase ? loc_+step_ : loc_-step_, 0.0, 1.0);
        if (loc != loc_) { set_location(loc); }
    }
}

bool Slider_impl::mouse_on_slider(const Point & pt) const {
    if (area_->hover() && area_->size()) {
        double l = horizontal() ? pt.x()/double(area_->size().width()) : pt.y()/double(area_->size().height());
        return l >= loc_ && l < loc_+range_;
    }

    return false;
}

void Slider_impl::on_area_mouse_leave() {
    area_->unset_cursor();
    bool m = false;

    if (mouse_on_slider_) {
        mouse_on_slider_ = false;
        m = true;
    }

    if (press_) {
        press_ = false;
        m = true;
    }

    if (m) {
        paint_now();
    }
}

void Slider_impl::on_area_mouse_enter(const Point & pt) {
    if (mouse_on_slider(pt)) {
        if (!mouse_on_slider_) {
            mouse_on_slider_ = true;
            paint_now();
        }
    }
}

bool Slider_impl::on_area_mouse_up(int mbt, int mm, const Point & pt) {
    if (MBT_LEFT == mbt) {
        if (area_->grabs_mouse()) {
            area_->ungrab_mouse();
            area_->unset_cursor();

            if (press_) {
                press_ = false;
                paint_now();
            }
        }

        return true;
    }

    return false;
}

bool Slider_impl::on_area_mouse_down(int mbt, int mm, const Point & pt) {
    if (MBT_LEFT == mbt) {
        // Click onto slider rectangle.
        // Start drag the slider.
        // Simply fix mouse position here.
        if (mouse_on_slider(pt)) {
            if (!press_) {
                press_ = true;
                area_->grab_mouse();
                area_->set_cursor(horizontal() ? "size_hor" : "size_ver");
                mloc_ = horizontal() ? pt.x() : pt.y();
                paint_now();
            }
        }

        // The click point is outside of slider rectangle.
        // Move slider to the click position.
        else {
            if (area_->size()) {
                double l = horizontal() ? pt.x()/double(area_->size().width()) : pt.y()/double(area_->size().height());
                if (l < range_) { l = 0.0; }
                else if (l > 1.0-range_) { l = 1.0-range_; }
                else { l -= range_/2; }
                set_location(l);
            }
        }

        return true;
    }

    return false;
}

void Slider_impl::on_area_mouse_motion(int mm, const Point & pt) {
    if (area_->grabs_mouse()) {
        if (area_->size()) {
            if (horizontal()) {
                double d = pt.x()-mloc_;
                mloc_ = pt.x();
                set_location(std::max(0.0, std::min(1.0-range_, loc_+d/area_->size().width())));
            }

            else {
                double d = pt.y()-mloc_;
                mloc_ = pt.y();
                set_location(std::max(0.0, std::min(1.0-range_, loc_+d/area_->size().height())));
            }
        }
    }

    else {
        if (mouse_on_slider(pt)) {
            if (!mouse_on_slider_) {
                mouse_on_slider_ = true;
                paint_now();
            }
        }

        else {
            if (mouse_on_slider_) {
                mouse_on_slider_ = false;
                paint_now();
            }
        }

        if (press_) {
            press_ = false;
            paint_now();
        }
    }
}

bool Slider_impl::on_area_mouse_wheel(int delta, int mm, const Point & where) {
    if (delta < 0) { set_location(std::clamp(loc_-step_, 0.0, 1.0-range_)); }
    else if (delta > 0) { set_location(std::clamp(loc_+step_, 0.0, 1.0-range_)); }
    return false;
}

// Here is painter from area_ child.
void Slider_impl::draw_slider(Painter pr, bool erase_bkgnd) {
    if (Rect r = Rect(area_->size())) {
        int x1, y1, x2, y2;

        if (erase_bkgnd) {
            pr.rectangle(r.left(), r.top(), r.right(), r.bottom(), conf().integer(Conf::RADIUS));
            pr.set_brush(area_->conf().brush(Conf::BACKGROUND));
            pr.fill();
        }

        if (horizontal()) {
            int rng = range_*r.width();
            rng = std::max(rng, MIN_SLIDER);    // See defs-impl.hh
            int xmax = r.width()-rng;
            int x0 = loc_*r.width();
            x1 = std::min(x0, xmax);
            y1 = 0;
            x2 = x1+rng;
            y2 = y1+r.height()-1;
        }

        else {
            int rng = range_*r.height();
            rng = std::max(rng, MIN_SLIDER);    // See defs-impl.hh
            int ymax = r.height()-rng;
            int y0 = loc_*r.height();
            x1 = 0;
            y1 = std::min(y0, ymax);
            x2 = x1+r.width()-1;
            y2 = y1+rng;
        }

        Color c = area_->conf().color(Conf::SLIDER_FOREGROUND);
        if (press_) { c.darker(0.1); }
        else if (mouse_on_slider_) { c.lighter(0.1); }

        pr.rectangle(x1, y1, x2, y2, 71*conf().integer(Conf::RADIUS)/100);
        pr.set_brush(Brush(c));
        pr.fill_preserve();

        c.darker(0.15);
        pr.set_pen(Pen(c));
        pr.stroke();
    }
}

void Slider_impl::paint_now() {
    if (visible()) {
        if (Painter pr = area_->painter()) { draw_slider(pr, true); }
        else { area_->invalidate(); }
    }
}

bool Slider_impl::on_area_paint(Painter pr, const Rect & inval) {
    draw_slider(pr, true);
    return false;
}

void Slider_impl::on_size_changed() {
    if (!hidden()) {
        Size z = area_->size();
        if (orient_auto_) { orientation(z.width() > 8*z.height() ? Orientation::RIGHT : Orientation::DOWN); }

        // Automatically hide/show buttons when it's not enought space avail.
        if (start_ && end_) {
            unsigned s = horizontal() ? z.width() : z.height(), smin = 2*MIN_SLIDER; // See defs-impl.hh.
            smin += horizontal() ? start_->required_size().width() : start_->required_size().height();
            smin += horizontal() ? end_->required_size().width() : end_->required_size().height();
            start_->par_show(s >= smin), end_->par_show(s >= smin);
            area_->hint_min_size(s >= smin && start_ ? start_->required_size().max() : MIN_SLIDER);
        }
    }
}

void Slider_impl::show_buttons() {
    if (!buttons_visible_) {
        buttons_visible_ = true;
        if (start_) { start_->show(); }
        if (end_) { end_->show(); }
        area_->hint_min_size(start_ ? start_->required_size().max() : MIN_SLIDER);
    }
}

void Slider_impl::hide_buttons() {
    if (buttons_visible_) {
        buttons_visible_ = false;
        if (start_) { start_->hide(); }
        if (end_) { end_->hide(); }
        area_->hint_min_size(start_ ? start_->required_size().max() : MIN_SLIDER);
    }
}

void Slider_impl::update_scroller(Scroller_impl * scroller) {
    Size lsize = scroller->pan_size(), size = scroller->size(), max(lsize-size);
    range_ = lsize ? (horizontal() ? double(size.width())/lsize.width() : double(size.height())/lsize.height()) : 1.0;

    Point ofs = scroller->pan();
    loc_ = lsize ? (horizontal() ? double(ofs.x())/lsize.iwidth() : double(ofs.y())/lsize.iheight()) : 0.0;
    paint_now();

    if (start_ && end_) {
        if (horizontal()) {
            start_->par_enable(ofs.x() > 0);
            end_->par_enable(ofs.x() < max.iwidth());
        }

        else {
            start_->par_enable(ofs.y() > 0);
            end_->par_enable(ofs.y() < max.iheight());
        }
    }
}

void Slider_impl::on_scroller_changed(Scroller_impl * scroller) {
    pansize_ = scroller->pan_size(), size_ = scroller->size();
    Size max(pansize_-size_);
    if (autohide_ && ( (horizontal() && !max.width()) || (!horizontal() && !max.height()) )) { tmr_.start(31); }
    else { tmr_.stop(); appear(); }
    update_scroller(scroller);
}

void Slider_impl::set_scroller(Scroller_impl * scroller, bool autohide) {
    autohide_ = autohide;
    pansize_ = scroller->pan_size(), size_ = scroller->size();
    size_changed_cx_ = scroller->signal_size_changed().connect(bind_back(fun(this, &Slider_impl::on_scroller_changed), scroller));
    pan_size_changed_cx_ = scroller->signal_pan_size_changed().connect(bind_back(fun(this, &Slider_impl::on_scroller_changed), scroller));
    pan_changed_cx_ = scroller->signal_pan_changed().connect(bind_back(fun(this, &Slider_impl::on_scroller_changed), scroller));
    signal_show_.connect(bind_back(fun(this, &Slider_impl::update_scroller), scroller));
    pan_x_cx_ = signal_pan_x_.connect(fun(scroller, &Scroller_impl::pan_x));
    pan_y_cx_ = signal_pan_y_.connect(fun(scroller, &Scroller_impl::pan_y));
    on_scroller_changed(scroller);
}

void Slider_impl::allow_autohide(bool force) {
    if (!autohide_ || force) {
        autohide_ = true;
        autohide_cx_ = tmr_.connect(fun(this, &Slider_impl::disappear));
    }
}

void Slider_impl::disallow_autohide() {
    if (autohide_) {
        autohide_ = false;
        autohide_cx_.drop();
    }
}

void Slider_impl::on_button_hints(Hints op) {
    if (Hints::SIZE == op) {
        area_->hint_size(horizontal() ? 0 : std::max(start_->required_size().width(), end_->required_size().width()), horizontal() ? std::max(start_->required_size().height(), end_->required_size().height()) : 0);
    }
}

} // namespace tau

//END

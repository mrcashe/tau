// ----------------------------------------------------------------------------
// SPDX-License-Identifier: BSD-2-Clause
// Copyright © 2014-2024 Konstantin Shmelkov <mrcashe@gmail.com>.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
// THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
// PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
// CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
// EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
// PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
// OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
// WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
// OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
// EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// ----------------------------------------------------------------------------

#include <tau/brush.hh>
#include <tau/font.hh>
#include <tau/glyph.hh>
#include <tau/painter.hh>
#include <tau/pen.hh>
#include <tau/pixmap.hh>
#include <font-impl.hh>
#include <glyph-impl.hh>
#include <painter-impl.hh>
#include <iostream>

namespace {

tau::Matrix dummy_matrix;

void log(const char * s) {
    std::cerr << "tau::Painter::" << s << "() called on pure Painter" << std::endl;
}

} // anonymous namespace

namespace tau {

Painter::Painter() {}

Painter::~Painter() {}

Painter::Painter(Painter_ptr pimpl):
    impl(pimpl)
{
}

Painter::operator bool() const noexcept {
    return nullptr != impl;
}

void Painter::reset() {
    impl.reset();
}

Painter_ptr Painter::ptr() {
    return impl;
}

Painter_cptr Painter::ptr() const {
    return impl;
}

void Painter::set_font(Font font) {
    if (impl) { impl->set_font(font.ptr()); }
    else { log(__func__); }
}

Font Painter::select_font(const ustring & font_spec) {
    if (impl) { return impl->select_font(font_spec); }
    log(__func__);
    return Font();
}

Font Painter::font() {
    if (impl) { return impl->font(); }
    log(__func__);
    return Font();
}

Vector Painter::text_size(const ustring & s) const {
    if (impl) { return impl->text_size(s); }
    log(__func__);
    return Vector();
}

Vector Painter::text_size(const std::u32string & s) const {
    if (impl) { return impl->text_size(s); }
    log(__func__);
    return Vector();
}

Vector Painter::text_size(const std::wstring & s) const {
    if (impl) { return impl->text_size(s); }
    log(__func__);
    return Vector();
}

std::vector<int> Painter::offsets(const ustring & s) const {
    if (impl) { return impl->offsets(s); }
    log(__func__);
    return std::vector<int>();
}

std::vector<int> Painter::offsets(const std::u32string & s) const {
    if (impl) { return impl->offsets(s); }
    log(__func__);
    return std::vector<int>();
}

void Painter::text(const ustring & s, const Color & c) {
    if (impl) { impl->text(s, c); }
    else { log(__func__); }
}

void Painter::text(const std::u32string & s, const Color & c) {
    if (impl) { impl->text(s, c); }
    else { log(__func__); }
}

void Painter::text(const std::wstring & s, const Color & c) {
    if (impl) { impl->text(s, c); }
    else { log(__func__); }
}

void Painter::text(std::wstring && s, const Color & c) {
    if (impl) { impl->text(std::move(s), c); }
    else { log(__func__); }
}

void Painter::glyph(Glyph glyph) {
    if (impl) { impl->glyph(glyph.ptr()); }
    else { log(__func__); }
}

Matrix & Painter::matrix() const {
    if (impl) { return impl->matrix(); }
    log(__func__);
    return dummy_matrix;
}

void Painter::set_pen(Pen pen) {
    if (impl) { impl->set_pen(pen); }
    else { log(__func__); }
}

Pen Painter::pen() const {
    if (impl) { return impl->pen(); }
    log(__func__);
    return Pen();
}

void Painter::set_brush(Brush brush) {
    if (impl) { impl->set_brush(brush); }
    else { log(__func__); }
}

Brush Painter::brush() const {
    if (impl) { return impl->brush(); }
    log(__func__);
    return Brush();
}

void Painter::paint() {
    if (impl) { impl->paint(); }
    else { log(__func__); }
}

void Painter::fill() {
    if (impl) { impl->fill(); }
    else { log(__func__); }
}

void Painter::fill_preserve() {
    if (impl) { impl->fill_preserve(); }
    else { log(__func__); }
}

void Painter::stroke() {
    if (impl) { impl->stroke(); }
    else { log(__func__); }
}

void Painter::stroke_preserve() {
    if (impl) { impl->stroke_preserve(); }
    else { log(__func__); }
}

void Painter::push() {
    if (impl) { impl->push(); }
    else { log(__func__); }
}

void Painter::pop() {
    if (impl) { impl->pop(); }
    else { log(__func__); }
}

void Painter::clear() {
    if (impl) { impl->clear(); }
    else { log(__func__); }
}

void Painter::set_oper(Oper op) {
    if (impl) { impl->set_oper(op); }
    else { log(__func__); }
}

Oper Painter::oper() const {
    if (impl) { return impl->oper(); }
    log(__func__);
    return Oper::COPY;
}

void Painter::pixmap(const Pixmap pix, const Point pix_origin, const Size & pix_size, bool transparent) {
    if (impl) { impl->pixmap(pix.ptr(), pix_origin, pix_size, transparent); }
    else { log(__func__); }
}

void Painter::pixmap(const Pixmap pix, bool transparent) {
    if (impl) { impl->pixmap(pix.ptr(), transparent); }
    else { log(__func__); }
}

Vector Painter::position() const {
    if (impl) { return impl->position(); }
    log(__func__);
    return Vector();
}

void Painter::move_to(double x, double y) {
    if (impl) { impl->move_to(x, y); }
    else { log(__func__); }
}

void Painter::move_to(const Vector & v) {
    if (impl) { impl->move_to(v); }
    else { log(__func__); }
}

void Painter::move_rel(double x, double y) {
    if (impl) { impl->move_rel(x, y); }
    else { log(__func__); }
}

void Painter::move_rel(const Vector & v) {
    if (impl) { impl->move_rel(v); }
    else { log(__func__); }
}

void Painter::rectangle(double x1, double y1, double x2, double y2, double radius) {
    if (impl) { impl->rectangle(x1, y1, x2, y2, radius); }
    else { log(__func__); }
}

void Painter::rectangle(const Vector & v1, const Vector & v2, double radius) {
    if (impl) { impl->rectangle(v1, v2, radius); }
    else { log(__func__); }
}

void Painter::line_to(double x, double y) {
    if (impl) { impl->line_to(x, y); }
    else { log(__func__); }
}

void Painter::line_to(const Vector & end) {
    if (impl) { impl->line_to(end); }
    else { log(__func__); }
}

void Painter::line_rel(double dx, double dy) {
    if (impl) { impl->line_rel(dx, dy); }
    else { log(__func__); }
}

void Painter::line_rel(const Vector & dv) {
    if (impl) { impl->line_rel(dv); }
    else { log(__func__); }
}

void Painter::conic_to(double cx, double cy, double ex, double ey) {
    if (impl) { impl->conic_to(cx, cy, ex, ey); }
    else { log(__func__); }
}

void Painter::conic_to(const Vector & cp, const Vector & end) {
    if (impl) { impl->conic_to(cp, end); }
    else { log(__func__); }
}

void Painter::cubic_to(double cx1, double cy1, double cx2, double cy2, double ex, double ey) {
    if (impl) { impl->cubic_to(cx1, cy1, cx2, cy2, ex, ey); }
    else { log(__func__); }
}

void Painter::cubic_to(const Vector & cp1, const Vector & cp2, const Vector & end) {
    if (impl) { impl->cubic_to(cp1, cp2, end); }
    else { log(__func__); }
}

void Painter::arc(const Vector & center, double radius, double angle1, double angle2, bool pie) {
    if (impl) { impl->arc(center, radius, angle1, angle2, pie); }
    else { log(__func__); }
}

void Painter::arc(double cx, double cy, double radius, double angle1, double angle2, bool pie) {
    if (impl) { impl->arc(cx, cy, radius, angle1, angle2, pie); }
    else { log(__func__); }
}

void Painter::circle(const Vector & center, double radius) {
    if (impl) { impl->circle(center, radius); }
    else { log(__func__); }
}

void Painter::circle(double cx, double cy, double radius) {
    if (impl) { impl->circle(cx, cy, radius); }
    else { log(__func__); }
}

void Painter::contour(const Contour & ctr) {
    if (impl) { impl->contour(ctr); }
    else { log(__func__); }
}

void Painter::contour(Contour && ctr) {
    if (impl) { impl->contour(std::move(ctr)); }
    else { log(__func__); }
}

} // namespace tau

//END

// ----------------------------------------------------------------------------
// SPDX-License-Identifier: BSD-2-Clause
// Copyright © 2014-2024 Konstantin Shmelkov <mrcashe@gmail.com>.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
// THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
// PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
// CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
// EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
// PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
// OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
// WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
// OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
// EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// ----------------------------------------------------------------------------

#include <separator-impl.hh>
#include <twins-impl.hh>
#include <iostream>

namespace tau {

Twins_impl::Twins_impl(Orientation orient, double ratio):
    orient_(orient)
{
    signal_arrange_.connect(fun(this, &Twins_impl::arrange));
    signal_display_in_.connect(fun(this, &Twins_impl::update_requisition));
    signal_size_changed_.connect(fun(this, &Twins_impl::arrange));
    signal_take_focus_.connect(fun(this, &Twins_impl::on_take_focus));

    auto sep = std::make_shared<Separator_impl>(Separator::HANDLE);
    sep_ = sep.get();
    sep_->signal_hints_changed().connect(tau::bind_front(fun(this, &Twins_impl::on_hints), sep_));
    update_child_bounds(sep_, INT_MIN, INT_MIN);
    make_child(sep);
    sep_->hide();
    sep_->signal_mouse_down().connect(fun(this, &Twins_impl::on_sep_mouse_down));
    sep_->signal_mouse_up().connect(fun(this, &Twins_impl::on_sep_mouse_up));
    sep_->set_cursor((Orientation::RIGHT == orient || Orientation::LEFT == orient) ? "size_hor" : "size_ver");
    set_ratio(ratio);
}

void Twins_impl::insert_first(Widget_ptr wp) {
    chk_parent(wp);
    rm_first();
    first_ = wp.get();
    update_child_bounds(first_, INT_MIN, INT_MIN);
    first_cx_ = first_->signal_hints_changed().connect(tau::bind_front(fun(this, &Twins_impl::on_hints), first_));
    sep_->par_show(first_ && second_ && !first_->hidden() && !second_->hidden());
    make_child(wp);
    update_requisition();
    queue_arrange();
}

void Twins_impl::insert_second(Widget_ptr wp) {
    chk_parent(wp);
    rm_second();
    second_ = wp.get();
    update_child_bounds(second_, INT_MIN, INT_MIN);
    second_cx_ = second_->signal_hints_changed().connect(tau::bind_front(fun(this, &Twins_impl::on_hints), second_));
    sep_->par_show(first_ && second_ && !first_->hidden() && !second_->hidden());
    make_child(wp);
    update_requisition();
    queue_arrange();
}

void Twins_impl::insert(Widget_ptr first, Widget_ptr second) {
    chk_parent(first);
    chk_parent(second);
    rm_first();
    rm_second();
    first_ = first.get();
    second_ = second.get();
    update_child_bounds(first_, INT_MIN, INT_MIN);
    update_child_bounds(second_, INT_MIN, INT_MIN);
    first_cx_ = first_->signal_hints_changed().connect(tau::bind_front(fun(this, &Twins_impl::on_hints), first_));
    second_cx_ = second_->signal_hints_changed().connect(tau::bind_front(fun(this, &Twins_impl::on_hints), second_));
    sep_->show();
    make_child(first);
    make_child(second);
    update_requisition();
    queue_arrange();
}

void Twins_impl::rm_first() {
    if (auto wp = first_) {
        first_cx_.drop();
        first_ = nullptr;
        unparent({ wp });
        update_child_bounds(wp, INT_MIN, INT_MIN);
    }
}

void Twins_impl::rm_second() {
    if (auto wp = second_) {
        second_cx_.drop();
        second_ = nullptr;
        unparent({ wp });
        update_child_bounds(wp, INT_MIN, INT_MIN);
    }
}

void Twins_impl::remove_first() {
    if (first_) {
        rm_first();
        sep_->hide();
        update_requisition();
        queue_arrange();
        invalidate();
    }
}

void Twins_impl::remove_second() {
    if (second_) {
        rm_second();
        sep_->hide();
        update_requisition();
        queue_arrange();
        invalidate();
    }
}

void Twins_impl::clear() {
    bool need_arrange = false;

    if (first_) {
        rm_first();
        need_arrange = true;
    }

    if (second_) {
        rm_second();
        need_arrange = true;
    }

    if (need_arrange) {
        update_requisition();
        queue_arrange();
    }
}

Size Twins_impl::child_requisition(Widget_impl * wp) {
    Margin m = wp->margin_hint();
    Size req(wp->required_size()), min(wp->min_size_hint()), max(wp->max_size_hint()),
         hint(wp->size_hint()), margins(m.left+m.right, m.top+m.bottom);

    unsigned w = 0 == hint.width() ? req.width() : hint.width();
    unsigned h = 0 == hint.height() ? req.height() : hint.height();

    if (0 != min.width()) { w = std::max(w, min.width()); }
    if (0 != min.height()) { h = std::max(h, min.height()); }
    if (0 != max.width()) { w = std::min(w, max.width()); }
    if (0 != max.height()) { h = std::min(h, max.height()); }

    w += margins.width(); h += margins.height();
    return Size(w, h);
}

void Twins_impl::update_requisition() {
    Size rq;

    if (first_ && !first_->hidden() && (!second_ || second_->hidden())) {
        rq += child_requisition(first_);
    }

    else if (second_ && !second_->hidden() && (!first_ || first_->hidden())) {
        rq += child_requisition(second_);
    }

    else if (first_ && second_ && !first_->hidden() && !second_->hidden()) {
        Size req0(child_requisition(sep_)), req1(child_requisition(first_)), req2(child_requisition(second_));

        if (Orientation::UP == orient_ || Orientation::DOWN == orient_) {
            rq.increase(0, req1.height()+req0.height()+req2.height());
            rq.update_width(std::max(req1.width(), req2.width()));
        }

        else {
            rq.increase(req1.width()+req0.width()+req2.width(), 0);
            rq.update_height(std::max(req1.height(), req2.height()));
        }
    }

    require_size(rq);
}

void Twins_impl::on_hints(Widget_impl * wp, Hints op) {
    if (!in_shutdown()) {
        if (Hints::SHOW == op) {
            sep_->par_show(first_ && second_ && !first_->hidden() && !second_->hidden());
            update_requisition();
            queue_arrange();
        }

        else if (Hints::HIDE == op) {
            sep_->hide();
            update_child_bounds(wp, INT_MIN, INT_MIN);
            update_requisition();
            queue_arrange();
        }

        else {
            update_requisition();
            queue_arrange();
        }
    }
}

void Twins_impl::arrange() {
    bool inval = false;

    if (first_ && !first_->hidden() && (!second_ || second_->hidden())) {
        Margin m = first_->margin_hint();
        if (first_->update_origin(m.left, m.top)) { inval = true; }
        if (first_->update_size(size()-Size(m.left+m.right, m.top+m.bottom))) { inval = true; }
    }

    else if (second_ && !second_->hidden() && (!first_ || first_->hidden())) {
        Margin m = second_->margin_hint();
        if (second_->update_origin(m.left, m.top)) { inval = true; }
        if (second_->update_size(size()-Size(m.left+m.right, m.top+m.bottom))) { inval = true; }
    }

    else if (first_ && second_ && !first_->hidden() && !second_->hidden()) {
        Margin m1 = first_->margin_hint(), m2 = second_->margin_hint();

        if (Orientation::UP == orient_) {
            unsigned spc = child_requisition(sep_).height(), h2 = (1.0-ratio_)*(size().height()-spc);
            if (second_->update_origin(m2.left, m2.top)) { inval = true; }
            if (second_->update_size(Size(size().width(), h2)-Size(m2.left+m2.right, m2.top+m2.bottom))) { inval = true; }
            if (first_->update_origin(m1.left, h2+spc+m1.top)) { inval = true; }
            if (first_->update_size(Size(size().width(), size().height()-h2-spc)-Size(m1.left+m1.right, m1.top+m1.bottom))) { inval = true; }
            if (sep_->update_origin(0, h2)) { inval = true; }
            if (sep_->update_size(size().width(), spc)) { inval = true; }
        }

        else if (Orientation::RIGHT == orient_) {
            unsigned spc = child_requisition(sep_).width(), w1 = ratio_*(size().width()-spc);
            if (first_->update_origin(m1.left, m1.top)) { inval = true; }
            if (first_->update_size(Size(w1, size().height())-Size(m1.left+m1.right, m1.top+m1.bottom))) { inval = true; }
            if (second_->update_origin(m2.left+w1+spc, m2.top)) { inval = true; }
            if (second_->update_size(Size(size().width()-w1-spc, size().height())-Size(m2.left+m2.right, m2.top+m2.bottom))) { inval = true; }
            if (sep_->update_origin(w1, 0)) { inval = true; }
            if (sep_->update_size(spc, size().height())) { inval = true; }
        }

        else if (Orientation::LEFT == orient_) {
            unsigned spc = child_requisition(sep_).width(), w2 = (1.0-ratio_)*(size().width()-spc);
            if (second_->update_origin(m2.left, m2.top)) { inval = true; }
            if (second_->update_size(Size(w2, size().height())-Size(m2.left+m2.right, m2.top+m2.bottom))) { inval = true; }
            if (first_->update_origin(m1.left+w2+spc, m2.top)) { inval = true; }
            if (first_->update_size(Size(size().width()-w2-spc, size().height())-Size(m1.left+m1.right, m1.top+m1.bottom))) { inval = true; }
            if (sep_->update_origin(w2, 0)) { inval = true; }
            if (sep_->update_size(spc, size().height())) { inval = true; }
        }

        else {
            unsigned spc = child_requisition(sep_).height(), h1 = ratio_*(size().height()-spc);
            if (update_child_bounds(first_, m1.left, m1.top, Size(size().width(), h1)-Size(m1.left+m1.right, m1.top+m1.bottom))) { inval = true; }
            if (update_child_bounds(second_, m2.left, m2.top+h1+spc, Size(size().width(), size().height()-h1-spc)-Size(m2.left+m2.right, m2.top+m2.bottom))) { inval = true; }
            if (update_child_bounds(sep_, 0, h1, size().width(), spc)) { inval = true; }
        }
    }

    if (inval) { invalidate(); }
}

void Twins_impl::set_ratio(double ratio) {
    ratio_ = std::max(0.0, std::min(1.0, ratio));
    if (first_ && !first_->hidden() && second_ && !second_->hidden()) { queue_arrange(); }
}

bool Twins_impl::on_sep_mouse_down(int mbt, int mm, const Point & pt) {
    if (MBT_LEFT == mbt) {
        sep_mouse_motion_cx_ = sep_->signal_mouse_motion().connect(fun(this, &Twins_impl::on_sep_mouse_motion));
        sep_->grab_mouse();
    }

    return false;
}

bool Twins_impl::on_sep_mouse_up(int mbt, int mm, const Point & pt) {
    if (MBT_LEFT == mbt) {
        sep_->ungrab_mouse();
        sep_mouse_motion_cx_.drop();
    }

    return false;
}

void Twins_impl::on_sep_mouse_motion(int mm, const Point & pt) {
    Size sz(size());
    Point ptp = sep_->to_parent(sep_->container(), pt);
    int dmin, dmax;
    unsigned spc;

    if (Orientation::RIGHT == orient_ || Orientation::LEFT == orient_) {
        spc = sep_->size().width();
        dmin = spc;
        dmax = spc < sz.width() ? sz.width()-spc : sz.width();

        if (ptp.x() >= dmin && ptp.x() < dmax) {
            double ratio = double(ptp.x())/dmax;
            if (Orientation::LEFT == orient_) { ratio = 1.0-ratio; }
            set_ratio(ratio);
            signal_ratio_changed_(ratio);
        }
    }

    else {
        spc = sep_->size().height();
        dmin = spc;
        dmax = spc < sz.height() ? sz.height()-spc : sz.height();

        if (ptp.y() >= dmin && ptp.y() < dmax) {
            double ratio = double(ptp.y())/dmax;
            if (Orientation::UP == orient_) { ratio = 1.0-ratio; }
            set_ratio(ratio);
            signal_ratio_changed_(ratio);
        }
    }
}

bool Twins_impl::on_take_focus() {
    if (focused_child_ && focused_child_->take_focus()) { return true; }
    if (first_ && first_->take_focus()) { return true; }
    if (second_ && second_->take_focus()) { return true; }
    return grab_focus();
}

} // namespace tau

//END

// ----------------------------------------------------------------------------
// SPDX-License-Identifier: BSD-2-Clause
// Copyright © 2014-2024 Konstantin Shmelkov <mrcashe@gmail.com>.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
// THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
// PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
// CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
// EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
// PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
// OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
// WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
// OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
// EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// ----------------------------------------------------------------------------

#include <tau/scroller.hh>
#include <scroller-impl.hh>

namespace tau {

#define SCROLLER_IMPL (std::static_pointer_cast<Scroller_impl>(impl))

Scroller::Scroller():
    Container(std::make_shared<Scroller_impl>())
{
}

Scroller::Scroller(const Scroller & other):
    Container(other.impl)
{
}

Scroller & Scroller::operator=(const Scroller & other) {
    Container::operator=(other);
    return *this;
}

Scroller::Scroller(Scroller && other):
    Container(other.impl)
{
}

Scroller & Scroller::operator=(Scroller && other) {
    Container::operator=(other);
    return *this;
}

Scroller::Scroller(Widget_ptr wp):
    Container(std::dynamic_pointer_cast<Scroller_impl>(wp))
{
}

Scroller & Scroller::operator=(Widget_ptr wp) {
    Container::operator=(std::dynamic_pointer_cast<Scroller_impl>(wp));
    return *this;
}

void Scroller::insert(Widget & w) {
    SCROLLER_IMPL->insert(w.ptr());
}

Widget_ptr Scroller::widget() noexcept {
    return SCROLLER_IMPL->chptr(SCROLLER_IMPL->widget());
}

Widget_cptr Scroller::widget() const noexcept {
    return SCROLLER_IMPL->chptr(SCROLLER_IMPL->widget());
}

bool Scroller::empty() const noexcept {
    return SCROLLER_IMPL->empty();
}

void Scroller::clear() {
    SCROLLER_IMPL->clear();
}

Size Scroller::pan_size() const {
    return SCROLLER_IMPL->pan_size();
}

void Scroller::pan_x(int x) {
    SCROLLER_IMPL->pan_x(x);
}

void Scroller::pan_y(int y) {
    SCROLLER_IMPL->pan_y(y);
}

void Scroller::pan(const Point & pos) {
    SCROLLER_IMPL->pan(pos);
}

void Scroller::pan(int x, int y) {
    SCROLLER_IMPL->pan(x, y);
}

void Scroller::pan(Widget & w) {
    SCROLLER_IMPL->pan(w.ptr().get());
}

Point Scroller::pan() const {
    return SCROLLER_IMPL->pan();
}

void Scroller::set_step(const Point & step) {
    SCROLLER_IMPL->set_step(step);
}

void Scroller::set_step(int xstep, int ystep) {
    SCROLLER_IMPL->set_step(xstep, ystep);
}

Point Scroller::step() const {
    return SCROLLER_IMPL->step();
}

void Scroller::gravity(Gravity g) {
    SCROLLER_IMPL->gravity(g);
}

Gravity Scroller::gravity() const noexcept {
    return SCROLLER_IMPL->gravity();
}

Action & Scroller::action_pan_left() {
    return SCROLLER_IMPL->action_pan_left();
}

Action & Scroller::action_pan_right() {
    return SCROLLER_IMPL->action_pan_right();
}

Action & Scroller::action_pan_up() {
    return SCROLLER_IMPL->action_pan_up();
}

Action & Scroller::action_pan_down() {
    return SCROLLER_IMPL->action_pan_down();
}

Action & Scroller::action_previous_page() {
    return SCROLLER_IMPL->action_previous_page();
}

Action & Scroller::action_next_page() {
    return SCROLLER_IMPL->action_next_page();
}

Action & Scroller::action_home() {
    return SCROLLER_IMPL->action_home();
}

Action & Scroller::action_end() {
    return SCROLLER_IMPL->action_end();
}

signal<void()> & Scroller::signal_pan_changed() {
    return SCROLLER_IMPL->signal_pan_changed();
}

signal<void()> & Scroller::signal_pan_size_changed() {
    return SCROLLER_IMPL->signal_pan_size_changed();
}

} // namespace tau

//END

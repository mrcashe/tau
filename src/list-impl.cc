// ----------------------------------------------------------------------------
// SPDX-License-Identifier: BSD-2-Clause
// Copyright © 2014-2024 Konstantin Shmelkov <mrcashe@gmail.com>.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
// THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
// PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
// CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
// EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
// PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
// OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
// WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
// OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
// EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// ----------------------------------------------------------------------------

#include <tau/exception.hh>
#include <tau/icon.hh>
#include <list-impl.hh>
#include <label-impl.hh>
#include <algorithm>
#include <iostream>

namespace tau {

List_impl::List_impl() {
    init();
}

List_impl::List_impl(unsigned spacing):
    Table_impl(spacing)
{
    init();
}

List_impl::List_impl(unsigned xspacing, unsigned yspacing):
    Table_impl(xspacing, yspacing)
{
    init();
}

void List_impl::init() {
    connect_action(action_cancel_);
    connect_action(action_activate_);
    connect_action(action_previous_);
    connect_action(action_next_);
    connect_action(action_previous_page_);
    connect_action(action_next_page_);
    connect_action(action_home_);
    connect_action(action_end_);

    for (auto & act: sel_actions_) { connect_action(*act); act->disable(); }

    action_select_previous_.disable();
    action_select_next_.disable();
    action_select_previous_page_.disable();
    action_select_next_page_.disable();
    action_select_home_.disable();
    action_select_end_.disable();
    action_select_all_.disable();
    conf().redirect(Conf::BACKGROUND, Conf::WHITESPACE_BACKGROUND);
    conf().signal_changed(Conf::SELECT_BACKGROUND).connect(fun(this, &List_impl::on_select_background));

    signal_mouse_down().connect(fun(this, &List_impl::on_mouse_down), true);
    signal_mouse_down().connect(fun(this, &List_impl::on_fake_mouse));
    signal_mouse_double_click().connect(fun(this, &List_impl::on_mouse_double_click), true);
    signal_take_focus_.connect(fun(this, &List_impl::on_take_focus), true);
    signal_enable_.connect(fun(this, &List_impl::reset_activated));
    signal_disable_.connect(fun(this, &List_impl::reset_activated));
    signal_selection_changed().connect(fun(this, &List_impl::on_selection_changed));
    signal_children_changed_.connect(fun(this, &List_impl::on_children_changed));
}

void List_impl::reset_activated() {
    activated_ = INT_MIN;
}

bool List_impl::on_take_focus() {
    return sels_.empty() || grab_focus();
}

int List_impl::prepend_row(Widget_ptr wp, Align xalign, bool shrink) {
    auto spn = span();
    int row = INT_MAX == spn.ymin ? -1 : spn.ymin-1;
    put(wp, xalign, Align::CENTER, 0, row, 1, 1, shrink, true);
    wp->signal_hints_changed().connect(tau::bind_front(fun(this, &List_impl::on_hints_changed), wp.get()));
    set_column_margin(0, 2, 2);
    set_row_margin(row, 1, 1);
    sels_.insert(row);
    return row;
}

int List_impl::prepend_row(Widget_ptr wp, bool shrink) {
    return prepend_row(wp, Align::START, shrink);
}

int List_impl::insert_row(Widget_ptr wp, int y, Align xalign, bool shrink) {
    auto spn = span();
    if (y >= spn.ymax) { return append_row(wp, xalign, shrink); }
    if (y < spn.ymin) { return prepend_row(wp, xalign, shrink); }

    insert_rows(y);
    put(wp, xalign, Align::CENTER, 0, y, 1, 1, shrink, true);
    wp->signal_hints_changed().connect(tau::bind_front(fun(this, &List_impl::on_hints_changed), wp.get()));
    set_column_margin(0, 2, 2);
    set_row_margin(y, 1, 1);

    if (!sels_.empty()) {
        int first = *sels_.begin(), last = *sels_.rbegin();

        if (y >= first && y <= last) {
            for (int yy = last; yy >= y; --yy) {
                if (0 != sels_.erase(yy)) {
                    sels_.insert(yy+1);
                    signal_move_row_(yy, yy+1);
                }
            }
        }
    }

    sels_.insert(y);
    if (INT_MIN == current() && focused()) { select(y); }
    return y;
}

int List_impl::insert_row(Widget_ptr wp, int row, bool shrink) {
    return insert_row(wp, row, Align::START, shrink);
}

int List_impl::append_row(Widget_ptr wp, Align xalign, bool shrink) {
    auto spn = span();
    int y = INT_MIN == spn.ymax ? 0 : spn.ymax;
    put(wp, xalign, Align::CENTER, 0, y, 1, 1, shrink, true);
    wp->signal_hints_changed().connect(tau::bind_front(fun(this, &List_impl::on_hints_changed), wp.get()));
    set_column_margin(0, 2, 2);
    set_row_margin(y, 1, 1);
    sels_.insert(y);
    return y;
}

int List_impl::append_row(Widget_ptr wp, bool shrink) {
    return append_row(wp, Align::START, shrink);
}

int List_impl::prepend(Widget_ptr wp, Align xalign, bool shrink) {
    auto rng = span();
    int y = INT_MAX == rng.ymin ? -1 : rng.ymin-1;
    int xx = rng.xmax > rng.xmin ? rng.xmin : 0;
    put(wp, xalign, Align::CENTER, xx, y, rng.xmax > rng.xmin ? rng.xmax-rng.xmin : 1, 1, shrink, true);
    wp->signal_hints_changed().connect(tau::bind_front(fun(this, &List_impl::on_hints_changed), wp.get()));
    set_column_margin(xx, 2, 2);
    set_row_margin(y, 1, 1);
    frees_.insert(wp.get());
    return y;
}

int List_impl::prepend(Widget_ptr wp, bool shrink) {
    return prepend(wp, Align::START, shrink);
}

int List_impl::insert(Widget_ptr wp, int yy, Align xalign, bool shrink) {
    auto spn = span();
    if (yy >= spn.ymax) { return append(wp, xalign, shrink); }
    if (yy < spn.ymin) { return prepend(wp, xalign, shrink); }
    std::set<int, std::greater<int>> ys;
    for (auto i = sels_.find(yy); i != sels_.end(); ++i) { ys.insert(*i); }
    for (int y: ys) { sels_.erase(y); sels_.insert(y+1); }
    sels_.erase(yy);
    insert_columns(yy);
    auto trng = span();
    int xx = trng.xmax > trng.xmin ? trng.xmin : 0;
    put(wp, xalign, Align::CENTER, xx, yy, trng.xmax > trng.xmin ? trng.xmax-trng.xmin : 1, 1, shrink, true);
    wp->signal_hints_changed().connect(tau::bind_front(fun(this, &List_impl::on_hints_changed), wp.get()));
    set_column_margin(xx, 2, 2);
    set_row_margin(yy, 1, 1);
    frees_.insert(wp.get());
    return yy;
}

int List_impl::insert(Widget_ptr wp, int row, bool shrink) {
    return insert(wp, row, Align::START, shrink);
}

int List_impl::append(Widget_ptr wp, Align xalign, bool shrink) {
    auto rng = span();
    int xx = rng.xmax > rng.xmin ? rng.xmin : 0;
    int row = INT_MIN == rng.ymax ? 0 : rng.ymax;
    put(wp, xalign, Align::CENTER, xx, row, rng.xmax > rng.xmin ? rng.xmax-rng.xmin : 1, 1, shrink, true);
    wp->signal_hints_changed().connect(tau::bind_front(fun(this, &List_impl::on_hints_changed), wp.get()));
    set_column_margin(xx, 2, 2);
    set_row_margin(row, 1, 1);
    frees_.insert(wp.get());
    return row;
}

int List_impl::append(Widget_ptr wp, bool shrink) {
    return append(wp, Align::CENTER, shrink);
}

int List_impl::prepend(int yy, Widget_ptr wp, Align xalign, bool shrink) {
    auto i = sels_.find(yy);
    if (i == sels_.end()) { throw user_error(str_format("List_impl: bad row ", yy)); }
    int xx = row_span(yy).xmin-1;
    put(wp, xalign, Align::CENTER, xx, yy, 1, 1, shrink, true);
    wp->signal_hints_changed().connect(tau::bind_front(fun(this, &List_impl::on_hints_changed), wp.get()));
    set_column_margin(xx, 2, 2);
    set_row_margin(yy, 1, 1);
    return xx;
}

int List_impl::prepend(int row, Widget_ptr wp, bool shrink) {
    return prepend(row, wp, Align::CENTER, shrink);
}

int List_impl::insert(int row, Widget_ptr wp, int xx, Align xalign, bool shrink) {
    auto i = sels_.find(row);
    if (i == sels_.end()) { throw user_error(str_format("List_impl: bad row ", row)); }
    put(wp, xalign, Align::CENTER, xx < 0 ? xx : 1+xx, row, 1, 1, shrink, true);
    wp->signal_hints_changed().connect(tau::bind_front(fun(this, &List_impl::on_hints_changed), wp.get()));
    set_column_margin(xx < 0 ? xx : 1+xx, 2, 2);
    set_row_margin(row, 1, 1);
    return xx;
}

int List_impl::insert(int row, Widget_ptr wp, int index, bool shrink) {
    return insert(row, wp, index, Align::CENTER, shrink);
}

int List_impl::append(int yy, Widget_ptr wp, Align xalign, bool shrink) {
    auto i = sels_.find(yy);
    if (i == sels_.end()) { throw user_error(str_format("List_impl: bad row ", yy)); }
    int xx = row_span(yy).xmax;
    put(wp, xalign, Align::CENTER, xx, yy, 1, 1, shrink, true);
    wp->signal_hints_changed().connect(tau::bind_front(fun(this, &List_impl::on_hints_changed), wp.get()));
    set_column_margin(xx, 2, 2);
    set_row_margin(yy, 1, 1);
    return xx;
}

int List_impl::append(int yy, Widget_ptr wp, bool shrink) {
    return append(yy, wp, Align::CENTER, shrink);
}

std::size_t List_impl::remove(int yy) {
    if (INT_MIN < yy && yy < INT_MAX) {
        int ysel = current();

        if (activated_ == yy) { activated_ = INT_MIN; }
        if (fixed_ == yy) { unfix(); }
        auto i = sels_.find(yy);

        if (i != sels_.end()) {
            signal_remove_row_(yy);
            int last = *sels_.rbegin();

            // Move rows up.
            for (int y = yy+1; y <= last; ++y) {
                auto iter = sels_.find(y);

                if (iter != sels_.end()) {
                    sels_.insert(y-1);
                    signal_move_row_(y, y-1);
                }
            }

            sels_.erase(last);
        }

        else {
            for (auto wp: widgets(yy)) {
                frees_.erase(wp.get());
            }
        }

        remove_rows(yy);

        if (ysel == yy) {
            select(ysel);
            if (INT_MIN == current()) { select_back(); }
        }

        return 1;
    }

    return 0;
}

std::size_t List_impl::remove(Widget_impl * wp) {
    return remove(Table_impl::span(wp).ymin);
}

Widget_ptr List_impl::widget_at(int row, int col) noexcept {
    auto v = Table_impl::widgets({ col, row, 1+col, 1+row });
    return !v.empty() ? v.front() : nullptr;
}

Widget_cptr List_impl::widget_at(int row, int col) const noexcept {
    auto v = Table_impl::widgets({ col, row, 1+col, 1+row });
    return !v.empty() ? v.front() : nullptr;
}

int List_impl::next_row() {
    if (sels_.size() > 1) {
        auto i = sels_.find(Table_impl::selection().ymin);

        if (i != sels_.end()) {
            i = std::find_if(++i, sels_.end(), [this](int y) { return row_visible(y); });
            if (i != sels_.end()) { return *i; }
        }
    }

    return INT_MIN;
}

int List_impl::prev_row() {
    if (sels_.size() > 1) {
        int ysel = Table_impl::selection().ymin;
        auto i = std::find_if(sels_.rbegin(), sels_.rend(), [ysel](auto & y) { return y == ysel; });

        if (i != sels_.rend()) {
            i = std::find_if(++i, sels_.rend(), [this](int y) { return row_visible(y); });
            if (i != sels_.rend()) { return *i; }
        }
    }

    return INT_MIN;
}

int List_impl::select_next(bool fix) {
    if (!fixed() && !sels_.empty()) { unmark_all(); return select(next_row(), fix); }
    return INT_MIN;
}

int List_impl::select_previous(bool fix) {
    if (!fixed() && !sels_.empty()) { unmark_all(); return select(prev_row(), fix); }
    return INT_MIN;
}

int List_impl::select_front(bool fix) {
    if (!fixed() && !sels_.empty()) {
        unmark_all();
        auto i = std::find_if(sels_.begin(), sels_.end(), [this](int y) { return row_visible(y); });
        return select(*i, fix);
    }

    return INT_MIN;
}

int List_impl::select_back(bool fix) {
    if (!fixed() && !sels_.empty()) {
        unmark_all();
        auto i = std::find_if(sels_.rbegin(), sels_.rend(), [this](int y) { return row_visible(y); });
        return select(*i, fix);
    }

    return INT_MIN;
}

int List_impl::select(int y, bool fix) {
    if (!fixed() && sels_.contains(y) && row_visible(y)) {
        unmark_row(y);
        Table_impl::select({ INT_MIN, y, INT_MAX, y+1 });
        if (fix) { fixed_ = y; }
        return current();
    }

    return INT_MIN;
}

void List_impl::unselect_only() {
    if (!fixed()) {
        Table_impl::unselect();
    }
}

// Redefines Table_impl::unselect().
void List_impl::unselect() {
    if (!fixed()) {
        activated_ = INT_MIN;
        unmark_all();
        Table_impl::unselect();
    }
}

int List_impl::current() const noexcept {
    Span sel = Table_impl::selection();
    return sel.ymax > sel.ymin ? sel.ymin : INT_MIN;
}

// NOTE We can not make it noexcept!
bool List_impl::row_visible(int y) const {
    auto v = widgets(y);
    return !v.empty() && !std::all_of(v.begin(), v.end(), [](auto wp) { return wp->hidden(); });
}

std::vector<int> List_impl::selection() const {
    std::vector<int> v;
    Span sel = Table_impl::selection();
    if (sel.ymax > sel.ymin) { v.push_back(sel.ymin); }
    for (auto & spn: marks()) { v.push_back(spn.ymin); }
    return v;
}

void List_impl::activate_current() {
    int cur = current();

    if (activated_ != cur) {
        activated_ = cur;
        signal_activate_row_(cur);
    }
}

void List_impl::allow_multiple_select(bool yes) {
    multiple_select_allowed_ = yes;
    for (auto & act: sel_actions_) { act->par_enable(yes); }
}

bool List_impl::row_marked(int y) {
    for (auto & spn: marks()) {
        if (spn.ymin == y) {
            return true;
        }
    }

    return false;
}

void List_impl::mark_row(int y) {
    if (signal_mark_validate_(y)) {
        Span spn = span();

        if (spn.xmax > spn.xmin && spn.ymax > spn.ymin) {
            for (auto wp: widgets(y)) { wp->signal_select()(); }
            mark_back(conf().color(Conf::SELECT_BACKGROUND).value().darken(0.1), { spn.xmin, y, spn.xmax, y+1 }, true);
        }
    }
}

void List_impl::unmark_row(int y) {
    for (auto & m: marks()) {
        if (m.ymin == y) {
            for (auto wp: widgets(y)) { wp->signal_unselect()(); }
            unmark(m);
            return;
        }
    }
}

void List_impl::unmark_all() {
    std::list<Widget_ptr> wps;
    for (auto & spn: marks()) { wps.merge(Table_impl::widgets(spn)); }
    for (auto wp: wps) { wp->signal_unselect()(); }
    unmark();
}

void List_impl::flip_mark(int y) {
    if (row_marked(y)) { unmark_row(y); }
    else { mark_row(y); }
}

void List_impl::adjust() {
    Span rng = span();

    if (rng.xmax > rng.xmin) {
        for (auto wp: frees_) {
            Span spn = span(wp);
            respan(wp, { rng.xmin, spn.ymin, rng.xmax, 1+spn.ymin });
        }
    }
}

std::size_t List_impl::sel_count() const {
    return marks().size();
}

// Do not return true from here!
bool List_impl::on_mouse_down(int mbt, int mm, const Point & pt) {
    if (MBT_LEFT == mbt) {
        if (grab_focus() && !fixed()) {
            int y = row_at_y(pt.y());

            if (y > INT_MIN && y != current()) {
                if ((MM_SHIFT|MM_CONTROL) & mm && multiple_select_allowed_) {
                    if (row_marked(y)) { unmark_row(y); }
                    else { mark_row(y); }
                }

                else {
                    unmark_all();
                    select(y);
                }
            }
        }
    }

    return false;
}

// Do not return true from here!
bool List_impl::on_mouse_double_click(int mbt, int mm, const Point & pt) {
    if (MBT_LEFT == mbt) { action_activate_(); }
    return false;
}

int List_impl::page_down_row() {
    if (sels_.empty()) { return INT_MIN; }
    auto rng = span(), sel = Table_impl::selection();

    if (Rect rsel = bounds(sel)) {
        Size lsize = logical_size(), max = lsize-size();

        if (max.height()) {
            Rect va { offset(), size() };

            if (*sels_.rbegin() != sel.ymin) {
                int yt = rsel.top()+va.height()-rsel.height()-rsel.height();
                auto iter = std::find_if(sels_.begin(), sels_.end(), [sel](auto & y) { return y == sel.ymin; });

                Rect r;

                for (; iter != sels_.end(); ++iter) {
                    r = bounds({ rng.xmin, *iter, rng.xmax, 1+*iter });
                    if (r.top() >= yt) break;
                }

                if (iter != sels_.end()) {
                    return *iter;
                }
            }
        }
    }

    return *sels_.rbegin();
}

void List_impl::on_page_down() {
    int sel = current(), new_sel = page_down_row();
    if (new_sel != sel) { unselect(); select(new_sel); }
}

int List_impl::page_up_row() {
    auto rng = span(), sel = Table_impl::selection();

    if (Rect rsel = bounds(sel)) {
        Size lsize = logical_size(), max = lsize-size();
        Rect r { offset(), size() };

        if (max.height()) {
            int yt = rsel.top()-(7*r.height()/8);
            auto i = sels_.find(sel.ymin);

            if (i != sels_.end()) {
                for (; i != sels_.begin(); --i) {
                    r = bounds({ rng.xmin, *i, rng.xmax, 1+*i });
                    if (r.top() <= yt) { return *i; }
                }
            }
        }
    }

    return *sels_.begin();
}

void List_impl::on_page_up() {
    int sel = current(), new_sel = page_up_row();
    if (new_sel != sel) { unselect(); select(new_sel); }
}

void List_impl::on_select_page_down() {
    int sel = current(), next = page_down_row();

    if (INT_MIN != next && next > sel) {
        unselect_only();
        for (int y = sel; y < next; ++y) { flip_mark(y); }
        select(next);
    }
}

void List_impl::on_select_page_up() {
    if (!fixed()) {
        int sel = current(), next = page_up_row();

        if (INT_MIN != next && next < sel) {
            unselect_only();
            for (int y = sel; y > next; --y) { flip_mark(y); }
            select(next);
        }
    }
}

void List_impl::on_prev() {
    if (!fixed()) {
        unmark_all();
        select(prev_row());
    }
}

void List_impl::on_select_prev() {
    if (!fixed()) {
        int sel = current(), prev = prev_row();

        if (INT_MIN != prev && sel != prev) {
            unselect_only();
            if (row_marked(prev)) { unmark_row(sel); }
            else { mark_row(sel); }
            select(prev);
        }
    }
}

void List_impl::on_next() {
    if (!fixed()) {
        unmark_all();
        select(next_row());
    }
}

void List_impl::on_select_next() {
    if (!fixed()) {
        int sel = current(), next = next_row();

        if (INT_MIN != next && sel != next) {
            unselect_only();
            if (row_marked(next)) { unmark_row(sel); }
            else { mark_row(sel); }
            select(next);
        }
    }
}

void List_impl::on_home() {
    if (!fixed()) {
        unmark_all();
        select_front();
    }
}

void List_impl::on_end() {
    if (!fixed()) {
        unmark_all();
        select_back();
    }
}

void List_impl::on_select_home() {
    if (!fixed() && !sels_.empty()) {
        int sel = current();

        if (INT_MIN != sel) {
            int first = *sels_.begin();
            select(first);

            if (first < sel) {
                for (int y = sel; y > first; --y) { flip_mark(y); }
            }
        }
    }
}

void List_impl::on_select_end() {
    if (!fixed() && !sels_.empty()) {
        int sel = current();

        if (INT_MIN != sel) {
            int last = *sels_.rbegin();
            select(last);

            if (last > sel) {
                for (int y = sel; y < last; ++y) { flip_mark(y); }
            }
        }
    }
}

void List_impl::on_select_all() {
    if (!fixed() && !sels_.empty()) {
        int cur = current();

        for (int y: sels_) {
            if (y != cur) {
                flip_mark(y);
            }
        }
    }
}

void List_impl::on_selection_changed() {
    if (!in_shutdown()) {
        signal_select_row_(current());
    }
}

void List_impl::on_select_background() {
    for (auto & m: marks()) {
        mark_back(conf().color(Conf::SELECT_BACKGROUND).value().darken(0.1), m, true);
    }
}

void List_impl::on_children_changed() {
    if (empty()) {
        sels_.clear(), frees_.clear();
        activated_ = fixed_ = INT_MIN;
    }

    else {
        adjust();
    }
}

void List_impl::on_hints_changed(Widget_impl * wp, Hints hints) {
    if (!in_destroy() && Hints::HIDE == hints) {
        int y = span(wp).ymin;
        unmark_row(y);

        if (y == current()) {
            on_next();
            if (INT_MIN == current()) { on_prev(); }
        }
    }
}

} // namespace tau

//END

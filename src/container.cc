// ----------------------------------------------------------------------------
// SPDX-License-Identifier: BSD-2-Clause
// Copyright © 2014-2024 Konstantin Shmelkov <mrcashe@gmail.com>.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
// THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
// PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
// CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
// EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
// PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
// OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
// WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
// OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
// EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// ----------------------------------------------------------------------------

/// @file container.cc The Container interface implementation.
/// The header is container.hh.

#include <tau/container.hh>
#include <container-impl.hh>

namespace tau {

#define CONTAINER_IMPL (std::static_pointer_cast<Container_impl>(impl)->compound())

Container::Container(Widget_ptr wp):
    Widget(wp)
{
}

Container & Container::operator=(Widget_ptr wp) {
    Widget::operator=(std::dynamic_pointer_cast<Container_impl>(wp));
    return *this;
}

Widget_ptr Container::chptr(Widget_impl * wp) noexcept {
    return CONTAINER_IMPL->chptr(wp);
}

Widget_cptr Container::chptr(const Widget_impl * wp) const noexcept {
    return CONTAINER_IMPL->chptr(wp);
}

Widget & Container::manage(Widget * w) {
    return CONTAINER_IMPL->manage(w);
}

Widget_ptr Container::focus_endpoint() {
    return CONTAINER_IMPL->focus_endpoint();
}

Widget_cptr Container::focus_endpoint() const {
    return CONTAINER_IMPL->focus_endpoint();
}

Widget_ptr Container::focused_child() {
    Widget_impl * wp = const_cast<Widget_impl *>(CONTAINER_IMPL->focused_child());
    return CONTAINER_IMPL->chptr(wp);
}

Widget_cptr Container::focused_child() const {
    return CONTAINER_IMPL->chptr(CONTAINER_IMPL->focused_child());
}

std::list<Widget_ptr> Container::children() const {
    return CONTAINER_IMPL->children();
}

void Container::queue_arrange() {
    CONTAINER_IMPL->queue_arrange();
}

void Container::make_child(Widget & w) {
    CONTAINER_IMPL->make_child(w.ptr());
}

void Container::unparent_child(Widget & w) {
    CONTAINER_IMPL->unparent({ w.ptr().get() });
}

bool Container::update_child_bounds(Widget & w, const Rect & bounds) {
    if (w.ptr()->container() == CONTAINER_IMPL) {
        return CONTAINER_IMPL->update_child_bounds(w.ptr().get(), bounds);
    }

    return false;
}

bool Container::update_child_bounds(Widget & w, const Point & origin, const Size & sz) {
    if (w.ptr()->container() == CONTAINER_IMPL) {
        return CONTAINER_IMPL->update_child_bounds(w.ptr().get(), origin, sz);
    }

    return false;
}

bool Container::update_child_bounds(Widget & w, int x, int y, const Size & sz) {
    if (w.ptr()->container() == CONTAINER_IMPL) {
        return CONTAINER_IMPL->update_child_bounds(w.ptr().get(), x, y, sz);
    }

    return false;
}

bool Container::update_child_bounds(Widget & w, int x, int y, unsigned width, unsigned height) {
    if (w.ptr()->container() == CONTAINER_IMPL) {
        return CONTAINER_IMPL->update_child_bounds(w.ptr().get(), x, y, width, height);
    }

    return false;
}

void Container::focus_after(Widget & w, Widget & after_this) {
    CONTAINER_IMPL->focus_after(w.ptr().get(), after_this.ptr().get());
}

void Container::focus_before(Widget & w, Widget & before_this) {
    CONTAINER_IMPL->focus_before(w.ptr().get(), before_this.ptr().get());
}

void Container::unchain_focus(Widget & w) {
    CONTAINER_IMPL->unchain_focus(w.ptr().get());
}

Widget_ptr Container::lookup(const ustring & ep) const {
    auto o = static_cast<Widget_impl *>(CONTAINER_IMPL->lookup(ep));
    return o && o->container() ? o->container()->chptr(o) : nullptr;
}

void Container::link(Widget & w, const ustring & name) {
    CONTAINER_IMPL->link(w.ptr().get(), name);
}

ustring Container::name(const Widget & w) const {
    return CONTAINER_IMPL->name(w.ptr().get());
}

std::vector<ustring> Container::names() const {
    std::vector<ustring> v;

    for (auto o: CONTAINER_IMPL->objects()) {
        if (dynamic_cast<const Widget_impl *>(o)) {
            v.push_back(CONTAINER_IMPL->name(o));
        }
    }

    return v;
}

Action & Container::action_focus_next() {
    return CONTAINER_IMPL->action_focus_next();
}

Action & Container::action_focus_previous() {
    return CONTAINER_IMPL->action_focus_previous();
}

signal<void()> & Container::signal_arrange() {
    return CONTAINER_IMPL->signal_arrange();
}

signal<void()> & Container::signal_children_changed() {
    return CONTAINER_IMPL->signal_children_changed();
}

} // namespace tau

//END

// ----------------------------------------------------------------------------
// SPDX-License-Identifier: BSD-2-Clause
// Copyright © 2014-2024 Konstantin Shmelkov <mrcashe@gmail.com>.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
// THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
// PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
// CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
// EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
// PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
// OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
// WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
// OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
// EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// ----------------------------------------------------------------------------

#ifndef __TAU_THEME_IMPL_HH__
#define __TAU_THEME_IMPL_HH__

#include <tau/action.hh>
#include <tau/cursor.hh>
#include <tau/enums.hh>
#include <tau/icon.hh>
#include <tau/pixmap.hh>
#include <tau/signal.hh>
#include <tau/conf.hh>
#include <tau/timeval.hh>
#include <tau/theme.hh>
#include <tau/ustring.hh>
#include <loop-impl.hh>
#include <atomic>
#include <queue>
#include <map>
#include <mutex>
#include <set>
#include <unordered_map>

namespace tau {

class Theme_impl: public trackable {
public:

    virtual ~Theme_impl() {}
    Theme_impl(const Theme_impl & other) = delete;
    Theme_impl(Theme_impl && other) = delete;
    Theme_impl & operator=(const Theme_impl & other) = delete;
    Theme_impl & operator=(Theme_impl && other) = delete;

    static Theme_ptr root();

    void add_icon_dir(const ustring & dir);
    void add_pixmap_dir(const ustring & dir);
    void add_cursor_dir(const ustring & dir);

    Cursor_ptr  find_cursor(const ustring & names, int size);
    Pixmap_cptr find_pixmap(const ustring & names);
    Pixmap_cptr find_icon(const ustring & names, int icon_size, const ustring & context=ustring());
    Pixmap_ptr  get_icon(const ustring & names, int icon_size, const ustring & context=ustring());

    std::vector<ustring> list_icon_themes() const;
    std::vector<ustring> list_cursor_themes() const;
    void set_cursor_theme(const ustring & names);       // Accepts colon separated list of themes.
    void set_icon_theme(const ustring & names);         // Accepts colon separated list of themes.
    void bind_icon_theme(const ustring & name);
    ustring cursor_theme() const;
    ustring icon_theme() const;
    int icon_pixels(int icon_size) const noexcept;      // Convert Icon_size enum to the icon size in pixels.
    Conf & this_conf();

    Master_action * find_action(const std::string & name);

    void take_cursor_lookup_slot(slot<Cursor_ptr(ustring)> s);
    Conf & conf() noexcept { return conf_; }
    const Conf & conf() const noexcept { return conf_; }

    signal<void()> & signal_cursors_changed();
    signal<void()> & signal_icons_changed();

protected:

    std::recursive_mutex    mmx_;                           // Member mutex.
    std::atomic_int         icursor_                        { -1 };
    std::atomic_int         iicon_                          { -1 };
    int                     cursor_size_                    { 24 };
    int                     icon_sizes_[1+Icon::LARGEST]    { 22, 8, 12, 16, 22, 32, 48 };

protected:

    Theme_impl();

    // Overridden by Theme_unix.
    // Overridden by Theme_win.
    virtual void boot();

    virtual void sweep();

    // Overriden by Theme_win.
    // Used to expand icon search process.
    // On Windows it tries to find icon within PE file resources.
    virtual Pixmap_ptr icon_hook(const ustring & name, const ustring & context, int size) { return nullptr; }

    void boot_icon_themes(const ustring & themes);
    void boot_cursor_themes(const ustring & themes);
    void boot_fallback_theme(const ustring & theme);

private:

    using Actions = std::unordered_map<std::string, Master_action>;
    using Items   = std::deque<std::pair<std::string, std::string>>;

    struct Cursor_holder {
        Cursor_ptr      cursor;
        Timeval         tv;
    };

    using Cursor_cache  = std::unordered_map<std::string, Cursor_holder>;

    struct Thread {
        slot<Cursor_ptr(ustring)>   lookup;
        Cursor_cache                cursor_cache;
        Event_ptr                   event_cursor_theme_changed;
        Event_ptr                   event_icon_theme_changed;
        Event_ptr                   event_conf;                     // Configuration changed.
        Actions                     actions;
        Conf                        conf;
        Items                       conf_pipe;
    };

    using Threads = std::map<std::thread::id, Thread>;

    Conf                conf_;
    Threads             threads_;
    Loop_impl *         cleanup_loop_ = nullptr;

private:

    void feed_icon_dir(const ustring & stop_after_theme=ustring()) const;
    bool feed_icon_root(const ustring & root, const ustring & stop_after_theme=ustring()) const;

    void cache_cursor(Cursor_cache & cache, Cursor_ptr cursor, const ustring & name, int size);
    Cursor_ptr uncache_cursor(Cursor_cache & cache, const ustring & name, int size);
    int find_cursor_theme(const ustring & name) const;
    int find_cursor_theme_nolock(const ustring & name) const;
    Cursor_ptr find_cursor_in_theme(int ctheme, const std::vector<ustring> & unames, std::vector<int> & seen, int size);

    int find_icon_theme(const ustring & name) const;
    int find_icon_theme_nolock(const ustring & name) const;
    Pixmap_ptr find_icon_in_theme(int itheme, const std::vector<ustring> & unames, const ustring & context, std::set<ustring> & seen, int size);
    Pixmap_ptr find_picto(const ustring & name, int size);

    Thread & update_this_thread();
    void boot_linkage(); // Linkage dependent method: shared (Unix/so/so-posix.cc) or static (Unix/a/a-posix.cc).
    void update_fonts();

    void on_loop_quit(Loop_impl * loop);
    void on_display_open(Display_impl *);
    void on_conf_set(std::string_view name, std::string_view value);
    void on_conf_event();
};

} // namespace tau

#endif // __TAU_THEME_IMPL_HH__

# -----------------------------------------------------------------------------
# SPDX-License-Identifier: BSD-2-Clause
# Copyright © 2014-2024 Konstantin Shmelkov <mrcashe@gmail.com>.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
# 1. Redistributions of source code must retain the above copyright notice,
#    this list of conditions and the following disclaimer.
#
# 2. Redistributions in binary form must reproduce the above copyright notice,
#    this list of conditions and the following disclaimer in the documentation
#    and/or other materials provided with the distribution.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
# THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
# PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
# CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
# EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
# PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
# OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
# WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
# OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
# EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
# -----------------------------------------------------------------------------

makefile	:= $(lastword $(MAKEFILE_LIST))
shared		:= $(findstring .so,$(makefile))
host		:= $(shell echo $(makefile) | sed -e 's/^...//' -e 's/\.so.mk//' -e 's/\.a.mk//')
msvc		:= $(findstring msvc,$(host))
include		$(confdir)/$(host).mk
libdir		:= $(bindir)
afile 		:= libtau$(Major_).$(Minor_).a
a 		:= $(libdir)/$(afile)
adest 		:= $(lib_prefix)/$(afile)
sofile 		:= libtau$(Major_).$(Minor_).dll
so 		:= $(bindir)/$(sofile)
sodest 		:= $(bin_prefix)/$(sofile)
tmplib		:= $(libdir)/tmp.a
ifneq (,$(msvc))
libfile 	:= $(libdir)/libtau$(Major_).$(Minor_).lib
libdest 	:= $(bin_prefix)/libtau$(Major_).$(Minor_).lib
pdbfile 	:= $(libdir)/libtau$(Major_).$(Minor_).pdb
pdbdest 	:= $(bin_prefix)/libtau$(Major_).$(Minor_).pdb
endif
hhs		:= $(addprefix $(hh_src)/,$(headers))
hhd		:= $(addprefix $(hh_prefix)/,$(headers))
oshare		:= $(if $(msvc),,$(libdir)/share.o)
shares		:= $(shell find $(topdir)/share \( -name "*.ico" -o -name "*.mo" \) | sed 's%$(topdir)/share/%%')

ifeq (,$(shared))
buildhost 	:= $(builddir)/$(host)-a
sources		+= a-impl.cc a-win.cc
lib		:= $(a)
install_lib	:= install-a
else
buildhost 	:= $(builddir)/$(host)-so
sources		+= so-impl.cc so-win.cc
lib		:= $(so)
install_lib	:= install-so
endif

CXXFLAGS 	+= $(if $(enable_debug),-g3 -O0,-g0 -O2)
CXXFLAGS	+= -I$(hh_src) -I$(srcdir) -pipe
srcdirs 	+= $(srcdir)/Windows
sources		+= conf-$(host).cc $(optional)
sources 	+= $(foreach dir, $(srcdirs), $(wildcard $(dir)/*.cc))
objects 	:= $(addprefix $(buildhost)/, $(addsuffix $(if $(msvc),.obj,.o), $(sort $(basename $(notdir $(sources))))))
VPATH 		:= $(confdir) $(srcdirs) $(srcdir)/opt
pc 		:= $(pc_prefix)/tau-$(Major_).$(Minor_)-$(host).pc
ppc 		:= $(ppc_prefix)/tau-$(Major_).$(Minor_).pc

all:	$(libdir) $(lib) $(oshare)

$(so):	$(objects)
	$(cxx) -shared -fvisibility=hidden -o $@ $(buildhost)/*$(if $(msvc),.obj,.o) $(CXXFLAGS) $(LDFLAGS)
	@chmod -x $@

$(a):	$(objects) $(bundle)
ifneq (,$(bundle))
	$(AR) r $(tmplib) $(buildhost)/*$(if $(msvc),.obj,.o)
	@echo "create $(a)\n addlib $(tmplib)\n $(addprefix addlib ,$(addsuffix \n,$(bundle))) save\n end" | $(AR) -M
	@$(rm) $(tmplib)
else
	$(AR) r $@ $(buildhost)/*$(if $(msvc),.obj,.o)
endif

ifneq (,$(WINDRES))
$(oshare): $(addprefix $(topdir)/share/,$(shares))
	@tmp=$$(mktemp); \
	for f in $(shares); do echo "$$(echo $$f | sed 's%/%:%g') RCDATA \"$(topdir)/share/$$f\"" >>$$tmp; done; \
	$(WINDRES) -J rc -o $@ $$tmp
	@rm -f $$tmp

$(lib_prefix)/share.o: $(lib_prefix)
	@$(cp) $(oshare) $@

install-oshare: $(lib_prefix)/share.o

uninstall-oshare:
	@$(rm) $(lib_prefix)/share.o

install: install-oshare
uninstall: uninstall-oshare

endif

clean:
	@$(rm) $(buildhost)/*$(if $(msvc),.obj,.o) $(buildhost)/*.dep $(lib)

rm:	clean
	@$(rmr) $(buildhost)

$(buildhost)/%$(if $(msvc),.obj,.o): %.cc
	@$(mkdir) $(buildhost)
	$(cxx) $< -o $@ $(CXXFLAGS) -c $(if $(nodeps),,-MD -MF $(basename $@).dep)

include $(wildcard $(buildhost)/*.dep)

install-so: $(bin_prefix)
	@$(cp) $(so) $(libfile) $(pdbfile) $(bin_prefix)
	@chmod -x $(sodest)
	@$(STRIP) --strip-unneeded $(sodest)

install-a: $(lib_prefix)
	@$(cp) $(a) $(adest)
	@$(STRIP) --strip-unneeded $(adest)

ifneq (,$(enable_devel))
$(pc):	$(confdir)/$(host).pc
	@$(mkdir) $(pc_prefix)
	@$(cp) $(confdir)/$(host).pc $@

ifneq (,$(ppc))
$(ppc):	$(confdir)/$(host).pc
	@$(mkdir) $(ppc_prefix)
	@$(cp) $(confdir)/$(host).pc $@
endif

install-pc: $(pc) $(ppc)

uninstall-pc:
	@$(rm) $(pc) $(ppc)

$(hh_prefix): $(hhs)
	@$(rm) $(hhd)
	@$(mkdir) $(hh_prefix)
	@$(cpr) $(hh_src)/* $(hh_prefix)

install-hh: $(hh_prefix)

uninstall-hh:
	@$(rm) $(hhd)

install: install-pc install-hh
uninstall: uninstall-pc uninstall-hh

endif

install: $(install_lib)

uninstall:
	@$(rm) $(adest) $(sodest) $(libdest) $(pdbdest)

$(libdir) $(lib_prefix) $(bin_prefix):
	@$(mkdir) $@

.PHONY:	install uninstall install-so install-a install-pc uninstall-pc install-hh uninstall-hh install-oshare uninstall-oshare

#END

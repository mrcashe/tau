# -----------------------------------------------------------------------------
# SPDX-License-Identifier: BSD-2-Clause
# Copyright © 2014-2024 Konstantin Shmelkov <mrcashe@gmail.com>.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
# 1. Redistributions of source code must retain the above copyright notice,
#    this list of conditions and the following disclaimer.
#
# 2. Redistributions in binary form must reproduce the above copyright notice,
#    this list of conditions and the following disclaimer in the documentation
#    and/or other materials provided with the distribution.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
# THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
# PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
# CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
# EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
# PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
# OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
# WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
# OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
# EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
# -----------------------------------------------------------------------------

makefile	:= $(lastword $(MAKEFILE_LIST))
host 		:= $(shell echo $(makefile) | sed -e 's/^...//' -e 's/\.so\.mk//' -e 's/\.a\.mk//')
so		:= $(findstring .so,$(makefile))
include		$(confdir)/$(host).mk
libdir		:= $(bindir)

ifeq (,$(so))
demodir 	:= $(builddir)/$(host)-demo-a
libtau		:= $(libdir)/libtau$(Major_).$(Minor_).a
static		:= $(libtau)
else
demodir 	:= $(builddir)/$(host)-demo-so
libtau		:= $(libdir)/libtau$(Major_).$(Minor_).so
endif

sources 	:= $(basename $(notdir $(wildcard $(srcdir)/demo/*.cc)))
binaries 	:= $(addprefix $(bindir)/, $(sources))
CXXFLAGS 	+= $(if $(enable_debug),-g3 -O0,-g0 -O2) -pthread -I$(hh_src)
VPATH 		:= $(srcdir)/demo

all: $(demodir) $(bindir) $(binaries)

install: $(bin_prefix) $(binaries) $(share_prefix)
	@for f in $(sources); do \
		if [ -h $(bin_prefix)/$$f ]; then \
			echo "!! Skipping install of $(bin_prefix)/$$f: destination is a symlink"; \
		elif [ -n "$(so)" ]; then \
			echo "Link $$f against just installed library..."; \
			$(cxx) $(srcdir)/demo/$$f.cc -o $(bin_prefix)/$$f $(CXXFLAGS) -L$(lib_prefix) -ltau$(Major_).$(Minor_) $(LDFLAGS); \
		else \
			$(cp) $(bindir)/$$f $(bin_prefix); \
		fi; \
	done
	@$(cpr) $(topdir)/share/cursors $(share_prefix)
	@$(cpr) $(topdir)/share/resource $(share_prefix)

uninstall:
	@for f in $(sources); do \
		if [ -h $(bin_prefix)/$$f ]; then $(info !! $(makefile): skipping removal of $(bin_prefix)/$$f: file is a symlink); \
		else rm -vf $(bin_prefix)/$$f; fi; \
	done		
	@$(rmr) $(share_prefix)/cursors $(share_prefix)/resource

$(bindir)/%: %.cc $(static)
	$(cxx) $< -o $@ $(CXXFLAGS) $(if $(nodeps),,-MD -MF $(demodir)/$(notdir $@).dep) $(libtau) $(LDFLAGS)

$(bindir) $(bin_prefix) $(demodir) $(share_prefix):
	@mkdir -vp $@

clean:
	@rm -vf $(binaries) $(demodir)/*.dep

rm: clean
	@rm -vrf $(demodir)

include $(wildcard $(demodir)/*.dep)

#END

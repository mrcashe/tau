# -----------------------------------------------------------------------------
# SPDX-License-Identifier: BSD-2-Clause
# Copyright © 2014-2024 Konstantin Shmelkov <mrcashe@gmail.com>.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
# 1. Redistributions of source code must retain the above copyright notice,
#    this list of conditions and the following disclaimer.
#
# 2. Redistributions in binary form must reproduce the above copyright notice,
#    this list of conditions and the following disclaimer in the documentation
#    and/or other materials provided with the distribution.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
# THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
# PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
# CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
# EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
# PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
# OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
# WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
# OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
# EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
# -----------------------------------------------------------------------------

all: all-build
install: all-build install-build install-share install-doc
uninstall: uninstall-build uninstall-share uninstall-doc
clean: clean-build clean-doc

include conf/conf.mk
has_machine	:= $(findstring $(machine),$(all_hosts))

ifneq (,$(unix_hosts))
include	$(supdir)/unix.mk
endif

ifneq (,$(win_hosts))
include $(supdir)/cross-win.mk
endif

ifneq (,$(freebsd_hosts))
include $(supdir)/cross-freebsd.mk
endif

ifneq (,$(xgettext))
include $(supdir)/po.mk
endif

ifneq (,$(debuild))
include $(supdir)/debuild.mk
endif

BUILD_TARGETS := all-build install-build uninstall-build clean-build

$(BUILD_TARGETS):
	@$(mkdir) $(builddir); target=`echo $@ | sed s/-build//`; $(MAKE) -f $(supdir)/build.mk -C $$builddir $$target || exit 1;

dist-clean rm:
	@rm -vrf $(builddir) $(bin) $(confdir) GNUmakefile

xz:
	@cd ../; \
	tar --exclude=*.mo -cJf tau-$(shell date +%Y%m%d).tar.xz \
		tau/share tau/doc tau/include tau/src tau/sup \
		tau/AUTHORS* tau/configure tau/LICENSE* tau/.gitignore tau/README* tau/VERSION

dist: doxy
	@cwd=$(PWD); \
	cd ../; \
	versioned="tau-$(Major_).$(Minor_).$(Micro_)"; \
	ln -sf tau $$versioned; \
	tar --exclude=*.mo -cJf $$versioned.tar.xz \
		$$versioned/include \
		$$versioned/share \
		$$versioned/doc \
		$$versioned/src \
		$$versioned/sup \
		$$versioned/AUTHORS* \
		$$versioned/configure \
		$$versioned/LICENSE* \
		$$versioned/README* \
		$$versioned/VERSION; \
	rm -f $$versioned; \
	cd $(builddir)/doxygen; \
	pdf=''; \
	if [ -d $(builddir)/doxygen/latex ]; then pdf='*.pdf'; fi; \
	doc="$$versioned-doc.tar.xz"; \
	tar -cJf $$doc html $$pdf; \
	mv -f $$doc $$cwd/..;\
	cd $$cwd

.PHONY: $(BUILD_TARGETS) doc doxy chk_doc rm xz dist dist-clean install-share uninstall-share install-doc uninstall-doc install-doxy

export headers		:= tau.hh $(addprefix tau/,$(notdir $(wildcard $(hh_src)/tau/*.hh)))

# -----------------------------------------------------------------------------
# Documentation and shares
# -----------------------------------------------------------------------------

doc_prefix 	:= $(prefix)/share/doc/libtau$(Major_).$(Minor_)

ifneq (,$(doxygen))
imgpath		:= $(topdir)/doc/img $(topdir)/share/icons/actions/22 $(topdir)/share/icons/devices/22
imgpath		+= $(topdir)/share/icons/places/22 $(topdir)/share/icons/status/22

doxy:
	@sed=$$(mktemp); \
	echo "s%PROJECT_NUMBER *=.*%PROJECT_NUMBER = $(Major_).$(Minor_).$(Micro_)%" >>$$sed; \
	echo "s%INPUT *=.*%INPUT = $(topdir)/README.md $(topdir)/doc $(topdir)/include%" >>$$sed; \
	echo "s%OUTPUT_DIRECTORY *=.*%OUTPUT_DIRECTORY = $(doxydir)%" >>$$sed; \
	echo "s%IMAGE_PATH *=.*%IMAGE_PATH = $(imgpath)%" >>$$sed; \
	echo "s%PROJECT_LOGO *=.*%PROJECT_LOGO = $(topdir)/share/icons/places/48/tau.ico%" >>$$sed; \
	echo "s%EXAMPLE_PATH *=.*%EXAMPLE_PATH = $(topdir)/doc/examples%" >>$$sed; \
	if [ -z "$(disable_pdf)" ] && [ -n "$(latex)" ]; then echo "s%GENERATE_LATEX *=.*%GENERATE_LATEX = YES%" >>$$sed; fi; \
	doxy=$$(mktemp); sed -f "$$sed" "$(topdir)/doc/Doxyfile" >$$doxy; \
	$(doxygen) "$$doxy"; rm -f $$sed $$doxy; \
	if [ -d $(doxydir)/latex ]; then \
		$(MAKE) -s -C $(doxydir)/latex pdf; \
		if [ ! -e $(doxydir)/latex/refman.pdf ]; then $(MAKE) -C $(doxydir)/latex; fi; \
		mv -f $(doxydir)/latex/refman.pdf $(doxydir)/tau-$(Major_).$(Minor_).pdf; \
	fi

install-doxy:
$(doxydir): doxy

ifneq (,$(has_machine))
install-doxy: $(doxydir)
	@if [ -d $(doxydir) ]; then \
	    $(rmr) $(DESTDIR)$(doc_prefix)/html; \
	    $(mkdir) $(DESTDIR)$(doc_prefix)/html; \
	    $(cpr) $(doxydir)/html/* $(DESTDIR)$(doc_prefix)/html; \
	    if [ -d $(doxydir)/latex ]; then $(cp) $(doxydir)/tau-$(Major_).$(Minor_).pdf $(DESTDIR)$(doc_prefix); fi; \
	fi

doc:	doxy
endif
endif

clean-doc:
	@$(rmr) $(doxydir)

doc:

install-doc: doc install-doxy
	@for host in $(all_hosts); do \
		if [ "$(machine)" == "$$host" ]; then dir=$(doc_prefix); else dir=$(prefix)/$$host/share/doc/libtau$(Major_).$(Minor_); fi; \
		$(mkdir) $(DESTDIR)$$dir; $(cp) $(topdir)/AUTHORS $(topdir)/LICENSE $(topdir)/README.md $(topdir)/VERSION $(DESTDIR)$$dir; \
	done

uninstall-doc:
	@for host in $(all_hosts); do \
		if [ "$(machine)" == "$$host" ]; then dir=$(doc_prefix); else dir=$(prefix)/$$host/share/doc/libtau$(Major_).$(Minor_); fi; \
		$(rmr) $(DESTDIR)$$dir; \
	done

install-share:
	@for host in $(all_hosts); do \
		if [ "$(machine)" == "$$host" ]; then dir=$(share_prefix); else dir=$(prefix)/$$host/share/tau-$(Major_).$(Minor_); fi; \
		$(mkdir) $(DESTDIR)$$dir; $(cpr) $(topdir)/share/icons $(DESTDIR)$$dir; \
	done

uninstall-share:
	@for host in $(all_hosts); do \
		if [ "$(machine)" == "$$host" ]; then dir=$(share_prefix); else dir=$(prefix)/$$host/share/tau-$(Major_).$(Minor_); fi; \
		$(rmr) $(DESTDIR)$$dir/icons; \
	done

#END

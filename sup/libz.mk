# -----------------------------------------------------------------------------
# SPDX-License-Identifier: BSD-2-Clause
# Copyright © 2014-2024 Konstantin Shmelkov <mrcashe@gmail.com>.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
# 1. Redistributions of source code must retain the above copyright notice,
#    this list of conditions and the following disclaimer.
#
# 2. Redistributions in binary form must reproduce the above copyright notice,
#    this list of conditions and the following disclaimer in the documentation
#    and/or other materials provided with the distribution.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
# THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
# PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
# CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
# EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
# PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
# OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
# WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
# OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
# EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
# -----------------------------------------------------------------------------

makefile	:= $(lastword $(MAKEFILE_LIST))
full		:= $(shell echo $(makefile) | sed -e 's/^.*libz-//' -e 's/.mk//')
host 		:= $(shell echo $(full) | sed 's/\(\.shared\|\.static\)//')
msvc		:= $(findstring msvc,$(host))
include		$(confdir)/$(full).mk
ar		:= zlib-1.3.1.tar.gz
tarball		:= $(builddir)/$(ar)
srcdir		:= $(builddir)/libz-$(host)
Prefix		:= $(bin)/$(host)
pcdir		:= $(Prefix)/lib
pc		:= $(pcdir)/zlib.pc
hostmk		:= $(confdir)/$(host).mk
pkgconf		:= pkg-config
ifneq ($(machine),$(host))
cross		:= $(host)-
cmake_cross	:= -DCMAKE_TOOLCHAIN_FILE=$(srcdir)/toolchain.cmake -DCMAKE_MAKE_PROGRAM=make
toolchain	:= $(srcdir)/toolchain.cmake
pkgconf		:= $(cross)pkg-config
ifeq (,$(shell which $(pkgconf) 2>/dev/null))
pkgconf		:= PKG_CONFIG_DIR='' PKG_CONFIG_LIBDIR=$(sysroot)/usr/local/pkgconfig PKG_CONFIG_SYSROOT_DIR=$(sysroot) pkg-config
endif
endif

all: $(pc)

clean:
install:
uninstall:

.PHONY: all clean install uninstall

$(pc):	$(toolchain) $(tarball)
	@$(mkdir) $(Prefix) $(srcdir)/build
	@(cd $(srcdir) && tar xf $(tarball) || exit 1)
	@cmake $(wildcard $(srcdir)/zlib-*) -B $(srcdir)/build $(cmake_cross) -DBUILD_SHARED_LIBS=NO -DCMAKE_BUILD_TYPE=Release -DCMAKE_INSTALL_PREFIX=$(Prefix) || exit 1
	@cmake --build $(srcdir)/build || exit 1
	@cmake --install $(srcdir)/build || exit 1
	@rm -rf $(srcdir)/build $(srcdir)/zlib-*
	@echo "CXXFLAGS     += $$(PKG_CONFIG_PATH=$(pcdir):$$PKG_CONFIG_PATH $(pkgconf) --cflags zlib)" >> $(hostmk)
	@echo "bundle       += $(Prefix)/lib/libz.a" >> $(hostmk)

$(srcdir)/toolchain.cmake:
	@$(mkdir) $(srcdir)
	@echo "set(CMAKE_SYSTEM_NAME $(this_plat))" >$@
	@echo "set(CMAKE_C_COMPILER $(cross)gcc)" >>$@
	@echo "set(CMAKE_FIND_ROOT_PATH $(sysroot))" >>$@
	@echo "set(CMAKE_FIND_ROOT_PATH_MODE_PROGRAM BOTH)" >>$@
	@echo "set(CMAKE_FIND_ROOT_PATH_MODE_LIBRARY ONLY)" >>$@
	@echo "set(CMAKE_FIND_ROOT_PATH_MODE_INCLUDE ONLY)" >>$@

$(tarball):
	@$(mkdir) $(srcdir)
	@wget https://www.zlib.net/$(ar) -O $@

#END

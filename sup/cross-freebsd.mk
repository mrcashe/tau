# -----------------------------------------------------------------------------
# SPDX-License-Identifier: BSD-2-Clause
# Copyright © 2014-2024 Konstantin Shmelkov <mrcashe@gmail.com>.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
# 1. Redistributions of source code must retain the above copyright notice,
#    this list of conditions and the following disclaimer.
#
# 2. Redistributions in binary form must reproduce the above copyright notice,
#    this list of conditions and the following disclaimer in the documentation
#    and/or other materials provided with the distribution.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
# THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
# PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
# CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
# EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
# PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
# OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
# WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
# OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
# EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
# -----------------------------------------------------------------------------

en-freebsd-a:
	@for host in $(freebsd_hosts); do \
		$(ln) "$(supdir)/unix-lib.mk" "$(builddir)/42-$$host.a.mk"; \
	done;

su-freebsd-a: su-freebsd-demo-a
	@for host in $(freebsd_hosts); do \
		$(rm) "$(builddir)/42-$$host.a.mk"; \
	done;

rm-freebsd-a: rm-freebsd-demo-a
	@for host in $(freebsd_hosts); do \
		if [ -e "$(builddir)/42-$$host.a.mk" ]; then $(MAKE) -C $(builddir) -f 42-$$host.a.mk rm; fi; \
		$(rm) "$(builddir)/42-$$host.a.mk"; \
	done;

en-freebsd-so:
	@for host in $(freebsd_hosts); do \
		$(ln) "$(supdir)/unix-lib.mk" "$(builddir)/40-$$host.so.mk"; \
	done;

su-freebsd-so: su-freebsd-demo-so
	@for host in $(freebsd_hosts); do \
		$(rm) "$(builddir)/40-$$host.so.mk"; \
	done;

rm-freebsd-so: rm-freebsd-demo-so
	@for host in $(freebsd_hosts); do \
		if [ -e "$(builddir)/40-$$host.so.mk" ]; then $(MAKE) -C $(builddir) -f 40-$$host.so.mk rm; fi; \
		$(rm) "$(builddir)/40-$$host.so.mk"; \
	done;

en-freebsd-demo-a: rm-freebsd-demo-so en-freebsd-a
	@for host in $(freebsd_hosts); do \
		$(ln) "$(supdir)/unix-demo.mk" "$(builddir)/60-$$host.a.mk"; \
	done;

su-freebsd-demo-a:
	@for host in $(freebsd_hosts); do \
		$(rm) "$(builddir)/60-$$host.a.mk"; \
	done;

rm-freebsd-demo-a:
	@for host in $(freebsd_hosts); do \
		if [ -e "$(builddir)/60-$$host.a.mk" ]; then $(MAKE) -C $(builddir) -f 60-$$host.a.mk rm; fi; \
		$(rm) $(builddir)/60-$$host.a.mk; \
	done;

en-freebsd-demo-so: rm-freebsd-demo-a en-freebsd-so
	@for host in $(freebsd_hosts); do \
		$(ln) "$(supdir)/unix-demo.mk" "$(builddir)/60-$$host.so.mk"; \
	done;

su-freebsd-demo-so:
	@for host in $(freebsd_hosts); do \
		$(rm) $(builddir)/60-$$host.so.mk; \
	done;

rm-freebsd-demo-so:
	@for host in $(freebsd_hosts); do \
		if [ -e "$(builddir)/60-$$host.so.mk" ]; then $(MAKE) -C $(builddir) -f 60-$$host.so.mk rm; fi; \
		$(rm) "$(builddir)/60-$$host.so.mk"; \
	done;

en-freebsd-demo:
	@for host in $(freebsd_hosts); do \
		if [ -e $(builddir)/40-$$host.so.mk ]; then $(MAKE) en-freebsd-demo-so; \
		else $(MAKE) en-freebsd-demo-a; fi; \
	done;

su-freebsd-demo: su-freebsd-demo-a su-freebsd-demo-so
rm-freebsd-demo: rm-freebsd-demo-a rm-freebsd-demo-so

en-freebsd: en-freebsd-a en-freebsd-so en-freebsd-demo
su-freebsd: su-freebsd-a su-freebsd-so su-freebsd-demo
rm-freebsd: rm-freebsd-a rm-freebsd-so rm-freebsd-demo

su-a:	 su-freebsd-a
su-so:	 su-freebsd-so
su-demo: su-freebsd-demo

rm-a:	 rm-freebsd-a
rm-so:	 rm-freebsd-so
rm-demo: rm-freebsd-demo

en-a: 	 en-freebsd-a
en-so:	 en-freebsd-so
en-demo: en-freebsd-demo
en-demo-a: en-freebsd-demo-a
en-demo-so: en-freebsd-demo-so

#END

# -----------------------------------------------------------------------------
# SPDX-License-Identifier: BSD-2-Clause
# Copyright © 2014-2024 Konstantin Shmelkov <mrcashe@gmail.com>.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
# 1. Redistributions of source code must retain the above copyright notice,
#    this list of conditions and the following disclaimer.
#
# 2. Redistributions in binary form must reproduce the above copyright notice,
#    this list of conditions and the following disclaimer in the documentation
#    and/or other materials provided with the distribution.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
# THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
# PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
# CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
# EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
# PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
# OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
# WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
# OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
# EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
# -----------------------------------------------------------------------------

en-unix-a:
	@for host in $(unix_hosts); do \
		$(ln) "$(supdir)/unix-lib.mk" "$(builddir)/22-$$host.a.mk"; \
	done;

su-unix-a: su-unix-demo-a
	@for host in $(unix_hosts); do \
		$(rm) "$(builddir)/22-$$host.a.mk"; \
	done;

rm-unix-a: rm-unix-demo-a
	@for host in $(unix_hosts); do \
		if [ -e "$(builddir)/22-$$host.a.mk" ]; then $(MAKE) -C $(builddir) -f 22-$$host.a.mk rm; fi; \
		$(rm) "$(builddir)/22-$$host.a.mk"; \
	done;

en-unix-so:
	@for host in $(unix_hosts); do \
		$(ln) "$(supdir)/unix-lib.mk" "$(builddir)/20-$$host.so.mk"; \
	done;

su-unix-so: su-unix-demo-so
	@for host in $(unix_hosts); do \
		$(rm) "$(builddir)/20-$$host.so.mk"; \
	done;

rm-unix-so: rm-unix-demo-so
	@for host in $(unix_hosts); do \
		if [ -e "$(builddir)/20-$$host.so.mk" ]; then $(MAKE) -C $(builddir) -f 20-$$host.so.mk rm; fi; \
		$(rm) "$(builddir)/20-$$host.so.mk"; \
	done;

en-unix-demo-a: rm-unix-demo-so en-unix-a
	@for host in $(unix_hosts); do \
		$(ln) "$(supdir)/unix-demo.mk" "$(builddir)/60-$$host.a.mk"; \
	done;

su-unix-demo-a:
	@for host in $(unix_hosts); do \
		$(rm) "$(builddir)/60-$$host.a.mk"; \
	done;

rm-unix-demo-a:
	@for host in $(unix_hosts); do \
		if [ -e "$(builddir)/60-$$host.a.mk" ]; then $(MAKE) -C $(builddir) -f 60-$$host.a.mk rm; fi; \
		$(rm) $(builddir)/60-$$host.a.mk; \
	done;

en-unix-demo-so: rm-unix-demo-a en-unix-so
	@for host in $(unix_hosts); do \
		$(ln) "$(supdir)/unix-demo.mk" "$(builddir)/60-$$host.so.mk"; \
	done;

su-unix-demo-so:
	@for host in $(unix_hosts); do \
		$(rm) $(builddir)/60-$$host.so.mk; \
	done;

rm-unix-demo-so:
	@for host in $(unix_hosts); do \
		if [ -e "$(builddir)/60-$$host.so.mk" ]; then $(MAKE) -C $(builddir) -f 60-$$host.so.mk rm; fi; \
		$(rm) "$(builddir)/60-$$host.so.mk"; \
	done;

en-unix-demo:
	@for host in $(unix_hosts); do \
		if [ -e $(builddir)/20-$$host.so.mk ]; then $(MAKE) en-unix-demo-so; \
		else $(MAKE) en-unix-demo-a; fi; \
	done;

su-unix-demo: su-unix-demo-a su-unix-demo-so
rm-unix-demo: rm-unix-demo-a rm-unix-demo-so

en-unix: en-unix-a en-unix-so en-unix-demo
su-unix: su-unix-a su-unix-so su-unix-demo
rm-unix: rm-unix-a rm-unix-so rm-unix-demo

su-a:	 su-unix-a
su-so:	 su-unix-so
su-demo: su-unix-demo

rm-a:	 rm-unix-a
rm-so:	 rm-unix-so
rm-demo: rm-unix-demo

en-a: 	 en-unix-a
en-so:	 en-unix-so
en-demo: en-unix-demo
en-demo-a: en-unix-demo-a
en-demo-so: en-unix-demo-so

#END

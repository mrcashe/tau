# -----------------------------------------------------------------------------
# SPDX-License-Identifier: BSD-2-Clause
# Copyright © 2014-2024 Konstantin Shmelkov <mrcashe@gmail.com>.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
# 1. Redistributions of source code must retain the above copyright notice,
#    this list of conditions and the following disclaimer.
#
# 2. Redistributions in binary form must reproduce the above copyright notice,
#    this list of conditions and the following disclaimer in the documentation
#    and/or other materials provided with the distribution.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
# THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
# PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
# CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
# EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
# PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
# OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
# WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
# OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
# EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
# -----------------------------------------------------------------------------

ifeq (,$(REVISION))
REVISION	:= 1
endif
debuild_base	:= libtau$(Major_).$(Minor_)
debuild_root	:= $(builddir)/debuild
debuild_bin	:= $(debuild_root)/bin/$(debuild_base)_$(Major_).$(Minor_).$(Micro_)-$(REVISION)
debuild_ppa	:= $(debuild_root)/ppa/$(debuild_base)_$(Major_).$(Minor_).$(Micro_)-$(REVISION)
debuild_debian	:= $(debuild_bin)/debian
debuild_arname	:= $(debuild_base)_$(Major_).$(Minor_).$(Micro_).orig.tar.xz
debuild_ar	:= $(debuild_root)/bin/$(debuild_arname)
debuild_doc	:= $(debuild_root)/bin/$(debuild_base)-doc_$(Major_).$(Minor_).$(Micro_)-$(REVISION)_all.deb

all:
clean:
	@$(rmr) $(debuild_root)

install:
uninstall:

# Debianize source, base dir is $1
define make_debian
	@$(mkdir) $1/debian/source
	@echo '3.0 (quilt)' >$1/debian/source/format
	@$(cp) $(supdir)/debian/changelog $(supdir)/debian/rules $1/debian
	@$(cp) $(topdir)/LICENSE $1/debian/copyright
	@cat $(supdir)/debian/control | sed 's%$${libtau}%$(debuild_base)%g' >$1/debian/control
	@cat $(supdir)/debian/libtau-dev.install | sed 's%$${libtau}%$(debuild_base)%g' >$1/debian/$(debuild_base)-dev.install
	@cat $(supdir)/debian/libtau-doc.install | sed 's%$${libtau}%$(debuild_base)%g' >$1/debian/$(debuild_base)-doc.install
	@cat $(supdir)/debian/libtau.install | sed 's%$${libtau}%$(debuild_base)%g' >$1/debian/$(debuild_base).install
endef

$(debuild_ar): $(debuild_bin)
	@tar --exclude=*.mo --xz -cf $@ include share doc src sup AUTHORS* configure LICENSE* README* VERSION
	@tar -xf $@ -C $<

$(debuild_bin): $(debuild_root)
	@$(call make_debian, $@)

$(debuild_doc): $(debuild_ar)
	@(cd $(debuild_bin) && debuild -i -us -uc -b $(if $(GPGKEY),-k$(GPGKEY),))
ifneq (,$(GPGKEY))
	@changes=$$(find $(debuild_root)/bin -name "*.changes"); \
	for chan in $$changes; do (cd $(debuild_root)/bin && debsign -k $(GPGKEY) $$chan); done
endif

# Builds binary package only.
debuild: $(debuild_doc)

# Builds source package only.
ifneq (,$(GPGKEY))
$(debuild_ppa):
	$(mkdir) $@

ppa:	$(debuild_ppa)
	@tar --exclude=*.mo --xz -cf $(debuild_root)/ppa/$(debuild_arname) include share doc src sup AUTHORS* configure LICENSE* README* VERSION
	@tar -xf $(debuild_root)/ppa/$(debuild_arname) -C $<
	@$(call make_debian, $<)
	@(cd $< && debuild -i -us -uc -S $(if $(GPGKEY),-k$(GPGKEY),))
	@changes=$$(find $(debuild_root)/ppa -name "*.changes"); \
	for chan in $$changes; do \
		(cd $(debuild_root)/ppa && debsign -k $(GPGKEY) $$chan); \
		(cd $(debuild_root)/ppa && dput ppa:mrcashe/ppa $$chan); \
	done
endif

.PHONY:	all clean install uninstall debuild

$(debuild_root):
	$(mkdir) $@

#END

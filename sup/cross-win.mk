# -----------------------------------------------------------------------------
# SPDX-License-Identifier: BSD-2-Clause
# Copyright © 2014-2024 Konstantin Shmelkov <mrcashe@gmail.com>.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
# 1. Redistributions of source code must retain the above copyright notice,
#    this list of conditions and the following disclaimer.
#
# 2. Redistributions in binary form must reproduce the above copyright notice,
#    this list of conditions and the following disclaimer in the documentation
#    and/or other materials provided with the distribution.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
# THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
# PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
# CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
# EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
# PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
# OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
# WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
# OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
# EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
# -----------------------------------------------------------------------------

en-mingw-a:
	@for host in $(mingw_hosts); do $(ln) $(supdir)/win-lib.mk $(builddir)/"31-$$host.a.mk"; done;

su-mingw-a: su-mingw-demo-a
	@for host in $(mingw_hosts); do $(rm) $(builddir)/"31-$$host.a.mk"; done;

rm-mingw-a: rm-mingw-demo-a
	@for host in $(mingw_hosts); do \
	    if [ -e "$(builddir)/31-$$host.a.mk" ]; then $(MAKE) -C $(builddir) -f "31-$$host.a.mk" rm; fi; \
	    $(rm) $(builddir)/"31-$$host.a.mk"; \
	done;

en-mingw-so:
	@for host in $(mingw_hosts); do $(ln) $(supdir)/win-lib.mk $(builddir)/"30-$$host.so.mk"; done;

su-mingw-so: su-mingw-demo-so
	@for host in $(mingw_hosts); do $(rm) $(builddir)/"30-$$host.so.mk"; done;

rm-mingw-so: rm-mingw-demo-so
	@for host in $(mingw_hosts); do \
		if [ -e $(builddir)/"30-$$host.so.mk" ]; then $(MAKE) -C $(builddir) -f "30-$$host.so.mk" rm; fi; \
		$(rm) $(builddir)/"30-$$host.so.mk"; \
	done;

en-mingw-demo-a: rm-mingw-demo-so en-mingw-a
	@for host in $(mingw_hosts); do $(ln) $(supdir)/win-demo.mk $(builddir)/"60-$$host.a.mk"; done

su-mingw-demo-a:
	@for host in $(mingw_hosts); do $(rm) $(builddir)/"60-$$host.a.mk"; done

rm-mingw-demo-a:
	@for host in $(mingw_hosts); do \
		if [ -e $(builddir)/"60-$$host.a.mk" ]; then $(MAKE) -C $(builddir) -f "60-$$host.a.mk" rm; fi; \
		$(rm) $(builddir)/"60-$$host.a.mk"; \
	done

en-mingw-demo-so: rm-mingw-demo-a en-mingw-so
	@for host in $(mingw_hosts); do $(ln) $(supdir)/win-demo.mk $(builddir)/"60-$$host.so.mk"; done

su-mingw-demo-so:
	@for host in $(mingw_hosts); do $(rm) $(builddir)/"60-$$host.so.mk"; done

rm-mingw-demo-so:
	@for host in $(mingw_hosts); do \
		if [ -e "$(builddir)/"60-$$host.demo.so.mk"" ]; then $(MAKE) -C $(builddir) -f "60-$$host.so.mk" rm; fi; \
		$(rm) $(builddir)/"60-$$host.so.mk"; \
	done

en-msvc-a:
	@for host in $(msvc_hosts); do $(ln) "$(supdir)/win-lib.mk" "$(builddir)/33-$$host.a.mk"; done

en-msvc-so:
	@for host in $(msvc_hosts); do $(ln) "$(supdir)/win-lib.mk" "$(builddir)/32-$$host.so.mk"; done

su-msvc-a: su-msvc-demo-a
	@for host in $(msvc_hosts); do $(rm) "$(builddir)/33-$$host.a.mk"; done

su-msvc-so: su-msvc-demo-so
	@for host in $(msvc_hosts); do $(rm) "$(builddir)/32-$$host.so.mk"; done

rm-msvc-a: rm-msvc-demo-a
	@for host in $(msvc_hosts); do \
		if [ -e "$(builddir)/33-$$host.a.mk" ]; then $(MAKE) -C $(builddir) -f 33-$$host.a.mk rm; fi; \
		$(rm) "$(builddir)/33-$$host.a.mk"; \
	done

rm-msvc-so: rm-msvc-demo-so
	@for host in $(msvc_hosts); do \
		if [ -e "$(builddir)/32-$$host.so.mk" ]; then $(MAKE) -C $(builddir) -f 32-$$host.so.mk rm; fi; \
		$(rm) "$(builddir)/32-$$host.so.mk"; \
	done

en-msvc-demo-a: rm-nsvc-demo-so en-msvc-a
	@for host in $(msvc_hosts); do $(link) $(supdir)/win-demo.mk $(builddir)/"60-$$host.a.mk"; done

en-msvc-demo-so: rm-msvc-demo-a en-msvc-so
	@for host in $(msvc_hosts); do $(link) $(supdir)/win-demo.mk $(builddir)/"60-$$host.so.mk"; done

su-msvc-demo-a:
	@for host in $(msvc_hosts); do $(rm) $(builddir)/"60-$$host.a.mk"; done

su-msvc-demo-so:
	@for host in $(msvc_hosts); do $(rm) $(builddir)/"60-$$host.so.mk"; done

rm-msvc-demo-a:
	@for host in $(msvc_hosts); do \
		if [ -e $(builddir)/"60-$$host.a.mk" ]; then $(MAKE) -C $(builddir) -f "60-$$host.a.mk" rm; fi; \
		$(rm) $(builddir)/"60-$$host.a.mk"; \
	done

rm-msvc-demo-so:
	@for host in $(msvc_hosts); do \
		if [ -e $(builddir)/"60-$$host.so.mk" ]; then $(MAKE) -C $(builddir) -f "60-$$host.so.mk" rm; fi; \
		$(rm) $(builddir)/"60-$$host.so.mk"; \
	done

en-win-demo-a:
	@for host in $(win_hosts); do \
	    if [ -e "$(builddir)/31-$$host.a.mk" ]; then $(MAKE) en-mingw-demo-a; fi; \
	    if [ -e "$(builddir)/33-$$host.a.mk" ]; then $(MAKE) en-msvc-demo-a; fi; \
	done;

en-win-demo-so:
	@for host in $(win_hosts); do \
	    if [ -e "$(builddir)/30-$$host.so.mk" ]; then $(MAKE) en-mingw-demo-so; fi; \
	    if [ -e "$(builddir)/32-$$host.so.mk" ]; then $(MAKE) en-msvc-demo-so; fi; \
	done;

en-win-demo:
	@for host in $(win_hosts); do \
		if [ -e "$(builddir)/30-$$host.so.mk" ]; then $(MAKE) en-mingw-demo-so; \
		elif [ -e "$(builddir)/31-$$host.a.mk" ]; then $(MAKE) en-mingw-demo-a; fi; \
		if [ -e "$(builddir)/32-$$host.so.mk" ]; then $(MAKE) en-msvc-demo-so; \
		elif [ -e "$(builddir)/33-$$host.a.mk" ]; then $(MAKE) en-msvc-demo-a; fi; \
	done;

su-msvc-demo: su-msvc-demo-a su-msvc-demo-so
rm-msvc-demo: rm-msvc-demo-a rm-msvc-demo-so

su-mingw-demo: su-mingw-demo-a su-mingw-demo-so
rm-mingw-demo: rm-mingw-demo-a rm-mingw-demo-so

en-mingw:  en-mingw-a en-mingw-so
su-mingw:  su-mingw-a su-mingw-so su-mingw-demo
rm-mingw:  rm-mingw-a rm-mingw-so rm-mingw-demo

su-a:	 su-mingw-a su-msvc-a
su-so:	 su-mingw-so su-msvc-so
su-demo: su-mingw-demo su-msvc-demo

rm-a:	 rm-mingw-a rm-msvc-a rm-mingw-demo-a rm-msvc-demo-a
rm-so:	 rm-mingw-so rm-mingw-demo-so rm-msvc-demo-so
rm-demo: rm-mingw-demo rm-msvc-demo

en-win-a: en-mingw-a en-msvc-a
en-win-so: en-mingw-so en-msvc-so

en-a: 	 en-win-a
en-so:	 en-win-so

en-demo: en-win-demo
en-demo-so: en-mingw-demo-so
en-demo-a: en-win-a

en-msvc: en-msvc-a
su-msvc: su-msvc-a
rm-msvc: rm-msvc-a rm-msvc-demo

su-win: su-msvc su-mingw
rm-win: rm-msvc rm-mingw

#END

# -----------------------------------------------------------------------------
# SPDX-License-Identifier: BSD-2-Clause
# Copyright © 2014-2024 Konstantin Shmelkov <mrcashe@gmail.com>.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
# 1. Redistributions of source code must retain the above copyright notice,
#    this list of conditions and the following disclaimer.
#
# 2. Redistributions in binary form must reproduce the above copyright notice,
#    this list of conditions and the following disclaimer in the documentation
#    and/or other materials provided with the distribution.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
# THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
# PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
# CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
# EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
# PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
# OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
# WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
# OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
# EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
# -----------------------------------------------------------------------------

podir 		:= $(topdir)/share/locale
po_prefix 	:= $(prefix)/share/locale
sources		+= $(topdir)/src/*.cc $(topdir)/src/*.hh
sources		+= $(topdir)/src/Windows/*.cc $(topdir)/src/Windows/*.hh
sources		+= $(topdir)/src/Linux/*.cc $(topdir)/src/Linux/*.hh
sources		+= $(topdir)/src/FreeBSD/*.cc $(topdir)/src/FreeBSD/*.hh
sources		+= $(topdir)/src/Unix/*.cc $(topdir)/src/Unix/*.hh
sources		+= $(topdir)/src/xcb/*.cc $(topdir)/src/xcb/*.hh
sources		+= $(topdir)/src/opt/*.cc
sources		+= $(topdir)/src/demo/*.cc
mo		:= tau-$(Major_).$(Minor_).mo

.PHONY: po-init po-update po-merge po-fmt po-install po-uninstall

install: po-install
uninstall: po-uninstall

# Update message catalog.
# Build this target after new *gettext() function calls added to source files.
# After this you have to translate updated LL_CC.po files using some translation tool, such as poedit.
# When translation is done, build 'msg-fmt' target.
po-update: $(podir)
	@xgettext $(sources) -o $(podir)/tau.pot --c++ --from-code=UTF-8 --keyword=lgettext --keyword=lngettext:1,2 --keyword=gettext_noop --sort-output --package-name=tau --msgid-bugs-address=mrcashe@gmail.com --package-version=2

# Init new translation.
po-new-%: $(podir)
	@echo Initialization of message catalog $*...
	@len=$(shell printf '%s' '$*' | wc -c);\
	if [ $$len -lt 2 ]; then echo "must specify locale name in form LL[_CC.ENCODING]" 1>&2; exit 1; fi
	@if [ -d $(podir)/$* ]; then echo "locale directory $(podir)/$* already exist" 1>&2; exit 1; fi
	@mkdir -vp $(podir)/$*/LC_MESSAGES
	@msginit --input=$(podir)/tau.pot --locale=$* --output-file=$(podir)/$*/LC_MESSAGES/tau.po

# Run after 'po-update' to merge new translations from tau.pot file into .po files.
po-merge: po-update
	@dirs=`find $(podir) -mindepth 1 -maxdepth 1 -type d`; \
	for dir in $$dirs; do \
	    locale=`basename $$dir`; \
	    po=$(podir)/$$locale/LC_MESSAGES/tau.po; \
	    msgmerge $$po $(podir)/tau.pot -o $$po --sort-output; \
	done

# Generate .mo files.
po-fmt: $(podir)
	@dirs=`find $(podir) -mindepth 1 -maxdepth 1 -type d`; \
	for dir in $$dirs; do \
	    locale=`basename $$dir`; \
	    msgfmt $(podir)/$$locale/LC_MESSAGES/tau.po --output-file=$(podir)/$$locale/LC_MESSAGES/$(mo); \
	done

po-install: po-fmt
	@for host in $(all_hosts); do \
		if [ "$(machine)" == "$$host" ]; then dest=$(po_prefix); else dest=$(prefix)/$$host/share/locale; fi; \
		dirs=`find $(podir) -mindepth 1 -maxdepth 1 -type d`; \
		for dir in $$dirs; do \
		dstdir=$(DESTDIR)$$dest/`basename $$dir`/LC_MESSAGES; \
		$(mkdir) $$dstdir; \
		$(cp) $$dir/LC_MESSAGES/*.mo $$dstdir; \
		done; \
	done

po-uninstall: $(podir)
	@for host in $(all_hosts); do \
		if [ "$(machine)" == "$$host" ]; then dest=$(po_prefix); else dest=$(prefix)/$$host/share/locale; fi; \
		dirs=`find $(podir) -mindepth 1 -maxdepth 1 -type d`; \
		for dir in $$dirs; do \
		dstdir=$(DESTDIR)$$dest/`basename $$dir`/LC_MESSAGES; \
		rm -vf $$dstdir/$(mo); \
		done; \
	done
:
$(podir):
	@mkdir -vp $@

#END

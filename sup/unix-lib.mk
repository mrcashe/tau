# -----------------------------------------------------------------------------
# SPDX-License-Identifier: BSD-2-Clause
# Copyright © 2014-2024 Konstantin Shmelkov <mrcashe@gmail.com>.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
# 1. Redistributions of source code must retain the above copyright notice,
#    this list of conditions and the following disclaimer.
#
# 2. Redistributions in binary form must reproduce the above copyright notice,
#    this list of conditions and the following disclaimer in the documentation
#    and/or other materials provided with the distribution.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
# THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
# PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
# CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
# EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
# PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
# OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
# WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
# OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
# EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
# -----------------------------------------------------------------------------

makefile	:= $(lastword $(MAKEFILE_LIST))
shared		:= $(findstring .so,$(makefile))
host 		:= $(shell echo $(makefile) | sed -e 's/^...//' -e 's/\.so.mk//' -e 's/\.a.mk//')
include		$(confdir)/$(host).mk
afile 		:= libtau$(Major_).$(Minor_).a
sofile 		:= libtau.so.$(Major_).$(Minor_).$(Micro_)
soname 		:= libtau.so.$(Major_).$(Minor_)
solink 		:= libtau$(Major_).$(Minor_).so
libdir 		:= $(bindir)
a 		:= $(libdir)/$(afile)
so 		:= $(libdir)/$(sofile)
adest 		:= $(lib_prefix)/$(afile)
sopath 		:= $(lib_prefix)/$(sofile)
install_lib	:= $(if $(shared),install-so,install-a)
uninst_files	+= $(if $(shared),$(lib_prefix)/$(solink) $(lib_prefix)/$(soname) $(sopath),$(adest))
buildhost 	:= $(builddir)/$(host)-$(if $(shared),so,a)
pc 		:= $(pc_prefix)/tau-$(Major_).$(Minor_)$(if $(cross),-$(cross),).pc
ppc		:= $(if $(ppc_prefix),$(ppc_prefix)/tau-$(Major_).$(Minor_).pc,)
hhs		:= $(addprefix $(hh_src)/,$(headers))
hhd		:= $(addprefix $(hh_prefix)/,$(headers))

ifeq (,$(shared))
sources		+= a-impl.cc a-unix.cc
lib		:= $(a)
else
sources		+= so-impl.cc so-unix.cc
CXXFLAGS 	+= -fpic
lib		:= $(so)
liblink		:= $(libdir)/$(solink)
endif

CXXFLAGS 	+= $(if $(enable_debug),-g3 -O0,-g0 -O2) -I$(hh_src) -I$(srcdir) -pipe
srcdirs 	+= $(srcdir)/xcb $(srcdir)/Unix $(srcdir)/$(this_plat)
VPATH 		:= $(confdir) $(srcdirs) $(srcdir)/opt
sources		+= conf-$(host).cc $(optional)
sources 	+= $(foreach dir, $(srcdirs), $(wildcard $(dir)/*.cc))
sources 	+= $(optional)
objects 	:= $(addprefix $(buildhost)/, $(addsuffix .o, $(sort $(basename $(notdir $(sources))))))

all: $(buildhost) $(libdir) $(lib)

$(so):	$(objects)
	$(cxx) -o $@ -shared -fvisibility=hidden $(CXXFLAGS) $(buildhost)/*.o $(LDFLAGS) && (cd $(libdir) && ln -sf $(sofile) $(solink))
ifeq 'Linux' "$(this_plat)"
    ifeq 'Linux' "$(plat)"
	@lddout=`LANG=C ldd -r $@ | grep undefined`; \
	if test -n "$$lddout"; then \
		echo "** $(makefile): undefined symbols in $@: " >/dev/stderr; \
		echo $$lddout >/dev/stderr; \
		exit 1; \
	fi;
    endif
endif
	@chmod -x $@

$(a):	$(objects)
	ar r $@ $(buildhost)/*.o

install-so: $(lib_prefix)
	@echo "Rebuild $(so) with -soname option..."
	$(cxx) -o $(sopath) -shared -Wl,-soname,$(soname) $(CXXFLAGS) $(buildhost)/*.o $(LDFLAGS)
	@chmod -x $(sopath)
	@$(STRIP) --strip-unneeded $(sopath)
	@(cd $(lib_prefix) && ln -vsf $(sofile) $(soname) && ln -vsf $(soname) $(solink))
ifeq 'root' "$(USER)"
	@ldconfig
endif

install-a: $(lib_prefix)
	$(cp) $(a) $(adest) && $(STRIP) --strip-unneeded $(adest)

ifneq (,$(enable_devel))

$(pc):	$(confdir)/$(host).pc
	@$(mkdir) $(pc_prefix)
	@$(cp) $(confdir)/$(host).pc $@

ifneq (,$(ppc))
$(ppc): $(confdir)/$(host).pc
	@$(mkdir) $(ppc_prefix)
	@$(cp) $(confdir)/$(host).pc $@
endif

install-pc: $(pc) $(ppc)

uninstall-pc:
	@$(rm) $(pc) $(ppc)

$(hh_prefix): $(hhs)
	@$(rm) $(hhd)
	@$(mkdir) $(hh_prefix)
	@$(cpr) $(hh_src)/* $(hh_prefix)

install-hh: $(hh_prefix)

uninstall-hh:
	@$(rm) $(hhd)

install: install-pc install-hh
uninstall: uninstall-pc uninstall-hh

endif

install: $(install_lib)

uninstall:
	@$(rm) $(uninst_files)

clean:
	@$(rm) $(buildhost)/*.o $(buildhost)/*.dep $(lib) $(liblink)

rm: clean
	@$(rmr) $(buildhost)

.PHONY: install uninstall clean install-so install-a install-hh uninstall-hh install-pc uninstall-pc rm

$(lib_prefix) $(buildhost) $(libdir):
	@$(mkdir) $@

$(buildhost)/%.o: %.cc
	$(cxx) $< -o $@ $(CXXFLAGS) -c $(if $(nodeps),,-MD -MF $(basename $@).dep)

include $(wildcard $(buildhost)/*.dep)

#END

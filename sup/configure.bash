#! /bin/bash
# -----------------------------------------------------------------------------
# SPDX-License-Identifier: BSD-2-Clause
# Copyright © 2014-2023 Konstantin Shmelkov <mrcashe@gmail.com>.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
# 1. Redistributions of source code must retain the above copyright notice,
#    this list of conditions and the following disclaimer.
#
# 2. Redistributions in binary form must reproduce the above copyright notice,
#    this list of conditions and the following disclaimer in the documentation
#    and/or other materials provided with the distribution.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
# THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
# PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
# CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
# EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
# PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
# OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
# WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
# OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
# EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
# -----------------------------------------------------------------------------

arg0='configure'
prefix='/usr/local'
GCC_MIN='11'
CLANG_MIN='12'

# Check program existence using 'which' command.
# Here is:
#   $1 is for variable name, which has form of 'which_$1', for example, 'which_pkgconfig'.
#   $2 is for searching command name (or names), for example, 'pkg-config'.
#   $3 is 'MANDATORY' for strict dependency and everything else for optional dependency.
function chk_which {
    local arr=($2)
    echo -n "$arg0: checking for ${arr[0]}... "
    local p

    for cmd in $2; do
        p=$(which $cmd 2>/dev/null)
        [ $? -eq 0 ] && break;
    done;

    if [ $? -eq 0 ]; then
        printf " $p\n"
        eval which_$1=$p
    else
        if [ 'MANDATORY' == "$3" ]; then
            echo "NOT FOUND"
            echo "$arg0: existence of '${arr[0]}' is mandatory, configure failed, exitting with status 1" 1>&2
            exit 1
        else
            echo "MISSING"
            eval which_$1=''
        fi
    fi
}

# Check file existence.
#   $1 is path to the file.
#   $2 is 'MANDATORY' for strict dependency and everything else for optional dependency.
function chk_file {
    echo -n "$arg0: checking for $(basename $1)... "

    if [ -e $1 ]; then
        echo "$1"
    elif [ 'MANDATORY' == $2 ]; then
        echo "NOT FOUND"
        echo "$arg0: existence of '$1' is mandatory, configure failed, exitting with status 1" 1>&2
        exit 1
    else
        echo "MISSING"
        CHK_FILE='NO'
        return 1
    fi

    return 0
}

# Find file $2 within directory $1
function find_file {
    echo -n "$arg0: searching for file $2 within $1... "
    local paths=$(find -L "$1" -name "$2" 2>/dev/null)
    if [ -z "$paths" ]; then echo "NOT FOUND, exitting with status 1" 1>&2; exit 1; fi
    echo $paths
}

# Check version:
#   $1 testing version string
#   $2 required version string
#   $3 testing program name for report
#   $4 if 'STRICT' then terminate script
function chk_ver {
    local testing=($(echo $1 | tr "." "\n"))
    local value=($(echo $2 | tr "." "\n"))
    local n=${#testing[@]}
    local i=0
    [ $n -gt ${#value[@]} ] && n=${#value[@]}

    while [ $i -ne $n ]; do
        if [ ${testing[i]} -lt ${value[i]} ]; then
            if [ "$4" == 'STRICT' ]; then
                echo "$arg0: $3 version too old: have $1 but $2 required, exitting with status 1" 1>&2
                exit 1
            else
                echo "$arg0: $3 version too old: have $1 but $2 desired, feature will be disabled" 1>&2
                return 1
            fi
        fi

        i=$[$i+1]
    done
    return 0
}

# Outputs usage.
function usage {
    echo "Usage: configure [options...]"
    echo "Options are:"
    echo "  --help, -h                      show this help"
    echo "  --build=<TRIPLET>               supported for compatibility"
    echo "  --host=<TRIPLET>                specify system where you want to install library"
    echo "  --prefix=<prefix>               set install prefix"
    echo "  --includedir=<DIR>              set header files installation prefix to <DIR>"
    echo "  --libdir=<DIR>                  set library files installation prefix to <DIR>"
    echo "  --mandir=<DIR>                  set man files installation prefix to <DIR> (not used)"
    echo "  --infodir=<DIR>                 set info files installation prefix to <DIR> (not used)"
    echo "  --sysconfdir=<DIR>              set sysconf files installation prefix to <DIR> (not used)"
    echo "  --localstatedir=<DIR>           set local state files installation prefix to <DIR> (not used)"
    echo "  --runstatedir=<DIR>             set run state files installation prefix to <DIR> (not used)"
    echo "  --conf-targets=<TARGETS>        add post-configure TARGETS, can be used multiple times"
    echo "  --with-mxe=<prefix>             use MXE installation from the specified prefix"
    echo "  --cross=<TARGETS|DIRS>          add TARGETS or DIRS for cross-compile, can be used multiple times"
    echo "  --enable-bundle                 bundle missing optional libraries into resulting library"
    echo "  --disable-option-checking       supported for compatibility"
    echo "  --disable-silent-rules          supported for compatibility"
    echo "  --disable-maintainer-mode       supported for compatibility"
    echo "  --disable-dependency-tracking   disable dependency tracking while compile"
    echo "  --enable-debug                  enable debug information generation"
    echo "  --enable-static                 enable static library building"
    echo "  --disable-shared                disable shared library building"
    echo "  --enable-demo                   enable demo suite building"
    echo "  --enable-devel                  enable development files creation and install"
    echo "  --disable-doc                   disable documentation generation"
    echo "  --disable-pdf                   disable pdf documentation generation"
}

function mxe_exists {
    echo -n "$arg0: checking for M cross environment existence... "
    [ -n "$mxe_prefix" ] && echo "$arg0: have user defined MXE prefix: <$mxe_prefix>..."
    local target
    local targets='i686-w64-mingw32.static i686-w64-mingw32.shared x86_64-w64-mingw32.static x86_64-w64-mingw32.shared'

    for target in $targets; do
        local pfx=$(which "$target-g++")
        if [ -n "$pfx" ]; then mxe_prefix=$(dirname $(dirname "$pfx")); echo "$mxe_prefix"; return 0; fi
    done

    echo " NOT FOUND"
}

# $1 = target
function mxe_check {
    CHK_FILE=''
    echo "$arg0: checking for MXE tools existence [$1]..."
    printf "  - "; chk_file "$mxe_prefix/bin/$1-g++"
    [ 0 != $? ] && return 1
    chk_ver $($mxe_prefix/bin/$1-g++ -dumpversion) $GCC_MIN "$mxe_prefix/bin/$1-g++"
    [ 0 != $? ] && return 1
    printf "  - "; chk_file "$mxe_prefix/bin/$1-ar"
    [ 'NO' == "$CHK_FILE" ] && return 1
    return 0
}

# $1 target triplet
# $2 sysroot
function configure {
    echo ""; echo "$arg0: configuring target $1... "
    local refined=$(echo $1 | sed -e 's/.shared//' -e 's/.static//')
    local plat
    local cross; [ "$1" != "$machine" ] && cross="$1"
    local pkgconf
    local pkg_required
    local libs_required
    local cxx="$CXX"
    local gettext
    local mk="$confdir/$refined.mk"
    cp "$supdir/LICENSE.mkt" $mk

    case $1 in
        *linux*)
            unix_hosts+="$1 "
            all_hosts+="$1 "
            plat='Linux'
            pkgconf='pkg-config'
            find_file "$2/usr/include" 'inotify.h'
            if test -n "$(find "$2/usr/include" -name "libintl.h" 2>/dev/null)"; then gettext="gettext-unix.cc gettext-real.cc"; fi
            echo "pc_prefix    := \$(DESTDIR)$lib_prefix/pkgconfig"             >>$mk
            echo "STRIP        := strip"                                        >>$mk
            pkg_required='xkbcommon-x11>=0.5.0 xcb>=1.11.1 xcb-cursor>=0.1.2 xcb-icccm>=0.4.1 xcb-renderutil>=0.3.9 xcb-screensaver>=1.11.1 xcb-sync>=1.11.1 xcb-xfixes>=1.11.1'
            libs_required+=' -lpthread '
            if [ -n "$cross" ]; then
                echo "ppc_prefix   := \$(DESTDIR)$prefix/$1/lib/pkgconfig"      >>$mk
            else
                cxx='$(CXX)'
            fi
        ;;

        *freebsd*)
            # FreeBSD lacks of zlib.pc file, however has libpng.pc and it works without zlib.pc.
            plat='FreeBSD'
            find_file "$2/usr/include" 'mount.h'
            all_hosts+="$1 "

            if test -n "$(find "$2/usr/local/include" -name "libintl.h" 2>/dev/null)"; then
                gettext="gettext-unix.cc gettext-real.cc"
                libs_required+='-lintl -liconv '
                echo "LDFLAGS      += -lintl -liconv"                           >>$mk
            fi

            if [ -n "$cross" ]; then
                freebsd_hosts+="$1 "
                pkgconf="PKG_CONFIG_DIR='' PKG_CONFIG_LIBDIR=$2/usr/local/libdata/pkgconfig PKG_CONFIG_SYSROOT_DIR=$2 pkg-config"
                cxx="clang++ --target=$1 -I$2/usr/local/include --sysroot=$2"
                echo "CXX           := $cxx"                                    >>$mk
                echo "pc_prefix     := \$(DESTDIR)$lib_prefix/pkgconfig"        >>$mk
                echo "ppc_prefix    := \$(DESTDIR)$prefix/$1/libdata/pkgconfig" >>$mk
            else
                cxx='$(CXX)'
                unix_hosts+="$1 "
                pkgconf='pkg-config'
                echo "pc_prefix     := \$(DESTDIR)$prefix/libdata/pkgconfig"    >>$mk
            fi

            echo "STRIP         := strip"                                       >>$mk
            pkg_required='libinotify xkbcommon-x11>=0.5.0 xcb>=1.11.1 xcb-cursor>=0.1.2 xcb-icccm>=0.4.1 xcb-renderutil>=0.3.9 xcb-screensaver>=1.11.1 xcb-sync>=1.11.1 xcb-xfixes>=1.11.1'
        ;;

        *mingw*)
            win_hosts+="$refined "
            mingw_hosts+="$refined "
            all_hosts+="$refined "
            plat='Windows'
            pkgconf="$1-pkg-config"
            cxx="$1-g++"
            libs_required+='-lgdi32 -lmsimg32 -lole32 -lshlwapi'
            echo "AR           := $1-ar"                                        >>$mk
            echo "STRIP        := $1-strip"                                     >>$mk
            echo "WINDRES      := $1-windres"                                   >>$mk
            echo "pc_prefix    := \$(DESTDIR)$lib_prefix/pkgconfig"             >>$mk
            echo "ppc_prefix   := \$(DESTDIR)$prefix/$refined/lib/pkgconfig"    >>$mk

            if test -n "$(find $2 -name "libintl.a" 2>/dev/null)"; then
                gettext="gettext-win.cc gettext-real.cc"
                libs_required+=' -lintl -liconv '
            fi

            echo "LDFLAGS      += -L$2/lib"                                     >>$mk
            echo "LDFLAGS      += $libs_required"                               >>$mk
        ;;

        *msvc*)
            msvc_hosts+="$1 "
            win_hosts+="$1 "
            all_hosts+="$1 "
            plat='Windows'
            local winplat=$(echo $1 | sed -e 's/x86_64-.*/x64/' -e 's/x86-.*/x86/')
            echo "LDFLAGS      += -L$(dirname `find $2 -name "user32.lib" | grep $winplat`)"      >>$mk
            echo "LDFLAGS      += -L$(dirname `find $2 -name "libcpmt.lib" | grep lib/$winplat`)" >>$mk
            echo "LDFLAGS      += -L$(dirname `find $2 -name "libucrt.lib" | grep $winplat`)"     >>$mk
            libs_required+=' -lkernel32 -luser32 -lgdi32 -lshlwapi -lshell32 -lole32 -ladvapi32 -lmsimg32 '
            echo "pc_prefix    := \$(DESTDIR)$lib_prefix/pkgconfig"             >>$mk
            echo "ppc_prefix   := \$(DESTDIR)$prefix/$refined/lib/pkgconfig"    >>$mk
            cxx="clang++ --target=$1 -fuse-ld=lld"
            cxxflags+=" -Wno-nonportable-include-path"
            cxxflags+=" -Wno-ignored-attributes"
            cxxflags+=" -Wno-pragma-pack"
            cxxflags+=" -Wno-ignored-pragma-intrinsic"
            cxxflags+=" -Wno-unknown-pragmas"
            cxxflags+=" -I$(dirname `find $2 -name "scoped_allocator"`)"
            cxxflags+=" -I$(dirname `find $2 -name "crtdbg.h"`)"
            cxxflags+=" -I$(dirname `find $2 -name "windows.h"`)"
            cxxflags+=" -I$(dirname `find $2 -name "winapifamily.h"`)"
            echo "CXXFLAGS     += $cxxflags"                                    >>$mk
            echo "LDFLAGS      += $libs_required"                               >>$mk
            echo "AR           := llvm-ar"                                      >>$mk
            echo "STRIP        := strip"                                        >>$mk
        ;;

        *)
            echo "$arg0: failed to determine host '$1' ($2), exitting with status 1" 1>&2
            exit 1
        ;;
    esac

    # Test if --enable-bundle option got. If so, enable package download and build.
    echo -n "$arg0: checking for GNU gettext for $1... "

    if [ -z "$gettext" ]; then
        if [[ "$1" =~ .*mingw.* ]] && [ -n "$bundle" ] && [ -n $which_wget ]; then
            echo "BUNDLE"
            echo -n "$arg0: checking for GNU libiconv for $1... "
            local iconv=$(find $2 -name "iconv.h")

            if [ -z "$iconv" ]; then
                echo "BUNDLE"
                ln -vsf "$supdir/libiconv.mk" "$builddir/00-libiconv-$1.mk"
            else
                echo "CXXFLAGS     += -I$(dirname $iconv)"                                  >>$mk
                echo "LDFLAGS      += -L$(dirname $(find $2 -name "libiconv.a")) -liconv"   >>$mk
                echo "Ok"
            fi

            ln -vsf "$supdir/libintl.mk" "$builddir/01-libintl-$1.mk"
            gettext='gettext-win.cc'
        else
            gettext='gettext-idle.cc'
            echo "NOT FOUND"
        fi
    else
        echo "Ok"
    fi

    echo "optional     += $gettext"                                             >>$mk
    echo "cxx          := $cxx"                                                 >>$mk

    if [ -n "$pkg_required" ]; then
        local tmp=$(mktemp)
        local pkg_filtered
        local pkg_needed

        for pkg in $pkg_required; do
            ver=$(echo $pkg | sed 's/.*>=//')
            [ "$ver" == "$pkg" ] && ver=''
            pkg=$(echo $pkg | sed 's/>=.*//')
            pkg_filtered+="$pkg "
            pkg_needed+="$pkg "
            [ -n "$ver" ] && pkg_needed+=">= $ver "
            if test -z "$ver"; then arg='--exists'; echo -n "  - $pkg... ";
            else arg="--atleast-version=$ver"; echo -n "  - $pkg at least $ver... "; fi;
            eval "$pkgconf" "$arg" --print-errors "$pkg" 2>$tmp

            if [ 0 -ne $? ]; then
                echo "FAILED"; cat "$tmp" 1>&2
                echo "$arg0: Package requirements check failed, exitting with status 1" 1>&2
                rm -f $tmp; exit 1
            else
                echo "Ok, version is $(eval "$pkgconf" --modversion $pkg)"
            fi
        done

        rm -f $tmp
        echo "CXXFLAGS      += $(eval "$pkgconf" --cflags $pkg_filtered)"       >>$mk
        libs_required+=$(eval "$pkgconf" --libs $pkg_filtered)
        echo "LDFLAGS       += $libs_required"                                  >>$mk
        echo "libs_required += $libs_required"                                  >>$mk
    fi

    if [ -n "$pkgconf" ]; then
        echo -n "$arg0: checking for libpng for $1... "
        local png=$(eval "$pkgconf" --libs libpng 2>/dev/null)

        if [ -n "$png" ]; then
            echo "Ok"
            libs_required+=" $png "
            echo "CXXFALGS      += $(eval "$pkgconf" --cflags libpng 2>/dev/null)" >>$mk
            echo "LDFLAGS       += $png"                                        >>$mk
            echo "optional      += pixmap-png.cc"                               >>$mk
            echo "libs_required += $png"                                        >>$mk
        else
            if [[ "$1" =~ .*mingw.* ]] && [ -n "$bundle" ] && [ -n $which_wget ]; then
                echo "BUNDLE"
                echo -n "$arg0: checking for zlib for $1... "
                local zlib=$(eval "$pkgconf" --libs zlib 2>/dev/null)

                if [ -n "$zlib" ]; then
                    echo "Ok"
                    libs_required+=" $zlib "
                    echo "CXXFALGS      += $(eval "$pkgconf" --cflags zlib)"    >>$mk
                    echo "LDFLAGS       += $zlib"                               >>$mk
                    echo "libs_required += $zlib"                               >>$mk
                else
                    echo "BUNDLE"
                    ln -vsf "$supdir/libz.mk" "$builddir/00-libz-$1.mk"
                fi

                ln -vsf "$supdir/libpng.mk" "$builddir/01-libpng-$1.mk"
                echo "optional      += pixmap-png.cc"                           >>$mk
                png='png'
            else
                echo "NOT FOUND"
            fi
        fi
    fi

    local libprefix
    local hhdest
    echo "this_plat    := $plat"                                                >>$mk
    [ -n "$2" ] && echo "sysroot      := $2"                                    >>$mk

    if [ -n "$cross" ]; then
        echo "cross        := $cross"                                           >>$mk
        echo "bindir       := $bin/$refined"                                    >>$mk
        echo "share_prefix := \$(DESTDIR)$prefix/$refined/share/tau-$Major_.$Minor_" >>$mk
        echo "bin_prefix   := \$(DESTDIR)$prefix/$refined/bin"                  >>$mk
        libprefix="$prefix/$refined/lib"
        hhdest="$prefix/$refined/include"
    else
        echo "bindir       := $bin"                                             >>$mk
        echo "share_prefix := \$(DESTDIR)$prefix/share/tau-$Major_.$Minor_"     >>$mk
        echo "bin_prefix   := \$(DESTDIR)$prefix/bin"                           >>$mk
        libprefix="$lib_prefix"
        hhdest="$hh_prefix"
    fi
    echo "hh_prefix    := \$(DESTDIR)$hhdest"                                   >>$mk
    echo "lib_prefix   := \$(DESTDIR)$libprefix"                                >>$mk

    local conf="$confdir/conf-$refined.cc"
    cp "$supdir/LICENSE.cct" $conf
    echo "#include <sys-impl.hh>"                                               >>$conf
    echo "#include <tau/sysinfo.hh>"                                            >>$conf
    [ -n "$png" ] && echo "#include <pixmap-impl.hh>"                           >>$conf
    echo ""                                                                     >>$conf
    echo "namespace tau {"                                                      >>$conf
    echo ""                                                                     >>$conf
    echo "Sysinfo sysinfo_ = {"                                                 >>$conf
    echo "    .Major    = $Major_,"                                             >>$conf
    echo "    .Minor    = $Minor_,"                                             >>$conf
    echo "    .Micro    = $Micro_,"                                             >>$conf
    echo "    .plat     = \"$plat\","                                           >>$conf
    echo "    .uname    = \"$plat\","                                           >>$conf
    echo "    .host     = \"$1\","                                              >>$conf
    echo "    .abits =  8*sizeof(void*),"                                       >>$conf
    echo "    .ibits =  8*sizeof(int),"                                         >>$conf
    echo "    .lbits =  8*sizeof(long),"                                        >>$conf
    echo "    .llbits = 8*sizeof(long long),"                                   >>$conf
    echo "    .mbits =  8*sizeof(intmax_t),"                                    >>$conf
    echo "    .wcbits = 8*sizeof(wchar_t)"                                      >>$conf
    echo "};"                                                                   >>$conf
    echo ""                                                                     >>$conf
    echo "const char * gettext_domain_ = \"tau-$Major_.$Minor_\";"              >>$conf
    echo ""                                                                     >>$conf
    echo "void boot_plat() {"                                                   >>$conf
    [ -n "$png" ] && echo "    pixmap_png_init();"                              >>$conf
    echo "}"                                                                    >>$conf
    echo ""                                                                     >>$conf
    echo "} // namespace tau"                                                   >>$conf
    echo ""                                                                     >>$conf
    echo "//END"                                                                >>$conf

    # Writing .pc file.
    local pc="$confdir/$refined.pc"
    echo "prefix=$prefix"                                                        >$pc
    echo "libdir=$libprefix"                                                    >>$pc
    echo "includedir=$hhdest"                                                   >>$pc
    echo "a=$libprefix/libtau$Major_.$Minor_.a -pthread -lm $libs_required"     >>$pc
    if test -n "$cross"; then
        echo "host=$1"                                                          >>$pc
        echo "CXX=$cxx"                                                         >>$pc
    fi
    echo ""                                                                     >>$pc
    echo "Name: tau"                                                            >>$pc
    echo "Description: C++ GUI toolkit"                                         >>$pc
    echo "Version: $Major_.$Minor_.$Micro_"                                     >>$pc
    echo "Libs: -L\${libdir} -ltau$Major_.$Minor_"                              >>$pc
    echo "Requires.private: $pkg_needed"                                        >>$pc
    echo "Libs.private: -lpthread -lm $libs_required"                           >>$pc
    echo "Cflags: -pthread -I\${includedir} $cxxflags"                          >>$pc

    echo "--- Summary for $1 ---"
    echo -n "GNU gettext:              "; if [ 'gettext-idle.cc' != "$gettext" ]; then echo 'yes'; else echo 'no'; fi
    echo -n "libpng:                   "; if [ -n "$png" ]; then echo 'yes'; else echo 'no'; fi
    $cxx -c -o /dev/null $CXXFLAGS $cxxflags "$supdir/test-pmr.cc" 2>/dev/null; local pmr=$?
    echo -n "Polymorphic allocators:   "; if [ 0 -eq $pmr ]; then echo 'yes'; else echo 'no'; fi
}

# -----------------------------------------------------------------------------
# Start here.
# -----------------------------------------------------------------------------

opts=$@

for opt in $opts; do
    case "$opt" in
        --help | -h)
        usage
        exit 0
    ;;

    *)
    ;;
    esac
done

cwdir=$(pwd)
builddir="$cwdir/build"
bin="$cwdir/bin"
confdir="$cwdir/conf"
chk_which 'basename' 'basename' 'MANDATORY'
arg0="$(basename $0)"
chk_which 'cat' 'cat' 'MANDATORY'

# Program from GNU gettext-runtime.
chk_which 'envsubst' 'envsubst' 'OPTIONAL'

argv="$cwdir/configure.argv"

if [ -e $argv ]; then
    if test -n "$which_envsubst"; then opts="$(cat $argv | envsubst) $opts";
    else opts="$(cat $argv) $opts"; fi
    echo "$arg0: found $(basename $argv) file, using its contents"
fi

chk_which 'sed' 'sed' 'MANDATORY'
chk_which 'dirname' 'dirname' 'MANDATORY'

# Not found in NetBSD!
chk_which 'realpath' 'realpath' 'OPTIONAL'
if test -n "$which_realpath"; then topdir="$(realpath $(dirname $(dirname $0)))";
else topdir="$(echo $PWD/$(dirname $(dirname $0)) | sed 's/\.$//')"; fi

chk_which 'uname' 'uname' 'MANDATORY'
plat=$(uname)
chk_which 'bash' 'bash' 'MANDATORY'
chk_which 'rm' 'rm' 'MANDATORY'
rm -vrf $confdir/* $builddir/lib* $bin/*
link='cp -vf'
doxydir="$builddir/doxygen"
supdir="$topdir/sup"
srcdir="$topdir/src"
shdir="$topdir/share"
hh_src="$topdir/include"
CXXFLAGS="-std=c++20 -Wall"
OPT='0'

chk_which 'cp' 'cp' 'MANDATORY'
chk_which 'ln' 'ln' 'MANDATORY'
chk_which 'ls' 'ls' 'MANDATORY'
chk_which 'mkdir' 'mkdir' 'MANDATORY'
chk_which 'mktemp' 'mktemp' 'MANDATORY'
chk_which 'find' 'find' 'MANDATORY'
chk_which 'tr' 'tr' 'MANDATORY'
chk_which 'grep' 'grep' 'MANDATORY'
chk_which 'cxx' 'c++ g++ clang++' 'MANDATORY'
machine=$($which_cxx -dumpmachine)
hosts="$machine"
chk_which 'ar' 'ar' 'MANDATORY'
chk_which 'pkgconfig' 'pkg-config' 'MANDATORY'
chk_which 'make' "$gmake" 'MANDATORY'
chk_which 'debuild' "debuild"

for opt in $opts; do
    case "$opt" in
        --prefix=*)
            pfx=$(echo -n $opt |sed 's%--prefix=%%')
            [ -n "$pfx" ] && prefix=$pfx
        ;;

        --with-mxe=*)
            pfx=$(echo -n $opt |sed 's%--with-mxe=%%')
            [ -n "$pfx" ] && mxe_prefix=$pfx
        ;;

        --cross=*)
            targets=$(echo -n $opt |sed 's/--cross=//')
            for target in $targets; do hosts+=" $target "; done
        ;;

        --enable-bundle)
            bundle='true'
        ;;

        --build=*)
            build_arch="$(echo -n $opt |sed 's%--build=%%')"
        ;;

        --host=*)
            hosts="$(echo -n $opt |sed 's%--host=%%')"
        ;;

        --includedir=*)
            hh_prefix="$(echo -n $opt |sed 's%--includedir=%%')"
        ;;

        --libdir=*)
            lib_prefix="$(echo -n $opt |sed 's%--libdir=%%')"
        ;;

        --mandir=*)
            man_dir="$(echo -n $opt |sed 's%--mandir=%%')"
        ;;

        --infodir=*)
            info_dir="$(echo -n $opt |sed 's%--infodir=%%')"
        ;;

        --sysconfdir=*)
            sysconf_dir="$(echo -n $opt |sed 's%--sysconfdir=%%')"
        ;;

        --localstatedir=*)
            localstate_dir="$(echo -n $opt |sed 's%--localstatedir=%%')"
        ;;

        --runstatedir=*)
            runstate_dir="$(echo -n $opt |sed 's%--runstatedir=%%')"
        ;;

        --conf-targets=*)
            targets=$(echo -n $opt |sed 's/--conf-targets=//')
            conf_targets+="$targets "
        ;;

        --disable-option-checking)
            disable_option_checking='YES'
        ;;

        --disable-silent-rules)
            disable_silent_rules='YES'
        ;;

        --disable-maintainer-mode)
            disable_maintainer_mode='YES'
        ;;

        --disable-dependency-tracking)
            nodeps='YES'
        ;;

        --disable-doc)
            disable_doc='YES'
        ;;

        --disable-pdf)
            disable_pdf='YES'
        ;;

        --enable-debug)
            enable_debug='true'
        ;;

        --enable-static)
            enable_static='true'
        ;;

        --disable-shared)
            disable_shared='true'
        ;;

        --enable-demo)
            enable_demo='true'
        ;;

        --enable-devel)
            enable_devel='true'
        ;;

        *)
            usage >&2
            echo "$arg0: unrecognized parameter '$opt', exiting with status 1" 1>&2
            exit 1
        ;;
    esac
done

chk_which 'gcc' 'g++'
chk_which 'clang' 'clang++'
chk_which 'tar' 'tar'
chk_which 'xz' 'xz'
chk_which 'latex' 'latex'
chk_which 'xgettext' 'xgettext'
chk_which 'wget' 'wget'

verfile="$topdir/VERSION"
if [ ! -e "$verfile" ]; then echo "$arg0: version file '$verfile' not found, exitting with status 1" 1>&2; exit 1; fi
ver=($(cat $verfile | tr "." "\n"))
if [ ${#ver[@]} -ne 3 ]; then echo "$arg0: version file '$verfile' has incorrect format, exitting with status 1" 1>&2; exit 1; fi
Major_=${ver[0]}; Minor_=${ver[1]}; Micro_=${ver[2]}
version="$Major_.$Minor_.$Micro_"
echo "$arg0: API version is '$version'"
echo "$arg0: install prefix is '$prefix'"
link='ln -vsf'
bin_prefix="$prefix/bin"
[ -z "$lib_prefix" ] && lib_prefix="$prefix/lib"
if [ -z "$hh_prefix" ]; then hh_prefix="$prefix/include/tau-$Major_.$Minor_"; else hh_prefix+="/tau-$Major_.$Minor_"; fi
share_prefix="$prefix/share/tau-$Major_.$Minor_"
mkdir -vp $confdir $builddir

if [ "$plat" == 'Linux' ]; then
    gmake='make'
    if test -n "$which_gcc"; then chk_ver $(g++ -dumpversion) $GCC_MIN 'g++' 'STRICT'; fi
elif [ "$plat" == 'FreeBSD' ]; then
    gmake='gmake'
fi

if test -n "$which_clang"; then
    chk_ver $(clang++ -dumpversion) $CLANG_MIN 'clang++'
    [ 0 -ne $? ] && which_clang=''
fi

if [ -z "$disable_doc" ]; then chk_which 'doxygen' 'doxygen' 'OPTIONAL'
else echo "$arg0: documentation generation disabled by user"; fi

# ---------------------------------------------------------------------------
# Loop over hosts.
# ---------------------------------------------------------------------------

for host in $hosts; do
    if [ -d "$host/VC" ]; then
        chk_which 'clang' 'clang++' 'MANDATORY'
        chk_which 'llvm_ar' 'llvm-ar' 'MANDATORY'
        chk_which 'lld' 'lld' 'MANDATORY'
        configure 'x86_64-windows-msvc' "$host"
    elif [[ "$host" =~ .*mingw.* ]]; then
        if [ -n "$(echo $host | grep '.static\|.shared')" ]; then
            [ -z "$mxe_prefix" ] && mxe_exists
            if [ -n "$mxe_prefix" ]; then
                mxe_check "$host"
                [ 0 -eq $? ] && configure "$host" "$mxe_prefix/$host"
            fi
        else
            configure "$host" "/usr/$host"
        fi
    elif [ -d "$host" ]; then
        target=$(find "$host" -maxdepth 1 -type d | sed 's%.*/%%g' | grep '-')
        if [ -n "$target" ]; then
            echo "$target"
            chk_which 'clang' 'clang++' 'MANDATORY'
            configure "$target" "$host"
        fi
    else
        split=(${host/:/ })
        configure "${split[0]}" "${split[1]}"
    fi
done

# ---------------------------------------------------------------------------
# Generating conf.mk
# ---------------------------------------------------------------------------

conf_mk="$confdir/conf.mk"
cp "$supdir/LICENSE.mkt" $conf_mk

if [ -z "$conf_targets" ]; then
    if [ -z "$disable_shared" ]; then
        [ -n "$unix_hosts" ] && conf_targets+='en-unix-so '
        [ -n "$mingw_hosts" ] && conf_targets+='en-mingw-so '
        [ -n "$msvc_hosts" ] && conf_targets+=' en-msvc-so '
        [ -n "$freebsd_hosts" ] && conf_targets+='en-freebsd-so '
    fi
fi

[ -n "$enable_static" ] && conf_targets+='en-a '
[ -n "$enable_demo" ] && conf_targets+='en-demo '

echo "export SHELL          := bash"                                            >>$conf_mk
echo "export plat           := $plat"                                           >>$conf_mk
echo "export machine        := $machine"                                        >>$conf_mk
echo "export topdir         := $topdir"                                         >>$conf_mk
echo "export builddir       := $builddir"                                       >>$conf_mk
echo "export doxydir        := $doxydir"                                        >>$conf_mk
echo "export confdir        := $confdir"                                        >>$conf_mk
echo "export supdir         := $supdir"                                         >>$conf_mk
echo "export bin            := $bin"                                            >>$conf_mk
echo "export srcdir         := $srcdir"                                         >>$conf_mk
echo "export srcdirs        := $srcdir"                                         >>$conf_mk
echo "export hh_src         := $hh_src"                                         >>$conf_mk
echo "export Major_         := $Major_"                                         >>$conf_mk
echo "export Minor_         := $Minor_"                                         >>$conf_mk
echo "export Micro_         := $Micro_"                                         >>$conf_mk
echo "export prefix         := $prefix"                                         >>$conf_mk
echo "export hh_prefix      := $hh_prefix"                                      >>$conf_mk
echo "export share_prefix   := $share_prefix"                                   >>$conf_mk
echo "export CXXFLAGS       := $CXXFLAGS"                                       >>$conf_mk
echo "export link           := $link"                                           >>$conf_mk
echo "export ln             := $link"                                           >>$conf_mk
echo "export cp             := cp -vfp"                                         >>$conf_mk
echo "export cpr            := cp -vRfp"                                        >>$conf_mk
echo "export rm             := rm -vf"                                          >>$conf_mk
echo "export rmr            := rm -vrf"                                         >>$conf_mk
echo "export mkdir          := mkdir -vp"                                       >>$conf_mk
[ -n "$disable_doc" ]       && echo "export disable_doc    := true"             >>$conf_mk
[ -n "$disable_pdf" ]       && echo "export disable_pdf    := true"             >>$conf_mk
[ -n "$enable_devel" ]      && echo "export enable_devel   := true"             >>$conf_mk
[ -n "$enable_debug" ]      && echo "export enable_debug   := true"             >>$conf_mk
[ -n "$nodeps" ]            && echo "export nodeps         := true"             >>$conf_mk
[ -n "$which_doxygen" ]     && echo "export doxygen        := $which_doxygen"   >>$conf_mk
[ -n "$which_latex" ]       && echo "export latex          := $which_latex"     >>$conf_mk
[ -n "$which_xgettext" ]    && echo "export xgettext       := $which_xgettext"  >>$conf_mk
[ -n "$which_debuild" ]     && echo "export debuild        := $which_debuild"   >>$conf_mk
[ -n "$freebsd_hosts" ]     && echo "export freebsd_hosts  := $freebsd_hosts"   >>$conf_mk
[ -n "$msvc_hosts" ]        && echo "export msvc_hosts     := $msvc_hosts"      >>$conf_mk
[ -n "$win_hosts" ]         && echo "export win_hosts      := $win_hosts"       >>$conf_mk
[ -n "$mingw_hosts" ]       && echo "export mingw_hosts    := $mingw_hosts"     >>$conf_mk
[ -n "$unix_hosts" ]        && echo "export unix_hosts     := $unix_hosts"      >>$conf_mk
[ -n "$all_hosts" ]         && echo "export all_hosts      := $all_hosts"       >>$conf_mk

# ---------------------------------------------------------------------------
# ---------------------------------------------------------------------------

echo "include $supdir/main.mk" >GNUmakefile
$gmake $conf_targets
exit 0

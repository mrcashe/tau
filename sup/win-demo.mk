# -----------------------------------------------------------------------------
# SPDX-License-Identifier: BSD-2-Clause
# Copyright © 2014-2024 Konstantin Shmelkov <mrcashe@gmail.com>.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
# 1. Redistributions of source code must retain the above copyright notice,
#    this list of conditions and the following disclaimer.
#
# 2. Redistributions in binary form must reproduce the above copyright notice,
#    this list of conditions and the following disclaimer in the documentation
#    and/or other materials provided with the distribution.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
# THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
# PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
# CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
# EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
# PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
# OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
# WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
# OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
# EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
# -----------------------------------------------------------------------------

makefile	:= $(lastword $(MAKEFILE_LIST))
host		:= $(shell echo $(makefile) | sed -e 's/^...//' -e 's/\.so\.mk//' -e 's/\.a\.mk//')
so		:= $(findstring .so,$(makefile))
msvc		:= $(findstring msvc,$(host))
include		$(confdir)/$(host).mk
libdir		:= $(bindir)

ifeq (,$(so))
demodir 	:= $(builddir)/$(host)-demo-a
libtau		:= $(libdir)/libtau$(Major_).$(Minor_).a
static		:= $(libtau)
else
demodir 	:= $(builddir)/$(host)-demo-so
libtau		:= $(bindir)/libtau$(Major_).$(Minor_).$(if $(msvc),lib,dll)
endif

CXXFLAGS	+= -I$(hh_src)
sources 	:= $(basename $(notdir $(wildcard $(srcdir)/demo/*.cc)))
exe		:= $(addsuffix .exe, $(sources))
binaries 	:= $(addprefix $(bindir)/,$(exe))
installed	:= $(addprefix $(bin_prefix)/,$(exe))
VPATH 		:= $(srcdir)/demo
CXXFLAGS 	+= $(if $(enable_debug),-g3 -O0,-g0 -O2)
LDFLAGS 	+= -static $(if $(msvc),,-mwindows)

all: $(demodir) $(bindir) $(binaries)

install: $(bin_prefix) $(share_prefix)
	@for file in $(exe); do $(cp) $(bindir)/$$file $(bin_prefix)/$$file; done
	@$(cpr) $(topdir)/share/cursors $(share_prefix)
	@$(cpr) $(topdir)/share/resource $(share_prefix)

uninstall:
	@$(rmr) $(installed) $(share_prefix)/cursors $(share_prefix)/resource

$(bindir)/%.exe: %.cc $(static)
	$(cxx) $< -o $@ $(CXXFLAGS) $(libtau) $(LDFLAGS) $(if $(nodeps),,-MD -MF $(demodir)/$(basename $(notdir $@)).dep)

$(bindir) $(bin_prefix) $(demodir) $(share_prefix):
	@$(mkdir) $@

clean:
	@$(rm) $(binaries) $(demodir)/*.dep

rm: clean
	@$(rmr) $(demodir)

include $(wildcard $(demodir)/*.dep)

#END

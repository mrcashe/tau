# -----------------------------------------------------------------------------
# SPDX-License-Identifier: BSD-2-Clause
# Copyright © 2014-2024 Konstantin Shmelkov <mrcashe@gmail.com>.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
# 1. Redistributions of source code must retain the above copyright notice,
#    this list of conditions and the following disclaimer.
#
# 2. Redistributions in binary form must reproduce the above copyright notice,
#    this list of conditions and the following disclaimer in the documentation
#    and/or other materials provided with the distribution.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
# THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
# PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
# CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
# EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
# PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
# OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
# WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
# OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
# EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
# -----------------------------------------------------------------------------

makefile	:= $(lastword $(MAKEFILE_LIST))
full		:= $(shell echo $(makefile) | sed -e 's/^.*libiconv-//' -e 's/.mk//')
host 		:= $(shell echo $(full) | sed 's/\(\.shared\|\.static\)//')
msvc		:= $(findstring msvc,$(host))
include		$(confdir)/$(full).mk
base		:= libiconv-1.17
ar		:= $(base).tar.gz
tarball		:= $(builddir)/$(ar)
srcdir		:= $(builddir)/libiconv-$(host)
Prefix		:= $(bin)/$(host)
libdir		:= $(Prefix)/lib
lib		:= $(libdir)/libiconv.a
hostmk		:= $(confdir)/$(host).mk
ifneq ($(machine),$(host))
cross		:= $(host)-
endif

all: 	$(lib)

clean:
install:
uninstall:

.PHONY: all clean install uninstall

$(lib): $(tarball)
	@$(mkdir) $(Prefix) $(srcdir)/build
	@(cd $(srcdir) && tar xf $(tarball) || exit 1)
	@(cd $(srcdir)/build && ../$(base)/configure --host=$(host) --prefix=$(Prefix) --disable-shared --enable-static || exit 1)
	@(cd $(srcdir)/build && $(MAKE) || exit 1)
	@(cd $(srcdir)/build && $(MAKE) install || exit 1)
	@$(rmr) $(srcdir)/build $(srcdir)/libiconv-*
	@echo "bundle      += $(libdir)/libiconv.a"	>> $(hostmk)
	@echo "CXXFLAGS    += -L$(Prefix)/include"	>> $(hostmk)

$(tarball):
	@$(mkdir) $(srcdir)
	@wget https://ftp.gnu.org/gnu/libiconv/$(ar) -O $@

#END

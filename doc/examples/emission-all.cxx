#include <tau.hh>

tau::signal_all<int>    signal_validate_;

bool on_validate1(int smt) {
    std::cout << smt << std::endl;
    return 42 == smt;   // Return true to continue emission.
}

bool on_validate2(int smt) {
    std::cout << smt << std::endl;
    return 41 == smt;   // Return false to stop emission.
}

// Unreachable.
bool on_validate3(int smt) {
    std::cout << smt << std::endl;
    return true;
}

int main(int, const char **) {
    // Emit empty signal -> the return value is true.
    std::cout << std::boolaplpha << signal_validate_(42) << std::endl;  // Outputs "true" to the console.

    // Connect 3 slots.
    signal_validate_.connect(tau::fun(on_validate1));
    signal_validate_.connect(tau::fun(on_validate2));
    signal_validate_.connect(tau::fun(on_validate3));

    // Emit not empty signal -> the return value is false, because on_validate2() returned false.
    signal_validate_(42);  // Outputs only 2 lines of "42" to the console.
    return 0;
}

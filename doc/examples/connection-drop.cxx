#include <tau.hh>

tau::signal<int()> signal_get_;

// ...

struct Getter {
    int get_something() const {
        return 42;
    }
};

// ...

int main(int, const char **) {
    Getter getter;
    tau::connection cx = signal_get_.connect(tau::fun(getter, &Getter::get_something));

    // Outputs: "slot count = 1, the result is 42".
    std::cout << "slot count = " << signal_get_.size() << ", the result is " << signal_get_() << std::endl;

    // Disconnect slot from the signal -> signal becomes empty.
    cx.drop();

    // Outputs: "slot count = 0, the result is 0".
    std::cout << "slot count = " << signal_get_.size() << ", the result is " << signal_get_() << std::endl;
    return 0;
}

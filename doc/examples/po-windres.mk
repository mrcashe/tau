mos     := $(shell find $(topdir) -name "*.mo")

mo.o:   $(mos)
	@tmp=`mktemp`; \
	dirs=$$(basename `ls -d $(topdir)/share/locale/*/`); \
	for dir in $$dirs; do \
		files=$$(find $(topdir) -name "*.mo"); \
		for file in $$files; do \
			name=$$(echo $$file | sed s%$(topdir)/share/locale/$$dir/LC_MESSAGES/%% | sed s%\\.mo%%); \
			echo 'mo-'"$$dir-"$$name RCDATA '"'$$file'"'>>$$tmp; \
		done; \
	done; \
	$(WINDRES) -J rc -o $@ $$tmp; \
	rm -f $$tmp

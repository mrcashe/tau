#include <tau.hh>

tau::signal<void(int)>  signal_something_;

void on_something1(int smt) {
    std::cout << smt << std::endl;
}

void on_something2(int smt) {
    std::cout << smt << std::endl;
}

void on_something3(int smt) {
    std::cout << smt << std::endl;
}

int main(int, const char **) {
    // All connected slots will be called.
    signal_something_.connect(tau::fun(on_something1));
    signal_something_.connect(tau::fun(on_something2));
    signal_something_.connect(tau::fun(on_something3));
    signal_something_(42);  // Outputs 3 lines of "42" to the console.
    return 0;
}

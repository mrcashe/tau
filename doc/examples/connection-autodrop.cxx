#include <tau.hh>

tau::signal<int()> signal_get_;

// ...

int get_something() { return 42; }

// ...

int main(int, const char **) {
    {
        // Set autodrop parameter to true while construct.
        tau::connection cx { true };
        cx = signal_get_.connect(tau::fun(get_something));

        // Outputs: "slot count = 1, the result is 42".
        std::cout << "slot count = " << signal_get_.size() << ", the result is " << signal_get_() << std::endl;

        // Disconnect slot from the signal when leaving scope -> signal becomes empty.
    }

    // Outputs: "slot count = 0, the result is 0".
    std::cout << "slot count = " << signal_get_.size() << ", the result is " << signal_get_() << std::endl;
    return 0;
}


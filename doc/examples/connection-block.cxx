#include <tau.hh>

tau::signal<int()> signal_get_;

// ...

int get_something() { return 42; }

// ...

int main(int, const char **) {
    tau::connection cx = signal_get_.connect(tau::fun(get_something));

    // Outputs: "blocked = false, the result is 42".
    std::cout << std::boolalpha << "blocked = " << cx.blocked() << ", the result is " << signal_get_() << std::endl;

    // Block connection twice.
    cx.block();
    cx.block();

    // Outputs: "blocked = true, the result is 0".
    std::cout << std::boolalpha << "blocked = " << cx.blocked() << ", the result is " << signal_get_() << std::endl;

    // Unblock once.
    cx.unblock();

    // Outputs: "blocked = true, the result is 0".
    std::cout << std::boolalpha << "blocked = " << cx.blocked() << ", the result is " << signal_get_() << std::endl;

    // Unblock one more time.
    cx.unblock();

    // Outputs: "blocked = false, the result is 42".
    std::cout << std::boolalpha << "blocked = " << cx.blocked() << ", the result is " << signal_get_() << std::endl;
    return 0;
}

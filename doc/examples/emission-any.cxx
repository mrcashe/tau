#include <tau.hh>

tau::signal<bool(int)>  signal_something_;

bool on_something1(int smt) {
    std::cout << smt << std::endl;
    return false;   // Return false to continue emission.
}

bool on_something2(int smt) {
    std::cout << smt << std::endl;
    return true;    // Return true to stop emission.
}

// Unreachable.
bool on_something3(int smt) {
    std::cout << smt << std::endl;
    return false;
}

int main(int, const char **) {
    signal_something_.connect(tau::fun(on_something1));
    signal_something_.connect(tau::fun(on_something2));
    signal_something_.connect(tau::fun(on_something3));
    signal_something_(42);  // Outputs only 2 lines of "42" to the console.
    return 0;
}

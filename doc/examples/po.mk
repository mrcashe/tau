# -----------------------------------------------------------------------------
# Update message catalog.
# Build this target after new gettext() function calls added to source files.
# After this you have to translate updated LL[_CC].po files using some translation tool, such as poedit.
# When translation is done, build 'msg-fmt' target.
# -----------------------------------------------------------------------------

project         := some-project
po_keywords     := --keyword=gettext_noop
podir           := $(topdir)/share/locale
po_prefix       := $(prefix)/share/locale
sources         += $(topdir)/src/*.cc $(topdir)/src/*.hh

# Init new translation.
po-new-%: $(podir)
	@echo Initialization of message catalog $*...
	@len=$(shell printf '%s' '$*' | wc -c);\
	if [ $$len -lt 2 ]; then echo "must specify locale name in form LL[_CC.ENCODING]" 1>&2; exit 1; fi
	@if [ -d $(podir)/$* ]; then echo "locale directory $(podir)/$* already exist" 1>&2; exit 1; fi
	@mkdir -vp $(podir)/$*/LC_MESSAGES
	@if [ ! -f $(podir)/$(project).pot ]; then xgettext --force-po $(po_src) -o $(podir)/$(project).pot --c++ --from-code=UTF-8 $(po_keywords) --sort-output --package-name=$(project) --msgid-bugs-address=mrcashe@gmail.com --package-version=2; fi
	@msginit --input=$(podir)/$(project).pot --locale=$* --output-file=$(podir)/$*/LC_MESSAGES/$(project).po
	@msgfmt $(podir)/$*/LC_MESSAGES/$(project).po --output-file=$(podir)/$*/LC_MESSAGES/$(project).mo

po-update: $(podir)
	@xgettext $(po_src) -o $(podir)/$(project).pot --c++ --from-code=UTF-8 $(po_keywords) --sort-output --package-name=$(project) --msgid-bugs-address=mrcashe@gmail.com --package-version=2

# Run after 'pot-update' to merge new and old data.
po-merge: po-update
	@dirs=`find $(podir) -mindepth 1 -maxdepth 1 -type d`; \
	for dir in $$dirs; do \
	    locale=`basename $$dir`; \
	    msgmerge $(podir)/$$locale/LC_MESSAGES/$(project).po $(podir)/$(project).pot -o $(podir)/$$locale/LC_MESSAGES/$(project).po --sort-output; \
	done

# Generate .mo files.
po-fmt: $(podir) po-merge
	@dirs=`find $(podir) -mindepth 1 -maxdepth 1 -type d`; \
	for dir in $$dirs; do \
	    locale=`basename $$dir`; \
	    msgfmt $(podir)/$$locale/LC_MESSAGES/$(project).po --output-file=$(podir)/$$locale/LC_MESSAGES/$(project).mo; \
	done

po-install: msg-fmt $(po_prefix)
	@dirs=`find $(podir) -mindepth 1 -maxdepth 1 -type d`; \
	for dir in $$dirs; do \
	    dstdir=$(po_prefix)/`basename $$dir`/LC_MESSAGES; \
	    mkdir -vp $$dstdir; \
	    cp -vp $$dir/LC_MESSAGES/*.mo $$dstdir; \
	done

po-uninstall: $(podir)
	@dirs=`find $(podir) -mindepth 1 -maxdepth 1 -type d`; \
	for dir in $$dirs; do \
	    dstdir=$(po_prefix)/`basename $$dir`/LC_MESSAGES; \
	    rm -vf $$dstdir/$(project).mo; \
	done

.PHONY: po-update po-merge po-fmt po-install po-uninstall

$(podir) $(po_prefix):
	@mkdir -vp $@

// ----------------------------------------------------------------------------
// SPDX-License-Identifier: BSD-2-Clause
// Copyright © 2014-2024 Konstantin Shmelkov <mrcashe@gmail.com>.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
// THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
// PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
// CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
// EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
// PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
// OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
// WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
// OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
// EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// ----------------------------------------------------------------------------

namespace tau {

/**

@page signal_guide Signals and Slots

@section connection_guide Using connection Class

@subsection connection_drop_example Disconnecting Slots Example
@include connection-drop.cxx

@subsection connection_autodrop_example Automatic Slot Disconnecting Example
@include connection-autodrop.cxx

@subsection connection_block_example Connection Block Example
@include connection-block.cxx

// ----------------------------------------------------------------------------

@section emission_schemes Using Different Emission Schemes

Three kinds of signal emission schemes provided:
- @ref emission_each
- @ref emission_any
- @ref emission_all

// ----------------------------------------------------------------------------

@subsection emission_each EACH Emission Scheme
Provided by signal< R(Args...)> class using @b void return value.
Each slot is visited unconditionally. Such a signal has no return value.

@include emission-each.cxx

// ----------------------------------------------------------------------------

@subsection emission_any ANY Emission Scheme
Provided by signal< R(Args...)> class using return value that
can be casted to @b bool. Emission continues until some slot
returns a value that can be interpreted as @b true. In this case this
value returned from signal operator(). In case all slots
return value interpreted as @b false, the signal return value will be
also @b false.

@include emission-any.cxx

// ----------------------------------------------------------------------------

@subsection emission_all ALL Emission Scheme
This scheme suitable for validation purposes.
Provided by signal_all class.
The return value type is always @b bool. Emission continues while
each slot returns @b true value. If some slot returns @b false value,
the emission result will be @b false. If all slots return @b true,
the result will be @b true.
When emission is done on empty signal (no slots connected), the
emission result will be @b true.

@include emission-all.cxx

**/

} // namespace tau

// ----------------------------------------------------------------------------
// SPDX-License-Identifier: BSD-2-Clause
// Copyright © 2014-2024 Konstantin Shmelkov <mrcashe@gmail.com>.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
// THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
// PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
// CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
// EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
// PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
// OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
// WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
// OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
// EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// ----------------------------------------------------------------------------

namespace tau {

/**

@page BUILD

- @b MAJOR is for major library version
- @b MINOR for minor version
- @b MICRO for micro version
- @b HOST is a host triplet, for example, @a i686-w64-mingw32
- @b machine  the platform on which the compilation tools are executed

The build system mimics GNU Autotools, namely: the configuration process is started by the `configure` script,
the build process is started by calling `make` (or `gmake`), the installation process is started by calling
`make install` (or `gmake install`). But in contrast to GNU Autotools, the @b tau build system allows to build
several targets using single `gmake` invocation.

For addition, development options can be enabled or disabled either by `configure` or `gmake`.
The development options include:
- C++ header and pkg-config *.pc files installation/de-installation.
- Doxygen and PDF documentation generation can be switched on/off independently.
- Individual cross-compilation targets can be independently enabled/disabled.

@section optional_sect Optional Libraries
Some dependencies are optional. Here is a complete list of optional packages:
- [GNU libiconv](https://www.gnu.org/software/libiconv/)
- [GNU gettext](https://www.gnu.org/software/gettext/)
- [zlib](https://www.zlib.net/)
- [libpng](http://www.libpng.org/pub/png/libpng.html)

@section configure_sect Configure
Detailed information about configure process

@note the relative paths looks like `./something..` originated from the project's root directory

`configure` script is a hand-written `bash(1)` script that generates configuration files used by
@ref make_sect "Make", @ref install_sect "Install" and @ref uninstall_sect "Uninstall" processes.
The root project directory contains a bootstrap script named `./configure` that calls real script
`./sup/configure.bash` which really works. Such a two-level design is dictated by the need to
launch the script on the Free BSD system, where default shell is `csh` but not `bash`, so up level
script which capable to run under both shells, calls `bash` on either Linux or Free BSD.

The generated configuration files collected within `./conf` subdirectory.

@subsection configure_opts Configure script options summary
<table>
<tr><th>Option                                   <th>Meaning
<tr><td><em>\--help, -h                     </em><td>show help message and exit
<tr><td><em>\--prefix=\<PREFIX\>            </em><td>@ref prefix_subsect "set install prefix"
<tr><td><em>\--build=\<ARCH\>               </em><td>not yet implemented, supported for compatibility
<tr><td><em>\--includedir=\<DIR\>           </em><td>set header files installation prefix to DIR
<tr><td><em>\--libdir=\<DIR\>               </em><td>set library files installation prefix to DIR
<tr><td><em>\--mandir=\<DIR\>               </em><td>set man files installation prefix to DIR (not used)
<tr><td><em>\--infodir=\<DIR\>              </em><td>set info files installation prefix to DIR (not used)
<tr><td><em>\--sysconfdir=\<DIR\>           </em><td>set sysconf files installation prefix to DIR (not used)
<tr><td><em>\--localstatedir=\<DIR\>        </em><td>set local state files installation prefix to DIR (not used)
<tr><td><em>\--runstatedir=\<DIR\>          </em><td>set run state files installation prefix to DIR (not used)
<tr><td><em>\--disable-option-checking      </em><td>supported for compatibility
<tr><td><em>\--disable-silent-rules         </em><td>supported for compatibility
<tr><td><em>\--disable-maintainer-mode      </em><td>supported for compatibility
<tr><td><em>\--enable-debug                 </em><td>Adds '-g -O0' flags to CXXFLAGS variable during compile. If not set, the flags are '-g0 -O2'
<tr><td><em>\--enable-devel                 </em><td>@ref endevel_subsect "enable development files creation and install"
<tr><td><em>\--enable-demo                  </em><td>@ref demo_subsect "enable demo suite building and install"
<tr><td><em>\--enable-static                </em><td>enable static library building
<tr><td><em>\--disable-shared               </em><td>disable shared library building
<tr><td><em>\--disable-doc                  </em><td>disable documentation generation
<tr><td><em>\--disable-pdf                  </em><td>disable pdf documentation generation
<tr><td><em>\--enable-bundle                </em><td>@ref bundle_subsect "bundle missing optional libraries"
<tr><td><em>\--cross=\<TARGETS|DIRS\>       </em><td>@ref cross_subsect "add target(s) or directory(ies) for cross-compile"
<tr><td><em>\--with-mxe=\<PREFIX\>          </em><td>@ref mxepfx_subsect "set MXE prefix"
<tr><td><em>\--conf-targets=\<TARGETS\>     </em><td>@ref post_subsect "set post-configure targets"
</table>

@subsection prefix_subsect \--prefix=\<PREFIX\>
Set install prefix

By default, the install prefix is `/usr/local` which is traditional for Unix systems. Using
this options, you can redefine the prefix. The value of prefix used by @ref install_sect "Install"
and @ref uninstall_sect "Uninstall" stages. The program code does not keep this value and do
not use it.

@subsection post_subsect \--conf-targets=\<TARGETS\>
Set post-configure targets

This is convenience options helps to automate post-configuration behaviour of `gmake`.
Targets, mentioned within this option, will be automatically built without user intervention.
For example, I added targets for demo suite building (which is disabled by default) and
sometimes use targets enabling static library generation. For more information see
@ref make_sect.

@subsection mxepfx_subsect \--with-mxe=\<PREFIX\>
Set MXE prefix

This option sets so-call MXE prefix, which is not an install prefix but prefix where
MXE itself installed. Actually, the `configure` can automatically detect this prefix,
but only when MXE binaries can be found using @b PATH environment variable. But if
@b PATH is not pointing to MXE binaries, using this option is required.

@subsection cross_subsect \--cross=\<HOSTS|DIRS\>
Add target(s) or directory(ies) for cross-compilation.

Possible hosts are:
- i686-w64-mingw32.static (Windows 32-bit, MXE static cross-compiler used)
- i686-w64-mingw32.shared (Windows 32-bit, MXE shared cross-compiler used)
- x86_64-w64-mingw32.static (Windows 64-bit, MXE static cross-compiler used)
- x86_64-w64-mingw32.shared (Windows 64-bit, MXE shared cross-compiler used)
- i686-w64-mingw32 (Windows 32-bit, system MinGW cross-compiler used)
- x86_64-w64-mingw32 (Windows 32-bit, system MinGW cross-compiler used)

Supported directories:
- FreeBSD
- MSVC

@subsection bundle_subsect \--enable-bundle
Bundle missing optional libraries to the library.
When optional library missing, the source package will be downloaded from the
internet and build processes will be performed for each host. The resulting
library merged into main tau library. Header files for those packages does not
provided. The complete list of optional packages can be found at @ref optional_sect.
Currently bundling available only for MinGW hosts.

@subsection demo_subsect \--enable-demo
Enable demo suite building

By default, the demo suite building is disabled. The demo suite consists of several
binary executable files that used as a demo bed and demo programs same time.

@subsection endevel_subsect \--enable-devel
Enable development files creation and install

By default, the development option is switched off. It means, the C++ header files,
pkg-config *.pc files and Doxygen documentation will not be installed. Using this
option, you can switch development option on.

@subsection argv_subsect configure.argv file
`configure` script reads this file on startup before some actions will be performed.

You can fill this file with any above options to automate configure process.
@note The location of `configure.argv` file determined by using @b $PWD environment
variable (see `man 1 pwd`) but not from directory where `configure` is located.

Here is the contents of my `configure.argv` file:
~~~
--prefix=$HOME
--cross=i686-w64-mingw32.static
--cross=x86_64-w64-mingw32.static
--enable-devel --enable-debug --enable-demo --disable-pdf
~~~

@note here is in the example you can see the `--conf-targets=` option used twice. This is
because `bash` splits given arguments by spaces and newlines, so if you like to assign more
than one post-configure target, use `--conf-targets=` option repeatedly with only one argument.

@section make_sect Make
Detailed information about make process
Series of targets named like <b>en-*</b>, <b>su-*</b> and <b>rm-*</b> provided, here is
@a en- for enable/resume feature, @a su- for suspend feature, @a rm- for disable/remove feature.

## Common targets

<table>
<tr><th>Target                  <th>Meaning
<tr><td>@a en-a                 <td>Enable static library building for all platforms
<tr><td>@a en-so                <td>Enable shared library building for all platforms
<tr><td>@a en-demo              <td>Enable demo suite building for all platforms
<tr><td>@a en-demo-a            <td>Enable demo suite building with static linkage for all platforms
<tr><td>@a en-demo-so           <td>Enable demo suite building with shared linkage for all platforms
<tr><td>@a su-a                 <td>Suspend static library building for all platforms
<tr><td>@a su-so                <td>Suspend shared library building for all platforms
<tr><td>@a su-demo              <td>Suspend demo suite building for all platforms
<tr><td>@a rm-a                 <td>Disable and remove static library building for all platforms
<tr><td>@a rm-so                <td>Disable and remove shared library building for all platforms
<tr><td>@a rm-demo              <td>Disable and remove demo suite building for all platforms
<tr><td>@a doc                  <td>Generate documentation if enabled
<tr><td>@a rm                   <td>Disable and remove all, `configure` invocation needed to enable
<tr><td>@a clean                <td>Remove all built object files, libraries and documentation
<tr><td>@a install              <td>Install everything enabled at the moment
<tr><td>@a uninstall            <td>Uninstall everything enabled at the moment
<tr><td>@a all                  <td>Build everything enabled at the moment
</table>

## Unix specific targets

<table>
<tr><th>Target                  <th>Meaning
<tr><td>@a en-unix-a            <td>Enable static library building for host platform
<tr><td>@a su-unix-a            <td>Suspend static library building for host platform
<tr><td>@a rm-unix-a            <td>Disable and remove static library building for host platform
<tr><td>@a en-unix-so           <td>Enable shared library building for host platform
<tr><td>@a su-unix-so           <td>Suspend shared library building for host platform
<tr><td>@a rm-unix-so           <td>Disable and remove shared library building for host platform
<tr><td>@a en-unix-demo-a       <td>Enable demo suite building with static linkage for host platform
<tr><td>@a su-unix-demo-a       <td>Suspend demo suite building with static linkage for host platform
<tr><td>@a rm-unix-demo-a       <td>Disable and remove demo suite building with static linkage for host platform
<tr><td>@a en-unix-demo-so      <td>Enable demo suite building with shared linkage for host platform
<tr><td>@a su-unix-demo-so      <td>Suspend demo suite building with shared linkage for host platform
<tr><td>@a rm-unix-demo-so      <td>Disable and remove demo suite building with shared linkage for host platform
<tr><td>@a en-unix-demo         <td>Enable demo suite building with static or shared linkage for host platform
<tr><td>@a su-unix-demo         <td>Suspend demo suite building for host platform
<tr><td>@a rm-unix-demo         <td>Disable and remove demo suite building for host platform
</table>

## Windows specific targets (cross-complie using MinGW gcc or MSVC using clang++)

<table>
<tr><th>Target                  <th>Meaning
<tr><td>@a en-mingw-a           <td>Enable static library building for MinGW platform
<tr><td>@a su-mingw-a           <td>Suspend static library building for MinGW platform
<tr><td>@a rm-mingw-a           <td>Disable and remove static library building for MinGW platform
<tr><td>@a en-mingw-so          <td>Enable shared library building for MinGW platform
<tr><td>@a su-mingw-so          <td>Suspend shared library building for MinGW platform
<tr><td>@a rm-mingw-so          <td>Disable and remove shared library building for MinGW platform
<tr><td>@a en-mingw-demo-a      <td>Enable demo suite building with static linkage for MinGW platform
<tr><td>@a su-mingw-demo-a      <td>Suspend demo suite building with static linkage for MinGW platform
<tr><td>@a rm-mingw-demo-a      <td>Disable and remove demo suite building with static linkage for MinGW platform
<tr><td>@a en-mingw-demo-so     <td>Enable demo suite building with shared linkage for MinGW platform
<tr><td>@a su-mingw-demo-so     <td>Suspend demo suite building with shared linkage for MinGW platform
<tr><td>@a rm-mingw-demo-so     <td>Disable and remove demo suite building with shared linkage for MinGW platform
<tr><td>@a en-mingw-demo        <td>Enable demo suite building with static or shared linkage for MinGW platform
<tr><td>@a su-mingw-demo        <td>Suspend demo suite building for MinGW platform
<tr><td>@a rm-mingw-demo        <td>Disable and remove demo suite building for MinGW platform
<tr><td>@a en-msvc-a            <td>Enable static library building
<tr><td>@a su-msvc-a            <td>Suspend static library building
<tr><td>@a rm-msvc-a            <td>Disable and remove static library building
<tr><td>@a en-msvc-so           <td>Enable shared library building
<tr><td>@a su-msvc-so           <td>Suspend shared library building
<tr><td>@a rm-msvc-so           <td>Disable and remove shared library building
<tr><td>@a rm-win               <td>Disable and remove all windows targets
</table>

## FreeBSD specific targets (cross-compile using clang++)

<table>
<tr><th>Target                  <th>Meaning
<tr><td>@a en-freebsd-a         <td>Enable static library building for host platform
<tr><td>@a su-freebsd-a         <td>Suspend static library building for host platform
<tr><td>@a rm-freebsd-a         <td>Disable and remove static library building for host platform
<tr><td>@a en-freebsd-so        <td>Enable shared library building for host platform
<tr><td>@a su-freebsd-so        <td>Suspend shared library building for host platform
<tr><td>@a rm-freebsd-so        <td>Disable and remove shared library building for host platform
<tr><td>@a en-freebsd-demo-a    <td>Enable demo suite building with static linkage for host platform
<tr><td>@a su-freebsd-demo-a    <td>Suspend demo suite building with static linkage for host platform
<tr><td>@a rm-freebsd-demo-a    <td>Disable and remove demo suite building with static linkage for host platform
<tr><td>@a en-freebsd-demo-so   <td>Enable demo suite building with shared linkage for host platform
<tr><td>@a su-freebsd-demo-so   <td>Suspend demo suite building with shared linkage for host platform
<tr><td>@a rm-freebsd-demo-so   <td>Disable and remove demo suite building with shared linkage for host platform
<tr><td>@a en-freebsd-demo      <td>Enable demo suite building with static or shared linkage for host platform
<tr><td>@a su-freebsd-demo      <td>Suspend demo suite building for host platform
<tr><td>@a rm-freebsd-demo      <td>Disable and remove demo suite building for host platform
</table>

## Debian/Ubuntu Linux specific targets
It is possible to build Debian packages on Ubuntu machines. Upload to the PPA requires the build variable GPGKEY was set:
```sh
make GPGKEY=1234567890 ppa
```

<table>
<tr><th>Target                  <th>Meaning
<tr><td>@a debuild              <td>Build packages for @b machine
<tr><td>@a ppa                  <td>Build packages for @b machine and upload to Launchpad
</table>

@section install_sect Install
The installation is made on the directory specified by <b><em>--prefix</em></b> parameter of the
`configure` script or to the default directory which is `/usr/local`.

@subsection install_shared Shared libraries
Shared library for @b machine will be installed into <b><em>PREFIX</em></b>/lib subdirectory.
Cross-compiled shared library will be installed into <b><em>PREFIX</em></b>/<b>HOST</b>/lib subdirectory,
for example, i686-w64-mingw32, x86_64-pc-windows-msvc.

There is 3 files will be installed:

- The library itself, named as <em>libtau.so.MAJOR.MINOR.MICRO</em>, for example, `libtau.so.0.7.0`;
- Symlink to the library named <em>libtau.so.MAJOR.MINOR</em>, so-called @a soname, for example, `libtau.so.0.7`;
- Symlink to the @a soname named <em>libtauMAJOR.MINOR.so</em>, so-called linker name, for example, `libtau0.7.so`.

The Windows DLL will be installed into <b><em>PREFIX</em></b>/<b>HOST</b>/bin subdirectory.
The DLL's name is <em>libtauMAJOR.MINOR.dll</em>, for example, `libtau0.7.dll`.

@subsection install_static Static libraries
Static library for @b machine will be installed into <b><em>PREFIX</em></b>/lib subdirectory.
Cross-compiled static library will be installed into <b><em>PREFIX</em></b>/<b>HOST</b>/lib subdirectory.
The library name is <em>libtauMAJOR.MINOR.a</em>, for example, `libtau0.7.a`.

@subsection install_demo Demo executable binaries
Binaries for @b machine will be installed into <b><em>PREFIX</em></b>/bin subdirectory.
Cross-compiled binaries will be installed into <b><em>PREFIX</em></b>/<b>HOST</b>/bin subdirectory.

-   The Unix names are @a taudemo, @a tautext, @a tauhello and so on.
-   The Windows names are <em>taudemo.exe</em> and so on.

@subsection install_hh C++ Headers
The header files will be installed for each target into its prefix directories:
- For @b machine it is <b><em>PREFIX</em></b>/include/tau-MAJOR.MINOR, for example, `/usr/local/include/tau-0.7`;
- For cross-compiled libraries it is <b><em>PREFIX</em></b>/<b>HOST</b>/include/tau-MAJOR.MINOR,
for example, `/usr/local/i686-w64-mingw32/include/tau-0.7`.

@subsection install_pc pkg-config files
For the @b machine there is single pc-file installed into:
-   <b><em>PREFIX</em></b>/lib/pkgconfig subdirectory on Linux;
-   <b><em>PREFIX</em></b>/libdata/pkgconfig subdirectory on FreeBSD.

`.pc` file name is tau-MAJOR.MINOR.pc, for example, `tau-0.7.pc`.

For cross-compiled libraries, two .pc files installed:
1. <b><em>PREFIX</em></b>/<b>HOST</b>/lib/pkgconfig/tau-MAJOR.MINOR.pc (Windows, Linux) or <b><em>PREFIX</em></b>/<b>HOST</b>/libdata/pkgconfig/tau-MAJOR.MINOR.pc (FreeBSD);
2. <b><em>PREFIX</em></b>/lib/pkgconfig/tau-MAJOR.MINOR-HOST.pc (Linux) or <b><em>PREFIX</em></b>/libdata/pkgconfig/tau-MAJOR.MINOR-HOST.pc (FreeBSD).

@subsection install_doc Documentation files
Documentation for the @b machine will be installed into <b><em>PREFIX</em></b>/share/doc/tau-MAJOR.MINOR subdirectory, for example, `/usr/local/share/doc/tau-0.7`.<br>
For the cross-compiled libraries HTML and PDF documentation not installed.

@subsection install_share Share stuff
Everything located into project's `./share` subdirectory will be copied into <b><em>PREFIX</em></b>/share/tau-MAJOR.MINOR subdirectory,
for example, `/usr/local/share/tau-0.7`.

@section uninstall_sect Uninstall
Everything that was installed will be removed.

**/

}

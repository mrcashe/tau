// ----------------------------------------------------------------------------
// SPDX-License-Identifier: BSD-2-Clause
// Copyright © 2014-2024 Konstantin Shmelkov <mrcashe@gmail.com>.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
// THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
// PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
// CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
// EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
// PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
// OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
// WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
// OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
// EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// ----------------------------------------------------------------------------

namespace tau {

/**

@page whats_new_050 Whats New in 0.5.0

Whats new in older releases:

- @subpage whats_new_040

----------------------------------------------------------

### Broken Toplevel methods

- Toplevel::signal_fullscreen() -> signature changed

### Broken Widget methods

- Widget::signal_display() -> renamed to Widget::signal_display_in()

### Broken Frame methods

- Frame::Frame(const ustring & label) -> removed
- Frame::Frame(const ustring & label, Align align) -> align parameter made default
- Frame::set_label(Widget & widget, Align align) -> default parameter added

### Broken Text methods

- Text::text() -> renamed to Text::str()
- Text::assign(Buffer buf): changed behaviour: now it assigns contents of buffer instead of changing buffer itself.

### Broken Entry methods

- Entry::text() -> renamed to Entry::str()
- Entry::buffer()
- Entry::select() -> signature changed to Entry::select(std::size_t begin, std::size_t end)
- Entry::move_to(const Buffer_citer & pos)
- Entry::move_to(std::size_t row, std::size_t col) -> signature changed to Entry::move_to(std::size_t col)
- Entry::caret() -> return value changed

### Broken Counter methods

- Counter::set_value() -> renamed to Counter::assign()
- Counter::text() -> renamed to Counter::str()

### Broken Icon methods

- Icon::set_icon_name(const ustring & icon_name) -> renamed to Icon::assign(const ustring & icon_name)
- Icon::set_icon_size() -> renamed to Icon::resize()

### Broken Notebook methods

- Notebook::show_page() -> added default parameter

### Broken Table methods

- Table::remove(int xmin, int ymin, int xmax, int ymax) -> signature changed to

### Broken List_text methods

- List_text::remove_text(const ustring & str) -> renamed to List_text::remove(const ustring & str)
- List_text::find(const ustring & str, bool similar) -> default parameter added
- List_text::contains(const ustring & str, bool similar) -> default parameter added

### Broken Action_menu_item methods

- Action_menu_item::Action_menu_item(Action & action, Align accel_align) -> added default parameter

### Broken Submenu_item methods

- Submenu_item::Submenu_item(const ustring & label, Menu & menu, const ustring & icon_name) -> added default parameter

### Broken Slot_menu_item methods

- Slot_menu_item::Slot_menu_item(const ustring & label, const slot< void()> & slot_activate, const ustring & icon_name) -> added default parameter

### Broken Toggle_menu_item methods

- Toggle_menu_item::Toggle_menu_itemToggle_menu_item(Toggle_action & toggle_action, Check_style check_style, Border border_style) -> check_style parameter no longer default

### Broken Menu methods

- Menu::append(const ustring & label, Menu & menu, const ustring & icon_name) -> added default parameter
- Menu::append(const ustring & label, const slot< void()> & slot_activate, const ustring & icon_name) -> added default parameter
- Menu::prepend(const ustring & label, Menu & menu, const ustring & icon_name) -> added default parameter
- Menu::prepend(const ustring & label, const slot< void()> & slot_activate, const ustring & icon_name) -> added default parameter
- Menu::insert_before(const ustring & label, Menu & menu, const Widget & other, const ustring & icon_name) -> added default parameter
- Menu::insert_before(const ustring & label, const slot< void()> & slot_activate, const ustring & icon_name, const Widget & other) ->removed
- Menu::insert_after(const ustring & label, Menu & menu, const Widget & other, const ustring & icon_name) -> added default parameter
- Menu::insert_after(const ustring & label, const slot< void()> & slot_activate, const Widget & other, const ustring & icon_name) -> added default parameter
- Menu::insert_after(const ustring & label, const slot< void()> & slot_activate, const ustring & icon_name, const Widget & other) -> removed
- Menu::append(Action & action, Align accel_align) -> added default parameter
- Menu::prepend(Action & action, Align accel_align) -> added default parameter
- Menu::insert_before(Action & action, const Widget & other, Align accel_align) -> added default parameter
- Menu::insert_after(Action & action, const Widget & other, Align accel_align) -> added default parameter
- Menu::append(Toggle_action & action, Check_style check_style, Border border_style) -> check_style parameter no longer default
- Menu::prepend(Toggle_action & action, Check_style check_style, Border border_style) -> check_style parameter no longer default
- Menu::insert_before(Toggle_action & action, const Widget & other, Check_style check_style, Border border_style) -> check_style parameter no longer default
- Menu::insert_after(Toggle_action & action, const Widget & other, Check_style check_style, Border border_style) -> check_style parameter no longer default

### Broken Action methods

- Action::set_icon_name() -> Action::renamed to set_icon()

### Broken Master_action methods

- Master_action::set_icon_name() -> renamed to Master_action::set_icon()

### Broken Font methods

- Font::glyph() -> now const qualified.

### Broken Key_file methods

- Key_file::has_key(const ustring & sect_name, const ustring & key_name, bool similar) -> removed due to clash with overloaded method.
- Key_file::get_strings(Key_section & sect, const ustring & key_name, std::size_t min_size, const ustring & fallback) -> added default parameters
- Key_file::get_strings(const ustring & key_name, std::size_t min_size, const ustring & fallback) -> added default parameters
- Key_file::get_booleans(Key_section & sect, const ustring & key_name, std::size_t min_size, bool fallback) -> added default parameters
- Key_file::get_booleans(const ustring & key_name, std::size_t min_size, bool fallback) -> added default parameters
- Key_file::get_integers(Key_section & sect, const ustring & key_name, std::size_t min_size, long long fallback) -> added default parameters
- Key_file::get_integers(const ustring & key_name, std::size_t min_size, long long fallback) -> added default parameters
- Key_file::get_doubles(Key_section & sect, const ustring & key_name, std::size_t min_size, double fallback) -> added default parameters
- Key_file::get_doubles(const ustring & key_name, std::size_t min_size, double fallback) -> added default parameters

### Broken Action_base methods

- Action_base::accels() -> return value type changed
- Action_base::signal_accel_removed() -> signal signature changed

### Broken Buffer_citer methods

- Buffer_citer::text(Buffer_citer other) const -> renamed to Buffer_citer::str(Buffer_citer other) const
- Buffer_citer::text(std::size_t nchars) const -> renamed to Buffer_citer::str(std::size_t nchars) const
- Buffer_citer::text32(Buffer_citer other) const -> renamed to Buffer_citer::wstr(Buffer_citer other) const
- Buffer_citer::text32(std::size_t nchars) const -> renamed to Buffer_citer::wstr(std::size_t nchars) const

### Broken Buffer methods

- Buffer::text() const -> renamed to Buffer::str() const
- Buffer::text(Buffer_citer begin, Buffer_citer end) const -> renamed to - Buffer::str(Buffer_citer begin, Buffer_citer end) const
- Buffer::text32() const -> renamed to Buffer::wstr() const
- Buffer::text32(Buffer_citer begin, Buffer_citer end) const -> renamed to - Buffer::wstr(Buffer_citer begin, Buffer_citer end) const

### Broken Text_element methods

- Text_element::text() -> renamed to Text_element::str()

### Broken Color_style methods

- Color_style::get() -> renamed to Color_style::value()

### Broken Color methods

- Color::inactive() -> renamed to Color::faded()

### Broken system functions

- user_name() -> renamed to login_name()

----------------------------------------------------------

### New classes

- Spinner

----------------------------------------------------------

### New Widget methods

- Widget::set_enable()
- Widget::set_visible()
- Widget::has_display()
- Widget::in_shutdown()
- Widget::signal_display_out()
- Widget::signal_focusable_changed()

### New Frame methods

- Frame::Frame(Border bs, const Color & border_color, unsigned border_width, int border_radius)
- Frame::Frame(const ustring & label, Border bs, const Color & border_color, unsigned border_width, int border_radius)
- Frame::Frame(const ustring & label, Align align, Border bs, const Color & border_color, unsigned border_width, int border_radius)
- Frame::empty()
- Frame::set_border(unsigned px, Border bs, int radius)
- Frame::set_border(unsigned px, Border bs, const Color & color, int radius)

### New Box methods

- Box::Box(Orientation orient, Align align, unsigned spacing)

### New Card methods

- Card::remove_others()
- Card::count()
- Card::current()
- Card::current() const

### New Check methods

- Check::set()
- Check::setup()

### New Counter methods

- Counter::cancel_action()
- Counter::signal_activate()
- Counter::wstr()
- Counter::assign() (no parameters)
- Counter::clear()
- Counter::empty()

### New Scroller methods

- Scroller::pan_to(Widget & w)

### New Text methods

- Text::Text(const std::u32string & ws, Align halign, Align valign)
- Text::set_buffer()
- Text::wstr()
- Text::assign(const std::u32string &)

### New Entry methods

- Entry::Entry(const std::u32string & ws, Border border_style)
- Entry::Entry(const std::u32string & ws, Align text_align, Border border_style)
- Entry::set_wrap_mode()
- Entry::wrap_mode()
- Entry::wstr()
- Entry::assign(const std::u32string &)

### New Fileman methods

- Fileman::mode()

### New Navigator methods

- Navigator::show_dirs_only()
- Navigator::show_all()
- Navigator::dirs_only_visible()

### New Fontsel methods

- Fontsel::set_show_monospace_only(bool yes)
- Fontsel::monospace_only_visible()

### New Table methods

- Table::Table(Align xalign, Align yalign)
- Table::Table(Align xalign, Align yalign, unsigned spacing)
- Table::Table(Align xalign, Align yalign, unsigned xspacing, unsigned yspacing)
- Table::remove(const Table::Span & range)

### New Notebook methods

- Notebook::append_page(Widget & w, const ustring & title, Align align, Widget_ptr * rptr)
- Notebook::prepend_page(Widget & w, const ustring & title, Align align)
- Notebook::insert_page(Widget & w, int nth_page, const ustring & title, Align align, Widget_ptr * rptr)
- Notebook::insert_page_after(Widget & w, Widget & after_this, const ustring & title, Align align, Widget_ptr * rptr)
- Notebook::insert_page_before(Widget & w, Widget & before_this, const ustring & title, Align align, Widget_ptr * rptr)

### New List methods

- List::has_selection()
- List::span()

### New List_text methods

- List_text::insert(int row, const ustring & str, int col, bool shrink)
- List_text::insert(int row, const ustring & str, int col, Align align)
- List_text::set_wrap_mode(Wrap wrap_mode)
- List_text::wrap_mode()
- List_text::has_selection()
- List_text::span()
- List_text::signal_text_changed()
- List_text::signal_text_validate()
- List_text::signal_text_cancel()
- List_text::allow_edit()
- List_text::disallow_edit()
- List_text::editable()
- List_text::allow_edit(int row)
- List_text::disallow_edit(int row)
- List_text::editable(int row)
- List_text::allow_edit(const ustring & text)
- List_text::disallow_edit(const ustring & text)
- List_text::editable(const ustring & text)
- List_text::edit()

### New Button_base methods

- Button_base::icon_size()
- Button_base::resize_icon()

### New Toggle methods

- Toggle::setup()
- Toggle::set()

### New Icon methods

- Icon::Icon(const Pixmap pix)
- Icon::assign(const Pixmap pix)
- Icon::empty() const

### New Image methods

- Image::empty() const

### New Menu methods

- Menu::append(const ustring & label, const slot< void()> & slot_activate, const Pixmap pix)
- Menu::prepend(const ustring & label, const slot< void()> & slot_activate, const Pixmap pix)
- Menu::insert_before(const ustring & label, const slot< void()> & slot_activate, const Widget & other, const Pixmap pix)
- Menu::insert_after(const ustring & label, const slot< void()> & slot_activate, const Widget & other, const Pixmap pix)
- Menu::append(const ustring & label, Menu & menu, const Pixmap pix)
- Menu::prepend(const ustring & label, Menu & menu, const Pixmap pix)
- Menu::insert_before(const ustring & label, Menu & menu, const Widget & other, const Pixmap pix)
- Menu::insert_after(const ustring & label, Menu & menu, const Widget & other, const Pixmap pix)
- Menu::append(Toggle_action & action, Align accel_align, Check_style check_style, Border border_style)
- Menu::prepend(Toggle_action & action, Align accel_align, Check_style check_style, Border border_style)
- Menu::insert_before(Toggle_action & action, Align accel_align, const Widget & other, Check_style check_style, Border border_style)
- Menu::insert_after(Toggle_action & action, Align accel_align, const Widget & other, Check_style check_style, Border border_style)
- Menu::append(Toggle_action & action, Align accel_align)
- Menu::prepend(Toggle_action & action, Align accel_align)
- Menu::insert_before(Toggle_action & action, Align accel_align, const Widget & other)
- Menu::insert_after(Toggle_action & action, Align accel_align, const Widget & other)
- Menu::append(Toggle_action & action)
- Menu::prepend(Toggle_action & action)
- Menu::insert_before(Toggle_action & action, const Widget & other)
- Menu::insert_after(Toggle_action & action, const Widget & other)

### New Action_menu_item methods

- Action_menu_item::set_icon(const ustring & icon_name, int icon_size)
- Action_menu_item::set_icon(const Pixmap pix)

### New Check_menu_item methods

- Check_menu_item::set()
- Check_menu_item::setup()
- Check_menu_item::joined()

### New Slot_menu_item methods

- Slot_menu_item::Slot_menu_item(const ustring & label, const slot<void()> & slot_activate, const Pixmap pix)
- Slot_menu_item::set_icon(const ustring & icon_name, int icon_size)
- Slot_menu_item::set_icon(const Pixmap pix)

### New Submenu_menu_item methods

- Submenu_item::Submenu_item(const ustring & label, Menu & menu, const Pixmap pix)
- Submenu_item::set_icon(const ustring & icon_name, int icon_size)
- Submenu_item::set_icon(const Pixmap pix)

### New Toggle_menu_item methods

- Toggle_menu_item::Toggle_menu_item(Toggle_action & toggle_action);
- Toggle_menu_item::Toggle_menu_item(Toggle_action & toggle_action, Align accel_align);
- Toggle_menu_item::Toggle_menu_item(Toggle_action & toggle_action, Align accel_align, Check_style check_style, Border border_style)

### New Theme methods

- Theme::bind_icon_theme()

### New Accel methods

- Accel::set_enable()

### New Action_base methods

- Action_base::set_enable()
- Action_base::set_visible()
- Action_base::signal_accel_changed()

### New Toggle_action methods

- Toggle_action::setup()
- Toggle_action::signal_setup()

### New Painter methods

- Painter::text_size(const std::wstring & s, Orientation orient)
- Painter::text(const std::wstring & s, const Color & c, Orientation orient)
- Painter::text(std::wstring && s, const Color & c, Orientation orient)

### New Key_file methods

- Key_file::load()
- Key_file::reload()
- Key_file::rename_section()
- Key_file::swap_sections()

### New Font methods

- Font::is_monospace()
- Font::list_families_async()

### New Timer methods

- Timer::flush()
- Timer::timeout()

### New File methods

- File::read_link()
- File::cp()
- File::icon()

### New Style methods

- Style::set_from()

### New ustring methods

- ustring::ustring(const std::wstring &)
- ustring::ustring(const wchar_t * wc)
- ustring::ustring(const wchar_t * wc, size_type nchars)
- ustring::operator std::wstring() const

### New path utils functions

- path_same()

### New string utils functions

- str_similar(const ustring & test, const ustring * vars, std::size_t array_size)

### New file utils functions

- file_is_regular()

### New system utils functions

- uri_escape()
- uri_unescape()

### New font utils functions

- Font::enlarge(const ustring & spec, double delta)

**/

} // namespace tau

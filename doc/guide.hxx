// ----------------------------------------------------------------------------
// SPDX-License-Identifier: BSD-2-Clause
// Copyright © 2014-2024 Konstantin Shmelkov <mrcashe@gmail.com>.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
// THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
// PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
// CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
// EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
// PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
// OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
// WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
// OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
// EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// ----------------------------------------------------------------------------

namespace tau {

/**

@page guide_page Programming Guide
A collection of notes, pages, source code snippets describing some aspects of library usage

@subpage pricnciples_guide

@subpage object_guide

@subpage signal_guide

@page pricnciples_guide Design Principles

### Signals and Slots

@ref signal_guide with automatic connection tracking used to organize information flow between objects. The connection tracking done
with using tau::trackable class as a base class for the most classes.

### PImpl Idiom and Facade Pattern

[PImpl Idiom](https://en.cppreference.com/w/cpp/language/pimpl) and [Facade Pattern](https://en.wikipedia.org/wiki/Facade_pattern)
are basic design principles of the @b tau library. Many user accessible classes (and entire widget stack) are facades made by using
PImpl idiom. Typically [std::shared_ptr](https://en.cppreference.com/w/cpp/memory/shared_ptr) used to hold implementation class,
while the facade open to the user contains only one data member - a pointer to the implementation of the class.

I will not dwell on the shortcomings of this principle of building a system; there is a lot of information on this subject on
specialized sites. As for this project, the advantages of this approach, without a doubt, more than compensate for the disadvantages.

#### Consequences and Limitations

##### Facade Copying

Facade copying does not copy object - it only increases reference count to holding object. As a rule, real objects hidden behind the facade
are not copiable. These are objects such as graphic widgets, including windows, and some other resource objects.
~~~
tau::Button yes { "YES" };
tau::Button no = yes;           // no references yes!
~~~
The correctness of above example is under the question because sometimes it is desirable to make a copy of some facade.

##### Facade Lifetime

As we all know, of course, variables in C++ can be placed in one of the following ways:
- As a static variable
- As a class data member
- As a temporary variable on the stack

Of course, first two ways are safe, without consequences. Issues coming when we allocate facade on the stack. The common error is to
connect facade's methods to some signal. Communication between the userspace and internal objects is organized anisotropically:
information from userspace to internal space can be sent by method call or (sometimes) by signal emission, but from the internal
space to user space only signals are working. Below is an example of incorrect code:
~~~
tau::signal<void()> signal_hide_;

void func() {
    tau::Button ok { "OK" };                            // Allocate button on the stack.
    container_.insert(ok);                              // Insert into some container.
    signal_hide_.connect(fun(ok, &tau::Button::hide));  // Wanting signal_hide_ hide the button...
                                                        // ...and connecting signal to the facade's method hide()!!!
}   // Facade died at this point and Button's implementation will never know when to hide!
    // Because tau::Button is a descendant of tau::trackable, the connection between signal and button dropped here.
~~~

So if you plan to control the behavior of the created library object, be sure to store it as a data member or statically.

##### Managed objects

@b Gtk framework used (or still use?) special function Gtk::manage() to mark widget as managed by the owning container. When we allocate
an object using **operator new**, this is always good question, when we must destroy it using **operator delete**. In our library there are
no need to use such a techique, because facade disappear in a natural way for the C++ language.
Opposite to above example information flow which is correct:
~~~
tau::signal<void()> signal_hide_;

void func() {
    tau::Button ok { "OK" };                            // Allocate button on the stack.
    container_.insert(ok);                              // Insert into some container.
    ok.signal_click().connect(fun(signal_hide_));       // Wanting Button click to emit signal_hide_.
}   // Facade died at this point but Button's signal_click_ belongs to the implementation, thus this code works properly.
    // The connection between internal signal_click_ signal_hide_ still alive.
~~~

##### Facades and Signals

Sometimes desirable to bind object or its pointer to the signal. This can be done using some of bind_* functions from signal.hh header.
~~~
void print(int value) { std::cout << __func__ << " " << value << std::endl; }

tau::signal<void()> signal_print_;

signal_print_.connect(bind_back(fun(print)), 42);
signal_print_();    // Prints "42".
~~~
The above example is correct but lets take a look at the incorrect one.
~~~

void on_fileman_apply(tau::Fileman fm) {
    // ... Do something
}

void on_file_open() {
    tau::Fileman fm;        // File manager widget is a facade with std::shared_ptr<> inside.
    fm.action_apply().connect(bind_front(fun(on_fileman_apply), fm));
    container_.insert(fm);   // Insert file manager into some container.
    // Fileman::action_apply_ is an Action which has signal inside.
    // Facade copied to the Fileman implementation along with self pointing std::shared_ptr<>.
    // The implementation has no chance to die before program exit!
}
~~~
@warning Do not bind shared pointers to the slots!

### Loop and Display

@remark The @b tau library is not a framework because:
- It does not invert control
- It does not parse command line arguments
- It hasn't Application object

But event loop is present: the object of class Loop is an event loop. The %Loop is a singleton within thread.
On Unix systems the heart of %Loop is a [poll(2)](https://man7.org/linux/man-pages/man2/poll.2.html) syscall
(might change in future), on Windows - [MsgWaitForMultipleObject()](https://learn.microsoft.com/en-us/windows/win32/api/winuser/nf-winuser-msgwaitformultipleobjects) function.
Each %Loop run within its separate thread.
~~~
int main(int, const char **) {
    std::cout << tau::Loop().id() << std::endl;     // Outputs loop id (most likely "0").
    // ...
    std::cout << tau::Loop().id() << std::endl;     // Outputs same loop id again.
    return 0;
}
~~~
Once %Loop quit, this is impossible to start %Loop again within that thread.
~~~
int main(int, const char **) {
    // ...
    tau::Loop().run();          // Will not return until somebody call Loop::quit()!
    // ...
    tau::Loop().run();          // Throws an exception: dead loop!
    return 0;
}
~~~
The Display is a companion to the %Loop. It is also singleton within same thread as its %Loop is. When %Loop quits, %Display closes.
While the %Loop performs actions not related to graphics, the %Display, on the contrary, provides interaction with the graphics subsystem.
This two-level organization is dictated by the nature of Unix systems, where system and graphical functions are separated.
On Windows, this division of roles is largely artificial. On the other hand, in the Windows system, graphic threads are separated
and graphic objects from one thread are independent of objects created in another thread. Unix family systems do not have such restrictions
and can have as many graphic threads as they like on one connection to the X server. Therefore, our library has adopted a compromise solution
that satisfies the requirements of both systems: there is a single %Loop on a thread and an accompanying single %Display, which in
X11-based systems holds a connection to the X-server.

Typically the programmer does not need to access the display directly: each widget ultimately has a connection to the %Display that is hidden
in implementation details. In contrast, %Loop started manually by the user by calling Loop::run() method. Usually this is done from
the main() function.
~~~
int main(int, const char **) {
    tau::Loop().run();          // At least this line must present in each program.
    return 0;
}
~~~
But sometimes desirable to open %Display before running loop.
~~~
int main(int, const char **) {
    int dpi = tau::Display().dpi(); // Fetch dpi value from the %Display.
    // ...
    tau::Loop().run();              // Will not return until somebody call Loop::quit()!
    return 0;
}
~~~
In the example above call to Display::dpi() function constructs an instance of %Display class, which brings up %Loop. When Loop::run() called,
the %Loop object already constructed.

Both %Display and %Loop are facades, but destroyng them does not mean destroying of the underlying implementation classes - there are
more shared pointers stored withing internal data structures.

### Windows

Window class is an abstract class in our library and can not be constructed.
To construct ordinar window with caption, frame and buttons we need to create object of class Toplevel.
~~~
int main(int, const char **) {
    tau::Toplevel wnd;
    tau::Loop().run();              // Will return until window close by clicking close button of pressing "Alt+F4".
    return 0;
}
~~~
This program will run as expected because %Toplevel constructor brings up current %Display (which brings up %Loop) and inserts itself to
the %Display client list. When %Toplevel quits, the %Display checks if some clients remining. If no, %Display calls Loop::quit() method
and Loop::run() quits.

The other types of windows: Dialog, Popup.

### Widget
A widget in library's terminology has a generally accepted meaning, namely, it is difficult to define exactly what it is. Those who are
familiar with the QWidget or GtkWidget classes can extend their knowledge to tau::Widget.

%Widget is a rectangular part of the screen with size accessed by Widget::size() method and origin within its parent accessed by Widget::origin().
%Widget has no children, it is a leaf on the widget's tree. %Widget can be hidden or visible: all widgets are visible from the construction, except
@ref window_group, which are hidden from the construction and have to be shown by calling Widget::show(). %Widget can be disabled by calling
Widget::disable() and than enabled by Widget::enable(). All widgets are enabled from the construction.

### Containers
Containers are widgets that has children, so containers are nodes in the widget's tree. Containers divided into @ref generic_container_group and
@ref compound_container_group. There is no universal container that can host widgets in any way. Instead, there are 8 types of generic containers,
each of which places child widgets in a specific way. The compound containers are combinations of generic containers. However, from the user point
of view, this is no matter.

Windows also generic containers and, of course, widgets too.

**/

} // namespace tau

// ----------------------------------------------------------------------------
// SPDX-License-Identifier: BSD-2-Clause
// Copyright © 2014-2024 Konstantin Shmelkov <mrcashe@gmail.com>.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
// THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
// PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
// CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
// EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
// PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
// OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
// WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
// OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
// EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// ----------------------------------------------------------------------------

namespace tau {

/**

@page whats_new_070 Whats New in 0.7.0

Whats new in older releases:

- @subpage whats_new_060
- @subpage whats_new_050
- @subpage whats_new_040

## Release Notes

- New build target x86_64-windows-msvc using clang++ (experimental)
- New build target i686-w64-mingw32 using installed system cross-compiler
- New build target x86_64-w64-mingw32 using installed system cross-compiler
- New build target x86_64-unknown-freebsd14.0 using clang++ (on Linux)
- Package build for Debian/Ubuntu now available
- Massive bugfix

## API Breaks

### Broken Action methods

- Action::connected() -> removed

### Broken Button methods

- Button::Button(const ustring &, int) -> signature changed, see Button::Button(int, const ustring &)
- Button::Button(const ustring &, const ustring &, int) -> signature changed, see Button::Button(const ustring &, int, const ustring &)

### Broken Card methods

- Card::insert() -> default parameter added

### Broken Conf methods

- Conf::set_from(std::string_view, std::string_view, bool) -> default parameter added
- Conf::set_from(Item, std::string_view, bool) -> default parameter added
- Conf::set_from(std::string_view, Item, bool) -> default parameter added
- Conf::set_from(Item, Item, bool) -> default parameter added

### Broken Contour methods

- Contour::from_rectangle() -> removed, see Contour::rectangle()
- Contour::from_arc() -> removed, see Contour::arc()

### Broken Display methods

- Display::signal_paste_text() -> removed, see Widget::signal_input()

### Broken Header methods

- Header::Header(bool) -> removed
- Header::Header(List_base &, bool) -> removed
- Header::attach_to(List_base &) -> removed

### Broken List_base methods

- List_base::signal_mark_validate() -> signal type changed

### Broken Matrix methods

- Matrix::Matrix(double, double, double, double, double, double) -> signature chnged
- Matrix::mul_distance() -> renamed to Matrix::distance()

### Broken Object methods

- Object::str(const ustring &) -> renamed to Object::set_string(const ustring &)
- Object::str() const -> renamed to Object::get_string() const
- Object::boolean(bool) -> renamed to Object::set_boolean(bool)
- Object::boolean() const -> renamed to Object::get_boolean() const
- Object::integer(intmax_t) -> renamed to Object::set_integer(intmax_t)
- Object::integer() const -> renamed to Object::get_integer() const
- Object::real(double) -> renamed to Object::set_real(double)
- Object::real() const -> renamed to Object::get_real() const

### Broken Progress methods

- Progress::Progress(bool) -> removed, see Progress::Progress(Border, bool)

### Broken Table methods

- Table::select(const Color &, const Span &) -> removed
- Table::select(const Color &, int, int, unsigned, unsigned) -> removed
- Table::mark(int, int, unsigned, unsigned, bool) -> removed
- Table::mark(const Span &, bool) -> removed
- Table::mark(const Color &, int, int, unsigned, unsigned, bool) -> renamed to Table::mark_back(const Color &, int, int, unsigned, unsigned, bool)
- Table::mark(const Color &, const Span &, bool) -> renamed to - Table::mark_back(const Color &, const Span &, bool)

### Broken Toggle methods

- Toggle::Toggle(const ustring &, int) -> signature changed, see Toggle::Toggle(int, const ustring &)
- Toggle::Toggle(const ustring &, const ustring &, int) -> signature changed, see Toggle::Toggle(const ustring &, int, const ustring &)

### Broken Toggle_action methods

- Toggle_action::connected() -> removed

### Broken Toplevel methods

- Toplevel::show_frame() -> doesn't work properly on X11, removed
- Toplevel::hide_frame() -> doesn't work properly on X11, removed
- Toplevel::frame_visible() const -> doesn't work properly on X11, removed
- Toplevel::enable_maximize() -> doesn't work properly on X11, removed
- Toplevel::disable_maximize() -> doesn't work properly on X11, removed
- Toplevel::maximize_enabled() const -> doesn't work properly on X11, removed
- Toplevel::enable_minimize() -> doesn't work properly on X11, removed
- Toplevel::disable_minimize() -> doesn't work properly on X11, removed
- Toplevel::minimize_enabled() const -> doesn't work properly on X11, removed

### Broken Conf::Item

- TOOLTIP_BACKGROUND removed, now HELP_BACKGROUND used

-------------------------------------------------------------------------------

### New Classes

- Bool_conf
- Int_conf
- Observer
- Opt_color

-------------------------------------------------------------------------------

## New Features

### New Action_base methods

- Action_base::accels() const

### New Action_menu_item methods

- Action_menu_item::Action_menu_item(const ustring &, std::string_view, Align)
- Action_menu_item::Action_menu_item(Align)
- Action_menu_item::select(Action &)

### New Bin methods

- Bin::widget()
- Bin::widget() const

### New Buffer methods

- Buffer::operator<<(std::ostream &)

### New Buffer_citer methods

- operator+(const Buffer_citer &, int);
- operator-(const Buffer_citer &, int);
- operator-(const Buffer_citer &, const Buffer_citer &);

### New Button_base methods

- Button_base::set_border_top_left_radius(unsigned)
- Button_base::set_border_top_right_radius(unsigned)
- Button_base::set_border_bottom_right_radius(unsigned)
- Button_base::set_border_bottom_left_radius(unsigned)
- Button_base::set_border_radius(unsigned)
- Button_base::set_border_radius(unsigned, unsigned, unsigned, unsigned)
- Button_base::border_top_left_radius()
- Button_base::border_top_right_radius()
- Button_base::border_bottom_right_radius()
- Button_base::border_bottom_left_radius()
- Button_base::unset_border_top_left_radius()
- Button_base::unset_border_top_right_radius()
- Button_base::unset_border_bottom_right_radius()
- Button_base::unset_border_bottom_left_radius()
- Button_base::unset_border_radius()

### New Button methods

- Button::Button(unsigned)
- Button::Button(unsigned, unsigned, unsigned, unsigned)
- Button::Button(const ustring &, unsigned)
- Button::Button(const ustring &, unsigned, unsigned, unsigned, unsigned)
- Button::Button(Widget &, unsigned)
- Button::Button(Widget &, unsigned, unsigned, unsigned, unsigned)
- Button::Button(Widget &, const ustring &, unsigned)
- Button::Button(Widget &, const ustring &, unsigned, unsigned, unsigned, unsigned)
- Button::Button(int, const ustring &, unsigned)
- Button::Button(int, const ustring &, unsigned, unsigned, unsigned, unsigned)
- Button::Button(const ustring &, int, const ustring &, unsigned)
- Button::Button(const ustring &, int, const ustring &, unsigned, unsigned, unsigned, unsigned)
- Button::Button(Action &, unsigned, Action::Flags)
- Button::Button(Action &, unsigned, unsigned, unsigned, unsigned, Action::Flags)
- Button::Button(Action &, int, unsigned, Action::Flags)
- Button::Button(Action &, int, unsigned, unsigned, unsigned, unsigned, Action::Flags)
- Button::Button(Master_action &, Action::Flags)
- Button::Button(Master_action &, unsigned, Action::Flags)
- Button::Button(Master_action &, unsigned, unsigned, unsigned, unsigned, Action::Flags)
- Button::Button(Master_action &, int, Action::Flags)
- Button::Button(Master_action &, int, unsigned, Action::Flags)
- Button::Button(Master_action &, int, unsigned, unsigned, unsigned, unsigned, Action::Flags items)
- Button::set_action(Action &, Action::Flags)
- Button::set_action(Action &, int, Action::Flags)
- Button::select(Action &)

### New Color methods

- Color::validate()

### New Conf methods

- Conf::push(std::string_view name, const ustring &)
- Conf::push(Item ename, const ustring &)
- Conf::boolean(std::string_view)
- Conf::boolean(std::string_view) const
- Conf::boolean(Item)
- Conf::boolean(Item) const
- Conf::integer(std::string_view)
- Conf::integer(std::string_view) const
- Conf::integer(Item)
- Conf::integer(Item) const
- Conf::signal_set()

### New Contour methods

- Contour::Contour(const Vector &, const Vector &)
- Contour::Contour(const Vector &, const Vector &, const Vector &)
- Contour::Contour(const Vector &, const Vector &, const Vector &, const Vector &)
- Contour::Contour(const Contour &, const Matrix &)
- Contour::arc()
- Contour::merge()
- Contour::rectangle()

### New Display methods

- Display::lookup()
- Display::lookup() const

### New Font methods

- Font::height()
- Font::iascent()
- Font::idescent()

### New Frame methods

- Frame::Frame(Border, unsigned, unsigned, unsigned, unsigned, unsigned)
- Frame::Frame(Border, const Color &, unsigned, unsigned, unsigned, unsigned, unsigned)
- Frame::Frame(const ustring &, Border, unsigned, unsigned, unsigned, unsigned, unsigned)
- Frame::Frame(const ustring &, Border, const Color &, unsigned, unsigned, unsigned, unsigned, unsigned)
- Frame::Frame(const ustring &, Align, Border, unsigned, unsigned, unsigned, unsigned, unsigned)
- Frame::Frame(const ustring &, Align, Border, const Color &, unsigned, unsigned, unsigned, unsigned, unsigned)
- Frame::set_border(unsigned, Border, unsigned, unsigned, unsigned, unsigned)
- Frame::set_border_radius(unsigned, unsigned, unsigned, unsigned)
- Frame::set_border_top_left_radius()
- Frame::set_border_top_right_radius()
- Frame::set_border_bottom_left_radius()
- Frame::set_border_bottom_right_radius()
- Frame::widget()
- Frame::widget() const

### New Key_file methods

- Key_file::create_from_file()
- Key_file::signal_modified()

### New List_base methods

- List_base::action_select_all()

### New Loop methods

- Loop::count()
- Loop::list()

### New Master_action methods

- Master_action::accels() const

### New Matrix methods

- Matrix::angle()
- Matrix::translated()

### New Menu methods

- Menu::empty()

### New Menubox methods

- Menubox::popup()
- Menubox::popup(Toplevel &)
- Menubox::popup(Toplevel &, const Point &)

### New Notebook methods

- Notebook::show_page(const Widget &, bool)
- Notebook::append_widget(Widget &, bool)
- Notebook::prepend_widget(Widget &, bool)
- Notebook::append_tab(Widget &, bool)
- Notebook::prepend_tab(Widget &, bool)
- Notebook::remove(Widget &)

### New Pixmap methods

- Pixmap::cptr()

### New Progress methods

- Progress::Progress(Border, bool)

### New Size methods

- Size::apply_aspect_ratio()

### New Theme methods

- Theme::conf()

### New Toggle methods

- Toggle::Toggle(unsigned)
- Toggle::Toggle(unsigned, unsigned, unsigned, unsigned)
- Toggle::Toggle(const ustring &, unsigned)
- Toggle::Toggle(const ustring &, unsigned, unsigned, unsigned, unsigned)
- Toggle::Toggle(Widget &, unsigned)
- Toggle::Toggle(Widget &, unsigned, unsigned, unsigned, unsigned)
- Toggle::Toggle(Widget &, const ustring &, unsigned)
- Toggle::Toggle(Widget &, const ustring &, unsigned, unsigned, unsigned, unsigned)
- Toggle::Toggle(int, const ustring &, unsigned)
- Toggle::Toggle(int, const ustring &, unsigned, unsigned, unsigned, unsigned)
- Toggle::Toggle(const ustring &, int, const ustring &, unsigned)
- Toggle::Toggle(const ustring &, int, const ustring &, unsigned, unsigned, unsigned, unsigned)
- Toggle::Toggle(Toggle_action &, unsigned, Action::Flags)
- Toggle::Toggle(Toggle_action &, unsigned, unsigned, unsigned, unsigned, Action::Flags)
- Toggle::Toggle(Toggle_action &, int, unsigned, Action::Flags)
- Toggle::Toggle(Toggle_action &, int, unsigned, unsigned, unsigned, unsigned, Action::Flags)
- Toggle::Toggle(Master_action &, Action::Flags)
- Toggle::Toggle(Master_action &, unsigned, Action::Flags)
- Toggle::Toggle(Master_action &, unsigned, unsigned, unsigned, unsigned, Action::Flags)
- Toggle::Toggle(Master_action &, int, Action::Flags)
- Toggle::Toggle(Master_action &, int, unsigned, Action::Flags)
- Toggle::Toggle(Master_action &, int, unsigned, unsigned, unsigned, unsigned, Action::Flags items)
- Toggle::set_action(Toggle_action &, Action::Flags)
- Toggle::set_action(Toggle_action &, int, Action::Flags)
- Toggle::select(Toggle_action &)

### New Toggle_menu_item methods

- Toggle_menu_item::Toggle_menu_item(const ustring &, std::string_view)
- Toggle_menu_item::Toggle_menu_item(Master_action &)
- Toggle_menu_item::Toggle_menu_item(const ustring &, std::string_view, Align)
- Toggle_menu_item::Toggle_menu_item(Master_action &, Align)
- Toggle_menu_item::Toggle_menu_item(const ustring &, Check::Style, Border)
- Toggle_menu_item::Toggle_menu_item(Master_action &, Check::Style, Border)
- Toggle_menu_item::Toggle_menu_item(const ustring &, Align, Check::Style, Border)
- Toggle_menu_item::Toggle_menu_item(Master_action &, Align, Check::Style, Border)
- Toggle_menu_item::get()
- Toggle_menu_item::select(Toggle_action &)

### New ustring methods

- ustring::bits()

### New Widget methods

- Widget::has_aspect_ratio_hint()
- Widget::set_string(const ustring &)
- Widget::get_string() const
- Widget::set_boolean(bool)
- Widget::get_boolean() const
- Widget::set_integer(intmax_t)
- Widget::get_integer() const
- Widget::set_real(double)
- Widget::get_real() const

### New Window methods

- Window::move(const Point &, const Size &)

### New Path Utilities

- path_user_desktop_dir()
- path_user_documents_dir()
- path_user_downloads_dir()
- path_user_music_dir()
- path_user_pictures_dir()
- path_user_videos_dir()
- path_user_templates_dir()

### New System Utilities

- path_build(const ustring &, const std::vector< ustring> &, char32_t)
- str_envsubst(const ustring &)
- metric()

### New String Utilities

- str_boolean()
- str_title()
- str_unicode()
- str_trimboth(const ustring &)
- str_trimboth(const ustring &, const ustring &)
- str_trimboth(const std::u32string  &)
- str_trimboth(const std::u32string  &, const std::u32string &)
- str_bytes(uintmax_t, const std::locale &, bool)

-------------------------------------------------------------------------------

**/

} // namespace tau

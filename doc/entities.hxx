// ----------------------------------------------------------------------------
// SPDX-License-Identifier: BSD-2-Clause
// Copyright © 2014-2024 Konstantin Shmelkov <mrcashe@gmail.com>.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
// THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
// PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
// CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
// EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
// PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
// OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
// WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
// OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
// EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// ----------------------------------------------------------------------------

namespace tau {

/**

@namespace tau TAU library namespace
All tau symbols are placed in tau namespace.

@defgroup enum_group Enumerations
Enumerations provided by library

@addtogroup fileman_modes_group Fileman Operation Modes
@ingroup enum_group
Enumeration defining file manager operation modes.

@addtogroup icon_size_group Icon Sizes
@ingroup enum_group
Enumeration defining Icon size indice.

@addtogroup sep_style_group Separator Styles
@ingroup enum_group
Enumeration defining Separator styles.

@addtogroup action_flags_group Action Construction Flags
@ingroup enum_group
Enumeration defining Action construction flags.

@addtogroup file_flags_group File Flags
@ingroup enum_group
Enumeration defining File flags.

@addtogroup check_style_group Check Styles
@ingroup enum_group
Enumeration defining Check styles.

@addtogroup conf_items_group Styles used by Conf
@ingroup enum_group
Enumeration defining configuration styles.

@addtogroup spinner_style_group Spinner Styles
@ingroup enum_group
Enumeration defining Spinner styles.

@defgroup signal_group Signals and Slots
The signalling system based on libsigc++-2.0 code.
libsigc++ originated as part of the [**gtkmm**](https://gtkmm.org) project in 1997 and later
was rewritten to be a standalone library.

Each signal has a particular function profile which designates the number
of arguments and argument type associated with the callback.
Functions and methods are then wrapped using template calls to produce
function objects (functors) which can be bound to a signal.
Each signal can be connected to multiple functors thus creating an
observer pattern through which a message can be distributed to multiple
anonymous listener objects. Reference counting based object lifespan tracking
was used to disconnect the functors from signals as objects are deleted.

Unlike [**boost::signals2**](https://www.boost.org/doc/libs/1_83_0/doc/html/signals2.html),
this implementation is not thread safe.

From the programmer point of view, the most interesting classes are:
- slot< R(Args...)>
- signal< R(Args...)>
- signal_all
- tau::connection
- tau::trackable

#### Slot

Slot object consists of a functor and an opaque slot implementation object.
To make a slot, use one of the overloaded tempalte helper function.
- fun(R (*fn)(Args...))
- fun(const std::function< R(Args...)> & fn)
- fun(Obj * obj, R (Class::*fun)(Args...))
- fun(std::shared_ptr< Obj> obj_ptr, R (Class::*fun)(Args...))
- fun(Obj & obj, R (Class::*fun)(Args...))
- fun(Obj * obj, R (Class::*fun)(Args...) const)
- fun(std::shared_ptr< Obj> obj_ptr, R (Class::*fun)(Args...) const)
- fun(Obj & obj, R (Class::*fun)(Args...) const)
- fun(Obj * obj, R (Class::*fun)(Args...) volatile)
- fun(std::shared_ptr< Obj> obj_ptr, R (Class::*fun)(Args...) volatile)
- fun(Obj & obj, R (Class::*fun)(Args...) volatile)
- fun(Obj * obj, R (Class::*fun)(Args...) const volatile)
- fun(std::shared_ptr< Obj> obj_ptr, R (Class::*fun)(Args...) const volatile)
- fun(Obj & obj, R (Class::*fun)(Args...) const volatile)
- fun(signal< R(Args...)> & sig)
- fun(signal< R(Args...)> * sig)
- fun(signal_all< Args...> & sig)
- fun(signal_all< Args...> * sig)
- bind_back(const functor_type & functor, Bound... bounds)
- bind_front(const functor_type & functor, Bound... bounds)
- bind_void(const functor_type & functor)
- bind_return(const functor_type & functor, R result)

#### Signals

Signal is a list of slots. You may add slots at any time or
you can remove slots by using saved connection object.
This implementation of signals and slots does not provide signal::disconnect() or signal::clear() facilities.

The %signal derives tau::trackable functionality, so when signal going out
of scope, all connections to it properly disconnected.
You can also connect two signals one after one.

##### Three kinds of signal emission schemes provided:

- @ref emission_each
- @ref emission_any
- @ref emission_all

##### Emission Safety

This implementation aware of signal's data change while emission. The programmer can
freely add, remove slots or even delete signal itself from inside of the slot function.
This is achieved by bringing slot function pointers onto the stack while operator() call.
This solution slightly reduces performance, but at the same time dramatically reduces
the number of logical errors in the source code.

@defgroup object_group Objects
Basic object (entity) interfaces

@defgroup throw_group Exceptions
Exceptions provided and used by the library

@defgroup sys_group System
System classes and functions

@addtogroup time_group Time & Date
@ingroup sys_group
Time management classes and functions

@addtogroup file_group Files
@ingroup sys_group
File management classes and functions

@addtogroup path_group Paths
@ingroup sys_group
Path management functions

@addtogroup uri_group URI
@ingroup sys_group
URI (Uniform Resource Identifier) management functions

@defgroup string_group Strings
Unicode string and character managing classes and functions

@defgroup text_group Text
Text processing classes and functions

@defgroup font_group Fonts
Font managing classes and functions

@defgroup input_group Keyboard & Mouse
Keyboard & Mouse management classes, functions, constants
See @ref kbd_sect for more information

@defgroup action_group Actions
Action related stuff

@defgroup geometry_group Geometry
Geometrical classes

@defgroup paint_group Painting and Imaging
Classes used for painting and image drawing

@defgroup i18n_group Localization
Classes and functions used for localization (l10n) and internationalization (i18n)

@defgroup gettext_group GNU gettext Support
Functions for interface to GNU gettext runtime library (libintl)

The library has built-in support for the localization system [GNU gettext](https://www.gnu.org/software/gettext/).
The user is allowed to connect new localization domains using the gettext_open() function. Only UTF-8 encoding allowed.
To use these functions, you must include the gettext.hh header file explicitly, since it is not listed in the global tau.hh header.

~~~~
#include <tau/gettext.hh>
~~~~

For message translation there are wrappers around original @a libintl runtime functions:

- gettext(const ustring &)
- dgettext(std::string_view, const ustring &)
- dcgettext(std::string_view, const ustring &, int)
- ngettext(const ustring &, const ustring &, unsigned long)
- dngettext(std::string_view, const ustring &, const ustring &, unsigned long)
- dcngettext(std::string_view, const ustring &, const ustring &, unsigned long, int)

All above functions placed in @b tau:: namespace and returned tau::ustring as a result. These functions differ from the original ones
in the type of parameters and result, but they have the usual names and the same order of arguments.
When operating system lacks of libintl, the dummy functions used for translations, so the user does not have to worry about
the presence or absence of libintl within the system. Also, each of the above functions has @b using statement in the tau/gettext.hh header:

- using tau::gettext
- using tau::dgettext
- using tau::dcgettext
- using tau::ngettext
- using tau::dngettext
- using tau::dcngettext

Thus, the user does not need to specify a namespace when using these functions.

Of course, the user can independently bind the desired domain. But the library itself binds the "tau" domain and tries to bind the application
domain when calling the Locale::set() function. The application domain name is determined by the value returned by the program_name() function,
which in turn can receive a value from the following sources:

1. By executable module name.
2. Based on the value set using the program_name(const ustring &) function.

If there is a need to bind additional domains, then this must be done after calling the Locale::set() function. But, if there is no need to bind
domains other than the "tau" domain and the name of your application is such that it is correctly returned by the program_name() function
and the generated file `*.mo` matches the name with this name, then in a “manual” call to the gettext_open() not necessary.

As for generating *.mo files, the current implementation of the library does not impose any special requirements;
you can use various tools and techniques. The file encoding can be any that is supported by the [GNU libiconv library](https://www.gnu.org/software/libiconv/).
Since the function names match those from the `libintl` library, the `xgettext` program does its job just fine. There is no need to specify additional keywords,
except in the case of statically allocated strings. For such data, it has become common practice to use the gettext_noop() macro. %Header file gettext.hh provides
that macro too:

~~~~
#define gettext_noop(msgid) (msgid)
~~~~

Here is an example Makefile to produce valid message object files:
@include po.mk

#### Link message object files into the Windows PE file

@warning The below information relates only to current release and very likely to change in the next release!

It is possible to link *.mo files into the *.exe file. To do this, the resource name must be formatted as follows:
@verbatim
mo-LL-domain
@endverbatim
where `LL` is 2-letter language code, such as "en". The type of resource must be @b RCDATA.

Below is an example of shell script for cross-compile on Unix:
@include po-windres.mk

@defgroup base64_group Base64
Base64 encoding/decoding classes

Base64 is an encoding that allows a sequence of arbitrary bytes to be
encoded as a sequence of printable ASCII characters. For the definition
of Base64, see [RFC 1421](http://www.ietf.org/rfc/rfc1421.txt)
or [RFC 2045](http://www.ietf.org/rfc/rfc2045.txt).

Base64 is most commonly used as a MIME transfer encoding for email.

@defgroup conf_group Configuration
Configuration and configuration items

@defgroup widget_stack Widget Stack
All available widgets

@addtogroup widget_group Widgets
@ingroup widget_stack
General purpose widgets

@addtogroup container_group Containers
@ingroup widget_stack
Containers are widgets that capable to have one or more children widgets

@addtogroup generic_container_group Generic Containers
@ingroup container_group
Generic containers provide its own child allocation policy

@addtogroup flat_container_group Flat Containers
@ingroup generic_container_group
Flat containers allocate its children on the same level of visibilty and does not allow children overlap

@addtogroup layered_container_group Layered Containers
@ingroup generic_container_group
Layered containers allocate its children on the different levels of visibilty and allow children overlap

@addtogroup compound_container_group Compound Containers
@ingroup container_group
Compound containers consist of @ref generic_container_group and use its allocation policy

@addtogroup menu_group Menus
@ingroup widget_stack
Menus and menu items

@addtogroup window_group Windows
@ingroup widget_stack
Classes derived from Window

**/

} // namespace tau

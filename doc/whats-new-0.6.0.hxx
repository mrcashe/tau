// ----------------------------------------------------------------------------
// SPDX-License-Identifier: BSD-2-Clause
// Copyright © 2014-2024 Konstantin Shmelkov <mrcashe@gmail.com>.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
// THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
// PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
// CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
// EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
// PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
// OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
// WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
// OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
// EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// ----------------------------------------------------------------------------

namespace tau {

/**

@page whats_new_060 Whats New in 0.6.0

Whats new in older releases:

- @subpage whats_new_050
- @subpage whats_new_040

## Release Notes

- API highly incompatible with previous release, source editing needed.
- Significant performance improvement.
- Build system now support up to four targets while building using MXE.
- Cross compile for FreeBSD on Linux host.
- Asynchronous API started.
- New UTF-8 and Unicode management functions.

## API Breaks

-------------------------------------------------------------------------------

### Fileinfo class

- Has been renamed to File

### Table::Span struct

- Moved out from Table class to the **tau** namespace.

### Broken Accel methods

- Accel::set_enable() -> renamed to Accel::par_enable(bool yes)

### Broken Action_base methods

- Action_base::set_enable() -> renamed to Action_base::par_enable(bool yes)
- Action_base::set_visible() -> renamed to Action_base::par_show(bool yes)

### Broken Box methods

- Box::focus_next_action() -> removed, see Container::action_focus_next()
- Box::focus_previous_action() -> removed, see Container::action_focus_previous()

### Broken Buffer_citer methods

- Buffer_citer::move_forward_line() -> renamed to Buffer_citer::forward_line()
- Buffer_citer::move_backward_line() -> renamed to Buffer_citer::backward_line()
- Buffer_citer::move_word_left() -> renamed to Buffer_citer::backward_word()
- Buffer_citer::move_word_right() -> renamed to Buffer_citer::forward_word()
- Buffer_citer::move_to_col() -> renamed to Buffer_citer::move_to(std::size_t col)

### Broken Button methods

- Button::signal_click() -> removed, see Button::connect()

### Broken Cycle_text methods

- Cycle_text::set_text_align() -> renamed to Cycle_text::text_align(Align)
- Cycle_text::remove_text() -> renamed to Cycle_text::remove(const ustring &)
- Cycle_text::select_text() -> renamed to Cycle_text::select(const ustring &)
- Cycle_text::selection() -> renamed to Cycle_text::str()
- Cycle_text::wselection() -> renamed to Cycle_text::wstr()
- Cycle_text::signal_selection_changed() -> removed, see Cycle_text::signal_select_text()

### Broken Colorsel methods

- Colorsel::apply_action() -> renamed to Colorsel::action_apply()
- Colorsel::cancel_action() -> renamed to Colorsel::action_cancel()

### Broken Container methods

- Container::children() -> return value changed

### Broken Counter methods

- Counter::cancel_action() -> renamed to Counter::action_cancel()

### Broken Cycle_text methods

- Cycle_text::add_text() -> renamed to Cycle_text::add()

### Broken Edit methods

- Edit::cut_action() -> renamed to Edit::action_cut()
- Edit::paste_action() -> renamed to Edit::action_paste()
- Edit::enter_action() -> renamed to Edit::action_enter()
- Edit::del_action() -> renamed to Edit::action_delete()
- Edit::backspace_action() -> renamed to Edit::action_backspace()
- Edit::undo_action() -> renamed to Edit::action_undo()
- Edit::redo_action() -> renamed to Edit::action_redo()
- Edit::tab_action() -> renamed to Edit::action_tab()
- Edit::insert_action() -> renamed to Edit::action_insert()

### Broken Entry methods

- Entry::cancel_action() -> renamed to Entry::action_cancel()
- Entry::signal_changed() -> signal signature changed
- Entry::set_text_align() -> renamed to Entry::text_align(Align)
- Entry::set_wrap_mode() -> renamed to Entry::wrap(Wrap)
- Entry::wrap_mode() -> renamed to Entry::wrap(void)

### Broken Fileman methods

- Fileman::apply_action() -> renamed to Fileman::action_apply()
- Fileman::cancel_action() -> renamed to Fileman::action_cancel()
- Fileman::navigator_ptr() -> renamed to Fileman::navigator() and return value changed

### Broken Fontsel methods

- Fontsel::focus_next_action() -> removed
- Fontsel::focus_previous_action() -> removed
- Fontsel::apply_action() -> renamed to Fontsel::action_apply()
- Fontsel::cancel_action() -> renamed to Fontsel::action_cancel()

### Broken Frame methods

- Frame::border_radius() -> removed, see new Frame::border_*_radius() methods
- Frame::set_label_position() -> renamed to Frame::move_label()
- Frame::label_position() -> renamed to Frame::where_label()

### Broken Key_file methods

- Key_file::get_integer(Key_section & sect, const ustring & key_name, intmax_t fallback) -> signature changed
- Key_file::get_integer(const ustring & key_name, intmax_t fallback) -> signature changed
- Key_file::get_integers(Key_section & sect, const ustring & key_name, std::size_t min_size, intmax_t fallback) -> signature changed
- Key_file::get_integers(const ustring & key_name, std::size_t min_size, intmax_t fallback) -> signature changed
- Key_file::set_integer(Key_section & sect, const ustring & key_name, intmax_t value) -> signature changed
- Key_file::set_integer(const ustring & key_name, intmax_t value) -> signature changed
- Key_file::set_integers(Key_section & sect, const ustring & key_name, const std::vector<intmax_t> & vec) -> signature changed
- Key_file::set_integers(const ustring & key_name, const std::vector<intmax_t> & vec) -> signature changed

### Broken Label methods

- Label::get_row_bounds() -> renamed to Label::row_bounds() and signature changed
- Label::set_text_align() -> renamed to Label::text_align(Align, Align)
- Label::get_text_align() -> renamed to Label::text_align(void) and signature changed
- Label::set_wrap_mode() -> renamed to Label::wrap(Wrap)
- Label::wrap_mode() -> renamed to Label::wrap()

### Broken Language methods

- Language::shortest() -> renamed to Language::code()

### Broken List_base methods

- List_base::enter_action() -> renamed to List_base::action_activate()
- List_base::cancel_action() -> renamed to List_base::action_cancel()
- List_base::previous_action() -> renamed to List_base::action_previous()
- List_base::previous_page_action() -> renamed to List_base::action_previous_page()
- List_base::next_action() -> renamed to List_base::action_next()
- List_base::next_page_action() -> renamed to List_base::action_next_page()
- List_base::home_action() -> renamed to List_base::action_home()
- List_base::end_action() -> renamed to List_base::action_end()
- List_base::select_previous_action() -> renamed to List_base::action_select_previous()
- List_base::select_previous_page_action() -> renamed to List_base::action_select_previous_page()
- List_base::select_next_action() -> renamed to List_base::action_select_next()
- List_base::select_next_page_action() -> renamed to List_base::action_select_next_page()
- List_base::select_home_action() -> renamed to List_base::action_select_home()
- List_base::select_end_action() -> renamed to List_base::action_select_end()
- List_base::span() -> signature and return value changed
- List_base::get_column_margin() -> renamed to List_base::column_margin() and signature+return value changed
- List_base::get_row_margin() -> renamed to List_base::row_margin() and signature+return value changed
- List_base::show_headers() -> removed, see new Header class
- List_base::hide_headers() -> removed, see new Header class
- List_base::headers_visible() const -> removed, see new Header class
- List_base::show_header(int column) -> removed, see new Header class
- List_base::show_header(int column, const ustring & title, Align align) -> removed, see new Header class
- List_base::show_header(int column, Widget_ptr title) -> removed, see new Header class
- List_base::hide_header(int column) -> removed, see new Header class
- List_base::show_sort_marker(int column, bool descend) -> removed, see new Header class
- List_base::hide_sort_marker() -> removed, see new Header class
- List_base::header_bounds() const  -> removed, see new Header class
- List_base::signal_header_click() -> removed, see new Header class
- List_base::signal_header_width_changed() -> removed, see new Header class
- List_base::remove(Widget & w) -> removed
- List_base::selected_row() -> renamed to List_base::current()

### Broken List_text methods

- List_text::set_text_align() -> renamed to List_text::text_align(Align)
- List_text::set_wrap_mode() -> renamed to List_text::wrap(Wrap)
- List_text::wrap_mode() -> renamed to List_text::wrap(void)
- List_text::signal_text_cancel() -> renamed to List_text::signal_cancel_edit()
- List_text::signal_text_validate() -> renamed to List_text::signal_validate() and signal signature changed
- List_text::selection() -> renamed to List_text::str()
- List_text::wselection() -> renamed to List_text::wstr()
- List_text::at() -> renamed to List_text::str(int)

### Broken Menubar methods

- Menubar::activate() -> returned value changed

### Broken Progress methods

- Progress::set_text_align() -> renamed to Progress::text_align(Align)

### Broken Roller methods

- Roller::roll_to(int) -> renamed to Roller::Pan(int)
- Roller::roll_to(Widget &) -> renamed to Roller::Pan(Widget &)

### Broken Scroller methods

- Scroller::previous_page_action() -> renamed to Scroller::action_previous_page()
- Scroller::next_page_action() -> renamed to Scroller::action_next_page()
- Scroller::home_action() -> renamed to Scroller::action_home()
- Scroller::end_action() -> renamed to Scroller::action_end()
- Scroller::pan_left_action() -> renamed to Scroller::action_pan_left()
- Scroller::pan_right_action() -> renamed to Scroller::action_pan_right()
- Scroller::pan_up_action() -> renamed to Scroller::action_pan_up()
- Scroller::pan_down_action() -> renamed to Scroller::action_pan_down()
- Scroller::logical_size() -> renamed to Scroller::pan_size()
- Scroller::signal_logical_size_changed() -> renamed to Scroller::signal_pan_size_changed()
- Scroller::pan_to() -> renamed to Scroller::pan(const Point &)
- Scroller::pan_to_x() -> renamed to Scroller::pan_x()
- Scroller::pan_to_y() -> renamed to Scroller::pan_y()

### Broken Table methods

- Table::get_column_span() -> renamed to Table::column_span() and signature+return value changed
- Table::get_row_span() -> renamed to Table::row_span() and signature+return value changed
- Table::get_column_bouns() -> renamed to Table::column_bounds() and signature+return value changed
- Table::get_row_bounds() -> renamed to Table::row_bounds() and signature+return value changed
- Table::get_column_margin() -> renamed to Table::column_margin() and signature+return value changed
- Table::get_row_margin() -> renamed to Table::row_margin() and signature+return value changed
- Table::get_columns_margin() -> renamed to Table::columns_margin() and signature+return value changed
- Table::get_rows_margin() -> renamed to Table::rows_margin() and signature+return value changed
- Table::columns_align() -> removed, see Table::align(void)
- Table::rows_align() -> removed, see Table::align(void)
- Table::get_align() -> renamed to Table::align(const Widget &) and signature changed
- Table::unmark_all() -> renamed to Table::unmark()
- Table::unmark(int, int, int, int) -> removed, see Table::unmark(const Span &) and Table::unmark(int, int, unsigned, unsigned)

### Broken Text methods

- Text::select_next_char_action() -> renamed to Text::action_select_next()
- Text::select_previous_line_action() -> renamed to Text::action_select_previous_line()
- Text::select_next_line_action() -> renamed to Text::action_select_next_line()
- Text::select_previous_word_action() -> renamed to Text::action_select_previous_word()
- Text::select_next_word_action() -> renamed to Text::action_select_next_word()
- Text::select_to_eol_action() -> renamed to Text::action_select_to_eol()
- Text::select_to_sof_action() -> renamed to Text::action_select_to_sof()
- Text::select_to_eof_action() -> renamed to Text::action_select_to_eof()
- Text::select_all_action() -> renamed to Text::action_select_all()
- Text::cancel_action() -> renamed to Text::action_cancel()
- Text::previous_page_action() -> renamed to Text::action_previous_page()
- Text::next_page_action() -> renamed to Text::action_next_page()
- Text::select_previous_page_action() -> renamed to Text::action_select_previous_page()
- Text::select_next_page_action() -> renamed to Text::action_select_next_page()
- Text::move_home_action() -> renamed to Text::action_home()
- Text::select_home_action() -> renamed to Text::action_select_home()
- Text::select_previous_char_action() -> renamed to Text::action_select_previous()
- Text::next_char_action() -> renamed to Text::action_next()
- Text::previous_line_action() -> renamed to Text::action_previous_line()
- Text::next_line_action() -> renamed to Text::action_next_line()
- Text::previous_word_action() -> renamed to Text::action_previous_word()
- Text::previous_char_action() -> renamed to Text::action_previous()
- Text::next_word_action() -> renamed to Text::action_next_word()
- Text::move_to_eol_action() -> renamed to Text::action_end()
- Text::move_to_sof_action() -> renamed to Text::action_sof()
- Text::move_to_eof_action() -> renamed to Text::action_eof()
- Text::copy_action() -> renamed to Text::action_copy()
- Text::assign(Buffer buf) -> removed, see Buffer::assign(const Buffer buf)

### Broken Toggle methods

- Toggle::signal_toggle() -> removed, see Toggle::connect()

### Broken Widget methods

- Widget::margin_hint() -> renamed to Widget::margin_size()
- Widget::margin_left_hint() const -> removed, see new Widget::margin_hint() const
- Widget::margin_right_hint() const -> removed, see new Widget::margin_hint() const
- Widget::margin_top_hint() const -> removed, see new Widget::margin_hint() const
- Widget::margin_bottom_hint() const -> removed, see new Widget::margin_hint() const
- Widget::margin_size() -> removed, see Widget::margin_hint()
- Widget::margin_origin() -> removed, see Widget::margin_hint()
- Widget::viewable() const -> removed, see Widget::viewable_area() const
- Widget::visible_area() const -> removed, use Widget::scroll_position() & Widget::size()
- Widget::has_modal() const -> renamed to Widget::grabs_modal() and behaviour changed
- Widget::scroll_position() -> renamed to Widget::offset()
- Widget::scroll_to() -> renamed to Widget::offset() and return value changed
- Widget::scroll_to_x() -> removed
- Widget::scroll_to_y() -> removed
- Widget::signal_scroll_changed() -> renamed to Widget::signal_offset_changed()
- Widget::set_enable() -> renamed to Widget::par_enable(bool yes)
- Widget::set_visible() -> renamed to Widget::par_show(bool yes)
- Widget::to_parent(void) -> removed, see Widget::to_parent(const Widget &, const Point &), Widget::to_parent(Object_cptr, const Point &)
- Widget::to_root() -> removed, see Widget::to_parent(const Widget &, const Point &), Widget::to_parent(Object_cptr, const Point &)
- Widget::to_window() -> removed, see Widget::to_parent(const Widget &, const Point &), Widget::to_parent(Object_cptr, const Point &)
- Widget::signal_focusable_changed() -> removed
- Widget::signal_requisition_changed() -> removed, functionality moved to Widget::signal_hints_changed()
- Widget::signal_visible() -> removed, functionality moved to Widget::signal_show()
- Widget::signal_invisible() -> removed, functionality moved to Widget::signal_hide()
- Widget::signal_sensitive() -> removed, functionality moved to Widget::signal_enable()
- Widget::signal_insensitive() -> removed, functionality moved to Widget::signal_disable()

### Broken File Utils

- path_which(const ustring & cmd) -> returned value changed
- path_mkdir() -> renamed to file_mkdir()
- path_glob() -> renamed to file_glob()
- path_list() -> renamed to file_list()
- path_find() -> renamed to file_find()

### Broken Path Utils

- path_build() -> added default parameter

### Broken String Utils

- str_blanks() -> removed, see Locale::blanks(), Locale::wblanks().
- str_newlines() -> removed, see Locale::newlines(), Locale::wnewlines().
- str_delimiters() -> removed, see Locale::delimiters(), Locale::wdelimiters().

## New Features

-------------------------------------------------------------------------------

### New Classes

- Brush_conf
- Date
- Header
- Label
- Margin

-------------------------------------------------------------------------------

### New Widget Stack methods

Added constructors from Object_ptr class and assignment operators from
Object_ptr for all Widget classes.

### New Action methods

- Action::connect_before()
- Action::Action(Master_action * master_action, slot<void()> slot_activate)

### New Brush methods

- Brush::Brush(const ustring &)
- Brush::color()
- Brush::darker()
- Brush::lighter()
- Brush::str()

### New Buffer methods

- Buffer::replace(Buffer_citer i, char32_t wc, std::size_t count)

### New Buffer_citer methods

- Buffer_citer::operator[](long long)

### New Button methods

- Button::click()
- Button::connect()

### New Check methods

- Check::allow_edit()
- Check::disallow_edit()
- Check::edit_allowed()

### New Conf methods

- Conf::brush()
- Conf::items()
- Conf::provides()
- Conf::redirected()
- Conf::redirection()

### New Container methods

- Container::focus_after()
- Container::focus_before()
- Container::unchain_focus()
- Container::action_focus_next()
- Container::action_focus_next() const
- Container::action_focus_previous()
- Container::action_focus_previous() const
- Container::focused_child()
- Container::focused_child() const
- Container::lookup()
- Container::name()
- Container::link()
- Container::manage()

### New Contour methods

- Contour::from_arc(const Vector & center, double radius, double angle1, double angle2, const Matrix & matrix, double tolerance)
- Contour::from_arc(double cx, double cy, double radius, double angle1, double angle2, const Matrix & matrix, double tolerance)

### New Counter methods

- Counter::action_activate()
- Counter::append(const ustring &, const tau::Color &, unsigned, unsigned)
- Counter::prepend(const ustring &, const tau::Color &, unsigned, unsigned)

### New Cursor methods

- Cursor::ptr()

### New Cycle methods

- Cycle::remove(int)
- Cycle::append(const ustring &, const tau::Color &, unsigned, unsigned)
- Cycle::prepend(const ustring &, const tau::Color &, unsigned, unsigned)

### New Cycle_base methods

- Cycle_base::action_cancel()
- Cycle_base::action_activate()
- Cycle_base::empty()
- Cycle_base::count()
- Cycle_base::fixed()
- Cycle_base::unfix()
- Cycle_base::signal_select_id()
- Cycle_base::signal_new_id()
- Cycle_base::signal_remove_id()
- Cycle_base::current()
- Cycle_base::count()

### New Cycle_text methods

- Cycle_text::Cycle_text(const std::vector< ustring> &, Border)
- Cycle_text::Cycle_text(const std::vector< ustring> &, Align, Border)
- Cycle_text::add(const ustring &, Align, const ustring &)
- Cycle_text::add(const std::vector< ustring> &, Align)
- Cycle_text::wrap()
- Cycle_text::wrap(Wrap)
- Cycle_text::remove(int)
- Cycle_text::select(int)
- Cycle_text::select_similar()
- Cycle_text::str(int)
- Cycle_text::wstr(int)
- Cycle_text::strings()
- Cycle_text::wstrings()
- Cycle_text::edit()
- Cycle_text::editor()
- Cycle_text::editor() const
- Cycle_text::signal_select_text()
- Cycle_text::signal_activate_text()
- Cycle_text::signal_remove_text()
- Cycle_text::signal_text_changed()
- Cycle_text::signal_activate_edit()
- Cycle_text::signal_cancel_edit()
- Cycle_text::signal_validate()
- Cycle_text::signal_approve()
- Cycle_text::append(const ustring &, const tau::Color &, unsigned, unsigned)
- Cycle_text::prepend(const ustring &, const tau::Color &, unsigned, unsigned)

### New Display methods

- Display::focus_endpoint()
- Display::loop()

### New Edit methods

- Edit::signal_changed()
- Edit::signal_changed() const

### New Entry methods

- Entry::action_activate()
- Entry::signal_approve()
- Entry::append(const ustring &, const tau::Color &, unsigned, unsigned)
- Entry::prepend(const ustring &, const tau::Color &, unsigned, unsigned)

### New Frame methods

- Frame::border_top_left_radius()
- Frame::border_top_right_radius()
- Frame::border_bottom_left_radius()
- Frame::border_bottom_right_radius()
- Frame::border_visible()
- Frame::signal_border_changed()
- Frame::set_label(const ustring & label, Side label_pos, Align align)
- Frame::set_label(Widget & widget, Side label_pos, Align align)

### New File methods

- File::mime()
- File::type_title()

### New Font methods

- Font::ptr()
- Font::master_glyph()
- Font::same(const ustring &, const ustring &)
- Font::make_bold()
- Font::make_italic()
- Font::label()

### New Font_conf methods

- Font_conf::make_bold()
- Font_conf::make_italic()
- Font_conf::char_width()

### New Glyph methods

- Glyph::ptr()

### New Header methods

- Header::Header(Separator::Style, bool)
- Header::Header(Separator::Style, List_base &, bool)
- Header::set_separator_style()
- Header::separator_style()
- Header::allow_resize()
- Header::disallow_resize()
- Header::resize_allowed()

### New Icon methods

- Icon::Icon(int)
- Icon::clear()
- Icon::signal_click()

### New Image methods

- Image::clear()

### New Key_file methods

- Key_file::Key_file(Buffer, char32_t, char32_t)
- Key_file::load(Buffer)

### New Label methods

- Label::iter(const Point &)

### New Language methods

- Language::blanks()
- Language::newlines()
- Language::delimiters()

### New List methods

- List::List(unsigned spacing)
- List::List(unsigned xspacing, unsigned yspacing)
- List_text::remove(Widget &)

### New List_base methods

- List_base::widgets()
- List_base::column_bounds()
- List_base::row_bounds()
- List_base::set_column_margin(int column, const Margin & margin)
- List_base::set_row_margin(int row, const Margin & margin)
- List_base::columns_margin()
- List_base::rows_margin()
- List_base::set_columns_margin(unsigned left, unsigned right)
- List_base::set_columns_margin(const Margin & margin)
- List_base::set_rows_margin(unsigned top, unsigned bottom)
- List_base::set_rows_margin(const Margin & margin)
- List_base::signal_column_bounds_changed()
- List_base::selection()
- List_base::align_column(int x, Align xalign)
- List_base::column_align(int x) const
- List_base::unalign_column(int x)
- List_base::align_row(int y, Align yalign)
- List_base::row_align(int y) const
- List_base::unalign_row(int y)
- List_base::fixed()
- List_base::unfix()

### New List_text methods

- List_text::List_text(unsigned, Align align)
- List_text::List_text(unsigned, unsigned, Align)
- List_text::List_text(const std::vector< ustring> &, Align align)
- List_text::List_text(const std::vector< ustring> &, unsigned, Align align)
- List_text::List_text(const std::vector< ustring> &, unsigned, unsigned, Align)
- List_text::select(int)
- List_text::select_similar()
- List_text::signal_approve()
- List_text::widget_at()
- List_text::widget_at() const
- List_text::wselection()
- List_text::signal_activate_edit()
- List_text::insert(int, Widget &, int, bool)
- List_text::insert(int, Widget &, int, Align, bool)
- List_text::wstr(int)
- List_text::strings()
- List_text::wstrings()
- List_text::remove(Widget &)
- List_text::editor()
- List_text::editor() const
- List_text::append(const ustring &, Align, bool)
- List_text::prepend(const ustring &, Align, bool)
- List_text::insert(const ustring &, int, Align, bool)
- List_text::insert_before(const ustring &, const ustring &, Align, bool)
- List_text::insert_after(const ustring &, const ustring &, Align, bool)
- List_text::append(const std::vector< ustring> &, bool)
- List_text::append(const std::vector< ustring> &, Align, bool)
- List_text::prepend(const std::vector< ustring> &, bool)
- List_text::prepend(const std::vector< ustring> &, Align, bool)
- List_text::insert(const std::vector< ustring> &, int, bool)
- List_text::insert(const std::vector< ustring> &, int, Align, bool)
- List_text::insert_before(const std::vector< ustring> &, const ustring &, bool)
- List_text::insert_before(const std::vector< ustring> &, const ustring &, Align, bool)
- List_text::insert_after(const std::vector< ustring> &, const ustring &, bool)
- List_text::insert_after(const std::vector< ustring> &, const ustring &, Align, bool)
- List_text::similar()
- List_text::like()

### New Locale methods

- Locale::blanks()
- Locale::wblanks()
- Locale::newlines()
- Locale::wnewlines()
- Locale::delimiters()
- Locale::wdelimiters()

### New Loop methods

- Loop::this_loop()
- Loop::operator bool() const

### New Painter methods

- Painter::offsets(const ustring &)
- Painter::offsets(const std::u32string &)

### New Pixmap methods

- Pixmap::ptr()

### New Roller methods

- Roller::gravity(Gravity)
- Roller::gravity(void)

### New Scroller methods

- Scroller::gravity(Gravity)
- Scroller::gravity(void)

### New Slider methods

- Slider::set_scroller()
- Slider::allow_autohide()
- Slider::disallow_autohide()
- Slider::autohide_allowed()

### New Submenu_item methods

- Submenu_item::set_menu()
- Submenu_item::menu()

### New Table methods

- Table::bounds(const Span &)
- Table::put(Widget & w, const Span & span, bool xsh, bool ysh)
- Table::put(Widget & w, int x, int y, Align xalign, Align yalign, unsigned xspan, unsigned yspan, bool xsh, bool ysh)
- Table::put(Widget & w, const Span & span, Align xalign, Align yalign, bool xsh, bool ysh)
- Table::respan(Widget & w, const Span &)
- Table::respan(Widget & w, const Span &, bool xsh, bool ysh)
- Table::set_column_margin(int column, const Margin &)
- Table::set_row_margin(int row, const Margin &)
- Table::set_columns_margin(const Margin &)
- Table::set_rows_margin(const Margin &)
- Table::widgets(int x, int y, unsigned xspan, unsigned yspan)
- Table::widgets(const Span &)
- Table::align()
- Table::mark(const Span &)
- Table::mark(const Color &, const Span &)
- Table::mark(const Color &, int, int, unsigned, unsigned)
- Table::mark_column(const Color &, int)
- Table::mark_row(const Color &, int)
- Table::unmark(int, int, unsigned, unsigned)
- Table::unmark(const Span &)
- Table::select(const Span &)
- Table::select(const Color &, const Span &)
- Table::select(const Color &, int, int, unsigned, unsigned)
- Table::select_column(const Color &, int)
- Table::select_row(const Color &, int)

### New Text methods

- Text::get_selection() const
- Text::wselection() const
- Text::select(std::size_t row1, std::size_t col1, std::size_t row2, std::size_t col2)
- Text::signal_buffer_changed()

### New Toggle methods

- Toggle::connect()

### New Toggle_action methods

- Toggle_action::Toggle_action(Master_action * master_action, slot<void(bool)> slot_toggle)

### New Toplevel methods

- Toplevel::title()

### New Twins methods

- Twins::insert()
- Twins::first()
- Twins::second()

### New Widget methods

- Widget::allocate_accel(char32_t, int, bool)
- Widget::allocate_accel(const ustring &, bool)
- Widget::allow_tooltip()
- Widget::connect_run()
- Widget::container()
- Widget::container() const
- Widget::disallow_tooltip()
- Widget::epath() const
- Widget::facade()
- Widget::hint_aspect_ratio(double ratio)
- Widget::hint_margin(const Margin & margin)
- Widget::margin_hint() const
- Widget::lower()
- Widget::level()
- Widget::logical_size()
- Widget::require_aspect_ratio(double ratio)
- Widget::required_aspect_ratio() const
- Widget::rise()
- Widget::selected()
- Widget::tooltip_allowed()
- Widget::toplevel()
- Widget::toplevel() const
- Widget::to_parent(const Container &, const Rect &)
- Widget::to_screen(const Rect &)
- Widget::window()
- Widget::window() const

-----------------------------------------

### New File, Path & URI Utils

- uri_scheme()
- uri_has_scheme()
- uri_notscheme()
- uri_is_file()
- path_which(const ustring & filename, const std::vector & paths)
- path_explode()
- path_implode()

-----------------------------------------

### New String Utils

- str_toupper(const std::u32string & str)
- str_tolower(const std::u32string & str)
- str_explode(const std::u32string & str)
- str_explode(const std::u32string & str, char32_t wc)
- str_explode(const std::u32string & str, const std::u32string & delimiters)
- str_is_numeric(const ustring & text, int base)
- str_is_numeric(const std::u32string & text, int base)
- str_replace(const ustring & str, char32_t match, char32_t replacement)
- str_replace(const ustring & str, char32_t match, const ustring & replacement)
- str_replace(const ustring & str, const ustring & match_any, char32_t replacement)
- str_replace(const ustring & str, const ustring & match_any, const ustring & replacement)
- str_replace(const std::u32string & str, char32_t match, char32_t replacement)
- str_replace(const std::u32string & str, char32_t match, const std::u32string & replacement)
- str_replace(const std::u32string & str, const std::u32string & match_any, char32_t replacement)
- str_replace(const std::u32string & str, const std::u32string & match_any, const std::u32string & replacement)
- str_escape()
- str_unescape()
- str_trimleft(const std::u32string & str)
- str_trimright(const std::u32string  & str)
- str_trim(const std::u32string & str)
- str_not_empty(const std::u32string &)
- str_like(const ustring &)
- str_like(const std::u32string &)

-----------------------------------------

### New System Utils

- program_name(const ustring & name) [overload]

**/

} // namespace tau

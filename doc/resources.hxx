// ----------------------------------------------------------------------------
// SPDX-License-Identifier: BSD-2-Clause
// Copyright © 2014-2024 Konstantin Shmelkov <mrcashe@gmail.com>.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
// THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
// PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
// CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
// EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
// PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
// OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
// WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
// OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
// EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// ----------------------------------------------------------------------------

// Doxygen source file.

namespace tau {

/**
@page Resources
Stuff that didn't find yet better location
@todo Documentation: Resource Overview

@section icon_sect Standard Icons
Standard icon names used by library.
Some of icons are compliant to XDG icon name standards (https://standards.freedesktop.org/icon-naming-spec/icon-naming-spec-latest.html).
Some are special, like "picto-*".
The presence of these icons is mandatory for normal library work.
The below information related to following methods:
- Theme::find_icon()
- Theme::get_icon()
- Theme::find_pixmap()
@todo Documentation: %Icon's locations

%Icon names provided by builtin scheme named "Tau":
<table>
<tr><th>%Icon name                  <th>Is Standard?       <th>%Icon sample
<tr><td>configure                   <td align="center">YES <td> @image html configure.ico
<tr><td>dialog-cancel               <td align="center">YES <td> @image html dialog-cancel.ico
<tr><td>dialog-information          <td align="center">YES <td> @image html dialog-information.ico
<tr><td>dialog-ok                   <td align="center">YES <td> @image html dialog-ok.ico
<tr><td>dialog-question             <td align="center">YES <td> @image html dialog-question.ico
<tr><td>dialog-warning              <td align="center">YES <td> @image html dialog-warning.ico
<tr><td>document-close              <td align="center">YES <td> @image html document-close.ico
<tr><td>document-new                <td align="center">YES <td> @image html document-new.ico
<tr><td>document-open-recent        <td align="center">YES <td> @image html document-open-recent.ico
<tr><td>document-open               <td align="center">YES <td> @image html document-open.ico
<tr><td>document-save-all           <td align="center">YES <td> @image html document-save-all.ico
<tr><td>document-save-as            <td align="center">YES <td> @image html document-save-as.ico
<tr><td>document-save               <td align="center">YES <td> @image html document-save.ico
<tr><td>drive-optical               <td align="center">YES <td> @image html drive-optical.ico
<tr><td>drive-harddisk              <td align="center">YES <td> @image html drive-harddisk.ico
<tr><td>drive-removable-media       <td align="center">YES <td> @image html drive-removable-media.ico
<tr><td>edit-clear-locationbar-ltr  <td align="center">YES <td> @image html edit-clear-locationbar-ltr.ico
<tr><td>edit-clear-locationbar-rtl  <td align="center">YES <td> @image html edit-clear-locationbar-rtl.ico
<tr><td>edit-clear                  <td align="center">YES <td> @image html edit-clear.ico
<tr><td>edit-copy                   <td align="center">YES <td> @image html edit-copy.ico
<tr><td>edit-cut                    <td align="center">YES <td> @image html edit-cut.ico
<tr><td>edit-find                   <td align="center">YES <td> @image html edit-find.ico
<tr><td>edit-paste                  <td align="center">YES <td> @image html edit-paste.ico
<tr><td>edit-redo                   <td align="center">YES <td> @image html edit-redo.ico
<tr><td>edit-rename                 <td align="center">YES <td> @image html edit-rename.ico
<tr><td>edit-undo                   <td align="center">YES <td> @image html edit-undo.ico
<tr><td>filter                      <td align="center">NO  <td> @image html filter.ico
<tr><td>folder                      <td align="center">YES <td> @image html folder.ico
<tr><td>folder-new                  <td align="center">YES <td> @image html folder-new.ico
<tr><td>folder-remote               <td align="center">YES <td> @image html folder-remote.ico
<tr><td>go-down                     <td align="center">YES <td> @image html go-down.ico
<tr><td>go-home                     <td align="center">YES <td> @image html go-home.ico
<tr><td>go-next                     <td align="center">YES <td> @image html go-next.ico
<tr><td>go-previous                 <td align="center">YES <td> @image html go-previous.ico
<tr><td>go-up                       <td align="center">YES <td> @image html go-up.ico
<tr><td>link                        <td align="center">NO  <td> @image html link.ico
<tr><td>link-broken                 <td align="center">NO  <td> @image html link-broken.ico
<tr><td>picto-dec                   <td align="center">NO  <td> @image html picto-dec.ico
<tr><td>picto-inc                   <td align="center">NO  <td> @image html picto-inc.ico
<tr><td>view-refresh                <td align="center">YES <td> @image html view-refresh.ico
<tr><td>window-close                <td align="center">YES <td> @image html window-close.ico
<tr><td>window-new                  <td align="center">YES <td> @image html window-new.ico
</table>

@section conf_sect Configuration item names, types and built-in (default) values
Conf class has string-based item naming scheme.
The below information related to following methods:
- Conf::set()
- Conf::str()
- Conf::font()
- Conf::color()
- Conf::brush()
- Conf::redirect()
- Conf::unset()

Standard type names.
<table>
<tr><th>Actual style item name  <th>Item type   <th>Built-In Value
<tr><td>font                    <td>font        <td>
<tr><td>mono_font               <td>font        <td>
<tr><td>edit_font               <td>font        <td>
<tr><td>tooltip_font            <td>font        <td>
<tr><td>menu_font               <td>font        <td>
<tr><td>foreground              <td>color       <td>Black
<tr><td>menu_foreground         <td>color       <td>DarkSlateGray
<tr><td>slider_foreground       <td>color       <td>SteelBlue
<tr><td>progress_foreground     <td>color       <td>Blue
<tr><td>accel_foreground        <td>color       <td>\#395CC8
<tr><td>tooltip_foreground      <td>color       <td>Black
<tr><td>background              <td>brush       <td>LightGray
<tr><td>progress_background     <td>brush       <td>DeepSkyBlue
<tr><td>whitespace_background   <td>brush       <td>Snow
<tr><td>menu_background         <td>brush       <td>Silver
<tr><td>select_background       <td>color       <td>DeepSkyBlue
<tr><td>tooltip_background      <td>brush       <td>Aquamarine
<tr><td>button_background       <td>brush       <td>Gainsboro
<tr><td>slider_background       <td>brush       <td>DarkGray
<tr><td>mark_background         <td>color       <td>\#00ADE8
<tr><td>info_background         <td>brush       <td>\#95C8AF
<tr><td>help_background         <td>brush       <td>LightBlue
<tr><td>warn_background         <td>brush       <td>\#F8ED89
<tr><td>error_background        <td>brush       <td>\#EC99B5
<tr><td>highlight_background    <td>brush       <td>\#C5FBFB
</table>

@section action_sect Standard Actions
Standard action names used by library.

Actions provide:
- Key bindings
- Standard icon names
- Labels
- Tooltips

Some actions provide more than one keyboard accelerator, in such cases
key bindings shown here separated by spaces.

To obtain a standard action, use Theme::find_action() method.

Standard action names:
<table>
<tr><th>Action name         <th>Meaning                                 <th>Key bindings                    <th>%Icon sample
<tr><td>dialog-apply        <td>Apply some changes                      <td><b>Enter                    </b><td> @image html dialog-ok.ico
<tr><td>dialog-cancel       <td>Cancel cahnges                          <td><b>Escape Cancel            </b><td> @image html dialog-cancel.ico
<tr><td>edit-copy           <td>Copy selection to clipboard             <td><b>Ctrl+C Ctrl+Insert       </b><td> @image html edit-copy.ico
<tr><td>edit-cut            <td>Cut selection to clipboard              <td><b>Ctrl+X Ctrl+Delete       </b><td> @image html edit-cut.ico
<tr><td>edit-paste          <td>Paste selection from clipboard          <td><b>Ctrl+V Shift+Insert      </b><td> @image html edit-paste.ico
<tr><td>focus-next          <td>Focus next child within container       <td><b>Tab                      </b><td> @image html go-next.ico
<tr><td>focus-previous      <td>Focus previous child within container   <td><b>Shift+Tab Shift+LeftTab  </b><td> @image html go-previous.ico
<tr><td>pan-left            <td>Pan left                                <td><b>Ctrl+Alt+Left            </b><td>
<tr><td>pan-right           <td>Pan right                               <td><b>Ctrl+Alt+Right           </b><td>
<tr><td>pan-up              <td>Pan up                                  <td><b>Ctrl+Up                  </b><td>
<tr><td>pan-down            <td>Pan down                                <td><b>Ctrl+Down                </b><td>
</table>

@section navigator_info Navigator and Fileman classes info item names
Navigator and Fileman classes has text-based info items management. The following information related to methods:
-  Navigator::set_option()
-  Navigator::reset_option()
-  Navigator::has_option()
-  Navigator::options()
-  Fileman::set_option()
-  Fileman::reset_option()
-  Fileman::has_option()
-  Fileman::options()

Navigator info names:
<table>
<tr><th>Item name           <th>Meaning
<tr><td>name                <td>Show/hide file/folder name
<tr><td>bytes               <td>Show/hide file/folder size in bytes
<tr><td>date                <td>Show/hide file/folder modify time
<tr><td>hidden              <td>Show/hide hidden files
<tr><td>backward            <td>Sort backward
<tr><td>multiple            <td>Allow multiple select
<tr><td>dir_select          <td>Allow directory selection
<tr><td>dirs_only           <td>Show directories only
<tr><td>observer            <td>Show/hide observer
</table>

@section kbd_sect Everything Regarding the Keyboard
Keyboard management done with using pairs of values key_code:key_modifier named <b>key specification</b>.
The possible values of key codes are summarized in #Key_codes enumeration, key modifiers are summarized
in #Key_modifiers enumeration. The prefix for key code constants is @b KC_ (KC is for Key Code), the
prefix for key modifiers constants is @b KM_ (KM is for Key Modifier). The #Key_codes enumeration currently contains
only non-Unicode key codes (except @link KC_SPACE @endlink, which is a "Space" character code, U+0020) but it might change.
The Unicode key codes handled using its Unicode value.

The system processing of keyboard events perform by Display class object. Display translates key codes and key
modifiers into internal representation and emit series of signals to the current focused Window in the following order:
-   If key pressed:
    +   <em>Widget_impl::signal_accel() [implementation API, opaque]</em>.
        This signal is connected to the keyboard accelerator objects, named Accel. If any accelerator within
        current focus path handle emitted signal, the emission of the signal is interrupted.
    +   Widget::signal_input() [public API].
        If entered key value belong to Unicode range of codes, this signal emitted. If any of Widget within
        current focus path handle emitted signal, the emission of the signal is interrupted.
    +   Widget::signal_key_down() [public API].
        If signal was not handled earlier, this signal is emmited. It can handle both Unicode and
        non-Unicode key codes.
-   If key released:
    +   Widget::signal_key_up() [public API].

Typically, the ordinary user must not use above signals directly. The best way to handle keyboard events is to use
specially designed for this purpose classes Accel, Action and Toggle_action. For Unicode input, you can use
specially designed classes Edit and Entry.

Each pair of key_code:key_modifier values can be represented as text, using for serialization, displaying, saving
within a file etc. See also @ref km_sect and @ref kc_sect.

Related functions and methods:

- key_modifiers_to_string()
- key_modifiers_from_string()
- key_spec_to_string()
- key_spec_to_label()
- key_spec_from_string()
- key_code_is_modifier()
- Widget::signal_input()
- Widget::signal_key_down()
- Widget::signal_key_up()
- Accel::Accel(char32_t kc, int km)
- Accel::Accel(char32_t kc, int km, slot< bool()>)
- Accel::Accel(const ustring & spec)
- Accel::Accel(const ustring & spec, slot< bool()>)
- bool Accel::equal(char32_t kc, int km) const
- bool Accel::equal(const ustring & spec) const
- void Accel::assign(char32_t kc, int km)
- void Accel::assign(const ustring & spec)
- Accel::keys()
- Accel::spec()
- Accel::label()
- Master_action::Master_action(char32_t kc, int km)
- Master_action::Master_action(const ustring & accels)
- Master_action::Master_action(char32_t kc, int km, const ustring &)
- Master_action::Master_action(const ustring & accels, const ustring &)
- Master_action::Master_action(char32_t kc, int km, const ustring &, const ustring &)
- Master_action::Master_action(const ustring & accels, const ustring &, const ustring &)
- Master_action::Master_action(char32_t kc, int km, const ustring &, const ustring &, const ustring &)
- Master_action::Master_action(const ustring & accels, const ustring &, const ustring &, const ustring &)
- Master_action::add_accel()
- Master_action::add_accels()
- Master_action::remove_accel()
- Master_action::remove_accels()
- Action_base::add_accel()
- Action_base::add_accels()
- Action_base::remove_accel()
- Action_base::remove_accels()
- Action::Action(char32_t kc, int km, slot< void()>)
- Action::Action(const ustring & accels, slot< void()>)
- Action::Action(char32_t kc, int km, const ustring &, slot< void()>)
- Action::Action(const ustring & accels, const ustring &, slot< void()>)
- Action::Action(char32_t kc, int km, const ustring &, const ustring &, slot< void()>)
- Action::Action(const ustring & accels, const ustring &, const ustring &, slot< void()>)
- Action::Action(char32_t kc, int km, const ustring &, const ustring &, const ustring &, slot< void()>)
- Action::Action(const ustring & accels, const ustring &, const ustring &, const ustring &, slot< void()>)
- Toggle_action::Toggle_action(char32_t kc, int km, slot< void(bool)>)
- Toggle_action::Toggle_action(const ustring & accels, slot< void(bool)>)
- Toggle_action::Toggle_action(char32_t kc, int km, const ustring &, slot< void(bool)>)
- Toggle_action::Toggle_action(const ustring & accels, const ustring &, slot< void(bool)>)
- Toggle_action::Toggle_action(char32_t kc, int km, const ustring &, const ustring &, slot< void(bool)>)
- Toggle_action::Toggle_action(const ustring & accels, const ustring &, const ustring &, slot< void(bool)>)
- Toggle_action::Toggle_action(char32_t kc, int km, const ustring &, const ustring &, const ustring &, slot< void(bool)>)
- Toggle_action::Toggle_action(const ustring & accels, const ustring &, const ustring &, const ustring &, slot< void(bool)>)

@section km_sect Key modifier textual representation
Each key modifier from #Key_modifiers enumeration can be represented as text in two ways:
1.  As part of key specification. In this case the representation of the key modifiers looks like, for example,
    <b>"\<CONTROL\>" </b>, <b>"\<ALT\>\<SHIFT\>" </b> and so on.
2.  As a human readable text, for the tool tips and menu items. In this case the textual representation looks like,
    for example, <b>"Ctrl+BackSpace"</b>, <b>"Alt+Shift+N"</b>. There are no way to represent key modifier only as a
    human readable text.

At the basic level, the first conversion can be done with calling key_modifiers_to_string() function, the
back conversion can be done with calling key_modifiers_from_string() function. For addition, Accel::spec()
can be used, which calls key_modifiers_to_string().

The second case is handled by key_spec_to_label() function, which converts pair key_code:key_modifier to
human readable representation. For addition, Accel::label() can be used, which calls key_spec_to_label().

Related functions and methods using textual key modifiers representation:

- key_modifiers_to_string()
- key_modifiers_from_string()
- key_spec_to_string()
- key_spec_to_label()
- key_spec_from_string()
- Accel::Accel(const ustring & spec)
- Accel::Accel(const ustring & spec, slot< bool()>)
- void Accel::assign(const ustring & spec)
- Accel::spec()
- Accel::label()
- Master_action::Master_action(const ustring & accels)
- Master_action::Master_action(const ustring & accels, const ustring &)
- Master_action::Master_action(const ustring & accels, const ustring &, const ustring &)
- Master_action::Master_action(const ustring & accels, const ustring &, const ustring &, const ustring &)
- Master_action::add_accels()
- Master_action::remove_accels()
- Action_base::add_accels()
- Action_base::remove_accels()
- Action::Action(const ustring & accels, slot< void()>)
- Action::Action(const ustring & accels, const ustring &, slot< void()>)
- Action::Action(const ustring & accels, const ustring &, const ustring &, slot< void()>)
- Action::Action(const ustring & accels, const ustring &, const ustring &, const ustring &, slot< void()>)
- Toggle_action::Toggle_action(const ustring & accels, slot< void(bool)>)
- Toggle_action::Toggle_action(const ustring & accels, const ustring &, slot< void(bool)>)
- Toggle_action::Toggle_action(const ustring & accels, const ustring &, const ustring &, slot< void(bool)>)
- Toggle_action::Toggle_action(const ustring & accels, const ustring &, const ustring &, const ustring &, slot< void(bool)>)

Key modifiers names:
<table>
<tr><th><b>KM_ constant</b>         <th>Actual value
<tr><td>@link KM_NONE       @endlink<td>@b NONE
<tr><td>@link KM_SHIFT      @endlink<td>@b SHIFT
<tr><td>@link KM_CONTROL    @endlink<td>@b CONTROL
<tr><td>@link KM_ALT        @endlink<td>@b ALT
<tr><td>@link KM_META       @endlink<td>@b META
<tr><td>@link KM_WIN        @endlink<td>@b WIN
<tr><td>@link KM_SUPER      @endlink<td>@b SUPER
<tr><td>@link KM_MENU       @endlink<td>@b MENU
<tr><td>@link KM_HYPER      @endlink<td>@b HYPER
<tr><td>@link KM_GROUP      @endlink<td>@b GROUP
<tr><td>@link KM_NUMLOCK    @endlink<td>@b NUMLOCK
<tr><td>@link KM_SCROLL     @endlink<td>@b SCROLL
<tr><td>@link KM_CAPS       @endlink<td>@b CAPS
</table>

@section kc_sect Key code textual representation
Unlike key modifiers, the key code representation is same within specification or human readable text.
For Unicode key codes, the textual representation is a character itself (except "Space" character, which
textual representation is "Space"). For example, key specification of pair U'A':KM_CONTROL returned by
key_spec_to_string() is <b>"\<CONTROL\>A" </b> and human readable text returned by key_spec_to_label()
is <b>"Ctrl+A" </b>. If key code does not belong to Unicode range, the textual representation depend
on that, is that code listed in #Key_codes enumeration or not. If it is, the predefined string will be
produced, otherwise the string like "U+xxxx" will be produced.

Related functions and methods using textual key code representation

- key_modifiers_to_string()
- key_modifiers_from_string()
- key_spec_to_string()
- key_spec_to_label()
- key_spec_from_string()
- Accel::Accel(const ustring & spec)
- Accel::Accel(const ustring & spec, slot< bool()>)
- void Accel::assign(const ustring & spec)
- Accel::spec()
- Accel::label()
- Master_action::Master_action(const ustring & accels)
- Master_action::Master_action(const ustring & accels, const ustring &)
- Master_action::Master_action(const ustring & accels, const ustring &, const ustring &)
- Master_action::Master_action(const ustring & accels, const ustring &, const ustring &, const ustring &)
- Master_action::add_accels()
- Master_action::remove_accels()
- Action_base::add_accels()
- Action_base::remove_accels()
- Action::Action(const ustring & accels, slot< void()>)
- Action::Action(const ustring & accels, const ustring &, slot< void()>)
- Action::Action(const ustring & accels, const ustring &, const ustring &, slot< void()>)
- Action::Action(const ustring & accels, const ustring &, const ustring &, const ustring &, slot< void()>)
- Toggle_action::Toggle_action(const ustring & accels, slot< void(bool)>)
- Toggle_action::Toggle_action(const ustring & accels, const ustring &, slot< void(bool)>)
- Toggle_action::Toggle_action(const ustring & accels, const ustring &, const ustring &, slot< void(bool)>)
- Toggle_action::Toggle_action(const ustring & accels, const ustring &, const ustring &, const ustring &, slot< void(bool)>)

Key names:
<table>
<tr><th><b>KC_ constant (#Key_codes enum)</b>       <th>Actual value                <th>Comment
<tr><td>@link KC_NONE                       @endlink<td>@b None                     <td>None pressed or unknown
<tr><td>@link KC_BACKSPACE                  @endlink<td>@b BackSpace                <td>BackSpace
<tr><td>@link KC_TAB                        @endlink<td>@b Tab                      <td>Tab
<tr><td>@link KC_LINEFEED                   @endlink<td>@b LineFeed                 <td>LineFeed
<tr><td>@link KC_ENTER                      @endlink<td>@b Enter                    <td>Enter
<tr><td>@link KC_RETURN                     @endlink<td>@b Return                   <td>Synonymous Enter
<tr><td>@link KC_ESCAPE                     @endlink<td>@b Escape                   <td>Escape
<tr><td>@link KC_SPACE                      @endlink<td>@b Space                    <td>Space
<tr><td>@link KC_DELETE                     @endlink<td>@b Delete                   <td>Delete
<tr><td>@link KC_F1                         @endlink<td>@b F1                       <td>Functional, F1
<tr><td>@link KC_F2                         @endlink<td>@b F2                       <td>Functional, F2
<tr><td>@link KC_F3                         @endlink<td>@b F3                       <td>Functional, F3
<tr><td>@link KC_F4                         @endlink<td>@b F4                       <td>Functional, F4
<tr><td>@link KC_F5                         @endlink<td>@b F5                       <td>Functional, F5
<tr><td>@link KC_F6                         @endlink<td>@b F6                       <td>Functional, F6
<tr><td>@link KC_F7                         @endlink<td>@b F7                       <td>Functional, F7
<tr><td>@link KC_F8                         @endlink<td>@b F8                       <td>Functional, F8
<tr><td>@link KC_F9                         @endlink<td>@b F9                       <td>Functional, F9
<tr><td>@link KC_F10                        @endlink<td>@b F10                      <td>Functional, F10
<tr><td>@link KC_F11                        @endlink<td>@b F11                      <td>Functional, F11
<tr><td>@link KC_F12                        @endlink<td>@b F12                      <td>Functional, F12
<tr><td>@link KC_F13                        @endlink<td>@b F13                      <td>Functional, F13
<tr><td>@link KC_F14                        @endlink<td>@b F14                      <td>Functional, F14
<tr><td>@link KC_F15                        @endlink<td>@b F15                      <td>Functional, F15
<tr><td>@link KC_F16                        @endlink<td>@b F16                      <td>Functional, F16
<tr><td>@link KC_F17                        @endlink<td>@b F17                      <td>Functional, F17
<tr><td>@link KC_F18                        @endlink<td>@b F18                      <td>Functional, F18
<tr><td>@link KC_F19                        @endlink<td>@b F19                      <td>Functional, F19
<tr><td>@link KC_F20                        @endlink<td>@b F20                      <td>Functional, F20
<tr><td>@link KC_F21                        @endlink<td>@b F21                      <td>Functional, F21
<tr><td>@link KC_F22                        @endlink<td>@b F22                      <td>Functional, F22
<tr><td>@link KC_F23                        @endlink<td>@b F23                      <td>Functional, F23
<tr><td>@link KC_F24                        @endlink<td>@b F24                      <td>Functional, F24
<tr><td>@link KC_F25                        @endlink<td>@b F25                      <td>Functional, F25
<tr><td>@link KC_F26                        @endlink<td>@b F26                      <td>Functional, F26
<tr><td>@link KC_F27                        @endlink<td>@b F27                      <td>Functional, F27
<tr><td>@link KC_F28                        @endlink<td>@b F28                      <td>Functional, F28
<tr><td>@link KC_F29                        @endlink<td>@b F29                      <td>Functional, F29
<tr><td>@link KC_F30                        @endlink<td>@b F30                      <td>Functional, F30
<tr><td>@link KC_F31                        @endlink<td>@b F31                      <td>Functional, F31
<tr><td>@link KC_F32                        @endlink<td>@b F32                      <td>Functional, F32
<tr><td>@link KC_F33                        @endlink<td>@b F33                      <td>Functional, F33
<tr><td>@link KC_F34                        @endlink<td>@b F34                      <td>Functional, F34
<tr><td>@link KC_F35                        @endlink<td>@b F35                      <td>Functional, F35
<tr><td>@link KC_LEFT                       @endlink<td>@b Left                     <td>Left
<tr><td>@link KC_RIGHT                      @endlink<td>@b Right                    <td>Right
<tr><td>@link KC_UP                         @endlink<td>@b Up                       <td>Up
<tr><td>@link KC_DOWN                       @endlink<td>@b Down                     <td>Down
<tr><td>@link KC_HOME                       @endlink<td>@b Home                     <td>Home
<tr><td>@link KC_END                        @endlink<td>@b End                      <td>End
<tr><td>@link KC_PAGE_UP                    @endlink<td>@b PageUp                   <td>Page Up
<tr><td>@link KC_PAGE_DOWN                  @endlink<td>@b PageDown                 <td>Page Down
<tr><td>@link KC_SCROLL_UP                  @endlink<td>@b ScrollUp                 <td>Scroll Up
<tr><td>@link KC_SCROLL_DOWN                @endlink<td>@b ScrollDown               <td>Scroll Down
<tr><td>@link KC_INSERT                     @endlink<td>@b Insert                   <td>Insert
<tr><td>@link KC_BEGIN                      @endlink<td>@b Begin                    <td>Begin
<tr><td>@link KC_CANCEL                     @endlink<td>@b Cancel                   <td>Cancel
<tr><td>@link KC_BREAK                      @endlink<td>@b Break                    <td>Break
<tr><td>@link KC_CLEAR                      @endlink<td>@b Clear                    <td>Clear
<tr><td>@link KC_PAUSE                      @endlink<td>@b Pause                    <td>Pause
<tr><td>@link KC_PRINT                      @endlink<td>@b Print                    <td>Print
<tr><td>@link KC_SYSREQ                     @endlink<td>@b SysReq                   <td>Sys Request
<tr><td>@link KC_HELP                       @endlink<td>@b Help                     <td>Help
<tr><td>@link KC_UNDO                       @endlink<td>@b Undo                     <td>Undo
<tr><td>@link KC_REDO                       @endlink<td>@b Redo                     <td>Redo
<tr><td>@link KC_FIND                       @endlink<td>@b Find                     <td>Find
<tr><td>@link KC_SELECT                     @endlink<td>@b Select                   <td>Select
<tr><td>@link KC_NO                         @endlink<td>@b No                       <td>No
<tr><td>@link KC_YES                        @endlink<td>@b Yes                      <td>Yes
<tr><td>@link KC_COPY                       @endlink<td>@b Copy                     <td>Copy
<tr><td>@link KC_CUT                        @endlink<td>@b Cut                      <td>Cut
<tr><td>@link KC_PASTE                      @endlink<td>@b Paste                    <td>Paste
<tr><td>@link KC_LDIR                       @endlink<td>@b DirectionL               <td>Left Direction
<tr><td>@link KC_RDIR                       @endlink<td>@b DirectionR               <td>Right Direction
<tr><td>@link KC_MULTI                      @endlink<td>@b Multi                    <td>Multi
<tr><td>@link KC_CODE_INPUT                 @endlink<td>@b CodeInput                <td>Code Input
<tr><td>@link KC_SINGLE_CANDIDATE           @endlink<td>@b SingleCandidate          <td>Single Candidate
<tr><td>@link KC_MULTIPLE_CANDIDATE         @endlink<td>@b MultipleCandidate        <td>Multiple Candidate
<tr><td>@link KC_PREVIOUS_CANDIDATE         @endlink<td>@b PreviousCandidate        <td>Previous Candidate
<tr><td>@link KC_MODE_SWITCH                @endlink<td>@b ModeSwitch               <td>Mode Switch
<tr><td>@link KC_SEPARATOR                  @endlink<td>@b %Separator               <td>Often comma.
<tr><td>@link KC_DECIMAL                    @endlink<td>@b Decimal                  <td>Decimal
<tr><td>@link KC_ISO_LOCK                   @endlink<td>@b ISO_Lock                 <td>ISO Lock
<tr><td>@link KC_LEVEL2_LATCH               @endlink<td>@b Level2_Latch             <td>Level2 Latch
<tr><td>@link KC_LEVEL3_SHIFT               @endlink<td>@b Level3_Shift             <td>Level3 Shift
<tr><td>@link KC_LEVEL3_LATCH               @endlink<td>@b Level3_Latch             <td>Level3 Latch
<tr><td>@link KC_LEVEL3_LOCK                @endlink<td>@b Level3_Lock              <td>Level3 Lock
<tr><td>@link KC_LEVEL5_SHIFT               @endlink<td>@b Level5_Shift             <td>Level5 Shift
<tr><td>@link KC_LEVEL5_LATCH               @endlink<td>@b Level5_Latch             <td>Level5 Latch
<tr><td>@link KC_LEVEL5_LOCK                @endlink<td>@b Level5_Lock              <td>Level5 Lock
<tr><td>@link KC_GROUP_SHIFT                @endlink<td>@b GroupShift               <td>Group Shift
<tr><td>@link KC_GROUP_LATCH                @endlink<td>@b GroupLatch               <td>Group Latch
<tr><td>@link KC_GROUP_LOCK                 @endlink<td>@b GroupLock                <td>Group Lock
<tr><td>@link KC_NEXT_GROUP                 @endlink<td>@b NextGroup                <td>Next Group
<tr><td>@link KC_NEXT_GROUP_LOCK            @endlink<td>@b NextGroupLock            <td>Next Group Lock
<tr><td>@link KC_PREV_GROUP                 @endlink<td>@b PrevGroup                <td>Prev Group
<tr><td>@link KC_PREV_GROUP_LOCK            @endlink<td>@b PrevGroupLock            <td>Prev Group Lock
<tr><td>@link KC_FIRST_GROUP                @endlink<td>@b FirstGroup               <td>First Group
<tr><td>@link KC_FIRST_GROUP_LOCK           @endlink<td>@b FirstGroupLock           <td>First Group Lock
<tr><td>@link KC_LAST_GROUP                 @endlink<td>@b LastGroup                <td>Last Group
<tr><td>@link KC_LAST_GROUP_LOCK            @endlink<td>@b LastGroupLock            <td>Last Group Lock
<tr><td>@link KC_LEFT_TAB                   @endlink<td>@b TabL                     <td>Left Tab
<tr><td>@link KC_MOVE_LINE_UP               @endlink<td>@b MoveLineUp               <td>Move Line Up
<tr><td>@link KC_MOVE_LINE_DOWN             @endlink<td>@b MoveLineDown             <td>Move Line Down
<tr><td>@link KC_PARTIAL_SPACE_LEFT         @endlink<td>@b PartialSpaceLeft         <td>Partial Space Left
<tr><td>@link KC_PARTIAL_SPACE_RIGHT        @endlink<td>@b PartialSpaceRight        <td>Partial Space Right
<tr><td>@link KC_PARTIAL_LINE_UP            @endlink<td>@b PartialLineUp            <td>Partial Line Up
<tr><td>@link KC_PARTIAL_LINE_DOWN          @endlink<td>@b PartialLineDown          <td>Partial Line Down
<tr><td>@link KC_SET_MARGIN_LEFT            @endlink<td>@b SetMarginLeft            <td>Set %Margin Left
<tr><td>@link KC_SET_MARGIN_RIGHT           @endlink<td>@b SetMarginRight           <td>Set %Margin Right
<tr><td>@link KC_RELEASE_MARGIN_LEFT        @endlink<td>@b ReleaseMarginLeft        <td>Release %Margin Left
<tr><td>@link KC_RELEASE_MARGIN_RIGHT       @endlink<td>@b ReleaseMarginRight       <td>Release %Margin Right
<tr><td>@link KC_RELEASE_BOTH_MARGINS       @endlink<td>@b ReleaseBothMargins       <td>Release Both Margins
<tr><td>@link KC_FAST_CURSOR_LEFT           @endlink<td>@b FastCursorLeft           <td>Fast %Cursor Left
<tr><td>@link KC_FAST_CURSOR_RIGHT          @endlink<td>@b FastCursorRight          <td>Fast %Cursor Right
<tr><td>@link KC_FAST_CURSOR_UP             @endlink<td>@b FastCursorUp             <td>Fast %Cursor Up
<tr><td>@link KC_FAST_CURSOR_DOWN           @endlink<td>@b FastCursorDown           <td>Fast %Cursor Down
<tr><td>@link KC_CONTINUOUS_UNDERLINE       @endlink<td>@b ContinuousUnderline      <td>Continuous Underline
<tr><td>@link KC_DISCONTINUOUS_UNDERLINE    @endlink<td>@b DiscontinuousUnderline   <td>Discontinuous Underline
<tr><td>@link KC_EMPHASIZE                  @endlink<td>@b Emphasize                <td>Emphasize
<tr><td>@link KC_CENTER_OBJECT              @endlink<td>@b CenterObject             <td>Center %Object
<tr><td>@link KC_SEND                       @endlink<td>@b Send                     <td>Send mail, file, object
<tr><td>@link KC_REPLY                      @endlink<td>@b Reply                    <td>Reply e.g., mail
<tr><td>@link KC_ZOOM_IN                    @endlink<td>@b ZoomIn                   <td>Zoom in view, map, etc.
<tr><td>@link KC_ZOOM_OUT                   @endlink<td>@b ZoomOut                  <td>Zoom out view, map, etc.
<tr><td>@link KC_FORWARD                    @endlink<td>@b Forward                  <td>Forward
<tr><td>@link KC_BACK                       @endlink<td>@b Back                     <td>Back
<tr><td>@link KC_STOP                       @endlink<td>@b Stop                     <td>Stop
<tr><td>@link KC_REFRESH                    @endlink<td>@b Refresh                  <td>Refresh
<tr><td>@link KC_HOME_PAGE                  @endlink<td>@b HomePage                 <td>Home Page
<tr><td>@link KC_FAVORITES                  @endlink<td>@b Favorites                <td>Favorites
<tr><td>@link KC_SEARCH                     @endlink<td>@b Search                   <td>Search
<tr><td>@link KC_STANDBY                    @endlink<td>@b Standby                  <td>Standby
<tr><td>@link KC_OPEN_URL                   @endlink<td>@b OpenURL                  <td>Open URL
<tr><td>@link KC_LAUNCH_MAIL                @endlink<td>@b LaunchMail               <td>Launch Mail
<tr><td>@link KC_LAUNCH_MEDIA               @endlink<td>@b LaunchMedia              <td>Launch Media Player
<tr><td>@link KC_LAUNCH0                    @endlink<td>@b Launch0                  <td>Launch0
<tr><td>@link KC_LAUNCH1                    @endlink<td>@b Launch1                  <td>Launch1
<tr><td>@link KC_LAUNCH2                    @endlink<td>@b Launch2                  <td>Launch2
<tr><td>@link KC_LAUNCH3                    @endlink<td>@b Launch3                  <td>Launch3
<tr><td>@link KC_LAUNCH4                    @endlink<td>@b Launch4                  <td>Launch4
<tr><td>@link KC_LAUNCH5                    @endlink<td>@b Launch5                  <td>Launch5
<tr><td>@link KC_LAUNCH6                    @endlink<td>@b Launch6                  <td>Launch6
<tr><td>@link KC_LAUNCH7                    @endlink<td>@b Launch7                  <td>Launch7
<tr><td>@link KC_LAUNCH8                    @endlink<td>@b Launch8                  <td>Launch8
<tr><td>@link KC_LAUNCH9                    @endlink<td>@b Launch9                  <td>Launch9
<tr><td>@link KC_LAUNCHA                    @endlink<td>@b LaunchA                  <td>LaunchA
<tr><td>@link KC_LAUNCHB                    @endlink<td>@b LaunchB                  <td>LaunchB
<tr><td>@link KC_LAUNCHC                    @endlink<td>@b LaunchC                  <td>LaunchC
<tr><td>@link KC_LAUNCHD                    @endlink<td>@b LaunchD                  <td>LaunchD
<tr><td>@link KC_LAUNCHE                    @endlink<td>@b LaunchE                  <td>LaunchE
<tr><td>@link KC_LAUNCHF                    @endlink<td>@b LaunchF                  <td>LaunchF
<tr><td>@link KC_EXECUTE                    @endlink<td>@b Execute                  <td>Execute
<tr><td>@link KC_TERMINAL                   @endlink<td>@b Terminal                 <td>Terminal
<tr><td>@link KC_POWER_OFF                  @endlink<td>@b PowerOff                 <td>Power Off
<tr><td>@link KC_SLEEP                      @endlink<td>@b Sleep                    <td>Sleep
<tr><td>@link KC_WAKE_UP                    @endlink<td>@b WakeUp                   <td>Wake Up
<tr><td>@link KC_TERMINATE_SERVER           @endlink<td>@b TerminateServer          <td>Terminate Server
<tr><td>@link KC_CLOSE                      @endlink<td>@b Close                    <td>Close
<tr><td>@link KC_PHONE                      @endlink<td>@b Phone                    <td>Launch phone; dial number
<tr><td>@link KC_RELOAD                     @endlink<td>@b Reload                   <td>Reload web page, file, etc.
<tr><td>@link KC_SAVE                       @endlink<td>@b Save                     <td>Save (file, document, state
<tr><td>@link KC_VIDEO                      @endlink<td>@b Video                    <td>Launch video player
<tr><td>@link KC_MUSIC                      @endlink<td>@b Music                    <td>Launch music applicaton
<tr><td>@link KC_BATTERY                    @endlink<td>@b Battery                  <td>%Display battery information
<tr><td>@link KC_BLUETOOTH                  @endlink<td>@b Bluetooth                <td>Enable/disable Bluetooth
<tr><td>@link KC_WLAN                       @endlink<td>@b WLAN                     <td>Enable/disable WLAN
<tr><td>@link KC_VOLUME_UP                  @endlink<td>@b VolumeUp                 <td>Volume Up
<tr><td>@link KC_VOLUME_DOWN                @endlink<td>@b VolumeDown               <td>Volume Down
<tr><td>@link KC_VOLUME_MUTE                @endlink<td>@b VolumeMute               <td>Volume Mute
<tr><td>@link KC_BASS_BOOST                 @endlink<td>@b BassBoost                <td>Bass Boost
<tr><td>@link KC_BASS_UP                    @endlink<td>@b BassUp                   <td>Bass Up
<tr><td>@link KC_BASS_DOWN                  @endlink<td>@b BassDown                 <td>Bass Down
<tr><td>@link KC_TREBLE_UP                  @endlink<td>@b TrebleUp                 <td>Treble Up
<tr><td>@link KC_TREBLE_DOWN                @endlink<td>@b TrebleDown               <td>Treble Down
<tr><td>@link KC_MEDIA_PLAY                 @endlink<td>@b MediaPlay                <td>Media Play
<tr><td>@link KC_MEDIA_PAUSE                @endlink<td>@b MediaPause               <td>Media Pause
<tr><td>@link KC_MEDIA_STOP                 @endlink<td>@b MediaStop                <td>Media Stop
<tr><td>@link KC_MEDIA_PREVIOUS             @endlink<td>@b MediaPrevious            <td>Media Previous
<tr><td>@link KC_MEDIA_NEXT                 @endlink<td>@b MediaNext                <td>Media Next
<tr><td>@link KC_MEDIA_RECORD               @endlink<td>@b MediaRecord              <td>Media Record
<tr><td>@link KC_KANJI                      @endlink<td>@b Kanji                    <td>Kanji, Kanji convert
<tr><td>@link KC_MUHENKAN                   @endlink<td>@b Muhenkan                 <td>Cancel Conversion
<tr><td>@link KC_HENKAN                     @endlink<td>@b Henkan                   <td>Alias for Henkan_Mode
<tr><td>@link KC_ROMAJI                     @endlink<td>@b Romaji                   <td>to Romaji
<tr><td>@link KC_HIRAGANA                   @endlink<td>@b Hiragana                 <td>to Hiragana
<tr><td>@link KC_KATAKANA                   @endlink<td>@b Katakana                 <td>to Katakana
<tr><td>@link KC_HIRAGANA_KATAKANA          @endlink<td>@b HiraganaKatakana         <td>Hiragana/Katakana toggle
<tr><td>@link KC_ZENKAKU                    @endlink<td>@b Zenkaku                  <td>to Zenkaku
<tr><td>@link KC_HANKAKU                    @endlink<td>@b Hankaku                  <td>to Hankaku
<tr><td>@link KC_ZENKAKU_HANKAKU            @endlink<td>@b ZenkakuHankaku           <td>Zenkaku/Hankaku toggle
<tr><td>@link KC_TOUROKU                    @endlink<td>@b Touroku                  <td>Add to Dictionary
<tr><td>@link KC_MASSYO                     @endlink<td>@b Massyo                   <td>Delete from Dictionary
<tr><td>@link KC_KANA_LOCK                  @endlink<td>@b KanaLock                 <td>Kana Lock
<tr><td>@link KC_KANA_SHIFT                 @endlink<td>@b KanaShift                <td>Kana Shift
<tr><td>@link KC_EISU_SHIFT                 @endlink<td>@b EisuShift                <td>Alphanumeric Shift
<tr><td>@link KC_EISU_TOGGLE                @endlink<td>@b EisuToggle               <td>Alphanumeric toggle
<tr><td>@link KC_HANGUL                     @endlink<td>@b Hangul                   <td>Hangul start/stop(toggle)
<tr><td>@link KC_HANGUL_START               @endlink<td>@b HangulStart              <td>Hangul start
<tr><td>@link KC_HANGUL_END                 @endlink<td>@b HangulEnd                <td>Hangul end, English start
<tr><td>@link KC_HANGUL_HANJA               @endlink<td>@b HangulHanja              <td>Start Hangul->Hanja Conversion
<tr><td>@link KC_HANGUL_JAMO                @endlink<td>@b HangulJamo               <td>Hangul Jamo mode
<tr><td>@link KC_HANGUL_ROMAJA              @endlink<td>@b HangulRomaja             <td>Hangul Romaja mode
<tr><td>@link KC_HANGUL_JEONJA              @endlink<td>@b HangulJeonja             <td>Jeonja mode
<tr><td>@link KC_HANGUL_BANJA               @endlink<td>@b HangulBanja              <td>Banja mode
<tr><td>@link KC_HANGUL_PREHANJA            @endlink<td>@b HangulPrehanja           <td>Pre Hanja conversion
<tr><td>@link KC_HANGUL_POSTHANJA           @endlink<td>@b HangulPosthanja          <td>Post Hanja conversion
<tr><td>@link KC_HANGUL_SPECIAL             @endlink<td>@b HangulSpecial            <td>Special symbols
<tr><td>@link KC_BRAILLE_DOT_1              @endlink<td>@b BrailleDot_1             <td>Braille Dot 1
<tr><td>@link KC_BRAILLE_DOT_2              @endlink<td>@b BrailleDot_2             <td>Braille Dot 2
<tr><td>@link KC_BRAILLE_DOT_3              @endlink<td>@b BrailleDot_3             <td>Braille Dot 3
<tr><td>@link KC_BRAILLE_DOT_4              @endlink<td>@b BrailleDot_4             <td>Braille Dot 4
<tr><td>@link KC_BRAILLE_DOT_5              @endlink<td>@b BrailleDot_5             <td>Braille Dot 5
<tr><td>@link KC_BRAILLE_DOT_6              @endlink<td>@b BrailleDot_6             <td>Braille Dot 6
<tr><td>@link KC_BRAILLE_DOT_7              @endlink<td>@b BrailleDot_7             <td>Braille Dot 7
<tr><td>@link KC_BRAILLE_DOT_8              @endlink<td>@b BrailleDot_8             <td>Braille Dot 8
<tr><td>@link KC_BRAILLE_DOT_9              @endlink<td>@b BrailleDot_9             <td>Braille Dot 9
<tr><td>@link KC_BRAILLE_DOT_10             @endlink<td>@b BrailleDot_10            <td>Braille Dot 10
<tr><td>@link KC_LSHIFT                     @endlink<td>@b ShiftL                   <td>Left Shift
<tr><td>@link KC_RSHIFT                     @endlink<td>@b ShiftR                   <td>Right Shift
<tr><td>@link KC_SHIFT                      @endlink<td>@b Shift                    <td>Shift
<tr><td>@link KC_LCONTROL                   @endlink<td>@b ControlL                 <td>Left Control
<tr><td>@link KC_RCONTROL                   @endlink<td>@b ControlR                 <td>Right Control
<tr><td>@link KC_CONTROL                    @endlink<td>@b Control                  <td>Control
<tr><td>@link KC_LALT                       @endlink<td>@b AltL                     <td>Left Alt
<tr><td>@link KC_RALT                       @endlink<td>@b AltR                     <td>Right Alt
<tr><td>@link KC_ALT                        @endlink<td>@b Alt                      <td>Alt
<tr><td>@link KC_LMETA                      @endlink<td>@b MetaL                    <td>Left Meta
<tr><td>@link KC_RMETA                      @endlink<td>@b MetaR                    <td>Right Meta
<tr><td>@link KC_META                       @endlink<td>@b Meta                     <td>Meta
<tr><td>@link KC_LWIN                       @endlink<td>@b WinL                     <td>Left Windows
<tr><td>@link KC_RWIN                       @endlink<td>@b WinR                     <td>Right Windows
<tr><td>@link KC_WIN                        @endlink<td>@b Win                      <td>Windows
<tr><td>@link KC_LSUPER                     @endlink<td>@b SuperL                   <td>Left Super
<tr><td>@link KC_RSUPER                     @endlink<td>@b SuperR                   <td>Right Super
<tr><td>@link KC_SUPER                      @endlink<td>@b Super                    <td>Super
<tr><td>@link KC_LMENU                      @endlink<td>@b MenuL                    <td>Left %Menu
<tr><td>@link KC_RMENU                      @endlink<td>@b MenuR                    <td>Right %Menu
<tr><td>@link KC_MENU                       @endlink<td>@b %Menu                     <td>%Menu
<tr><td>@link KC_LHYPER                     @endlink<td>@b HyperL                   <td>Left Hyper
<tr><td>@link KC_RHYPER                     @endlink<td>@b HyperR                   <td>Right Hyper
<tr><td>@link KC_HYPER                      @endlink<td>@b Hyper                    <td>Hyper
<tr><td>@link KC_GROUP                      @endlink<td>@b Group                    <td>Group
<tr><td>@link KC_NUM_LOCK                   @endlink<td>@b NumLock                  <td>Num Lock
<tr><td>@link KC_SCROLL_LOCK                @endlink<td>@b ScrollLock               <td>Scroll Lock
<tr><td>@link KC_CAPS_LOCK                  @endlink<td>@b CapsLock                 <td>Caps Lock
<tr><td>@link KC_SHIFT_LOCK                 @endlink<td>@b ShiftLock                <td>Shift Lock
</table>

**/
}

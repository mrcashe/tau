# Overview

**tau** is an Open Source cross-platform library for creating graphical user interfaces (GUI) in C++ programming language.
The library has the same purpose as the leading frameworks Qt, Gtk, Fltk - building graphical interfaces for desktop operating systems, 
but does not have the same extensive potential as the above-mentioned products.

### API versioning

**tau** uses classical three-component versioning scheme: *Major_.Minor_.Micro_*.
I use underscore character ('_') here because library source code declares that
variables this manner. The actual version number can be obtained from `./VERSION` file
in project's root directory.

The latest release is [0.7.1](https://gitlab.com/mrcashe/tau/-/releases/0.7.1).

---

# Features

- BSD 2-clause license - no need to comply with (L)GPL
- Pure C++ design
- Uses exceptions for error handling
- Uses C++20 standard
- Uses only standard numeric types
- Uses only standard containers
- A small number of external dependencies
- Built-in signals and slots for event handling (derived from libsigc++-2.0)
- Set of widgets for GUI building
- UTF-8 encoding as base encoding
- Built-in i18n/l10n support
- Built-in font engine
- Cross-platform file system management
- Built-in Unicode management
- Built-in image managing classes and functions provide import from BMP, ICO, ANI, XPM, CUR, Xcursor graphics formats
- Import from PNG graphics format done with using of (optional) libpng library
- Optional [GNU gettext](https://www.gnu.org/software/gettext/) support
- The build system is [GNU Make](https://www.gnu.org/software/make/)

## Host platforms

These are platforms on which the generation of target files is possible:

-   Linux
-   FreeBSD

## Target platforms

These are platforms for which file generation is possible:

-   Linux
-   FreeBSD
-   Windows:
    1. Using M cross-platform environment, [MXE](https://mxe.cc)), MXE uses [MinGW-w64](https://www.mingw-w64.org) tool chain.
    2. Using MinGW cross-compilers provided by operating system.
    3. Using system provided **clang++** compiler with downloaded MSVC libraries (New, Experimental).

## Generating output

-   Static library for the host system (optional).
-   Shared library for the host system (default).
-   `.pc` files for `pkg-config(1)` utility.
-   Demonstration executable binary files linked against any of generated
    shared or static library.
-   Static and shared libraries for supported MinGW targets (optional).
-   C++ header files (optional).
-   [Doxygen](https://doxygen.nl/) documentation in HTML format (optional).
-   Documentation in PDF format (optional).

---

# Motivation

I am an Electronics Engineer targetted on embedded systems (microcontroller driven devices), so in my work I have to spend
more time on hardware than on software. Most of the devices I have created, although small in terms of processing power and
price, require client software at a minimum for configuration. Some of them were created for research tasks and have
relatively complex graphical interfaces designed for desktop operating systems, primarily for Windows, and less often for Linux.

Therefore, the issue of time spent on mastering modern frameworks is acute. Most devices only need very simple client
graphics software, consisting of just a couple of buttons and 5-6 labels.

At the beginning of my working career, I first used libraries for MS-DOS (Borland Turbo Vision, Borland Graphics Interface),
later the transition to Windows took place and heavier frameworks were used: Borland Object Windows Library,
Borland Visible Component Library. There was an endless rush to master more and more new frameworks, as the previous ones
left the arena in favor of newer ones. And all this for the sake of a couple of buttons and six text labels! In addition,
the Borland International ceased operations.

The meager documentation of Windows API forced us to switch to Linux at the beginning of the 2000s. The time has come
to move from proprietary frameworks to freely distributed ones: Qt, Gtk. Without a doubt, both of the above-mentioned
frameworks have shown themselves to be better in terms of survivability (compared to products from Borland).

However, the race of frameworks has been replaced by a race of releases. The first blow was the release of Qt-4, which
prompted the transition to a less complex, and, as it seemed then, more stable Gtk-2. However, the painful transition
from Gtk-2 to Gtk-3 made us think about creating our own software for a simple cross-platform graphical interface.

In addition, the licensing of leading frameworks is such that it is difficult to create low-profit proprietary software
without copyright infringement. If on Unix systems it is relatively easy to comply with the requirements of (L)GPL licenses,
then on Windows the problems begin. No one has yet been able to answer me clearly whether it is possible to link libraries of 
the Gtk family statically to a Windows PE file, provided that the source code is closed. The less complex Fltk library also 
GPL licensed. All that remains is the Qt commercial license, but we couldn’t afford it.

---

# Goals

- Be cross-platform
- Support at least Windows and Linux
- Minimize dependencies
- Use C++ Programming Language (more qualified developers)
- Use object-oriented design
- Make extensive use of the standard library
- Be as simple as possible, lower learning curve (140 classes to learn; Qt-6.7 - 1911)

---

# Issues & Missing Features

- Documentation for the project still far from what it require
- Font quality still poor on Unix: no antialiasing, hinting, ...
- Ineffective Table class implementation
- No Tree view class
- No Combo Box class
- No Unicode normalization
- No Drag'N'Drop support
- No audio support
- No video support
- No networking
- No recycle bin support
- No system tray support
- No GL/DX support
- No jpeg, gif, tiff, ... support
- ...more...

---

# Getting Started

This section provides information only for a quick start,
more detailed information can be found in the documentation
generated by Doxygen.

## Prerequisites
It would be a good idea to check the availability of the
necessary software tools before downloading code.
Most of that tools are part of the base system installation,
but some can be missing on your system.

### Programs from the Unix base system
Must present if you have working system. Presence of the following
programs is mandatory or build process will fail.

- basename
- cat
- cp
- dirname
- find
- grep
- ln
- ls
- mkdir
- mktemp
- rm
- sed
- tr
- uname

### Software build tools
May not be on your system, so you may require to install additional packages.

- ar(1) (GNU binutils)
- bash(1) (GNU Bourne Again Shell)
- g++ >=11 or clang++>=12
- gmake(1) (GNU make)
- pkg-config(1)
- strip(1) (GNU binutils)

> Note that, the build system does not support native **FreeBSD**
> `make` and `csh`, so if you are **FreeBSD** user, you may require to run:
> ```
> [~]$ sudo pkg install gmake bash pkgconf
> ```

### Optional programs
You probably won't need these programs, but the build system will still check
for them and issue appropriate messages.

- doxygen
- latex
- GNU wget

### Software packages used during build process

Note that, the following packages must have installed development files
(typically, C or C++ headers). Some Linux distros (like Ubuntu, Mageia) provide
separated packages for runtime and development files. But some others
(like ArchLinux, FreeBSD) provide monolithic packages including both runtime and
development files. In any case, the configure script will inform you if any
package is missing.

-   libc++ or libstdc++ (All platforms)
-   libpng (All platforms, optional) >= 1.6.28
-   libgettext (MXE, *BSD, optional)
-   libz (MXE, *BSD, optional, dependency of libgettext)
-   libinotify (FreeBSD only)

-   On POSIX platforms:

    + libxkbcommon-x11 >= 0.6.0
    + libxcb >= 1.11.1
    + libxcb-cursor >= 0.1.2
    + libxcb-icccm >= 0.4.1
    + libxcb-renderutil >= 0.3.9
    + libxcb-screensaver >= 1.11.1
    + libxcb-sync >= 1.11.1
    + libxcb-xfixes >= 1.11.1

> Also, on Linux, the header **inotify.h** must be accessible. Since it is part
> of the **Glibc** library, there should be no problem with its availability,
> however, the installation of the Linux Kernel headers may be required.

## Download
Download can be done from the **GitLab** site.
At this moment, only source code and documentation packages are available.

### Clone from the git repository
Therefore, **git** should be installed on your system.
Note that, the downloaded this way code is not proven and may be even unable to work.

The link is: https://gitlab.com/mrcashe/tau.git

Choose directory where to clone remote directory and type in the console:

```sh
[~]$ git clone https://gitlab.com/mrcashe/tau.git
```
The directory named `tau` will appear.

### Download latest release
All of the archives listed below contain the same source code, so you can use any of them.

#### Download zip archive
This is automatically generated [**archive**](https://gitlab.com/mrcashe/tau/-/archive/0.7.1/tau-0.7.1.zip).
```sh
[~]$ wget https://gitlab.com/mrcashe/tau/-/archive/0.7.1/tau-0.7.1.zip
[~]$ unzip 0.7.1.zip
```
The directory named `tau-0.7.1` will appear.

#### Download gzip tarball
This is automatically generated [**tarball**](https://gitlab.com/mrcashe/tau/-/archive/0.7.1/tau-0.7.1.tar.gz).
```sh
[~]$ wget https://gitlab.com/mrcashe/tau/-/archive/0.7.1/tau-0.7.1.tar.gz
[~]$ tar xf 0.7.1.tar.gz
```
The directory named `tau-0.7.1` will appear.

#### Download bzip2 tarball
This is automatically generated [**tarball**](https://gitlab.com/mrcashe/tau/-/archive/0.7.1/tau-0.7.1.tar.bz2).
```sh
[~]$ wget https://gitlab.com/mrcashe/tau/-/archive/0.7.1/tau-0.7.1.tar.bz2
[~]$ tar xf 0.7.1.tar.bz2
```
The directory named `tau-0.7.1` will appear.

#### Download Ubuntu Linux packages (amd64)
You can also install Ubuntu packages bundled within release:
- [Binary](https://gitlab.com/-/project/51102311/uploads/813d438b886b35d3d265450f554e0b3a/libtau0.7_0.7.1-1_amd64.deb)
- [Development](https://gitlab.com/-/project/51102311/uploads/a8ac8069a98bc4d7eafff15a542cb3dd/libtau0.7-dev_0.7.1-1_amd64.deb)
- [Documentation](https://gitlab.com/-/project/51102311/uploads/360ff19e221fe9cc004c661c3851959a/libtau0.7-doc_0.7.1-1_all.deb)

#### Add Ubuntu PPA repository
There is [Launchpad](https://launchpad.net) repository named [Mr.Cashe Ubuntu Packages ](https://launchpad.net/~mrcashe/+archive/ubuntu/ppa).
Follow the instructions to add archive to your Ubuntu distribution.

#### Download Doxygen documentation only
If you are not interested in building the library, you can simply download
the documentation from the [**link**](https://gitlab.com/-/project/51102311/uploads/512b9f09dbb4706e9853f6f94e5f1950/tau-0.7.1-doc.tar.xz).
```sh
[~]$ wget https://gitlab.com/-/project/51102311/uploads/fd205a2a64f6261b6a56d8b47870d5b0/tau-0.7.1-doc.tar.xz
[~]$ tar xf tau-0.7.1-doc.tar.xz
```
The directory named `html` will appear, use `html/index.html` file as argument for WWW browser.
For addition, tau-0.7.1.pdf file will appear, use your favourite PDF viewer for reading.

## Source Tree

The typical layout of downloaded directory looks similar to:

```
doc/
include/
share/
src/
sup/
AUTHORS
configure
LICENSE
README.md
VERSION
```
This is so-called **Source Tree**.
The most important file is *configure*.

## Build Tree

The **Build Tree** consists of several subdirectories:

-   `conf/` is a result of configuration process and holds control data for build process.
-   `build/` where object files collected during build process.
-   `bin/` is a place where library files and test/demo binary executable files will be located.

The Build Tree is fully independent of Source Tree and may be located at any place within file system.
The Source Tree doesn't modified during Configure, Build or Install processed.

## Configure

Despite the fact that the library does not use the **GNU Autotools**,
the `configure` script is provided. In contrast to *automake's* generated
scripts, this script is hand-made.

At this point, you can run `configure` script with default arguments:
```sh
[~]$ ./configure
```

As a result, the subdirectory named *./conf* must be created and filled within files.
Also file named `GNUmakefile` will appear. 

You can also use configure options, the brief list is:
-   *--prefix*=PREFIX the install prefix where built files will be copied.
-   *--enable-static* enable static library building.
-   *--enable-devel* enable development files (C++ headers and pkg-config) generation.

One more feature of `configure` script is it can be called from any place but not only
from project's root directory. Suppose, you downloaded source into ~/build/tau/ directory
and do not want to build there. You can make another directory, say ~/build/tau-build/,
`cd` to it and enter:

```sh
[~]$ ../tau/configure
```

...and build process will happen within `~/build/tau-build/` directory. Same way, you may
specify an absolute path to the `configure` script:

```sh
[~]$ /home/user/build/tau/configure
```

...and the result will be the same. This way you can build library with unlimited count of
different configurations. You even can write protect the source tree:

```sh
[~]$ chmod 0500 ~/build/tau
```

## Make

On BSD systems, **GNU make** named as `gmake`. On Linux, some distros (Arch, Manjaro) does not support
`gmake` symbolic link, thus, only `make` works.

The standard make targets are provided:

-   **all** means usual action: to build everything that enabled;
-   **clean** also has obvious meaning: remove all built object and library files;
-   **install** install enabled components using defined install ***PREFIX***;
-   **uninstall** uninstall previously installed package using defined install ***PREFIX***.

And one more non-standard is:

-   **rm** remove entire **Build Tree** completely, the `configure` call needed to resume build process.

> The complete list of all available targets introduced in Doxygen documentation.

To build the library, run `gmake` without arguments or run `gmake all`. The build
process will start, the generated object files will be written into `./build/` subdirectory
and built binary files, including libraries, will be written into `./bin/` subdirectory.
You can use [GNU make Parallel Execution](https://www.gnu.org/software/make/manual/html_node/Parallel.html) to 
speed up build process using `-jN` or `-j N` options, where 'N' is maximal allowed job count. 
If you have 8-core CPU, the good value for N is 6, if you have only 2-core CPU, 
the better way is don't use Parallel Execution at all.

## Install

After `gmake` succeed, you may install generated files using `gmake install` command.
The default install ***PREFIX*** is `/usr/local` and you should use `sudo` or `su`
command to install files. Good alternative is specify ***$HOME*** variable as
***PREFIX*** during configure stage, but you should to setup some environment
variables in your `~/.profile`, `~/.bash_profile` or `~/.csh_profile` file,
such as ***PATH***, ***LD_LIBRARY_PATH*** and ***PKG_CONFIG_PATH***.

### What will be installed and where

-   Shared library (and static, if enabled, too) will be installed
    into ***PREFIX***/lib/ subdirectory.
-   `pkg-config` file(s) will be installed into ***PREFIX***/*LIBDATA*/pkgconfig/
    subdirectory, where *LIBDATA* is *lib* for **Linux** and *libdata* for **FreeBSD**.
-   C++ header files will be installed into ***PREFIX***/include/tau-Major_.Minor_/
    subdirectory, where *Major_* and *Minor_* are 1st and 2nd API version
    components, see above section about API versioning.
-   Binary executable files will be installed into ***PREFIX***/bin/
    subdirectory.
-   Various data files will be installed into ***PREFIX***/share/tau-Major_.Minor_/
    subdirectory, where *Major_* and *Minor_* are 1st and 2nd API version
    components, see above section about API versioning.

> Despite libtau has builtin support for [XDG Icon Themes](https://www.freedesktop.org/wiki/Specifications/icon-theme-spec/),
> SVG graphics format not yet realized in library, so I have to bundle fallback
> icon theme derived from Oxygen icon theme that consists of ICO files.
> Its name is 'Tau'.

## Usage

### Using without Install

If you do not planning to use built libraries, you may not install the package,
all binaries are capable to run from the `./bin/` subdirectory.
> If you didn't run `gmake install` and using separate build directory, the
> fallback icon theme will not be accessible by test applications so you will
> see the black boxes on the buttons instead of icons. To resolve this, you
> can create symbolic link to the `./share/` subdirectory within Source Tree manually:
> `ln -s /path/to/tau/share share`.

### Using Built Libraries
To use library in your applications, you should have installed `pkg-config(1)` utility.
To have access to the generated `.pc` file(s), you should install the library using
`gmake install` command. `ldconfig` invocation required when you install library into
public directory, such as `/usr/local`. The build system will try to do it for you in
case you run `gmake install` as superuser ("root") using `sudo` command or somehow else.
But in some cases the manual intervention may be required to update `ldconfig(8)` database.
Run `sudo /sbin/ldconfig` after package install if you enabled shared library building.

From the other hand, if you installed shared library somewhere withing your **$HOME** directory, the
superuser rights are not required. But you should provide proper values for
**PATH**, **LD_LIBRARY_PATH** and **PKG_CONFIG_PATH** environment variables. The most proper way
to do so, is to append:
```sh
export PATH=$HOME/bin:$PATH
export PKG_CONFIG_PATH=$HOME/lib/pkgconfig:$PKG_CONFIG_PATH	# for Linux
export PKG_CONFIG_PATH=$HOME/libdata/pkgconfig:$PKG_CONFIG_PATH	# for FreeBSD
export LD_LIBRARY_PATH=$HOME/lib:$LD_LIBRARY_PATH
```
lines into your `~/.bash_profile` or `~/.csh_profile` file and do not forget to exit your session and login again.

The `pkg-config` package name for `tau` library is **tau-Major_.Minor_**, for example,
`tau-0.3`.

### Using C++ header files
The library public API consists of series of C++ header files. Those location can be obtained
by using `--cflags` option of `pkg-config(1)` utility. This is a standard approach.

### Compile

```make
# Makefile
CXXFLAGS += $(shell pkg-config --cflags tau-0.7)
objects = src-1.o src-2.o

%.o: %.cc
	$(CXX) -c -o $@ $< $(CXXFLAGS)
```

### Source Code
To use public API at the source code level, the simplest way is to include `tau.hh` header.
This is a recommended approach.
Here is a snippet from `./src/test/tauhello.cc` source file, the simplest program that can
use **tau** library:

```cpp
/// @file tauhello.cc

#include <tau.hh>
#include <iostream>

int main(int, char **) {
    try {
        tau::ustring hello("Hello, world!");
        tau::Toplevel wnd(hello);
        tau::Text text(hello);
        wnd.insert(text);
        tau::Loop().run();
    }

    catch (tau::exception & x) {
        std::cerr << "** tau::exception thrown: " << x.what() << std::endl;
    }

    catch (std::exception & x) {
        std::cerr << "** std::exception thrown: " << x.what() << std::endl;
    }

    catch (...) {
        std::cerr << "** unknown exception thrown" << std::endl;
    }

    return 0;
}
```

### Linking against shared library
The linking process is standard:

```make
# Makefile
LDFLAGS += $(shell pkg-config --libs tau-0.7)
objects = src-1.o src-2.o

program: $(objects)
        $(CXX) -o program $(objects) $(LDFLAGS)
```

### Linking against static library
For static linking, `pkg-config` file provides variable named 'a'.

```make
# Makefile
LDFLAGS += $(shell pkg-config --variable=a tau-0.7)
objects = src-1.o src-2.o

program: $(objects)
        $(CXX) -o program $(objects) $(LDFLAGS)
```

// ----------------------------------------------------------------------------
// SPDX-License-Identifier: BSD-2-Clause
// Copyright © 2014-2024 Konstantin Shmelkov <mrcashe@gmail.com>.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
// THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
// PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
// CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
// EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
// PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
// OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
// WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
// OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
// EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// ----------------------------------------------------------------------------

/// @file gettext.hh GNU gettext wrapper functions.
/// @note This header not included into global header tau.hh, so include it inclusively!

#ifndef __TAU_GETTEXT_HH__
#define __TAU_GETTEXT_HH__

#include <tau/ustring.hh>

#define gettext_noop(msgid) (msgid)

namespace tau {

/// Look up @a msgid in the current default message catalog for the current @b LC_MESSAGES locale.
/// If not found, returns @a msgid itself (the default text).
/// @ingroup gettext_group
__TAUEXPORT__ ustring gettext(const ustring & msgid);

/// Look up @a msgid in the @a domain message catalog for the current @b LC_MESSAGES locale.
/// @ingroup gettext_group
__TAUEXPORT__ ustring dgettext(std::string_view domain, const ustring & msgid);

/// Look up @a msgid in the @a domain message catalog for the current @a category locale.
/// @ingroup gettext_group
__TAUEXPORT__ ustring dcgettext(std::string_view domain, const ustring & msgid, int category);

/// Similar to gettext() but select the plural form corresponding to the number @a n.
/// @ingroup gettext_group
__TAUEXPORT__ ustring ngettext(const ustring & msgid1, const ustring & msgid2, unsigned long n);

/// Similar to dgettext() but select the plural form corresponding to the number @a n.
/// @ingroup gettext_group
__TAUEXPORT__ ustring dngettext(std::string_view domain, const ustring & msgid1, const ustring & msgid2, unsigned long n);

/// Similar to dcgettext() but select the plural form corresponding to the number @a n.
/// @ingroup gettext_group
__TAUEXPORT__ ustring dcngettext(std::string_view domain, const ustring & msgid1, const ustring & msgid2, unsigned long n, int category);

/// Specify that the @a domain message catalog will be found in @a dirname rather than in the system locale data base.
/// @return path to the file with translations or empty string if not found.
/// @ingroup gettext_group
__TAUEXPORT__ ustring gettext_open(std::string_view domain, const ustring & dirname);

/// Bind @a domain message catalog.
/// @return path to the file with translations or empty string if not found.
/// @ingroup gettext_group
__TAUEXPORT__ ustring gettext_open(std::string_view domain);

} // namespace tau

using tau::gettext;
using tau::dgettext;
using tau::dcgettext;
using tau::ngettext;
using tau::dngettext;
using tau::dcngettext;

#endif // __TAU_GETTEXT_HH__

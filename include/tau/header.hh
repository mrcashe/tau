// ----------------------------------------------------------------------------
// SPDX-License-Identifier: BSD-2-Clause
// Copyright © 2014-2024 Konstantin Shmelkov <mrcashe@gmail.com>.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
// THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
// PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
// CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
// EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
// PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
// OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
// WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
// OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
// EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// ----------------------------------------------------------------------------

/// @file header.hh Header class.
/// @since 0.6.0

#ifndef __TAU_HEADER_HH__
#define __TAU_HEADER_HH__

#include <tau/separator.hh>

namespace tau {

/// List header.
/// @note This class is a wrapper around its implementation shared pointer.
/// @ingroup widget_group
/// @since 0.6.0
class __TAUEXPORT__ Header: public Widget {
public:

    /// @name Constructors & Operators
    /// @{

    /// Default constructor.
    Header(bool resize_allowed=true);

    /// Constructor with List or List_text.
    Header(List_base & list, bool resize_allowed=true);

    /// Constructor with separator style.
    /// @since 0.6.0
    Header(Separator::Style sep_style, bool resize_allowed=true);

    /// Constructor with separator style and List or List_text.
    /// @since 0.6.0
    Header(Separator::Style sep_style, List_base & list, bool resize_allowed=true);

    /// Copy constructor.
    ///
    /// @note This class is a wrapper around its implementation shared pointer,
    /// so copying it just increasing implementation pointer use count, but isn't
    /// really copies the object. The underlying implementation is not copyable.
    Header(const Header & other);

    /// Copy operator.
    ///
    /// @note This class is a wrapper around its implementation shared pointer,
    /// so copying it just increasing implementation pointer use count, but isn't
    /// really copies the object. The underlying implementation is not copyable.
    Header & operator=(const Header & other);

    /// Move constructor.
    ///
    /// @note This class is a wrapper around its implementation shared pointer,
    /// so copying it just increasing implementation pointer use count, but isn't
    /// really copies the object. The underlying implementation is not copyable.
    /// @note This constructor does not reset @c other implementation pointer.
    Header(Header && other);

    /// Move operator.
    ///
    /// @note This class is a wrapper around its implementation shared pointer,
    /// so copying it just increasing implementation pointer use count, but isn't
    /// really copies the object. The underlying implementation is not copyable.
    /// @note This operator does not reset @c other implementation pointer.
    Header & operator=(Header && other);

    /// Constructor with implementation pointer.
    ///
    /// @warning Unlike some other classes (Painter as an example), the whole
    /// @ref widget_stack "widget stack" is unable to run with pure implementation
    /// pointer, so attempting to construct widget from a pure (@b nullptr) pointer
    /// will cause throwing an user_error exception!
    /// That exception also will be thrown if user tries to construct the object
    /// from incompatible implementation shared pointer.
    ///
    /// @throw user_error in case of pure implementation pointer or incompatible
    /// implementation pointer class.
    Header(Widget_ptr wp);

    /// Assign implementation.
    ///
    /// @throw user_error in case of pure implementation pointer or incompatible
    /// implementation pointer class.
    Header & operator=(Widget_ptr wp);

    /// @}
    /// @name Row Headers
    /// @{

    void show_column(int column);
    void show_column(int column, const ustring & title, Align align=Align::START);
    void show_column(int column, Widget & title);
    void hide_column(int column);

    /// @}
    /// @name Resize
    /// @{

    /// Allow column resize.
    /// @remark Allowed by default.
    /// @since 0.6.0
    void allow_resize();

    /// Disallow column resize.
    /// @remark Allowed by default.
    /// @since 0.6.0
    void disallow_resize();

    /// Test if column resize allowed.
    /// @remark Allowed by default.
    /// @since 0.6.0
    bool resize_allowed() const noexcept;

    /// @}
    /// @name Separators
    /// @{

    /// Set separator style.
    /// @remark by default, separator style is Separator::HANDLE if resize allowed and Seprator::SOLID otherwise.
    /// @since 0.6.0
    void set_separator_style(Separator::Style sep_style);

    /// Get separator style.
    /// @remark by default, separator style is Separator::HANDLE if resize allowed and Seprator::SOLID otherwise.
    /// @since 0.6.0
    Separator::Style separator_style() const noexcept;

    /// @}
    /// @name Sort Markers
    /// @{

    void show_sort_marker(int column, bool descend=false);
    void hide_sort_marker();

    /// @}
    /// @name Signals
    /// @{

    /// Signal emitted when user clicks on the header.
    ///
    /// Slot prototype:
    /// ~~~~~~~~~~~~~~~
    /// void on_header_click(int column);
    /// ~~~~~~~~~~~~~~~
    signal<void(int)> & signal_click();

    /// Signal emitted when user changes header width.
    ///
    /// Slot prototype:
    /// ~~~~~~~~~~~~~~~
    /// void on_header_width_changed(int column);
    /// ~~~~~~~~~~~~~~~
    signal<void(int)> & signal_width_changed();

    /// @}
};

} // namespace tau

#endif // __TAU_HEADER_HH__

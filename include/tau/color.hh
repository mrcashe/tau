// ----------------------------------------------------------------------------
// Copyright © 2014-2024 Konstantin Shmelkov <mrcashe@gmail.com>.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
// THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
// PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
// CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
// EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
// PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
// OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
// WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
// OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
// EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// ----------------------------------------------------------------------------

/// @file color.hh The Color class.

#ifndef __TAU_COLOR_HH__
#define __TAU_COLOR_HH__

#include <tau/ustring.hh>
#include <optional>
#include <vector>

namespace tau {

/// ARGB and HSV model color.
/// @ingroup paint_group
class __TAUEXPORT__ Color {
public:

    /// @name Constructors
    /// @{

    /// Default constructor.
    Color() = default;

    /// Copy constructor.
    Color(const Color & other) = default;

    /// Copy operator.
    Color & operator=(const Color & other) = default;

    /// Constructor with RGB, range is 0.0...1.0.
    Color(double red, double green, double blue, double alpha=1.0);

    /// Constructor with HTML-like string and alpha.
    Color(std::string_view s, double alpha=1.0);

    /// @}
    /// @name Control
    /// @{

    /// Assign value from HTML-like string and alpha.
    void set(std::string_view s, double alpha=1.0);

    /// Assign value form ARGB components, range is 0.0...1.0.
    void set(double red, double green, double blue, double alpha=1.0);

    /// Assign value from RGB24.
    void set_rgb24(uint32_t rgb24, double alpha=1.0);

    /// Set red component, range is 0.0...1.0.
    void set_red(double red);

    /// Set green component, range is 0.0...1.0.
    void set_green(double green);

    /// Set blue component, range is 0.0...1.0.
    void set_blue(double blue);

    /// Set alpha channel value, range is 0.0...1.0.
    void set_alpha(double alpha);

    /// Set HSV components, hue range is 0.0...360.0, other range is 0.0...1.0.
    void set_hsv(double hue, double saturation, double value);

    /// Set HSV components and alpha channel, hue range is 0.0...360.0, other range is 0.0...1.0.
    void set_hsv(double hue, double saturation, double value, double alpha);

    /// Set hue component, range is 0.0...360.0.
    void set_hue(double hue);

    /// Set saturation component, range is 0.0...1.0.
    void set_saturation(double sat);

    /// Set value component, range is 0.0...1.0.
    void set_value(double value);

    /// Get red component, range is 0.0...1.0.
    double red() const noexcept;

    /// Get green component, range is 0.0...1.0.
    double green() const noexcept;

    /// Get blue component, range is 0.0...1.0.
    double blue() const noexcept;

    /// Get alpha channel value, range is 0.0...1.0.
    double alpha() const noexcept;

    /// Get hue component, range is 0.0...1.0.
    double hue() const noexcept;

    /// Get saturation component, range is 0.0...1.0.
    double saturation() const noexcept;

    /// Get value component, range is 0.0...1.0.
    double value() const noexcept;

    /// Assignment from HTML-like string.
    Color & operator=(std::string_view s);

    /// Compare operator.
    bool operator==(const Color & other) const noexcept;

    /// Compare operator.
    bool operator!=(const Color & other) const noexcept;

    /// Get ARGB32 data as 32-bit value, range 0...255 for every component.
    uint32_t argb32() const noexcept;

    /// Get ABGR32 data as 32-bit value, range 0...255 for every component.
    uint32_t abgr32() const noexcept;

    /// Get RGB24 data as 32-bit value, range 0...255 for every component.
    uint32_t rgb24() const noexcept;

    /// Get BGR24 data as 32-bit value, range 0...255 for every component.
    uint32_t bgr24() const noexcept;

    /// Convert to 8-bit gray.
    uint8_t gray8() const noexcept;

    /// Convert to 24-bit gray.
    uint32_t gray24() const noexcept;

    /// Convert to gray.
    double gray() const noexcept;

    /// Get HTML-like string representation (smt. like #102030).
    std::string html() const;

    /// Get inverted color.
    Color inverted() const;

    /// Make lighter.
    void lighter(double factor);

    /// Make darker.
    void darker(double factor);

    /// Return lighten color.
    Color lighten(double factor) const;

    /// Return darken color.
    Color darken(double factor) const;

    /// Return low-contrast color (used for disabled GUI elements painting).
    Color faded() const;

    /// Performs alpha blending.
    /// Here is this color is a target color.
    /// @param src the source color.
    /// @since 0.4.0
    void alpha_blend(const Color & src) noexcept;

    /// Performs alpha blending.
    /// Here is this color is a constant target color, but the result will be returned.
    /// @param src the source color.
    /// @return target color.
    /// @since 0.4.0
    Color alpha_blended(const Color & src) const;

    /// Create from ARGB32 data.
    static Color from_argb32(uint32_t argb32);

    /// Create from RGB24 data.
    static Color from_rgb24(uint32_t rgb24, double alpha=1.0);

    /// Create from gray level, range is 0.0...1.0.
    static Color from_gray(double gray, double alpha=1.0);

    /// Create from 8-bit gray level, range is 0...255.
    static Color from_gray8(uint8_t gray, double alpha=1.0);

    /// List CSS color names.
    static std::vector<std::string_view> list_css_names();

    /// Validate textual color representation.
    /// @since 0.7.0
    static bool validate(const ustring & s);

    /// @}

private:

    void calc_hsv();
    void calc_rgb();

private:

    double red_         = 0.0;
    double green_       = 0.0;
    double blue_        = 0.0;
    double alpha_       = 1.0;
    double hue_         = 0.0;
    double sat_         = 0.0;
    double value_       = 0.0;
};

/// Optional color.
/// @since 0.7.0
using Opt_color = std::optional<Color>;

} // namespace tau

#endif // __TAU_COLOR_HH__

// ----------------------------------------------------------------------------
// SPDX-License-Identifier: BSD-2-Clause
// Copyright © 2014-2024 Konstantin Shmelkov <mrcashe@gmail.com>.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
// THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
// PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
// CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
// EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
// PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
// OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
// WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
// OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
// EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// ----------------------------------------------------------------------------

/// @file string.hh Unicode manipulation functions.

#ifndef __TAU_STRING_HH__
#define __TAU_STRING_HH__

#include <tau/locale.hh>
#include <tau/ustring.hh>
#include <sstream>
#include <vector>

namespace tau {

/// Convert to upper case.
/// @ingroup string_group
__TAUEXPORT__ char32_t char32_toupper(char32_t uc);

/// Convert to lower case.
/// @ingroup string_group
__TAUEXPORT__ char32_t char32_tolower(char32_t uc);

/// Test if newline character.
/// @ingroup string_group
__TAUEXPORT__ bool char32_is_newline(char32_t uc);

/// Test if delimiter.
/// @ingroup string_group
__TAUEXPORT__ bool char32_is_delimiter(char32_t uc);

/// Test if blank.
/// @ingroup string_group
__TAUEXPORT__ bool char32_isblank(char32_t uc);

/// Test if both modifier letter or diacritical mark.
/// @ingroup string_group
__TAUEXPORT__ bool char32_is_modifier(char32_t wc);

/// Test if zero width character.
/// @ingroup string_group
__TAUEXPORT__ bool char32_is_zerowidth(char32_t uc);

/// Test if code is a control code, not alpha-numeric.
/// @ingroup string_group
__TAUEXPORT__ bool char32_is_control(char32_t kc);

/// Test if valid Unicode.
/// @ingroup string_group
__TAUEXPORT__ bool char32_is_unicode(char32_t uc);

/// Test if 16-bit character is from surrogate pair.
/// @ingroup string_group
__TAUEXPORT__ bool char16_is_surrogate(char16_t uc);

/// Convert surrogate pair into 32-bit unicode character.
/// @ingroup string_group
__TAUEXPORT__ char32_t char32_from_surrogate(char16_t c1, char16_t c2);

/// Split 32-bit character into pair of 16-bit characters.
/// If c32 belongs to surrogate range, both c1 and c2
/// values are non-zero. If it is not, the c2 is zero.
/// @ingroup string_group
__TAUEXPORT__ void char32_to_surrogate(char32_t c32, char16_t & c1, char16_t & c2);

/// Calculate an UTF-8 sequence length for specified UTF-32 character.
/// @ingroup string_group
__TAUEXPORT__ std::size_t char32_len(char32_t wc);

/// Extract UCS-4 character from pointer.
/// @ingroup string_group
__TAUEXPORT__ char32_t char32_from_pointer(const char * u);

/// Calculate UTF-8 sequence length in bytes using UTF-8 leading character.
/// @ingroup string_group
__TAUEXPORT__ std::size_t utf8_charlen(char leader);

/// Get pointer to the next UTF-8 character.
/// @ingroup string_group
__TAUEXPORT__ const char * utf8_next(const char * u);

/// Get UTF-8 string length.
/// @param u        raw C string, probably containing UTF-8 characters.
/// @param bytes    number of bytes to count:
///                 - if positive, no more than @c bytes will be examined.
///                 - if negative, count all characters to the end of string (before '\0' character).
/// @return         - if positive and non-zero, number of @b Unicode characters found (including ASCII characters).
///                 - if negative, the string does not contain any UTF-8 sequence and must be threadted as ASCII or 8-bit non-UTF-8 string.
///                   Returned value represents string length in @b bytes subtracted from zero, i.e. 0-std::strnlen(u, bytes).
/// @ingroup string_group
__TAUEXPORT__ ssize_t utf8_strlen(const char * u, ssize_t bytes=-1);

/// Convert raw UTF-8 string into UTF-32 string.
/// @param u        raw C string, probably containing UTF-8 characters.
/// @param bytes    number of bytes to convert:
///                 - if positive, no more than @c bytes will be converted.
///                 - if negative, count all characters to the end of string (before '\0' character).
/// @ingroup string_group
__TAUEXPORT__ std::u32string utf8_to_utf32(const char * u, ssize_t bytes=-1);

/// Get leading character in UTF-8 sequence.
/// @param nbytes number of bytes in sequence, 1...6.
/// @return leading character.
/// @ingroup string_group
__TAUEXPORT__ char utf8_leader(std::size_t nbytes);

/// Formatting function that accepts std::ios_base format flags such as std::hex etc  using specified locale.
/// @ingroup string_group
template<typename... Args>
inline ustring str_format(std::locale lc, Args && ... args) {
    std::ostringstream os; os.imbue(lc);
    (os << ... << std::forward<Args &&>(args));
    return os.str();
}

/// Formatting function that accepts std::ios_base format flags such as std::hex etc using "C"-locale.
/// @ingroup string_group
template<typename... Args>
inline ustring str_format(Args && ... args) {
    return str_format(std::locale::classic(), std::forward<Args &&>(args)...);
}

/// Test if string starts with given prefix.
/// @param str testing string.
/// @param prefix searching prefix.
/// @param similar if true, test case insensitive.
/// @ingroup string_group
__TAUEXPORT__ bool str_has_prefix(const ustring & str, const ustring & prefix, bool similar=false);

/// Test if string ends with given suffix.
/// @param str testing string.
/// @param suffix searching prefix.
/// @param similar if true, test case insensitive.
/// @ingroup string_group
__TAUEXPORT__ bool str_has_suffix(const ustring & str, const ustring & suffix, bool similar=false);

/// Convert to upper case.
/// @ingroup string_group
__TAUEXPORT__ ustring str_toupper(const ustring & str);

/// Convert to upper case.
/// @ingroup string_group
/// @since 0.6.0
__TAUEXPORT__ std::u32string str_toupper(const std::u32string & str);

/// Convert to lower case.
/// @ingroup string_group
__TAUEXPORT__ ustring str_tolower(const ustring & str);

/// Convert to lower case.
/// @ingroup string_group
/// @since 0.6.0
__TAUEXPORT__ std::u32string str_tolower(const std::u32string & str);

/// Remove leading blanks and newlines.
/// @ingroup string_group
__TAUEXPORT__ ustring str_trimleft(const ustring & s);

/// Remove leading characters which the user considers to be erased.
/// @ingroup string_group
/// @since 0.6.0
__TAUEXPORT__ ustring str_trimleft(const ustring & s, const ustring & match);

/// Remove leading blanks and newlines.
/// @ingroup string_group
/// @since 0.6.0
__TAUEXPORT__ std::u32string str_trimleft(const std::u32string & s);

/// Remove leading characters which the user considers to be erased.
/// @ingroup string_group
/// @since 0.6.0
__TAUEXPORT__ std::u32string str_trimleft(const std::u32string & s, const std::u32string & match);

/// Remove trailing blanks and newlines.
/// @ingroup string_group
__TAUEXPORT__ ustring str_trimright(const ustring & s);

/// Remove trailing characters which the user considers to be erased.
/// @ingroup string_group
/// @since 0.6.0
__TAUEXPORT__ ustring str_trimright(const ustring & s, const ustring & match);

/// Remove trailing blanks and newlines.
/// @ingroup string_group
/// @since 0.6.0
__TAUEXPORT__ std::u32string str_trimright(const std::u32string  & s);

/// Remove trailing characters which the user considers to be erased.
/// @ingroup string_group
/// @since 0.6.0
__TAUEXPORT__ std::u32string str_trimright(const std::u32string  & s, const std::u32string & match);

/// Remove leading and trailing blanks and newlines.
/// @ingroup string_group
/// @since 0.7.0
__TAUEXPORT__ ustring str_trimboth(const ustring & s);

/// Remove leading and trailing characters which the user considers to be erased.
/// @ingroup string_group
/// @since 0.7.0
__TAUEXPORT__ ustring str_trimboth(const ustring & s, const ustring & match);

/// Remove leading and trailing blanks and newlines.
/// @ingroup string_group
/// @since 0.7.0
__TAUEXPORT__ std::u32string str_trimboth(const std::u32string  & s);

/// Remove leading and trailing characters which the user considers to be erased.
/// @ingroup string_group
/// @since 0.7.0
__TAUEXPORT__ std::u32string str_trimboth(const std::u32string  & s, const std::u32string & match);

/// Remove consecutive, leading and trailing blanks and newlines.
/// @ingroup string_group
__TAUEXPORT__ ustring str_trim(const ustring & s);

/// Remove consecutive, leading and trailing characters which the user considers to be erased.
/// @ingroup string_group
/// @since 0.6.0
__TAUEXPORT__ ustring str_trim(const ustring & s, const ustring & match);

/// Remove consecutive, leading and trailing blanks and newlines.
/// @ingroup string_group
/// @since 0.6.0
__TAUEXPORT__ std::u32string str_trim(const std::u32string & s);

/// Remove consecutive, leading and trailing characters which the user considers to be erased.
/// @ingroup string_group
/// @since 0.6.0
__TAUEXPORT__ std::u32string str_trim(const std::u32string & s, const std::u32string & match);

/// Split a string by character.
/// @ingroup string_group
__TAUEXPORT__ std::vector<ustring> str_explode(const ustring & str, char32_t wc);

/// Split a string by any character from given string.
/// @ingroup string_group
__TAUEXPORT__ std::vector<ustring> str_explode(const ustring & str, const ustring & delimiters);

/// Split a string by blanks and newlines.
/// @ingroup string_group
/// @since 0.6.0
__TAUEXPORT__ std::vector<std::u32string> str_explode(const std::u32string & str);

/// Split a string by character.
/// @ingroup string_group
/// @since 0.6.0
__TAUEXPORT__ std::vector<std::u32string> str_explode(const std::u32string & str, char32_t wc);

/// Split a string by any character from given string.
/// @overload
/// @ingroup string_group
/// @since 0.6.0
__TAUEXPORT__ std::vector<std::u32string> str_explode(const std::u32string & str, const std::u32string & delimiters);

/// Split a string by blanks and newlines.
/// @overload
/// @ingroup string_group
__TAUEXPORT__ std::vector<ustring> str_explode(const ustring & str);

/// Join array elements with a character.
/// @ingroup string_group
__TAUEXPORT__ ustring str_implode(const std::vector<ustring> & pieces, char32_t glue);

/// Join array elements with a string.
/// @ingroup string_group
__TAUEXPORT__ ustring str_implode(const std::vector<ustring> & pieces, const ustring & glue=ustring());

/// Test strings are similar.
/// @param s1 testing string 1.
/// @param s2 testing string 2.
/// @ingroup string_group
__TAUEXPORT__ bool str_similar(const ustring & s1, const ustring & s2);

/// Test strings are similar.
/// @param test testing string.
/// @param vars variant(s) for compare against to.
/// @ingroup string_group
__TAUEXPORT__ bool str_similar(const ustring & test, const std::vector<ustring> & vars);

/// Test strings are similar.
/// @param test testing string.
/// @param vars variant(s) for compare against to.
/// @param array_size @a variant array size in elements.
/// @ingroup string_group
/// @since 0.5.0
__TAUEXPORT__ bool str_similar(const ustring & test, const ustring * vars, std::size_t array_size);

/// Test strings are similar.
/// @param test testing string.
/// @param vars variant(s) for compare against to.
/// @param delimiter variant delimiter character for use with str_explode().
/// @ingroup string_group
__TAUEXPORT__ bool str_similar(const ustring & test, const ustring & vars, char32_t delimiter);

/// Test strings are similar.
/// @param test testing string.
/// @param vars variant(s) for compare against to.
/// @param delimiters variant delimiter characters for use with str_explode().
/// @ingroup string_group
__TAUEXPORT__ bool str_similar(const ustring & test, const ustring & vars, const ustring & delimiters);

/// Test string looks like other string.
/// Performs case-insensitive match of other string.
/// @param s        testing string
/// @param match    string that probably contained by @c s
/// @param  i       the starting position within @c s
/// @return position where @c match occured or ustring::npos if not found.
/// @ingroup string_group
/// @since 0.6.0
__TAUEXPORT__ ustring::size_type str_like(const ustring & s, const ustring & match, std::size_t i=0);

/// Test string looks like other string.
/// Performs case-insensitive match of other string.
/// @param s        testing string
/// @param match    string that probably contained by @c s
/// @param  i       the starting position within @c s
/// @return position where @c match occured or std::u32string::npos if not found.
/// @ingroup string_group
/// @since 0.6.0
__TAUEXPORT__ std::u32string::size_type str_like(const std::u32string & s, const std::u32string & match, std::size_t i=0);

/// Replace characters.
/// @ingroup string_group
/// @since 0.6.0
__TAUEXPORT__ ustring str_replace(const ustring & str, char32_t match, char32_t replacement);

/// Replace characters.
/// @ingroup string_group
/// @since 0.6.0
__TAUEXPORT__ ustring str_replace(const ustring & str, char32_t match, const ustring & replacement);

/// Replace characters.
/// @ingroup string_group
/// @since 0.6.0
__TAUEXPORT__ ustring str_replace(const ustring & str, const ustring & match_any, char32_t replacement);

/// Replace characters.
/// @ingroup string_group
/// @since 0.6.0
__TAUEXPORT__ ustring str_replace(const ustring & str, const ustring & match_any, const ustring & replacement);

/// Replace characters.
/// @ingroup string_group
/// @since 0.6.0
__TAUEXPORT__ std::u32string str_replace(const std::u32string & str, char32_t match, char32_t replacement);

/// Replace characters.
/// @ingroup string_group
/// @since 0.6.0
__TAUEXPORT__ std::u32string str_replace(const std::u32string & str, char32_t match, const std::u32string & replacement);

/// Replace characters.
/// @ingroup string_group
/// @since 0.6.0
__TAUEXPORT__ std::u32string str_replace(const std::u32string & str, const std::u32string & match_any, char32_t replacement);

/// Replace characters.
/// @ingroup string_group
/// @since 0.6.0
__TAUEXPORT__ std::u32string str_replace(const std::u32string & str, const std::u32string & match_any, const std::u32string & replacement);

/// Test string is numeric.
/// Suitable for validation purposes.
/// @ingroup string_group
/// @since 0.6.0
__TAUEXPORT__ bool str_is_numeric(const ustring & s, int base=10);

/// Test string is numeric.
/// Suitable for validation purposes.
/// @ingroup string_group
/// @since 0.6.0
__TAUEXPORT__ bool str_is_numeric(const std::u32string & s, int base=10);

/// Test string not empty.
/// Suitable for validation purposes.
/// @ingroup string_group
/// @since 0.6.0
__TAUEXPORT__ bool str_not_empty(const std::u32string & s) noexcept;

/// Format file or memory size in bytes using current locale.
/// @param nbytes   number of bytes to be formatted.
/// @param si       if @b true, format in SI units (1kB = 1000 bytes), else 1KiB = 1024 bytes.
/// @ingroup string_group
__TAUEXPORT__ ustring str_bytes(uintmax_t nbytes, bool si=false);

/// Format file or memory size in bytes using given locale.
/// @param nbytes   number of bytes to be formatted.
/// @param lc       locale to use for formatting.
/// @param si if @b true, format in SI units (1kB = 1000 bytes), else 1KiB = 1024 bytes.
/// @ingroup string_group
/// @since 0.7.0
__TAUEXPORT__ ustring str_bytes(uintmax_t nbytes, const std::locale & lc, bool si=false);

/// Format Unicode character value.
/// @ingroup string_group
/// @return smt. like "U+10fedc".
/// @since 0.7.0
__TAUEXPORT__ ustring str_unicode(char32_t wc);

/// Escape the special characters.
/// Escapes the special characters '\\b', '\\f', '\\n', '\\r', '\\t', '\\v', '\\'
/// and '"' in the string @a str by inserting a '\\' before
/// them. Additionally all characters in the range 0x01-0x1F (everything
/// below SPACE) and in the range 0x7F-0xFF (all non-ASCII chars) are
/// replaced with a '\\' followed by their octal representation.
/// Characters supplied in @a exceptions are not escaped.
/// @ingroup string_group
/// @since 0.6.0
/// @sa str_unescape()
__TAUEXPORT__ std::string str_escape(const std::string & str, const std::string & exceptions=std::string());

/// Unescape the special characters.
/// Unescapes characters previously escaped by str_escape() function.
/// @ingroup string_group
/// @since 0.6.0
/// @sa str_escape()
__TAUEXPORT__ std::string str_unescape(const std::string & str);

/// Get boolean value from the string.
/// @return @b true if string looks like "true yes y on" or contains non-zero numeric value.
/// @ingroup string_group
/// @since 0.7.0
__TAUEXPORT__ bool str_boolean(const ustring & s);

/// Convert string to title case.
/// Capitalize all first letters within given string.
/// @ingroup string_group
/// @since 0.7.0
__TAUEXPORT__ std::u32string str_title(const std::u32string & s, const Locale & lc=Locale());

/// Convert string to title case.
/// Capitalize all first letters within given string.
/// @ingroup string_group
/// @since 0.7.0
__TAUEXPORT__ ustring str_title(const ustring & s, const Locale & lc=Locale());

} // namespace tau

#endif // __TAU_STRING_HH__

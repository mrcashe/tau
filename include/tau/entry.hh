// ----------------------------------------------------------------------------
// SPDX-License-Identifier: BSD-2-Clause
// Copyright © 2014-2024 Konstantin Shmelkov <mrcashe@gmail.com>.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
// THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
// PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
// CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
// EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
// PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
// OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
// WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
// OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
// EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// ----------------------------------------------------------------------------

/// @file entry.hh The Entry (input line) class.

#ifndef __TAU_ENTRY_HH__
#define __TAU_ENTRY_HH__

#include <tau/enums.hh>
#include <tau/widget.hh>
#include <tau/ustring.hh>

namespace tau {

/// Single line text editor with decorations.
/// @note This class is a wrapper around its implementation shared pointer.
///
/// Has builtin handler of Widget::signal_focus_in() that calls select_all()
/// in case there nothing selected at the moment. To cancel this behaviour,
/// add your own handler for mentioned signal that calls unselect().
///
/// @ingroup widget_group
/// @ingroup input_group
/// @ingroup text_group
class __TAUEXPORT__ Entry: public Widget {
public:

    /// @name Constructors & Operators
    /// @{

    /// Default constructor.
    Entry(Border border_style=Border::INSET);

    /// Constructor with horizontal text alignment.
    Entry(Align text_align, Border border_style=Border::INSET);

    /// Constructor with text.
    Entry(const ustring & s, Border border_style=Border::INSET);

    /// Constructor with text.
    /// @since 0.5.0
    Entry(const std::u32string & ws, Border border_style=Border::INSET);

    /// Constructor with text and horizontal text alignment.
    Entry(const ustring & s, Align text_align, Border border_style=Border::INSET);

    /// Constructor with text and horizontal text alignment.
    /// @since 0.5.0
    Entry(const std::u32string & ws, Align text_align, Border border_style=Border::INSET);

    /// Copy constructor.
    ///
    /// @note This class is a wrapper around its implementation shared pointer,
    /// so copying it just increasing implementation pointer use count, but isn't
    /// really copies the object. The underlying implementation is not copyable.
    Entry(const Entry & other);

    /// Copy operator.
    ///
    /// @note This class is a wrapper around its implementation shared pointer,
    /// so copying it just increasing implementation pointer use count, but isn't
    /// really copies the object. The underlying implementation is not copyable.
    Entry & operator=(const Entry & other);

    /// Move constructor.
    ///
    /// @note This class is a wrapper around its implementation shared pointer,
    /// so copying it just increasing implementation pointer use count, but isn't
    /// really copies the object. The underlying implementation is not copyable.
    /// @note This constructor does not reset @c other implementation pointer.
    Entry(Entry && other);

    /// Move operator.
    ///
    /// @note This class is a wrapper around its implementation shared pointer,
    /// so copying it just increasing implementation pointer use count, but isn't
    /// really copies the object. The underlying implementation is not copyable.
    /// @note This operator does not reset @c other implementation pointer.
    Entry & operator=(Entry && other);

    /// Constructor with implementation pointer.
    ///
    /// @warning Unlike some other classes (Painter as an example), the whole
    /// @ref widget_stack "widget stack" is unable to run with pure implementation
    /// pointer, so attempting to construct widget from a pure (@b nullptr) pointer
    /// will cause throwing an user_error exception!
    /// That exception also will be thrown if user tries to construct the object
    /// from incompatible implementation shared pointer.
    ///
    /// @throw user_error in case of pure implementation pointer or incompatible
    /// implementation pointer class.
    /// @since 0.4.0
    Entry(Widget_ptr wp);

    /// Assign implementation.
    ///
    /// @throw user_error in case of pure implementation pointer or incompatible
    /// implementation pointer class.
    /// @since 0.4.0
    Entry & operator=(Widget_ptr wp);

    /// @}
    /// @name Controls
    /// @{

    /// Assign text from UTF-8 string.
    /// If @c s has any newline characters, the assignment will be truncated to the
    /// first occurence of that character.
    void assign(const ustring & s);

    /// Assign text from UTF-32 string.
    /// If @c ws has any newline characters, the assignment will be truncated to the
    /// first occurence of that character.
    /// @since 0.5.0
    void assign(const std::u32string & ws);

    /// Get text as UTF-8.
    ustring str() const;

    /// Get text as UTF-32.
    /// @since 0.5.0
    std::u32string wstr() const;

    /// Test if empty.
    bool empty() const noexcept;

    /// Clear input.
    void clear();

    /// Get text size in pixels.
    Size text_size(const ustring & s);

    /// Allow edit.
    void allow_edit();

    /// Disallow edit.
    void disallow_edit();

    /// Determines whether edit allowed.
    bool editable() const noexcept;

    /// Set border style.
    /// @since 0.4.0
    void set_border_style(Border bs);

    /// Get border style.
    /// @since 0.4.0
    Border border_style() const noexcept;

    /// Set horizontal text alignment.
    void text_align(Align align);

    /// Get horizontal text alignment.
    Align text_align() const noexcept;

    /// Set wrap mode.
    /// @since 0.5.0
    void wrap(Wrap wmode);

    /// Get wrap mode.
    /// @since 0.5.0
    Wrap wrap() const noexcept;

    /// Set caret position.
    void move_to(std::size_t col);

    /// Get caret position (column part only).
    std::size_t caret() const;

    /// @}
    /// @name Selection Control
    /// @{

    /// Select all.
    void select_all();

    /// Select text.
    void select(std::size_t begin, std::size_t end);

    /// Test if has selection.
    bool has_selection() const noexcept;

    /// Clear selection.
    void unselect();

    /// @}
    /// @name Additional Widgets Management
    /// @{

    /// Append widget after cycling widget.
    /// @throw user_error if widget already inserted into another container.
    void append(Widget & w, bool shrink=false);

    /// Append text after cycling widget.
    /// @param text text for append.
    /// @param margin_left left margin.
    /// @param margin_right right margin.
    /// @return pointer to the created Text widget.
    Widget_ptr append(const ustring & text, unsigned margin_left=0, unsigned margin_right=0);

    /// Append text after counting value.
    /// @param text         text to be appended.
    /// @param color        text color.
    /// @param margin_left  left margin.
    /// @param margin_right right margin.
    /// @return             pointer to the created Text widget.
    /// @since 0.6.0
    Widget_ptr append(const ustring & text, const Color & color, unsigned margin_left=0, unsigned margin_right=0);

    /// Prepend widget before cycling widget.
    /// @throw user_error if widget already inserted into another container.
    void prepend(Widget & w, bool shrink=false);

    /// Prepend text before cycling widget.
    /// @param text text for prepend.
    /// @param margin_left left margin.
    /// @param margin_right right margin.
    /// @return pointer to the created Text widget.
    Widget_ptr prepend(const ustring & text, unsigned margin_left=0, unsigned margin_right=0);

    /// Append text after counting value.
    /// @param text         text to be appended.
    /// @param color        text color.
    /// @param margin_left  left margin.
    /// @param margin_right right margin.
    /// @return             pointer to the created Text widget.
    /// @since 0.6.0
    Widget_ptr prepend(const ustring & text, const Color & color, unsigned margin_left=0, unsigned margin_right=0);

    /// @}
    /// @name Actions and Signals
    /// @{

    /// Gets "Cancel" action.
    Action & action_cancel();

    /// Gets "Activate" action.
    /// This action can be blocked by both signal_validate() or signal_approve().
    /// Action executes in two phase: first phase is data validation and second is signal_activate() emission.
    /// When signal_approve() connected, its @b false return value can block action execution.
    /// Otherwise, when signal_validate() returns @b false, the action execution will be also blocked.
    /// The internal handler connected to the %Action using Action::connect_before() call. That handler
    /// performs containing data validation. If you want to override that validation, insert your own
    /// handler before the default one. And if you want to add extra validation procedure, insert your
    /// handler after default one.
    /// @since 0.6.0
    /// @sa signal_validate()
    /// @sa signal_approve()
    Action & action_activate();

    /// Signal emitted when entered text changed.
    ///
    /// Slot prototype:
    /// ~~~~~~~~~~~~~~
    /// void on_changed();
    /// ~~~~~~~~~~~~~~
    signal<void()> & signal_changed();

    /// Signal emitted when user pressed "ENTER" or double clicks onto the %Entry.
    /// This signal emitted before action_activate() executes. Before emission
    /// validation performed and if validation result is negative, the signal
    /// will not be emitted.
    ///
    /// Slot prototype:
    /// ~~~~~~~~~~~~~~
    /// void on_activate(const std::u32string & text);
    /// ~~~~~~~~~~~~~~
    signal<void(const std::u32string &)> & signal_activate();

    /// Signal emitted when input pending.
    /// Return true from signal handler to decline changes.
    /// Slot prototype:
    /// ~~~~~~~~~~~~~~
    /// bool validator(const std::u32string & text);
    /// ~~~~~~~~~~~~~~
    /// @sa signal_approve()
    /// @sa action_activate()
    signal_all<const std::u32string &> & signal_validate();

    /// Signal emitted when %Entry is going to activate the input.
    /// Return true from signal handler to decline value.
    /// Slot prototype:
    /// ~~~~~~~~~~~~~~
    /// bool approve(const std::u32string & text);
    /// ~~~~~~~~~~~~~~
    /// @since 0.6.0
    /// @sa signal_validate()
    /// @sa action_activate()
    signal_all<const std::u32string &> & signal_approve();

    /// @}
};

} // namespace tau

#endif // __TAU_ENTRY_HH__

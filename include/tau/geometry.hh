// ----------------------------------------------------------------------------
// SPDX-License-Identifier: BSD-2-Clause
// Copyright © 2014-2024 Konstantin Shmelkov <mrcashe@gmail.com>.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
// THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
// PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
// CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
// EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
// PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
// OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
// WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
// OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
// EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// ----------------------------------------------------------------------------

#ifndef __TAU_GEOMETRY_HH__
#define __TAU_GEOMETRY_HH__

/// @file geometry.hh Vector, Point, Size, Rect classes and related stuff.

#include <tau/defs.hh>
#include <vector>

namespace tau {

/// The 2D/3D point with floating point coordinates.
/// @ingroup geometry_group
class __TAUEXPORT__ Vector {
public:

    /// Default constructor.
    Vector() = default;

    /// Copy cnstructor.
    Vector(const Vector & other) = default;

    /// Copy operator.
    Vector & operator=(const Vector & other) = default;

    /// 2D constructor.
    Vector(double x, double y) noexcept;

    /// 3D constructor.
    Vector(double x, double y, double z) noexcept;

    /// Get x coordinate.
    double x() const noexcept;

    /// Get y coordinate.
    double y() const noexcept;

    /// Get z coordinate.
    double z() const noexcept;

    /// Add other vector.
    void operator+=(const Vector & other) noexcept;

    /// Subtract other vector.
    void operator-=(const Vector & other) noexcept;

    /// Multiply by scale factor.
    void operator*=(double s) noexcept;

    /// Multiply by other vector.
    void operator*=(const Vector & other) noexcept;

    /// Divide by scale factor.
    void operator/=(double s);

    /// Divide by other vector.
    void operator/=(const Vector & other);

    /// Set x value.
    void x(double x) noexcept;

    /// Set y value.
    void y(double y) noexcept;

    /// Set z value.
    void z(double z) noexcept;

    /// Set x and y values.
    void set(double x, double y) noexcept;

    /// Set all values.
    void set(double x, double y, double z) noexcept;

    /// Set from Point.
    void set(const Point & pt) noexcept;

    /// Set from Size.
    void set(const Size & sz) noexcept;

    /// Reset coordinates to 0.0.
    void reset() noexcept;

    /// Returns the length of the vector.
    double length() const;

    /// Returns normalized vector.
    Vector normalized() const;

private:

    double x_ = 0.0;
    double y_ = 0.0;
    double z_ = 0.0;
};

/// @relates Vector
__TAUEXPORT__ Vector operator+(const Vector & lhs, const Vector & rhs) noexcept;

/// @relates Vector
__TAUEXPORT__ Vector operator-(const Vector & lhs, const Vector & rhs) noexcept;

/// @relates Vector
__TAUEXPORT__ Vector operator*(const Vector & lhs, const Vector & rhs) noexcept;

/// @relates Vector
__TAUEXPORT__ Vector operator*(const Vector & v, double s) noexcept;

/// @relates Vector
__TAUEXPORT__ Vector operator*(double s, const Vector & v) noexcept;

/// @relates Vector
__TAUEXPORT__ Vector operator/(const Vector & lhs, const Vector & rhs) noexcept;

/// @relates Vector
__TAUEXPORT__ Vector operator/(const Vector & lhs, double s) noexcept;

/// The 2D/3D point with integer coordinates.
/// @ingroup geometry_group
class __TAUEXPORT__ Point {
public:

    /// @name Constructors
    /// @{

    /// Default constructor.
    Point() = default;

    /// Copy constructor.
    Point(const Point & other) = default;

    /// Copy operator.
    Point & operator=(const Point & other) = default;

    /// 2D constructor.
    Point(int x, int y) noexcept;

    /// 3D constructor.
    Point(int x, int y, int z) noexcept;

    /// Constructor with Vector.
    Point(const Vector & vec) noexcept;

    /// Assignment from Vector.
    Point & operator=(const Vector & vec) noexcept;

    /// @}
    /// @name Modifying
    /// @{

    /// 2D setter.
    Point & set(int x, int y) noexcept;

    /// 3D setter.
    Point & set(int x, int y, int z) noexcept;

    /// Set values from other point.
    Point & set(const Point & pt) noexcept;

    /// Set X coordinate.
    void x(int xx);

    /// Set X coordinate.
    void y(int yy);

    /// Set X coordinate.
    void z(int zz);

    /// 2D updater.
    /// @return true if values changed.
    bool update(int x, int y) noexcept;

    /// 3D updater.
    /// @return true if values changed.
    bool update(int x, int y, int z) noexcept;

    /// Update from other point.
    /// @return true if values changed.
    bool update(const Point & py) noexcept;

    /// Update x coordinate.
    /// @return true if value changed.
    bool update_x(int x) noexcept;

    /// Update y coordinate.
    /// @return true if value changed.
    bool update_y(int y) noexcept;

    /// Update z coordinate.
    /// @return true if value changed.
    bool update_z(int z) noexcept;

    /// Move the point along the x and y axes.
    /// @return true if values changed.
    bool translate(int dx, int dy) noexcept;

    /// Move the point along the x, y and z axes.
    /// @return true if values changed.
    bool translate(int dx, int dy, int dz) noexcept;

    /// Move the point along the x and y axes.
    /// @return true if values changed.
    bool translate(const Point & other) noexcept;

    /// Reset coordinates to 0.
    void reset() noexcept;

    /// Transpose coordinates.
    Point & operator+=(const Point & p) noexcept;

    /// Transpose coordinates.
    Point & operator-=(const Point & p) noexcept;

    /// Scale coordinates.
    Point & operator*=(double m) noexcept;

    /// @}
    /// @name Accessors
    /// @{

    /// X coordinate accessor.
    int x() const noexcept;

    /// Y coordinate accessor.
    int y() const noexcept;

    /// Z coordinate accessor.
    int z() const noexcept;

    /// @}

private:

    int x_ = 0;
    int y_ = 0;
    int z_ = 0;
};

/// @relates Point
__TAUEXPORT__ Point operator*(const Point & p, double m) noexcept;

/// @relates Point
__TAUEXPORT__ Point operator*(double m, const Point & p) noexcept;

/// @relates Point
__TAUEXPORT__ Point operator+(const Point & p, const Point & q) noexcept;

/// @relates Point
__TAUEXPORT__ Point operator-(const Point & p, const Point & q) noexcept;

/// @relates Point
__TAUEXPORT__ Point operator-(const Point & p) noexcept;

/// @relates Point
__TAUEXPORT__ bool operator==(const Point & p, const Point & q) noexcept;

/// @relates Point
__TAUEXPORT__ bool operator!=(const Point & p, const Point & q) noexcept;

/// @relates Point
__TAUEXPORT__ bool operator<(const Point & p, const Point & q) noexcept;

/// @relates Point
__TAUEXPORT__ std::ostream & operator<<(std::ostream & os, const Point & pt);

/// The 2D/3D size with unsigned integer values.
/// @ingroup geometry_group
class __TAUEXPORT__ Size {
public:

    Size() = default;
    Size(const Size & other) = default;
    Size & operator=(const Size & other) = default;

    Size(unsigned width, unsigned height) noexcept;

    Size(unsigned width, unsigned height, unsigned depth) noexcept;

    Size(unsigned all) noexcept;

    /// Constructs from Vector.
    Size(const Vector & v) noexcept;

    void set(const Size & other) noexcept;

    void set(unsigned width, unsigned height) noexcept;

    void set(unsigned width, unsigned height, unsigned depth) noexcept;

    unsigned width() const noexcept;

    unsigned height() const noexcept;

    unsigned depth() const noexcept;

    int iwidth() const noexcept;

    int iheight() const noexcept;

    int idepth() const noexcept;

    /// Update width.
    /// @return @b true if changed.
    bool update_width(unsigned width) noexcept;

    /// Update height.
    /// @return @b true if changed.
    bool update_height(unsigned height) noexcept;

    /// Update depth.
    /// @return @b true if changed.
    bool update_depth(unsigned depth) noexcept;

    /// Update width, height and depth.
    bool update(unsigned all) noexcept;

    /// Update width and height.
    /// @return @b true if changed.
    bool update(unsigned width, unsigned height) noexcept;

    /// Update width, height and depth.
    /// @return @b true if changed.
    bool update(unsigned width, unsigned height, unsigned depth) noexcept;

    /// Update all.
    /// @param sz other size.
    /// @param nz update values only if new values not equals to 0.
    /// @return @b true if changed.
    bool update(const Size & sz, bool nz=false) noexcept;

    /// Update all.
    /// @return @b true if changed.
    bool update(const Vector & vec) noexcept;

    /// Update if given value greater.
    /// @return @b true if changed.
    bool update_max_width(unsigned width) noexcept;

    /// Update if given value greater.
    /// @return @b true if changed.
    bool update_max_height(unsigned height) noexcept;

    /// Update if given value greater.
    /// @return @b true if changed.
    bool update_max_depth(unsigned depth) noexcept;

    /// Update if given values greater.
    bool update_max(unsigned all) noexcept;

    /// Update width and height if given values greater.
    /// @return @b true if changed.
    bool update_max(unsigned width, unsigned height) noexcept;

    /// Update width, height and depth if given values greater.
    /// @return @b true if changed.
    bool update_max(unsigned width, unsigned height, unsigned depth) noexcept;

    /// Update all if given values greater.
    /// @param sz other size.
    /// @param nz update values only if new values not equals to 0.
    /// @return @b true if changed.
    bool update_max(const Size & sz, bool nz=false) noexcept;

    /// Update all if given values greater.
    /// @return @b true if changed.
    bool update_max(const Vector & vec) noexcept;

    /// Update width if given value less.
    /// @return @b true if changed.
    bool update_min_width(unsigned width) noexcept;

    /// Update height if given value less.
    /// @return @b true if changed.
    bool update_min_height(unsigned height) noexcept;

    /// Update depth if given value less.
    /// @return @b true if changed.
    bool update_min_depth(unsigned depth) noexcept;

    /// Update width, height and depth if given values less.
    bool update_min(unsigned all) noexcept;

    /// Update width and height if given values less.
    /// @return @b true if changed.
    bool update_min(unsigned width, unsigned height) noexcept;

    /// Update width, height and depth if given values less.
    /// @return @b true if changed.
    bool update_min(unsigned width, unsigned height, unsigned depth) noexcept;

    /// Update all if given value less.
    /// @param sz other size.
    /// @param nz update values only if new values not equals to 0.
    /// @return @b true if changed.
    bool update_min(const Size & sz, bool nz=false) noexcept;

    /// Update all if given value less.
    /// @return @b true if changed.
    bool update_min(const Vector & vec) noexcept;

    /// Increase 2D size.
    /// @return @b true if changed.
    bool increase(unsigned dx, unsigned dy) noexcept;

    /// Increase 3D size.
    /// @return @b true if changed.
    bool increase(unsigned dx, unsigned dy, unsigned dz) noexcept;

    /// Increase size.
    /// @return @b true if changed.
    bool increase(const Size & sz) noexcept;

    /// Decrease 2D size.
    /// @return @b true if changed.
    bool decrease(unsigned dx, unsigned dy) noexcept;

    /// Decrease 3D size.
    /// @return @b true if changed.
    bool decrease(unsigned dx, unsigned dy, unsigned dz) noexcept;

    /// Decrease size.
    /// @return @b true if changed.
    bool decrease(const Size & sz) noexcept;

    /// Grow 2D size.
    /// @return @b true if changed.
    bool grow(int dx, int dy) noexcept;

    /// Grow 3D size.
    /// @return @b true if changed.
    bool grow(int dx, int dy, int dz) noexcept;

    /// Make increased size.
    Size increased(unsigned dx, unsigned dy) const noexcept;

    /// Make increased size.
    Size increased(unsigned dx, unsigned dy, unsigned dz) const noexcept;

    /// Make increased size.
    Size increased(const Size & sz) const noexcept;

    /// Make decreased size.
    Size decreased(unsigned dx, unsigned dy) const noexcept;

    /// Make decreased size.
    Size decreased(unsigned dx, unsigned dy, unsigned dz) const noexcept;

    /// Make decreased size.
    Size decreased(const Size & sz) const noexcept;

    /// Make grown size.
    Size grown(int dx, int dy) const noexcept;

    /// Make grown size.
    Size grown(int dx, int dy, int dz) const noexcept;

    /// Apply aspect ratio.
    /// @param asp      aspect ratio.
    /// @param expand   if @b true, expand of width:height values, otherwise shrink to minimal one.
    /// @return @b true if modified.
    /// @since 0.7.0
    bool apply_aspect_ratio(double asp, bool expand=false) noexcept;

    /// 2D check empty.
    bool empty() const noexcept;

    /// 3D check empty.
    bool empty3() const noexcept;

    /// Reset values to 0.
    void reset() noexcept;

    /// Return minimal of width(), height().
    unsigned min() const noexcept;

    /// Return maximal of width(), height().
    unsigned max() const noexcept;

    /// Return minimal of width(), height(), depth().
    unsigned min3() const noexcept;

    /// Return maximal of width(), height(), depth().
    unsigned max3() const noexcept;

    /// Test if not empty.
    operator bool() const noexcept;

    /// Add other Size.
    Size & operator+=(const Size & size) noexcept;

    /// Subtract other Size.
    Size & operator-=(const Size & size) noexcept;

    Size & operator|=(const Size & sz) noexcept;

    Size & operator&=(const Size & sz) noexcept;

    Size & operator*=(double m) noexcept;

private:

    unsigned width_  = 0;
    unsigned height_ = 0;
    unsigned depth_  = 0;
};

/// @relates Size
__TAUEXPORT__ bool operator==(const Size & s, const Size & t) noexcept;

/// @relates Size
__TAUEXPORT__ bool operator!=(const Size & s, const Size & t) noexcept;

/// @relates Size
__TAUEXPORT__ Size operator+(const Size & lhs, const Size & rhs) noexcept;

/// @relates Size
__TAUEXPORT__ Size operator|(const Size & lhs, const Size & rhs) noexcept;

/// @relates Size
__TAUEXPORT__ Size operator&(const Size & lhs, const Size & rhs) noexcept;

/// @relates Size
__TAUEXPORT__ Size operator-(const Size & lhs, const Size & rhs) noexcept;

/// @relates Point
__TAUEXPORT__ Point operator+(const Point & p, const Size & sz) noexcept;

/// @relates Point
__TAUEXPORT__ Point operator+(const Size & sz, const Point & p) noexcept;

/// @relates Point
__TAUEXPORT__ Point operator-(const Point & p, const Size & sz) noexcept;

/// @relates Size
__TAUEXPORT__ std::ostream & operator<<(std::ostream & os, const Size & z);

/// The rectangle with integer coordinates.
/// @ingroup geometry_group
class __TAUEXPORT__ Rect {
public:

    Rect() = default;

    /// Constructor with coordinates and size.
    Rect(int x, int y, const Size & size) noexcept;

    /// Constructor with coordinates and size.
    Rect(const Point & org, const Size & size) noexcept;

    /// Constructor with coordinates.
    Rect(int x1, int y1, int x2, int y2) noexcept;

    /// Constructor with coordinates.
    Rect(const Point & a, const Point & b) noexcept;

    /// Constructor with Size and coordinates at (0, 0).
    Rect(const Size & size) noexcept;

    /// Constructor with width and height and coordinates at (0, 0).
    Rect(unsigned width, unsigned height) noexcept;

    /// Get left coordinate.
    int left() const noexcept;

    /// Get right coordinate.
    int right() const noexcept;

    /// Get top coordinate.
    int top() const noexcept;

    /// Get bottom coordinate.
    int bottom() const noexcept;

    /// Get size.
    Size size() const noexcept;

    /// Get width.
    unsigned width() const noexcept;

    /// Get height.
    unsigned height() const noexcept;

    /// Get signed width.
    int iwidth() const noexcept;

    /// Get signed height.
    int iheight() const noexcept;

    /// Assign new value.
    void set(const Rect & r) noexcept;

    /// Assign new value.
    void set(const Point & org, const Size & size) noexcept;

    /// Assign new value.
    void set(int left, int top, const Size & size) noexcept;

    /// Assign new value.
    void set(const Size & size) noexcept;

    /// Assign new value.
    void set(int left, int top, int right, int bottom) noexcept;

    /// Assign new value.
    void set(const Point & a, const Point & b) noexcept;

    /// Reset origin and size.
    void reset() noexcept;

    /// Test if empty.
    bool empty() const noexcept;

    /// Get top left point.
    Point top_left() const noexcept;

    /// Get top right point.
    Point top_right() const noexcept;

    /// Get bottom left point.
    Point bottom_left() const noexcept;

    /// Get bottom right point.
    Point bottom_right() const noexcept;

    /// Get center point.
    Point center() const noexcept;

    void center_to(int x, int y) noexcept;
    void center_to(const Point & pt) noexcept;
    Rect moved(const Point & p) const noexcept;
    Rect moved(int x, int y) const noexcept;
    bool contains(int x, int y) const noexcept;
    bool contains(const Point & p) const noexcept;
    bool contains(const Rect & r) const noexcept;

    /// Intersect with other rectangle.
    void intersect(const Rect & other) noexcept;

    /// Unite with other rectangle.
    void unite(const Rect & other) noexcept;

    /// Make intersection between this rectangle and other one.
    Rect intersected(const Rect & other) const noexcept;

    /// Make union of two rectangles.
    Rect united(const Rect & other) const noexcept;

    void move_to(const Point & p) noexcept;

    /// Assign new origin.
    void move_to(int x, int y) noexcept;

    /// Move relatively.
    void translate(const Point & p) noexcept;

    /// Move relatively.
    void translate(int dx, int dy) noexcept;

    /// Move relatively.
    void translate(const Size & sz) noexcept;

    Rect translated(const Point & p) const noexcept;

    Rect translated(const Size & sz) const noexcept;

    Rect translated(int x, int y) const noexcept;

    /// Resize.
    void resize(unsigned width, unsigned height) noexcept;

    /// Resize.
    void resize(const Size & size) noexcept;

    bool update_left(int x) noexcept;

    bool update_top(int y) noexcept;

    bool update_width(unsigned width) noexcept;

    bool update_height(unsigned height) noexcept;

    bool update_origin(const Point & pt) noexcept;

    bool update_origin(int x, int y) noexcept;

    bool update_size(const Size & sz) noexcept;

    bool update_size(unsigned width, unsigned height) noexcept;

    /// Increase size.
    void increase(unsigned dx, unsigned dy) noexcept;

    /// Increase size.
    void increase(const Size & sz) noexcept;

    /// Decrease size.
    void decrease(unsigned dx, unsigned dy) noexcept;

    /// Decrease size.
    void decrease(const Size & sz) noexcept;

    /// Make increased rectangle.
    Rect increased(unsigned dx, unsigned dy) const noexcept;

    /// Make increased rectangle.
    Rect increased(const Size & sz) const noexcept;

    /// Make decreased rectangle.
    Rect decreased(unsigned dx, unsigned dy) const noexcept;

    /// Make decreased rectangle.
    Rect decreased(const Size & sz) const noexcept;

    /// Grow size up or down.
    void grow(int dx, int dy) noexcept;

    /// Make grown rectangle.
    Rect grown(int dx, int dy) const noexcept;

    /// Assign new value.
    Rect & operator=(const Rect & r) noexcept;

    /// Unite with other rectangle.
    void operator|=(const Rect & other) noexcept;

    /// Intersect with other rectangle.
    void operator&=(const Rect & other) noexcept;

    /// Unite with size.
    void operator|=(const Size & size) noexcept;

    /// Test if not empty.
    operator bool() const noexcept;

private:

    Point org_;
    Size  sz_;
};

/// @relates Rect
__TAUEXPORT__ Rect operator|(const Rect & r1, const Rect & r2) noexcept;

/// @relates Rect
__TAUEXPORT__ Rect operator&(const Rect & r1, const Rect & r2) noexcept;

/// @relates Rect
__TAUEXPORT__ bool operator==(const Rect & r1, const Rect & r2) noexcept;

/// @relates Rect
__TAUEXPORT__ bool operator!=(const Rect & r1, const Rect & r2) noexcept;

/// @relates Rect
__TAUEXPORT__ std::ostream & operator<<(std::ostream & os, const Rect & r);

/// A Bezier curve.
/// @ingroup geometry_group
class __TAUEXPORT__ Curve {
public:

    /// @name Constructors
    /// @{

    Curve(const Vector & end);
    Curve(const Vector & cp1, const Vector & end);
    Curve(const Vector & cp1, const Vector & cp2, const Vector & end);
    Curve(const Curve & other) = default;
    Curve & operator=(const Curve & other) = default;
    Curve(Curve && other) = default;
    Curve & operator=(Curve && other) = default;

    /// @}
    /// Gets curve order.
    /// @return 0 if empty, 1 if linear, 2 if conic and 3 if cubic.
    unsigned order() const noexcept;

    /// Gets end point.
    Vector end() const;

    /// Gets control point (for conic curves).
    Vector cp() const;

    /// Gets control point 1 (for conic and cubic curves).
    Vector cp1() const;

    /// Gets control point 2 (for cubic curves).
    Vector cp2() const;

    /// Assign value.
    void assign(const Vector & end);

    /// Assign value.
    void assign(const Vector & cp1, const Vector & end);

    /// Assign value.
    void assign(const Vector & cp1, const Vector & cp2, const Vector & end);

    /// Set control point 1.
    void set_cp1(const Vector & v);

    /// Set control point 2.
    void set_cp2(const Vector & v);

    /// Transform by Matrix.
    void transform(const Matrix & mat);

private:

    unsigned    order_;
    Vector      cp1_;
    Vector      cp2_;
    Vector      end_;
};

/// @relates Curve
__TAUEXPORT__ Curve & operator*=(Curve & cv, const Matrix & mat);

/// @relates Curve
__TAUEXPORT__ Curve operator*(const Curve & cv, const Matrix & mat);

/// Transformation matrix.
/// TODO Matrix multiply
/// @ingroup geometry_group
class __TAUEXPORT__ Matrix {
public:

    /// @name Contructors
    /// @{

    /// Constructor with transformation parameters.
    Matrix(double xx=1.0, double yy=1.0, double xy=0.0, double yx=0.0, double x0=0.0, double y0=0.0);

    /// Copy constructor.
    Matrix(const Matrix & other) = default;

    /// Copy operator.
    Matrix & operator=(const Matrix & other) = default;

    /// @}
    /// @name Accessors
    /// @{

    /// Get parameter.
    double xx() const noexcept;

    /// Get parameter.
    double yy() const noexcept;

    /// Get parameter.
    double xy() const noexcept;

    /// Get parameter.
    double yx() const noexcept;

    /// Get parameter.
    double x0() const noexcept;

    /// Get parameter.
    double y0() const noexcept;

    /// Get maximal scale factor.
    double max_factor() const noexcept;

    /// Calculate determinant.
    double det() const;

    /// Get angle of rotation.
    /// @return angle in radians.
    /// @since 0.7.0
    double angle() const noexcept;

    /// @}
    /// @name Tests
    /// @{

    /// Test if equal.
    bool operator==(const Matrix & other) const noexcept;

    /// Test if not equal.
    bool operator!=(const Matrix & other) const noexcept;

    /// Test if has unity scale.
    bool unity() const noexcept;

    /// Test if identity.
    bool identity() const noexcept;

    /// @}
    /// @name Modifiers
    /// @{

    /// Reset to identity.
    void reset();

    /// Translate by coordinates.
    void translate(double dx, double dy);

    /// Translate by coordinates.
    void translate(const Vector & v);

    /// Skew.
    void skew(double horiz, double vert);

    /// Rotate.
    void rotate(double radians);

    /// Scale.
    void scale(double sx, double sy);

    /// Scale.
    void scale(double s);

    /// @}
    /// @name Transform
    /// @{

    /// Get inverted matrix.
    /// If it impossible to get inverted matrix, returns itself.
    Matrix inverted() const;

    /// Get translated matrix.
    /// @since 0.7.0
    Matrix translated(const Vector & v) const;

    /// Get translated matrix.
    /// @since 0.7.0
    Matrix translated(double dx, double dy) const;

    /// Transform coordinates.
    Vector mul(const Vector & vec) const;

    /// Transform distance.
    Vector distance(double dx, double dy) const;

    /// Transform distance.
    Vector distance(const Vector & vec) const;

    /// @}

private:

    double xx_;
    double yy_;
    double xy_;
    double yx_;
    double x0_;
    double y0_;
};

/// Scale Vector by Matrix.
/// @ingroup geometry_group
/// @relates Vector
__TAUEXPORT__ Vector operator*(const Vector & vec, const Matrix & mat);

/// Scale Vector by Matrix.
/// @ingroup geometry_group
/// @relates Vector
__TAUEXPORT__ Vector operator*(const Matrix & mat, const Vector & vec);

/// Scale Vector by Matrix.
/// @ingroup geometry_group
/// @relates Vector
__TAUEXPORT__ Vector & operator*=(Vector & vec, const Matrix & mat);

/// %List of Bezier curves and a starting point.
///
/// @ingroup geometry_group
class __TAUEXPORT__ Contour {
public:

    using list_type = std::vector<Curve>;
    using iterator = list_type::iterator;
    using const_iterator = list_type::const_iterator;

    /// @name Constructors
    /// @{

    /// Default constructor.
    /// @param start the start point.
    Contour(const Vector & start=Vector());

    /// Constructs contour with single line segment.
    /// @since 0.7.0
    Contour(const Vector & start, const Vector & end);

    /// Constructs contour with single conic curve.
    /// @since 0.7.0
    Contour(const Vector & start, const Vector & cp, const Vector & end);

    /// Constructs contour with single cubic curve.
    /// @since 0.7.0
    Contour(const Vector & start, const Vector & cp1, const Vector & cp2, const Vector & end);

    // Copy constructor.
    Contour(const Contour & other) = default;

    // Constructor with transform.
    Contour(const Contour & other, const Matrix & mat);

    // Move constructor.
    Contour(Contour && other);

    /// Copy operator.
    Contour & operator=(const Contour & other) = default;

    /// Move operator.
    Contour & operator=(Contour && other);

    /// @}

    /// Add rectangle path.
    /// @since 0.7.0
    Contour & rectangle(const Vector & a, const Vector & b, double radius=0.0);

    /// Add arc path.
    /// @since 0.7.0
    Contour & arc(const Vector & center, double radius, double angle1, double angle2, const Matrix & mat=Matrix(), double tolerance=1e-5);

    /// Get starting point.
    Vector start() const;

    /// Get access to curves.
    list_type & curves();

    /// Get access to curves.
    const list_type & curves() const;

    /// Get curve by index.
    Curve & operator[](std::size_t index);

    /// Get curve by index.
    const Curve & operator[](std::size_t index) const;

    /// Count curves.
    std::size_t size() const noexcept;

    /// Test if empty.
    bool empty() const noexcept;

    /// Add a 1st order curve.
    void line_to(const Vector & end);

    /// Add a 1st order curve.
    void line_to(double x, double y);

    /// Add quadratic curve.
    /// @param cp control point coordinates.
    /// @param end end point coordinates.
    void conic_to(const Vector & cp, const Vector & end);

    /// Add quadratic curve.
    /// @param cx control point x coordinate.
    /// @param cy control point y coordinate.
    /// @param ex end point x coordinate.
    /// @param ey end point y coordinate.
    void conic_to(double cx, double cy, double ex, double ey);

    /// Add cubic curve.
    /// @param cp1 control point 1 coordinates.
    /// @param cp2 control point 2 coordinates.
    /// @param end end point coordinates.
    void cubic_to(const Vector & cp1, const Vector & cp2, const Vector & end);

    /// Add cubic curve.
    /// @param cx1 control point 1 x coordinate.
    /// @param cy1 control point 1 y coordinate.
    /// @param cx2 control point 2 x coordinate.
    /// @param cy2 control point 2 y coordinate.
    /// @param ex end point x coordinate.
    /// @param ey end point y coordinate.
    void cubic_to(double cx1, double cy1, double cx2, double cy2, double ex, double ey);

    /// Revert contour.
    void revert();

    /// Transform contour by matrix.
    void transform(const Matrix & mat);

    /// Get begin iterator.
    iterator begin();

    /// Get begin constant iterator.
    const_iterator begin() const;

    /// Get end iterator.
    iterator end();

    /// Get end constant iterator.
    const_iterator end() const;

    /// Get maximal order value.
    /// @return 0 if contour is empty or in range 1...3.
    unsigned order() const;

    /// Merge two contours.
    /// @since 0.7.0
    Contour & merge(const Contour & ctr);

private:

    Vector start_;
    std::vector<Curve> curves_;
};

/// @relates Contour
__TAUEXPORT__ Contour & operator*=(Contour & ctr, const Matrix & mat);

/// Represents Widget span within Table, List, List_text.
/// It may be also table's or list's span.
///
/// @ingroup geometry_group
struct __TAUEXPORT__ Span {

    /// Minimal column index.
    int     xmin = INT_MAX;

    /// Minimal row index.
    int     ymin = INT_MAX;

    /// Maximal column index outside of specified range.
    int     xmax = INT_MIN;

    /// Maximal row index outside of specified range.
    int     ymax = INT_MIN;

    /// Default constructor.
    Span() noexcept = default;

    /// Constructor with parameters.
    Span(int x1, int y1, int x2, int y2) noexcept: xmin(x1), ymin(y1), xmax(x2), ymax(y2) {}
};

/// Span compare operator.
/// @relates Span
__TAUEXPORT__ bool operator==(const Span & s1, const Span & s2) noexcept;

/// Span compare operator.
/// @relates Span
__TAUEXPORT__ bool operator!=(const Span & s1, const Span & s2) noexcept;

/// Represents Widget margins.
///
/// @ingroup geometry_group
struct __TAUEXPORT__ Margin {
    unsigned    left    = 0;
    unsigned    right   = 0;
    unsigned    top     = 0;
    unsigned    bottom  = 0;
};

/// Margin compare operator.
/// @relates Margin
__TAUEXPORT__ bool operator==(const Margin & m1, const Margin & m2) noexcept;

/// Margin compare operator.
/// @relates Margin
__TAUEXPORT__ bool operator!=(const Margin & m1, const Margin & m2) noexcept;

} // namespace tau

#endif // __TAU_GEOMETRY_HH__

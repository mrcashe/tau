// ----------------------------------------------------------------------------
// SPDX-License-Identifier: BSD-2-Clause
// Copyright © 2014-2024 Konstantin Shmelkov <mrcashe@gmail.com>.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
// THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
// PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
// CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
// EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
// PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
// OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
// WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
// OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
// EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// ----------------------------------------------------------------------------

/// @file notebook.hh The Notebook class definition.

#ifndef __TAU_NOTEBOOK_HH__
#define __TAU_NOTEBOOK_HH__

#include <tau/enums.hh>
#include <tau/container.hh>

namespace tau {

/// %Container which shows one of its children at a time, in tabbed windows.
///
/// @note This class is a wrapper around its implementation shared pointer.
///
/// The %Notebook widget is a Container whose children are pages that can be
/// switched between using tab labels along one gravity.
/// The gravity at which the tabs for switching pages in the notebook are drawn
/// is set in the class constructor.
/// @ingroup compound_container_group
class __TAUEXPORT__ Notebook: public Container {
public:

    /// @name Constructors & Operators
    /// @{

    /// Default constructor.
    /// @param tab_pos the tab position.
    Notebook(Side tab_pos=Side::TOP);

    /// Copy constructor.
    ///
    /// @note This class is a wrapper around its implementation shared pointer,
    /// so copying it just increasing implementation pointer use count, but isn't
    /// really copies the object. The underlying implementation is not copyable.
    Notebook(const Notebook & other);

    /// Copy operator.
    ///
    /// @note This class is a wrapper around its implementation shared pointer,
    /// so copying it just increasing implementation pointer use count, but isn't
    /// really copies the object. The underlying implementation is not copyable.
    Notebook & operator=(const Notebook & other);

    /// Move constructor.
    ///
    /// @note This class is a wrapper around its implementation shared pointer,
    /// so copying it just increasing implementation pointer use count, but isn't
    /// really copies the object. The underlying implementation is not copyable.
    /// @note This constructor does not reset @c other implementation pointer.
    Notebook(Notebook && other);

    /// Move operator.
    ///
    /// @note This class is a wrapper around its implementation shared pointer,
    /// so copying it just increasing implementation pointer use count, but isn't
    /// really copies the object. The underlying implementation is not copyable.
    /// @note This operator does not reset @c other implementation pointer.
    Notebook & operator=(Notebook && other);

    /// Constructor with implementation pointer.
    ///
    /// @warning Unlike some other classes (Painter as an example), the whole
    /// @ref widget_stack "widget stack" is unable to run with pure implementation
    /// pointer, so attempting to construct widget from a pure (@b nullptr) pointer
    /// will cause throwing an user_error exception!
    /// That exception also will be thrown if user tries to construct the object
    /// from incompatible implementation shared pointer.
    ///
    /// @throw user_error in case of pure implementation pointer or incompatible
    /// implementation pointer class.
    /// @since 0.4.0
    Notebook(Widget_ptr wp);

    /// Assign implementation.
    ///
    /// @throw user_error in case of pure implementation pointer or incompatible
    /// implementation pointer class.
    /// @since 0.4.0
    Notebook & operator=(Widget_ptr wp);

    /// @}
    /// @name Widget insertion/movement/removal
    /// @{

    /// Appends a page to notebook.
    /// @param w the widget for append.
    /// @return a number of added page.
    /// @throw user_error if widget already inserted into another container.
    int append_page(Widget & w);

    /// Appends a page to notebook.
    /// @overload
    /// @param w the widget for append.
    /// @param title the title to be used in tab.
    /// @return a number of added page.
    /// @throw user_error if widget already inserted into another container.
    int append_page(Widget & w, const ustring & title);

    /// Appends a page to notebook.
    /// @overload
    /// @param w the widget for append.
    /// @param title the title to be used in tab.
    /// @param align the title alignment.
    /// @return a number of added page.
    /// @throw user_error if widget already inserted into another container.
    /// @since 0.5.0
    int append_page(Widget & w, const ustring & title, Align align);

    /// Appends a page to notebook.
    /// @overload
    /// @param w the widget for append.
    /// @param title_widget the widget to be drawn in the tab.
    /// @return a number of added page.
    /// @throw user_error if any widget already inserted into another container.
    int append_page(Widget & w, Widget & title_widget);

    /// Prepends a page to notebook.
    /// @param w the widget for prepend.
    /// @throw user_error if widget already inserted into another container.
    void prepend_page(Widget & w);

    /// Prepends a page to notebook.
    /// @overload
    /// @param w the widget for prepend.
    /// @param title the title to be used in tab.
    /// @throw user_error if widget already inserted into another container.
    void prepend_page(Widget & w, const ustring & title);

    /// Prepends a page to notebook.
    /// @overload
    /// @param w the widget for prepend.
    /// @param title the title to be used in tab.
    /// @param align the title alignment.
    /// @throw user_error if widget already inserted into another container.
    /// @since 0.5.0
    void prepend_page(Widget & w, const ustring & title, Align align);

    /// Prepends a page to notebook.
    /// @overload
    /// @param w the widget for prepend.
    /// @param title_widget the widget to be drawn in the tab.
    /// @throw user_error if any widget already inserted into another container.
    void prepend_page(Widget & w, Widget & title_widget);

    /// Inserts a page to notebook.
    /// @param w the widget to be inserted.
    /// @param nth_page the page number for the new page.
    /// @return a number of added page.
    /// @throw user_error if widget already inserted into another container.
    int insert_page(Widget & w, int nth_page);

    /// Inserts a page to notebook.
    /// @overload
    /// @param w the widget to be inserted.
    /// @param nth_page the page number for the new page.
    /// @param title the title to be used in tab.
    /// @return a number of added page.
    /// @throw user_error if widget already inserted into another container.
    int insert_page(Widget & w, int nth_page, const ustring & title);

    /// Inserts a page to notebook.
    /// @overload
    /// @param w the widget to be inserted.
    /// @param nth_page the page number for the new page.
    /// @param title the title to be used in tab.
    /// @param align the title alignment.
    /// @return a number of added page.
    /// @throw user_error if widget already inserted into another container.
    /// @since 0.5.0
    int insert_page(Widget & w, int nth_page, const ustring & title, Align align);

    /// Inserts a page to notebook.
    /// @overload
    /// @param w the widget to be inserted.
    /// @param nth_page the page number for the new page.
    /// @param title_widget the widget to be drawn in the tab.
    /// @return a number of added page.
    /// @throw user_error if any widget already inserted into another container.
    int insert_page(Widget & w, int nth_page, Widget & title_widget);

    /// Inserts a page to notebook.
    /// @param w the widget to be inserted.
    /// @param after_this the new widget will be inserted after this widget.
    /// @return a number of added page.
    /// @throw user_error if widget already inserted into another container.
    int insert_page_after(Widget & w, Widget & after_this);

    /// Inserts a page to notebook.
    /// @overload
    /// @param w the widget to be inserted.
    /// @param after_this the new widget will be inserted after this widget.
    /// @param title the title to be used in tab.
    /// @return a number of added page.
    /// @throw user_error if widget already inserted into another container.
    int insert_page_after(Widget & w, Widget & after_this, const ustring & title);

    /// Inserts a page to notebook.
    /// @overload
    /// @param w the widget to be inserted.
    /// @param after_this the new widget will be inserted after this widget.
    /// @param title the title to be used in tab.
    /// @param align the title alignment.
    /// @return a number of added page.
    /// @throw user_error if widget already inserted into another container.
    /// @since 0.5.0
    int insert_page_after(Widget & w, Widget & after_this, const ustring & title, Align align);

    /// Inserts a page to notebook.
    /// @overload
    /// @param w the widget to be inserted.
    /// @param after_this the new widget will be inserted after this widget.
    /// @param title_widget the widget to be drawn in the tab.
    /// @return a number of added page.
    /// @throw user_error if any widget already inserted into another container.
    int insert_page_after(Widget & w, Widget & after_this, Widget & title_widget);

    /// Inserts a page to notebook.
    /// @param w the widget to be inserted.
    /// @param before_this the new widget will be inserted before this widget.
    /// @return a number of added page.
    /// @throw user_error if widget already inserted into another container.
    int insert_page_before(Widget & w, Widget & before_this);

    /// Inserts a page to notebook.
    /// @overload
    /// @param w the widget to be inserted.
    /// @param before_this the new widget will be inserted before this widget.
    /// @param title the title to be used in tab.
    /// @return a number of added page.
    /// @throw user_error if widget already inserted into another container.
    int insert_page_before(Widget & w, Widget & before_this, const ustring & title);

    /// Inserts a page to notebook.
    /// @overload
    /// @param w the widget to be inserted.
    /// @param before_this the new widget will be inserted before this widget.
    /// @param title the title to be used in tab.
    /// @param align the title alignment.
    /// @return a number of added page.
    /// @throw user_error if widget already inserted into another container.
    /// @since 0.5.0
    int insert_page_before(Widget & w, Widget & before_this, const ustring & title, Align align);

    /// Inserts a page to notebook.
    /// @overload
    /// @param w the widget to be inserted.
    /// @param before_this the new widget will be inserted before this widget.
    /// @param title_widget the widget to be drawn in the tab.
    /// @return a number of added page.
    /// @throw user_error if any widget already inserted into another container.
    int insert_page_before(Widget & w, Widget & before_this, Widget & title_widget);

    /// Removes a page from notebook.
    /// You can specify any widget be it title widget or main widget or those child.
    /// @return a number of deleted page or -1 if page not found.
    int remove_page(const Widget & w);

    /// Removes a page from notebook.
    /// @overload
    /// @return a number of deleted page or -1 if page not found.
    int remove_page(int nth_page);

    /// Remove all pages.
    void clear();

    /// Remove children from main and tab box.
    /// @sa append_widget()
    /// @sa prepend_widget()
    /// @sa append_tab()
    /// @sa prepend_tab()
    /// @since 0.7.0
    void remove(Widget & w);

    /// @}
    /// @name Page Management
    /// @{

    /// Get page count.
    std::size_t page_count() const noexcept;

    /// Test if empty.
    bool empty() const noexcept;

    /// Get current page number.
    /// @return -1 if empty or current page number.
    int current_page() const noexcept;

    /// Finds the index of the page which contains the given child widget.
    /// You can specify any widget be it title widget or main widget or those child.
    /// @return -1 if not found or page number.
    int page_number(const Widget & w) const noexcept;

    /// Append widget into main box.
    /// @param w        Widget to be inserted.
    /// @param shrink   Shrink widget to fit.
    /// @sa prepend_widget()
    /// @sa append_tab()
    /// @sa prepend_tab()
    /// @sa remove()
    /// @since 0.7.0
    void append_widget(Widget & w, bool shrink=false);

    /// Prepend widget into main box.
    /// @param w        Widget to be inserted.
    /// @param shrink   Shrink widget to fit.
    /// @sa append_widget()
    /// @sa append_tab()
    /// @sa prepend_tab()
    /// @sa remove()
    /// @since 0.7.0
    void prepend_widget(Widget & w, bool shrink=false);

    /// Get widget at specified page.
    /// Gets implementation pointer of the widget inserted into @c page.
    /// @param page page number.
    /// @return an implementation pointer or pure smart pointer if @c page value is invaid.
    /// @since 0.6.0
    Widget_ptr widget_at(int page);

    /// Get widget at specified page.
    /// Gets implementation pointer of the widget inserted into @c page.
    /// @param page page number.
    /// @return an implementation pointer or pure smart pointer if @c page value is invaid.
    /// @since 0.6.0
    Widget_cptr widget_at(int page) const;

    /// Show specified page.
    /// @param nth_page     page number to be shown.
    /// @param take_focus   if @b true, shows @a nth_page and calls Widget::take_focus() on containing widget.
    /// @return             page number of the shown page or @b INT_MIN if no such page found.
    int show_page(int nth_page, bool take_focus=false);

    /// Show specified page.
    /// @param w            main or tab widget to be shown.
    /// @param take_focus   if @b true, shows @a nth_page and calls Widget::take_focus() on containing widget.
    /// @return             page number of the shown page or @b INT_MIN if no such widget found.
    /// @since 0.7.0
    int show_page(const Widget & w, bool take_focus=false);

    /// Show next page.
    /// @return page number of the shown page.
    /// @sa allow_rollover().
    /// @sa disallow_rollover().
    /// @sa rollover_allowed().
    int show_next();

    /// Show previous page.
    /// @return page number of the shown page.
    /// @sa allow_rollover().
    /// @sa disallow_rollover().
    /// @sa rollover_allowed().
    int show_previous();

    /// Reorders the page containing w, so that it appears in page nth_page.
    /// @sa allow_reorder().
    /// @sa disallow_reorder().
    /// @sa reorder_allowed().
    void reorder_page(Widget & w, int nth_page);

    /// Reorders the page old_page, so that it appears in page new_page.
    /// @overload
    /// @sa allow_reorder().
    /// @sa disallow_reorder().
    /// @sa reorder_allowed().
    void reorder_page(int old_page, int new_page);

    /// @}
    /// @name Tab Management
    /// @{

    /// Get tab widget at specified page.
    /// Gets implementation pointer of the tab widget for @c page.
    /// @param page page number.
    /// @return an implementation pointer or pure smart pointer if @c page value is invaid.
    /// @since 0.6.0
    Widget_ptr tab_at(int page);

    /// Get tab widget at specified page.
    /// Gets implementation pointer of the tab widget for @c page.
    /// @param page page number.
    /// @return an implementation pointer or pure smart pointer if @c page value is invaid.
    /// @since 0.6.0
    Widget_cptr tab_at(int page) const;

    /// Append widget into tab box.
    /// @param w        Widget to be inserted.
    /// @param shrink   Shrink widget to fit.
    /// @sa prepend_tab()
    /// @sa append_widget()
    /// @sa prepend_widget()
    /// @sa remove()
    /// @since 0.7.0
    void append_tab(Widget & w, bool shrink=false);

    /// Prepend widget into tab box.
    /// @param w        Widget to be inserted.
    /// @param shrink   Shrink widget to fit.
    /// @sa append_tab()
    /// @sa append_widget()
    /// @sa prepend_widget()
    /// @sa remove()
    /// @since 0.7.0
    void prepend_tab(Widget & w, bool shrink=false);

    /// Allow page reordering by user using mouse.
    /// @sa disallow_reorder().
    /// @sa reorder_allowed().
    /// @note allowed by default.
    void allow_reorder();

    /// Disallow page reordering by user using mouse.
    /// @sa allow_reorder().
    /// @sa reorder_allowed().
    /// @note allowed by default.
    void disallow_reorder();

    /// Determines whether reordering enabled.
    /// @sa allow_reorder().
    /// @sa disallow_reorder().
    /// @note allowed by default.
    bool reorder_allowed() const noexcept;

    /// Allow tabs rollover.
    /// When allowed:
    /// When show_next() or show_previous() invoked and current tab is first or last,
    /// the next current page will be last or first, correspondingly.
    /// When disallowed, show_next() and show_previous() will do nothing on first and
    /// last pages.
    /// @note allowed by default.
    /// @sa show_next().
    /// @sa show_previous().
    /// @sa disallow_rollover().
    /// @sa rollover_allowed().
    void allow_rollover();

    /// Disallow tabs rollover.
    /// @note allowed by default.
    /// @sa show_next().
    /// @sa show_previous().
    /// @sa allow_rollover().
    /// @sa rollover_allowed().
    void disallow_rollover();

    /// Test if rollover allowed.
    /// @sa show_next().
    /// @sa show_previous().
    /// @sa allow_rollover().
    /// @sa disallow_rollover().
    /// @note allowed by default.
    bool rollover_allowed() const noexcept;

    /// Shows tabs.
    /// @note Tabs are visible by default.
    void show_tabs();

    /// Hide tabs.
    /// @note Tabs are visible by default.
    void hide_tabs();

    /// Determines if tabs visible.
    bool tabs_visible() const noexcept;

    /// Set homogeneous tabs.
    void set_homogeneous_tabs();

    /// Unset homogeneous tabs.
    void unset_homogeneous_tabs();

    /// Test if tabs are homogeneous.
    bool homogeneous_tabs() const noexcept;

    /// @}
    /// @name Internal Frame control
    /// @{

    /// Set border width.
    /// @param px border width in pixels.
    /// @param radius border radius in pixels.
    void set_border(unsigned px, int radius=0);

    /// Set border width and style.
    /// @param px border width in pixels.
    /// @param bs the border style.
    /// @param radius border radius in pixels.
    void set_border(unsigned px, Border bs, int radius=0);

    /// Set border width, style and color.
    /// @param px border width in pixels.
    /// @param bs the border style.
    /// @param color the color.
    /// @param radius border radius in pixels.
    void set_border(unsigned px, Border bs, const Color & color, int radius=0);

    /// Set border style.
    /// @param bs the border style.
    void set_border_style(Border bs);

    /// Get border style.
    Border border_style() const noexcept;

    /// Set all borders color.
    /// @param color the color.
    void set_border_color(const Color & color);

    /// Set default border color.
    void unset_border_color();

    /// Get border color.
    Color border_color() const noexcept;

    /// Set border radius.
    void set_border_radius(int radius);

    /// Get border radius.
    int border_radius() const noexcept;

    /// @}
    /// @name Signals
    /// @{

    /// Signal emitted when page added.
    /// Slot prototype:
    /// ~~~~~~~~~~~~~~
    /// void on_page_added(int new_page);
    /// ~~~~~~~~~~~~~~
    signal<void(int)> & signal_page_added();

    /// Signal emitted when page removed.
    /// Slot prototype:
    /// ~~~~~~~~~~~~~~
    /// void on_page_removed(int removed_page);
    /// ~~~~~~~~~~~~~~
    signal<void(int)> & signal_page_removed();

    /// Signal emitted when current page changed.
    /// Slot prototype:
    /// ~~~~~~~~~~~~~~
    /// void on_page_changed(int pg_number);
    /// ~~~~~~~~~~~~~~
    signal<void(int)> & signal_page_changed();

    /// Signal emitted when page reordered.
    /// Slot prototype:
    /// ~~~~~~~~~~~~~~
    /// void on_page_reordered(int new_page, int old_page);
    /// ~~~~~~~~~~~~~~
    signal<void(int, int)> & signal_page_reordered();

    /// @}
};

} // namespace tau

#endif // __TAU_NOTEBOOK_HH__

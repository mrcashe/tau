// ----------------------------------------------------------------------------
// SPDX-License-Identifier: BSD-2-Clause
// Copyright © 2014-2024 Konstantin Shmelkov <mrcashe@gmail.com>.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
// THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
// PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
// CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
// EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
// PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
// OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
// WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
// OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
// EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// ----------------------------------------------------------------------------

/// @file signal.hh Signals and Slots

#ifndef __TAU_SIGNAL_HH__
#define __TAU_SIGNAL_HH__

#include <tau/defs.hh>
#include <functional>
#include <list>
#include <tuple>
#include <vector>

namespace tau {

template <typename R, typename... Args> class fun_functor;
template <class Class, typename R, typename... Args> class mem_functor;
template <typename R, typename... Args> class slot;
template <typename R, typename... Args> class signal_T;
template <typename R, typename... Args> class signal;
template <typename... Args> class signal_all;
template <typename R, typename... Args> struct slot_impl_T;
template <class Functor, typename R, typename... Args> struct slot_impl_F;

class  __TAUEXPORT__ trackable;
class  __TAUEXPORT__ slot_base;
class  __TAUEXPORT__ signal_base;
class  __TAUEXPORT__ connection;
struct __TAUEXPORT__ slot_impl;
using  slot_ptr = std::shared_ptr<slot_impl>;

/// Functor base, abstract.
/// @ingroup signal_group
class functor_base {
protected:

    functor_base() = default;

public:

    virtual ~functor_base() = default;
    virtual bool empty() const noexcept = 0;
    virtual void reset() = 0;
    virtual trackable * target() noexcept = 0;
};

/// Functor for static functions and std::function<T> objects.
/// @ingroup signal_group
template<typename R, typename... Args>
class fun_functor<R(Args...)>: public functor_base {
    using Function = std::function<R(Args...)>;
    Function    fun_;

public:

    using Result = R;

    fun_functor(const Function & fn):
        fun_(fn)
    {
    }

    R operator()(Args... args) const {
        return fun_ ? fun_(args...) : R();
    }

    bool empty() const noexcept override {
        return !fun_;
    }

    trackable * target() noexcept override {
        return nullptr;
    }

    void reset() override {
        fun_ = nullptr;
    }
};

/// Functor that holds pointer to the object of some class and pointer to the member function of that class.
/// @ingroup signal_group
template <class Class, typename R, typename... Args>
class mem_functor<Class, R(Args...)>: public functor_base {
    using Function = std::function<R(Class *, Args...)>;

    Class *     obj_;
    Function    fun_;

public:

    using Result = R;

    mem_functor(Class * obj, const Function & fun):
        obj_(obj),
        fun_(fun)
    {
    }

    R operator()(Args... args) const {
        return fun_ ? fun_(obj_, args...) : R();
    }

    void reset() override {
        fun_ = nullptr;
        obj_ = nullptr;
    }

    trackable * target() noexcept override {
        if constexpr (std::is_base_of_v<trackable, Class>) {
            return obj_;
        }

        else {
            return nullptr;
        }
    }

    bool empty() const noexcept override {
        return !fun_;
    }
};

/// The functor that holds one of below functor and bound parameters.
/// @ingroup signal_group
/// @sa fun_functor
/// @sa mem_functor
/// @sa void_functor
template <class Functor, bool front, typename... Bound>
class bind_functor: public functor_base {
    Functor                 functor_;
    std::tuple<Bound...>    tuple_;


public:

    using Result = typename Functor::Result;

    bind_functor(const Functor & functor, Bound... bounds):
        functor_(functor),
        tuple_(bounds...)
    {
    }

    template <typename... Args>
    Result operator()(Args... args) const {
        if constexpr(front) {
            return std::apply(functor_, std::tuple_cat(tuple_, std::make_tuple(args...)));
        }

        else {
            return std::apply(functor_, std::tuple_cat(std::make_tuple(args...), tuple_));
        }
    }

    bool empty() const noexcept override {
        return functor_.empty();
    }

    trackable * target() noexcept override {
        return functor_.target();
    }

    void reset() override {
        functor_.reset();
    }
};

/// The functor that returns specified value.
/// @ingroup signal_group
/// @sa fun_functor
/// @sa mem_functor
/// @sa bind_functor
/// @sa void_functor
template <class Functor, typename R>
class return_functor: public functor_base {
    Functor functor_;
    R       result_;

public:

    using Result = R;

    return_functor(const Functor & functor, R result):
        functor_(functor),
        result_(result)
    {
    }

    template <typename... Args>
    R operator()(Args... args) const {
        functor_(args...);
        return result_;
    }

    bool empty() const noexcept override {
        return functor_.empty();
    }

    trackable * target() noexcept override {
        return functor_.target();
    }

    void reset() override {
        functor_.reset();
    }
};

/// The functor that always returns void value.
/// @ingroup signal_group
/// @sa fun_functor
/// @sa mem_functor
/// @sa bind_functor
template <class Functor>
class void_functor: public functor_base {
    using R = void;
    Functor functor_;

public:

    using Result = void;

    void_functor(const Functor & functor):
        functor_(functor)
    {
    }

    template <typename... Args>
    void operator()(Args... args) const {
        functor_(args...);
    }

    bool empty() const noexcept override {
        return functor_.empty();
    }

    trackable * target() noexcept override {
        return functor_.target();
    }

    void reset() override {
        functor_.reset();
    }
};

/// Basic functionality for abstract %slot.
/// @ingroup signal_group
class slot_base: public functor_base {
    friend slot_impl;
    friend signal_base;

    void disconnect();
    void link(signal_base * signal) noexcept;

protected:

    /// @private
    slot_ptr      impl_;

    /// @private
    signal_base * signal_ = nullptr;

protected:

    /// @private
    void track();

    /// @private
    void untrack();

    /// @private
    slot_base();

    /// @private
   ~slot_base();

public:

    /// Test if empty.
    bool empty() const noexcept override;

    /// Reset.
    void reset() override;
    trackable * target() noexcept override;

    /// @name Connection Management
    /// @{

    /// Get connection.
    connection cx();

    /// @}
};

/// An object that tracks signal->slot connections automatically.
/// @ingroup signal_group
class trackable {
    mutable std::vector<slot_ptr> tracks_;

    friend slot_base;

    void track(slot_ptr sp) const;
    void untrack(slot_ptr sp) const;

protected:

    /// Default constructor.
    trackable();

public:

    /// Destructor.
    /// Disconnects all connected slots.
    ~trackable();

    /// Copy constructor.
    /// This constructor does not copy slots from src.
    trackable(const trackable & src);

    /// Move constructor.
    /// This constructor does not copy slots from src.
    trackable(trackable && src);

    /// Copy operator.
    /// This operator does not copy slots from src.
    trackable & operator=(const trackable & src);

    /// Move operator.
    /// This operator does not copy slots from src.
    trackable & operator=(trackable && src);
};

// Slot implementation base, untyped.
/// @ingroup signal_group
struct slot_impl {
    /// @private
    virtual ~slot_impl();

    /// @private
    slot_impl(slot_base * base);

    /// @private
    slot_impl(const slot_impl & other) = delete;

    /// @private
    slot_impl(slot_impl && other) = delete;

    /// @private
    slot_impl & operator=(const slot_impl & other) = delete;

    /// @private
    slot_impl & operator=(slot_impl && other) = delete;

    /// @private
    virtual slot_ptr clone(slot_base * base) const = 0;

    /// @private
    virtual void reset() = 0;

    /// @private
    virtual trackable * target() noexcept = 0;

    /// @private
    virtual bool empty() const noexcept = 0;

    /// @private
    bool blocked() const noexcept;

    /// @private
    void block() noexcept;

    /// @private
    void unblock() noexcept;

    /// @private
    void link(slot_base * base) noexcept;

    /// @private
    void disconnect();

    /// @private
    slot_base * base_ = nullptr;

    /// @private
    unsigned    blocked_ = 0;
};

// Typed slot_impl.
/// @ingroup signal_group
template <typename R, typename... Args>
struct slot_impl_T<R(Args...)>: slot_impl {
    /// @private
    slot_impl_T(slot_base * slot):
        slot_impl(slot)
    {
    }

    /// @private
    virtual R operator()(Args... args) = 0;
};

// Typed slot_impl with functor.
/// @ingroup signal_group
template <class Functor, typename R, typename... Args>
struct slot_impl_F<Functor, R(Args...)>: slot_impl_T<R(Args...)> {
    /// @private
    using self_type = slot_impl_F<Functor, R(Args...)>;

    /// @private
    Functor functor_;

    /// @private
    slot_impl_F(const Functor & functor, slot_base * base):
        slot_impl_T<R(Args...)>(base),
        functor_(functor)
    {
    }

    /// @private
    R operator()(Args... args) override {
        return !this->blocked() ? functor_(args...) : R();
    }

    /// @private
    slot_ptr clone(slot_base * base) const override {
        return std::make_shared<self_type>(functor_, base);
    }

    /// @private
    void reset() override {
        functor_.reset();
    }

    /// @private
    trackable * target() noexcept override {
        return functor_.target();
    }

    /// @private
    bool empty() const noexcept override {
        return functor_.empty();
    }
};

/// @brief Typed %slot.
/// @ingroup signal_group
template <typename R, typename... Args>
class slot<R(Args...)>: public slot_base {
    friend class signal<R(Args...)>;
    friend class signal_all<Args...>;

public:

    using Result = R;

    /// @name Constructors & Operators
    /// @{

    /// Default constructor.
    slot() = default;

    /// Constructor with functor.
    template <class Functor>
    slot(const Functor & functor) {
        using impl_type = slot_impl_F<Functor, R(Args...)>;
        impl_ = std::make_shared<impl_type>(functor, this);
        track();
    }

    /// Copy constructor.
    slot(const slot & other) {
        if (other.impl_) {
            impl_ = other.impl_->clone(this);
            track();
        }
    }

    /// Copy operator.
    slot & operator=(const slot & other) {
        if (this != &other) {
            untrack();
            impl_.reset();

            if (other.impl_) {
                impl_ = other.impl_->clone(this);
                track();
            }
        }

        return *this;
    }

    /// Move constructor.
    slot(slot && other) {
        impl_ = other.impl_;
        if (impl_) { impl_->link(this); }
        other.impl_.reset();
    }

    /// Move operator.
    slot & operator=(slot && other) {
        untrack();
        impl_ = other.impl_;
        if (impl_) { impl_->link(this); }
        other.impl_.reset();
        return *this;
    }

    /// @}
    /// @name Control
    /// @{

    /// Test if empty.
    operator bool() const noexcept {
        return impl_ && !impl_->empty();
    }

    /// @}
    /// @name Functor Calling
    /// @{

    /// Call holding functor.
    R operator()(Args... args) const {
        using impl_type = slot_impl_T<R(Args...)>;
        return impl_ ? static_cast<impl_type &>(*impl_)(args...) : R();
    }

    /// @}
};

/// @brief Holds a %connection between the %signal and %slot.
/// The %connection class uniquely represents the connection between a particular %signal and a particular %slot.
/// The empty() method checks if the %signal and %slot are still connected,
/// and the drop() method disconnects the %signal and %slot if they are connected before it is called.
///
/// @sa @ref connection_guide
///
/// Note that, the %connection object is an only way to disconnect %slot from the %signal. This implementation
/// of signals and slots does not provide signal::disconnect() or signal::clear() facilities.
///
/// @sa @ref connection_drop_example
///
///
/// The @b autodrop property used to drop %connection on destruction or assignment. This can be useful
/// to create a scoped connection.
///
/// @sa @ref connection_autodrop_example
///
/// The %slot has accumulated block counter, after calling a block() method, the same number of calls
/// to the unblock() method must follow.
///
/// @sa @ref connection_block_example
/// @ingroup signal_group
class connection {
public:

    /// Default constructor.
    /// @param autodrop if @b true, call drop() while destruction or assignment.
    explicit connection(bool autodrop=false);

    /// Copy constructor.
    /// The @a autodrop property value will be copied from the @a other %connection.
    connection(const connection & other);

    /// Move constructor.
    /// The @a autodrop property value will be copied from the @a other %connection.
    /// The @a autodrop property value of the @a other %connection not changed.
    connection(connection && other);

    /// Assignment operator.
    /// Calls drop() if @a autodrop property is set before actual copying.
    /// The @a autodrop property value will be @b true if it set in this or @a other %connection.
    connection & operator=(const connection & other);

    /// Move operator.
    /// Calls drop() if @a autodrop property is set before actual copying.
    /// The @a autodrop property value will be @b true if it set in this or @a other %connection.
    /// The @a autodrop property value of the @a other %connection set to @b false.
    /// The holding slot of the @a other %connection resetted.
    connection & operator=(connection && other);

    /// Destructor.
    /// Calls drop() if @a autodrop property set.
   ~connection();

    /// Disconnect connected %slot from the %signal.
    /// @note This call does not affect @a autodrop property.
    void drop();

    /// Enable autodrop.
    /// @note @a autodrop property disabled by default.
    void set_autodrop() noexcept;

    /// Disable autodrop.
    /// @note @a autodrop property disabled by default.
    void unset_autodrop() noexcept;

    /// Test if @a autodrop property set.
    /// @see set_autodrop()
    /// @see unset_autodrop()
    bool autodrop() const noexcept;

    /// Test if %connection has been blocked by calling block() method.
    /// By default, the %connection is not blocked.
    /// @see block()
    /// @see unblock()
    bool blocked() const noexcept;

    /// Block the %connection.
    /// If %connection has been blocked, the %signal emition does not happens.
    /// Such functionality can be useful in the event that it is necessary
    /// to temporarily prevent a particular slot from being called.
    /// By default, the %connection is not blocked.
    /// @see unblock()
    void block() noexcept;

    /// Unblock the %connection.
    /// By default, the %connection is not blocked.
    /// @see block()
    void unblock() noexcept;

    /// Test if empty.
    /// The %connection is empty when constructed using default constructor
    /// or after drop() has been called.
    /// @see block()
    /// @see unblock()
    bool empty() const noexcept;

private:

    slot_ptr     slot_;
    mutable bool autodrop_ = false;
    friend class slot_base;
    explicit connection(slot_ptr slot);
};

/// Basic functionality for abstract %signal.
/// @ingroup signal_group
class signal_base: public trackable {
    friend class slot_base;
    virtual void erase(slot_base * slot) = 0;

protected:

    /// @private
    signal_base();

    /// @private
    virtual ~signal_base();

    /// @private
    connection link(slot_base & slot);

public:

    /// Get slot count.
    virtual std::size_t size() const noexcept = 0;

    /// Test if empty.
    virtual bool empty() const noexcept = 0;
};

/// Typed signal.
/// @ingroup signal_group
template<typename R, typename... Args>
class signal_T<R(Args...)>: public signal_base {
protected:

    /// @private
    using slot_type = slot<R(Args...)>;

    /// @private
    using impl_type = slot_impl_T<R(Args...)>;

    /// @private
    using ptr_type  = std::shared_ptr<impl_type>;

    /// @private
    inline constexpr static std::size_t STACK_MAX = (64*1024U)/sizeof(ptr_type);

    /// @private
    std::list<slot_type>    slots_;

    /// @private
    void erase(slot_base * s) override {
        slots_.remove(*static_cast<slot_type *>(s));
    }

public:

    /// @name Connections
    /// @{

    /// Connect slot using %slot's copy constructor.
    /// @param slot the %slot to be connected.
    /// @param prepend pass @b true to push front %slot and @b false to push back.
    /// @return the connection object holding information about %connection.
    connection connect(const slot_type & slot, bool prepend=false) {
        if (!prepend) {
            slots_.emplace_back(slot);
            return link(slots_.back());
        }

        else {
            slots_.emplace_front(slot);
            return link(slots_.front());
        }
    }

    /// Connect slot using %slot's move constructor.
    /// @param slot the %slot to be connected.
    /// @param prepend pass @b true to push front %slot and @b false to push back.
    /// @return the connection object holding information about %connection.
    connection connect(slot_type && slot, bool prepend=false) {
        if (!prepend) {
            slots_.emplace_back(std::move(slot));
            return link(slots_.back());
        }

        else {
            slots_.emplace_front(std::move(slot));
            return link(slots_.front());
        }
    }

    /// @{
    /// @name Controls
    /// @{

    /// Merge signals.
    void operator+=(const signal_T & other) {
        if (this != &other) {
            for (auto & slot: other.slots_) { slots_.emplace_back(slot); }
        }
    }

    /// Test if empty.
    bool empty() const noexcept override {
        return slots_.empty();
    }

    /// Count slots connected.
    /// @return connected slot count.
    std::size_t size() const noexcept override {
        return slots_.size();
    }

    /// @}
};

/// @brief Signal implementing @b EACH or @b ANY emission scheme.
/// Depending on the return value, two schemes available:
/// - When return value is @b void, the scheme is @ref emission_each
/// - When return value is something that casts to @b bool, the scheme is @ref emission_any
///
/// @sa @ref emission_each
/// @sa @ref emission_any
/// @ingroup signal_group
template<typename R, typename... Args>
class signal<R(Args...)>: public signal_T<R(Args...)> {
    using impl_type = typename signal_T<R(Args...)>::impl_type;
    using ptr_type = typename signal_T<R(Args...)>::ptr_type;

public:

    /// @name Emission

    /// Emit the signal.
    R operator()(Args... args) const {
        std::size_t n = std::min(this->size(), this->STACK_MAX);

        if (n) {
            // TODO Use std::vector<> instead of VLA.
            ptr_type tmp[n], * d = tmp, * p = tmp;

            for (auto i = this->slots_.begin(); n; ++i, --n) { *(d++) = std::static_pointer_cast<impl_type>(i->impl_); }

            if constexpr(std::is_same_v<R, void>) {
                for (; p != d; ++p) {
                    (*p)->operator()(args...);
                }
            }

            else {
                for (; p != d; ++p) {
                    R any = (*p)->operator()(args...);
                    if (any) { return any; }
                }
            }
        }

        return R();
    }
};

/// @brief Signal implementing @ref emission_all emission scheme.
/// This scheme suitable for validation purposes.
/// The type of return value of the %signal is always @b bool.
/// When all connected slots return @b true, the result
/// is also @b true. When any slot returns @b false,
/// the result is @b false. When no slots connected,
/// the result is @b true.
///
/// @sa @ref emission_all
/// @ingroup signal_group
template<typename... Args>
class signal_all: public signal_T<bool(Args...)> {
    using impl_type = typename signal_T<bool(Args...)>::impl_type;
    using ptr_type = typename signal_T<bool(Args...)>::ptr_type;

public:

    /// @name Emission

    /// Emit the signal.
    bool operator()(Args... args) const {
        std::size_t n = std::min(this->size(), this->STACK_MAX), nn = n;

        if (n) {
            // TODO Use std::vector<> instead of VLA.
            ptr_type tmp[n], * d = tmp;
            for (auto i = this->slots_.begin(); nn; ++i, --nn) { *(d++) = std::static_pointer_cast<impl_type>(i->impl_); }

            for (ptr_type * p = tmp; n--; ++p) {
                if (!(*p)->operator()(args...)) {
                    return false;
                }
            }
        }

        return true;
    }
};

/// @brief Make a functor from pointer to a static function.
/// @ingroup signal_group
template <typename R, typename... Args>
inline decltype(auto)
fun(R (*fn)(Args...)) {
    return fun_functor<R(Args...)>(fn);
}

/// @brief Make a functor from [std::function[(https://en.cppreference.com/w/cpp/utility/functional/function) object.
/// @ingroup signal_group
template <typename R, typename... Args>
inline decltype(auto)
fun(const std::function<R(Args...)> & fn) {
    return fun_functor<R(Args...)>(fn);
}

/// @brief Create functor from pointer to object of some class and method of that class.
/// @ingroup signal_group
template <typename R, typename Obj, class Class, typename... Args>
inline decltype(auto)
fun(Obj * obj, R (Class::*fun)(Args...)) {
    return mem_functor<Obj, R(Args...)>(obj, fun);
}

/// @brief Create functor from shared pointer to object of some class and method of that class.
/// @ingroup signal_group
template <typename R, typename Obj, class Class, typename... Args>
inline decltype(auto)
fun(std::shared_ptr<Obj> obj_ptr, R (Class::*fun)(Args...)) {
    return mem_functor<Obj, R(Args...)>(obj_ptr.get(), fun);
}

/// @brief Create functor from link to object of some class and method of that class.
/// @ingroup signal_group
template <typename R, typename Obj, class Class, typename... Args>
inline decltype(auto)
fun(Obj & obj, R (Class::*fun)(Args...)) {
    return mem_functor<Obj, R(Args...)>(&obj, fun);
}

/// @brief Create functor from pointer to object of some class and const-qualified method of that class.
/// @ingroup signal_group
template <typename R, typename Obj, class Class, typename... Args>
inline decltype(auto)
fun(Obj * obj, R (Class::*fun)(Args...) const) {
    return mem_functor<Obj, R(Args...)>(obj, fun);
}

/// @brief Create functor from shared pointer to object of some class and const-qualified method of that class.
/// @ingroup signal_group
template <typename R, typename Obj, class Class, typename... Args>
inline decltype(auto)
fun(std::shared_ptr<Obj> obj_ptr, R (Class::*fun)(Args...) const) {
    return mem_functor<Obj, R(Args...)>(obj_ptr.get(), fun);
}

/// @brief Create functor from link to object of some class and const-qualified method of that class.
/// @ingroup signal_group
template <typename R, typename Obj, class Class, typename... Args>
inline decltype(auto)
fun(Obj & obj, R (Class::*fun)(Args...) const) {
    return mem_functor<Obj, R(Args...)>(&obj, fun);
}

/// @brief Create functor from pointer to object of some class and volatile-qualified method of that class.
/// @ingroup signal_group
template <typename R, typename Obj, class Class, typename... Args>
inline decltype(auto)
fun(Obj * obj, R (Class::*fun)(Args...) volatile) {
    return mem_functor<Obj, R(Args...)>(obj, fun);
}

/// @brief Create functor from shared pointer to object of some class and volatile-qualified method of that class.
/// @ingroup signal_group
template <typename R, typename Obj, class Class, typename... Args>
inline decltype(auto)
fun(std::shared_ptr<Obj> obj_ptr, R (Class::*fun)(Args...) volatile) {
    return mem_functor<Obj, R(Args...)>(obj_ptr.get(), fun);
}

/// @brief Create functor from link to object of some class and volatile-qualified method of that class.
/// @ingroup signal_group
template <typename R, typename Obj, class Class, typename... Args>
inline decltype(auto)
fun(Obj & obj, R (Class::*fun)(Args...) volatile) {
    return mem_functor<Obj, R(Args...)>(&obj, fun);
}

/// @brief Create functor from pointer to object of some class and const-volatile-qualified method of that class.
/// @ingroup signal_group
template <typename R, typename Obj, class Class, typename... Args>
inline decltype(auto)
fun(Obj * obj, R (Class::*fun)(Args...) const volatile) {
    return mem_functor<Obj, R(Args...)>(obj, fun);
}

/// @brief Create functor from shared pointer to object of some class and const-volatile-qualified method of that class.
/// @ingroup signal_group
template <typename R, typename Obj, class Class, typename... Args>
inline decltype(auto)
fun(std::shared_ptr<Obj> obj_ptr, R (Class::*fun)(Args...) const volatile) {
    return mem_functor<Obj, R(Args...)>(obj_ptr.get(), fun);
}

/// @brief Create functor from link to object of some class and const-volatile-qualified method of that class.
/// @ingroup signal_group
template <typename R, typename Obj, class Class, typename... Args>
inline decltype(auto)
fun(Obj & obj, R (Class::*fun)(Args...) const volatile) {
    return mem_functor<Obj, R(Args...)>(&obj, fun);
}

/// @brief Create functor from a reference to the signal.
/// @ingroup signal_group
template <typename R, typename... Args>
inline decltype(auto)
fun(signal<R(Args...)> & sig) {
    return mem_functor<signal<R(Args...)>, R(Args...)>(&sig, &signal<R(Args...)>::operator());
}

/// @brief Create functor from a pointer to the signal.
/// @ingroup signal_group
template <typename R, typename... Args>
inline decltype(auto)
fun(signal<R(Args...)> * sig) {
    return mem_functor<signal<R(Args...)>, R(Args...)>(sig, &signal<R(Args...)>::operator());
}

/// @brief Create functor from a reference to the signal_all.
/// @ingroup signal_group
template <typename... Args>
inline decltype(auto)
fun(signal_all<Args...> & sig) {
    return mem_functor<signal_all<Args...>, bool(Args...)>(&sig, &signal_all<Args...>::operator());
}

/// @brief Create functor from a pointer to the signal_all.
/// @ingroup signal_group
template <typename... Args>
inline decltype(auto)
fun(signal_all<Args...> * sig) {
    return mem_functor<signal_all<Args...>, bool(Args...)>(sig, &signal_all<Args...>::operator());
}

/// @brief Create functor with front bound parameters.
/// @ingroup signal_group
template <class Functor, typename... Bound>
inline decltype(auto)
bind_front(const Functor & functor, Bound... bounds) {
    return bind_functor<Functor, true, Bound...>(functor, bounds...);
}

/// @brief Create functor with back bound parameters.
/// @ingroup signal_group
template <class Functor, typename... Bound>
inline decltype(auto)
bind_back(const Functor & functor, Bound... bounds) {
    return bind_functor<Functor, false, Bound...>(functor, bounds...);
}

/// @brief Create functor with specified result.
/// @ingroup signal_group
template <class Functor, typename R>
inline decltype(auto)
bind_return(const Functor & functor, R result) {
    return return_functor<Functor, R>(functor, result);
}

/// @brief Create functor with void result type.
/// @ingroup signal_group
template <class Functor>
inline decltype(auto)
bind_void(const Functor & functor) {
    return void_functor<Functor>(functor);
}

} // namespace tau

#endif // __TAU_SIGNAL_HH__

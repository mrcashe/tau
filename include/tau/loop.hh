// ----------------------------------------------------------------------------
// SPDX-License-Identifier: BSD-2-Clause
// Copyright © 2014-2024 Konstantin Shmelkov <mrcashe@gmail.com>.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
// THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
// PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
// CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
// EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
// PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
// OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
// WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
// OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
// EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// ----------------------------------------------------------------------------

/// @file loop.hh The Loop class.

#ifndef __TAU_LOOP_HH__
#define __TAU_LOOP_HH__

#include <tau/signal.hh>
#include <tau/defs.hh>

namespace tau {

/// The event loop.
/// @note This class is a wrapper around its implementation shared pointer.
/// @ingroup sys_group
class __TAUEXPORT__ Loop: public trackable {
public:

    /// @name Public Types
    /// @{

    /// Slot type used for loop iteration.
    /// @sa Signal_iterate
    /// @sa signal_iterate()
    /// @since 0.6.0
    using Slot_iterate = slot<void()>;

    /// Signal type used for custom loop iteration function install.
    /// @sa Slot_iterate
    /// @sa signal_iterate()
    /// @since 0.6.0
    using Signal_iterate = signal<bool(const Slot_iterate &)>;

    /// @}
    /// @name Constructors & Operators
    /// @{

    /// @brief Default constructor creates a pure %Loop.
    /// This constructor intended for futher assignment.
    ///
    /// Example:
    /// @code
    /// struct A {
    ///     // Uses this constructor, the loop is pure now.
    ///     tau::Loop   lp_;
    ///
    ///     // Assign correct value.
    ///     A(): lp_(tau::Loop::this_loop()) {}
    /// };
    /// @endcode
    Loop();

    /// @brief Destructor.
   ~Loop();

    /// @brief Copy constructor.
    /// @note This class is a wrapper around its implementation shared pointer,
    /// so copying it just increasing implementation pointer use count, but isn't
    /// really copies the object. The underlying implementation is not copyable.
    Loop(const Loop & other) = default;

    /// @brief Copy operator.
    /// @note This class is a wrapper around its implementation shared pointer,
    /// so copying it just increasing implementation pointer use count, but isn't
    /// really copies the object. The underlying implementation is not copyable.
    Loop & operator=(const Loop & other) = default;

    /// Constructor with implementation pointer.
    Loop(Loop_ptr lp);

    /// @brief Creates loop for the current thread.
    /// @since 0.6.0
    static Loop this_loop();

    /// Determines if loop pure.
    /// @return @b false if loop does not have implementation pointer set.
    /// @since 0.6.0
    operator bool() const noexcept;

    /// Get implementation pointer.
    /// @since 0.6.0
    Loop_ptr ptr();

    /// Get implementation pointer.
    /// @since 0.6.0
    Loop_cptr ptr() const;

    /// @}
    /// @name Controls
    /// @{

    /// @brief Run loop.
    /// @warning This function can be called only once per loop lifecycle!
    /// @note If loop is pure, this method called on the this_loop().
    /// @sa signal_iterate()
    void run();

    /// @brief Quit loop.
    /// @note If loop is pure, this method called on the this_loop().
    void quit();

    /// @brief Test if alive.
    /// @note If loop is pure, this method called on the this_loop().
    bool alive() const noexcept;

    /// @brief Test if running.
    /// @note If loop is pure, this method called on the this_loop().
    bool running() const noexcept;

    /// @brief Get unique loop id.
    /// @note If loop is pure, this method called on the this_loop().
    int id() const noexcept;

    /// @brief Get running loop count.
    /// @since 0.7.0
    static std::size_t count() noexcept;

    /// @brief List running loops.
    /// @since 0.7.0
    static std::vector<Loop> list();

    /// @brief List mount points.
    /// @note If loop is pure, this method called on the this_loop().
    std::vector<ustring> mounts() const;

    /// Create alarm.
    /// @note If loop is pure, this method called on the this_loop().
    connection alarm(slot<void()> slot_alarm, int timeout_ms, bool periodical=false);

    /// @}
    /// @name Events
    /// @{

    /// @brief Create Event.
    /// @note If loop is pure, this method called on the this_loop().
    /// @since 0.6.0
    Event event(slot<void()> slot_ready=slot<void()>());

    /// @}
    /// @name Signals
    /// @{

    /// Signal emitted when first instance of run() method start its work.
    /// Slot prototype:
    /// ~~~~~~~~~~~~~~~
    /// void on_start();
    /// ~~~~~~~~~~~~~~~
    /// @note If loop is pure, this method called on the this_loop().
    signal<void()> & signal_start();

    /// Signal emitted when there are no events happened during last idle timeout.
    /// Slot prototype:
    /// ~~~~~~~~~~~~~~~
    /// void on_idle();
    /// ~~~~~~~~~~~~~~~
    /// @note If loop is pure, this method called on the this_loop().
    signal<void()> & signal_idle();

    /// Signal emitted when first instance of run() method quits.
    /// Slot prototype:
    /// ~~~~~~~~~~~~~~~
    /// void on_quit();
    /// ~~~~~~~~~~~~~~~
    /// @note If loop is pure, this method called on the this_loop().
    signal<void()> & signal_quit();

    /// Signal used for custom slot iteration function.
    /// When run() called, it calls slots of type Slot_iterate connected to the
    /// signal_iterate() signal. While constraction, the %Loop installs its own
    /// default handler. To override that handler, the programmer can install
    /// its own custom handler by connecting it to the returned signal. This feature
    /// provided for proper C++ exception handling. If this handler not installed,
    /// the exceptions will be thrown to the run() method:
    /// @code
    /// int main(int, char **) {
    ///     tau::Loop lp_ = tau::Loop::this_loop();
    ///     try { lp_.run(); }
    ///     // We have to catch exception here.
    ///     catch (tau::exception & x) { ... }
    ///     catch (std::exception & x) { ... }
    ///     catch (...) { ... }
    ///     return 0;
    /// }
    /// @endcode
    ///
    /// The disadvantage of above scheme is run() method can not be called twice, so
    /// you have nothing to do except to log exception to the console and quit program.
    ///
    /// After you installed custom iteration handler, you can handle exceptions from inside
    /// of run() call. This is may be useful to show error message at the main window or
    /// Dialog window and let user to choose between program abort or continue.
    /// @code
    /// tau::Loop lp_;
    ///
    /// bool custom_iterate(const tau::Loop::Slot_iterate & s) {
    ///     try { s(); }
    ///     // We capable to catch exception here.
    ///     catch (tau::exception & x) { ... }
    ///     catch (std::exception & x) { ... }
    ///     catch (...) { ... }
    ///     // Return true to disable default handler.
    ///     return true;
    /// }
    ///
    /// int main(int, char **) {
    ///     lp_ = tau::Loop::this_loop();
    ///     // Connect our handler before by specifiyng additional bool parameter at the end!
    ///     lp_.signal_iterate().connect(tau::fun(custom_iterate), true);
    ///     lp_.run();
    ///     return 0;
    /// }
    /// @endcode
    /// @note If loop is pure, this method called on the this_loop().
    /// @since 0.6.0
    Signal_iterate & signal_iterate();

    /// Signal emitted on every loop iteration.
    /// Slot prototype:
    /// ~~~~~~~~~~~~~~~
    /// void on_run();
    /// ~~~~~~~~~~~~~~~
    /// @note If loop is pure, this method called on the this_loop().
    signal<void()> & signal_run();

    /// Signal emitted when disk partition mounted or unmounted.
    /// Slot prototype:
    /// ~~~~~~~~~~~~~~~
    /// void on_watch(int file_flags, const ustring & path);
    /// ~~~~~~~~~~~~~~~
    /// The meaning of file_flags can be found @ref file_flags_group.
    /// @note If loop is pure, this method called on the this_loop().
    signal<void(int, const ustring &)> & signal_watch();

    /// @}

private:

    Loop_ptr impl;
};

} // namespace tau

#endif // __TAU_LOOP_HH__

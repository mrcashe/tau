// ----------------------------------------------------------------------------
// SPDX-License-Identifier: BSD-2-Clause
// Copyright © 2014-2024 Konstantin Shmelkov <mrcashe@gmail.com>.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
// THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
// PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
// CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
// EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
// PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
// OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
// WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
// OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
// EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// ----------------------------------------------------------------------------

/// @file menu.hh Menu classes.

#ifndef __TAU_MENU_HH__
#define __TAU_MENU_HH__

#include <tau/check.hh>
#include <tau/container.hh>
#include <tau/icon.hh>
#include <tau/separator.hh>

namespace tau {

/// %Menu item base.
/// @note This class is a wrapper around its implementation shared pointer.
/// @ingroup menu_group
class __TAUEXPORT__ Menu_item: public Widget {
public:

    /// Get label.
    ustring label() const;

protected:

    /// @private
    Menu_item(Widget_ptr wp);

    /// @private
    Menu_item & operator=(Widget_ptr wp);
};

/// %Menu item with Action.
/// @note This class is a wrapper around its implementation shared pointer.
/// @ingroup menu_group
class __TAUEXPORT__ Action_menu_item: public Menu_item {
public:

    /// @name Constructors and operators
    /// @{

    /// Constructor with label, icon name and accel label align.
    /// @since 0.7.0
    explicit Action_menu_item(const ustring & label, std::string_view icon_name, Align accel_align=Align::END);

    /// Constructor with Master_action and accel label align.
    /// @since 0.7.0
    explicit Action_menu_item(Master_action & action, Align accel_align=Align::END);

    /// Constructor with Action and accel label align.
    explicit Action_menu_item(Action & action, Align accel_align=Align::END);

    /// Copy constructor.
    ///
    /// @note This class is a wrapper around its implementation shared pointer,
    /// so copying it just increasing implementation pointer use count, but isn't
    /// really copies the object. The underlying implementation is not copyable.
    Action_menu_item(const Action_menu_item & other);

    /// Copy operator.
    ///
    /// @note This class is a wrapper around its implementation shared pointer,
    /// so copying it just increasing implementation pointer use count, but isn't
    /// really copies the object. The underlying implementation is not copyable.
    Action_menu_item & operator=(const Action_menu_item & other);

    /// Move constructor.
    ///
    /// @note This class is a wrapper around its implementation shared pointer,
    /// so copying it just increasing implementation pointer use count, but isn't
    /// really copies the object. The underlying implementation is not copyable.
    /// @note This constructor does not reset @c other implementation pointer.
    Action_menu_item(Action_menu_item && other);

    /// Move operator.
    ///
    /// @note This class is a wrapper around its implementation shared pointer,
    /// so copying it just increasing implementation pointer use count, but isn't
    /// really copies the object. The underlying implementation is not copyable.
    /// @note This operator does not reset @c other implementation pointer.
    Action_menu_item & operator=(Action_menu_item && other);

    /// Constructor with implementation pointer.
    ///
    /// @warning Unlike some other classes (Painter as an example), the whole
    /// @ref widget_stack "widget stack" is unable to run with pure implementation
    /// pointer, so attempting to construct widget from a pure (@b nullptr) pointer
    /// will cause throwing an user_error exception!
    /// That exception also will be thrown if user tries to construct the object
    /// from incompatible implementation shared pointer.
    ///
    /// @throw user_error in case of pure implementation pointer or incompatible
    /// implementation pointer class.
    /// @since 0.4.0
    Action_menu_item(Widget_ptr wp);

    /// Assign implementation.
    ///
    /// @throw user_error in case of pure implementation pointer or incompatible
    /// implementation pointer class.
    /// @since 0.4.0
    Action_menu_item & operator=(Widget_ptr wp);

    /// @}
    /// @name Controls
    /// @{

    /// Set icon.
    /// @since 0.5.0
    void set_icon(const ustring & icon_name, int icon_size=Icon::SMALL);

    /// Set icon.
    /// @since 0.5.0
    void set_icon(const Pixmap pix);

    /// Select Action.
    /// @since 0.7.0
    void select(Action & action);

    /// @}
};

/// %Menu item with Toggle_action.
/// @note This class is a wrapper around its implementation shared pointer.
/// @ingroup menu_group
class __TAUEXPORT__ Toggle_menu_item: public Menu_item {
public:

    /// @name Constructors & Operators
    /// @{

    /// Constructor with Toggle as icon.
    /// @since 0.7.0
    Toggle_menu_item(const ustring & label, std::string_view icon_name);

    /// Constructor with Master_action and Toggle as icon.
    /// @since 0.7.0
    Toggle_menu_item(Master_action & master_action);

    /// Constructor with Toggle_action and Toggle as icon.
    /// @since 0.5.0
    Toggle_menu_item(Toggle_action & toggle_action);

    /// Constructor with Toggle_action, accelerator label align and Toggle as icon.
    /// @since 0.7.0
    Toggle_menu_item(const ustring & label, std::string_view icon_name, Align accel_align);

    /// Constructor with Master_action, accelerator label align and Toggle as icon.
    /// @since 0.7.0
    Toggle_menu_item(Master_action & master_action, Align accel_align);

    /// Constructor with Toggle_action, accelerator label align and Toggle as icon.
    /// @since 0.5.0
    Toggle_menu_item(Toggle_action & toggle_action, Align accel_align);

    /// Constructor with Check as icon.
    /// @since 0.7.0
    Toggle_menu_item(const ustring & label, Check::Style check_style, Border border_style=Border::SOLID);

    /// Constructor with Master_action and Check as icon.
    /// @since 0.7.0
    Toggle_menu_item(Master_action & master_action, Check::Style check_style, Border border_style=Border::SOLID);

    /// Constructor with Toggle_action and Check as icon.
    Toggle_menu_item(Toggle_action & toggle_action, Check::Style check_style, Border border_style=Border::SOLID);

    /// Constructor with accelerator label align and Check as icon.
    /// @since 0.7.0
    Toggle_menu_item(const ustring & label, Align accel_align, Check::Style check_style, Border border_style=Border::SOLID);

    /// Constructor with Master_action, accelerator label align and Check as icon.
    /// @since 0.7.0
    Toggle_menu_item(Master_action & master_action, Align accel_align, Check::Style check_style, Border border_style=Border::SOLID);

    /// Constructor with Toggle_action, accelerator label align and Check as icon.
    Toggle_menu_item(Toggle_action & toggle_action, Align accel_align, Check::Style check_style, Border border_style=Border::SOLID);

    /// Copy constructor.
    /// @note This class is a wrapper around its implementation shared pointer,
    /// so copying it just increasing implementation pointer use count, but isn't
    /// really copies the object. The underlying implementation is not copyable.
    Toggle_menu_item(const Toggle_menu_item & other);

    /// Copy operator.
    /// @note This class is a wrapper around its implementation shared pointer,
    /// so copying it just increasing implementation pointer use count, but isn't
    /// really copies the object. The underlying implementation is not copyable.
    Toggle_menu_item & operator=(const Toggle_menu_item & other);

    /// Move constructor.
    /// @note This class is a wrapper around its implementation shared pointer,
    /// so copying it just increasing implementation pointer use count, but isn't
    /// really copies the object. The underlying implementation is not copyable.
    /// @note This constructor does not reset @c other implementation pointer.
    Toggle_menu_item(Toggle_menu_item && other);

    /// Move operator.
    /// @note This class is a wrapper around its implementation shared pointer,
    /// so copying it just increasing implementation pointer use count, but isn't
    /// really copies the object. The underlying implementation is not copyable.
    /// @note This operator does not reset @c other implementation pointer.
    Toggle_menu_item & operator=(Toggle_menu_item && other);

    /// Constructor with implementation pointer.
    /// @warning Unlike some other classes (Painter as an example), the whole
    /// @ref widget_stack "widget stack" is unable to run with pure implementation
    /// pointer, so attempting to construct widget from a pure (@b nullptr) pointer
    /// will cause throwing an user_error exception!
    /// That exception also will be thrown if user tries to construct the object
    /// from incompatible implementation shared pointer.
    ///
    /// @throw user_error in case of pure implementation pointer or incompatible
    /// implementation pointer class.
    /// @since 0.4.0
    Toggle_menu_item(Widget_ptr wp);

    /// Assign implementation.
    ///
    /// @throw user_error in case of pure implementation pointer or incompatible
    /// implementation pointer class.
    /// @since 0.4.0
    Toggle_menu_item & operator=(Widget_ptr wp);

    /// @}
    /// @name Controls
    /// @{

    /// Set Check style.
    /// @param check_style the check style.
    /// If item built not using constructor with check_style parameter, this method has no effect.
    void set_check_style(Check::Style check_style);

    /// Get Check  style.
    /// If item built not using constructor with check_style parameter, this method returns CHECK_XSTYLE.
    Check::Style check_style() const noexcept;

    /// Set Check border style.
    /// If item built not using constructor with check_style parameter, this method has no effect.
    void set_border_style(Border border_style);

    /// Get Check border style.
    /// If item built not using constructor with check_style parameter, this method returns Border::NONE.
    Border border_style() const noexcept;

    /// Set Check border width.
    /// If item built not using constructor with check_style parameter, this method has no effect.
    void set_border_width(unsigned npx);

    /// Get Check border width.
    /// If item built not using constructor with check_style parameter, this method returns 0.
    unsigned border_width() const noexcept;

    /// Test if toggled.
    /// @since 0.7.0
    bool get() const noexcept;

    /// Select Action.
    /// @since 0.7.0
    void select(Toggle_action & action);

    /// @}
};

/// %Menu item with other menu.
///
/// @note This class is a wrapper around its implementation shared pointer.
///
/// @ingroup menu_group
class __TAUEXPORT__ Submenu_item: public Menu_item {
public:

    /// @name Constructors & Operators
    /// @{

    /// Constructor with label, submenu and optional icon name.
    Submenu_item(const ustring & label, Menu & menu, const ustring & icon_name=ustring());

    /// Constructor with label and optional icon name.
    /// @since 0.6.0
    Submenu_item(const ustring & label, const ustring & icon_name=ustring());

    /// Constructor with label, submenu and Pixmap.
    /// @since 0.5.0
    Submenu_item(const ustring & label, Menu & menu, const Pixmap pix);

    /// Constructor with label and Pixmap.
    /// @since 0.6.0
    Submenu_item(const ustring & label, const Pixmap pix);

    /// Copy constructor.
    ///
    /// @note This class is a wrapper around its implementation shared pointer,
    /// so copying it just increasing implementation pointer use count, but isn't
    /// really copies the object. The underlying implementation is not copyable.
    Submenu_item(const Submenu_item & other);

    /// Copy operator.
    ///
    /// @note This class is a wrapper around its implementation shared pointer,
    /// so copying it just increasing implementation pointer use count, but isn't
    /// really copies the object. The underlying implementation is not copyable.
    Submenu_item & operator=(const Submenu_item & other);

    /// Move constructor.
    ///
    /// @note This class is a wrapper around its implementation shared pointer,
    /// so copying it just increasing implementation pointer use count, but isn't
    /// really copies the object. The underlying implementation is not copyable.
    /// @note This constructor does not reset @c other implementation pointer.
    Submenu_item(Submenu_item && other);

    /// Move operator.
    ///
    /// @note This class is a wrapper around its implementation shared pointer,
    /// so copying it just increasing implementation pointer use count, but isn't
    /// really copies the object. The underlying implementation is not copyable.
    /// @note This operator does not reset @c other implementation pointer.
    Submenu_item & operator=(Submenu_item && other);

    /// Constructor with implementation pointer.
    ///
    /// @warning Unlike some other classes (Painter as an example), the whole
    /// @ref widget_stack "widget stack" is unable to run with pure implementation
    /// pointer, so attempting to construct widget from a pure (@b nullptr) pointer
    /// will cause throwing an user_error exception!
    /// That exception also will be thrown if user tries to construct the object
    /// from incompatible implementation shared pointer.
    ///
    /// @throw user_error in case of pure implementation pointer or incompatible
    /// implementation pointer class.
    /// @since 0.4.0
    Submenu_item(Widget_ptr wp);

    /// Assign implementation.
    ///
    /// @throw user_error in case of pure implementation pointer or incompatible
    /// implementation pointer class.
    /// @since 0.4.0
    Submenu_item & operator=(Widget_ptr op);

    /// @}
    /// @name Controls
    /// @{

    /// Set icon.
    /// @since 0.5.0
    void set_icon(const ustring & icon_name, int icon_size=Icon::SMALL);

    /// Set icon.
    /// @since 0.5.0
    void set_icon(const Pixmap pix);

    /// Set label.
    void set_label(const ustring & label);

    /// Set menu.
    /// @since 0.6.0
    void set_menu(Menu & m);

    /// Get menu.
    /// @since 0.6.0
    Widget_ptr menu();

    /// Get menu.
    /// @since 0.6.0
    Widget_cptr menu() const;

    /// @}
};

/// %Menu item with slot.
///
/// @note This class is a wrapper around its implementation shared pointer.
///
/// @ingroup menu_group
class __TAUEXPORT__ Slot_menu_item: public Menu_item {
public:

    /// @name Constructors and operators
    /// @{

    /// Constructor with label, slot and optional icon name.
    Slot_menu_item(const ustring & label, const slot<void()> & slot_activate, const ustring & icon_name=ustring());

    /// Constructor with label, slot and Pixmap.
    /// @since 0.5.0
    Slot_menu_item(const ustring & label, const slot<void()> & slot_activate, const Pixmap pix);

    /// Copy constructor.
    ///
    /// @note This class is a wrapper around its implementation shared pointer,
    /// so copying it just increasing implementation pointer use count, but isn't
    /// really copies the object. The underlying implementation is not copyable.
    Slot_menu_item(const Slot_menu_item & other);

    /// Copy operator.
    ///
    /// @note This class is a wrapper around its implementation shared pointer,
    /// so copying it just increasing implementation pointer use count, but isn't
    /// really copies the object. The underlying implementation is not copyable.
    Slot_menu_item & operator=(const Slot_menu_item & other);

    /// Move constructor.
    ///
    /// @note This class is a wrapper around its implementation shared pointer,
    /// so copying it just increasing implementation pointer use count, but isn't
    /// really copies the object. The underlying implementation is not copyable.
    /// @note This constructor does not reset @c other implementation pointer.
    Slot_menu_item(Slot_menu_item && other);

    /// Move operator.
    ///
    /// @note This class is a wrapper around its implementation shared pointer,
    /// so copying it just increasing implementation pointer use count, but isn't
    /// really copies the object. The underlying implementation is not copyable.
    /// @note This operator does not reset @c other implementation pointer.
    Slot_menu_item & operator=(Slot_menu_item && other);

    /// Constructor with implementation pointer.
    ///
    /// @warning Unlike some other classes (Painter as an example), the whole
    /// @ref widget_stack "widget stack" is unable to run with pure implementation
    /// pointer, so attempting to construct widget from a pure (@b nullptr) pointer
    /// will cause throwing an user_error exception!
    /// That exception also will be thrown if user tries to construct the object
    /// from incompatible implementation shared pointer.
    ///
    /// @throw user_error in case of pure implementation pointer or incompatible
    /// implementation pointer class.
    /// @since 0.4.0
    Slot_menu_item(Widget_ptr wp);

    /// Assign implementation.
    ///
    /// @throw user_error in case of pure implementation pointer or incompatible
    /// implementation pointer class.
    /// @since 0.4.0
    Slot_menu_item & operator=(Widget_ptr wp);

    /// @}
    /// @name Controls
    /// @{

    /// Set icon.
    /// @since 0.5.0
    void set_icon(const ustring & icon_name, int icon_size=Icon::SMALL);

    /// @overload
    /// Set icon.
    /// @since 0.5.0
    void set_icon(const Pixmap pix);

    /// Set label.
    void set_label(const ustring & label);

    /// @}
};

/// %Menu item with Check.
///
/// @note This class is a wrapper around its implementation shared pointer.
///
/// @ingroup menu_group
class __TAUEXPORT__ Check_menu_item: public Menu_item {
public:

    /// @name General Constructors
    /// @{

    /// Constructor with label and initial state.
    /// @param label the label.
    /// @param checked the initial state.
    Check_menu_item(const ustring & label, bool checked=false);

    /// Constructor with label, check style and initial state.
    /// @param label the label.
    /// @param check_style the check style.
    /// @param checked the initial state.
    Check_menu_item(const ustring & label, Check::Style check_style, bool checked=false);

    /// Constructor with label, border style and initial state.
    /// @param label the label.
    /// @param border_style the border style.
    /// @param checked the initial state.
    Check_menu_item(const ustring & label, Border border_style, bool checked=false);

    /// Constructor with label, check and border style and initial state.
    /// @param label the label.
    /// @param check_style the check style.
    /// @param border_style the border style.
    /// @param checked the initial state.
    Check_menu_item(const ustring & label, Check::Style check_style, Border border_style, bool checked=false);

    /// @}
    /// @name Special Constructors and Operators
    /// @{

    /// Copy constructor.
    /// @note This class is a wrapper around its implementation shared pointer,
    /// so copying it just increasing implementation pointer use count, but isn't
    /// really copies the object. The underlying implementation is not copyable.
    Check_menu_item(const Check_menu_item & other);

    /// Copy operator.
    /// @note This class is a wrapper around its implementation shared pointer,
    /// so copying it just increasing implementation pointer use count, but isn't
    /// really copies the object. The underlying implementation is not copyable.
    Check_menu_item & operator=(const Check_menu_item & other);

    /// Move constructor.
    /// @note This class is a wrapper around its implementation shared pointer,
    /// so copying it just increasing implementation pointer use count, but isn't
    /// really copies the object. The underlying implementation is not copyable.
    /// @note This constructor does not reset @c other implementation pointer.
    Check_menu_item(Check_menu_item && other);

    /// Move operator.
    /// @note This class is a wrapper around its implementation shared pointer,
    /// so copying it just increasing implementation pointer use count, but isn't
    /// really copies the object. The underlying implementation is not copyable.
    /// @note This operator does not reset @c other implementation pointer.
    Check_menu_item & operator=(Check_menu_item && other);

    /// Constructor with implementation pointer.
    /// @warning Unlike some other classes (Painter as an example), the whole
    /// @ref widget_stack "widget stack" is unable to run with pure implementation
    /// pointer, so attempting to construct widget from a pure (@b nullptr) pointer
    /// will cause throwing an user_error exception!
    /// That exception also will be thrown if user tries to construct the object
    /// from incompatible implementation shared pointer.
    ///
    /// @throw user_error in case of pure implementation pointer or incompatible
    /// implementation pointer class.
    /// @since 0.4.0
    Check_menu_item(Widget_ptr wp);

    /// Assign implementation.
    /// @throw user_error in case of pure implementation pointer or incompatible
    /// implementation pointer class.
    /// @since 0.4.0
    Check_menu_item & operator=(Widget_ptr wp);

    /// @}
    /// @name Controls
    /// @{

    /// Set label.
    void set_label(const ustring & label);

    /// Set check style.
    /// @param check_style the check style.
    /// @sa Check::set_check_style()
    void set_check_style(Check::Style check_style);

    /// Get check style.
    /// @sa Check::check_style()
    Check::Style check_style() const noexcept;

    /// Set border style.
    /// @sa Check::border_style()
    void set_border_style(Border border_style);

    /// Get border style.
    /// @sa Check::border_style()
    Border border_style() const noexcept;

    /// Set border width.
    /// @sa Check::border_width()
    void set_border_width(unsigned npx);

    /// Get border width.
    /// @sa Check::border_width()
    unsigned border_width() const noexcept;

    /// Set checked state.
    /// @sa uncheck()
    /// @sa toggle()
    /// @sa set()
    /// @sa setup()
    /// @sa Check::check()
    void check();

    /// Reset checked state.
    /// @sa check()
    /// @sa toggle()
    /// @sa set()
    /// @sa setup()
    /// @sa Check::uncheck()
    void uncheck();

    /// Set state with signals emission.
    /// @sa check()
    /// @sa uncheck()
    /// @sa toggle()
    /// @sa setup()
    /// @sa Check::set()
    /// @since 0.5.0
    void set(bool state);

    /// Set state without signals emission.
    /// @sa check()
    /// @sa uncheck()
    /// @sa toggle()
    /// @sa set()
    /// @sa Check::setup()
    /// @since 0.5.0
    void setup(bool state);

    /// %Toggle check.
    /// @sa check()
    /// @sa uncheck()
    /// @sa set()
    /// @sa setup()
    /// @sa Check::toggle()
    void toggle();

    /// Get current check value.
    /// @sa Check::checked()
    bool checked() const noexcept;

    /// Join same radio group which other item belongs.
    /// @sa Check::join()
    void join(Check_menu_item & other);

    /// Test if joined radio group.
    /// @since 0.5.0
    bool joined() const noexcept;

    /// @}
    /// @name Signals
    /// @{

    /// Signal emitted when check state changed to true.
    /// Slot prototype:
    /// ~~~~~~~~~~~~~~
    /// bool on_check();
    /// ~~~~~~~~~~~~~~
    signal<void()> & signal_check();

    /// Signal emitted when check state changed to false.
    /// Slot prototype:
    /// ~~~~~~~~~~~~~~
    /// bool on_uncheck();
    /// ~~~~~~~~~~~~~~
    signal<void()> & signal_uncheck();

    /// @}
};

/// Basic menu class.
///
/// @note This class is a wrapper around its implementation shared pointer.
///
/// @ingroup menu_group
class __TAUEXPORT__ Menu: public Container {
public:

    /// @name Widget Management
    /// @{

    /// Append widget.
    /// @param w widget to be inserted
    void append(Widget & w);

    /// Prepend widget.
    /// @param w widget to be inserted
    void prepend(Widget & w);

    /// Insert widget before other widget.
    /// @param w widget to be inserted
    /// @param other insert before this widget
    void insert_before(Widget & w, const Widget & other);

    /// Insert widget after other widget.
    /// @param w widget to be inserted
    /// @param other insert after this widget
    void insert_after(Widget & w, const Widget & other);

    /// Test if empty.
    /// @since 0.7.0
    bool empty() const noexcept;

    /// Remove widget.
    void remove(Widget & w);

    /// Remove all widgets.
    void clear();

    /// Quit menu.
    void quit();

    /// @}
    /// @name Insertion Helper Functions
    /// @{

    /// Append Action_menu_item.
    /// @overload
    /// @param action an action
    /// @param accel_align accelerator label text align
    /// @return menu item
    Action_menu_item append(Action & action, Align accel_align=Align::END);

    /// Append Toggle_menu_item with Toggle as icon.
    /// @overload
    /// @param action a toggle action
    /// @return menu item
    /// @since 0.5.0
    Toggle_menu_item append(Toggle_action & action);

    /// Append Toggle_menu_item with Toggle as icon.
    /// @overload
    /// @param action a toggle action
    /// @param accel_align accelerator label text align
    /// @return menu item
    /// @since 0.5.0
    Toggle_menu_item append(Toggle_action & action, Align accel_align);

    /// Append Toggle_menu_item with Check as icon.
    /// @overload
    /// @param action a toggle action
    /// @param check_style Check style
    /// @param border_style Check border style
    /// @return menu item
    Toggle_menu_item append(Toggle_action & action, Check::Style check_style, Border border_style=Border::SOLID);

    /// Append Toggle_menu_item with Check as icon.
    /// @overload
    /// @param action a toggle action
    /// @param accel_align accelerator label text align
    /// @param check_style Check style
    /// @param border_style Check border style
    /// @return menu item
    /// @since 0.5.0
    Toggle_menu_item append(Toggle_action & action, Align accel_align, Check::Style check_style, Border border_style=Border::SOLID);

    /// Append Submenu_item.
    /// @overload
    /// @param label the menu item label
    /// @param menu the submenu
    /// @param icon_name an optional icon name
    /// @return menu item
    Submenu_item append(const ustring & label, Menu & menu, const ustring & icon_name=ustring());

    /// Append Submenu_item.
    /// @overload
    /// @param label the menu item label
    /// @param menu the submenu
    /// @param pix the icon as pixmap
    /// @return menu item
    /// @since 0.5.0
    Submenu_item append(const ustring & label, Menu & menu, const Pixmap pix);

    /// Append Slot_menu_item with optional icon.
    /// @param label the menu item label
    /// @param slot_activate slot to be activated on menu item activation using mouse or keyboard
    /// @param icon_name an optional icon name
    /// @return menu item
    Slot_menu_item append(const ustring & label, const slot<void()> & slot_activate, const ustring & icon_name=ustring());

    /// Append Slot_menu_item with icon.
    /// @overload
    /// @param label the menu item label
    /// @param slot_activate slot to be activated on menu item activation using mouse or keyboard
    /// @param pix the icon as pixmap
    /// @return menu item
    /// @since 0.5.0
    Slot_menu_item append(const ustring & label, const slot<void()> & slot_activate, const Pixmap pix);

    /// Append Separator.
    /// @param separator_style the Separator style
    /// @return Separartor widget
    Separator append_separator(Separator::Style separator_style=Separator::GROOVE);

    /// Prepend Action_menu_item.
    /// @overload
    /// @param action an action
    /// @param accel_align accelerator label text align
    /// @return menu item
    Action_menu_item prepend(Action & action, Align accel_align=Align::END);

    /// Prepend Toggle_menu_item with Toggle as icon.
    /// @overload
    /// @param action a toggle action
    /// @return menu item
    /// @since 0.5.0
    Toggle_menu_item prepend(Toggle_action & action);

    /// Prepend Toggle_menu_item with Toggle as icon.
    /// @overload
    /// @param action a toggle action
    /// @param accel_align accelerator label text align
    /// @return menu item
    /// @since 0.5.0
    Toggle_menu_item prepend(Toggle_action & action, Align accel_align);

    /// Prepend Toggle_menu_item with Check as icon.
    /// @overload
    /// @param action a toggle action
    /// @param check_style Check style
    /// @param border_style Check border style
    /// @return menu item
    Toggle_menu_item prepend(Toggle_action & action, Check::Style check_style, Border border_style=Border::SOLID);

    /// Prepend Toggle_menu_item with Check as icon.
    /// @overload
    /// @param action a toggle action
    /// @param accel_align accelerator label text align
    /// @param check_style Check style
    /// @param border_style Check border style
    /// @return menu item
    /// @since 0.5.0
    Toggle_menu_item prepend(Toggle_action & action, Align accel_align, Check::Style check_style, Border border_style=Border::SOLID);

    /// Prpend Submenu_item.
    /// @overload
    /// @param label the menu item label
    /// @param menu the submenu
    /// @param icon_name an optional icon name
    /// @return menu item
    Submenu_item prepend(const ustring & label, Menu & menu, const ustring & icon_name=ustring());

    /// Prpend Submenu_item.
    /// @overload
    /// @param label the menu item label
    /// @param menu the submenu
    /// @param pix the icon as pixmap
    /// @return menu item
    /// @since 0.5.0
    Submenu_item prepend(const ustring & label, Menu & menu, const Pixmap pix);

    /// Prepend Slot_menu_item with optional icon.
    /// @param label the menu item label
    /// @param slot_activate slot to be activated on menu item activation using mouse or keyboard
    /// @param icon_name an optional icon name
    /// @return menu item
    Slot_menu_item prepend(const ustring & label, const slot<void()> & slot_activate, const ustring & icon_name=ustring());

    /// Prepend Slot_menu_item with icon.
    /// @overload
    /// @param label the menu item label
    /// @param slot_activate slot to be activated on menu item activation using mouse or keyboard
    /// @param pix the icon as pixmap
    /// @return menu item
    /// @since 0.5.0
    Slot_menu_item prepend(const ustring & label, const slot<void()> & slot_activate, const Pixmap pix);

    /// Prepend Separator.
    /// @param separator_style the Separator style
    /// @return Separartor widget
    Separator prepend_separator(Separator::Style separator_style=Separator::GROOVE);

    /// Insert Action_menu_item before other widget.
    /// @overload
    /// @param action an action
    /// @param accel_align accelerator label text align
    /// @param other insert before this widget
    /// @return menu item
    Action_menu_item insert_before(Action & action, const Widget & other, Align accel_align=Align::END);

    /// Insert Toggle_menu_item with Toggle as icon before other widget.
    /// @overload
    /// @param action a toggle action
    /// @param other insert before this widget
    /// @return menu item
    /// @since 0.5.0
    Toggle_menu_item insert_before(Toggle_action & action, const Widget & other);

    /// Insert Toggle_menu_item with Check as icon before other widget.
    /// @overload
    /// @param action a toggle action
    /// @param accel_align accelerator label text align
    /// @param other insert before this widget
    /// @return menu item
    /// @since 0.5.0
    Toggle_menu_item insert_before(Toggle_action & action, Align accel_align, const Widget & other);

    /// Insert Toggle_menu_item with Check as icon before other widget.
    /// @overload
    /// @param action a toggle action
    /// @param other insert before this widget
    /// @param check_style Check style
    /// @param border_style Check border style
    /// @return menu item
    Toggle_menu_item insert_before(Toggle_action & action, const Widget & other, Check::Style check_style, Border border_style=Border::SOLID);

    /// Insert Toggle_menu_item with Check as icon before other widget.
    /// @overload
    /// @param action a toggle action
    /// @param accel_align accelerator label text align
    /// @param other insert before this widget
    /// @param check_style Check style
    /// @param border_style Check border style
    /// @return menu item
    /// @since 0.5.0
    Toggle_menu_item insert_before(Toggle_action & action, Align accel_align, const Widget & other, Check::Style check_style, Border border_style=Border::SOLID);

    /// Insert Submenu_item before other widget.
    /// @overload
    /// @param label the menu item label
    /// @param menu the submenu
    /// @param other insert before this widget
    /// @param icon_name an optional icon name
    /// @return menu item
    Submenu_item insert_before(const ustring & label, Menu & menu, const Widget & other, const ustring & icon_name=ustring());

    /// Insert Submenu_item before other widget.
    /// @overload
    /// @param label the menu item label
    /// @param menu the submenu
    /// @param other insert before this widget
    /// @param pix the icon as pixmap
    /// @return menu item
    /// @since 0.5.0
    Submenu_item insert_before(const ustring & label, Menu & menu, const Widget & other, const Pixmap pix);

    /// Insert Slot_menu_item before other widget.
    /// @overload
    /// @param label the menu item label
    /// @param slot_activate slot to be activated on menu item activation using mouse or keyboard
    /// @param other insert before this widget
    /// @param icon_name an optional icon name
    /// @return menu item
    Slot_menu_item insert_before(const ustring & label, const slot<void()> & slot_activate, const Widget & other, const ustring & icon_name=ustring());

    /// Insert Slot_menu_item before other widget.
    /// @overload
    /// @param label the menu item label
    /// @param slot_activate slot to be activated on menu item activation using mouse or keyboard
    /// @param other insert before this widget
    /// @param pix the icon as pixmap
    /// @return menu item
    /// @since 0.5.0
    Slot_menu_item insert_before(const ustring & label, const slot<void()> & slot_activate, const Widget & other, const Pixmap pix);

    /// Insert Separator before other widget.
    /// @param other insert before this widget
    /// @param separator_style the Separator style
    /// @return Separartor widget
    Separator insert_separator_before(const Widget & other, Separator::Style separator_style=Separator::GROOVE);

    /// Insert Action_menu_item after other widget.
    /// @overload
    /// @param action an action
    /// @param accel_align accelerator label text align
    /// @param other insert after this widget
    /// @return menu item
    Action_menu_item insert_after(Action & action, const Widget & other, Align accel_align=Align::END);

    /// Insert Toggle_menu_item with Toggle as icon after other widget.
    /// @overload
    /// @param action a toggle action
    /// @param other insert after this widget
    /// @return menu item
    /// @since 0.5.0
    Toggle_menu_item insert_after(Toggle_action & action, const Widget & other);

    /// Insert Toggle_menu_item with Toggle as icon after other widget.
    /// @overload
    /// @param action a toggle action
    /// @param accel_align accelerator label text align
    /// @param other insert after this widget
    /// @return menu item
    /// @since 0.5.0
    Toggle_menu_item insert_after(Toggle_action & action, Align accel_align, const Widget & other);

    /// Insert Toggle_menu_item with Check as icon after other widget.
    /// @overload
    /// @param action a toggle action
    /// @param other insert after this widget
    /// @param check_style Check style
    /// @param border_style Check border style
    /// @return menu item
    Toggle_menu_item insert_after(Toggle_action & action, const Widget & other, Check::Style check_style, Border border_style=Border::SOLID);

    /// Insert Toggle_menu_item with Check as icon after other widget.
    /// @overload
    /// @param action a toggle action
    /// @param accel_align accelerator label text align
    /// @param other insert after this widget
    /// @param check_style Check style
    /// @param border_style Check border style
    /// @return menu item
    /// @since 0.5.0
    Toggle_menu_item insert_after(Toggle_action & action, Align accel_align, const Widget & other, Check::Style check_style, Border border_style=Border::SOLID);

    /// Insert Submenu_item after other widget.
    /// @overload
    /// @param label the menu item label
    /// @param menu the submenu
    /// @param other insert after this widget
    /// @param icon_name an optional icon name
    /// @return menu item
    Submenu_item insert_after(const ustring & label, Menu & menu, const Widget & other, const ustring & icon_name=ustring());

    /// Insert Submenu_item after other widget.
    /// @overload
    /// @param label the menu item label
    /// @param menu the submenu
    /// @param other insert after this widget
    /// @param pix the icon as pixmap
    /// @return menu item
    /// @since 0.5.0
    Submenu_item insert_after(const ustring & label, Menu & menu, const Widget & other, const Pixmap pix);

    /// Insert Slot_menu_item after other widget.
    /// @overload
    /// @param label the menu item label
    /// @param slot_activate slot to be activated on menu item activation using mouse or keyboard
    /// @param other insert after this widget
    /// @param icon_name an optional icon name
    /// @return menu item
    Slot_menu_item insert_after(const ustring & label, const slot<void()> & slot_activate, const Widget & other, const ustring & icon_name=ustring());

    /// Insert Slot_menu_item after other widget.
    /// @overload
    /// @param label the menu item label
    /// @param slot_activate slot to be activated on menu item activation using mouse or keyboard
    /// @param other insert after this widget
    /// @param pix the icon as pixmap
    /// @return menu item
    /// @since 0.5.0
    Slot_menu_item insert_after(const ustring & label, const slot<void()> & slot_activate, const Widget & other, const Pixmap pix);

    /// Insert Separator after other widget.
    /// @param other insert after this widget
    /// @param separator_style the Separator style
    /// @return Separartor widget
    Separator insert_separator_after(const Widget & other, Separator::Style separator_style=Separator::GROOVE);

    /// @}
    /// @name Signals
    /// @{

    /// Emitted when menu quits.
    signal<void()> & signal_quit();

    /// @}

protected:

    /// @private
    Menu(Widget_ptr wp);

    /// @private
    Menu & operator=(Widget_ptr wp);
};

/// %Popup menu.
///
/// @note This class is a wrapper around its implementation shared pointer.
///
/// @ingroup menu_group
class __TAUEXPORT__ Menubox: public Menu {
public:

    /// @name Contructors & Operators
    /// @{

    /// Constructor.
    Menubox();

    /// Copy constructor.
    ///
    /// @note This class is a wrapper around its implementation shared pointer,
    /// so copying it just increasing implementation pointer use count, but isn't
    /// really copies the object. The underlying implementation is not copyable.
    Menubox(const Menubox & other);

    /// Copy operator.
    ///
    /// @note This class is a wrapper around its implementation shared pointer,
    /// so copying it just increasing implementation pointer use count, but isn't
    /// really copies the object. The underlying implementation is not copyable.
    Menubox & operator=(const Menubox & other);

    /// Constructor with implementation pointer.
    ///
    /// @warning Unlike some other classes (Painter as an example), the whole
    /// @ref widget_stack "widget stack" is unable to run with pure implementation
    /// pointer, so attempting to construct widget from a pure (@b nullptr) pointer
    /// will cause throwing an user_error exception!
    /// That exception also will be thrown if user tries to construct the object
    /// from incompatible implementation shared pointer.
    ///
    /// @throw user_error in case of pure implementation pointer or incompatible
    /// implementation pointer class.
    /// @since 0.4.0
    Menubox(Widget_ptr wp);

    /// Assign implementation.
    ///
    /// @throw user_error in case of pure implementation pointer or incompatible
    /// implementation pointer class.
    /// @since 0.4.0
    Menubox & operator=(Widget_ptr wp);

    /// Move constructor.
    ///
    /// @note This class is a wrapper around its implementation shared pointer,
    /// so copying it just increasing implementation pointer use count, but isn't
    /// really copies the object. The underlying implementation is not copyable.
    /// @note This constructor does not reset @c other implementation pointer.
    Menubox(Menubox && other);

    /// Move operator.
    ///
    /// @note This class is a wrapper around its implementation shared pointer,
    /// so copying it just increasing implementation pointer use count, but isn't
    /// really copies the object. The underlying implementation is not copyable.
    /// @note This operator does not reset @c other implementation pointer.
    Menubox & operator=(Menubox && other);

    /// @}
    /// @name Controls
    /// @{

    /// %Popup menu.
    /// @throw internal_error in some cases
    /// @since 0.7.0
    Widget_ptr popup();

    /// %Popup menu.
    /// @throw internal_error in some cases
    /// @since 0.7.0
    Widget_ptr popup(Toplevel & root);

    /// %Popup menu.
    /// @throw internal_error in some cases
    Widget_ptr popup(Toplevel & root, const Point & origin);

    /// %Popup menu.
    /// @throw internal_error in some cases
    /// @since 0.7.0
    Widget_ptr popup(Toplevel & root, Gravity gravity);

    /// %Popup menu.
    /// @throw internal_error in some cases
    Widget_ptr popup(Toplevel & root, const Point & origin, Gravity gravity);

    /// @}
};

/// %Menu with items arranged horizontally.
///
/// @note This class is a wrapper around its implementation shared pointer.
///
/// @ingroup menu_group
class __TAUEXPORT__ Menubar: public Menu {
public:

    /// @name Contructors & Operators
    /// @{

    /// Constructor.
    Menubar();

    /// Copy constructor.
    ///
    /// @note This class is a wrapper around its implementation shared pointer,
    /// so copying it just increasing implementation pointer use count, but isn't
    /// really copies the object. The underlying implementation is not copyable.
    Menubar(const Menubar & other);

    /// Copy operator.
    ///
    /// @note This class is a wrapper around its implementation shared pointer,
    /// so copying it just increasing implementation pointer use count, but isn't
    /// really copies the object. The underlying implementation is not copyable.
    Menubar & operator=(const Menubar & other);

    /// Constructor with implementation pointer.
    ///
    /// @warning Unlike some other classes (Painter as an example), the whole
    /// @ref widget_stack "widget stack" is unable to run with pure implementation
    /// pointer, so attempting to construct widget from a pure (@b nullptr) pointer
    /// will cause throwing an user_error exception!
    /// That exception also will be thrown if user tries to construct the object
    /// from incompatible implementation shared pointer.
    ///
    /// @throw user_error in case of pure implementation pointer or incompatible
    /// implementation pointer class.
    /// @since 0.4.0
    Menubar(Widget_ptr wp);

    /// Assign implementation.
    ///
    /// @throw user_error in case of pure implementation pointer or incompatible
    /// implementation pointer class.
    /// @since 0.4.0
    Menubar & operator=(Widget_ptr wp);

    /// Move constructor.
    ///
    /// @note This class is a wrapper around its implementation shared pointer,
    /// so copying it just increasing implementation pointer use count, but isn't
    /// really copies the object. The underlying implementation is not copyable.
    /// @note This constructor does not reset @c other implementation pointer.
    Menubar(Menubar && other);

    /// Move operator.
    ///
    /// @note This class is a wrapper around its implementation shared pointer,
    /// so copying it just increasing implementation pointer use count, but isn't
    /// really copies the object. The underlying implementation is not copyable.
    /// @note This operator does not reset @c other implementation pointer.
    Menubar & operator=(Menubar && other);

    /// @}
    /// @name Controls
    /// @{

    /// Activate menu.
    /// @return @b true on success.
    bool activate();

    /// @}
};

} // namespace tau

#endif // __TAU_MENU_HH__

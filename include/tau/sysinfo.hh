// ----------------------------------------------------------------------------
// SPDX-License-Identifier: BSD-2-Clause
// Copyright © 2014-2024 Konstantin Shmelkov <mrcashe@gmail.com>.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
// THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
// PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
// CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
// EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
// PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
// OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
// WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
// OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
// EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// ----------------------------------------------------------------------------

#ifndef __TAU_SYSINFO_HH__
#define __TAU_SYSINFO_HH__

#include <tau/ustring.hh>

namespace tau {

/// System information structure.
/// @ingroup sys_group
struct __TAUEXPORT__ Sysinfo {
    /// Major version component.
    int         Major;

    /// Minor version component.
    int         Minor;

    /// Micro version component.
    int         Micro;

    /// Platform name.
    ///
    /// Possible values are:
    /// - Linux
    /// - FreeBSD
    /// - Windows
    std::string plat;

    /// Operating system name.
    ///
    /// This is what uname(1) outputs on POSIX.
    /// On Windows, so far GetVersionInfoExW() used.
    ustring     uname;

    /// Operating system version numbers: major and minor.
    /// Differ on various OSes.
    int         osmajor;
    int         osminor;

    /// @name Distributive related info (actual for Linux only).
    /// @{

    /// Distributive name, such as "Ubuntu", "Mageia" etc.
    ustring     distrib;

    /// Distributive major and ninor release numbers.
    int         distrib_major;
    int         distrib_minor;

    /// Distributive codename.
    ustring     distrib_codename;

    /// Distributive description.
    ustring     distrib_description;

    /// @}
    /// @name Platform Specific Data
    /// @{
    ///
    /// Target name.
    ///
    /// Possible values are:
    /// - x86_64-linux-gnu (g++)
    /// - x86_64-pc-linux-gnu (clang++)
    /// - i686-w64-mingw32.static
    /// - i686-w64-mingw32.shared
    /// - x86_64-w64-mingw32.static
    /// - x86_64-w64-mingw32.shared
    std::string host;

    /// Address size, in bits.
    ///
    /// Possible values are:
    /// - 32
    /// - 64
    int         abits;

    /// int type size, in bits.
    ///
    /// Possible values are:
    /// - 32
    /// - 64
    int         ibits;

    /// long type size, in bits.
    ///
    /// Possible values are:
    /// - 32
    /// - 64
    int         lbits;

    /// long long type size, in bits.
    ///
    /// Possible values are:
    /// - 32
    /// - 64
    int         llbits;

    /// intmax_t, uintmax_t type size, in bits.
    ///
    /// Possible values are:
    /// - 32
    /// - 64
    int         mbits;

    /// wchar_t type size, in bits.
    ///
    /// Possible values are:
    /// - 16
    /// - 32
    int         wcbits;

    /// Linkage type
    ///
    /// Possible values are:
    ///  - @b true if shared linkage;
    ///  - @b false if static linkage.
    bool        shared;

    /// Shared library path.
    /// When .shared is @b false, this field is empty.
    /// Else it @e must contain path to the shared library but it is possible it not.
    ustring     sopath;

    /// Library prefix.
    /// When linked statically, this field equals to value returned by path_prefix().
    /// When linked dynamically on *nix, this field is shared library prefix, e.g. ../$(sopath).
    /// When linked dynamically on Windows, this field is FIXME ???
    ustring     lib_prefix;

    /// System locale.
    std::string locale      = "C";

    /// Encoding for file i/o.
    std::string iocharset   = "C";

    /// Number of CPU cores.
    unsigned    cores   = 1;
};

/// Sysinfo is accessible using function.
/// @ingroup sys_group
//  Definition in $(confdir)/$(plat)/conf-$(plat).cc
__TAUEXPORT__ Sysinfo sysinfo() noexcept;

/// Get Sysinfo content as a text split into lines.
/// @ingroup string_group
//  implementation in $(srcdir)/sys.cc
__TAUEXPORT__ ustring str_sysinfo();

} // namespace tau

#endif // __TAU_SYSINFO_HH__

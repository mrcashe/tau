// ----------------------------------------------------------------------------
// SPDX-License-Identifier: BSD-2-Clause
// Copyright © 2014-2024 Konstantin Shmelkov <mrcashe@gmail.com>.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
// THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
// PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
// CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
// EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
// PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
// OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
// WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
// OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
// EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// ----------------------------------------------------------------------------

/// @file accel.hh The Accel class.

#ifndef __TAU_ACCEL_HH__
#define __TAU_ACCEL_HH__

#include <tau/defs.hh>
#include <tau/input.hh>
#include <tau/signal.hh>
#include <tau/ustring.hh>

namespace tau {

/// A keyboard accelerator.
/// @ingroup action_group
/// @sa @ref kbd_sect
class __TAUEXPORT__ Accel {
public:

    /// @name Constructors, operators
    /// @{

    /// Default constructor.
    Accel();

    /// Constructor with key code and key modifier.
    /// @param kc key code, see #Key_codes enum.
    /// @param km key modifier, see #Key_modifiers enum.
    Accel(char32_t kc, int km=KM_NONE);

    /// Constructor with key code, key modifier and slot.
    /// @param kc               key code, see #Key_codes enum.
    /// @param km               key modifier, see #Key_modifiers enum.
    /// @param slot_activate    slot to be connected.
    Accel(char32_t kc, int km, slot<bool()> slot_activate);

    /// Constructor with string representation.
    /// @param spec the key specification
    Accel(const ustring & spec);

    /// Constructor with string representation and slot.
    /// @param spec             the key specification
    /// @param slot_activate    slot to be connected.
    Accel(const ustring & spec, slot<bool()> slot_activate);

    /// Copy constructor.
    Accel(const Accel & other);

    /// Copy operator.
    Accel & operator=(const Accel & other);

    /// Move constructor.
    Accel(Accel && other);

    /// Move operator.
    Accel & operator=(Accel && other);

    /// Destructor.
   ~Accel();

    /// @}
    /// @name Controls
    /// @{

    /// Test if empty (unassigned).
    bool empty() const noexcept;

    /// Test if not empty (assigned).
    operator bool() const noexcept;

    /// Compare.
    /// @param kc key code, see #Key_codes enum.
    /// @param km key modifier, see #Key_modifiers enum.
    bool equal(char32_t kc, int km) const noexcept;

    /// Compare.
    bool equal(const ustring & spec) const noexcept;

    /// Assign new key code and modifiers.
    /// @param kc key code, see #Key_codes enum.
    /// @param km key modifier, see #Key_modifiers enum.
    void assign(char32_t kc, int km=KM_NONE);

    /// Set new key code and modifiers from string.
    /// @param spec the key specification.
    void assign(const ustring & spec);

    /// Gets key code.
    char32_t key_code() const noexcept;

    /// Gets key modifier.
    int key_modifier() const noexcept;

    /// Gets assigned key codes.
    std::pair<char32_t, int> keys() const noexcept;

    /// Gets specification of assigned key.
    /// @sa key_spec_to_string()
    ustring spec() const;

    /// Gets human readable label.
    /// @sa key_spec_to_label()
    ustring label() const;

    /// @}
    /// @name Sensitivity
    /// @{

    /// Enable accelerator.
    void enable();

    /// Disable accelerator.
    void disable();

    /// Parametric form of functions enable() and disable().
    /// @param yes enable or disable.
    /// Calls enable() if @a yes is @b true and disable() otherwise.
    /// @since 0.5.0
    void par_enable(bool yes);

    /// Determines whether accelerator enabled.
    bool enabled() const noexcept;

    /// @}
    /// @name Signals
    /// @{

    /// Connect slot to the internal signal.
    connection connect(slot<bool()> slot_activate);

    /// Signal emitted when key value changed.
    /// Slot prototype:
    /// ~~~
    /// void on_change();
    /// ~~~
    signal<void()> & signal_changed();

    /// @}

private:

    char32_t            kc_                 = 0;
    int                 km_                 = 0;
    bool                enabled_            = true;
    connection          handle_cx_          { true };
    signal<bool()>      signal_activate_;
    signal<void()> *    signal_changed_     = nullptr;

private:

    friend class Widget_impl;
    bool handle_accel(char32_t kc, int km) const;
};

/// Compare accelerators.
/// @relates Accel
bool operator==(const Accel & a1, const Accel & a2) noexcept;

/// Compare accelerators.
/// @relates Accel
bool operator!=(const Accel & a1, const Accel & a2) noexcept;

} // namespace tau

#endif // __TAU_ACCEL_HH__

// ----------------------------------------------------------------------------
// SPDX-License-Identifier: BSD-2-Clause
// Copyright © 2014-2024 Konstantin Shmelkov <mrcashe@gmail.com>.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
// THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
// PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
// CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
// EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
// PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
// OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
// WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
// OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
// EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// ----------------------------------------------------------------------------

/// @file sys.hh System Functions.

#ifndef __TAU_SYS_HH__
#define __TAU_SYS_HH__

#include <tau/enums.hh>
#include <tau/defs.hh>
#include <tau/ustring.hh>
#include <optional>
#include <vector>

namespace tau {

/// Slash character using for path elements separation ('/' for Unix and '\' for Windows).
/// @ingroup path_group
__TAUEXPORT__ char32_t path_slash() noexcept;

/// Path separator using for path separation in path lists (':' for Unix and ';' for Windows).
/// @ingroup path_group
__TAUEXPORT__ char32_t path_sep() noexcept;

/// Build path from two components.
/// @throw user_error if both paths are absolute.
/// @ingroup path_group
__TAUEXPORT__ ustring path_build(const ustring & s1, const ustring & s2, char32_t slash=path_slash());

/// Build path from two components.
/// @throw user_error if both paths are absolute.
/// @ingroup path_group
/// @since 0.7.0
__TAUEXPORT__ ustring path_build(const ustring & s1, const std::vector<ustring> & v, char32_t slash=path_slash());

/// Build path from three components.
/// @throw user_error if two or more paths are absolute.
/// @ingroup path_group
__TAUEXPORT__ ustring path_build(const ustring & s1, const ustring & s2, const ustring & s3, char32_t slash=path_slash());

/// Test if path absolute.
/// @ingroup path_group
__TAUEXPORT__ bool path_is_absolute(const ustring & path);

/// Extracts all but the directory-part of the path.
/// If the file name contains no slash, it is left unchanged.
/// Otherwise, everything through the last slash is removed from it.
/// @ingroup path_group
__TAUEXPORT__ ustring path_notdir(const ustring & path);

/// Get file extension.
/// @ingroup path_group
__TAUEXPORT__ ustring path_suffix(const ustring & path);

/// Compare two paths for equality.
/// @since 0.5.0
__TAUEXPORT__ bool path_same(const ustring & first_path, const ustring & second_path);

/// Get file name excluding directory part and suffix.
/// @ingroup path_group
__TAUEXPORT__ ustring path_basename(const ustring & path);

/// Get directory part of the path.
/// @ingroup path_group
__TAUEXPORT__ ustring path_dirname(const ustring & path);

/// Get resolved path.
/// @ingroup path_group
__TAUEXPORT__ ustring path_real(const ustring & path);

/// Get executable file path.
/// @ingroup path_group
__TAUEXPORT__ ustring path_self();

/// Test if path matches against given template.
/// @ingroup path_group
__TAUEXPORT__ bool path_match(const ustring & pattern, const ustring & path);

/// Get current directory.
/// @ingroup path_group
__TAUEXPORT__ ustring path_cwd();

/// Get temporary directory.
/// @ingroup path_group
__TAUEXPORT__ ustring path_tmp();

/// Get program installation prefix directory.
/// @ingroup path_group
__TAUEXPORT__ ustring path_prefix();

/// Get share directory path.
/// @ingroup path_group
__TAUEXPORT__ ustring path_share();

/// Get user home directory path.
/// @ingroup path_group
__TAUEXPORT__ ustring path_home();

/// Get user data directory path.
/// @ingroup path_group
__TAUEXPORT__ ustring path_user_data_dir();

/// Get user configuration directory path.
/// @ingroup path_group
__TAUEXPORT__ ustring path_user_config_dir();

/// Get user cache directory path.
/// @ingroup path_group
__TAUEXPORT__ ustring path_user_cache_dir();

/// Get user runtime directory path.
/// @ingroup path_group
__TAUEXPORT__ ustring path_user_runtime_dir();

/// Get user desktop directory path.
/// @ingroup path_group
/// @since 0.7.0
__TAUEXPORT__ ustring path_user_desktop_dir();

/// Get user document's directory path.
/// @ingroup path_group
/// @since 0.7.0
__TAUEXPORT__ ustring path_user_documents_dir();

/// Get user downloads directory path.
/// @ingroup path_group
/// @since 0.7.0
__TAUEXPORT__ ustring path_user_downloads_dir();

/// Get user music directory path.
/// @ingroup path_group
/// @since 0.7.0
__TAUEXPORT__ ustring path_user_music_dir();

/// Get user pictures directory path.
/// @ingroup path_group
/// @since 0.7.0
__TAUEXPORT__ ustring path_user_pictures_dir();

/// Get user videos directory path.
/// @ingroup path_group
/// @since 0.7.0
__TAUEXPORT__ ustring path_user_videos_dir();

/// Get user templates directory path.
/// @ingroup path_group
/// @since 0.7.0
__TAUEXPORT__ ustring path_user_templates_dir();

/// Get full path of shell commands.
/// @ingroup path_group
__TAUEXPORT__ ustring path_which(const ustring & cmd);

/// @overload
/// Get full file path from the list of paths.
/// @ingroup path_group
/// @since 0.6.0
__TAUEXPORT__ ustring path_which(const ustring & filename, const std::vector<ustring> & paths);

/// Split path into components.
/// @ingroup path_group
/// @since 0.6.0
__TAUEXPORT__ std::vector<ustring> path_explode(const ustring & path);

/// Compose path from components.
/// @ingroup path_group
/// @since 0.6.0
__TAUEXPORT__ ustring path_implode(const std::vector<ustring> & v, char32_t slash=path_slash());

/// Lists directory.
/// Directory names are included in search result.
/// @ingroup file_group
__TAUEXPORT__ std::vector<ustring> file_glob(const ustring & mask);

/// Lists directory.
/// Directory names doesn't included in search result.
/// @ingroup file_group
__TAUEXPORT__ std::vector<ustring> file_list(const ustring & dir_path);

/// Perform recursive file search under given directory.
/// Directory names included in search result.
/// @ingroup file_group
__TAUEXPORT__ std::vector<ustring> file_find(const ustring & dir, const ustring & mask="*");

/// Get login name.
/// @ingroup sys_group
__TAUEXPORT__ ustring login_name();

/// Get program name.
/// @ingroup sys_group
__TAUEXPORT__ ustring program_name();

/// Set program name.
/// @ingroup sys_group
/// @since 0.6.0
__TAUEXPORT__ ustring program_name(const ustring & name);

/// Test if file exist.
/// @remark You can specify multiple paths separated by colons for Unix or semicolons for Windows to test against multiple paths in single call.
/// @ingroup file_group
__TAUEXPORT__ bool file_exists(const ustring & path);

/// Test if file is directory.
/// @ingroup file_group
__TAUEXPORT__ bool file_is_dir(const ustring & path);

/// Test if file is a regular file.
/// @ingroup file_group
/// @since 0.5.0
__TAUEXPORT__ bool file_is_regular(const ustring & path);

/// @ingroup file_group
/// Make directory with parents.
/// @throw user_error in case of one of parent element is not a directory.
/// @throw sys_error on OS error.
__TAUEXPORT__ void file_mkdir(const ustring & path);

/// Get environment value.
/// Gets UTF-8 encoded value obtained from environment
/// variable named @e env. If that variable is not defined,
/// fallback value returned which is empty by default.
/// @ingroup sys_group
__TAUEXPORT__ ustring str_getenv(const ustring & env, const ustring & fallback=ustring());

/// Substitute environment variables given in forms @b $VAR, @b $(VAR), @b ${VAR} by variable values.
/// @ingroup sys_group
/// @since 0.7.0
__TAUEXPORT__ ustring str_envsubst(const ustring & s);

/// Sleep for a while.
/// @param time_ms time of the sleep, in milliseconds.
/// @ingroup time_group
__TAUEXPORT__ void msleep(unsigned time_ms);

/// Gets URI scheme.
/// @ingroup uri_group
/// @since 0.6.0
__TAUEXPORT__ ustring uri_scheme(const ustring & uri);

/// Test if URI has specified scheme.
/// @ingroup uri_group
/// @since 0.6.0
__TAUEXPORT__ bool uri_has_scheme(const ustring & uri, const ustring & scheme);

/// Gets part of the URI that is not a scheme.
/// @ingroup uri_group
/// @since 0.6.0
__TAUEXPORT__ ustring uri_notscheme(const ustring & uri);

/// Test if URI references local file.
/// @ingroup uri_group
/// @since 0.6.0
__TAUEXPORT__ bool uri_is_file(const ustring & uri);

/// Escapes URI.
/// @ingroup uri_group
/// @since 0.5.0
__TAUEXPORT__ ustring uri_escape(const ustring & s);

/// Unescapes URI.
/// @ingroup uri_group
/// @since 0.5.0
__TAUEXPORT__ ustring uri_unescape(const ustring & s);

/// Get shared resource.
/// @since 0.6.0
__TAUEXPORT__ std::optional<std::vector<uint8_t>> shared_resource(const ustring & resource_name);

/// Get metric.
/// @since 0.7.0
__TAUEXPORT__ intmax_t metric(Metric met);

/// TODO Documentation
__TAUEXPORT__ ustring pipe_in(const ustring & cmd);

} // namespace tau

#endif // __TAU_SYS_HH__

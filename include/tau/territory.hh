// ----------------------------------------------------------------------------
// SPDX-License-Identifier: BSD-2-Clause
// Copyright © 2014-2024 Konstantin Shmelkov <mrcashe@gmail.com>.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
// THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
// PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
// CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
// EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
// PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
// OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
// WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
// OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
// EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// ----------------------------------------------------------------------------

/// @file territory.hh The Territory class.

#ifndef __TAU_TERRITORY_HH__
#define __TAU_TERRITORY_HH__

#include <tau/defs.hh>
#include <tau/ustring.hh>
#include <vector>

namespace tau {

/// Represents territory based on ISO territory/country codes.
/// @ingroup i18n_group
class __TAUEXPORT__ Territory {
public:

    /// @name Contructors, operators, destructor.
    /// @{

    /// Constructs from current locale.
    Territory();

    /// Constructs from ISO code.
    Territory(std::string_view iso_code);

    /// Constructs from numeric code.
    Territory(int numeric);

    /// Copy constructor.
    Territory(const Territory & other);

    /// Copy operator.
    Territory & operator=(const Territory & other);

    /// Destructor.
    ~Territory();

    /// Create from system default territory.
    static Territory system();

    /// Compare operator.
    bool operator==(const Territory & other) const;

    /// Compare operator.
    bool operator!=(const Territory & other) const;

    /// @}
    /// @name Controls
    /// @{

    /// Get native full name.
    /// @return something like "United State of America".
    ustring full() const;

    /// Get native name.
    /// @return something like "United States".
    ustring name() const;

    /// Get native abbreviation.
    /// @return something like "USA".
    ustring abbrev() const;

    /// Get English full name.
    std::string efull() const;

    /// Get English name.
    std::string ename() const;

    /// Get English abbreviation.
    std::string eabbrev() const;

    /// Get shotest code.
    std::string code() const;

    /// Get 3-character code if available.
    std::string code3() const;

    /// Get numeric code.
    int numeric_code() const;

    /// @}
    /// @name Observers
    /// @{

    /// List all supported ISO codes.
    static std::vector<std::string> iso_codes();

    /// List all supported numeric codes.
    static std::vector<int> numeric_codes();

    /// @}

private:

    Territory_data * data;
};

} // namespace tau

#endif // __TAU_TERRITORY_HH__

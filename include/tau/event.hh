// ----------------------------------------------------------------------------
// SPDX-License-Identifier: BSD-2-Clause
// Copyright © 2014-2024 Konstantin Shmelkov <mrcashe@gmail.com>.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
// THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
// PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
// CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
// EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
// PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
// OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
// WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
// OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
// EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// ----------------------------------------------------------------------------

/// @file event.hh Event class.

#ifndef __TAU_EVENT_HH__
#define __TAU_EVENT_HH__

#include <tau/defs.hh>
#include <tau/signal.hh>

namespace tau {

/// %Event.
///
/// %Event can be emitted by non-GUI thread.
/// After emission an event loop emits signal_activate().
///
/// @note This class is a wrapper around its implementation shared pointer.
///
/// @ingroup sys_group
class __TAUEXPORT__ Event {
public:

    /// @name Constructors & Operators
    /// @{
    /// Default constructor makes pure event.
    Event();

    /// Constructor with slot.
    /// Makes event for @b this thread.
    Event(slot<void()> slot_ready);

    /// Copy constructor.
    ///
    /// @note This class is a wrapper around its implementation shared pointer,
    /// so copying it just increasing implementation pointer use count, but isn't
    /// really copies the object. The underlying implementation is not copyable.
    Event(const Event & other) = default;

    /// Copy operator.
    ///
    /// @note This class is a wrapper around its implementation shared pointer,
    /// so copying it just increasing implementation pointer use count, but isn't
    /// really copies the object. The underlying implementation is not copyable.
    Event & operator=(const Event & other) = default;

    /// Constructor with implementation pointer.
    Event(Event_ptr evp);

    /// Get implementation pointer.
    /// @since 0.6.0
    Event_ptr ptr();

    /// Get implementation pointer.
    /// @since 0.6.0
    Event_cptr ptr() const;

    /// @}
    /// @name Controls
    /// @{

    /// Test if pure.
    operator bool() const noexcept;

    /// Set event to signalled state.
    /// @throw user_error in case this event is pure, so test object before use this method.
    void emit();

    /// @}
    /// @name Signal
    /// @{

    /// Get "signal_ready" reference.
    /// @throw user_error in case this event is pure, so test object before use this method.
    signal<void()> & signal_ready();

    /// @}

private:

    Event_ptr impl;
};

} // namespace tau

#endif // __TAU_EVENT_HH__

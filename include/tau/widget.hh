// ----------------------------------------------------------------------------
// SPDX-License-Identifier: BSD-2-Clause
// Copyright © 2014-2024 Konstantin Shmelkov <mrcashe@gmail.com>.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
// THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
// PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
// CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
// EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
// PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
// OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
// WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
// OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
// EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// ----------------------------------------------------------------------------

/// @file widget.hh The Widget class.

#ifndef __TAU_WIDGET_HH__
#define __TAU_WIDGET_HH__

#include <tau/enums.hh>
#include <tau/geometry.hh>
#include <tau/input.hh>
#include <tau/signal.hh>

namespace tau {

/// Base class of all user interface objects and a simplest widget same time.
///
/// The widget is the atom of the user interface: it receives mouse,
/// keyboard and other events from the window system, and paints a representation
/// of itself on the screen. Every widget is rectangular. A widget is clipped by
/// its parent.
///
/// A widget that is not embedded in a parent widget is called a window.
/// Usually, windows have a frame and a title bar, although it is also possible
/// to create windows without such decoration.
///
/// The Widget class built using so called "pImpl" paradigm: the opaque implementation
/// class Widget_impl is hidden behind Widget class. The smart pointer is used to
/// hold an implementation class.
///
/// Such a construction architecture leads to far-reaching consequences. When you
/// insert widget into Container, the smart pointer holding Widget_impl copied to
/// container's data, but container knows nothing about Widget itself. Thus,
/// the implementation class defines the functionality of the widget. You can only
/// change the behavior of the widget using the signal handlers that are emitted
/// by the widget.
///
/// Another issue is we can not obtain widget parent from %Widget itself.
/// This is a consequence of the fact that we do not store a pointer to a possibly
/// inherited object in the data structures of the library. However, this is a
/// small drawback with the proper construction of the program code - the nice
/// programmer must always know about where parent is.
///
/// The Widget class is fully functional. You can construct it, you can derive it.
/// Typical application of the Widget is drawing area. Any derived class can be used
/// as drawing area.
///
/// The underlaying Widget_impl actually is a descendant of Object class,
/// so some methods of the %Widget class are related to Object.
///
/// @ingroup widget_group
class __TAUEXPORT__ Widget: public trackable {
public:

    /// @name Constructors, operators, shared pointers
    /// @{

    /// Default constructor.
    /// Creates basic widget commonly used for drawing.
    Widget();

    /// Destructor.
    virtual ~Widget();

    /// Copy constructor.
    ///
    /// @note This class is a wrapper around its implementation shared pointer,
    /// so copying it just increasing implementation pointer use count, but isn't
    /// really copies the object. The underlying implementation is not copyable.
    Widget(const Widget & other);

    /// Copy operator.
    ///
    /// @note This class is a wrapper around its implementation shared pointer,
    /// so copying it just increasing implementation pointer use count, but isn't
    /// really copies the object. The underlying implementation is not copyable.
    Widget & operator=(const Widget & other);

    /// Move constructor.
    ///
    /// @note This class is a wrapper around its implementation shared pointer,
    /// so copying it just increasing implementation pointer use count, but isn't
    /// really copies the object. The underlying implementation is not copyable.
    /// @note This constructor does not reset @c other implementation pointer.
    Widget(Widget && other);

    /// Move operator.
    ///
    /// @note This class is a wrapper around its implementation shared pointer,
    /// so copying it just increasing implementation pointer use count, but isn't
    /// really copies the object. The underlying implementation is not copyable.
    /// @note This operator does not reset @c other implementation pointer.
    Widget & operator=(Widget && other);

    /// Constructor with widget implementation shared pointer.
    ///
    /// @warning Unlike some other classes (Painter as an example), the whole
    /// @ref widget_stack "widget stack" is unable to run with pure implementation
    /// pointer, so attempting to construct widget from a pure (@b nullptr) pointer
    /// will cause throwing an user_error exception!
    /// That exception also will be thrown if user tries to
    /// construct derived class from incompatible implementation shared pointer.
    ///
    /// @throw user_error in case of pure implementation pointer or incompatible
    /// implementation pointer class.
    Widget(Widget_ptr wp);

    /// Assign implementation.
    ///
    /// @throw user_error in case of pure implementation pointer or incompatible
    /// implementation pointer class.
    /// @since 0.4.0
    Widget & operator=(Widget_ptr wp);

    /// Gets underlying shared pointer to implementation.
    Widget_ptr ptr() noexcept;

    /// Gets underlying shared pointer to implementation.
    Widget_cptr ptr() const noexcept;

    /// @}
    /// @name Size, position and coordinates
    /// Methods related to widget size, position and coordinates control.
    ///
    /// The library limits the user to the placement of widgets.
    /// The position and size of the widget are completely controlled by the parent container.
    /// Most containers allocate space automatically. And only the Absolute container allows
    /// you to freely manipulate the position and size of your widgets.
    ///
    /// %Widget can inform its parent container about preferred widget size by using set of
    /// <em>hint-methods</em>. For example, the Image widget changes its hints when the pixmap under it
    /// grows up or down. The parent container may or may not take in account that hints. For example,
    /// the Bin container always allocates all available space to its single child
    /// regardless of its size hints. In contrast, the Box always using size hints.
    /// Changing size hints may cause the parent to recalculate available space.
    ///
    /// The Window does not have its own container, so some hints ineffective for windows.
    /// @{

    /// Get widget size in pixels.
    /// Remember, you can not assign an arbitrary size to the widget. The only way to do so
    /// is to put your widget inside of an Absolute container.
    /// In case widget is a Window, the returning value is a size of window client area,
    /// excluding its caption and a frame.
    /// @return size allocated by parent container or by user for windows.
    Size size() const noexcept;

    /// Get widget origin within its parent container.
    /// Remember, you can not assign an arbitrary origin to the widget. The only way to do so
    /// is to put your widget inside of an Absolute container.
    /// In case widget is a Window, the returning value is a offset of window client area
    /// from the screen top left corner.
    /// @return an origin within the parent or within the screen for windows.
    Point origin() const noexcept;

    /// Get size requisition.
    /// Returns size requisition, previously set by require_size() methods.
    /// @return size requisition.
    /// @sa hint_size()
    /// @sa hint_min_size()
    /// @sa hint_max_size()
    /// @sa min_size_hint()
    /// @sa max_size_hint()
    /// @sa require_size()
    /// @sa signal_hints_changed()
    /// @sa signal_requisistion_changed()
    Size required_size() const noexcept;

    /// Get logical size.
    /// Logical size comes into scene when widget inserted into Scroller container.
    /// For now, in that case, returned value is equals to the maximal of required_size() and size().
    /// If widget doesn't inserted into Scroller, return value equals to size().
    /// @sa scrollable()
    /// @sa size()
    /// @sa required_size()
    /// @since 0.6.0
    Size logical_size() const noexcept;

protected:

    /// Set size requisition.
    /// Tells parent container about widget size requisition.
    /// @note This call might emit signal_hints_changed().
    /// @param size required size, values of zero mean "don't care".
    /// @return @b true if changed.
    /// @sa hint_size()
    /// @sa hint_min_size()
    /// @sa hint_max_size()
    /// @sa min_size_hint()
    /// @sa max_size_hint()
    /// @sa required_size()
    /// @sa signal_requisistion_changed()
    bool require_size(const Size & size);

    /// Set size requisition.
    /// Tells parent container about widget size requisition.
    /// @note This call might emit signal_hints_changed().
    /// @param width required width, value of zero mean "don't care".
    /// @param height required width, value of zero mean "don't care".
    /// @return @b true if changed.
    /// @sa hint_size()
    /// @sa hint_min_size()
    /// @sa hint_max_size()
    /// @sa min_size_hint()
    /// @sa max_size_hint()
    /// @sa required_size()
    /// @sa signal_requisistion_changed()
    bool require_size(unsigned width, unsigned height);

public:

    /// Signal is emitted when widget owner (parent) changes widget coordinates.
    /// Slot prototype:
    /// ~~~~~~~~~~~~
    /// void on_origin_changed();
    /// ~~~~~~~~~~~~
    signal<void()> & signal_origin_changed();

    /// Signal is emitted after widget's size had changed.
    /// Slot prototype:
    /// ~~~~~~~~~~~~
    /// void on_size_changed();
    /// ~~~~~~~~~~~~
    signal<void()> & signal_size_changed();

    /// Translate coordinates to screen coordinate system.
    /// @return point translated to screen coordinate system.
    Point to_screen(const Point & pt=Point()) const noexcept;

    /// Translate rectangle coordinates to screen coordinate system.
    /// @return rectangle translated to screen coordinate system.
    /// @since 0.6.0
    Rect to_screen(const Rect & r) const noexcept;

    /// Translate coordinates to known parent container coordinate system.
    /// @return point translated to known container coordinate system.
    /// @sa container()
    /// @sa window()
    /// @sa toplevel()
    Point to_parent(const Container & cont, const Point & pt=Point()) const noexcept;

    /// Translate rectangle coordinates to known parent container coordinate system.
    /// @return rectangle translated to known container coordinate system.
    /// @sa container()
    /// @sa window()
    /// @sa toplevel()
    /// @since 0.6.0
    Rect to_parent(const Container & cont, const Rect & r) const noexcept;

    /// @}
    /// @name Hints
    ///
    /// Widget can have different hints, such as:
    /// - Minimal size hint.
    /// - Maximal size hint.
    /// - Exact size hint.
    /// - Margin hint.
    /// - Aspect ratio hint.
    ///
    /// Each widget has HTML-like margins surrounded it. By default, all margins are
    /// set to zero width (no margins). If widget has assigned margins, that margins
    /// are reserved by owning Container and belongs to that container.
    ///
    /// Most of containers are take in account widget margins, but some of them are not.
    /// As an example, see Scroller and Absolute. Some compound containers does not
    /// support margins.
    ///
    /// Changing hints emits signal_hints_changed().
    ///
    /// @{

    /// Set exact size hint.
    /// Tells parent container about preferred widget size.
    ///
    /// @note This call might emit signal_hints_changed().
    /// @param size preferred size, values of zero mean "don't care".
    /// @return @b true if hint changed.
    /// @sa hint_min_size()
    /// @sa hint_max_size()
    /// @sa size_hint()
    /// @sa min_size_hint()
    /// @sa max_size_hint()
    /// @sa require_size()
    /// @sa required_size()
    /// @sa signal_hints_changed()
    bool hint_size(const Size & size);

    /// Set exact size hint.
    /// Tells parent container about preferred widget size.
    /// @note This call might emit signal_hints_changed().
    /// @param width preferred width, value of zero mean "don't care".
    /// @param height preferred width, value of zero mean "don't care".
    /// @return @b true if hint changed.
    /// @sa hint_min_size()
    /// @sa hint_max_size()
    /// @sa size_hint()
    /// @sa min_size_hint()
    /// @sa max_size_hint()
    /// @sa require_size()
    /// @sa required_size()
    /// @sa signal_hints_changed()
    bool hint_size(unsigned width, unsigned height);

    /// Get size hint.
    /// Returns size hint previously set by hint_size().
    /// @sa hint_size()
    /// @sa hint_min_size()
    /// @sa hint_max_size()
    /// @sa min_size_hint()
    /// @sa max_size_hint()
    /// @sa require_size()
    /// @sa required_size()
    /// @sa signal_hints_changed()
    Size size_hint() const noexcept;

    /// Set minimal size hint.
    /// Tells parent container about minimal widget size.
    /// @note This call might emit signal_hints_changed().
    /// @param size preferred minimal size in pixels, values of zero mean "don't care".
    /// @return @b true if hint changed.
    /// @sa hint_size()
    /// @sa hint_max_size()
    /// @sa min_size_hint()
    /// @sa max_size_hint()
    /// @sa require_size()
    /// @sa required_size()
    /// @sa signal_hints_changed()
    bool hint_min_size(const Size & size);

    /// Set minimal size hint.
    /// Tells parent container about minimal widget size.
    /// @note This call might emit signal_hints_changed().
    /// @param width preferred width, value of zero mean "don't care".
    /// @param height preferred width, value of zero mean "don't care".
    /// @return @b true if hint changed.
    /// @sa hint_size()
    /// @sa hint_max_size()
    /// @sa min_size_hint()
    /// @sa max_size_hint()
    /// @sa require_size()
    /// @sa required_size()
    /// @sa signal_hints_changed()
    bool hint_min_size(unsigned width, unsigned height);

    /// Get minimal size hint previously set by hint_min_size().
    /// @sa hint_size()
    /// @sa hint_min_size()
    /// @sa hint_max_size()
    /// @sa max_size_hint()
    /// @sa require_size()
    /// @sa required_size()
    /// @sa signal_hints_changed()
    Size min_size_hint() const noexcept;

    /// Set maximal size hint.
    /// Tells parent container about maximal widget size.
    /// @note This call might emit signal_hints_changed().
    /// @param size required maximal size in pixels, values of zero mean "don't care".
    /// @return @b true if hint changed.
    /// @sa hint_size()
    /// @sa hint_min_size()
    /// @sa hint_size()
    /// @sa min_size_hint()
    /// @sa max_size_hint()
    /// @sa require_size()
    /// @sa required_size()
    /// @sa signal_hints_changed()
    bool hint_max_size(const Size & size);

    /// Set maximal size hint.
    /// Tells parent container about maximal widget size.
    /// @note This call might emit signal_hints_changed().
    /// @param width preferred maximal width, value of zero mean "don't care".
    /// @param height required maximal width, value of zero mean "don't care".
    /// @return @b true if hint changed.
    /// @sa hint_size()
    /// @sa hint_min_size()
    /// @sa hint_size()
    /// @sa min_size_hint()
    /// @sa max_size_hint()
    /// @sa require_size()
    /// @sa required_size()
    /// @sa signal_hints_changed()
    bool hint_max_size(unsigned width, unsigned height);

    /// Get maximal size hint previously set by hint_max_size().
    /// @sa hint_size()
    /// @sa hint_min_size()
    /// @sa hint_max_size()
    /// @sa min_size_hint()
    /// @sa require_size()
    /// @sa required_size()
    /// @sa signal_hints_changed()
    Size max_size_hint() const noexcept;

    /// Set left margin.
    /// @return @b true if hint changed.
    /// @note This call might emit signal_hints_changed().
    bool hint_margin_left(unsigned left);

    /// Set right margin.
    /// @return @b true if hint changed.
    /// @note This call might emit signal_hints_changed().
    bool hint_margin_right(unsigned right);

    /// Set top margin.
    /// @return @b true if hint changed.
    /// @note This call might emit signal_hints_changed().
    bool hint_margin_top(unsigned top);

    /// Set bottom margin.
    /// @return @b true if hint changed.
    /// @note This call might emit signal_hints_changed().
    bool hint_margin_bottom(unsigned bottom);

    /// Set all margins to the same value.
    /// @return @b true if hint changed.
    /// @note This call might emit signal_hints_changed().
    bool hint_margin(unsigned all);

    /// Set all margins.
    /// @return @b true if hint changed.
    /// @note This call might emit signal_hints_changed().
    bool hint_margin(unsigned left, unsigned right, unsigned top, unsigned bottom);

    /// Set all margins.
    /// @return @b true if hint changed.
    /// @note This call might emit signal_hints_changed().
    /// @since 0.6.0
    bool hint_margin(const Margin & margin);

    /// Get margin hint.
    /// @since 0.6.0
    Margin margin_hint() const noexcept;

    /// Set aspect ratio hint.
    /// Along with require_aspect_ratio(), sets desired aspect ratio.
    /// @note This call has less priority than require_aspect_ratio().
    /// @note This call might emit signal_hints_changed().
    /// @return @b true if hint changed.
    /// @sa require_aspect_ratio()
    /// @sa required_aspect_ratio()
    /// @since 0.6.0
    bool hint_aspect_ratio(double ratio);

    /// Get aspect ratio hint.
    /// Gets aspect ratio combined from values set by hint_aspect_ratio() and require_aspect_ratio() calls.
    /// @sa hint_aspect_ratio()
    /// @sa require_aspect_ratio()
    /// @since 0.6.0
    double required_aspect_ratio() const noexcept;

    /// Test if has aspect ratio hint.
    /// @since 0.7.0
    bool has_aspect_ratio_hint() const noexcept;

protected:

    /// Request aspect ratio.
    /// Along with hint_aspect_ratio(), sets desired aspect ratio.
    /// @note This call has higher priority than hint_aspect_ratio().
    /// @note This call might emit signal_hints_changed().
    /// @return @b true if requisition changed.
    /// @sa hint_aspect_ratio()
    /// @sa required_aspect_ratio()
    /// @since 0.6.0
    bool require_aspect_ratio(double ratio);

public:

    /// Signal emitted when hints changed.
    /// Slot prototype:
    /// ~~~~~~~~~~~~
    /// void on_hints_changed(Hints op);
    /// ~~~~~~~~~~~~
    signal<void(Hints)> & signal_hints_changed();

    /// @}
    /// @name Offset Control (Scrolling, Panning)
    ///
    /// Each widget has builtin scrolling capabilities. However, that capabilities
    /// might be realized only when the widget inserted into special container
    /// named Scroller. If widget does not has Scroller as an immediate parent, the
    /// scrolling will not work.
    ///
    /// @{

    /// Get current offset within owning Scroller.
    /// When widget has Scroller container as an immediate owner, this method
    /// returns its Scroller::pan() value.
    /// When owning widget is not Scroller, the returned value is <b>0:0</b>.
    /// @sa offset()
    /// @sa signal_offset_changed()
    /// @sa Scroller
    Point offset() const noexcept;

    /// Ask owning Scroller to change widget offset.
    /// @param pt new offset value.
    ///
    /// @note This call takes effect only when widget inserted into Scroller container.
    ///
    /// @sa offset(void)
    /// @sa signal_offset_changed()
    /// @sa Scroller
    void offset(const Point & pt);

    /// Ask owning Scroller to change widget offset.
    /// @param x new offset x coordinate.
    /// @param y new offset y coordinate.
    ///
    /// @note This call takes effect only when widget inserted into Scroller container.
    ///
    /// @sa offset()
    /// @sa signal_offset_changed()
    /// @sa Scroller
    void offset(int x, int y);

    /// Signal emitted after widget's offset had changed.
    /// Slot prototype:
    /// ~~~~~~~~~~~~~~~
    /// void on_offset_changed();
    /// ~~~~~~~~~~~~~~~
    ///
    /// @sa offset()
    /// @sa Scroller::pan()
    /// @sa Scroller::signal_pan_changed()
    signal<void()> & signal_offset_changed();

    /// Test if widget inserted into Scroller.
    bool scrollable() const noexcept;

    /// @}
    /// @name Visibility
    /// @{

    /// Hide widget.
    /// From the construction of widget, all widgets (except Window and its descendants) are visible.
    /// The Window is hidden from the construction and needs to call show() to be exposed.
    ///
    /// The widget can be hidden in following cases:
    /// 1. When hide() method called from outside of widget (by the user)
    /// 2. When disappear() method called from inside of widget realization.
    /// After one of these methods called, the owning container deallocates space occupied by widget.
    ///
    /// signal_hide() emitted by this method if widget was not hidden before method call.
    /// signal_invisible() emitted by this method if widget was visible before method call.
    ///
    /// @sa hide()
    /// @sa disappear()
    /// @sa appear()
    /// @sa hidden()
    /// @sa visible()
    /// @sa signal_show()
    /// @sa signal_hide()
    void hide();

    /// Show widget.
    /// Along with appear(), shows the widget.
    ///
    /// @sa hide()
    /// @sa disappear()
    /// @sa appear()
    /// @sa hidden()
    /// @sa visible()
    /// @sa signal_show()
    /// @sa signal_hide()
    void show();

    /// Parametric form of functions show() and hide().
    /// @param yes enable or disable.
    /// Calls show() if @a yes is @b true and hide() otherwise.
    /// @since 0.5.0
    void par_show(bool yes);

protected:

    /// Hide widget.
    /// Along with hide(), hides the widget.
    ///
    /// In contrast to hide(), this method is protected and intended to internal widget realization.
    /// For example, the Menu_item uses this method to hide itself when menu item becomes invisible.
    ///
    /// signal_hide() emitted by this method if widget was not hidden before method call.
    /// signal_invisible() emitted by this method if widget was visible before method call.
    ///
    /// @sa show()
    /// @sa hide()
    /// @sa hidden()
    /// @sa visible()
    /// @sa appear()
    /// @sa signal_show()
    /// @sa signal_hide()
    void disappear();

    /// Show widget.
    /// Along with show(), shows the widget.
    ///
    /// In contrast to show(), this method is protected and intended to internal widget realization.
    /// For example, the Menu_item uses this method to show itself when menu item becomes visible.
    ///
    /// @sa show()
    /// @sa hide()
    /// @sa hidden()
    /// @sa visible()
    /// @sa disappear()
    /// @sa signal_show()
    /// @sa signal_hide()
    void appear();

public:

    /// Determines if widget hidden.
    /// The widget can be hidden in two cases:
    /// - when hide() method called from outside of widget (by the user)
    /// - when disappear() method called from inside of widget realization.
    ///
    /// @sa show()
    /// @sa hide()
    /// @sa visible()
    /// @sa appear()
    /// @sa disappear()
    /// @sa signal_show()
    /// @sa signal_hide()
    bool hidden() const noexcept;

    /// Determines if widget and it's parent are visible.
    ///
    /// The widget is visible, if:
    /// -   widget is not hidden by calling hide() or disappear().
    /// -   widget owner is visible (if Container owns widget) or Display (in case widget is Window) allows window
    ///     to be shown.
    ///
    /// @sa show()
    /// @sa hide()
    /// @sa hidden()
    /// @sa appear()
    /// @sa disappear()
    /// @sa signal_show()
    /// @sa signal_hide()
    bool visible() const noexcept;

    /// Signal is emitted when widget becomes invisible.
    /// Slot prototype:
    /// ~~~~~~~~~~~~
    /// void on_hide();
    /// ~~~~~~~~~~~~
    /// @sa show()
    /// @sa hide()
    /// @sa hidden()
    /// @sa visible()
    /// @sa appear()
    /// @sa disappear()
    /// @sa signal_show()
    signal<void()> & signal_hide();

    /// Signal is emitted when widget becomes visible.
    /// Slot prototype:
    /// ~~~~~~~~~~~~
    /// void on_show();
    /// ~~~~~~~~~~~~
    /// @sa show()
    /// @sa hide()
    /// @sa hidden()
    /// @sa visible()
    /// @sa appear()
    /// @sa disappear()
    /// @sa signal_hide()
    signal<void()> & signal_show();

    /// Get part of the widget currently viewable by user.
    /// This function takes in account current scroll (pan) position.
    /// If widget scrolled out somehow, the returned rectangle is not equal to the bounds occupied by widget.
    /// ~~~~~~~~~~~~~~~~~
    /// bool fully_visible = viewable_area() == Rect(size());
    /// ~~~~~~~~~~~~~~~~~
    /// If widget fully scrolled out, the return value is empty rectangle.
    /// @since 0.4.0
    Rect viewable_area() const noexcept;

    /// @}
    /// @name Sensitivity
    /// @{

    /// Enable widget sensitivity.
    ///
    /// From the construction, the widget is sensitive.
    ///
    /// @sa disable()
    /// @sa freeze()
    /// @sa thaw()
    /// @sa disabled()
    /// @sa frozen()
    /// @sa enabled()
    void enable();

    /// Disable widget sensitivity.
    ///
    /// From the construction, the widget is enabled.
    ///
    /// @sa enable()
    /// @sa freeze()
    /// @sa thaw()
    /// @sa disabled()
    /// @sa frozen()
    /// @sa enabled()
    void disable();

    /// Parametric form of functions enable() and disable().
    /// @param yes enable or disable.
    /// Calls enable() if @a yes is @b true and disable() otherwise.
    /// @since 0.5.0
    void par_enable(bool yes);

protected:

    /// Disable widget sensitivity.
    ///
    /// Along with disable(), disables the widget sensitivity.
    /// In contrast to disable(), this method is protected and
    /// intended for internal widget realization. For example, the
    /// Menu_item uses this method to disable itself when menu item
    /// becomes disabled.
    ///
    /// @sa enable()
    /// @sa disable()
    /// @sa thaw()
    /// @sa disabled()
    /// @sa frozen()
    /// @sa enabled()
    void freeze();

    /// Enable widget sensitivity.
    ///
    /// Along with enable(), enables the widget sensitivity.
    /// In contrast to enable(), this method is protected and
    /// intended to internal widget realization. For example, the
    /// Menu_item uses this method to enable itself when menu item
    /// becomes enabled.
    ///
    /// @sa enable()
    /// @sa disable()
    /// @sa freeze()
    /// @sa disabled()
    /// @sa frozen()
    /// @sa enabled()
    void thaw();

public:

    /// @brief Determines if widget and it's parent are sensitive.
    /// The widget is sensitive if it not disabled by calling disable() or freeze()
    /// and, in addition, it's parent container is also sensitive.
    /// @sa enable()
    /// @sa disable()
    /// @sa freeze()
    /// @sa thaw()
    /// @sa disabled()
    /// @sa frozen()
    /// @sa signal_enable()
    /// @sa signal_disable()
    bool enabled() const noexcept;

    /// @brief Determines if widget is disabled.
    /// The widget can be disabled by calling these methods:
    /// 1. disable()
    /// 2. freeze()
    ///
    /// From the construction, the widget is enabled.
    ///
    /// @sa enable()
    /// @sa disable()
    /// @sa freeze()
    /// @sa thaw()
    /// @sa enabled()
    /// @sa frozen()
    bool disabled() const noexcept;

    /// @brief Determines if widget is disabled by using freeze() method.
    /// From the construction, the widget is not frozen.
    ///
    /// @sa enable()
    /// @sa disable()
    /// @sa freeze()
    /// @sa thaw()
    /// @sa enabled()
    /// @sa disabled()
    bool frozen() const noexcept;

    /// Signal emitted when widget becomes enabled.
    /// Slot prototype:
    /// ~~~~~~~~~~~~
    /// void on_enable();
    /// ~~~~~~~~~~~~
    /// @sa enable()
    /// @sa thaw()
    /// @sa signal_disable()
    signal<void()> & signal_enable();

    /// Signal emitted when widget becomes disabled.
    /// Slot prototype:
    /// ~~~~~~~~~~~~
    /// void on_disable();
    /// ~~~~~~~~~~~~
    /// @sa disable()
    /// @sa freeze()
    /// @sa signal_enable()
    signal<void()> & signal_disable();

    /// @}
    /// @name Focus & Selection Management
    /// Each widget can have a keyboard focus.
    /// By default, focusing of a widget disallowed. But you can allow it
    /// or disallow at any time.
    /// @{

    /// Allow widget focusing.
    /// By default, focusing is disallowed.
    /// @sa disallow_focus()
    /// @sa focusable()
    void allow_focus();

    /// Disallow widget focusing.
    /// @sa allow_focus()
    /// @sa focusable()
    /// By default, focusing is disallowed.
    void disallow_focus();

    /// Test if focusable.
    /// @sa allow_focus()
    /// @sa disallow_focus()
    bool focusable() const noexcept;

    /// Causes widget to have the keyboard focus.
    /// This call might fail in the following cases:
    /// 1. The widget hierarchy is incomplete and has no Window at the top.
    /// 2. There are modal focus elsewhere.
    /// 3. The widget does not allowed to gain focus, i.e. allow_focus() was not called.
    /// @sa grab_modal()
    /// @sa drop_focus()
    /// @return @b true if widget gained keyboard focus.
    bool grab_focus();

    /// Release keyboard focus.
    /// After this call there are no focused widgets within owning window.
    /// This method can not release a modal focus.
    void drop_focus();

    /// Determines if the widget is on the focus path within its parents.
    /// This method also returns @b true if widget has a modal focus.
    bool focused() const noexcept;

    /// Invite the widget to grab focus.
    /// To do this method worked, the widget must connect its signal_take_focus()
    /// to the internal handler that will decide, wether widget likes to grab_focus() at the
    /// moment or not. The handler may call grab_focus() or simply return @b true to prevent
    /// futher signal emission and actually do not grab_focus().
    /// @sa signal_take_focus()
    /// @return @b true if widget gained keyboard focus.
    bool take_focus();

    /// Causes widget to have modal focus.
    /// After widget gained modal focus, only call to end_modal() can allow
    /// other widgets to gain any type of focus.
    /// To make this call successful, you must allow focus by calling allow_focus().
    /// @return @b true on success.
    ///
    /// This call might fail in the following cases:
    /// - The widget hierarchy is incomplete and has no Window at the top.
    /// - There are another modal focus elsewhere.
    /// - The widget does not allowed to gain focus, ie allow_focus() was not called.
    ///
    /// @sa end_modal()
    /// @sa grabs_modal()
    /// @sa allow_focus()
    bool grab_modal();

    /// Causes widget to release modal focus.
    /// @sa grab_modal()
    /// @sa grab_focus()
    /// @sa grabs_modal()
    /// @sa focused()
    /// @return @b true if modal focus was released successfuly.
    bool end_modal();

    /// Determines if the widget is the final acceptor of the modal focus.
    bool grabs_modal() const noexcept;

    /// Signal emitted when widget gains keyboard focus.
    /// Slot prototype:
    /// ~~~~~~~~~~~~
    /// void on_focus_in();
    /// ~~~~~~~~~~~~
    /// @sa grab_focus()
    /// @sa drop_focus()
    signal<void()> & signal_focus_in();

    /// Signal is emitted when widget loses keyboard focus.
    /// Slot prototype:
    /// ~~~~~~~~~~~~
    /// void on_focus_out();
    /// ~~~~~~~~~~~~
    /// @sa grab_focus()
    /// @sa drop_focus()
    signal<void()> & signal_focus_out();

    /// Signal emitted when widget invited to grab focus.
    /// This signal emitted by calling take_focus() method.
    /// Slot prototype:
    /// ~~~~~~~~~~~~
    /// bool on_take_focus();
    /// ~~~~~~~~~~~~
    /// Return @b true from here to stop further signal processing.
    /// @sa take_focus()
    signal<bool()> & signal_take_focus();

    /// Test if  selected.
    /// @since 0.6.0
    bool selected() const noexcept;

    /// Signal is emitted when widget inserted into selection.
    /// Slot prototype:
    /// ~~~~~~~~~~~~
    /// void on_select();
    /// ~~~~~~~~~~~~
    /// The widget can be inserted into container utilized function of selection,
    /// such as List. In that case the container emits this signal on each widget
    /// entering selection.
    /// @sa signal_unselect()
    signal<void()> & signal_select();

    /// Signal is emitted when widget removed from selection.
    /// Slot prototype:
    /// ~~~~~~~~~~~~
    /// void on_unselect();
    /// ~~~~~~~~~~~~
    /// The widget can be inserted into container utilized function of selection,
    /// such as List. In that case the container emits this signal on each widget
    /// leaving selection.
    /// @sa signal_select()
    signal<void()> & signal_unselect();

    /// @}
    /// @name Mouse
    /// @{

    /// Determines if mouse pointer is within widget bounds.
    bool hover() const noexcept;

    /// Get mouse pointer position relatively to the widget origin.
    /// The resulting point may be placed outside of widget drawing area.
    Point where_mouse() const noexcept;

    /// Actively grabs control of the pointer. Further pointer events are reported only to the grabbing widget.
    /// @return @b true on success.
    bool grab_mouse();

    /// Releases the mouse pointer and any queued events if you actively grabbed the pointer before using grab_mouse.
    /// @return @b true on success.
    bool ungrab_mouse();

    /// Determines is mouse pointer grabbed by widget.
    bool grabs_mouse() const noexcept;

    /// Set cursor.
    void set_cursor(Cursor cursor);

    /// Set cursor by name and, optionally, size.
    /// @param name the cursor name.
    /// @param size the cursor size in pixels, 0 for default size.
    void set_cursor(const ustring & name, unsigned size=0);

    /// Read back cursor set by set_cursor().
    /// @note You can not read system cursor image here.
    Cursor cursor() noexcept;

    /// Unset cursor.
    /// Unsetting cursor causes using parent's cursor.
    void unset_cursor();

    /// Show cursor.
    /// By default, the cursor is not hidden.
    /// @sa hide_cursor()
    /// @sa cursor_visible()
    void show_cursor();

    /// Hide cursor.
    /// By default, the cursor is not hidden.
    /// @sa show_cursor()
    /// @sa cursor_visible()
    void hide_cursor();

    /// Determines whether mouse cursor is hidden.
    /// The cursor can be hidden only by calling hide_cursor().
    /// By default, the cursor is not hidden.
    /// @sa hide_cursor()
    /// @sa show_cursor()
    bool cursor_hidden() const noexcept;

    /// Determine whether mouse cursor is visible or not.
    /// The cursor visible when it is not hidden by calling hide_cursor()
    /// and parent's cursor visible too.
    /// @sa hide_cursor()
    /// @sa show_cursor()
    bool cursor_visible() const noexcept;

    /// Signal is emitted when mouse button pressed.
    /// Slot prototype:
    /// ~~~~
    /// bool on_mouse_down(int mbt, int mm, const Point & position) {
    ///     // ... do something ...
    ///                     // Return true if you handle this signal
    ///     return true;    // or return false if your are not in interest of this signal.
    /// }
    /// ~~~~
    ///
    /// Here is:
    /// - @b mbt is mouse button number, see #Mouse_buttons enum.
    /// - @b mm  is a mouse modifier mask, see #Mouse_modifiers enum.
    ///
    /// Multiple slots may be connected to the signal.
    /// Return @b true from the signal handler to disable further signal emission.
    signal<bool(int, int, Point)> & signal_mouse_down();

    /// Signal is emitted when the mouse button double pressed.
    /// Slot prototype:
    /// ~~~~
    /// bool on_mouse_double_click(int mbt, int mm, const Point & position) {
    ///     // ... do something ...
    ///                     // Return true if you handle this signal
    ///     return true;    // or return false if your are not in interest of this signal.
    /// }
    /// ~~~~
    ///
    /// Here is:
    /// - @b mbt is mouse button number, see #Mouse_buttons enum.
    /// - @b mm  is a mouse modifier mask, see #Mouse_modifiers enum.
    ///
    /// Multiple slots may be connected to the signal.
    /// Return @b true from the signal handler to disable further signal emission.
    signal<bool(int, int, Point)> & signal_mouse_double_click();

    /// Signal is emitted when the mouse button released.
    /// Slot prototype:
    /// ~~~~
    /// bool on_mouse_up(int mbt, int mm, const Point & position) {
    ///     // ... do something ...
    ///                     // Return true if you handle this signal
    ///     return true;    // or return false if your are not in interest of this signal.
    /// }
    /// ~~~~
    ///
    /// Here is:
    /// - @b mbt is mouse button number, see #Mouse_buttons enum.
    /// - @b mm  is a mouse modifier mask, see #Mouse_modifiers enum.
    ///
    /// Multiple slots may be connected to the signal.
    /// Return @b true from the signal handler to disable further signal emission.
    signal<bool(int, int, Point)> & signal_mouse_up();

    /// Signal is emitted while the mouse pointer moves.
    /// Slot prototype:
    /// ~~~~~~~~~~~~
    /// void on_mouse_motion(int mm, const Point & position);
    /// ~~~~~~~~~~~~
    ///
    /// Here is:
    /// - @b mm  is a mouse modifier mask, see #Mouse_modifiers enum.
    signal<void(int, Point)> & signal_mouse_motion();

    /// Signal is emitted when the mouse pointer entered widget.
    /// Slot prototype:
    /// ~~~~~~~~~~~~
    /// void on_mouse_enter(const Point & position);
    /// ~~~~~~~~~~~~
    signal<void(Point)> & signal_mouse_enter();

    /// Signal is emitted when the mouse pointer leaved widget.
    /// Slot prototype:
    /// ~~~~~~~~~~~~
    /// void on_mouse_leave();
    /// ~~~~~~~~~~~~
    signal<void()> & signal_mouse_leave();

    /// Signal is emitted when the mouse wheel rotates.
    /// Slot prototype:
    /// ~~~~~~~~~~~~
    /// bool on_mouse_wheel(int delta, int mm, const Point & position);
    /// ~~~~~~~~~~~~
    ///
    ///
    /// Here is:
    /// - @b delta  the positive value is for right or down direction,
    ///             the negative one is for left or up direction.
    /// - @b mm     is a mouse modifier mask, see #Mouse_modifiers enum.
    ///
    /// Multiple slots may be connected to the signal.
    /// Return @b true from the signal handler to disable further signal emission.
    signal<bool(int, int, Point)> & signal_mouse_wheel();

    /// @}
    /// @name Tool Tips
    /// @{

    /// Set tool tip as text.
    /// @param s        text to be displayed.
    /// @param align    text alignment.
    void set_tooltip(const ustring & s, Align align=Align::FILL);

    /// Set tool tip as Widget.
    /// @param w        widget to be used as tooltip.
    void set_tooltip(Widget & w);

    /// Test if widget has tool tip.
    bool has_tooltip() const noexcept;

    /// Unset tool tip.
    void unset_tooltip();

    /// Allow tool tip aapearence on mouse enter.
    /// @remark Allowed by default.
    /// @since 0.6.0
    void allow_tooltip();

    /// Disallow tool tip aapearence on mouse enter.
    /// @remark Allowed by default.
    /// @since 0.6.0
    void disallow_tooltip();

    /// Test if tool tip aapearence allowed.
    /// @since 0.6.0
    bool tooltip_allowed() const noexcept;

    /// Show tool tip now with specified text.
    /// @param s        text to be displayed.
    /// @param align    text alignment.
    /// @return         created Label widget implementation pointer.
    Widget_ptr show_tooltip(const ustring & s, Align align=Align::FILL);

    /// Show tool tip now with specified text at specified position and during specified time.
    /// @param s        text to be displayed.
    /// @param pt       point where tool tip will be placed.
    /// @param gravity  Popup window gravity.
    /// @param time_ms  show time in milliseconds, 0 for default value.
    /// @return         created Label widget implementation pointer.
    Widget_ptr show_tooltip(const ustring & s, const Point & pt, Gravity gravity, unsigned time_ms=0);

    /// Show tool tip now with specified text at specified position and during specified time.
    /// @param s        text to be displayed.
    /// @param align    text alignment.
    /// @param pt       point where tool tip will be placed.
    /// @param gravity  Popup window gravity.
    /// @param time_ms  show time in milliseconds, 0 for default value.
    /// @return         created Label widget implementation pointer.
    Widget_ptr show_tooltip(const ustring & s, Align align, const Point & pt, Gravity gravity, unsigned time_ms=0);

    /// Show tool tip now as widget.
    /// @param w        widget to be used as tooltip.
    void show_tooltip(Widget & w);

    /// Show tool tip now as widget at specified position and during specified time.
    /// @param w        widget to be used as tooltip.
    /// @param pt       point where tool tip will be placed.
    /// @param gravity  Popup window gravity.
    /// @param time_ms  show time in milliseconds, 0 for default value.
    void show_tooltip(Widget & w, const Point & pt, Gravity gravity, unsigned time_ms=0);

    /// Hide tool tip.
    /// Hides currently displayed tooltip.
    void hide_tooltip();

    /// @}
    /// @name Painting
    /// All painting operations are performed with the help of a Painter object.
    ///
    /// You can obtain painter object in two ways:
    /// -   Wait until signal_backpaint() and signal_paint() arrives with bound painter.
    /// -   Get painter by calling painter() method.
    ///
    /// ### System Repaint Event ###
    ///
    /// The system generated rendering event are divided into two phases.
    /// Two successive signals are emitted by the library:
    ///
    /// -   signal_backpaint() comes first and emitting over entire hierarchy of widgets.
    ///     Widget class has default signal_backpaint() handler connected to the signal
    ///     during Widget class construction. That handler do nothing if <i>%conf().get("background").is_set()</i>
    ///     (see Conf::get() and Conf_item::is_set() methods) returns @b false.
    ///     If it return @b true, the handler simply fill all accessible drawing area
    ///     by background color set by user with calling <i>%conf().get("background").set()</i> method
    ///     (see Conf_item::set()). Thus, if no one widget within hierarchy have its
    ///     background set, only top level window renders its background and all other widgets within
    ///     hierarchy shares its background with owning window. The Window class has its background set
    ///     by Display during window creation procedure. The programmer can unset widget background
    ///     by calling <i>%conf().get("background").unset()</i> call (see Conf_item::unset()).
    /// -   After all background handlers invoked, signal_paint() arrives. This signal used to draw foreground
    ///     only. The generic Widget class has no default handler for that signal, but every widget having its
    ///     graphical content, connect its handlers to render itself.
    /// @{

    /// Signal is emitted when a widget is supposed to render it's background.
    /// This %signal emitted when graphical system sends event for repaint,
    /// such as ExposeEvent in X11 or WM_PAINT message on Windows.
    ///
    /// Multiple slots may be connected to the signal.
    /// Return @b true from signal handler to disable other handlers.
    ///
    /// Slot prototype:
    /// ~~~~~~~~~~~~~~~
    /// bool on_backpaint(Painter_ptr pr, const Rect & inval) {
    ///     // ... do painting here ...
    ///     return false;   // Return false to allow other possible handlers.
    /// }
    /// ~~~~~~~~~~~~~~~
    ///
    /// Here is:
    /// -   @b pr    the Painter used for painting operations.
    /// -   @b inval the rectangle to be repainted (you can ignore it for simplicity).
    ///
    /// @note the invalidated rectangle, @e inval, is translated by offset() during
    /// this signal emission, so coordinates shown by it, is logical, not physical.
    ///
    /// @sa signal_paint()
    /// @sa painter()
    /// @sa invalidate()
    /// @sa Window::update()
    signal<bool(Painter, Rect)> & signal_backpaint();

    /// Signal is emitted when a widget is supposed to render itself.
    /// This %signal emitted when graphical system sends event for repaint,
    /// such as ExposeEvent in X11 or WM_PAINT message on Windows.
    ///
    /// Multiple slots may be connected to the signal.
    /// Return @b true from signal handler to disable other handlers.
    /// Slot prototype:
    /// ~~~~~~~~~~~~
    /// bool on_paint(Painter_ptr pr, const Rect & inval) {
    ///     // ... do painting here ...
    ///     return false;   // Return false to allow other possible handlers.
    /// }
    /// ~~~~~~~~~~~~
    /// Here is:
    /// -   @b pr    the Painter used for painting operations.
    /// -   @b inval the rectangle to be repainted (you can ignore it for simplicity).
    ///
    /// @note the invalidated rectangle, @e inval, is translated by offset() during
    /// this signal emission, so coordinates shown by it, is logical, not physical.
    ///
    /// @sa signal_backpaint()
    /// @sa painter()
    /// @sa invalidate()
    /// @sa Window::update()
    signal<bool(Painter, Rect)> & signal_paint();

    /// Obtains a painter object.
    ///
    /// Gets so called private painter.
    /// The rendering of the widget can be done in three ways:
    /// -   By calling invalidate() method and than waiting for signal_backpaint() and
    ///     signal_paint() emission.
    /// -   The graphical system can emit two above signals when other window overlaps owning window or
    ///     owning window has been restored or its size has been changed.
    /// -   The widget can redraw itself at any time by using this method.
    ///
    /// The returned painter object can be empty in case Display object is unavailable, so the programmer
    /// can use this method after it receives signal_display_in() %signal.
    ///
    /// The main difference of the painter object returned by this method and painter object used by
    /// signal_backpaint() and signal_paint() signal is a system clip rectangle,
    /// which stored into painter. When painting done using signal's public painter, that clip rectangle
    /// is set to intersection of invalidate rectangle and widget bounds, so isn't entire widget area
    /// may be accessible by the painter. The painter, returned by this method, has full access to entire
    /// widget drawing area. Also, in contrast to signal_paint() and signal_backpaint() painters,
    /// when using private painter, the programmer must repaint widget background manually.
    ///
    /// The painter, returned by this method, can be stored as class member data and will be valid until
    /// widget will be removed from its owning container.
    ///
    /// Since returned painter can be empty, it's necessarily to test it by calling Painter::operator bool().
    ///
    /// Sample:
    /// ~~~~~~~~~~~~~~~~~~~~~~~~
    /// auto pr = this->painter();
    ///
    /// if (pr) {
    ///     // ... good painter ...
    /// }
    ///
    /// else {
    ///     // ... unusable painter ...
    /// }
    /// ~~~~~~~~~~~~~~~~~~~~~~~~
    ///
    /// @return a painter object.
    /// @sa signal_backpaint()
    /// @sa signal_paint()
    /// @sa invalidate()
    /// @sa Window::update()
    Painter painter();

    /// Invalidate widget drawing area.
    ///
    /// Mark widget drawing area as needed to be repainted.
    /// Using this method enqueues signal_backpaint() and signal_paint() emission in some nearest future.
    /// The time point of that future is system dependent, the typical timeout is a few dozens milliseconds.
    ///
    /// @param r the invalidated region, the entire widget drawing area if empty.
    /// @sa signal_backpaint()
    /// @sa signal_paint()
    /// @sa painter()
    /// @sa Window::update()
    void invalidate(const Rect & r=Rect());

    /// @}
    /// @name Keyboard
    /// @{

    /// Signal is emitted when the key is pressed.
    /// Slot prototype:
    /// ~~~~~~~~~~~~
    /// bool on_key_down(char32_t kc, int km);
    /// ~~~~~~~~~~~~
    ///
    /// Here is:
    /// - @b kc is a Unicode value or one of the #Key_codes enum values.
    /// - @b km is a bit mask of #Key_modifiers enum values.
    ///
    /// Multiple slots may be connected to the signal.
    /// Return @b true from signal hander to disable other handlers.
    /// @sa signal_key_up()
    signal<bool(char32_t, int)> & signal_key_down();

    /// Signal is emitted when the key is released.
    /// Slot prototype:
    /// ~~~~~~~~~~~~
    /// bool on_key_up(char32_t kc, int km);
    /// ~~~~~~~~~~~~
    ///
    /// Here is:
    /// - @b kc is a Unicode value or one of the #Key_codes enum values.
    /// - @b km is a bit mask of #Key_modifiers enum values.
    ///
    /// Multiple slots may be connected to the signal.
    /// Return @b true from signal handler to disable other handlers.
    /// @sa signal_key_down()
    signal<bool(char32_t, int)> & signal_key_up();

    /// Signal is emitted when Unicode characters entered from keyboard or somehow.
    /// Slot prototype:
    /// ~~~~~~~~~~~~~~~
    /// bool on_input(const ustring & s, int source);
    /// ~~~~~~~~~~~~~~~
    ///
    /// Here is:
    /// - @b s is a Unicode string.
    /// - @b source the source of input: 0 - keyboard, 1 - clipboard.
    ///
    /// Return @b true from signal handler if you done with input.
    //  TODO Enumerate sources.
    signal<bool(const ustring &, int)> & signal_input();

    /// Connect a keyboard accelerator.
    /// Connection is done by connecting accelerator to the internal implementation
    /// signal managing keyboard accelerators.
    ///
    /// The @b prepend parameter may be used in case the user likes to override an
    /// existing accelerator. If it @b true, the newly connected accelerator will be
    /// placed at the front of the internal signal and will be activated earlier
    /// than existing accelerators.
    ///
    /// The returning @b connection object can be used to suspend accelerator
    /// appearance by calling connection::block() method and than resume appearance
    /// by calling connection::unblock() method or accelerator can be completely
    /// disabled by calling connection::disconnect() method.
    ///
    /// @param accel an accelerator to be connected.
    /// @param prepend if @b true, add accelerator to the front of accelerator list.
    /// @return a connection object.
    /// @sa connect_action()
    connection connect_accel(Accel & accel, bool prepend=false);

    /// Create and connect a keyboard accelerator.
    /// Create an accelerator and store it in the internal memory.
    /// For more information, see connect(Accel &, bool).
    /// @warning Returned pointer never can be @b nullptr and must not be freed or deleted but can be stored!
    /// @since 0.6.0
    Accel * allocate_accel(char32_t kc, int km=KM_NONE, bool prepend=false);

    /// Create and connect a keyboard accelerator.
    /// Create an accelerator and store it in the internal memory.
    /// For more information, see connect(Accel &, bool).
    /// @warning Returned pointer never can be @b nullptr and must not be freed or deleted but can be stored!
    /// @since 0.6.0
    Accel * allocate_accel(const ustring & spec, bool prepend=false);

    /// Connect an action.
    /// Connects all accelerators managed by action.
    /// If some accelerators within given action will be added, changed or removed,
    /// the widget will handle that changes.
    /// If connected action will be destroyed, all accelerator will no longer works.
    /// @param action an action to be connected.
    /// @param prepend if @b true, add accelerators to the front of accelerator list.
    /// @sa connect_accel()
    void connect_action(Action_base & action, bool prepend=false);

    /// @}
    /// @name Object Related Stuff
    /// @{

    /// Gets effective path.
    /// @relates Object::epath()
    /// @since 0.6.0
    ustring epath() const;

    /// Store value as string.
    /// @since 0.7.0
    void set_string(const ustring & s);

    /// Get value as string.
    /// @since 0.7.0
    ustring get_string() const;

    /// Store value as boolean.
    /// @since 0.7.0
    void set_boolean(bool yes);

    /// Get value as boolean.
    /// @since 0.7.0
    bool get_boolean() const;

    /// Store value as integer.
    /// @since 0.7.0
    void set_integer(intmax_t i);

    /// Get value as integer.
    /// @since 0.7.0
    intmax_t get_integer() const;

    /// Store value as double.
    /// @since 0.7.0
    void set_real(double d);

    /// Get value as double.
    /// @since 0.7.0
    double get_real() const;

    /// @}
    /// @name Control
    /// @{

    /// Connect to Loop::signal_run() via management functions.
    /// @since 0.6.0
    connection connect_run(slot<void()> slot, bool prepend=false);

    /// Test if running.
    // TODO Document it.
    bool running() const noexcept;

    /// Test if object is in "shutdown" state.
    /// The "shutdown" state occurs when object is destroying
    /// or removing from parent container is going on. This is a critical state
    /// in the object's life time and some operations must be limited.
    /// @since 0.5.0
    bool in_shutdown() const noexcept;

    /// Get display.
    /// Obtains Display object which widget belongs to.
    /// The returning object may be pure in case widget not yet inserted into hierarchy.
    Display display() noexcept;

    /// Get display.
    /// Obtains Display object which widget belongs to.
    /// The returning object may be pure in case widget not yet inserted into hierarchy.
    const Display display() const noexcept;

    /// Get owning Container implementation pointer.
    /// @since 0.6.0
    Widget_ptr container() noexcept;

    /// Get owning Container implementation pointer.
    /// @since 0.6.0
    Widget_cptr container() const noexcept;

    /// Get owning Window implementation pointer.
    /// @since 0.6.0
    Widget_ptr window() noexcept;

    /// Get owning Window implementation pointer.
    /// @since 0.6.0
    Widget_cptr window() const noexcept;

    /// Get owning Toplevel implementation pointer.
    /// @since 0.6.0
    Widget_ptr toplevel() noexcept;

    /// Get owning Toplevel implementation pointer.
    /// @since 0.6.0
    Widget_cptr toplevel() const noexcept;

    /// Get style.
    Conf & conf() noexcept;

    /// Get style.
    const Conf & conf() const noexcept;

    /// Causes top level window to close, if that window is Dialog.
    void quit_dialog();

    /// Test if widget has parent container.
    bool has_parent() const noexcept;

    /// Increase level.
    /// @since 0.6.0
    void rise();

    /// Increase level.
    /// @since 0.6.0
    void lower();

    /// Get layer number.
    /// @since 0.6.0
    int level() const noexcept;

    /// @overload
    /// Test if widget is ancestor of specified container.
    /// @since 0.4.0
    bool has_parent(const Widget & w) const noexcept;

    /// Test if widget inserted into window.
    bool has_window() const noexcept;

    /// Test if widget has connection to the Display.
    /// @since 0.5.0
    bool has_display() const noexcept;

    /// Get facade.
    /// @since 0.6.0
    Widget * facade();

    /// Get facade.
    /// @since 0.6.0
    const Widget * facade() const;

    /// Signal emitted when Display became available for widget.
    /// From the construction, the widget haven't an access to the owning display.
    /// After widget inserted into hierarchy using some sort of insertion function, provided
    /// by the owning Container, it can gain an access to the owning display. When display
    /// became visible by owning container, it (owning container) emits signal_display_in()
    /// on each of its children.
    ///
    /// Slot prototype:
    /// ~~~~~~~~~~~~~~~
    /// void on_display_in() {
    ///     // At this line a Display object is visible and Widget::display() call returns non-empty result.
    /// }
    /// ~~~~~~~~~~~~~~~
    /// @sa signal_display_out()
    signal<void()> & signal_display_in();

    /// Signal is emitted when Display became unavailable for widget.
    /// Slot prototype:
    /// ~~~~~~~~~~~~~~~
    /// void on_display_out();
    /// ~~~~~~~~~~~~~~~
    /// @sa signal_display_in()
    signal<void()> & signal_display_out();

    /// Signal is emitted when:
    /// - Object inserted into up-level Object (parameter is not @b nullptr);
    /// - Object removed from the up-level Object (parameter is @b nullptr).
    /// Slot prototype:
    /// ~~~~~~~~~~~~~~~
    /// void on_parent(Object * parent);
    /// ~~~~~~~~~~~~~~~
    /// @relates Object
    signal<void(Object *)> & signal_parent();

    /// Signal is emitted when object removed from it's parent.
    /// Slot prototype:
    /// ~~~~~~~~~~~~
    /// void on_unparent();
    /// ~~~~~~~~~~~~
    /// @relates Object
    signal<void()> & signal_unparent();

    /// Signal is emitted when object destroyed.
    /// This is good idea to place this signal emission in derived class destructor.
    /// ~~~~~~~~~~~~
    /// class Derived: public tau::Widget {
    /// public:
    ///     ~Derived() { signal_destroy()(); }
    /// ~~~~~~~~~~~~
    ///
    /// Slot prototype:
    /// ~~~~~~~~~~~~
    /// void on_destroy();
    /// ~~~~~~~~~~~~
    /// @sa in_shutdown()
    signal<void()> & signal_destroy();

    /// @}

protected:

    /// @private
    Widget_ptr  impl;
};

} // namespace tau

#endif // __TAU_WIDGET_HH__

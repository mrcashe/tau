// ----------------------------------------------------------------------------
// Copyright © 2014-2024 Konstantin Shmelkov <mrcashe@gmail.com>.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
// THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
// PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
// CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
// EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
// PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
// OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
// WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
// OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
// EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// ----------------------------------------------------------------------------

/// @file script.hh The Script class.

#ifndef __TAU_SCRIPT_HH__
#define __TAU_SCRIPT_HH__

#include <tau/defs.hh>
#include <string>

namespace tau {

/// A class representing Unicode script according ISO 15924 standard.
/// In Unicode, a script is a collection of letters and other written signs
/// used to represent textual information in one or more writing systems.
/// @ingroup i18n_group
class __TAUEXPORT__ Script {
public:

    /// Constructs script from current locale.
    Script();

    /// Constructs script from id.
    Script(int id);

    /// Copy constructor.
    Script(const Script & other) = default;

    /// Copy operator.
    Script & operator=(const Script & other) = default;

    /// Compare operator.
    bool operator==(const Script & other) const;

    /// Compare operator.
    bool operator!=(const Script & other) const;

    /// Gets ISO 15924 script name.
    /// @return the script name, such as "Arabic", "Cyrillic" etc.
    std::string name() const;

    /// Gets ISO 15924 script code.
    /// @return the script code, such as "Arab", "Cyrl", etc.
    std::string code() const;

    /// Gets ISO 15924 script number.
    /// @return the script number, such as 160 for "Arabic" or 220 for "Cyrillic".
    int id() const;

    /// Create script from it's name.
    static Script from_name(std::string_view name);

    /// Create script from it's code.
    static Script from_code(std::string_view code);

private:

    const Script_data * data;
    Script(const Script_data * datap);
};

} // namespace tau

#endif // __TAU_SCRIPT_HH__

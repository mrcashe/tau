// ----------------------------------------------------------------------------
// SPDX-License-Identifier: BSD-2-Clause
// Copyright © 2014-2024 Konstantin Shmelkov <mrcashe@gmail.com>.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
// THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
// PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
// CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
// EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
// PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
// OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
// WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
// OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
// EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// ----------------------------------------------------------------------------

/// @file buffer-iter.hh The Buffer iterator class.

#ifndef __TAU_BUFFER_ITER_HH__
#define __TAU_BUFFER_ITER_HH__

#include <tau/defs.hh>
#include <tau/ustring.hh>

namespace tau {

/// Shows current position and perform search operations within Buffer.
/// @ingroup text_group
class __TAUEXPORT__ Buffer_citer {
public:

    /// @name Constructors, destructor, operators
    /// @{

    /// Default constructor.
    Buffer_citer();

    /// Copy constructor.
    Buffer_citer(const Buffer_citer & other);

    /// Copy operator.
    Buffer_citer & operator=(const Buffer_citer & other);

    /// Constructor with coordinates.
    /// Constructs Buffer_citer with same buffer as other and different location.
    Buffer_citer(const Buffer_citer & other, std::size_t row, std::size_t col);

    /// Destructor.
   ~Buffer_citer();

    /// @}
    /// @name Retrieving data
    /// @{

    /// Get pointing unicode character.
    char32_t operator*() const noexcept;

    /// Get unicode character with displacement.
    /// @since 0.6.0
    char32_t operator[](ssize_t offset) const noexcept;

    /// Get an UTF-8 text between two iterators.
    ustring str(Buffer_citer other) const;

    /// Get an UTF-8 text containing specified character count.
    ustring str(std::size_t nchars) const;

    /// Get an UTF-32 text between two iterators.
    std::u32string wstr(Buffer_citer other) const;

    /// Get an UTF-32 text containing specified character count.
    std::u32string wstr(std::size_t nchars) const;

    /// @}
    /// @name Controls
    /// @{

    /// Get current row number.
    std::size_t row() const noexcept;

    /// Get current position within the row.
    std::size_t col() const noexcept;

    /// Get text length between two iterators.
    /// @since 0.4.0
    std::size_t length(Buffer_citer other) const;

    /// Compare two positions.
    bool operator==(const Buffer_citer & other) const noexcept;

    /// Compare two positions.
    bool operator!=(const Buffer_citer & other) const noexcept;

    /// Compare two positions.
    bool operator<(const Buffer_citer & other) const noexcept;

    /// Determines whether Buffer_citer points to some place within the buffer or not.
    operator bool() const noexcept;

    /// Make Buffer_citer pointing to nowhere.
    void reset();

    /// Compare text at current position.
    /// Compare given text against text at current position.
    /// @param text text to be compared.
    /// @param advance advance position by the length of given string in case of equality.
    /// @return @b true if both strings are equal.
    bool equals(const ustring & text, bool advance=false) noexcept;

    /// Compare text at current position.
    /// Compare given text against text at current position.
    /// @param text text to be compared.
    /// @param advance advance position by the length of given string in case of equality.
    /// @return @b true if both strings are equal.
    bool equals(const std::u32string & text, bool advance=false) noexcept;

    /// Test if current position ends line or file.
    bool eol() const noexcept;

    /// Test if current position ends file.
    /// This position is also equals to buffer end position.
    bool eof() const noexcept;

    /// Test if current position starts file.
    /// The file start position is always at row 0 and column 0.
    bool sof() const noexcept;

    /// @}
    /// @name Movement
    /// @{

    /// Assign value of other Buffer_citer buffer and position.
    void set(const Buffer_citer & other, std::size_t row, std::size_t col);

    /// Advance pointer by one character.
    Buffer_citer & operator++();

    /// Advance pointer by one character.
    Buffer_citer operator++(int);

    /// Reverse pointer by one character.
    Buffer_citer & operator--();

    /// Reverse pointer by one character.
    Buffer_citer operator--(int);

    /// Move forward.
    Buffer_citer & operator+=(ssize_t npos);

    /// Move backward.
    Buffer_citer & operator-=(ssize_t npos);

    /// Skip any blank characters within current line.
    void skip_blanks() noexcept;

    /// Skip any blank and new line characters.
    void skip_whitespace() noexcept;

    /// Move to specified position.
    void move_to(std::size_t row, std::size_t col) noexcept;

    /// Move to position specified by other iterator.
    /// @since 0.4.0
    void move_to(Buffer_citer other) noexcept;

    /// Move to specified column within current line.
    /// Tries to move to specified position.
    /// If that position does not exist, the position will be set to the rightmost character.
    void move_to(std::size_t col) noexcept;

    /// Move to start of the current line.
    void move_to_sol() noexcept;

    /// Move to first end-of-line character or end-of-file.
    void move_to_eol() noexcept;

    /// Get iterator pointing to the first end-of-line character or end-of-file.
    /// @since 0.6.0
    Buffer_citer at_eol();

    /// Move to start of next row.
    void forward_line() noexcept;

    /// Move to start of previous row.
    void backward_line() noexcept;

    /// Move word left.
    void backward_word() noexcept;

    /// Move word right.
    void forward_word() noexcept;

    /// @}
    /// @name Search
    /// @{

    /// Find character forward.
    /// If specified character found within buffer, the iterator is pointing to it.
    /// Otherwise, the position is EOF.
    /// @return @b true if found.
    bool find(char32_t wc) noexcept;

    /// Find character forward.
    /// If specified character found within buffer, the iterator is pointing to it.
    /// Otherwise, the position is EOF.
    /// @return @b true if found.
    bool find(char32_t wc, Buffer_citer other) noexcept;

    /// Find text forward.
    /// If specified text found within buffer, the iterator is pointing to it.
    /// Otherwise, the position is EOF.
    /// @return @b true if found.
    bool find(const ustring & text) noexcept;

    /// Find text forward.
    /// The search is performed from the current position and until the place specified by the other.
    /// If specified text found within buffer, the iterator is pointing to it.
    /// Otherwise, the position is EOF.
    /// @return @b true if found.
    bool find(const ustring & text, Buffer_citer other) noexcept;

    /// Find text forward.
    /// If specified text found within buffer, the iterator is pointing to it.
    /// Otherwise, the position is EOF.
    /// @return @b true if found.
    bool find(const std::u32string & text) noexcept;

    /// Find text forward.
    /// The search is performed from the current position and until the place specified by the other.
    /// If specified text found within buffer, the iterator is pointing to it.
    /// Otherwise, the position is EOF.
    /// @return @b true if found.
    bool find(const std::u32string & text, Buffer_citer other) noexcept;

    /// Find characters forward.
    /// If any of specified characters found within buffer, the iterator is pointing to it.
    /// Otherwise, the position is EOF.
    /// @return @b true if found.
    bool find_first_of(const ustring & chars) noexcept;

    /// Find characters forward.
    /// The search is performed from the current position and until the place specified by the other.
    /// If any of specified characters found within buffer, the iterator is pointing to it.
    /// Otherwise, the position is EOF.
    /// @return @b true if found.
    bool find_first_of(const ustring & chars, Buffer_citer other) noexcept;

    /// Find characters forward.
    /// If any of specified characters found within buffer, the iterator is pointing to it.
    /// Otherwise, the position is EOF.
    /// @return @b true if found.
    bool find_first_of(const std::u32string & chars) noexcept;

    /// Find characters forward.
    /// The search is performed from the current position and until the place specified by the other.
    /// If any of specified characters found within buffer, the iterator is pointing to it.
    /// Otherwise, the position is EOF.
    /// @return @b true if found.
    bool find_first_of(const std::u32string & chars, Buffer_citer other) noexcept;

    /// Find characters forward.
    /// If any of specified characters not found within buffer, the iterator is pointing to it.
    /// Otherwise, the position is EOF.
    /// @return @b true if found.
    bool find_first_not_of(const ustring & chars) noexcept;

    /// Find characters forward.
    /// The search is performed from the current position and until the place specified by the other.
    /// If any of specified characters not found within buffer, the iterator is pointing to it.
    /// Otherwise, the position is EOF.
    /// @return @b true if found.
    bool find_first_not_of(const ustring & chars, Buffer_citer other) noexcept;

    /// Find characters forward.
    /// If any of specified characters not found within buffer, the iterator is pointing to it.
    /// Otherwise, the position is EOF.
    /// @return @b true if found.
    bool find_first_not_of(const std::u32string & chars) noexcept;

    /// Find characters forward.
    /// The search is performed from the current position and until the place specified by the other.
    /// If any of specified characters not found within buffer, the iterator is pointing to it.
    /// Otherwise, the position is EOF.
    /// @return @b true if found.
    bool find_first_not_of(const std::u32string & chars, Buffer_citer other) noexcept;

    /// @}

private:

    friend class Buffer;
    Buffer_citer(Buffer_citer_impl * p);
    Buffer_citer_impl * impl;
};

/// @relates Buffer_citer
/// @since 0.7.0
Buffer_citer operator+(const Buffer_citer & i, int n);

/// @relates Buffer_citer
/// @since 0.7.0
Buffer_citer operator-(const Buffer_citer & i, int n);

/// @relates Buffer_citer
/// @since 0.7.0
ssize_t operator-(const Buffer_citer & i, const Buffer_citer & j);

} // namespace tau

#endif // __TAU_BUFFER_ITER_HH__

// ----------------------------------------------------------------------------
// SPDX-License-Identifier: BSD-2-Clause
// Copyright © 2014-2024 Konstantin Shmelkov <mrcashe@gmail.com>.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
// THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
// PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
// CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
// EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
// PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
// OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
// WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
// OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
// EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// ----------------------------------------------------------------------------

#ifndef __TAU_FRAME_HH__
#define __TAU_FRAME_HH__

/// @file frame.hh The Frame class definition.

#include <tau/enums.hh>
#include <tau/container.hh>

namespace tau {

/// Single child container with border and optional text label.
///
/// @note This class is a wrapper around its implementation shared pointer.
///
/// @ingroup flat_container_group
class __TAUEXPORT__ Frame: public Container {
public:

    /// @name Constructors & Operators
    /// @{

    /// Default constructor.
    /// Constructs frame without border.
    Frame();

    /// Constructor with a label and alignment.
    /// @param label            label.
    /// @param align            label alignment.
    /// The border type will be Border::GROOVE and size of 3px, border radius is 0.
    Frame(const ustring & label, Align align=Align::CENTER);

    /// Constructor with border style, width and radius.
    /// @param bs               border style.
    /// @param border_width     border width, in pixels.
    /// @param border_radius    border radius, in pixels.
    Frame(Border bs, unsigned border_width=0, unsigned border_radius=0);

    /// Constructor with border style, width and radius.
    /// @param bs               border style.
    /// @param border_width     border width, in pixels.
    /// @param rtop_left        top left border radius, in pixels.
    /// @param rtop_right       top right border radius, in pixels.
    /// @param rbottom_right    bottom right border radius, in pixels.
    /// @param rbottom_left     bottom left border radius, in pixels.
    /// @since 0.7.0
    Frame(Border bs, unsigned border_width, unsigned rtop_left, unsigned rtop_right, unsigned rbottom_right, unsigned rbottom_left);

    /// Constructor with border style, border color, border width and radius.
    /// @param bs               border style.
    /// @param border_color     border color.
    /// @param border_width     border width, in pixels.
    /// @param border_radius    border radius, in pixels.
    /// @since 0.5.0
    Frame(Border bs, const Color & border_color, unsigned border_width=0, unsigned border_radius=0);

    /// Constructor with border style, border color, border width and radius.
    /// @param bs               border style.
    /// @param border_color     border color.
    /// @param border_width     border width, in pixels.
    /// @param rtop_left        top left border radius, in pixels.
    /// @param rtop_right       top right border radius, in pixels.
    /// @param rbottom_right    bottom right border radius, in pixels.
    /// @param rbottom_left     bottom left border radius, in pixels.
    /// @since 0.7.0
    Frame(Border bs, const Color & border_color, unsigned border_width, unsigned rtop_left, unsigned rtop_right, unsigned rbottom_right, unsigned rbottom_left);

    /// Constructor with label, border style, width and radius.
    /// @param label            label.
    /// @param bs               border style.
    /// @param border_width     border width, in pixels.
    /// @param border_radius    border radius, in pixels.
    Frame(const ustring & label, Border bs, unsigned border_width=0, unsigned border_radius=0);

    /// Constructor with label, border style, width and radius.
    /// @param label            label.
    /// @param bs               border style.
    /// @param border_width     border width, in pixels.
    /// @param rtop_left        top left border radius, in pixels.
    /// @param rtop_right       top right border radius, in pixels.
    /// @param rbottom_right    bottom right border radius, in pixels.
    /// @param rbottom_left     bottom left border radius, in pixels.
    /// @since 0.7.0
    Frame(const ustring & label, Border bs, unsigned border_width, unsigned rtop_left, unsigned rtop_right, unsigned rbottom_right, unsigned rbottom_left);

    /// Constructor with label, border style, border color, border width and radius.
    /// @param label            label.
    /// @param bs               border style.
    /// @param border_color     border color.
    /// @param border_width     border width, in pixels.
    /// @param border_radius    border radius, in pixels.
    /// @since 0.5.0
    Frame(const ustring & label, Border bs, const Color & border_color, unsigned border_width=0, unsigned border_radius=0);

    /// Constructor with label, border style, border color, border width and radius.
    /// @param label            label.
    /// @param bs               border style.
    /// @param border_color     border color.
    /// @param border_width     border width, in pixels.
    /// @param rtop_left        top left border radius, in pixels.
    /// @param rtop_right       top right border radius, in pixels.
    /// @param rbottom_right    bottom right border radius, in pixels.
    /// @param rbottom_left     bottom left border radius, in pixels.
    /// @since 0.7.0
    Frame(const ustring & label, Border bs, const Color & border_color, unsigned border_width, unsigned rtop_left, unsigned rtop_right, unsigned rbottom_right, unsigned rbottom_left);

    /// Constructor with label, alignment, border style, border width and radius.
    /// @param label            label.
    /// @param align            label alignment.
    /// @param bs               border style.
    /// @param border_width     border width, in pixels.
    /// @param border_radius    border radius, in pixels.
    /// @note By default, the label alignment is Align::CENTER.
    Frame(const ustring & label, Align align, Border bs, unsigned border_width=0, unsigned border_radius=0);

    /// Constructor with label, alignment, border style, border width and radius.
    /// @param label            label.
    /// @param align            label alignment.
    /// @param bs               border style.
    /// @param border_width     border width, in pixels.
    /// @param rtop_left        top left border radius, in pixels.
    /// @param rtop_right       top right border radius, in pixels.
    /// @param rbottom_right    bottom right border radius, in pixels.
    /// @param rbottom_left     bottom left border radius, in pixels.
    /// @note By default, the label alignment is Align::CENTER.
    /// @since 0.7.0
    Frame(const ustring & label, Align align, Border bs, unsigned border_width, unsigned rtop_left, unsigned rtop_right, unsigned rbottom_right, unsigned rbottom_left);

    /// Constructor with label, alignment, border style, border color, border width and radius.
    /// @note By default, the label alignment is Align::CENTER.
    /// @since 0.5.0
    Frame(const ustring & label, Align align, Border bs, const Color & border_color, unsigned border_width=0, unsigned border_radius=0);

    /// Constructor with label, alignment, border style, border color, border width and radius.
    /// @param label            label.
    /// @param align            label alignment.
    /// @param bs               border style.
    /// @param border_color     border color.
    /// @param border_width     border width, in pixels.
    /// @param rtop_left        top left border radius, in pixels.
    /// @param rtop_right       top right border radius, in pixels.
    /// @param rbottom_right    bottom right border radius, in pixels.
    /// @param rbottom_left     bottom left border radius, in pixels.
    /// @note By default, the label alignment is Align::CENTER.
    /// @since 0.7.0
    Frame(const ustring & label, Align align, Border bs, const Color & border_color, unsigned border_width, unsigned rtop_left, unsigned rtop_right, unsigned rbottom_right, unsigned rbottom_left);

    /// Copy constructor.
    ///
    /// @note This class is a wrapper around its implementation shared pointer,
    /// so copying it just increasing implementation pointer use count, but isn't
    /// really copies the object. The underlying implementation is not copyable.
    Frame(const Frame & other);

    /// Copy operator.
    ///
    /// @note This class is a wrapper around its implementation shared pointer,
    /// so copying it just increasing implementation pointer use count, but isn't
    /// really copies the object. The underlying implementation is not copyable.
    Frame & operator=(const Frame & other);

    /// Move constructor.
    ///
    /// @note This class is a wrapper around its implementation shared pointer,
    /// so copying it just increasing implementation pointer use count, but isn't
    /// really copies the object. The underlying implementation is not copyable.
    /// @note This constructor does not reset @c other implementation pointer.
    Frame(Frame && other);

    /// Move operator.
    ///
    /// @note This class is a wrapper around its implementation shared pointer,
    /// so copying it just increasing implementation pointer use count, but isn't
    /// really copies the object. The underlying implementation is not copyable.
    /// @note This operator does not reset @c other implementation pointer.
    Frame & operator=(Frame && other);

    /// Constructor with implementation pointer.
    ///
    /// @warning Unlike some other classes (Painter as an example), the whole
    /// @ref widget_stack "widget stack" is unable to run with pure implementation
    /// pointer, so attempting to construct widget from a pure (@b nullptr) pointer
    /// will cause throwing an user_error exception!
    /// That exception also will be thrown if user tries to construct the object
    /// from incompatible implementation shared pointer.
    ///
    /// @throw user_error in case of pure implementation pointer or incompatible
    /// implementation pointer class.
    /// @since 0.4.0
    Frame(Widget_ptr wp);

    /// Assign implementation.
    ///
    /// @throw user_error in case of pure implementation pointer or incompatible
    /// implementation pointer class.
    /// @since 0.4.0
    Frame & operator=(Widget_ptr wp);

    /// @}
    /// @name Widget Management
    /// @{

    /// Set owning widget.
    /// @throw user_error if widget already inserted into another container.
    void insert(Widget & w);

    /// Unset owning widget.
    void clear();

    /// Test if empty.
    /// @since 0.5.0
    bool empty() const noexcept;

    /// Get owning widget implementation pointer.
    /// @since 0.7.0
    Widget_ptr widget() noexcept;

    /// Get owning widget implementation pointer.
    /// @since 0.7.0
    Widget_cptr widget() const noexcept;

    /// @}
    /// @name Label Management
    /// @{

    /// Set label text.
    void set_label(const ustring & label, Align align=Align::CENTER);

    /// Set label text.
    /// @since 0.6.0
    void set_label(const ustring & label, Side label_pos, Align align=Align::CENTER);

    /// Set label widget.
    /// @throw user_error if widget already inserted into another container.
    void set_label(Widget & widget, Align align=Align::CENTER);

    /// Set label widget.
    /// @throw user_error if widget already inserted into another container.
    /// @since 0.6.0
    void set_label(Widget & widget, Side label_pos, Align align=Align::CENTER);

    /// Remove label.
    void unset_label();

    /// Test if has label.
    /// @since 0.6.0
    bool has_label() const noexcept;

    /// Get label implementation pointer.
    /// @note This call may be expensive, so keep returned pointer.
    /// @since 0.6.0
    Widget_ptr label() noexcept;

    /// Get label implementation pointer.
    /// @note This call may be expensive, so keep returned pointer.
    /// @since 0.6.0
    Widget_cptr label() const noexcept;

    /// Set label align.
    /// Emits signal_border_changed() on change.
    /// @note By default, the alignment is Align::CENTER.
    void align_label(Align align);

    /// Get label align.
    Align label_align() const noexcept;

    /// Set label position.
    /// Emits signal_border_changed() on change.
    void move_label(Side label_pos);

    /// Get label position.
    Side where_label() const noexcept;

    /// @}
    /// @name Borders
    /// @{

    /// Test if has visible border.
    /// Border may be invisible when Border::NONE style used or border size set to 0.
    /// @since 0.6.0
    bool border_visible() const noexcept;

    /// Set border width.
    /// @param npx border width in pixels.
    /// Emits signal_border_changed() on change.
    void set_border(unsigned npx);

    /// Set border width in pixels.
    /// @param left left border width in pixels.
    /// @param right right border width in pixels.
    /// @param top top border width in pixels.
    /// @param bottom bottom border width in pixels.
    /// Emits signal_border_changed() on change.
    void set_border(unsigned left, unsigned right, unsigned top, unsigned bottom);

    /// Set border width and style.
    /// @param px the border width, in pixels.
    /// @param bs the border style.
    /// Emits signal_border_changed() on change.
    void set_border(unsigned px, Border bs);

    /// Set border width, style and radius.
    /// @param px the border width, in pixels.
    /// @param bs the border style.
    /// @param radius the border radius, in pixels.
    /// Emits signal_border_changed() on change.
    void set_border(unsigned px, Border bs, unsigned radius);

    /// Set border width, style and radius.
    /// @param px the border width, in pixels.
    /// @param bs the border style.
    /// @param rtop_left        top left border radius, in pixels.
    /// @param rtop_right       top right border radius, in pixels.
    /// @param rbottom_right    bottom right border radius, in pixels.
    /// @param rbottom_left     bottom left border radius, in pixels.
    /// Emits signal_border_changed() on change.
    /// @since 0.7.0
    void set_border(unsigned px, Border bs, unsigned rtop_left, unsigned rtop_right, unsigned rbottom_right, unsigned rbottom_left);

    /// Set border width, style and color.
    /// @param px the border width, in pixels.
    /// @param bs the border style.
    /// @param color the color.
    /// Emits signal_border_changed() on change.
    void set_border(unsigned px, Border bs, const Color & color);

    /// Set border width, style, color and radius.
    /// @param px the border width, in pixels.
    /// @param bs the border style.
    /// @param color the color.
    /// @param radius the border radius, in pixels.
    /// Emits signal_border_changed() on change.
    void set_border(unsigned px, Border bs, const Color & color, unsigned radius);

    /// Set border width, style, color and radius.
    /// @param px the border width, in pixels.
    /// @param bs the border style.
    /// @param color the color.
    /// @param rtop_left        top left border radius, in pixels.
    /// @param rtop_right       top right border radius, in pixels.
    /// @param rbottom_right    bottom right border radius, in pixels.
    /// @param rbottom_left     bottom left border radius, in pixels.
    /// Emits signal_border_changed() on change.
    /// @since 0.7.0
    void set_border(unsigned px, Border bs, const Color & color, unsigned rtop_left, unsigned rtop_right, unsigned rbottom_right, unsigned rbottom_left);

    /// Set left border width in pixels.
    /// @param npx left border width in pixels.
    /// Emits signal_border_changed() on change.
    void set_border_left(unsigned npx);

    /// Set right border width in pixels.
    /// @param npx right border width in pixels.
    /// Emits signal_border_changed() on change.
    void set_border_right(unsigned npx);

    /// Set top border width in pixels.
    /// @param npx top border width in pixels.
    /// Emits signal_border_changed() on change.
    void set_border_top(unsigned npx);

    /// Set bottom border width in pixels and style.
    /// @param npx bottom border width in pixels.
    /// Emits signal_border_changed() on change.
    void set_border_bottom(unsigned npx);

    /// Set left border width in pixels and style.
    /// @param px left border width in pixels.
    /// @param bs the border style.
    /// Emits signal_border_changed() on change.
    void set_border_left(unsigned px, Border bs);

    /// Set right border width in pixels and style.
    /// @param px right border width in pixels.
    /// @param bs the border style.
    /// Emits signal_border_changed() on change.
    void set_border_right(unsigned px, Border bs);

    /// Set top border width in pixels and style.
    /// @param px top border width in pixels.
    /// @param bs the border style.
    /// Emits signal_border_changed() on change.
    void set_border_top(unsigned px, Border bs);

    /// Set bottom border width in pixels and style.
    /// @param px bottom border width in pixels.
    /// @param bs the border style.
    /// Emits signal_border_changed() on change.
    void set_border_bottom(unsigned px, Border bs);

    /// Set left border width, style and color.
    /// @param px border width in pixels.
    /// @param bs the border style.
    /// @param color the color.
    /// Emits signal_border_changed() on change.
    void set_border_left(unsigned px, Border bs, const Color & color);

    /// Set right border width, style and color.
    /// @param px border width in pixels.
    /// @param bs the border style.
    /// @param color the color.
    /// Emits signal_border_changed() on change.
    void set_border_right(unsigned px, Border bs, const Color & color);

    /// Set top border width, style and color.
    /// @param px border width in pixels.
    /// @param bs the border style.
    /// @param color the color.
    /// Emits signal_border_changed() on change.
    void set_border_top(unsigned px, Border bs, const Color & color);

    /// Set bottom border width, style and color.
    /// @param px border width in pixels.
    /// @param bs the border style.
    /// @param color the color.
    /// Emits signal_border_changed() on change.
    void set_border_bottom(unsigned px, Border bs, const Color & color);

    /// Get left border width in pixels.
    unsigned border_left() const noexcept;

    /// Get right border width in pixels.
    unsigned border_right() const noexcept;

    /// Get top border width in pixels.
    unsigned border_top() const noexcept;

    /// Get bottom border width in pixels.
    unsigned border_bottom() const noexcept;

    /// Set all borders color.
    /// @param color the color.
    /// Emits signal_border_changed() on change.
    void set_border_color(const Color & color);

    /// Set all borders color.
    /// @param left left border color.
    /// @param right right border color.
    /// @param top top border color.
    /// @param bottom bottom border color.
    /// Emits signal_border_changed() on change.
    void set_border_color(const Color & left, const Color & right, const Color & top, const Color & bottom);

    /// Set left border color.
    /// @param color the color.
    /// Emits signal_border_changed() on change.
    void set_border_left_color(const Color & color);

    /// Set right border color.
    /// @param color the color.
    /// Emits signal_border_changed() on change.
    void set_border_right_color(const Color & color);

    /// Set top border color.
    /// @param color the color.
    /// Emits signal_border_changed() on change.
    void set_border_top_color(const Color & color);

    /// Set bottom border color.
    /// @param color the color.
    /// Emits signal_border_changed() on change.
    void set_border_bottom_color(const Color & color);

    /// Set default border color.
    /// Emits signal_border_changed() on change.
    void unset_border_color();

    /// Set default left border color.
    /// Emits signal_border_changed() on change.
    void unset_border_left_color();

    /// Set default right border color.
    /// Emits signal_border_changed() on change.
    void unset_border_right_color();

    /// Set default top border color.
    /// Emits signal_border_changed() on change.
    void unset_border_top_color();

    /// Set default bottom border color.
    /// Emits signal_border_changed() on change.
    void unset_border_bottom_color();

    /// Get left border color.
    Color border_left_color() const noexcept;

    /// Get right border color.
    Color border_right_color() const noexcept;

    /// Get top border color.
    Color border_top_color() const noexcept;

    /// Get bottom border color.
    Color border_bottom_color() const noexcept;

    /// Set border style.
    /// @param bs the border style.
    /// Emits signal_border_changed() on change.
    void set_border_style(Border bs);

    /// Set border style.
    /// @param left left border style.
    /// @param right right border style.
    /// @param top top border style.
    /// @param bottom bottom border style.
    /// Emits signal_border_changed() on change.
    void set_border_style(Border left, Border right, Border top, Border bottom);

    /// Set left border style.
    /// @param bs the border style.
    /// Emits signal_border_changed() on change.
    void set_border_left_style(Border bs);

    /// Set right border style.
    /// @param bs the border style.
    /// Emits signal_border_changed() on change.
    void set_border_right_style(Border bs);

    /// Set top border style.
    /// @param bs the border style.
    /// Emits signal_border_changed() on change.
    void set_border_top_style(Border bs);

    /// Set bottom border style.
    /// @param bs the border style.
    /// Emits signal_border_changed() on change.
    void set_border_bottom_style(Border bs);

    /// Get left border style.
    Border border_left_style() const noexcept;

    /// Get right border style.
    Border border_right_style() const noexcept;

    /// Get top border style.
    Border border_top_style() const noexcept;

    /// Get bottom border style.
    Border border_bottom_style() const noexcept;

    /// Set border radius.
    /// Emits signal_border_changed() on change.
    /// @since 0.7.0
    void set_border_top_left_radius(unsigned radius);

    /// Set border radius.
    /// Emits signal_border_changed() on change.
    /// @since 0.7.0
    void set_border_top_right_radius(unsigned radius);

    /// Set border radius.
    /// Emits signal_border_changed() on change.
    /// @since 0.7.0
    void set_border_bottom_right_radius(unsigned radius);

    /// Set border radius.
    /// Emits signal_border_changed() on change.
    /// @since 0.7.0
    void set_border_bottom_left_radius(unsigned radius);

    /// Set border radius.
    /// Emits signal_border_changed() on change.
    /// @since 0.7.0
    void set_border_radius(unsigned radius);

    /// Set border radius.
    /// Emits signal_border_changed() on change.
    /// @since 0.7.0
    void set_border_radius(unsigned top_left, unsigned top_right, unsigned bottom_right, unsigned bottom_left);

    /// Get top left border radius.
    /// @since 0.6.0
    unsigned border_top_left_radius() const noexcept;

    /// Get top right border radius.
    /// @since 0.6.0
    unsigned border_top_right_radius() const noexcept;

    /// Get bottom left border radius.
    /// @since 0.6.0
    unsigned border_bottom_left_radius() const noexcept;

    /// Get bottom right border radius.
    /// @since 0.6.0
    unsigned border_bottom_right_radius() const noexcept;

    /// @}
    /// @name Signals
    /// @{

    /// Signal emitted when border parameters changed.
    /// Emitted when one of following parameters changed:
    /// - Border style
    /// - Border size
    /// - Border radius
    /// - Border color
    /// - Label position.
    ///
    /// Slot prototype:
    /// ~~~~~~~~~~~~~~~
    /// void on_border_changed();
    /// ~~~~~~~~~~~~~~~
    /// @since 0.6.0
    signal<void()> & signal_border_changed();

    /// @}
};

} // namespace tau

#endif // __TAU_FRAME_HH__

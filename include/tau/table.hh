// ----------------------------------------------------------------------------
// SPDX-License-Identifier: BSD-2-Clause
// Copyright © 2014-2024 Konstantin Shmelkov <mrcashe@gmail.com>.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
// THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
// PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
// CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
// EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
// PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
// OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
// WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
// OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
// EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// ----------------------------------------------------------------------------

/// @file table.hh The Table class.

#ifndef __TAU_TABLE_HH__
#define __TAU_TABLE_HH__

#include <tau/container.hh>
#include <tau/enums.hh>

namespace tau {

/// A container which arranges its child widgets in rows and columns.
///
/// @note This class is a wrapper around its implementation shared pointer.
///
/// The table places widgets in two-dimensional space using X and Y axes.
/// The X and Y cell coordinates may be both negative or positive excluding
/// INT_MIN and INT_MAX values.
/// %Table can select widgets within specified range.
/// @ingroup layered_container_group
class __TAUEXPORT__ Table: public Container {
public:

    /// @name Constructors and Operators
    /// @{

    /// Default constructor.
    Table();

    /// Constructor with spacings.
    /// @since 0.4.0
    explicit Table(unsigned xspacing, unsigned yspacing);

    /// Constructor with spacing.
    /// @since 0.4.0
    explicit Table(unsigned spacing);

    /// Constructor with alignment.
    /// @since 0.5.0
    explicit Table(Align xalign, Align yalign);

    /// Constructor with alignment and spacing.
    /// @since 0.5.0
    explicit Table(Align xalign, Align yalign, unsigned spacing);

    /// Constructor with alignment and spacings.
    /// @since 0.5.0
    explicit Table(Align xalign, Align yalign, unsigned xspacing, unsigned yspacing);

    /// Copy constructor.
    ///
    /// @note This class is a wrapper around its implementation shared pointer,
    /// so copying it just increasing implementation pointer use count, but isn't
    /// really copies the object. The underlying implementation is not copyable.
    Table(const Table & other);

    /// Copy operator.
    ///
    /// @note This class is a wrapper around its implementation shared pointer,
    /// so copying it just increasing implementation pointer use count, but isn't
    /// really copies the object. The underlying implementation is not copyable.
    Table & operator=(const Table & other);

    /// Move constructor.
    ///
    /// @note This class is a wrapper around its implementation shared pointer,
    /// so copying it just increasing implementation pointer use count, but isn't
    /// really copies the object. The underlying implementation is not copyable.
    /// @note This constructor does not reset @c other implementation pointer.
    Table(Table && other);

    /// Move operator.
    ///
    /// @note This class is a wrapper around its implementation shared pointer,
    /// so copying it just increasing implementation pointer use count, but isn't
    /// really copies the object. The underlying implementation is not copyable.
    /// @note This operator does not reset @c other implementation pointer.
    Table & operator=(Table && other);

    /// Constructor with implementation pointer.
    ///
    /// @warning Unlike some other classes (Painter as an example), the whole
    /// @ref widget_stack "widget stack" is unable to run with pure implementation
    /// pointer, so attempting to construct widget from a pure (@b nullptr) pointer
    /// will cause throwing an user_error exception!
    /// That exception also will be thrown if user tries to construct the object
    /// from incompatible implementation shared pointer.
    ///
    /// @throw user_error in case of pure implementation pointer or incompatible
    /// implementation pointer class.
    /// @since 0.4.0
    Table(Widget_ptr wp);

    /// Assign implementation.
    ///
    /// @throw user_error in case of pure implementation pointer or incompatible
    /// implementation pointer class.
    /// @since 0.4.0
    Table & operator=(Widget_ptr wp);

    /// @}
    /// @name Widgets Insertion/Movement/Removal
    /// @{

    /// Put widget.
    /// Puts widget using cell coordinates.
    /// @param w the widget to be inserted.
    /// @param x the X cell coordinate of top left corner.
    /// @param y the Y cell coordinate of top left corner.
    /// @param xspan the number of cells to be occupied along X axis.
    /// @param yspan the number of cells to be occupied along Y axis.
    /// @param xsh shrink child horizontally.
    /// @param ysh shrink child vertically.
    /// @throw user_error if widget already inserted into another container.
    void put(Widget & w, int x=0, int y=0, unsigned xspan=1, unsigned yspan=1, bool xsh=false, bool ysh=false);

    /// Put widget.
    /// Puts widget using Span.
    /// @overload
    /// @param w the widget to be inserted.
    /// @param span the widget span in cells.
    /// @param xsh shrink child horizontally.
    /// @param ysh shrink child vertically.
    /// @throw user_error if widget already inserted into another container.
    /// @since 0.6.0
    void put(Widget & w, const Span & span, bool xsh=false, bool ysh=false);

    /// Put widget.
    /// Puts widget using cell coordinates and alignment.
    /// @overload
    /// @param w the widget to be inserted.
    /// @param xalign align along X axis.
    /// @param yalign align along Y axis.
    /// @param x the X cell coordinate of top left corner.
    /// @param y the Y cell coordinate of top left corner.
    /// @param xspan the number of cells to be occupied along X axis.
    /// @param yspan the number of cells to be occupied along Y axis.
    /// @param xsh shrink child horizontally.
    /// @param ysh shrink child vertically.
    /// @throw user_error if widget already inserted into another container.
    /// @since 0.6.0
    void put(Widget & w, Align xalign, Align yalign=Align::CENTER, int x=0, int y=0, unsigned xspan=1, unsigned yspan=1, bool xsh=false, bool ysh=false);

    /// Put widget.
    /// Puts widget using Span and alignment.
    /// @overload
    /// @param w the widget to be inserted.
    /// @param xalign align along X axis.
    /// @param yalign align along Y axis.
    /// @param span the widget span in cells.
    /// @param xsh shrink child horizontally.
    /// @param ysh shrink child vertically.
    /// @throw user_error if widget already inserted into another container.
    /// @since 0.6.0
    void put(Widget & w, Align xalign, Align yalign, const Span & span, bool xsh=false, bool ysh=false);

    /// Remove specified widget.
    /// @param w widget to be removed.
    /// @return count of removed widgets.
    std::size_t remove(Widget & w);

    /// Remove all widgets within specified range.
    /// @param xmin minimal X coordinate.
    /// @param ymin minimal Y coordinate.
    /// @param xspan the number of cells to be freed along X axis.
    /// @param yspan the number of cells to be freed along Y axis.
    /// @return count of removed widgets.
    std::size_t remove(int xmin, int ymin, unsigned xspan=1, unsigned yspan=1);

    /// Remove all widgets within specified range.
    /// @param range the range
    /// @return count of removed widgets.
    /// @since 0.5.0
    std::size_t remove(const Span & range);

    /// Test if empty.
    /// @since 0.6.0
    bool empty() const noexcept;

    /// Remove all children.
    void clear();

    /// Change widget span in cells using existing shrink options.
    /// @param w the widget.
    /// @param x the X cell coordinate of top left corner.
    /// @param y the Y cell coordinate of top left corner.
    /// @param xspan the number of cells to be occupied along X axis.
    /// @param yspan the number of cells to be occupied along Y axis.
    /// %Widget w must be child of the table using put() method.
    void respan(Widget & w, int x, int y, unsigned xspan, unsigned yspan);

    /// Change widget span in cells using existing shrink options.
    /// @param w the widget.
    /// @param span the widget span in cells.
    /// %Widget w must be child of the table using put() method.
    void respan(Widget & w, const Span & span);

    /// Change widget span in cells.
    /// @param w the widget.
    /// @param x the X cell coordinate of top left corner.
    /// @param y the Y cell coordinate of top left corner.
    /// @param xspan the number of cells to be occupied along X axis.
    /// @param yspan the number of cells to be occupied along Y axis.
    /// @param xsh shrink widget along X axis.
    /// @param ysh shrink widget along X axis.
    /// %Widget w must be child of the table using put() method.
    void respan(Widget & w, int x, int y, unsigned xspan, unsigned yspan, bool xsh, bool ysh);

    /// Change widget span in cells.
    /// @param w the widget.
    /// @param span the widget span in cells.
    /// @param xsh shrink widget along X axis.
    /// @param ysh shrink widget along X axis.
    /// %Widget w must be child of the table using put() method.
    void respan(Widget & w, const Span & span, bool xsh, bool ysh);

    /// Get children within range.
    /// @since 0.6.0
    std::list<Widget_ptr> widgets(int x, int y, std::size_t xspan, std::size_t yspan) const;

    /// Get children within range.
    /// @since 0.6.0
    std::list<Widget_ptr> widgets(const Span & rng) const;

    /// @}
    /// @name Column & Row Inserton/Removal
    /// @{

    /// Insert columns.
    /// @param x the column index.
    /// @param n_columns column count to insert.
    void insert_columns(int x, unsigned n_columns=1);

    /// Insert rows.
    /// @param y the row index.
    /// @param n_rows row count to insert.
    void insert_rows(int y, unsigned n_rows=1);

    /// Remove columns.
    /// The remaining columns are shifted to the left.
    void remove_columns(int x, unsigned n_columns=1);

    /// Remove rows.
    /// The remaining rows are shifted to the top.
    void remove_rows(int y, unsigned n_rows=1);

    /// @}
    /// @name Getting Span & Bounds
    /// @{

    /// Get widget span.
    /// @param w the widget.
    ///
    /// If widget not found, both xmin and ymin are equals to @b INT_MAX and
    /// both xmax and ymax are equals to @b INT_MIN.
    Span span(const Widget & w) const noexcept;

    /// Get table span in cells.
    /// On empty table both xmin and ymin are equals to @b INT_MAX and
    /// both xmax and ymax are equals to @b INT_MIN.
    Span span() const noexcept;

    /// Get column span in cells.
    /// @param col the column number.
    ///
    /// When @a column does not exist, the returning values are @b INT_MAX for Span::xmin and Span::ymin,
    /// @b INT_MIN for Span::xmax and Span::ymax. Otherwise, the returning values are:
    /// - @a column for Span::xmin
    /// - @a column +1 for Span::xmax
    /// - minimal cell number for Span::ymin
    /// - maximal cell number +1 for Span::ymax
    Span column_span(int col) const noexcept;

    /// Get row span in cells.
    /// @param row the row number.
    ///
    /// When @a row does not exist, the returning values are @b INT_MAX for Span::xmin and Span::ymin,
    /// @b INT_MIN for Span::xmax and Span::ymax. Otherwise, the returning values are:
    /// - minimal cell number for Span::xmin
    /// - maximal cell number +1 for Span::xmax
    /// - @a row for Span::ymin
    /// - @a row +1 for Span::ymax
    Span row_span(int row) const noexcept;

    /// Get range bounds in pixels.
    /// @param x the X coordinate in cells of top left corner.
    /// @param y the Y coordinate in cells of top left corner.
    /// @param xspan the number of cells along X axis.
    /// @param yspan the number of cells along Y axis.
    /// @param with_margins include margins.
    Rect bounds(int x, int y, unsigned xspan, unsigned yspan, bool with_margins=false) const noexcept;

    /// Get range bounds in pixels.
    /// @param spn the span.
    /// @param with_margins include margins.
    /// @since 0.6.0
    Rect bounds(const Span & spn, bool with_margins=false) const noexcept;

    /// Get column bounds in pixels.
    /// @param column the column number.
    /// @param with_margins include margins.
    /// @return when @a column does not exist, the returning rectangle is empty.
    Rect column_bounds(int column, bool with_margins=false) const noexcept;

    /// Get row bounds in pixels.
    /// @param row the column number.
    /// @param with_margins include margins.
    /// @return when @a row does not exist, the returning rectangle is empty.
    Rect row_bounds(int row, bool with_margins=false) const noexcept;

    /// @}
    /// @name Spacing
    /// @{

    /// Set column and row spacing.
    /// @param xspacing the spacing in pixels along X axis.
    /// @param yspacing the spacing in pixels along Y axis.
    void set_spacing(unsigned xspacing, unsigned yspacing);

    /// Set column spacing in pixels.
    /// @param xspacing the spacing in pixels along X axis.
    void set_column_spacing(unsigned xspacing);

    /// Set row spacing in pixels.
    /// @param yspacing the spacing in pixels along Y axis.
    void set_row_spacing(unsigned yspacing);

    /// Set column and row spacing.
    /// @param spacing the spacing in pixels along both X and Y axes.
    /// @since 0.4.0
    void set_spacing(unsigned spacing);

    /// Get spacing in pixels.
    /// @return an @b std::pair, where @c .first member is for @c xspacing and @c .second is for @c yspacing.
    std::pair<unsigned, unsigned> spacing() const noexcept;

    /// @}
    /// @name Alignment
    /// %Table has three way to align widgets:
    /// 1. Overall alignment, for columns and rows separately.
    /// 2. Alignment for specified column or row separately.
    /// 3. Alignment for specified widget.
    ///
    /// If alignment for widget (#3) does not specified, the implementation trying
    /// to use alignment for columns and rows (#2). If those also not set by user,
    /// the overall alignment (#1) used.
    ///
    /// The default alignment for all 3 domains is Align::CENTER.
    /// @{

    /// Set overall alignment.
    void align(Align xalign, Align yalign);

    /// Set overall column alignment.
    void align_columns(Align xalign);

    /// Set overall row alignment.
    void align_rows(Align xalign);

    /// Get overall alignment.
    /// @return pair where @a .first member is for horizontal alignment and @a .second member is for vertical alignment.
    std::pair<Align, Align> align() const;

    /// Set specified column alignment.
    /// When single column align set, the overall column align ignored.
    /// @param x column index.
    /// @param xalign the align.
    void align_column(int x, Align xalign);

    /// Get specified column alignment.
    /// @param x column index.
    Align column_align(int x) const noexcept;

    /// Unset specified column alignment.
    /// Unsets align prevously set by align_column().
    /// @param x column index.
    void unalign_column(int x);

    /// Set specified row alignment.
    /// When single row align set, the overall row align ignored.
    /// @param y row index.
    /// @param yalign the align.
    void align_row(int y, Align yalign);

    /// Get specified row alignment.
    Align row_align(int y) const noexcept;

    /// Unset specified row alignment.
    /// Unsets row align previously set by align_row().
    /// @param y row index.
    void unalign_row(int y);

    /// Set widget alignment.
    /// @param w the widget.
    /// @param xalign align along X axis.
    /// @param yalign align along Y axis.
    void align(Widget & w, Align xalign, Align yalign=Align::CENTER);

    /// Get widget alignment.
    /// @param[in] w the widget.
    /// @return pair where @a .first member is for horizontal alignment and @a .second member is for vertical alignment.
    std::pair<Align, Align> align(const Widget & w) const;

    /// Unset widget alignment.
    /// @param w the widget.
    void unalign(Widget & w) noexcept;

    /// @}
    /// @name Selection & Marks
    /// The %Table capable to select and mark different regions.
    /// The selection can be applied only to single region and the marks
    /// may be applied to the multiple regions.
    /// @{

    /// Select range with Conf::SELECT_BACKGROUND color.
    /// Removes previous selection and selects specified range.
    /// @since 0.6.0
    void select(const Span & spn);

    /// Select range with Conf::SELECT_BACKGROUND color.
    /// Removes previous selection and selects specified range.
    /// @param x     the X coordinate in cells of top left selection corner.
    /// @param y     the Y coordinate in cells of top left selection corner.
    /// @param xspan the number of cells along X axis in selection.
    /// @param yspan the number of cells along Y axis in selection.
    void select(int x, int y, unsigned xspan=1, unsigned yspan=1);

    /// Remove current selection.
    void unselect();

    /// Test if have selection.
    /// @since 0.4.0
    bool has_selection() const noexcept;

    /// Get current selection coordinates.
    ///
    /// If no selection, both xmin and ymin are equals to @b INT_MAX and
    /// both xmax and ymax are equals to @b INT_MIN.
    Span selection() const noexcept;

    /// Mark specified range with custom color.
    /// The mark added to the list of previous marks.
    /// @since 0.6.0
    void mark_back(const Color & c, const Span & spn, bool with_margins=false);

    /// Mark specified range with custom color.
    /// The mark added to the list of previous marks.
    /// @param c     the color.
    /// @param x     the X coordinate in cells of top left mark corner.
    /// @param y     the Y coordinate in cells of top left mark corner.
    /// @param xspan the number of cells along X axis in mark.
    /// @param yspan the number of cells along Y axis in mark.
    /// @param with_margins include margins
    /// @since 0.6.0
    void mark_back(const Color & c, int x, int y, unsigned xspan, unsigned yspan, bool with_margins=false);

    /// Unmark all.
    void unmark();

    /// Unmark specified range.
    /// @param x     the X coordinate in cells of top left mark corner.
    /// @param y     the Y coordinate in cells of top left mark corner.
    /// @param xspan the number of cells along X axis in mark.
    /// @param yspan the number of cells along Y axis in mark.
    /// @since 0.6.0
    void unmark(int x, int y, unsigned xspan, unsigned yspan);

    /// Unmark specified range.
    /// @since 0.6.0
    void unmark(const Span & spn);

    /// Test if have marks.
    /// @since 0.4.0
    bool has_marks() const noexcept;

    /// Get marks.
    std::vector<Span> marks() const;

    /// @}
    /// @name Margins
    /// @{

    /// Set column margins.
    /// @param x the column coordinate in cells.
    /// @param left the left margin in pixels.
    /// @param right the left margin in pixels.
    /// When single column margins are set, the overall column margins ignored.
    void set_column_margin(int x, unsigned left, unsigned right);

    /// Set column margins.
    /// @param x the column coordinate in cells.
    /// @param margin the margins.
    /// When single column margins are set, the overall column margins ignored.
    /// @since 0.6.0
    void set_column_margin(int x, const Margin & margin);

    /// Set row margins.
    /// @param y the row coordinate in cells.
    /// @param top the top margin in pixels.
    /// @param bottom the bottom margin in pixels.
    /// When single row margins are set, the overall row margins ignored.
    void set_row_margin(int y, unsigned top, unsigned bottom);

    /// Set row margins.
    /// @param y the row coordinate in cells.
    /// @param margin the margins.
    /// When single row margins are set, the overall row margins ignored.
    /// @since 0.6.0
    void set_row_margin(int y, const Margin & margin);

    /// Get specified column margins.
    /// @param x the column coordinate in cells.
    /// @return column margins with top & bottom members set to 0.
    Margin column_margin(int x) const noexcept;

    /// Get specified row margins.
    /// @param y the row coordinate in cells.
    /// @return row margins with left & right members set to 0.
    Margin row_margin(int y) const noexcept;

    /// Set all columns margins.
    /// @param left the left margin in pixels.
    /// @param right the left margin in pixels.
    void set_columns_margin(unsigned left, unsigned right);

    /// Set all columns margins.
    /// .left & .right members used.
    /// @since 0.6.0
    void set_columns_margin(const Margin & margin);

    /// Set all rows margins.
    /// @param top the top margin in pixels.
    /// @param bottom the bottom margin in pixels.
    void set_rows_margin(unsigned top, unsigned bottom);

    /// Set all rows margins.
    /// .top & .bottom members used.
    /// @since 0.6.0
    void set_rows_margin(const Margin & margin);

    /// Get columns margins.
    /// @return columns margins with top & bottom members set to 0.
    Margin columns_margin() const noexcept;

    /// Get rows margins.
    /// @return row margins with left & right members set to 0.
    Margin rows_margin() const noexcept;

    /// @}
    /// @name Columns & Rows Size
    /// @{

    /// Set column width (in pixels).
    /// @param column the column number.
    /// @param width column width in pixels.
    void set_column_width(int column, unsigned width);

    /// Get column width (in pixels).
    /// @param column the column number.
    /// @return column width in pixels previously set by column_width(int, unsigned).
    unsigned column_width(int column) const noexcept;

    /// Set row height (in pixels).
    /// @param row the row number.
    /// @param height row height in pixels.
    void set_row_height(int row, unsigned height);

    /// Get row height (in pixels).
    /// @param row the row number.
    /// @return row height in pixels previously set by set_row_height(int, unsigned).
    unsigned row_height(int row) const noexcept;

    /// Set minimal column width (in pixels).
    /// @param column the column number.
    /// @param width minimal column width in pixels.
    void set_min_column_width(int column, unsigned width);

    /// Get minimal column width (in pixels).
    /// @param column the column number.
    /// @return minimal column width in pixels previously set by set_min_column_width(int, unsigned).
    unsigned min_column_width(int column) const noexcept;

    /// Set minimal row height (in pixels).
    /// @param row the row number.
    /// @param height minimal row height in pixels.
    void set_min_row_height(int row, unsigned height);

    /// Get minimal row height (in pixels).
    /// @param row the row number.
    /// @return minimal row height in pixels previously set by set_min_row_height(int, unsigned).
    unsigned min_row_height(int row) const noexcept;

    /// Set maximal column width (in pixels).
    /// @param column the column number.
    /// @param width maximal column width in pixels.
    void set_max_column_width(int column, unsigned width);

    /// Get maximal column width (in pixels).
    /// @param column the column number.
    /// @return maximal column width in pixels previously set by set_max_column_width(int, unsigned).
    unsigned max_column_width(int column) const noexcept;

    /// Set maximal row height (in pixels).
    /// @param row the row number.
    /// @param height maximal row height in pixels.
    void set_max_row_height(int row, unsigned height);

    /// Get maximal row height (in pixels).
    /// @param row the row number.
    /// @return minimal row height in pixels previously set by set_max_row_height(int, unsigned).
    unsigned max_row_height(int row) const noexcept;

    /// @}
    /// @name Signals
    /// @{

    /// Signal emitted when column bounds changed.
    /// Slot prototype:
    /// ~~~~~~~~~~~~~~
    /// void on_column_bounds_changed(int col);
    /// ~~~~~~~~~~~~~~
    signal<void(int)> & signal_column_bounds_changed();

    /// Signal emitted when row bounds changed.
    /// Slot prototype:
    /// ~~~~~~~~~~~~~~
    /// void on_row_bounds_changed(int row);
    /// ~~~~~~~~~~~~~~
    signal<void(int)> & signal_row_bounds_changed();

    /// Signal emitted when selection changed.
    /// Slot prototype:
    /// ~~~~~~~~~~~~~~
    /// void on_selection_changed();
    /// ~~~~~~~~~~~~~~
    signal<void()> & signal_selection_changed();

    /// @}
};

} // namespace tau

#endif // __TAU_TABLE_HH__

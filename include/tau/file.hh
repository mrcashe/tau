// ----------------------------------------------------------------------------
// SPDX-License-Identifier: BSD-2-Clause
// Copyright © 2014-2024 Konstantin Shmelkov <mrcashe@gmail.com>.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
// THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
// PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
// CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
// EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
// PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
// OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
// WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
// OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
// EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// ----------------------------------------------------------------------------

#ifndef __TAU_FILE_HH__
#define __TAU_FILE_HH__

#include <tau/defs.hh>
#include <tau/signal.hh>
#include <tau/timeval.hh>
#include <optional>

/// @file file.hh File class.

namespace tau {

/// File.
///
/// @note This class is a wrapper around its implementation shared pointer.
///
/// @ingroup file_group
class __TAUEXPORT__ File: public trackable {
public:

    /// File and directory flags.
    /// @ingroup file_flags_group
    enum {

        /// Nothing specified/happens.
        NOTHING         = 0x00000000,

        /// File or directory was accessed.
        ACCESSED        = 0x00000001,

        /// File or directory metadata changed.
        ATTRIB          = 0x00000002,

        /// File or directory was changed.
        CHANGED         = 0x00000004,

        /// File or directory was created.
        CREATED         = 0x00000008,

        /// File or directory was opened.
        OPENED          = 0x00000010,

        /// File or directory was closed.
        CLOSED          = 0x00000020,

        /// File or directory was deleted.
        DELETED         = 0x00000040,

        /// File or directory was moved into watched directory.
        MOVED_IN        = 0x00000080,

        /// File or directory was moved out of watched directory.
        MOVED_OUT       = 0x00000100,

        /// Watched file or directory was moved.
        SELF_MOVED      = 0x00000200,

        /// Watched file or directory was deleted.
        SELF_DELETED    = 0x00000400,

        /// All file events mask.
        EVENTS          = 0x000007ff,

        /// Partition was mounted.
        MOUNT           = 0x00008000,

        /// Partition was unmounted.
        UMOUNT          = 0x00004000,

        /// Mounted/unmounted partition is removable.
        REMOVABLE       = 0x00002000
    };

    /// Default constructor.
    File();

    /// Destructor.
   ~File();

    /// Copy constructor.
    ///
    /// @note This class is a wrapper around its implementation shared pointer,
    /// so copying it just increasing implementation pointer use count, but isn't
    /// really copies the object. The underlying implementation is not copyable.
    File(const File & other) = default;

    /// Copy operator.
    ///
    /// @note This class is a wrapper around its implementation shared pointer,
    /// so copying it just increasing implementation pointer use count, but isn't
    /// really copies the object. The underlying implementation is not copyable.
    File & operator=(const File & other) = default;

    /// Constructor with path.
    File(const ustring & uri);

    /// Test if file exists.
    bool exists() const noexcept;

    /// Test is directory.
    bool is_dir() const noexcept;

    /// Test is symbolic link.
    bool is_link() const noexcept;

    /// Test is regular.
    bool is_regular() const noexcept;

    /// Test if character device file.
    bool is_char() const noexcept;

    /// Test if block device file.
    bool is_block() const noexcept;

    /// Test if pipe.
    bool is_fifo() const noexcept;

    /// Test if socket.
    bool is_socket() const noexcept;

    /// Test if executable.
    bool is_exec() const noexcept;

    /// Test if hidden.
    bool is_hidden() const noexcept;

    /// Test if removable.
    bool is_removable() const noexcept;

    /// Get file size in bytes.
    uintmax_t bytes() const noexcept;

    /// Get file last access time.
    Timeval atime() const noexcept;

    /// Get file creation time.
    Timeval ctime() const noexcept;

    /// Get file last modification time.
    Timeval mtime() const noexcept;

    /// Get link target.
    /// If file is a symbolic link, returns path to the link target.
    /// If file is not a symbolic link, returns path specified by user.
    /// @throw sys_error on operationg system error.
    std::optional<ustring> read_link() const;

    /// Get MIME type.
    /// @since 0.6.0
    std::string mime() const;

    /// Get type title.
    /// @since 0.6.0
    ustring type(const Language & lang=Language()) const;

    /// Get file icon.
    /// @throw sys_error on operationg system error.
    /// @since 0.5.0
    const Pixmap icon(int icon_size) const;

    /// Remove file.
    /// @param opts the options (reserved but not yet implemented).
    /// @param slot_async if not pure, remove file asynchronously (reserved but not yet implemented).
    /// @throw sys_error in case of operating system error.
    void rm(int opts=0, slot<void(int)> slot_async=slot<void(int)>());

    /// Copy file.
    /// @param dest destination URI.
    /// @param opts the options (reserved but not yet implemented).
    /// @throw sys_error in case of operating system error.
    /// @since 0.5.0
    void cp(const ustring & dest, int opts=0);

    /// Signal watch.
    /// Used for file event monitoring.
    /// Slot prototype:
    /// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    /// void on_watch(int event_mask, const ustring & path);
    /// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    /// The meaning of @b event_mask bits can be found in @ref file_flags_group.
    /// @param event_mask same meaning as above.
    signal<void(int, const ustring &)> & signal_watch(int event_mask);

private:

    File_ptr impl;
};

} // namespace tau

#endif // __TAU_FILE_HH__

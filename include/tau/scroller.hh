// ----------------------------------------------------------------------------
// SPDX-License-Identifier: BSD-2-Clause
// Copyright © 2014-2024 Konstantin Shmelkov <mrcashe@gmail.com>.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
// THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
// PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
// CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
// EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
// PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
// OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
// WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
// OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
// EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// ----------------------------------------------------------------------------

#ifndef __TAU_SCROLLER_HH__
#define __TAU_SCROLLER_HH__

/// @file scroller.hh The Scroller class.

#include <tau/container.hh>

namespace tau {

/// Container for a widget that can be scrolled by the user, allowing it to be
/// larger than the physical display.
///
/// @note This class is a wrapper around its implementation shared pointer.
///
/// @ingroup flat_container_group
class __TAUEXPORT__ Scroller: public Container {
public:

    /// @name Constructor and operators
    /// @{

    /// Default constructor.
    Scroller();

    /// Copy constructor.
    ///
    /// @note This class is a wrapper around its implementation shared pointer,
    /// so copying it just increasing implementation pointer use count, but isn't
    /// really copies the object. The underlying implementation is not copyable.
    Scroller(const Scroller & other);

    /// Copy operator.
    ///
    /// @note This class is a wrapper around its implementation shared pointer,
    /// so copying it just increasing implementation pointer use count, but isn't
    /// really copies the object. The underlying implementation is not copyable.
    Scroller & operator=(const Scroller & other);

    /// Move constructor.
    ///
    /// @note This class is a wrapper around its implementation shared pointer,
    /// so copying it just increasing implementation pointer use count, but isn't
    /// really copies the object. The underlying implementation is not copyable.
    /// @note This constructor does not reset @c other implementation pointer.
    Scroller(Scroller && other);

    /// Move operator.
    ///
    /// @note This class is a wrapper around its implementation shared pointer,
    /// so copying it just increasing implementation pointer use count, but isn't
    /// really copies the object. The underlying implementation is not copyable.
    /// @note This operator does not reset @c other implementation pointer.
    Scroller & operator=(Scroller && other);

    /// Constructor with implementation pointer.
    ///
    /// @warning Unlike some other classes (Painter as an example), the whole
    /// @ref widget_stack "widget stack" is unable to run with pure implementation
    /// pointer, so attempting to construct widget from a pure (@b nullptr) pointer
    /// will cause throwing an user_error exception!
    /// That exception also will be thrown if user tries to construct the object
    /// from incompatible implementation shared pointer.
    ///
    /// @throw user_error in case of pure implementation pointer or incompatible
    /// implementation pointer class.
    /// @since 0.4.0
    Scroller(Widget_ptr wp);

    /// Assign implementation.
    /// @throw user_error in case of pure implementation pointer or incompatible implementation pointer class.
    /// @since 0.4.0
    Scroller & operator=(Widget_ptr wp);

    /// @}
    /// @name Child Management
    /// @{

    /// Put owning widget.
    /// @throw user_error if w already inserted into another container.
    void insert(Widget & w);

    /// Get owning widget implementation pointer.
    /// @since 0.6.0
    Widget_ptr widget() noexcept;

    /// Get owning widget implementation pointer.
    /// @since 0.6.0
    Widget_cptr widget() const noexcept;

    /// Remove owning widget.
    void clear();

    /// Test if empty.
    /// @since 0.6.0
    bool empty() const noexcept;

    /// @}
    /// @name Controls
    /// @{

    /// Get logical size in pixels.
    Size pan_size() const;

    /// Scroll child to X position.
    void pan_x(int x);

    /// Scroll child to Y position.
    void pan_y(int y);

    /// Scroll child to desired position.
    void pan(const Point & pos);

    /// Scroll child to desired position.
    void pan(int x, int y);

    /// Scroll to descendant.
    /// @since 0.5.0
    void pan(Widget & w);

    /// Get child scroll value.
    Point pan() const;

    /// Set step values used by action_pan_left(), action_pan_right(), action_pan_up() and action_pan_down().
    /// @sa action_pan_left()
    /// @sa action_pan_right()
    /// @sa action_pan_up()
    /// @sa action_pan_down()
    /// @sa step()
    void set_step(const Point & step);

    /// Set step values used by action_pan_left(), action_pan_right(), action_pan_up() and action_pan_down().
    /// @sa action_pan_left()
    /// @sa action_pan_right()
    /// @sa action_pan_up()
    /// @sa action_pan_down()
    /// @sa step()
    void set_step(int xstep, int ystep);

    /// Get step values used by action_pan_left(), action_pan_right(), action_pan_up() and action_pan_down().
    /// @sa action_pan_left()
    /// @sa action_pan_right()
    /// @sa action_pan_up()
    /// @sa action_pan_down()
    /// @sa step()
    Point step() const;

    /// Set gravity.
    /// @since 0.6.0
    void gravity(Gravity g);

    /// Get gravity.
    /// @since 0.6.0
    Gravity gravity() const noexcept;

    /// @}
    /// @name Actions & Signals
    /// @{

    /// Get pan_left Action.
    /// @sa step(int, int)
    /// @sa step(void)
    Action & action_pan_left();

    /// Get pan_right Action.
    /// @sa step(int, int)
    /// @sa step(void)
    Action & action_pan_right();

    /// Get pan_up Action.
    /// @sa step(int, int)
    /// @sa step(void)
    Action & action_pan_up();

    /// Get pan_down Action.
    /// @sa step(int, int)
    /// @sa step(void)
    Action & action_pan_down();

    /// Get page_up Action.
    Action & action_previous_page();

    /// Get page_down Action.
    Action & action_next_page();

    /// Get home Action.
    Action & action_home();

    /// Get end Action.
    Action & action_end();

    /// Signal emitted when child offset changed.
    /// Slot prototype:
    /// ~~~~~~~~~~~~~~
    /// void on_pan_changed();
    /// ~~~~~~~~~~~~~~
    signal<void()> & signal_pan_changed();

    /// Signal emitted when logical size changed.
    /// Slot prototype:
    /// ~~~~~~~~~~~~~~
    /// void on_pan_size_changed();
    /// ~~~~~~~~~~~~~~
    signal<void()> & signal_pan_size_changed();

    /// @}
};

} // namespace tau

#endif // __TAU_SCROLLER_HH__

// ----------------------------------------------------------------------------
// SPDX-License-Identifier: BSD-2-Clause
// Copyright © 2014-2024 Konstantin Shmelkov <mrcashe@gmail.com>.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
// THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
// PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
// CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
// EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
// PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
// OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
// WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
// OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
// EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// ----------------------------------------------------------------------------

/// @file key-file.hh The Key_file class.

#ifndef __TAU_KEY_FILE_HH__
#define __TAU_KEY_FILE_HH__

#include <tau/defs.hh>
#include <tau/signal.hh>
#include <tau/ustring.hh>
#include <istream>

namespace tau {

/// This class lets you parse, edit or create files containing groups of key-value pairs.
///
/// @ingroup text_group
class __TAUEXPORT__ Key_file: public trackable {
public:

    /// @name Constructors, destructor and operators
    /// @{

    /// @brief Default constructor.
    ///
    /// @param list_sep list separator character.
    /// @param comment_sep list separator character.
    Key_file(char32_t list_sep=U'\0', char32_t comment_sep=U'\0') noexcept;

    /// @brief Constructor with stream.
    /// If the input stream isn't good(), the constructor does nothing.
    ///
    /// @param is the input stream.
    /// @param list_sep list separator character.
    /// @param comment_sep list separator character.
    Key_file(std::istream & is, char32_t list_sep=U'\0', char32_t comment_sep=U'\0') noexcept;

    /// @brief Constructor with stream.
    /// If the input stream isn't good(), the constructor does nothing.
    ///
    /// @param buf the Buffer.
    /// @param list_sep list separator character.
    /// @param comment_sep list separator character.
    /// @since 0.6.0
    Key_file(Buffer buf, char32_t list_sep=U'\0', char32_t comment_sep=U'\0') noexcept;

    /// @brief Constructor with file name.
    /// Given path saved and can be used later to save content using save() method (without arguments).
    ///
    /// @param path path to the file.
    /// @param list_sep list separator character.
    /// @param comment_sep list separator character.
    Key_file(const ustring & path, char32_t list_sep=U'\0', char32_t comment_sep=U'\0') noexcept;

    /// @brief Copy constructor.
    Key_file(const Key_file & other);

    /// @brief Move constructor.
    Key_file(Key_file && other);

    /// @brief Copy operator,
    Key_file & operator=(const Key_file & other);

    /// @brief Move operator,
    Key_file & operator=(Key_file && other);

    /// @brief Destructor.
    /// Since 0.4.0 calls flush().
   ~Key_file();

    /// @}
    /// @name Input/Output
    /// @{

    /// @brief Load from stream.
    /// The current content of the key file is not cleared.
    ///
    /// @throw sys_error in case of OS error.
    void load(std::istream & is);

    /// @brief Load from Buffer.
    /// The current content of the key file is not cleared.
    /// @since 0.6.0
    void load_from_buffer(tau::Buffer buf);

    /// @brief Load from file.
    /// If file was successfuly loaded, given path stored and can be used later
    /// to save file using save() method (without parameters).
    ///
    /// The current content of the key file is not cleared.
    ///
    /// @throw sys_error in case of OS error.
    void load_from_file(const ustring & path);

    /// @brief Create from file.
    /// If specified file exists, the behaviour is same as load_from_file().
    /// Otherwise, save_to_file() called.
    /// @throw sys_error in case of OS error.
    /// @since 0.7.0
    void create_from_file(const ustring & path);

    /// @brief Reload contents from the file.
    /// Try to reload contents from the file named by:
    /// - Key_file(const ustring &, char32_t, char32_t)
    /// - load(const ustring & path)
    /// - save(const ustring & path)
    ///
    /// If specified file exists, the contents is cleared by using clear() method.
    ///
    /// @sa Key_file(const ustring &, char32_t, char32_t)
    /// @sa load_from_file()
    /// @sa save_to_file()
    /// @sa flush()
    /// @sa changed()
    /// @sa signal_changed()
    /// @since 0.5.0
    void reload() noexcept;

    /// @brief Save to stream.
    /// Resets "changed" state (since 0.4.0).
    ///
    /// @throw sys_error in case of OS error.
    /// @sa changed()
    /// @sa signal_changed()
    void save(std::ostream & os) const;

    /// @brief Save to file.
    /// If directory, containing destination file, does not exist, this call
    /// attempts to create it using file_mkdir() call.
    ///
    /// Resets "changed" state (since 0.4.0).
    ///
    /// @throw sys_error in case of OS error.
    /// @sa load_from_file()
    /// @sa flush()
    /// @sa changed()
    /// @sa signal_changed()
    void save_to_file(const ustring & path);

    /// @brief Flush file to the disk.
    /// If key file was loaded from file using load(const ustring &) method,
    /// or was constructed using Key_file(const ustring &, char32_t, char32_t),
    /// this call will save it back in case the key file had changed. If key file was not loaded from the disk
    /// using one of above methods or file specified for load(const ustring &)
    /// or Key_file(const ustring &, char32_t, char32_t) does not exist, this call have no effect.
    ///
    /// If directory, containing destination file, does not exist, this call
    /// attempts to create it using file_mkdir() call.
    ///
    /// Resets "changed" state.
    ///
    /// @since 0.4.0
    /// @sa Key_file(const ustring &, char32_t, char32_t)
    /// @sa load_from_file()
    /// @sa save_to_file()
    /// @sa changed()
    /// @sa signal_changed()
    void flush() noexcept;

    /// @}
    /// @name Getters
    /// @{

    /// @brief Get comment above @a sect section.
    /// @throw user_error if @a sect is foreign (belongs to another Key_file).
    ustring comment(const Key_section & sect) const;

    /// @overload
    /// @brief Get comment above root section.
    /// @since 0.4.0
    ustring comment() const;

    /// @brief Gets string value associated with @a key_name under @a sect section.
    /// @param sect section.
    /// @param key_name key name.
    /// @param fallback value to be returned if @a key not found.
    /// @return key value or fallback value if @a key not found.
    /// @throw user_error if @a sect is foreign (belongs to another Key_file) or @a key_name is invalid.
    ustring get_string(const Key_section & sect, std::string_view key_name, const ustring & fallback=ustring()) const;

    /// @overload
    /// @brief Gets string value associated with @a key_name under root section.
    /// @param key_name key name.
    /// @param fallback value to be returned if @a key_name not found.
    /// @return key value or @a fallback value if @a key_name not found.
    /// @throw user_error if @a key_name is invalid.
    /// @since 0.4.0
    ustring get_string(std::string_view key_name, const ustring & fallback=ustring()) const;

    /// @brief Gets a list of strings associated with @a key_name under @a sect section.
    /// @param sect section.
    /// @param key_name key name.
    /// @param min_size the minimal std::vector size to be returned.
    /// @param fallback value to be returned as extra std::vector elements.
    /// @return list of values or empty list if not found.
    /// @throw user_error if @a sect is foreign (belongs to another Key_file) or @a key_name is invalid.
    std::vector<ustring> get_strings(const Key_section & sect, std::string_view key_name, std::size_t min_size=0, const ustring & fallback=ustring()) const;

    /// @overload
    /// @brief Gets a list of strings associated with @a key_name under root section.
    /// @param key_name key name.
    /// @param min_size the minimal std::vector size to be returned.
    /// @param fallback value to be returned as extra std::vector elements.
    /// @return list of values or empty list if not found.
    /// @throw user_error if @a key_name is invalid.
    /// @since 0.4.0
    std::vector<ustring> get_strings(std::string_view key_name, std::size_t min_size=0, const ustring & fallback=ustring()) const;

    /// @brief Gets boolean value associated with @a key_name under @a sect section.
    /// @param sect section.
    /// @param key_name key name.
    /// @param fallback value to be returned if @a key_name not found.
    /// @return key value or @a fallback if @a key_name not found.
    /// @throw user_error if @a sect is foreign (belongs to another Key_file) or @a key_name is invalid.
    bool get_boolean(const Key_section & sect, std::string_view key_name, bool fallback=false) const;

    /// @overload
    /// @brief Gets boolean value associated with @a key_name under root section.
    /// @param key_name key name.
    /// @param fallback value to be returned if @a key_name not found.
    /// @return key value or @a fallback if @a key_name not found.
    /// @throw user_error if @a key_name is invalid.
    /// @since 0.4.0
    bool get_boolean(std::string_view key_name, bool fallback=false) const;

    /// @brief Gets a list of Boolean's associated with key @a key_name under @a sect section.
    /// @param sect section.
    /// @param key_name key name.
    /// @param min_size the minimal std::vector size to be returned.
    /// @param fallback value to be returned as extra std::vector elements.
    /// @return list of values or empty list if not found.
    /// @throw user_error if @a sect is foreign (belongs to another Key_file) or @a key_name is invalid.
    std::vector<bool> get_booleans(const Key_section & sect, std::string_view key_name, std::size_t min_size=0, bool fallback=false) const;

    /// @overload
    /// @brief Gets a list of Boolean's associated with key @a key_name under root section.
    /// @param key_name key name.
    /// @param min_size the minimal std::vector size to be returned.
    /// @param fallback value to be returned as extra std::vector elements.
    /// @return list of values or empty list if not found.
    /// @throw user_error if @a key_name is invalid.
    /// @since 0.4.0
    std::vector<bool> get_booleans(std::string_view key_name, std::size_t min_size=0, bool fallback=false) const;

    /// @brief Gets integer value associated with @a key_name under @a sect section.
    /// @param sect section.
    /// @param key_name key name.
    /// @param fallback value to be returned if @a key_name not found.
    /// @return key value or @a fallback if @a key_name not found.
    /// @throw user_error if @a sect is foreign (belongs to another Key_file) or @a key_name is invalid.
    intmax_t get_integer(const Key_section & sect, std::string_view key_name, intmax_t fallback=0) const;

    /// @overload
    /// @brief Gets integer value associated with @a key_name under root section.
    /// @param key_name key name.
    /// @param fallback value to be returned if @a key_name not found.
    /// @return key value or @a fallback if @a key_name not found.
    /// @throw user_error if @a key_name is invalid.
    /// @since 0.4.0
    intmax_t get_integer(std::string_view key_name, intmax_t fallback=0) const;

    /// @brief Gets a list of integers associated with @a key_name under @a sect section.
    /// @param sect section.
    /// @param key_name key name.
    /// @param min_size the minimal std::vector size to be returned.
    /// @param fallback value to be returned as extra std::vector elements.
    /// @return list of values or empty list if not found.
    /// @throw user_error if @a sect is foreign (belongs to another Key_file) or @a key_name is invalid.
    std::vector<intmax_t> get_integers(const Key_section & sect, std::string_view key_name, std::size_t min_size=0, intmax_t fallback=0) const;

    /// @overload
    /// @brief Gets a list of integers associated with @a key_name under root section.
    /// @param key_name key name.
    /// @param min_size the minimal std::vector size to be returned.
    /// @param fallback value to be returned as extra std::vector elements.
    /// @return list of values or empty list if not found.
    /// @throw user_error if @a key_name is invalid.
    /// @since 0.4.0
    std::vector<intmax_t> get_integers(std::string_view key_name, std::size_t min_size=0, intmax_t fallback=0) const;

    /// @brief Gets double value associated with @a key_name under @a sect section.
    /// @param sect section.
    /// @param key_name key name.
    /// @param fallback value to be returned if @a key_name not found.
    /// @return key value or @a fallback if @a key_name not found.
    /// @throw user_error if @a sect is foreign (belongs to another Key_file) or @a key_name is invalid.
    double get_double(const Key_section & sect, std::string_view key_name, double fallback=0.0) const;

    /// @overload
    /// @brief Gets double value associated with @a key_name under root section.
    /// @param key_name key name.
    /// @param fallback value to be returned if @a key_name not found.
    /// @return key value or fallback if key not found.
    /// @throw user_error if @a key_name is invalid.
    /// @since 0.4.0
    double get_double(std::string_view key_name, double fallback=0.0) const;

    /// @brief Gets a list of doubles associated with @a key_name under @a sect section.
    /// @param sect section.
    /// @param key_name key name.
    /// @param min_size the minimal std::vector size to be returned.
    /// @param fallback value to be returned as extra std::vector elements.
    /// @return list of values or empty list if not found.
    /// @throw user_error if @a sect is foreign (belongs to another Key_file) or @a key_name is invalid.
    std::vector<double> get_doubles(const Key_section & sect, std::string_view key_name, std::size_t min_size=0, double fallback=0.0) const;

    /// @overload
    /// @brief Gets a list of doubles associated with @a key_name under root section.
    /// @param key_name key name.
    /// @param min_size the minimal std::vector size to be returned.
    /// @param fallback value to be returned as extra std::vector elements.
    /// @return list of values or empty list if not found.
    /// @throw user_error if @a key_name is invalid.
    /// @since 0.4.0
    std::vector<double> get_doubles(std::string_view key_name, std::size_t min_size=0, double fallback=0.0) const;

    /// @}
    /// @name Setters
    /// @{

    /// @brief Places comment above @a sect section.
    /// @throw user_error if @a sect is foreign (belongs to another Key_file).
    void set_comment(Key_section & sect, const ustring & comment);

    /// @overload
    /// @brief Places comment above root section.
    /// @since 0.4.0
    void set_comment(const ustring & comment);

    /// @brief Associates a new string @a value with @a key_name under @a sect section.
    /// @param sect section.
    /// @param key_name key name.
    /// @param value value to be saved.
    /// @throw user_error if @a sect is foreign (belongs to another Key_file) or @a key_name is invalid.
    void set_string(Key_section & sect, std::string_view key_name, const ustring & value);

    /// @overload
    /// @brief Associates a new string @a value with @a key_name under root section.
    /// @param key_name key name.
    /// @param value value to be saved.
    /// @throw user_error if @a key_name is invalid.
    /// @since 0.4.0
    void set_string(std::string_view key_name, const ustring & value);

    /// @brief Sets a list of string values for @a key under @a sect section.
    /// @param sect section.
    /// @param key_name key name.
    /// @param vec vector of values.
    /// @throw user_error if @a sect is foreign (belongs to another Key_file) or @a key_name is invalid.
    void set_strings(Key_section & sect, std::string_view key_name, const std::vector<ustring> & vec);

    /// @overload
    /// @brief Sets a list of string values for @a key_name under root section.
    /// @param key_name key name.
    /// @param vec vector of values.
    /// @throw user_error if @a key_name is invalid.
    /// @since 0.4.0
    void set_strings(std::string_view key_name, const std::vector<ustring> & vec);

    /// @brief Associates a new boolean @a value with @a key_name under @a sect section.
    /// @param sect section.
    /// @param key_name key name.
    /// @param value value to be saved.
    /// @throw user_error if @a sect is foreign (belongs to another Key_file) or @a key_name is invalid.
    void set_boolean(Key_section & sect, std::string_view key_name, bool value);

    /// @overload
    /// @brief Associates a new boolean @a value with @a key_name under root section.
    /// @param key_name key name.
    /// @param value value to be saved.
    /// @throw user_error if @a key_name is invalid.
    /// @since 0.4.0
    void set_boolean(std::string_view key_name, bool value);

    /// @brief Sets a list of Boolean's for the @a key_name under @a sect section.
    /// @param sect section.
    /// @param key_name key name.
    /// @param vec vector of values.
    /// @throw user_error if @a sect is foreign (belongs to another Key_file) or @a key_name is invalid.
    void set_booleans(Key_section & sect, std::string_view key_name, const std::vector<bool> & vec);

    /// @overload
    /// @brief Sets a list of Boolean's for the @a key_name under root section.
    /// @param key_name key name.
    /// @param vec vector of values.
    /// @throw user_error if @a key_name is invalid.
    /// @since 0.4.0
    void set_booleans(std::string_view key_name, const std::vector<bool> & vec);

    /// @brief Associates a new integer @a value with @a key_name under @a sect section.
    /// @param sect section.
    /// @param key_name key name.
    /// @param value value to be saved.
    /// @throw user_error if @a sect is foreign (belongs to another Key_file) or @a key_name is invalid.
    void set_integer(Key_section & sect, std::string_view key_name, intmax_t value);

    /// @overload
    /// @brief Associates a new integer @a value with @a key_name under root section.
    /// @param key_name key name.
    /// @param value value to be saved.
    /// @throw user_error if @a key_name is invalid.
    /// @since 0.4.0
    void set_integer(std::string_view key_name, intmax_t value);

    /// @brief Sets a list of integers for the @a key_name under @a sect section.
    /// @param sect section.
    /// @param key_name key name.
    /// @param vec vector of values.
    /// @throw user_error if @a sect is foreign (belongs to another Key_file) or @a key_name is invalid.
    void set_integers(Key_section & sect, std::string_view key_name, const std::vector<intmax_t> & vec);

    /// @overload
    /// @brief Sets a list of integers for the @a key_name under root section.
    /// @param key_name key name.
    /// @param vec vector of values.
    /// @throw user_error if @a key_name is invalid.
    /// @since 0.4.0
    void set_integers(std::string_view key_name, const std::vector<intmax_t> & vec);

    /// @brief Associates a new double @a value with @a key_name under @a sect section.
    /// @param sect section.
    /// @param key_name key name.
    /// @param value value to be saved.
    /// @throw user_error if @a sect is foreign (belongs to another Key_file) or @a key_name is invalid.
    void set_double(Key_section & sect, std::string_view key_name, double value);

    /// @overload
    /// @brief Associates a new double @a value with @a key_name under root section.
    /// @param key_name key name.
    /// @param value value to be saved.
    /// @throw user_error if @a key_name is invalid.
    /// @since 0.4.0
    void set_double(std::string_view key_name, double value);

    /// @brief Sets a list of doubles for the @a key_name under @a sect section.
    /// @param sect section.
    /// @param key_name key name.
    /// @param vec vector of values.
    /// @throw user_error if @a sect is foreign (belongs to another Key_file) or @a key_name is invalid.
    void set_doubles(Key_section & sect, std::string_view key_name, const std::vector<double> & vec);

    /// @overload
    /// @brief Sets a list of doubles for the @a key_name under root section.
    /// @param key_name key name.
    /// @param vec vector of values.
    /// @throw user_error if @a key_name is invalid.
    /// @since 0.4.0
    void set_doubles(std::string_view key_name, const std::vector<double> & vec);

    /// @{
    /// @name Controls
    /// @{

    /// @brief Set comment separator.
    /// @note By default, the comment separator is a '#'.
    void set_comment_separator(char32_t comment_sep) noexcept;

    /// @brief Sets the character which is used to separate values in lists.
    /// @note By default, the list separator is path_sep().
    void set_list_separator(char32_t list_sep) noexcept;

    /// @brief Gets list separator.
    char32_t list_separator() const noexcept;

    /// @brief Gets comment separator.
    char32_t comment_separator() const noexcept;

    /// @brief Get root section.
    Key_section & root() noexcept;

    /// @brief Get root section.
    const Key_section & root() const noexcept;

    /// @brief Get specified section.
    /// @param sect_name section name.
    /// @param similar if @b true, use str_similar() for lookup (case insensitive), else compare @a sect_name byte-to-byte.
    /// @throw user_error if @a sect_name is invalid.
    Key_section & section(std::string_view sect_name, bool similar=false);

    /// @brief Get specified section.
    /// @param sect_name section name.
    /// @param similar if @b true, use str_similar() for lookup (case insensitive), else compare @a sect_name byte-to-byte.
    /// @throw user_error if @a sect_name is invalid or specified section does not exist.
    const Key_section & section(std::string_view sect_name, bool similar=false) const;

    /// @brief Gets a list of all sections in the Key_file.
    std::vector<std::string> list_sections() const;

    /// @brief Gets a list of all keys within given section.
    /// @throw user_error if @a sect is foreign (belongs to another Key_file) (since 0.4.0).
    std::vector<std::string> list_keys(const Key_section & sect) const;

    /// @brief Test if empty.
    bool empty() const noexcept;

    /// @brief Looks whether the key file has the section @a sect_name.
    /// @param sect_name section name.
    /// @param similar if @b true, use str_similar() for lookup (case insensitive), else compare @a sect_name byte-to-byte.
    /// @throw user_error if @a sect_name is invalid.
    bool has_section(std::string_view sect_name, bool similar=false) const noexcept;

    /// @brief Looks whether the key file has the key @a key_name in the section sect.
    /// @param sect section.
    /// @param key_name key name.
    /// @param similar if @b true, use str_similar() for lookup (case insensitive), else compare @a key_name byte-to-byte.
    /// @throw user_error if @a sect is foreign (belongs to another Key_file) or @a key_name is invalid.
    bool has_key(const Key_section & sect, std::string_view key_name, bool similar=false) const;

    /// @overload
    /// @brief Looks whether the key file has the key @a key_name in the root section.
    /// @param key_name key name.
    /// @param similar if @b true, use str_similar() for lookup (case insensitive), else compare key_name byte-to-byte.
    /// @throw user_error if @a key_name is invalid.
    /// @since 0.4.0
    bool has_key(std::string_view key_name, bool similar=false) const noexcept;

    /// @brief Gets exact key name by similar name within specified section.
    /// @param sect section.
    /// @param similar_name key name.
    /// @throw user_error if @a sect is foreign (belongs to another Key_file) or @a similar_name is invalid.
    std::string key_name(const Key_section & sect, std::string_view similar_name) const;

    /// @overload
    /// @brief Gets exact key name by @a similar_name within root section.
    /// @param similar_name key name.
    /// @throw user_error @a if similar_name is invalid.
    /// @since 0.4.0
    std::string key_name(std::string_view similar_name) const;

    /// @brief Renames section.
    /// @param sect section to be renamed.
    /// @param new_name the new section name.
    /// @throw user_error if @a new_name is invalid or busy or if @a sect is foreign (belongs to another Key_file).
    /// @since 0.5.0
    void rename_section(const Key_section & sect, std::string_view new_name);

    /// @brief Swap sections contents.
    /// @param sect1 first section.
    /// @param sect2 second section.
    /// @throw user_error if @a sect1 or @a sect2 is foreign (belongs to another Key_file).
    /// @since 0.5.0
    void swap_sections(Key_section & sect1, Key_section & sect2);

    /// @brief Remove key @a key form the @a sect section.
    /// @param sect section.
    /// @param key_name key name.
    /// @param similar if @b true, use str_similar() for lookup (case insensitive), else compare @a key_name byte-to-byte.
    /// @throw user_error if @a sect is foreign (belongs to another Key_file) or @a key_name is invalid.
    void remove_key(Key_section & sect, std::string_view key_name, bool similar=false);

    /// @overload
    /// @brief Remove key @a key_name form the root section.
    /// @param key_name key name.
    /// @param similar if @b true, use str_similar() for lookup (case insensitive), else compare @a key_name byte-to-byte.
    /// @throw user_error if @a key_name is invalid.
    /// @since 0.4.0
    void remove_key(std::string_view key_name, bool similar=false);

    /// @brief Remove section.
    /// @param sect_name section name.
    /// @param similar if @b true, use str_similar() for lookup (case insensitive), else compare @a sect_name byte-to-byte.
    /// @throw user_error if @a sect_name is invalid.
    void remove_section(std::string_view sect_name, bool similar=false);

    /// @brief Remove all sections.
    void clear();

    /// @brief Lock file (disable modification).
    void lock() noexcept;

    /// @brief Unlock file (enable modification).
    void unlock() noexcept;

    /// @brief Test if locked.
    bool locked() const noexcept;

    /// @brief Test if changed.
    /// The "changed" state occurs when key-value pairs change when using class methods
    /// @since 0.4.0
    /// @sa save()
    /// @sa flush()
    bool changed() const noexcept;

    /// @}
    /// @name Signals
    /// @{

    /// @brief Gets "modified" signal.
    /// This signal emitted when key-value pairs changes somehow when using class methods.
    /// At the moment the signal is emitted, the "changed" flag changes its value to @b true.
    /// Slot prototype:
    /// ~~~~~~~~~~~~~~~
    /// void on_modified(std::string_view sect_name, std::string_view key, const ustring & value);
    /// ~~~~~~~~~~~~~~~
    /// Where:
    /// - sect_name     is a section name, where key was modified, if empty, then root section modified.
    /// - key           key name which value modified, when empty that means new section was created, removed or renamed.
    /// - value         new value, if empty, then key was removed.
    /// @sa changed()
    /// @sa signal_changed()
    /// @since 0.7.0
    signal<void(std::string_view, std::string_view, const ustring &)> & signal_modified();

    /// @brief Gets "changed" signal.
    /// This signal emitted when key-value pairs changes somehow when using class methods.
    /// At the moment the signal is emitted, the "changed" flag changes its value to @b true.
    /// @sa changed()
    /// @sa signal_modified()
    signal<void()> & signal_changed();

    /// @}

private:

    Key_file_impl * impl;
};

/// @brief Stream Key_file in.
/// @relates Key_file
std::istream & operator>>(std::istream & is, Key_file & kf);

/// @brief Stream Key_file out.
/// @relates Key_file
std::ostream & operator<<(std::ostream & os, const Key_file & kf);

} // namespace tau

#endif // __TAU_KEY_FILE_HH__

// ----------------------------------------------------------------------------
// Copyright © 2014-2024 Konstantin Shmelkov <mrcashe@gmail.com>.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
// THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
// PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
// CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
// EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
// PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
// OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
// WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
// OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
// EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// ----------------------------------------------------------------------------

/// @file brush.hh The Brush class.

#ifndef __TAU_BRUSH_HH__
#define __TAU_BRUSH_HH__

#include <tau/defs.hh>

namespace tau {

/// The brush used for contour filling.
///
/// @note This class is a wrapper around its implementation shared pointer.
///
/// @ingroup paint_group
class __TAUEXPORT__ Brush {
public:

    /// @name Constructors, operators
    /// @{

    /// Default constructor.
    Brush();

    /// Destructor.
   ~Brush();

    /// Constructs solid brush of specified color.
    Brush(const Color & color);

    /// Constructs brush from text representation.
    /// @since 0.6.0
    Brush(const ustring & s);

    /// Copy constructor.
    /// @note This class is a wrapper around its implementation shared pointer,
    /// so copying it just increasing implementation pointer use count, but isn't
    /// really copies the object. The underlying implementation is not copyable.
    Brush(const Brush & other) = default;

    /// Copy operator.
    /// @note This class is a wrapper around its implementation shared pointer,
    /// so copying it just increasing implementation pointer use count, but isn't
    /// really copies the object. The underlying implementation is not copyable.
    Brush & operator=(const Brush & other) = default;

    /// Constructor with implementation pointer.
    Brush(Brush_ptr bp);

    /// Get implementation pointer.
    /// @since 0.6.0
    Brush_ptr ptr();

    /// Get implementation pointer.
    /// @since 0.6.0
    Brush_cptr ptr() const;

    /// @}
    /// @name Controls
    /// @{

    /// Make brush darker.
    /// @since 0.6.0
    void darker(double factor);

    /// Make brush lighter.
    /// @since 0.6.0
    void lighter(double factor);

    /// Get color.
    /// @since 0.6.0
    Color color() const noexcept;

    /// Get text representation.
    /// @since 0.6.0
    ustring str() const;

    /// @}

private:

    Brush_ptr impl;
};

} // namespace tau

#endif // __TAU_BRUSH_HH__

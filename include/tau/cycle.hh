// ----------------------------------------------------------------------------
// SPDX-License-Identifier: BSD-2-Clause
// Copyright © 2014-2024 Konstantin Shmelkov <mrcashe@gmail.com>.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
// THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
// PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
// CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
// EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
// PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
// OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
// WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
// OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
// EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// ----------------------------------------------------------------------------

/// @file cycle.hh The Cycle_base, Cycle and Cycle_text class declarations.

#ifndef __TAU_CYCLE_HH__
#define __TAU_CYCLE_HH__

#include <tau/enums.hh>
#include <tau/container.hh>

namespace tau {

/// %Container that show its children by cycle.
///
/// @note This class is a wrapper around its implementation shared pointer.
///
/// To obtain which child is selected now, connect to child Widget::signal_select()
/// or (and) Widget::signal_unselect().
///
/// @ingroup compound_container_group
class __TAUEXPORT__ Cycle_base: public Container {
public:

    /// @name Controls
    /// @{

    /// Set border style.
    void set_border_style(Border bs);

    /// Get border style.
    Border border_style() const noexcept;

    /// Test if empty.
    /// @since 0.6.0
    bool empty() const noexcept;

    /// Get string count.
    /// @since 0.6.0
    std::size_t count() const noexcept;

    /// Remove all children.
    void clear();

    /// Get widget by id.
    /// @since 0.6.0
    Widget_ptr widget(int id);

    /// Get widget by id.
    /// @since 0.6.0
    Widget_cptr widget(int id) const;

    /// @}
    /// @name Selection
    /// @{

    /// Get current selection id.
    /// @return an unique @a id for current string or @b INT_MIN if empty.
    /// @since 0.6.0
    int current() const noexcept;

    /// Test if selection fixed.
    /// @since 0.6.0
    bool fixed() const noexcept;

    /// Unfix selection.
    /// Allows selection to be altered.
    /// @sa select()
    /// @since 0.6.0
    void unfix();

    /// @}
    /// @name Additional Widgets Management
    /// @{

    /// Append widget after cycling widget.
    /// @throw user_error if w already inserted into another container.
    /// @throw internal_error if w has pure implementation pointer.
    void append(Widget & w, bool shrink=false);

    /// Append text after cycling widget.
    /// @param text text for append.
    /// @param margin_left left margin.
    /// @param margin_right right margin.
    /// @return a pointer to the created Label widget.
    Widget_ptr append(const ustring & text, unsigned margin_left=0, unsigned margin_right=0);

    /// Append text after counting value.
    /// @param text         text to be appended.
    /// @param color        text color.
    /// @param margin_left  left margin.
    /// @param margin_right right margin.
    /// @return             pointer to the created Label widget.
    /// @since 0.6.0
    Widget_ptr append(const ustring & text, const Color & color, unsigned margin_left=0, unsigned margin_right=0);

    /// Prepend widget before cycling widget.
    /// @throw user_error if w already inserted into another container.
    /// @throw internal_error if w has pure implementation pointer.
    void prepend(Widget & w, bool shrink=false);

    /// Prepend text before cycling widget.
    /// @param text text for prepend.
    /// @param margin_left left margin.
    /// @param margin_right right margin.
    /// @return a pointer to the created Label widget.
    Widget_ptr prepend(const ustring & text, unsigned margin_left=0, unsigned margin_right=0);

    /// Append text after counting value.
    /// @param text         text to be appended.
    /// @param color        text color.
    /// @param margin_left  left margin.
    /// @param margin_right right margin.
    /// @return             pointer to the created Label widget.
    /// @since 0.6.0
    Widget_ptr prepend(const ustring & text, const Color & color, unsigned margin_left=0, unsigned margin_right=0);

    /// @}
    /// @name Actions & Signals
    /// @{

    /// Gets action "Cancel".
    /// @since 0.6.0
    Action & action_cancel();

    /// Gets action "Activate".
    /// @since 0.6.0
    Action & action_activate();

    /// Signal emitted when new child added into Cycle or Cycle_text.
    /// Slot prototype:
    /// ~~~~~~~~~~~~~~~
    /// void on_new_id(int id);
    /// ~~~~~~~~~~~~~~~
    /// @since 0.6.0
    signal<void(int)> & signal_new_id();

    /// Signal emitted when child removed from Cycle or Cycle_text.
    /// Slot prototype:
    /// ~~~~~~~~~~~~~~~
    /// void on_remove_id(int id);
    /// ~~~~~~~~~~~~~~~
    /// @since 0.6.0
    signal<void(int)> & signal_remove_id();

    /// Signal emitted when child becomes selected.
    /// Slot prototype:
    /// ~~~~~~~~~~~~~~~
    /// void on_select_id(int id);
    /// ~~~~~~~~~~~~~~~
    /// @since 0.6.0
    signal<void(int)> & signal_select_id();

    /// @}

protected:

    /// @private
    Cycle_base(Widget_ptr wp);

    /// @private
    Cycle_base & operator=(Widget_ptr wp);

};

/// %Container that show its children by cycle.
///
/// @note This class is a wrapper around its implementation shared pointer.
///
/// To obtain which child is selected now, connect to child Widget::signal_select()
/// or (and) Widget::signal_unselect().
///
/// @ingroup compound_container_group
class __TAUEXPORT__ Cycle: public Cycle_base {
public:

    /// @name Constructors & Operators
    /// @{

    /// Default constructor.
    Cycle(Border bs=Border::INSET);

    /// Copy constructor.
    ///
    /// @note This class is a wrapper around its implementation shared pointer,
    /// so copying it just increasing implementation pointer use count, but isn't
    /// really copies the object. The underlying implementation is not copyable.
    Cycle(const Cycle & other);

    /// Copy operator.
    ///
    /// @note This class is a wrapper around its implementation shared pointer,
    /// so copying it just increasing implementation pointer use count, but isn't
    /// really copies the object. The underlying implementation is not copyable.
    Cycle & operator=(const Cycle & other);

    /// Move constructor.
    ///
    /// @note This class is a wrapper around its implementation shared pointer,
    /// so copying it just increasing implementation pointer use count, but isn't
    /// really copies the object. The underlying implementation is not copyable.
    /// @note This constructor does not reset @c other implementation pointer.
    Cycle(Cycle && other);

    /// Move operator.
    ///
    /// @note This class is a wrapper around its implementation shared pointer,
    /// so copying it just increasing implementation pointer use count, but isn't
    /// really copies the object. The underlying implementation is not copyable.
    /// @note This operator does not reset @c other implementation pointer.
    Cycle & operator=(Cycle && other);

    /// Constructor with implementation pointer.
    ///
    /// @warning Unlike some other classes (Painter as an example), the whole
    /// @ref widget_stack "widget stack" is unable to run with pure implementation
    /// pointer, so attempting to construct widget from a pure (@b nullptr) pointer
    /// will cause throwing an user_error exception!
    /// That exception also will be thrown if user tries to construct the object
    /// from incompatible implementation shared pointer.
    ///
    /// @throw user_error in case of pure implementation pointer or incompatible
    /// implementation pointer class.
    /// @since 0.4.0
    Cycle(Widget_ptr wp);

    /// Assign implementation.
    ///
    /// @throw user_error in case of pure implementation pointer or incompatible
    /// implementation pointer class.
    /// @since 0.4.0
    Cycle & operator=(Widget_ptr wp);

    /// @}
    /// @name Controls
    /// @{

    /// Add cycling widget.
    /// @return an unique @a id for added widget.
    int add(Widget & w);

    /// Remove any widget.
    /// @return count of removed widgets.
    std::size_t remove(Widget & w);

    /// Remove widget with id.
    /// @return count of removed widgets.
    /// @since 0.6.0
    std::size_t remove(int id);

    /// @}
    /// @name Selection
    /// @{

    /// Select specified widget.
    /// @param  w   widget to be selected.
    /// @param  fix fix selection (make selection permanent).
    /// If @c fix is set, all futher calls to select() will fail until unfix() call.
    /// @return selected id or INT_MIN on fail.
    /// @since 0.6.0
    int select(Widget & w, bool fix=false);

    /// Select specified widget by id.
    /// @param  id  the unique widget id returned by add().
    /// @param  fix fix selection (make selection permanent).
    /// If @c fix is set, all futher calls to select() will fail until unfix() call.
    /// @return selected id or INT_MIN on fail.
    /// @since 0.6.0
    int select(int id, bool fix=false);

    /// Select specified widget without signal_select_id() emission.
    /// @param  w   widget to be selected.
    /// @param  fix fix selection (make selection permanent).
    /// If @c fix is set, all futher calls to select() will fail until unfix() call.
    /// @return selected id or INT_MIN on fail.
    /// @since 0.6.0
    int setup(Widget & w, bool fix=false);

    /// Select specified widget by id without signal_select_id() emission.
    /// @param  id  the unique widget id returned by add().
    /// @param  fix fix selection (make selection permanent).
    /// If @c fix is set, all futher calls to select() will fail until unfix() call.
    /// @return selected id or INT_MIN on fail.
    /// @since 0.6.0
    int setup(int id, bool fix=false);

    /// @}
};

/** Cycle specification for text.
 *    %Cycle_text uses Card container for Entry allocation, so only single %Entry visible in a moment.
 *    For string identification, an integer unique id provided. You can read added text back and even
 *    get access to the %Entry implementation pointer.
 *
 *    @note This class is a wrapper around its implementation shared pointer.
 *
 *    @ingroup compound_container_group
 **/
class __TAUEXPORT__ Cycle_text: public Cycle_base {
public:

    /// @name Constructors & Operators
    /// @{

    /// Constructor with optional border style.
    /// @param bs the border style
    Cycle_text(Border bs=Border::INSET);

    /// Constructor with alignment and optional border style.
    /// @param align horizontal text align.
    /// @param bs the border style
    Cycle_text(Align align, Border bs=Border::INSET);

    /// Constructor with strings and optional border style.
    /// @since 0.6.0
    Cycle_text(const std::vector<ustring> & sv, Border bs=Border::INSET);

    /// Constructor with strings, alignment and optional border style.
    /// @since 0.6.0
    Cycle_text(const std::vector<ustring> & sv, Align align, Border bs=Border::INSET);

    /// Copy constructor.
    ///
    /// @note This class is a wrapper around its implementation shared pointer,
    /// so copying it just increasing implementation pointer use count, but isn't
    /// really copies the object. The underlying implementation is not copyable.
    Cycle_text(const Cycle_text & other);

    /// Copy operator.
    ///
    /// @note This class is a wrapper around its implementation shared pointer,
    /// so copying it just increasing implementation pointer use count, but isn't
    /// really copies the object. The underlying implementation is not copyable.
    Cycle_text & operator=(const Cycle_text & other);

    /// Move constructor.
    ///
    /// @note This class is a wrapper around its implementation shared pointer,
    /// so copying it just increasing implementation pointer use count, but isn't
    /// really copies the object. The underlying implementation is not copyable.
    /// @note This constructor does not reset @c other implementation pointer.
    Cycle_text(Cycle_text && other);

    /// Move operator.
    ///
    /// @note This class is a wrapper around its implementation shared pointer,
    /// so copying it just increasing implementation pointer use count, but isn't
    /// really copies the object. The underlying implementation is not copyable.
    /// @note This operator does not reset @c other implementation pointer.
    Cycle_text & operator=(Cycle_text && other);

    /// Constructor with implementation pointer.
    ///
    /// @warning Unlike some other classes (Painter as an example), the whole
    /// @ref widget_stack "widget stack" is unable to run with pure implementation
    /// pointer, so attempting to construct widget from a pure (@b nullptr) pointer
    /// will cause throwing an user_error exception!
    /// That exception also will be thrown if user tries to construct the object
    /// from incompatible implementation shared pointer.
    ///
    /// @throw user_error in case of pure implementation pointer or incompatible
    /// implementation pointer class.
    /// @since 0.4.0
    Cycle_text(Widget_ptr wp);

    /// Assign implementation.
    ///
    /// @throw user_error in case of pure implementation pointer or incompatible
    /// implementation pointer class.
    /// @since 0.4.0
    Cycle_text & operator=(Widget_ptr wp);

    /// @}
    /// @name Text Insertion/Removal
    /// @{

    /// Add text.
    /// Adds text to the %Cycle.
    /// When adding, a validation performed using probably connected signal_approve() and signal_validate().
    /// If that validation failed, the return value of this call is -1.
    /// @param text     text to add.
    /// @param tooltip  an optional tool tip for that text.
    /// @return an unique @a id for added string or @b INT_MIN if validation failed.
    int add(const ustring & text, const ustring & tooltip=ustring());

    /// Add text.
    /// Adds text to the %Cycle.
    /// When adding, a validation performed using probably connected signal_approve() and signal_validate().
    /// If that validation failed, the return value of this call is -1.
    /// @param text     text to add.
    /// @param align    text alignment.
    /// @param tooltip  an optional tool tip for that text.
    /// @return an unique @a id for added string or @b INT_MIN if validation failed.
    /// @since 0.6.0
    int add(const ustring & text, Align align, const ustring & tooltip=ustring());

    /// Add strings.
    /// Adds strings to the %Cycle.
    /// When adding, a validation performed using probably connected signal_approve() and signal_validate().
    /// @param sv       vector of strings to add.
    /// @param align    text alignment.
    /// @return         an unique @a id of the first added string or @b INT_MIN if validation failed.
    /// @since 0.6.0
    int add(const std::vector<ustring> & sv, Align align=Align::CENTER);

    /// Remove any widget.
    /// @return count of removed widgets.
    std::size_t remove(Widget & w);

    /// Remove widget with id.
    /// @return count of removed widgets.
    /// @since 0.6.0
    std::size_t remove(int id);

    /// Remove text.
    /// @return count of removed widgets.
    std::size_t remove(const ustring & s);

    /// @}
    /// @name Retrieving Information
    /// @{

    /// Get UTF-8 string by id.
    /// @since 0.6.0
    ustring str(int id) const;

    /// Get UTF-32 string by id.
    /// @since 0.6.0
    std::u32string wstr(int id) const;

    /// Get all strings as UTF-8.
    /// @since 0.6.0
    std::vector<ustring> strings() const;

    /// Get all strings as UTF-32.
    /// @since 0.6.0
    std::vector<std::u32string> wstrings() const;

    /// Get widget by text.
    /// @since 0.6.0
    Widget_ptr widget(const ustring & s);

    /// Get widget by text.
    /// @since 0.6.0
    Widget_cptr widget(const ustring & s) const;

    /// @}
    /// @name Text Editing
    /// %Cycle_text has text editing facilities.
    /// When editing enabled, than user can click on the entry and start to edit it.
    /// @{

    /// Allow edit.
    void allow_edit();

    /// Disallow edit.
    void disallow_edit();

    /// Test if edit allowed.
    bool editable() const noexcept;

    /// @brief Force text editing.
    /// This call enables editing even if that editing was not enabled previously.
    /// After editing complete, the previous state of the editing grants returned.
    ///
    /// While editing, the below signals connected to the edit widget (which is of type Entry).
    /// After editing complete, the signals disconnected.
    ///
    /// The return value is an implementation pointer of the Entry class. In case of error
    /// the returning pointer is pure, so you must test it before using. The main possible
    /// reason to fail is impossibility to gain keyboard focus for editing widget.
    /// You can construct Entry from non-pure pointer and perform any actions on it.
    ///
    /// @return implementation pointer of editor widget (Entry) or pure pointer on fail.
    /// @sa signal_activate_edit()
    /// @sa signal_cancel_edit()
    /// @sa signal_validate()
    /// @sa signal_approve()
    /// @sa editor()
    /// @since 0.6.0
    Widget_ptr edit();

    /// Signal emitted when some editing text activated.
    /// Slot prototype:
    /// ~~~~~~~~~~~~~~
    /// void on_activate_edit(int id, const std::u32string & text);
    /// ~~~~~~~~~~~~~~
    /// @since 0.6.0
    signal<void(int, const std::u32string &)> & signal_activate_edit();

    /// Signal emitted when text editting cancelled.
    /// Slot prototype:
    /// ~~~~~~~~~~~~~~
    /// void on_cancel_edit(int id);
    /// ~~~~~~~~~~~~~~
    /// @since 0.6.0
    signal<void(int)> & signal_cancel_edit();

    /// Get editing widget implementation pointer.
    /// @return When editing activated by calling edit() function, this call return
    /// exactly same pointer as edit() returned. If there are no editing
    /// action performed, the returned pointer is pure. The type of interface is Entry.
    /// @sa edit()
    /// @since 0.6.0
    Widget_ptr editor();

    /// Get editing widget implementation pointer.
    /// @return When editing activated by calling edit() function, this call return
    /// exactly same pointer as edit() returned. If there are no editing
    /// action performed, the returned pointer is pure. The type of interface is Entry.
    /// @sa edit()
    /// @since 0.6.0
    Widget_cptr editor() const;

    /// @}
    /// @name Selection
    /// @{

    /// Select specified widget.
    /// @param  w   widget to be selected.
    /// @param  fix fix selection (make selection permanent).
    /// If @c fix is set, all futher calls to select() will fail until unfix() call.
    /// @return selected id or INT_MIN on fail.
    /// @since 0.6.0
    int select(Widget & w, bool fix=false);

    /// Select specified widget by id.
    /// @param  id  the unique widget id returned by add().
    /// @param  fix fix selection (make selection permanent).
    /// If @c fix is set, all futher calls to select() will fail until unfix() call.
    /// @return selected id or INT_MIN on fail.
    /// @since 0.6.0
    int select(int id, bool fix=false);

    /// Select specified text.
    /// @param  s       text to be selected.
    /// @param  fix fix selection (make selection permanent).
    /// If @c fix is set, all futher calls to select() will fail until unfix() call.
    /// @return selected id or INT_MIN on fail.
    int select(const ustring & s, bool fix=false);

    /// Select similar text.
    /// @param  s       text to be selected.
    /// @param  fix fix selection (make selection permanent).
    /// If @c fix is set, all futher calls to select() will fail until unfix() call.
    /// @return selected id or INT_MIN on fail.
    /// @since 0.6.0
    int select_similar(const ustring & s, bool fix=false);

    /// Select specified widget without Cycle_base::signal_select_id() and signal_select_text() emission.
    /// @param  w   widget to be selected.
    /// @param  fix fix selection (make selection permanent).
    /// If @c fix is set, all futher calls to select() will fail until unfix() call.
    /// @return selected id or INT_MIN on fail.
    /// @since 0.6.0
    int setup(Widget & w, bool fix=false);

    /// Select specified widget by id without Cycle_base::signal_select_id() and signal_select_text() emission.
    /// @param  id  the unique widget id returned by add().
    /// @param  fix fix selection (make selection permanent).
    /// If @c fix is set, all futher calls to select() will fail until unfix() call.
    /// @return selected id or INT_MIN on fail.
    /// @since 0.6.0
    int setup(int id, bool fix=false);

    /// Select specified text without Cycle_base::signal_select_id() and signal_select_text() emission.
    /// @param  s       text to be selected.
    /// @param  fix fix selection (make selection permanent).
    /// If @c fix is set, all futher calls to select() will fail until unfix() call.
    /// @return selected id or INT_MIN on fail.
    int setup(const ustring & s, bool fix=false);

    /// Select similar text without Cycle_base::signal_select_id() and signal_select_text() emission.
    /// @param  s       text to be selected.
    /// @param  fix fix selection (make selection permanent).
    /// If @c fix is set, all futher calls to select() will fail until unfix() call.
    /// @return selected id or INT_MIN on fail.
    /// @since 0.6.0
    int setup_similar(const ustring & s, bool fix=false);

    /// Get current selection as UTF-8 string.
    ustring str() const;

    /// Get current selection as UTF-32 string.
    std::u32string wstr() const;

    /// @}
    /// @name Controls
    /// @{

    /// Set horizontal text alignment.
    void text_align(Align align);

    /// Get horizontal text alignment.
    Align text_align() const noexcept;

    /// Set wrap mode.
    /// @since 0.6.0
    void wrap(Wrap wm);

    /// Get wrap mode.
    Wrap wrap() const noexcept;

    /// @}
    /// @name Signals
    /// @{

    /// Emitted when text added.
    /// Slot prototype:
    /// ~~~~~~~~~~~~~~~
    /// void on_new_text(const std::u32string & s);
    /// ~~~~~~~~~~~~~~~
    /// @since 0.6.0
    signal<void(const std::u32string &)> & signal_new_text();

    /// Emitted when text removed.
    /// Slot prototype:
    /// ~~~~~~~~~~~~~~~
    /// void on_remove_text(const std::u32string & s);
    /// ~~~~~~~~~~~~~~~
    /// @since 0.6.0
    signal<void(const std::u32string &)> & signal_remove_text();

    /// Emitted when selection changed.
    /// Slot prototype:
    /// ~~~~~~~~~~~~~~~
    /// void on_select_text(const std::u32string & s);
    /// ~~~~~~~~~~~~~~~
    /// @since 0.6.0
    signal<void(const std::u32string &)> & signal_select_text();

    /// Emitted when text activated.
    /// Slot prototype:
    /// ~~~~~~~~~~~~~~~
    /// void on_text_activate(int id, const std::u32string & s);
    /// ~~~~~~~~~~~~~~~
    /// Here is:
    /// - @a id returned by add() functions.
    /// - @a s  activated text.
    /// @since 0.6.0
    signal<void(int, const std::u32string &)> & signal_activate_text();

    /// Emitted when text changed.
    /// Slot prototype:
    /// ~~~~~~~~~~~~~~~
    /// void on_text_changed(int id, const std::u32string & s);
    /// ~~~~~~~~~~~~~~~
    /// Here is:
    /// - @a id returned by add() functions.
    /// - @a s  changed text.
    /// @since 0.6.0
    signal<void(int, const std::u32string &)> & signal_text_changed();

    /// Signal emitted while text validation occurs.
    /// Slot prototype:
    /// ~~~~~~~~~~~~~~
    /// bool on_validate(const std::u32string & text);
    /// ~~~~~~~~~~~~~~
    /// @sa Entry::signal_validate()
    /// @since 0.6.0
    signal_all<const std::u32string &> & signal_validate();

    /// Signal emitted while text approval occurs.
    /// Slot prototype:
    /// ~~~~~~~~~~~~~~
    /// bool on_approval(const std::u32string & text);
    /// ~~~~~~~~~~~~~~
    /// @sa Entry::signal_approve()
    /// @since 0.6.0
    signal_all<const std::u32string &> & signal_approve();

    /// @}
};

} // namespace tau

#endif // __TAU_CYCLE_HH__

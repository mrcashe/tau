// ----------------------------------------------------------------------------
// SPDX-License-Identifier: BSD-2-Clause
// Copyright © 2014-2024 Konstantin Shmelkov <mrcashe@gmail.com>.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
// THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
// PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
// CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
// EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
// PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
// OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
// WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
// OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
// EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// ----------------------------------------------------------------------------

/// @file edit.hh The Edit class.

#ifndef __TAU_EDIT_HH__
#define __TAU_EDIT_HH__

#include <tau/text.hh>

namespace tau {

/// %Text editor.
///
/// @note This class is a wrapper around its implementation shared pointer.
///
/// @ingroup widget_group
/// @ingroup input_group
class __TAUEXPORT__ Edit: public Text {
public:

    /// @name Constructors & Operators
    /// @{

    /// Default constructor.
    Edit();

    /// Constructor with text alignment.
    explicit Edit(Align halign, Align valign=Align::START);

    /// Constructor with text and text alignment.
    explicit Edit(const ustring & text, Align halign=Align::START, Align valign=Align::START);

    /// Constructor with buffer and text alignment.
    explicit Edit(Buffer buf, Align halign=Align::START, Align valign=Align::START);

    /// Copy constructor.
    ///
    /// @note This class is a wrapper around its implementation shared pointer,
    /// so copying it just increasing implementation pointer use count, but isn't
    /// really copies the object. The underlying implementation is not copyable.
    Edit(const Edit & other);

    /// Copy operator.
    ///
    /// @note This class is a wrapper around its implementation shared pointer,
    /// so copying it just increasing implementation pointer use count, but isn't
    /// really copies the object. The underlying implementation is not copyable.
    Edit & operator=(const Edit & other);

    /// Move constructor.
    ///
    /// @note This class is a wrapper around its implementation shared pointer,
    /// so copying it just increasing implementation pointer use count, but isn't
    /// really copies the object. The underlying implementation is not copyable.
    /// @note This constructor does not reset @c other implementation pointer.
    Edit(Edit && other);

    /// Move operator.
    ///
    /// @note This class is a wrapper around its implementation shared pointer,
    /// so copying it just increasing implementation pointer use count, but isn't
    /// really copies the object. The underlying implementation is not copyable.
    /// @note This operator does not reset @c other implementation pointer.
    Edit & operator=(Edit && other);

    /// Constructor with implementation pointer.
    ///
    /// @warning Unlike some other classes (Painter as an example), the whole
    /// @ref widget_stack "widget stack" is unable to run with pure implementation
    /// pointer, so attempting to construct widget from a pure (@b nullptr) pointer
    /// will cause throwing an user_error exception!
    /// That exception also will be thrown if user tries to construct the object
    /// from incompatible implementation shared pointer.
    ///
    /// @throw user_error in case of pure implementation pointer or incompatible
    /// implementation pointer class.
    /// @since 0.4.0
    Edit(Widget_ptr wp);

    /// Assign implementation.
    ///
    /// @throw user_error in case of pure implementation pointer or incompatible
    /// implementation pointer class.
    /// @since 0.4.0
    Edit & operator=(Widget_ptr wp);

    /// @}
    /// @name Controls
    /// @{

    /// Allow edit.
    void allow_edit();

    /// Disallow edit.
    void disallow_edit();

    /// Determines whether edit allowed.
    bool editable() const noexcept;

    /// Enter text at current position.
    void enter_text(const ustring & text);

    /// Test if text modified.
    bool modified() const noexcept;

    /// @}
    /// @name Access to the established actions and signals.
    /// @{

    /// Gets "Cut" action (Ctrl+X, Ctrl+Delete).
    Action & action_cut();

    /// Gets "Paste" action (Ctrl+V, Shift+Insert).
    Action & action_paste();

    /// Gets "Enter" action (Enter).
    Action & action_enter();

    /// Gets "Delete" action (Delete).
    Action & action_delete();

    /// Gets "BackSpace" action (Backspace).
    Action & action_backspace();

    /// Gets "Undo" action (Alt+Backspace").
    Action & action_undo();

    /// Gets "Redo" action (Alt+Enter").
    Action & action_redo();

    /// Gets "Tab" action (Alt+Enter").
    Action & action_tab();

    /// Gets "Insert/Replace" action.
    Toggle_action & action_insert();

    /// Gets "signal_modified".
    signal<void(bool)> & signal_modified();

    /// Gets "signal_changed".
    /// @since 0.6.0
    signal<void()> & signal_changed();

    /// @}
};

} // namespace tau

#endif // __TAU_EDIT_HH__

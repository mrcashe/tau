// ----------------------------------------------------------------------------
// SPDX-License-Identifier: BSD-2-Clause
// Copyright © 2014-2024 Konstantin Shmelkov <mrcashe@gmail.com>.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
// THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
// PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
// CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
// EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
// PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
// OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
// WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
// OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
// EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// ----------------------------------------------------------------------------

/// @file font.hh The Font class and font manipulating functions.

#ifndef __TAU_FONT_HH__
#define __TAU_FONT_HH__

#include <tau/geometry.hh>
#include <tau/signal.hh>
#include <tau/defs.hh>
#include <tau/ustring.hh>

namespace tau {

/// The scaled font.
///
/// @note This class is a wrapper around its implementation shared pointer.
///
/// A font represents an organized collection of glyphs in which the various Glyph
/// representations will share a common look or styling such that, when a string of
/// characters is rendered together, the result is highly legible, conveys a particular
/// artistic style and provides consistent inter-character alignment and spacing.
/// @ingroup font_group
class __TAUEXPORT__ Font: public trackable {
public:

    /// @name Contructors and Operators
    /// @{

    /// Default constructor creates a pure font.
    Font();

    /// Destructor.
   ~Font();

    /// Copy constructor.
    ///
    /// @note This class is a wrapper around its implementation shared pointer,
    /// so copying it just increasing implementation pointer use count, but isn't
    /// really copies the object. The underlying implementation is not copyable.
    Font(const Font & other) = default;

    /// Copy operator.
    ///
    /// @note This class is a wrapper around its implementation shared pointer,
    /// so copying it just increasing implementation pointer use count, but isn't
    /// really copies the object. The underlying implementation is not copyable.
    Font & operator=(const Font & other) = default;

    /// Constructor with implementation pointer.
    Font(Font_ptr fp);

    /// @}
    /// @name Controls
    /// @{

    /// Test if pure.
    operator bool() const noexcept;

    /// Reset underlying implementation.
    void reset();

    /// Get implementation pointer.
    /// @since 0.6.0
    Font_ptr ptr();

    /// Get implementation pointer.
    /// @since 0.6.0
    Font_cptr ptr() const;

    /// Compare operator.
    bool operator==(const Font & other) const noexcept;

    /// Compare operator.
    bool operator!=(const Font & other) const noexcept;

    /// @}
    /// @name Miscellaneous Functions
    /// @{

    /// Get font specification.
    ustring spec() const noexcept;

    /// Get Postscript font name.
    ustring psname() const noexcept;

    /// Get dots-per-inch.
    unsigned dpi() const noexcept;

    /// Get ascent.
    /// The distance from the baseline to the highest or
    /// upper grid coordinate used to place an outline point.
    double ascent() const noexcept;

    /// Get integer ascent.
    /// The distance from the baseline to the highest or
    /// upper grid coordinate used to place an outline point.
    /// @since 0.7.0
    int iascent() const noexcept;

    /// Get descent.
    /// The distance from the baseline to the lowest
    /// grid coordinate used to place an outline point.
    double descent() const noexcept;

    /// Get integer descent.
    /// The distance from the baseline to the lowest
    /// grid coordinate used to place an outline point.
    /// @since 0.7.0
    int idescent() const noexcept;

    /// Get line gap.
    /// The distance that must be placed between two lines of text.
    double linegap() const noexcept;

    /// Get (xmin:ymin) pair.
    Vector min() const noexcept;

    /// Get (xmax:ymax) pair.
    Vector max() const noexcept;

    /// Get scaled glyph for specified Unicode character.
    Glyph glyph(char32_t wc) const;

    /// Get unscaled glyph for specified Unicode character.
    /// @since 0.6.0
    Glyph master_glyph(char32_t wc) const;

    /// Test if font monospace.
    /// @since 0.5.0
    bool monospace() const;

    /// Get character width.
    /// If font has fixed pitch, this method returns non-zero value.
    /// Otherwise (variable pitch), the return value is 0.
    /// @since 0.6.0
    unsigned char_width() const;

    /// Get font height in pixels.
    /// @since 0.7.0
    unsigned height() const noexcept;

    /// @}
    /// @name System Fonts Management
    /// These static functions uses %Font class instead of seprarated namespace.
    ///
    /// @{

    /// List all available font families synchronously.
    static std::vector<ustring> list_families();

    /// List all available font families asynchronously.
    /// @param slot_next slot to be called on every family name arrival.
    ///
    /// Slot prototype:
    /// ~~~~~~~~~~~~~~~
    /// void on_next_family(const ustring & fam);
    /// ~~~~~~~~~~~~~~~
    ///
    /// The asynchronous process terminated when empty string arrives.
    static void list_families_async(slot<void(ustring)> slot_next);

    /// List all available font faces for given font family.
    static std::vector<ustring> list_faces(const ustring & font_family);

    /// Get system default font specification.
    static ustring normal();

    /// Get system default monospace font specification.
    static ustring mono();

    /// @}
    /// @name Utility Functions
    /// These static functions uses %Font class instead of seprarated namespace.
    ///
    /// @{

    /// Split font specification into peaces.
    static std::vector<ustring> explode(const ustring & spec);

    /// Build specification from components.
    static ustring build(const ustring & family, const ustring & face, double size_pt=0.0);

    /// Build specification from components.
    static ustring build(const std::vector<ustring> & specv);

    /// Test two specifications for equality.
    /// @since 0.6.0
    static bool same(const ustring & spec1, const ustring & spec2) noexcept;

    /// Get font family from font specification.
    static ustring family(const ustring & spec);

    /// Get font family from font specification.
    static ustring family(const std::vector<ustring> & specv);

    /// Get font face from font specification.
    static ustring face(const ustring & spec);

    /// Get font face from font specification.
    static ustring face(const std::vector<ustring> & specv);

    /// Get label for font specification.
    /// @since 0.6.0
    static ustring label(const ustring & spec);

    /// Get label for font specification.
    /// @since 0.6.0
    static ustring label(const std::vector<ustring> & specv);

    /// Test if font specification has certain face element.
    /// @since 0.4.0
    static bool has_face(const ustring & spec, const ustring & face_element);

    /// Set face.
    /// @param spec initial font specification.
    /// @param face the face specification.
    /// If current font specification is "Arial Bold" and the face is
    /// "Italic", the result will be "Arial Italic".
    /// @return modified font specification.
    static ustring set_face(const ustring & spec, const ustring & face);

    /// Add face modificator to the font.
    /// @param spec initial font specification.
    /// @param face_elements elements to be added to font face specification.
    /// If current font specification is "Arial Bold" and the face_elements is
    /// "Italic", the result will be "Arial Bold Italic".
    /// @return modified font specification.
    static ustring add_face(const ustring & spec, const ustring & face_elements);

    /// Make bold font specification.
    /// @since 0.6.0
    static ustring make_bold(const ustring & spec);

    /// Make italic font specification.
    /// @since 0.6.0
    static ustring make_italic(const ustring & spec);

    /// Returns size component from font specification.
    /// @param spec font specification.
    /// @param fallback fallback value in case spec missing font size.
    static double points(const ustring & spec, double fallback=0.0);

    /// Returns size component from font specification.
    /// @param specv font specification vector.
    /// @param fallback fallback value in case spec missing font size.
    static double points(const std::vector<ustring> & specv, double fallback=0.0);

    /// Returns font specification without size component.
    static ustring without_points(const ustring & spec);

    /// Change size in font specification.
    /// Changes size if it present and add font size if it initially absent.
    static ustring resize(const ustring & spec, double size_pt);

    /// Enlarge size in font specification.
    /// Enlarges size if it present.
    /// @since 0.5.0
    static ustring enlarge(const ustring & spec, double delta);

    /// Change size in font specification.
    /// Changes size if it less than given value or if it absent.
    static ustring at_least(const ustring & spec, double min_size_pt);

    /// Change size in font specification.
    /// Changes size if it greater than given value or if it absent.
    static ustring as_max(const ustring & spec, double max_size_pt);

    /// @}

private:

    Font_ptr impl;
};

} // namespace tau

#endif // __TAU_FONT_HH__

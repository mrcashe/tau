// ----------------------------------------------------------------------------
// Copyright © 2014-2024 Konstantin Shmelkov <mrcashe@gmail.com>.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
// THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
// PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
// CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
// EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
// PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
// OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
// WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
// OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
// EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// ----------------------------------------------------------------------------

/// @file input.hh Keyboard and mouse enumerations and functions.

#ifndef __TAU_INPUT_HH__
#define __TAU_INPUT_HH__

#include <tau/ustring.hh>

namespace tau {

/// Convert UCS-4 character into string.
/// If kc is a member of #Key_codes enum, the result will be name of kc
/// (all available at the moment key names are summarized in @ref kc_sect).
/// Otherwise, the result will looks like "U+xxxx", where "xxxx" is key code
/// doesn't matter whether it Unicode character or not.
/// @ingroup input_group
/// @sa @ref kbd_sect
/// @sa @ref kc_sect
__TAUEXPORT__ ustring key_code_to_string(char32_t kc);

/// Gets key code from a string.
/// @ingroup input_group
/// @sa @ref kbd_sect
/// @sa @ref kc_sect
__TAUEXPORT__ char32_t key_code_from_string(const ustring & str);

/// Convert key modifiers into string representation.
/// Resulting string will contain something like this: "\<CONTROL\>\<SHIFT\>".
/// @ingroup input_group
/// @sa @ref kbd_sect
/// @sa @ref km_sect
__TAUEXPORT__ ustring key_modifiers_to_string(int km);

/// Convert string representation to key modifiers.
/// To successful convert, the str must contain key modifiers textual representation
/// in form like this: "\<CONTROL\>\<Alt\>". The character case does not matter.
/// See complete list of all recognizable key modifier representation at @ref km_sect.
/// If no representations found, the @link KM_NONE @endlink value will be returned.
/// @param str the string possibly containing representation of any key modifiers.
/// @return bitwise mask of flags mentioned in #Key_modifiers enumeration.
/// @ingroup input_group
/// @sa @ref kbd_sect
/// @sa @ref km_sect
__TAUEXPORT__ int key_modifiers_from_string(const ustring & str);

/// Convert key specification (code+modifiers) into string representation.
/// @ingroup input_group
/// @sa @ref kbd_sect
/// @sa @ref km_sect
/// @sa @ref kc_sect
__TAUEXPORT__ ustring key_spec_to_string(char32_t kc, int km);

/// Convert key specification (code+modifiers) into human readable text representation.
/// @ingroup input_group
/// @sa @ref kbd_sect
/// @sa @ref km_sect
/// @sa @ref kc_sect
__TAUEXPORT__ ustring key_spec_to_label(char32_t kc, int km);

/// Extract key code and modifier from string.
/// @ingroup input_group
/// @sa @ref kbd_sect
/// @sa @ref km_sect
/// @sa @ref kc_sect
__TAUEXPORT__ void key_spec_from_string(const ustring & spec, char32_t & kc, int & km);

/// Check is key code a modifier key.
/// @ingroup input_group
/// @sa @ref kbd_sect
/// @sa @ref km_sect
/// @sa @ref kc_sect
__TAUEXPORT__ bool key_code_is_modifier(char32_t kc);

/// Mouse buttons.
/// The better name for those enumerators should be @b "MB_xxx" but
/// @b "MB_" prefix clashes with Windows mouse button defines, so
/// I added "T" letter.
/// @ingroup input_group
enum Mouse_buttons {

    /// Unspecified mouse button.
    MBT_NONE                     = 0,

    /// Left mouse button.
    MBT_LEFT                     = 1,

    /// Middle mouse button.
    MBT_MIDDLE                   = 2,

    /// Right mouse button.
    MBT_RIGHT                    = 3
};

/// Mouse modifiers.
/// @ingroup input_group
enum Mouse_modifiers {

    /// No modifiers.
    MM_NONE                     = 0x0000,

    /// Left mouse modifier.
    MM_LEFT                     = 0x0001,

    /// Middle mouse modifier.
    MM_MIDDLE                   = 0x0002,

    /// Right mouse modifier.
    MM_RIGHT                    = 0x0004,

    /// Shift mouse modifier.
    MM_SHIFT                    = 0x0100,

    /// Control mouse modifier.
    MM_CONTROL                  = 0x0200
};

/// Key modifiers.
/// @ingroup input_group
enum Key_modifiers {

    /// None pressed.
    KM_NONE                     = 0x00000000,

    /// Shift pressed.
    KM_SHIFT                    = 0x00000001,

    /// Control pressed.
    KM_CONTROL                  = 0x00000002,

    /// Alt pressed.
    KM_ALT                      = 0x00000004,

    /// Meta pressed.
    KM_META                     = 0x00000008,

    /// Windows pressed.
    KM_WIN                      = 0x00000010,

    /// Super pressed.
    KM_SUPER                    = 0x00000020,

    /// %Menu pressed.
    KM_MENU                     = 0x00000040,

    /// Hyper pressed.
    KM_HYPER                    = 0x00000080,

    /// Group pressed.
    KM_GROUP                    = 0x00000100,

    /// NumLock pressed.
    KM_NUMLOCK                  = 0x00000200,

    /// ScrollLock pressed.
    KM_SCROLL                   = 0x00000400,

    /// Caps pressed.
    KM_CAPS                     = 0x00000800
};

/// Key codes, 32 bit.
/// @ingroup input_group
enum Key_codes {

    /// None pressed or unknown.
    KC_NONE                     = 0x00000000,

    /// BackSpace.
    KC_BACKSPACE                = 0x00000008,

    /// Tab.
    KC_TAB                      = 0x00000009,

    /// LineFeed
    KC_LINEFEED                 = 0x0000000A,

    /// Enter.
    KC_ENTER                    = 0x0000000D,

    /// Synonymous Enter.
    KC_RETURN                   = 0x0000000D,

    /// Escape.
    KC_ESCAPE                   = 0x0000001B,

    /// Space.
    KC_SPACE                    = 0x00000020,

    /// Delete.
    KC_DELETE                   = 0x0000007F,

    /// Functional, F1.
    KC_F1                       = 0x10000001,

    /// Functional, F2.
    KC_F2                       = 0x10000002,

    /// Functional, F3.
    KC_F3                       = 0x10000003,

    /// Functional, F4.
    KC_F4                       = 0x10000004,

    /// Functional, F5.
    KC_F5                       = 0x10000005,

    /// Functional, F6.
    KC_F6                       = 0x10000006,

    /// Functional, F7.
    KC_F7                       = 0x10000007,

    /// Functional, F8.
    KC_F8                       = 0x10000008,

    /// Functional, F9.
    KC_F9                       = 0x10000009,

    /// Functional, F10.
    KC_F10                      = 0x1000000A,

    /// Functional, F11.
    KC_F11                      = 0x1000000B,

    /// Functional, F12.
    KC_F12                      = 0x1000000C,

    /// Functional, F13.
    KC_F13                      = 0x1000000D,

    /// Functional, F14.
    KC_F14                      = 0x1000000E,

    /// Functional, F15.
    KC_F15                      = 0x1000000F,

    /// Functional, F16.
    KC_F16                      = 0x10000010,

    /// Functional, F17.
    KC_F17                      = 0x10000011,

    /// Functional, F18.
    KC_F18                      = 0x10000012,

    /// Functional, F19.
    KC_F19                      = 0x10000013,

    /// Functional, F20.
    KC_F20                      = 0x10000014,

    /// Functional, F21.
    KC_F21                      = 0x10000015,

    /// Functional, F22.
    KC_F22                      = 0x10000016,

    /// Functional, F23.
    KC_F23                      = 0x10000017,

    /// Functional, F24.
    KC_F24                      = 0x10000018,

    /// Functional, F25.
    KC_F25                      = 0x10000019,

    /// Functional, F26.
    KC_F26                      = 0x1000001A,

    /// Functional, F27.
    KC_F27                      = 0x1000001B,

    /// Functional, F28.
    KC_F28                      = 0x1000001C,

    /// Functional, F29.
    KC_F29                      = 0x1000001D,

    /// Functional, F30.
    KC_F30                      = 0x1000001E,

    /// Functional, F31.
    KC_F31                      = 0x1000001F,

    /// Functional, F32.
    KC_F32                      = 0x10000020,

    /// Functional, F33.
    KC_F33                      = 0x10000021,

    /// Functional, F34.
    KC_F34                      = 0x10000022,

    /// Functional, F35.
    KC_F35                      = 0x10000023,

    /// Left.
    KC_LEFT                     = 0x20000000,

    /// Right.
    KC_RIGHT                    = 0x20000001,

    /// Up.
    KC_UP                       = 0x20000008,

    /// Down.
    KC_DOWN                     = 0x20000009,

    /// Home.
    KC_HOME                     = 0x20000010,

    /// End.
    KC_END                      = 0x20000011,

    /// Page Up.
    KC_PAGE_UP                  = 0x20000020,

    /// Page Down.
    KC_PAGE_DOWN                = 0x20000021,

    /// Scroll Up.
    KC_SCROLL_UP                = 0x20000028,

    /// Scroll Down.
    KC_SCROLL_DOWN              = 0x20000029,

    /// Insert.
    KC_INSERT                   = 0x20000030,

    /// Begin.
    KC_BEGIN                    = 0x20000040,

    /// Cancel.
    KC_CANCEL                   = 0x10000050,

    /// Break.
    KC_BREAK                    = 0x10000058,

    /// Clear.
    KC_CLEAR                    = 0x10000060,

    /// Pause.
    KC_PAUSE                    = 0x20000070,

    /// Print.
    KC_PRINT                    = 0x20000080,

    /// Sys Request.
    KC_SYSREQ                   = 0x20000090,

    /// Help.
    KC_HELP                     = 0x200000A0,

    /// Undo.
    KC_UNDO                     = 0x200000B0,

    /// Redo.
    KC_REDO                     = 0x200000B1,

    /// Find.
    KC_FIND                     = 0x200000C0,

    /// Select.
    KC_SELECT                   = 0x200000D0,

    /// No.
    KC_NO                       = 0x200000E0,

    /// Yes.
    KC_YES                      = 0x200000E1,

    /// Copy.
    KC_COPY                     = 0x200000F0,

    /// Cut.
    KC_CUT                      = 0x200000F2,

    /// Paste.
    KC_PASTE                    = 0x200000F4,

    /// Left Direction.
    KC_LDIR                     = 0x30000010,

    /// Right Direction.
    KC_RDIR                     = 0x30000020,

    /// Multi.
    KC_MULTI                    = 0x30000030,

    /// Code Input.
    KC_CODE_INPUT               = 0x30000040,

    /// Single Candidate.
    KC_SINGLE_CANDIDATE         = 0x30000050,

    /// Multiple Candidate.
    KC_MULTIPLE_CANDIDATE       = 0x30000060,

    /// Previous Candidate.
    KC_PREVIOUS_CANDIDATE       = 0x30000070,

    /// Mode Switch.
    KC_MODE_SWITCH              = 0x30000080,

    /// Often comma.
    KC_SEPARATOR                = 0x30000090,

    /// Decimal.
    KC_DECIMAL                  = 0x300000A0,

    /// ISO Lock.
    KC_ISO_LOCK                 = 0x300000B0,

    /// Level 2 Latch.
    KC_LEVEL2_LATCH             = 0x300000C0,

    /// Level 3 Shift.
    KC_LEVEL3_SHIFT             = 0x300000D0,

    /// Level 3 Latch.
    KC_LEVEL3_LATCH             = 0x300000E0,

    /// Level 3 Lock.
    KC_LEVEL3_LOCK              = 0x300000F0,

    /// Level 5 Shift.
    KC_LEVEL5_SHIFT             = 0x30000100,

    /// Level 5 Latch.
    KC_LEVEL5_LATCH             = 0x30000110,

    /// Level 5 Lock.
    KC_LEVEL5_LOCK              = 0x30000120,

    /// Group Shift.
    KC_GROUP_SHIFT              = 0x30000130,

    /// Group Latch.
    KC_GROUP_LATCH              = 0x30000140,

    /// Group Lock.
    KC_GROUP_LOCK               = 0x30000150,

    /// Next Group.
    KC_NEXT_GROUP               = 0x30000160,

    /// Next Group Lock.
    KC_NEXT_GROUP_LOCK          = 0x30000170,

    /// Prev Group.
    KC_PREV_GROUP               = 0x30000180,

    /// Prev Group Lock.
    KC_PREV_GROUP_LOCK          = 0x30000190,

    /// First Group.
    KC_FIRST_GROUP              = 0x300001A0,

    /// First Group Lock.
    KC_FIRST_GROUP_LOCK         = 0x300001B0,

    /// Last Group.
    KC_LAST_GROUP               = 0x300001C0,

    /// Last Group Lock.
    KC_LAST_GROUP_LOCK          = 0x300001D0,

    /// Left Tab.
    KC_LEFT_TAB                 = 0x300001E0,

    /// Move Line Up.
    KC_MOVE_LINE_UP             = 0x300001F0,

    /// Move Line Down.
    KC_MOVE_LINE_DOWN           = 0x300001F1,

    /// Partial Space Left.
    KC_PARTIAL_SPACE_LEFT       = 0x30000200,

    /// Partial Space Right.
    KC_PARTIAL_SPACE_RIGHT      = 0x30000201,

    /// Partial Line Up.
    KC_PARTIAL_LINE_UP          = 0x30000210,

    /// Partial Line Down.
    KC_PARTIAL_LINE_DOWN        = 0x30000211,

    /// Set Margin Left.
    KC_SET_MARGIN_LEFT          = 0x30000220,

    /// Set Margin Right.
    KC_SET_MARGIN_RIGHT         = 0x30000221,

    /// Release Margin Left.
    KC_RELEASE_MARGIN_LEFT      = 0x30000230,

    /// Release Margin Right.
    KC_RELEASE_MARGIN_RIGHT     = 0x30000231,

    /// Release Both Margins.
    KC_RELEASE_BOTH_MARGINS     = 0x30000238,

    /// Fast %Cursor Left.
    KC_FAST_CURSOR_LEFT         = 0x30000240,

    /// Fast %Cursor Right.
    KC_FAST_CURSOR_RIGHT        = 0x30000241,

    /// Fast %Cursor Up.
    KC_FAST_CURSOR_UP           = 0x30000248,

    /// Fast %Cursor Down.
    KC_FAST_CURSOR_DOWN         = 0x30000249,

    /// Continuous Underline.
    KC_CONTINUOUS_UNDERLINE     = 0x30000250,

    /// Discontinuous Underline.
    KC_DISCONTINUOUS_UNDERLINE  = 0x30000251,

    /// Emphasize.
    KC_EMPHASIZE                = 0x30000260,

    /// Center Object.
    KC_CENTER_OBJECT            = 0x30000270,

    /// Send mail, file, object.
    KC_SEND                     = 0x30000280,

    /// Reply e.g., mail.
    KC_REPLY                    = 0x30000281,

    /// Zoom in view, map, etc.
    KC_ZOOM_IN                  = 0x30000290,

    /// Zoom out view, map, etc.
    KC_ZOOM_OUT                 = 0x30000291,

    /// Forward.
    KC_FORWARD                  = 0x30000320,

    /// Back.
    KC_BACK                     = 0x30000321,

    /// Stop.
    KC_STOP                     = 0x30000340,

    /// Refresh.
    KC_REFRESH                  = 0x30000350,

    /// Home Page.
    KC_HOME_PAGE                = 0x30000360,

    /// Favorites.
    KC_FAVORITES                = 0x30000370,

    /// Search.
    KC_SEARCH                   = 0x30000380,

    /// Standby.
    KC_STANDBY                  = 0x30000390,

    /// Open URL.
    KC_OPEN_URL                 = 0x300003A0,

    /// Launch Mail.
    KC_LAUNCH_MAIL              = 0x300003B0,

    /// Launch Media Player.
    KC_LAUNCH_MEDIA             = 0x300003C0,

    /// Launch 0.
    KC_LAUNCH0                  = 0x300003D0,

    /// Launch 1.
    KC_LAUNCH1                  = 0x300003D1,

    /// Launch 2.
    KC_LAUNCH2                  = 0x300003D2,

    /// Launch 3.
    KC_LAUNCH3                  = 0x300003D3,

    /// Launch 4.
    KC_LAUNCH4                  = 0x300003D4,

    /// Launch 5.
    KC_LAUNCH5                  = 0x300003D5,

    /// Launch 6.
    KC_LAUNCH6                  = 0x300003D6,

    /// Launch 7.
    KC_LAUNCH7                  = 0x300003D7,

    /// Launch 8.
    KC_LAUNCH8                  = 0x300003D8,

    /// Launch 9.
    KC_LAUNCH9                  = 0x300003D9,

    /// Launch A.
    KC_LAUNCHA                  = 0x300003DA,

    /// Launch B.
    KC_LAUNCHB                  = 0x300003DB,

    /// Launch C.
    KC_LAUNCHC                  = 0x300003DC,

    /// Launch D.
    KC_LAUNCHD                  = 0x300003DD,

    /// Launch E.
    KC_LAUNCHE                  = 0x300003DE,

    /// Launch F.
    KC_LAUNCHF                  = 0x300003DF,

    /// Execute.
    KC_EXECUTE                  = 0x300003F0,

    /// Terminal.
    KC_TERMINAL                 = 0x300003F8,

    /// Power Off.
    KC_POWER_OFF                = 0x30000400,

    /// Sleep.
    KC_SLEEP                    = 0x30000402,

    /// Wake Up.
    KC_WAKE_UP                  = 0x30000408,

    /// Terminate Server.
    KC_TERMINATE_SERVER         = 0x30000410,

    /// Close.
    KC_CLOSE                    = 0x30000420,

    /// Launch phone; dial number.
    KC_PHONE                    = 0x30000430,

    /// Reload web page, file, etc.
    KC_RELOAD                   = 0x30000440,

    /// Save (file, document, state).
    KC_SAVE                     = 0x30000450,

    /// Launch video player.
    KC_VIDEO                    = 0x30000460,

    /// Launch music application.
    KC_MUSIC                    = 0x30000462,

    /// %Display battery information.
    KC_BATTERY                  = 0x30000470,

    /// Enable/disable Bluetooth.
    KC_BLUETOOTH                = 0x30000472,

    /// Enable/disable WLAN.
    KC_WLAN                     = 0x30000474,

    /// Volume Up.
    KC_VOLUME_UP                = 0x37000010,

    /// Volume Down.
    KC_VOLUME_DOWN              = 0x37000030,

    /// Volume Mute.
    KC_VOLUME_MUTE              = 0x37000050,

    /// Bass Boost.
    KC_BASS_BOOST               = 0x37000070,

    /// Bass Up.
    KC_BASS_UP                  = 0x37000090,

    /// Bass Down.
    KC_BASS_DOWN                = 0x370000B0,

    /// Treble Up.
    KC_TREBLE_UP                = 0x370000D0,

    /// Treble Down.
    KC_TREBLE_DOWN              = 0x370000F0,

    /// Media Play.
    KC_MEDIA_PLAY               = 0x37000110,

    /// Media Pause.
    KC_MEDIA_PAUSE              = 0x37000130,

    /// Media Stop.
    KC_MEDIA_STOP               = 0x37000150,

    /// Media Previous.
    KC_MEDIA_PREVIOUS           = 0x37000170,

    /// Media Next.
    KC_MEDIA_NEXT               = 0x37000190,

    /// Media Record.
    KC_MEDIA_RECORD             = 0x370001B0,

    // Japanese keyboard support.
    /// Kanji, Kanji convert.
    KC_KANJI                    = 0x41000010,

    /// Cancel Conversion.
    KC_MUHENKAN                 = 0x41000020,

    /// Alias for Henkan_Mode.
    KC_HENKAN                   = 0x41000030,

    /// To Romaji.
    KC_ROMAJI                   = 0x41000040,

    /// To Hiragana.
    KC_HIRAGANA                 = 0x41000050,

    /// To Katakana.
    KC_KATAKANA                 = 0x41000060,

    /// Hiragana/Katakana toggle.
    KC_HIRAGANA_KATAKANA        = 0x41000070,

    /// To Zenkaku.
    KC_ZENKAKU                  = 0x41000080,

    /// To Hankaku.
    KC_HANKAKU                  = 0x41000090,

    /// Zenkaku/Hankaku toggle.
    KC_ZENKAKU_HANKAKU          = 0x410000A0,

    /// Add to Dictionary.
    KC_TOUROKU                  = 0x410000B0,

    /// Delete from Dictionary.
    KC_MASSYO                   = 0x410000C0,

    /// Kana Lock.
    KC_KANA_LOCK                = 0x410000D0,

    /// Kana Shift.
    KC_KANA_SHIFT               = 0x410000E0,

    /// Alphanumeric Shift.
    KC_EISU_SHIFT               = 0x410000F0,

    /// Alphanumeric toggle.
    KC_EISU_TOGGLE              = 0x41000100,

    // Korean keyboard support.
    /// Hangul start/stop(toggle).
    KC_HANGUL                   = 0x42000010,

    /// Hangul start.
    KC_HANGUL_START             = 0x42000020,

    /// Hangul end, English start.
    KC_HANGUL_END               = 0x42000030,

    /// Start Hangul->Hanja Conversion.
    KC_HANGUL_HANJA             = 0x42000040,

    /// Hangul Jamo mode.
    KC_HANGUL_JAMO              = 0x42000050,

    /// Hangul Romaja mode.
    KC_HANGUL_ROMAJA            = 0x42000060,

    /// Jeonja mode.
    KC_HANGUL_JEONJA            = 0x42000080,

    /// Banja mode.
    KC_HANGUL_BANJA             = 0x42000090,

    /// Pre Hanja conversion.
    KC_HANGUL_PREHANJA          = 0x420000A0,

    /// Post Hanja conversion.
    KC_HANGUL_POSTHANJA         = 0x420000B0,

    /// Special symbols.
    KC_HANGUL_SPECIAL           = 0x420000F0,

    // Braille
    /// Braille Dot 1.
    KC_BRAILLE_DOT_1            = 0x50000010,

    /// Braille Dot 2.
    KC_BRAILLE_DOT_2            = 0x50000020,

    /// Braille Dot 3.
    KC_BRAILLE_DOT_3            = 0x50000030,

    /// Braille Dot 4.
    KC_BRAILLE_DOT_4            = 0x50000040,

    /// Braille Dot 5.
    KC_BRAILLE_DOT_5            = 0x50000050,

    /// Braille Dot 6.
    KC_BRAILLE_DOT_6            = 0x50000060,

    /// Braille Dot 7.
    KC_BRAILLE_DOT_7            = 0x50000070,

    /// Braille Dot 8.
    KC_BRAILLE_DOT_8            = 0x50000080,

    /// Braille Dot 9.
    KC_BRAILLE_DOT_9            = 0x50000090,

    /// Braille Dot 10.
    KC_BRAILLE_DOT_10           = 0x500000a0,

    // Modifiers
    /// Left Shift.
    KC_LSHIFT                   = 0x70000110,

    /// Right Shift.
    KC_RSHIFT                   = 0x70000120,

    /// Shift.
    KC_SHIFT                    = 0x70000130,

    /// Left Control.
    KC_LCONTROL                 = 0x70000140,

    /// Right Control.
    KC_RCONTROL                 = 0x70000150,

    /// Control.
    KC_CONTROL                  = 0x70000160,

    /// Left Alt.
    KC_LALT                     = 0x70000170,

    /// Right Alt.
    KC_RALT                     = 0x70000180,

    /// Alt.
    KC_ALT                      = 0x70000190,

    /// Left Meta.
    KC_LMETA                    = 0x700001A0,

    /// Right Meta.
    KC_RMETA                    = 0x700001B0,

    /// Meta.
    KC_META                     = 0x700001C0,

    /// Left Windows.
    KC_LWIN                     = 0x700001D0,

    /// Right Windows.
    KC_RWIN                     = 0x700001E0,

    /// Windows.
    KC_WIN                      = 0x700001F0,

    /// Left Super.
    KC_LSUPER                   = 0x70000200,

    /// Right Super.
    KC_RSUPER                   = 0x70000210,

    /// Super.
    KC_SUPER                    = 0x70000220,

    /// Left %Menu.
    KC_LMENU                    = 0x70000230,

    /// Right %Menu.
    KC_RMENU                    = 0x70000240,

    /// %Menu.
    KC_MENU                     = 0x70000250,

    /// Left Hyper.
    KC_LHYPER                   = 0x70000260,

    /// Right Hyper.
    KC_RHYPER                   = 0x70000270,

    /// Hyper.
    KC_HYPER                    = 0x70000280,

    /// Group.
    KC_GROUP                    = 0x70000290,

    /// Num Lock.
    KC_NUM_LOCK                 = 0x700002A0,

    /// Scroll Lock.
    KC_SCROLL_LOCK              = 0x700002B0,

    /// Caps Lock.
    KC_CAPS_LOCK                = 0x700002C0,

    /// Shift Lock.
    KC_SHIFT_LOCK               = 0x700002D0
};

} // namespace tau

#endif // __TAU_INPUT_HH__

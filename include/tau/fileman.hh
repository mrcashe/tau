// ----------------------------------------------------------------------------
// SPDX-License-Identifier: BSD-2-Clause
// Copyright © 2014-2024 Konstantin Shmelkov <mrcashe@gmail.com>.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
// THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
// PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
// CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
// EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
// PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
// OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
// WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
// OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
// EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// ----------------------------------------------------------------------------

/// @file fileman.hh The Fileman class.

#ifndef __TAU_FILEMAN_HH__
#define __TAU_FILEMAN_HH__

#include <tau/ustring.hh>
#include <tau/widget.hh>

namespace tau {

/// File navigation and selection widget.
/// @note This class is a wrapper around its implementation shared pointer.
///
/// @ingroup file_group
/// @ingroup widget_group
class __TAUEXPORT__ Fileman: public Widget {
public:

    /// Fileman modes.
    /// @ingroup fileman_modes_group
    enum Mode {

        /// File manager in browse mode.
        BROWSE,

        /// File manager in open mode.
        OPEN,

        /// File manager in save mode.
        SAVE
    };

    /// @name Constructors & Operators
    /// @{

    /// Constructor with mode and path.
    /// @param fm_mode file manager working mode, see @ref fileman_modes_group.
    /// @param uri path to start browsing with or home directory if empty or not found.
    Fileman(Mode fm_mode, const ustring & uri=ustring());

    /// Copy constructor.
    ///
    /// @note This class is a wrapper around its implementation shared pointer,
    /// so copying it just increasing implementation pointer use count, but isn't
    /// really copies the object. The underlying implementation is not copyable.
    Fileman(const Fileman & other);

    /// Copy operator.
    ///
    /// @note This class is a wrapper around its implementation shared pointer,
    /// so copying it just increasing implementation pointer use count, but isn't
    /// really copies the object. The underlying implementation is not copyable.
    Fileman & operator=(const Fileman & other);

    /// Move constructor.
    ///
    /// @note This class is a wrapper around its implementation shared pointer,
    /// so copying it just increasing implementation pointer use count, but isn't
    /// really copies the object. The underlying implementation is not copyable.
    /// @note This constructor does not reset @c other implementation pointer.
    Fileman(Fileman && other);

    /// Move operator.
    ///
    /// @note This class is a wrapper around its implementation shared pointer,
    /// so copying it just increasing implementation pointer use count, but isn't
    /// really copies the object. The underlying implementation is not copyable.
    /// @note This operator does not reset @c other implementation pointer.
    Fileman & operator=(Fileman && other);

    /// Constructor with implementation pointer.
    ///
    /// @warning Unlike some other classes (Painter as an example), the whole
    /// @ref widget_stack "widget stack" is unable to run with pure implementation
    /// pointer, so attempting to construct widget from a pure (@b nullptr) pointer
    /// will cause throwing an user_error exception!
    /// That exception also will be thrown if user tries to construct the object
    /// from incompatible implementation shared pointer.
    ///
    /// @throw user_error in case of pure implementation pointer or incompatible
    /// implementation pointer class.
    Fileman(Widget_ptr wp);

    /// Assign implementation.
    ///
    /// @throw user_error in case of pure implementation pointer or incompatible
    /// implementation pointer class.
    /// @since 0.4.0
    Fileman & operator=(Widget_ptr wp);

    /// @}
    /// @name Controls
    /// @{

    /// Get current URI.
    ustring uri() const;

    /// Get mode of operation.
    /// @return file manager working mode, see @ref fileman_modes_group.
    Mode mode() const noexcept;

    /// Change current URI.
    void set_uri(const ustring & uri);

    /// Get buil-in Navigator object.
    Navigator navigator() noexcept;

    /// Gets selected filenames without path component.
    std::vector<ustring> selection() const;

    /// Gets text from entry.
    ustring entry() const;

    /// Add filter.
    void add_filter(const ustring & patterns, const ustring & title=ustring());

    /// Get current filter.
    ustring filter() const;

    /// Show info items.
    /// @param opt  option name, see @ref navigator_info for meaning.
    /// @sa reset_option() has_option() options()
    void set_option(std::string_view opt);

    /// Hide info items.
    /// @param opt  option name, see @ref navigator_info for meaning.
    /// @sa set_option() has_option() options()
    void reset_option(std::string_view opt);

    /// Test if option active.
    /// @param opt option name, see @ref navigator_info for meaning.
    /// @sa set_option() reset_option() options()
    bool has_option(std::string_view opt) const noexcept;

    /// %List active options.
    /// @param sep list separator
    /// @return option list, see @ref navigator_info for meaning.
    /// @sa set_option() reset_option() has_option()
    std::string options(char32_t sep=U':') const;

    /// Allow file overwrite without a prompt.
    /// Disallowed by default.
    void allow_overwrite();

    /// Disallow file overwrite without a prompt.
    /// Disallowed by default.
    void disallow_overwrite();

    /// Test if file overwrite without a prompt allowed.
    bool overwrite_allowed() const noexcept;

    /// Load state from the Key_file.
    void load_state(Key_file & kf, Key_section & sect);

    /// Save state to the Key_file.
    void save_state(Key_file & kf, Key_section & sect);

    /// @}
    /// @name Actions
    /// @{

    /// Get "Apply" action.
    Action & action_apply();

    /// Get "Cancel" action.
    Action & action_cancel();

    /// @}
};

} // namespace tau

#endif // __TAU_FILEMAN_HH__

// ----------------------------------------------------------------------------
// SPDX-License-Identifier: BSD-2-Clause
// Copyright © 2014-2024 Konstantin Shmelkov <mrcashe@gmail.com>.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
// THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
// PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
// CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
// EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
// PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
// OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
// WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
// OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
// EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// ----------------------------------------------------------------------------

/// @file list.hh List_base, List and List_text class declarations.

#ifndef __TAU_LIST_HH__
#define __TAU_LIST_HH__

#include <tau/container.hh>
#include <tau/ustring.hh>

namespace tau {

/// Common functionality for List and List_base classes.
/// @ingroup compound_container_group
class __TAUEXPORT__ List_base: public Container {
public:

    /// @name Controls
    /// @{

    /// Get span.
    /// @since 0.5.0
    Span span() const noexcept;

    /// Remove all.
    void clear();

    /// Test if empty.
    bool empty() const noexcept;

    /// Get row count.
    /// @since 0.5.0
    std::size_t count() const noexcept;

    /// Get widgets from specified row.
    /// @since 0.6.0
    std::list<Widget_ptr> widgets(int row) const;

    /// Get text widget at given row.
    /// @param row row number.
    /// @param col column number.
    /// @return implementation pointer of the text widget or pure pointer if not found.
    /// @since 0.6.0
    Widget_ptr widget_at(int row, int col=0) noexcept;

    /// Get text widget at given row.
    /// @param row row number.
    /// @param col column number.
    /// @return implementation pointer of the text widget or pure pointer if not found.
    /// @since 0.6.0
    Widget_cptr widget_at(int row, int col=0) const noexcept;

    /// @}
    /// @name Selection
    /// @{

    /// Select specified row.
    /// @return @b INT_MIN if no selection available or row index.
    int select(int row);

    /// Select next row.
    /// @return @b INT_MIN if no selection available or row index.
    /// @since 0.4.0
    int select_next();

    /// Select previous row.
    /// @return @b INT_MIN if no selection available or row index.
    /// @since 0.4.0
    int select_previous();

    /// Select first row.
    /// @return @b INT_MIN if no selection available or row index.
    int select_front();

    /// Select last row.
    /// @return @b INT_MIN if no selection available or row index.
    int select_back();

    /// Get selected row.
    /// @return @b INT_MIN if no selection available or row index.
    /// @sa selection()
    int current() const noexcept;

    /// Get selected rows.
    /// Useful when multiple select allowed.
    /// @sa current()
    /// @sa multiple_select_allowed()
    /// @since 0.6.0
    std::vector<int> selection() const;

    /// Test if has selection.
    /// @since 0.5.0
    bool has_selection() const noexcept;

    /// Clear selection.
    void unselect();

    /// Allow multiple select.
    /// Disallowed by default.
    /// @sa disallow_multiple_select()
    /// @sa multiple_select_allowed()
    /// @sa selection()
    void allow_multiple_select() noexcept;

    /// Disallow multiple select.
    /// Disallowed by default.
    /// @sa allow_multiple_select()
    /// @sa multiple_select_allowed()
    /// @sa selection()
    void disallow_multiple_select() noexcept;

    /// Test if multiple select allowed.
    /// Disallowed by default.
    /// @sa allow_multiple_select()
    /// @sa disallow_multiple_select()
    /// @sa selection()
    bool multiple_select_allowed() const noexcept;

    /// Test if selection fixed.
    /// @since 0.6.0
    bool fixed() const noexcept;

    /// Unfix selection.
    /// Allows selection to be altered.
    /// @sa select()
    /// @since 0.6.0
    void unfix();

    /// @}
    /// @name Alignment
    /// @{

    /// Set specified column alignment.
    /// When single column align set, the overall column align ignored.
    /// @param x column index.
    /// @param xalign the align.
    /// @since 0.6.0
    void align_column(int x, Align xalign);

    /// Get specified column alignment.
    /// @param x column index.
    /// @since 0.6.0
    Align column_align(int x) const noexcept;

    /// Unset specified column alignment.
    /// Unsets align prevously set by align_column().
    /// @param x column index.
    /// @since 0.6.0
    void unalign_column(int x);

    /// Set specified row alignment.
    /// When single row align set, the overall row align ignored.
    /// @param y row index.
    /// @param yalign the align.
    /// @since 0.6.0
    void align_row(int y, Align yalign);

    /// Get specified row alignment.
    /// @since 0.6.0
    Align row_align(int y) const noexcept;

    /// Unset specified row alignment.
    /// Unsets row align previously set by align_row().
    /// @param y row index.
    /// @since 0.6.0
    void unalign_row(int y);

    /// @}
    /// @name Geometry
    /// @{

    /// Set column spacing in pixels.
    /// @param spacing the spacing in pixels along X axis.
    void set_column_spacing(unsigned spacing);

    /// Set row spacing in pixels.
    /// @param spacing the spacing in pixels along Y axis.
    void set_row_spacing(unsigned spacing);

    /// Get spacing in pixels.
    /// @return an @b std::pair, where @c .first member is for @c xspacing and @c .second is for @c yspacing.
    std::pair<unsigned, unsigned> spacing() const noexcept;

    /// Set column width (in pixels).
    /// @param column the column number.
    /// @param width column width in pixels.
    void set_column_width(int column, unsigned width);

    /// Get column width (in pixels).
    /// @param column the column number.
    /// @return column width in pixels previously set by column_width().
    unsigned column_width(int column) const;

    /// Set row height (in pixels).
    /// @param row the row number.
    /// @param height row height in pixels.
    void set_row_height(int row, unsigned height);

    /// Get row height (in pixels).
    /// @param row the row number.
    /// @return row height in pixels previously set by row_height().
    unsigned row_height(int row) const noexcept;

    /// Set minimal column width (in pixels).
    /// @param column the column number.
    /// @param width minimal column width in pixels.
    void set_min_column_width(int column, unsigned width);

    /// Get minimal column width (in pixels).
    /// @param column the column number.
    /// @return minimal column width in pixels previously set by min_column_width().
    unsigned min_column_width(int column) const noexcept;

    /// Set minimal row height (in pixels).
    /// @param row the row number.
    /// @param height minimal row height in pixels.
    void set_min_row_height(int row, unsigned height);

    /// Get minimal row height (in pixels).
    /// @param row the row number.
    /// @return minimal row height in pixels previously set by min_row_height().
    unsigned min_row_height(int row) const noexcept;

    /// Set maximal column width (in pixels).
    /// @param column the column number.
    /// @param width maximal column width in pixels.
    void set_max_column_width(int column, unsigned width);

    /// Get maximal column width (in pixels).
    /// @param column the column number.
    /// @return maximal column width in pixels previously set by max_column_width().
    unsigned max_column_width(int column) const noexcept;

    /// Set maximal row height (in pixels).
    /// @param row the row number.
    /// @param height maximal row height in pixels.
    void set_max_row_height(int row, unsigned height);

    /// Get maximal row height (in pixels).
    /// @param row the row number.
    /// @return minimal row height in pixels previously set by max_row_height().
    unsigned max_row_height(int row) const noexcept;

    /// Set column margins.
    /// @param x the column coordinate in cells.
    /// @param left the left margin in pixels.
    /// @param right the left margin in pixels.
    /// When single column margins are set, the overall column margins ignored.
    void set_column_margin(int x, unsigned left, unsigned right);

    /// Set column margins.
    /// @param x the column coordinate in cells.
    /// @param margin the margins.
    /// When single column margins are set, the overall column margins ignored.
    /// @since 0.6.0
    void set_column_margin(int x, const Margin & margin);

    /// Set row margins.
    /// @param y the row coordinate in cells.
    /// @param top the top margin in pixels.
    /// @param bottom the bottom margin in pixels.
    /// When single row margins are set, the overall row margins ignored.
    /// @since 0.6.0
    void set_row_margin(int y, unsigned top, unsigned bottom);

    /// Set row margins.
    /// @param y the row coordinate in cells.
    /// @param margin the margins.
    /// When single row margins are set, the overall row margins ignored.
    /// @since 0.6.0
    void set_row_margin(int y, const Margin & margin);

    /// Get specified column margins.
    /// @param x the column coordinate in cells.
    /// @return column margins with top & bottom members set to 0.
    Margin column_margin(int x) const noexcept;

    /// Get specified row margins.
    /// @param y the row coordinate in cells.
    /// @return row margins with left & right members set to 0.
    Margin row_margin(int y) const noexcept;

    /// Set all columns margins.
    /// @param left the left margin in pixels.
    /// @param right the left margin in pixels.
    /// @since 0.6.0
    void set_columns_margin(unsigned left, unsigned right);

    /// Set all columns margins.
    /// .left & .right members used.
    /// @since 0.6.0
    void set_columns_margin(const Margin & margin);

    /// Set all rows margins.
    /// @param top the top margin in pixels.
    /// @param bottom the bottom margin in pixels.
    /// @since 0.6.0
    void set_rows_margin(unsigned top, unsigned bottom);

    /// Set all rows margins.
    /// .top & .bottom members used.
    /// @since 0.6.0
    void set_rows_margin(const Margin & margin);

    /// Get columns margins.
    /// @return columns margins with top & bottom members set to 0.
    /// @since 0.6.0
    Margin columns_margin() const noexcept;

    /// Get rows margins.
    /// @return row margins with left & right members set to 0.
    /// @since 0.6.0
    Margin rows_margin() const noexcept;

    /// Get column bounds in pixels.
    /// @param column the column number.
    /// When @a column does not exist, the returning rectangle is empty.
    /// @since 0.6.0
    Rect column_bounds(int column) const noexcept;

    /// Get row bounds in pixels.
    /// @param row the column number.
    /// When @a row does not exist, the returning rectangle is empty.
    /// @since 0.6.0
    Rect row_bounds(int row) const noexcept;

    /// @}
    /// @name Actions
    /// @{

    /// Get "Cancel" action.
    /// @since 0.4.0
    Action & action_cancel();

    /// Get "Activate" action.
    /// @since 0.4.0
    Action & action_activate();

    /// Get "Up" action.
    /// @since 0.4.0
    Action & action_previous();

    /// Get "Down" action.
    /// @since 0.4.0
    Action & action_next();

    /// Get "Page Up" action.
    /// @since 0.4.0
    Action & action_previous_page();

    /// Get "Page Down" action.
    /// @since 0.4.0
    Action & action_next_page();

    /// Get "Home" action.
    /// @since 0.4.0
    Action & action_home();

    /// Get "End" action.
    /// @since 0.4.0
    Action & action_end();

    /// Get "Select Up" action.
    /// @since 0.4.0
    Action & action_select_previous();

    /// Get "Select Down" action.
    /// @since 0.4.0
    Action & action_select_next();

    /// Get "Select Page Up" action.
    /// @since 0.4.0
    Action & action_select_previous_page();

    /// Get "Select Page Down" action.
    /// @since 0.4.0
    Action & action_select_next_page();

    /// Get "Select Home" action.
    /// @since 0.4.0
    Action & action_select_home();

    /// Get "Select End" action.
    /// @since 0.4.0
    Action & action_select_end();

    /// Get "Select All" action.
    /// @since 0.7.0
    Action & action_select_all();

    /// @}
    /// @name Signals
    /// @{

    /// Signal emitted when some row selected.
    /// Slot prototype:
    /// ~~~~~~~~~~~~~~
    /// void on_select_row(int y);
    /// ~~~~~~~~~~~~~~
    signal<void(int)> & signal_select_row();

    /// Signal emitted when some row activated.
    /// Slot prototype:
    /// ~~~~~~~~~~~~~~
    /// void on_activate_row(int y);
    /// ~~~~~~~~~~~~~~
    signal<void(int)> & signal_activate_row();

    /// Signal emitted when some row removed.
    /// Slot prototype:
    /// ~~~~~~~~~~~~~~
    /// void on_remove_row(int y);
    /// ~~~~~~~~~~~~~~
    signal<void(int)> & signal_remove_row();

    /// Signal emitted when some row changed it position.
    /// Slot prototype:
    /// ~~~~~~~~~~~~~~
    /// void on_move_row(int y, int ynew);
    /// ~~~~~~~~~~~~~~
    signal<void(int, int)> & signal_move_row();

    /// Signal emitted when %List going to mark row.
    /// Returning true from this signal will prevent marking.
    /// Slot prototype:
    /// ~~~~~~~~~~~~~~
    /// bool on_mark_validate(int y);
    /// ~~~~~~~~~~~~~~
    signal_all<int> & signal_mark_validate();

    /// Signal emitted when column bounds changed.
    /// Slot prototype:
    /// ~~~~~~~~~~~~~~
    /// void on_column_bounds_changed(int x);
    /// ~~~~~~~~~~~~~~
    /// @since 0.6.0
    signal<void(int)> & signal_column_bounds_changed();

    /// @}

protected:

    /// @private
    List_base(Widget_ptr wp);

    /// @private
    List_base & operator=(Widget_ptr wp);

};

/// A container that allocates widgets along the Y axis.
///
/// @note This class is a wrapper around its implementation shared pointer.
///
/// One or more widgets form a row. Each row is identified by its coordinate
/// on the Y axis. The coordinates in this case do not imply the
/// pixel coordinates on the screen - on the contrary, now we are talking about
/// cells that hold the whole widget. The coordinates of two neighboring
/// rows differ by at least one. All widgets on the same row have
/// the same coordinates on the Y axis.
/// As already noted above, the row can consist of several widgets.
/// This principle allows you to build lists consisting of several columns.
/// All rowes are selectable. When list selects or deselects row, it sends
/// signal_select() or signal_unselect(), respectively, to each widget in the row.
/// In addition, it is possible to insert widgets outside the rowes. Such widgets
/// are not selectable and can serve for better structuring of information.
/// @ingroup compound_container_group
class __TAUEXPORT__ List: public List_base {
public:

    /// @name Constructors & Operators
    /// @{

    /// Default constructor.
    List();

    /// Constructor with spacing.
    /// @since 0.6.0
    List(unsigned spacing);

    /// Constructor with spacings.
    /// @since 0.6.0
    List(unsigned xspacing, unsigned yspacing);

    /// Copy constructor.
    ///
    /// @note This class is a wrapper around its implementation shared pointer,
    /// so copying it just increasing implementation pointer use count, but isn't
    /// really copies the object. The underlying implementation is not copyable.
    List(const List & other);

    /// Copy operator.
    ///
    /// @note This class is a wrapper around its implementation shared pointer,
    /// so copying it just increasing implementation pointer use count, but isn't
    /// really copies the object. The underlying implementation is not copyable.
    List & operator=(const List & other);

    /// Move constructor.
    ///
    /// @note This class is a wrapper around its implementation shared pointer,
    /// so copying it just increasing implementation pointer use count, but isn't
    /// really copies the object. The underlying implementation is not copyable.
    /// @note This constructor does not reset @c other implementation pointer.
    List(List && other);

    /// Move operator.
    ///
    /// @note This class is a wrapper around its implementation shared pointer,
    /// so copying it just increasing implementation pointer use count, but isn't
    /// really copies the object. The underlying implementation is not copyable.
    /// @note This operator does not reset @c other implementation pointer.
    List & operator=(List && other);

    /// Constructor with implementation pointer.
    ///
    /// @warning Unlike some other classes (Painter as an example), the whole
    /// @ref widget_stack "widget stack" is unable to run with pure implementation
    /// pointer, so attempting to construct widget from a pure (@b nullptr) pointer
    /// will cause throwing an user_error exception!
    /// That exception also will be thrown if user tries to construct the object
    /// from incompatible implementation shared pointer.
    ///
    /// @throw user_error in case of pure implementation pointer or incompatible
    /// implementation pointer class.
    /// @since 0.4.0
    List(Widget_ptr wp);

    /// Assign implementation.
    ///
    /// @throw user_error in case of pure implementation pointer or incompatible
    /// implementation pointer class.
    /// @since 0.4.0
    List & operator=(Widget_ptr wp);

    /// @}
    /// @name Widget Insertion & Removal
    /// @{

    /// Prepend selectable row.
    /// @throw user_error if w already inserted into another container.
    /// @throw internal_error if w has pure implementation pointer.
    int prepend_row(Widget & w, bool shrink=false);

    /// Prepend selectable row.
    /// @throw user_error if w already inserted into another container.
    /// @throw internal_error if w has pure implementation pointer.
    int prepend_row(Widget & w, Align align, bool shrink=false);

    /// Insert selectable row at specified position.
    /// @throw user_error if w already inserted into another container.
    /// @throw internal_error if w has pure implementation pointer.
    int insert_row(Widget & w, int position, bool shrink=false);

    /// Insert selectable row at specified position.
    /// @throw user_error if w already inserted into another container.
    /// @throw internal_error if w has pure implementation pointer.
    int insert_row(Widget & w, int position, Align align, bool shrink=false);

    /// Append selectable row.
    /// @throw user_error if w already inserted into another container.
    /// @throw internal_error if w has pure implementation pointer.
    int append_row(Widget & w, bool shrink=false);

    /// Append selectable row.
    /// @throw user_error if w already inserted into another container.
    /// @throw internal_error if w has pure implementation pointer.
    int append_row(Widget & w, Align align, bool shrink=false);

    /// Prepend non-selectable widget.
    /// @throw user_error if w already inserted into another container.
    /// @throw internal_error if w has pure implementation pointer.
    int prepend(Widget & w, bool shrink=false);

    /// Prepend non-selectable widget.
    /// @throw user_error if w already inserted into another container.
    /// @throw internal_error if w has pure implementation pointer.
    int prepend(Widget & w, Align align, bool shrink=false);

    /// Insert non-selectable widget at specified position.
    /// @throw user_error if w already inserted into another container.
    /// @throw internal_error if w has pure implementation pointer.
    int insert(Widget & w, int position, bool shrink=false);

    /// Insert non-selectable widget at specified position.
    /// @throw user_error if w already inserted into another container.
    /// @throw internal_error if w has pure implementation pointer.
    int insert(Widget & w, int position, Align align, bool shrink=false);

    /// Append non-selectable widget.
    /// @throw user_error if w already inserted into another container.
    /// @throw internal_error if w has pure implementation pointer.
    int append(Widget & w, bool shrink=false);

    /// Append non-selectable widget.
    /// @throw user_error if w already inserted into another container.
    /// @throw internal_error if w has pure implementation pointer.
    int append(Widget & w, Align align, bool shrink=false);

    /// Prepend widget into specified row.
    /// @throw user_error if w already inserted into another container.
    /// @throw internal_error if w has pure implementation pointer.
    /// @throw user_error (bad row).
    int prepend(int row, Widget & w, bool shrink=false);

    /// Prepend widget into specified row.
    /// @throw user_error if w already inserted into another container.
    /// @throw internal_error if w has pure implementation pointer.
    /// @throw user_error (bad row).
    int prepend(int row, Widget & w, Align align, bool shrink=false);

    /// Insert widget into specified row and specified position.
    /// @throw user_error if w already inserted into another container.
    /// @throw internal_error if w has pure implementation pointer.
    /// @throw user_error (bad row).
    int insert(int row, Widget & w, int position, bool shrink=false);

    /// Insert widget into specified row and specified position.
    /// @throw user_error if w already inserted into another container.
    /// @throw internal_error if w has pure implementation pointer.
    /// @throw user_error (bad row).
    int insert(int row, Widget & w, int position, Align align, bool shrink=false);

    /// Append widget into specified row.
    /// @throw user_error if w already inserted into another container.
    /// @throw internal_error if w has pure implementation pointer.
    /// @throw user_error (bad row).
    int append(int row, Widget & w, bool shrink=false);

    /// Append widget into specified row.
    /// @throw user_error if w already inserted into another container.
    /// @throw internal_error if w has pure implementation pointer.
    /// @throw user_error (bad row).
    int append(int row, Widget & w, Align align, bool shrink=false);

    /// Remove specified row.
    /// @param row      the row index.
    /// @return         removed rows count.
    std::size_t remove(int row);

    /// Remove row where specified widget found.
    /// @return         removed rows count.
    /// @since 0.6.0
    std::size_t remove(Widget & w);

    /// @}
};

/// %Text specialization of List container.
/// Has displayed text editing facilities.
///
/// @note This class is a wrapper around its implementation shared pointer.
///
/// @ingroup compound_container_group
class __TAUEXPORT__ List_text: public List_base {
public:

    /// @name Constructors and operators
    /// @{

    /// Default constructor.
    List_text(Align align=Align::START);

    /// Constructor with spacing.
    /// @since 0.6.0
    List_text(unsigned spacing, Align align=Align::START);

    /// Constructor with spacings.
    /// @since 0.6.0
    List_text(unsigned xspacing, unsigned yspacing, Align align=Align::START);

    /// Constructor with strings and alignment.
    List_text(const std::vector<ustring> & sv, Align align=Align::START);

    /// Constructor with strings, spacing and alignment.
    /// @since 0.6.0
    List_text(const std::vector<ustring> & sv, unsigned spacing, Align align=Align::START);

    /// Constructor with strings, spacings and alignment.
    /// @since 0.6.0
    List_text(const std::vector<ustring> & sv, unsigned xspacing, unsigned yspacing, Align align=Align::START);

    /// Copy constructor.
    ///
    /// @note This class is a wrapper around its implementation shared pointer,
    /// so copying it just increasing implementation pointer use count, but isn't
    /// really copies the object. The underlying implementation is not copyable.
    List_text(const List_text & other);

    /// Copy operator.
    ///
    /// @note This class is a wrapper around its implementation shared pointer,
    /// so copying it just increasing implementation pointer use count, but isn't
    /// really copies the object. The underlying implementation is not copyable.
    List_text & operator=(const List_text & other);

    /// Move constructor.
    ///
    /// @note This class is a wrapper around its implementation shared pointer,
    /// so copying it just increasing implementation pointer use count, but isn't
    /// really copies the object. The underlying implementation is not copyable.
    /// @note This constructor does not reset @c other implementation pointer.
    List_text(List_text && other);

    /// Move operator.
    ///
    /// @note This class is a wrapper around its implementation shared pointer,
    /// so copying it just increasing implementation pointer use count, but isn't
    /// really copies the object. The underlying implementation is not copyable.
    /// @note This operator does not reset @c other implementation pointer.
    List_text & operator=(List_text && other);

    /// Constructor with implementation pointer.
    ///
    /// @warning Unlike some other classes (Painter as an example), the whole
    /// @ref widget_stack "widget stack" is unable to run with pure implementation
    /// pointer, so attempting to construct widget from a pure (@b nullptr) pointer
    /// will cause throwing an user_error exception!
    /// That exception also will be thrown if user tries to construct the object
    /// from incompatible implementation shared pointer.
    ///
    /// @throw user_error in case of pure implementation pointer or incompatible
    /// implementation pointer class.
    /// @since 0.4.0
    List_text(Widget_ptr wp);

    /// Assign implementation.
    /// @throw user_error in case of pure implementation pointer or incompatible
    /// implementation pointer class.
    /// @since 0.4.0
    List_text & operator=(Widget_ptr op);

    /// @}
    /// @name Row insertion
    /// @{

    /// Append text.
    /// @param str      text to be inserted.
    /// @param shrink   shrink text.
    /// @return         row number where @a str was inserted or @b INT_MIN if validation failed.
    int append(const ustring & str, bool shrink=false);

    /// Append text with alignment.
    /// @param str      text to be inserted.
    /// @param align    text alignment.
    /// @param shrink   shrink text.
    /// @return         row number where @a str was inserted or @b INT_MIN if validation failed.
    /// @since 0.6.0
    int append(const ustring & str, Align align, bool shrink=true);

    /// Append strings.
    /// @param sv       vector of strings to add.
    /// @param shrink   shrink text.
    /// @return         row number where first string was inserted or @b INT_MIN if validation failed.
    /// @since 0.6.0
    int append(const std::vector<ustring> & sv, bool shrink=false);

    /// Append strings with alignment.
    /// @param sv       vector of strings to add.
    /// @param align    text alignment.
    /// @param shrink   shrink text.
    /// @return         row number where first string was inserted or @b INT_MIN if validation failed.
    /// @since 0.6.0
    int append(const std::vector<ustring> & sv, Align align, bool shrink=false);

    /// Prepend text.
    /// @param str      text to be inserted.
    /// @param shrink   shrink text.
    /// @return         row number where @a str was inserted or @b INT_MIN if validation failed.
    int prepend(const ustring & str, bool shrink=false);

    /// Prepend text with alignment.
    /// @param str      text to be inserted.
    /// @param align    text alignment.
    /// @param shrink   shrink text.
    /// @return         row number where @a str was inserted or @b INT_MIN if validation failed.
    /// @since 0.6.0
    int prepend(const ustring & str, Align align, bool shrink=true);

    /// Prepend strings.
    /// @param sv       vector of strings to add.
    /// @param shrink   shrink text.
    /// @return         row number where first string was inserted or @b INT_MIN if validation failed.
    /// @since 0.6.0
    int prepend(const std::vector<ustring> & sv, bool shrink=false);

    /// Prepend strings with alignment.
    /// @param sv       vector of strings to add.
    /// @param align    text alignment.
    /// @param shrink   shrink text.
    /// @return         row number where first string was inserted or @b INT_MIN if validation failed.
    /// @since 0.6.0
    int prepend(const std::vector<ustring> & sv, Align align, bool shrink=false);

    /// Insert text at specified row.
    /// @param str      text to be inserted.
    /// @param row      desired row where text will be placed.
    /// @param shrink   shrink text.
    /// @return         real row number where @a str was inserted or @b INT_MIN if validation failed.
    int insert(const ustring & str, int row, bool shrink=false);

    /// Insert text at specified row with alignment.
    /// @param str      text to be inserted.
    /// @param row      desired row where text will be placed.
    /// @param align    text alignment.
    /// @param shrink   shrink text.
    /// @return         real row number where @a str was inserted or @b INT_MIN if validation failed.
    /// @since 0.6.0
    int insert(const ustring & str, int row, Align align, bool shrink=true);

    /// Insert strings at specified row.
    /// @param sv       vector of strings to add.
    /// @param row      desired row where strings will be placed.
    /// @param shrink   shrink text.
    /// @return         row number where first string was inserted or @b INT_MIN if validation failed.
    /// @since 0.6.0
    int insert(const std::vector<ustring> & sv, int row, bool shrink=false);

    /// Insert strings at specified row with alignment.
    /// @param sv       vector of strings to add.
    /// @param row      desired row where strings will be placed.
    /// @param align    text alignment.
    /// @param shrink   shrink text.
    /// @return         row number where first string was inserted or @b INT_MIN if validation failed.
    /// @since 0.6.0
    int insert(const std::vector<ustring> & sv, int row, Align align, bool shrink=false);

    /// Insert text above specified text.
    /// @param str      text to be inserted.
    /// @param other    insert before this.
    /// @param shrink   shrink text.
    /// @return         row number where @a str was inserted or @b INT_MIN if validation failed.
    int insert_before(const ustring & str, const ustring & other, bool shrink=false);

    /// Insert text above specified text with alignment.
    /// @param str      text to be inserted.
    /// @param other    insert before this.
    /// @param align    text alignment.
    /// @param shrink   shrink text.
    /// @return         row number where @a str was inserted or @b INT_MIN if validation failed.
    /// @since 0.6.0
    int insert_before(const ustring & str, const ustring & other, Align align, bool shrink=true);

    /// Insert strings above specified text.
    /// @param sv       vector of strings to add.
    /// @param other    insert before this.
    /// @param shrink   shrink text.
    /// @return         row number where first string was inserted or @b INT_MIN if validation failed.
    /// @since 0.6.0
    int insert_before(const std::vector<ustring> & sv, const ustring & other, bool shrink=false);

    /// Insert strings above specified text with alignment.
    /// @param sv       vector of strings to add.
    /// @param other    insert before this.
    /// @param align    text alignment.
    /// @param shrink   shrink text.
    /// @return         row number where first string was inserted or @b INT_MIN if validation failed.
    /// @since 0.6.0
    int insert_before(const std::vector<ustring> & sv, const ustring & other, Align align, bool shrink=false);

    /// Insert text below specified text.
    /// @param str      text to be inserted.
    /// @param other    insert after this.
    /// @param shrink   shrink text.
    /// @return         row number where @a str was inserted or @b INT_MIN if validation failed.
    int insert_after(const ustring & str, const ustring & other, bool shrink=false);

    /// Insert text below specified text with alignment.
    /// @param str      text to be inserted.
    /// @param other    insert after this.
    /// @param align    text alignment.
    /// @param shrink   shrink text.
    /// @return         row number where @a str was inserted or @b INT_MIN if validation failed.
    /// @since 0.6.0
    int insert_after(const ustring & str, const ustring & other, Align align, bool shrink=true);

    /// Insert strings below specified text.
    /// @param sv       vector of strings to add.
    /// @param other    insert before this.
    /// @param shrink   shrink text.
    /// @return         row number where first string was inserted or @b INT_MIN if validation failed.
    /// @since 0.6.0
    int insert_after(const std::vector<ustring> & sv, const ustring & other, bool shrink=false);

    /// Insert strings below specified text with alignment.
    /// @param sv       vector of strings to add.
    /// @param other    insert before this.
    /// @param align    text alignment.
    /// @param shrink   shrink text.
    /// @return         row number where first string was inserted or @b INT_MIN if validation failed.
    /// @since 0.6.0
    int insert_after(const std::vector<ustring> & sv, const ustring & other, Align align, bool shrink=false);

    /// @}
    /// @name Standalone Widget Insertion
    /// @{

    /// Insert text at specified row and column.
    /// @param row row where text will be placed.
    /// @param str text to be inserted.
    /// @param col column where text will be placed.
    /// @param shrink shrink text.
    /// @return @a row parameter.
    /// @throw user_error (bad row).
    /// This method can be used to insert text into columns other than 0th.
    /// @since 0.5.0
    int insert(int row, const ustring & str, int col, bool shrink=false);

    /// Insert text at specified row and column.
    /// @param row row where text will be placed.
    /// @param str text to be inserted.
    /// @param col column where text will be placed.
    /// @param align text alignment.
    /// @param shrink shrink text.
    /// @return @a row parameter.
    /// @throw user_error (bad row).
    /// This method can be used to insert text into columns other than 0th.
    /// @since 0.5.0
    int insert(int row, const ustring & str, int col, Align align, bool shrink=true);

    /// Insert widget at specified row and column.
    /// @param row row where widget will be placed.
    /// @param w widget to be inserted.
    /// @param col column where widget will be placed.
    /// @param shrink shrink widget.
    /// @return @a row parameter.
    /// @throw user_error (bad row).
    /// This method can be used to insert widget into columns other than 0th.
    /// @since 0.6.0
    int insert(int row, Widget & w, int col, bool shrink=false);

    /// Insert text at specified row and column.
    /// Insert widget at specified row and column.
    /// @param row row where widget will be placed.
    /// @param w widget to be inserted.
    /// @param col column where widget will be placed.
    /// @param align widget alignment.
    /// @param shrink shrink widget.
    /// @return @a row parameter.
    /// @throw user_error (bad row).
    /// This method can be used to insert widget into columns other than 0th.
    /// @since 0.6.0
    int insert(int row, Widget & w, int col, Align align, bool shrink=true);

    /// @}
    /// @name Row Removal
    /// @{

    /// Remove specified row.
    /// @param row      the row index.
    /// @return         removed rows count.
    std::size_t remove(int row);

    /// Remove row where specified widget found.
    /// @return         removed rows count.
    /// @since 0.6.0
    std::size_t remove(Widget & w);

    /// Remove rows having specified text.
    /// @param s        text to be removed.
    /// @param similar  use str_similar() to compare.
    /// @return         removed rows count.
    std::size_t remove(const ustring & s, bool similar=false);

    /// @}
    /// @name Selection
    /// @{

    /// Select specified row.
    /// @param row      row to be selected.
    /// @param fix      fix selection.
    /// @return @b INT_MIN on fail or row index.
    /// @since 0.6.0
    int select(int row, bool fix=false);

    /// Select specified text.
    /// @param str      text to be selected.
    /// @param fix      fix selection.
    /// @return @b INT_MIN on fail or row index.
    int select(const ustring & str, bool fix=false);

    /// Select similar text.
    /// @param str      similar text to be selected.
    /// @param fix      fix selection.
    /// @return @b INT_MIN on fail or row index.
    int select_similar(const ustring & str, bool fix=false);

    /// Get selected text as UTF-8 string.
    /// @return text at current row or empty string if there are no selection.
    ustring str() const;

    /// Get selected text as UTF-32 string.
    /// @return text at current row or empty string if there are no selection.
    /// @since 0.6.0
    std::u32string wstr() const;

    /// Get all selected strings as UTF-8.
    /// @since 0.6.0
    std::vector<ustring> strings() const;

    /// Get all selected strings as UTF-32.
    /// @since 0.6.0
    std::vector<std::u32string> wstrings() const;

    /// @{
    /// @name Search & Retrieving
    /// @{

    /// Get text at the given row as UTF-8 string.
    /// @overload
    /// @param row row number.
    /// @return text or empty string if not found.
    ustring str(int row) const;

    /// Get text at the given row as UTF-32 string.
    /// @overload
    /// @param row row number.
    /// @return text or empty string if not found.
    /// @since 0.6.0
    std::u32string wstr(int row) const;

    /// Find first row where specified text is.
    /// @param      s text to be found.
    /// @param      ymin    first row, inclusive
    /// @param      ymax    last row, non-inclusive
    /// @return     INT_MIN if not found or row number on success.
    int find(const ustring & s, int ymin=INT_MIN+1, int ymax=INT_MAX) const;

    /// Find first row where similar text is.
    /// @param      s       text to be found.
    /// @param      ymin    first row, inclusive
    /// @param      ymax    last row, non-inclusive
    /// @return     INT_MIN if not found or row number on success.
    /// @sa         str_similar()
    /// @since 0.6.0
    int similar(const ustring & s, int ymin=INT_MIN+1, int ymax=INT_MAX) const;

    /// Find first row where text like given is.
    /// @param      s       text to be found.
    /// @param      ymin    first row, inclusive
    /// @param      ymax    last row, non-inclusive
    /// @return     INT_MIN if not found or row number on success.
    /// @sa         str_like()
    /// @since 0.6.0
    int like(const ustring & s, int ymin=INT_MIN+1, int ymax=INT_MAX) const;

    /// Test if specified text already exists in the list.
    /// @param      s text to be tested.
    /// @param      similar if @b true, use str_similar() to compare, else compare byte-to-byte.
    bool contains(const ustring & s, bool similar=false) const;

    /// @}
    /// @name Text Editing
    /// %List_text has text editing facilities.
    /// When editing enabled for specified row or all row, than user can click
    /// on the desired row and start to edit it.
    /// @{

    /// Allow edit for all rows.
    /// @since 0.5.0
    void allow_edit();

    /// Disallow edit for all rows.
    /// @since 0.5.0
    void disallow_edit();

    /// Determines whether edit for all rows allowed.
    /// @since 0.5.0
    bool editable() const noexcept;

    /// @overload
    /// Allow edit for specified row.
    /// @since 0.5.0
    void allow_edit(int row);

    /// @overload
    /// Disallow edit for specified row.
    /// @since 0.5.0
    void disallow_edit(int row);

    /// @overload
    /// Determines whether edit for specified row allowed.
    /// @since 0.5.0
    bool editable(int row) const noexcept;

    /// @overload
    /// Allow edit for specified text.
    /// @since 0.5.0
    void allow_edit(const ustring & text);

    /// @overload
    /// Disallow edit for specified row.
    /// @since 0.5.0
    void disallow_edit(const ustring & text);

    /// @overload
    /// Determines whether edit for specified row allowed.
    /// @since 0.5.0
    bool editable(const ustring & text) const noexcept;

    /// @brief Force text editing.
    /// This call enables editing for specified row even if that editing was
    /// not enabled previously. After editing complete, the previous state of the
    /// editing grants returned.
    ///
    /// Before editing unselect() call performed. After editing complete, selection is clear,
    /// so user has to restore selection state manually.
    ///
    /// While editing, the below signals connected to the edit widget (which is of type Entry).
    /// After editing complete, the signals disconnected.
    ///
    /// The return value is an implementation pointer of the Entry class. In case of error
    /// the returning pointer is pure, so you must test it before using. The main possible
    /// reason to fail is impossibility to gain keyboard focus for editing widget.
    /// You can construct Entry from non-pure pointer and perform any actions on it.
    ///
    /// @param row which row needs to be edit.
    /// @return implementation pointer of editor widget (Entry) or pure pointer on fail.
    /// @since 0.5.0
    Widget_ptr edit(int row);

    /// Signal emitted when some editing text activated.
    /// Slot prototype:
    /// ~~~~~~~~~~~~~~
    /// void on_edit_activated(int row, const std::u32string & text);
    /// ~~~~~~~~~~~~~~
    /// @since 0.6.0
    signal<void(int, const std::u32string &)> & signal_activate_edit();

    /// Signal emitted when text editting cancelled.
    /// Slot prototype:
    /// ~~~~~~~~~~~~~~
    /// void on_text_cancel(int row);
    /// ~~~~~~~~~~~~~~
    /// @since 0.5.0
    signal<void(int)> & signal_cancel_edit();

    /// Get editing widget implementation pointer.
    /// @return When editing activated by calling edit() function, this call return
    /// exactly same pointer as edit() returned. If there are no editing
    /// action performed, the returned pointer is pure. The type of interface is Entry.
    /// @sa edit()
    /// @since 0.6.0
    Widget_ptr editor();

    /// Get editing widget implementation pointer.
    /// @return When editing activated by calling edit() function, this call return
    /// exactly same pointer as edit() returned. If there are no editing
    /// action performed, the returned pointer is pure. The type of interface is Entry.
    /// @sa edit()
    /// @since 0.6.0
    Widget_cptr editor() const;

    /// @}
    /// @name Controls
    /// @{

    /// Set horizontal text align.
    void text_align(Align align);

    /// Get horizontal text align.
    Align text_align() const noexcept;

    /// Set word wrap mode.
    /// @since 0.5.0
    void wrap(Wrap wrap);

    /// Get word wrap mode.
    /// @since 0.5.0
    Wrap wrap() const noexcept;

    /// @}
    /// @name Signals
    /// @{

    /// Signal emitted when text selected.
    /// Slot prototype:
    /// ~~~~~~~~~~~~~~
    /// void on_select_text(const std::u32string & text);
    /// ~~~~~~~~~~~~~~
    signal<void(const std::u32string &)> & signal_select_text();

    /// Signal emitted when some text activated.
    /// Slot prototype:
    /// ~~~~~~~~~~~~~~
    /// void on_activate_text(int row, const std::u32string & text);
    /// ~~~~~~~~~~~~~~
    signal<void(const std::u32string &)> & signal_activate_text();

    /// Signal emitted when some text changed.
    /// Slot prototype:
    /// ~~~~~~~~~~~~~~
    /// void on_text_changed(int row, const std::u32string & new_text);
    /// ~~~~~~~~~~~~~~
    /// @since 0.5.0
    signal<void(int, const std::u32string &)> & signal_text_changed();

    /// Signal emitted when some text removed.
    /// Slot prototype:
    /// ~~~~~~~~~~~~~~
    /// void on_remove_text(int row, const std::u32string & text);
    /// ~~~~~~~~~~~~~~
    signal<void(int, const std::u32string &)> & signal_remove_text();

    /// Signal emitted while text validation occurs.
    /// Slot prototype:
    /// ~~~~~~~~~~~~~~
    /// bool on_validate(const std::u32string & text);
    /// ~~~~~~~~~~~~~~
    /// @sa Entry::signal_validate()
    /// @since 0.5.0
    signal_all<const std::u32string &> & signal_validate();

    /// Signal emitted while editing text approval occurs.
    /// Slot prototype:
    /// ~~~~~~~~~~~~~~
    /// bool on_approve(const std::u32string & text);
    /// ~~~~~~~~~~~~~~
    /// @sa Entry::signal_approve()
    /// @since 0.6.0
    signal_all<const std::u32string &> & signal_approve();

    /// @}
};

} // namespace tau

#endif // __TAU_LIST_HH__

// ----------------------------------------------------------------------------
// SPDX-License-Identifier: BSD-2-Clause
// Copyright © 2014-2024 Konstantin Shmelkov <mrcashe@gmail.com>.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
// THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
// PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
// CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
// EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
// PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
// OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
// WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
// OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
// EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// ----------------------------------------------------------------------------

/// @file label.hh The Label class.

#ifndef __TAU_LABEL_HH__
#define __TAU_LABEL_HH__

#include <tau/enums.hh>
#include <tau/ustring.hh>
#include <tau/widget.hh>

namespace tau {

/// %Text label displaying widget.
///
/// @note This class is a wrapper around its implementation shared pointer.
///
/// @ingroup widget_group
/// @ingroup text_group
class __TAUEXPORT__ Label: public Widget {
public:

    /// @name Constructors & Operators
    /// @{

    /// Default constructor.
    Label();

    /// Constructor with text alignment.
    explicit Label(Align halign, Align valign=Align::CENTER);

    /// Constructor with text and text alignment.
    explicit Label(const ustring & s, Align halign=Align::CENTER, Align valign=Align::CENTER);

    /// Constructor with text and text alignment.
    /// @since 0.5.0
    explicit Label(const std::u32string & ws, Align halign=Align::CENTER, Align valign=Align::CENTER);

    /// Constructor with buffer and text alignment.
    explicit Label(Buffer buf, Align halign=Align::CENTER, Align valign=Align::CENTER);

    /// Copy constructor.
    ///
    /// @note This class is a wrapper around its implementation shared pointer,
    /// so copying it just increasing implementation pointer use count, but isn't
    /// really copies the object. The underlying implementation is not copyable.
    Label(const Label & other);

    /// Copy operator.
    ///
    /// @note This class is a wrapper around its implementation shared pointer,
    /// so copying it just increasing implementation pointer use count, but isn't
    /// really copies the object. The underlying implementation is not copyable.
    Label & operator=(const Label & other);

    /// Move constructor.
    ///
    /// @note This class is a wrapper around its implementation shared pointer,
    /// so copying it just increasing implementation pointer use count, but isn't
    /// really copies the object. The underlying implementation is not copyable.
    /// @note This constructor does not reset @c other implementation pointer.
    Label(Label && other);

    /// Move operator.
    ///
    /// @note This class is a wrapper around its implementation shared pointer,
    /// so copying it just increasing implementation pointer use count, but isn't
    /// really copies the object. The underlying implementation is not copyable.
    /// @note This operator does not reset @c other implementation pointer.
    Label & operator=(Label && other);

    /// Constructor with implementation pointer.
    ///
    /// @warning Unlike some other classes (Painter as an example), the whole
    /// @ref widget_stack "widget stack" is unable to run with pure implementation
    /// pointer, so attempting to construct widget from a pure (@b nullptr) pointer
    /// will cause throwing an user_error exception!
    /// That exception also will be thrown if user tries to construct the object
    /// from incompatible implementation shared pointer.
    ///
    /// @throw user_error in case of pure implementation pointer or incompatible
    /// implementation pointer class.
    /// @since 0.4.0
    Label(Widget_ptr wp);

    /// Assign implementation.
    ///
    /// @throw user_error in case of pure implementation pointer or incompatible
    /// implementation pointer class.
    /// @since 0.4.0
    Label & operator=(Widget_ptr wp);

    /// @}
    /// @name Buffer Operations
    /// @{

    /// Set buffer.
    /// @since 0.5.0
    void set_buffer(Buffer buf);

    /// Get buffer.
    Buffer buffer();

    /// Get buffer.
    const Buffer buffer() const;

    /// Get iterator with specified position.
    Buffer_citer iter(std::size_t row, std::size_t col) const;

    /// Get iterator for specified location.
    /// @since 0.6.0
    Buffer_citer iter(const Point & pt) const;

    /// @}
    /// @name Text Manipulation
    /// @{

    /// Assign text from UTF-8 string.
    void assign(const ustring & s);

    /// Assign text from UTF-32 string.
    /// @since 0.5.0
    void assign(const std::u32string & ws);

    /// Test if empty.
    bool empty() const noexcept;

    /// Clear text.
    void clear();

    /// Get row count.
    std::size_t rows() const noexcept;

    /// Get text as UTF-8.
    ustring str() const;

    /// Get text as UTF-32.
    /// @since 0.5.0
    std::u32string wstr() const;

    /// @}
    /// @name Geometry
    /// @{

    /// Calculate text size in pixels.
    /// Calculate given text size using current font settings. Unlike Painter::text_size(), this
    /// method returns text height not for given string, but for entire font currently set and consists
    /// of Font::ascent() and Font::descent() components.
    /// @param s the string to be measured.
    /// @return given text size in pixels.
    /// @note This method returns an empty Size if widget is not inserted into widget hierarchy.
    Size text_size(const ustring & s);

    /// Calculate text size in pixels.
    /// Calculate given text size using current font settings. Unlike Painter::text_size(), this
    /// method returns text height not for given string, but for entire font currently set and consists
    /// of Font::ascent() and Font::descent() components.
    /// @param s the string to be measured.
    /// @return given text size in pixels.
    /// @note This method returns an empty Size if widget is not inserted into widget hierarchy.
    Size text_size(const std::u32string & s);

    /// Get X coordinate of specified column.
    /// @param row the row index, starting with 0.
    /// @param col the column (character) index within row, starting with 0.
    /// @return the X column (character) coordinate.
    /// If there are no such column, the return value is 0.
    int x_at_col(std::size_t row, std::size_t col) const noexcept;

    /// Get column index from known row index and X coordinate.
    /// @param row the row index, starting with 0.
    /// @param x the X coordinate, starting with 0.
    /// @return the column (character) index.
    /// If specified row does not exist, the return value is 0.
    /// If specified X coordinate is out of row coordinate range, the return value is 0.
    std::size_t col_at_x(std::size_t row, int x) const noexcept;

    /// Get row index from Y coordinate.
    /// @param y the Y coordinate.
    /// @return the row index, starting with 0.
    /// If specified Y coordinate is above used space, the last row returned.
    /// If text buffer is empty or specified Y coordinate negative, the return value is 0.
    std::size_t row_at_y(int y) const noexcept;

    /// Get baseline coordinate of the specified row.
    /// @param row the row index, starting with 0.
    /// @return the baseline Y coordinate, starting with 0.
    /// If specified row index is out of range, the returning value is 0.
    /// If specified row index is valid and row exists, the returning value can not be 0.
    int baseline(std::size_t row) const noexcept;

    /// Get row bounds.
    /// @param row the row index, starting with 0.
    Rect row_bounds(std::size_t row) const noexcept;

    /// @}
    /// @name Controls
    /// @{

    /// Set extra space between text lines.
    /// @param spc extra space in pixels.
    /// @sa spacing.
    void set_spacing(unsigned spc);

    /// Get spacing.
    /// @sa set_spacing.
    unsigned spacing() const noexcept;

    /// Set text alignment.
    /// @param xalign horizontal text alignment.
    /// @param yalign vertical text alignment.
    void text_align(Align xalign, Align yalign=Align::CENTER);

    /// Get text alignment.
    /// @return pair where @a .first member is for horizontal alignment and @a .second member is for vertical alignment.
    std::pair<Align, Align> text_align() const;

    /// Get word wrap mode.
    Wrap wrap() const noexcept;

    /// Set word wrap mode.
    void wrap(Wrap wrap);

    /// @}
};

} // namespace tau

#endif // __TAU_LABEL_HH__

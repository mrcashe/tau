// ----------------------------------------------------------------------------
// SPDX-License-Identifier: BSD-2-Clause
// Copyright © 2014-2024 Konstantin Shmelkov <mrcashe@gmail.com>.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
// THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
// PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
// CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
// EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
// PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
// OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
// WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
// OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
// EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// ----------------------------------------------------------------------------

/// @file button.hh The Button_base, Button and Toggle classes.

#ifndef __TAU_BUTTON_HH__
#define __TAU_BUTTON_HH__

#include <tau/action.hh>
#include <tau/widget.hh>

namespace tau {

/// %Button base.
/// @ingroup widget_group
class __TAUEXPORT__ Button_base: public Widget {
public:

    /// @name Icon Management
    /// @{

    /// Set label.
    void set_label(const ustring & label);

    /// Set image widget.
    /// @param img an image widget.
    void set_image(Widget & img);

    /// Set icon.
    /// @param icon_name the icon name.
    /// @param icon_size the icon size in pixels or one of Icon class enumerator.
    void set_icon(const ustring & icon_name, int icon_size);

    /// Get icon size in pixels.
    /// @return icon size in pixels or -1, if no icon assigned.
    /// @since 0.5.0
    int icon_size() const noexcept;

    /// Resize icon if it already assigned.
    /// @param icon_size the icon size in pixels or one of Icon class enumerator.
    /// If button has no assigned icon, this method do nothing.
    /// @since 0.5.0
    void resize_icon(int icon_size);

    /// @}
    /// @name Shape Control
    /// @{

    /// Show button relief.
    void show_relief();

    /// Hide button relief.
    void hide_relief();

    /// Determines is relief visible.
    bool relief_visible() const noexcept;

    /// Set border radius.
    /// @since 0.7.0
    void set_border_top_left_radius(unsigned radius);

    /// Set border radius.
    /// @since 0.7.0
    void set_border_top_right_radius(unsigned radius);

    /// Set border radius.
    /// @since 0.7.0
    void set_border_bottom_right_radius(unsigned radius);

    /// Set border radius.
    /// @since 0.7.0
    void set_border_bottom_left_radius(unsigned radius);

    /// Set border radius.
    /// @since 0.7.0
    void set_border_radius(unsigned radius);

    /// Set border radius.
    /// @since 0.7.0
    void set_border_radius(unsigned top_left, unsigned top_right, unsigned bottom_right, unsigned bottom_left);

    /// Set top left border radius to theme default.
    /// @since 0.7.0
    void unset_border_top_left_radius();

    /// Set top right border radius to theme default.
    /// @since 0.7.0
    void unset_border_top_right_radius();

    /// Set bottom right border radius to theme default.
    /// @since 0.7.0
    void unset_border_bottom_right_radius();

    /// Set bottom left border radius to theme default.
    /// @since 0.7.0
    void unset_border_bottom_left_radius();

    /// Set border radius to theme default.
    /// @since 0.7.0
    void unset_border_radius();

    /// Get border top left radius.
    /// @since 0.7.0
    unsigned border_top_left_radius() const noexcept;

    /// Get border top right radius.
    /// @since 0.7.0
    unsigned border_top_right_radius() const noexcept;

    /// Get border bottom right radius.
    /// @since 0.7.0
    unsigned border_bottom_right_radius() const noexcept;

    /// Get border bottom left radius.
    /// @since 0.7.0
    unsigned border_bottom_left_radius() const noexcept;

    /// @}

protected:

    /// @private
    Button_base(Widget_ptr op);

    /// @private
    Button_base & operator=(Widget_ptr op);
};

/// Push button.
///
/// @note This class is a wrapper around its implementation shared pointer.
///
/// @ingroup widget_group
class __TAUEXPORT__ Button: public Button_base {
public:

    /// @name Constructors & Operators
    /// @{

    /// Default constructor.
    /// Constructs basic %Button.
    Button();

    /// Constructor with border radius.
    /// @param radius           the border radius, in pixels.
    /// @since 0.7.0
    explicit Button(unsigned radius);

    /// Constructor with border radius.
    /// @param rtop_left        top left border radius, in pixels.
    /// @param rtop_right       top right border radius, in pixels.
    /// @param rbottom_right    bottom right border radius, in pixels.
    /// @param rbottom_left     bottom left border radius, in pixels.
    /// @since 0.7.0
    explicit Button(unsigned rtop_left, unsigned rtop_right, unsigned rbottom_right, unsigned rbottom_left);

    /// Constructor with label.
    /// @param label            the label.
    explicit Button(const ustring & label);

    /// Constructor with label and border radius.
    /// @param label            the label.
    /// @param radius           the border radius, in pixels.
    /// @since 0.7.0
    explicit Button(const ustring & label, unsigned radius);

    /// Constructor with label and border radius.
    /// @param label            the label.
    /// @param rtop_left        top left border radius, in pixels.
    /// @param rtop_right       top right border radius, in pixels.
    /// @param rbottom_right    bottom right border radius, in pixels.
    /// @param rbottom_left     bottom left border radius, in pixels.
    /// @since 0.7.0
    explicit Button(const ustring & label, unsigned rtop_left, unsigned rtop_right, unsigned rbottom_right, unsigned rbottom_left);

    /// Constructor with image.
    /// @param img              an image widget.
    explicit Button(Widget & img);

    /// Constructor with image and border radius.
    /// @param img              an image widget.
    /// @param radius           the border radius, in pixels.
    /// @since 0.7.0
    explicit Button(Widget & img, unsigned radius);

    /// Constructor with image and border radius.
    /// @param img              an image widget.
    /// @param rtop_left        top left border radius, in pixels.
    /// @param rtop_right       top right border radius, in pixels.
    /// @param rbottom_right    bottom right border radius, in pixels.
    /// @param rbottom_left     bottom left border radius, in pixels.
    /// @since 0.7.0
    explicit Button(Widget & img, unsigned rtop_left, unsigned rtop_right, unsigned rbottom_right, unsigned rbottom_left);

    /// Constructor with image and label.
    /// @param img              an image widget.
    /// @param label            the label.
    explicit Button(Widget & img, const ustring & label);

    /// Constructor with image, label and border radius.
    /// @param img              an image widget.
    /// @param label            the label.
    /// @param radius           the border radius, in pixels.
    /// @since 0.7.0
    explicit Button(Widget & img, const ustring & label, unsigned radius);

    /// Constructor with image, label and border radius.
    /// @param img              an image widget.
    /// @param label            the label.
    /// @param rtop_left        top left border radius, in pixels.
    /// @param rtop_right       top right border radius, in pixels.
    /// @param rbottom_right    bottom right border radius, in pixels.
    /// @param rbottom_left     bottom left border radius, in pixels.
    /// @since 0.7.0
    explicit Button(Widget & img, const ustring & label, unsigned rtop_left, unsigned rtop_right, unsigned rbottom_right, unsigned rbottom_left);

    /// Constructor with icon.
    /// @param icon_size        the icon size in pixels or one of Icon class enumerator.
    /// @param icon_name        the icon name.
    explicit Button(int icon_size, const ustring & icon_name);

    /// Constructor with icon and border radius.
    /// @param icon_size        the icon size in pixels or one of Icon class enumerator.
    /// @param icon_name        the icon name.
    /// @param radius           the border radius, in pixels.
    /// @since 0.7.0
    explicit Button(int icon_size, const ustring & icon_name, unsigned radius);

    /// Constructor with icon and border radius.
    /// @param icon_size        the icon size in pixels or one of Icon class enumerator.
    /// @param icon_name        the icon name.
    /// @param rtop_left        top left border radius, in pixels.
    /// @param rtop_right       top right border radius, in pixels.
    /// @param rbottom_right    bottom right border radius, in pixels.
    /// @param rbottom_left     bottom left border radius, in pixels.
    /// @since 0.7.0
    explicit Button(int icon_size, const ustring & icon_name, unsigned rtop_left, unsigned rtop_right, unsigned rbottom_right, unsigned rbottom_left);

    /// Constructor with label and icon.
    /// @param label            the label.
    /// @param icon_size        the icon size in pixels or one of Icon class enumerator.
    /// @param icon_name        the icon name.
    explicit Button(const ustring & label, int icon_size, const ustring & icon_name);

    /// Constructor with label, icon and border radius.
    /// @param label            the label.
    /// @param icon_size        the icon size in pixels or one of Icon class enumerator.
    /// @param icon_name        the icon name.
    /// @param radius           the border radius, in pixels.
    /// @since 0.7.0
    explicit Button(const ustring & label, int icon_size, const ustring & icon_name, unsigned radius);

    /// Constructor with label, icon and border radius.
    /// @param label            the label.
    /// @param icon_size        the icon size in pixels or one of Icon class enumerator.
    /// @param icon_name        the icon name.
    /// @param rtop_left        top left border radius, in pixels.
    /// @param rtop_right       top right border radius, in pixels.
    /// @param rbottom_right    bottom right border radius, in pixels.
    /// @param rbottom_left     bottom left border radius, in pixels.
    /// @since 0.7.0
    explicit Button(const ustring & label, int icon_size, const ustring & icon_name, unsigned rtop_left, unsigned rtop_right, unsigned rbottom_right, unsigned rbottom_left);

    /// Constructor with Action.
    /// Suitable for tool buttons. Uses @b Icon::DEFAULT size.
    /// @param action           the action to be used.
    /// @param items            action items to be used, see @ref action_flags_group.
    explicit Button(Action & action, Action::Flags items=Action::ALL);

    /// Constructor with Action and border radius.
    /// Suitable for tool buttons. Uses @b Icon::DEFAULT size.
    /// @param action           the action to be used.
    /// @param radius           the border radius, in pixels.
    /// @param items            action items to be used, see @ref action_flags_group.
    /// @since 0.7.0
    explicit Button(Action & action, unsigned radius, Action::Flags items=Action::ALL);

    /// Constructor with Action and border radius.
    /// Suitable for tool buttons. Uses @b Icon::DEFAULT size.
    /// @param action           the action to be used.
    /// @param rtop_left        top left border radius, in pixels.
    /// @param rtop_right       top right border radius, in pixels.
    /// @param rbottom_right    bottom right border radius, in pixels.
    /// @param rbottom_left     bottom left border radius, in pixels.
    /// @param items            action items to be used, see @ref action_flags_group.
    /// @since 0.7.0
    explicit Button(Action & action, unsigned rtop_left, unsigned rtop_right, unsigned rbottom_right, unsigned rbottom_left, Action::Flags items=Action::ALL);

    /// Constructor with Action and icon size.
    /// @param action           the action to be used.
    /// @param icon_size        the icon size in pixels or one of Icon class enumerator.
    /// @param items            action items to be used, see @ref action_flags_group.
    explicit Button(Action & action, int icon_size, Action::Flags items=Action::ALL);

    /// Constructor with Action, icon size and border radius.
    /// @param action           the action to be used.
    /// @param icon_size        the icon size in pixels or one of Icon class enumerator.
    /// @param radius           the border radius, in pixels.
    /// @param items            action items to be used, see @ref action_flags_group.
    /// @since 0.7.0
    explicit Button(Action & action, int icon_size, unsigned radius, Action::Flags items=Action::ALL);

    /// Constructor with Action, icon size and border radius.
    /// @param action           the action to be used.
    /// @param icon_size        the icon size in pixels or one of Icon class enumerator.
    /// @param rtop_left        top left border radius, in pixels.
    /// @param rtop_right       top right border radius, in pixels.
    /// @param rbottom_right    bottom right border radius, in pixels.
    /// @param rbottom_left     bottom left border radius, in pixels.
    /// @param items            action items to be used, see @ref action_flags_group.
    /// @since 0.7.0
    explicit Button(Action & action, int icon_size, unsigned rtop_left, unsigned rtop_right, unsigned rbottom_right, unsigned rbottom_left, Action::Flags items=Action::ALL);

    /// Constructor with Master_action.
    /// Suitable for tool buttons. Uses @b Icon::DEFAULT size.
    /// @param action           the master action to be used.
    /// @param items            action items to be used, see @ref action_flags_group.
    /// @since 0.7.0
    explicit Button(Master_action & action, Action::Flags items=Action::ALL);

    /// Constructor with Master_action and border radius.
    /// Suitable for tool buttons. Uses @b Icon::DEFAULT size.
    /// @param action           the master action to be used.
    /// @param radius           the border radius, in pixels.
    /// @param items            action items to be used, see @ref action_flags_group.
    /// @since 0.7.0
    explicit Button(Master_action & action, unsigned radius, Action::Flags items=Action::ALL);

    /// Constructor with Master_action and border radius.
    /// Suitable for tool buttons. Uses @b Icon::DEFAULT size.
    /// @param action           the master action to be used.
    /// @param rtop_left        top left border radius, in pixels.
    /// @param rtop_right       top right border radius, in pixels.
    /// @param rbottom_right    bottom right border radius, in pixels.
    /// @param rbottom_left     bottom left border radius, in pixels.
    /// @param items            action items to be used, see @ref action_flags_group.
    /// @since 0.7.0
    explicit Button(Master_action & action, unsigned rtop_left, unsigned rtop_right, unsigned rbottom_right, unsigned rbottom_left, Action::Flags items=Action::ALL);

    /// Constructor with Master_action and icon size.
    /// @param action           the master action to be used.
    /// @param icon_size        the icon size in pixels or one of Icon class enumerator.
    /// @param items            action items to be used, see @ref action_flags_group.
    /// @since 0.7.0
    explicit Button(Master_action & action, int icon_size, Action::Flags items=Action::ALL);

    /// Constructor with Master_action, icon size and border radius.
    /// @param action           the master action to be used.
    /// @param icon_size        the icon size in pixels or one of Icon class enumerator.
    /// @param radius           the border radius, in pixels.
    /// @param items            action items to be used, see @ref action_flags_group.
    /// @since 0.7.0
    explicit Button(Master_action & action, int icon_size, unsigned radius, Action::Flags items=Action::ALL);

    /// Constructor with Master_action, icon size and border radius.
    /// @param action           the master action to be used.
    /// @param icon_size        the icon size in pixels or one of Icon class enumerator.
    /// @param rtop_left        top left border radius, in pixels.
    /// @param rtop_right       top right border radius, in pixels.
    /// @param rbottom_right    bottom right border radius, in pixels.
    /// @param rbottom_left     bottom left border radius, in pixels.
    /// @param items            action items to be used, see @ref action_flags_group.
    /// @since 0.7.0
    explicit Button(Master_action & action, int icon_size, unsigned rtop_left, unsigned rtop_right, unsigned rbottom_right, unsigned rbottom_left, Action::Flags items=Action::ALL);

    /// Copy constructor.
    /// @note This class is a wrapper around its implementation shared pointer,
    /// so copying it just increasing implementation pointer use count, but isn't
    /// really copies the object. The underlying implementation is not copyable.
    Button(const Button & other);

    /// Copy operator.
    /// @note This class is a wrapper around its implementation shared pointer,
    /// so copying it just increasing implementation pointer use count, but isn't
    /// really copies the object. The underlying implementation is not copyable.
    Button & operator=(const Button & other);

    /// Move constructor.
    /// @note This class is a wrapper around its implementation shared pointer,
    /// so copying it just increasing implementation pointer use count, but isn't
    /// really copies the object. The underlying implementation is not copyable.
    /// @note This constructor does not reset @c other implementation pointer.
    Button(Button && other);

    /// Move operator.
    /// @note This class is a wrapper around its implementation shared pointer,
    /// so copying it just increasing implementation pointer use count, but isn't
    /// really copies the object. The underlying implementation is not copyable.
    /// @note This operator does not reset @c other implementation pointer.
    Button & operator=(Button && other);

    /// Constructor with implementation pointer.
    /// @warning Unlike some other classes (Painter as an example), the whole
    /// @ref widget_stack "widget stack" is unable to run with pure implementation
    /// pointer, so attempting to construct widget from a pure (@b nullptr) pointer
    /// will cause throwing an user_error exception!
    /// That exception also will be thrown if user tries to construct the object
    /// from incompatible implementation shared pointer.
    ///
    /// @throw user_error in case of pure implementation pointer or incompatible
    /// implementation pointer class.
    /// @since 0.4.0
    Button(Widget_ptr wp);

    /// Assign implementation.
    /// @throw user_error in case of pure implementation pointer or incompatible
    /// implementation pointer class.
    /// @since 0.4.0
    Button & operator=(Widget_ptr wp);

    /// @}
    /// @name Controls
    /// @{

    /// Enable automatic repeat of mouse click.
    /// Disabled by default.
    void enable_repeat() noexcept;

    /// Disable automatic repeat of mouse click.
    void disable_repeat() noexcept;

    /// Determines is automatic repeat enabled.
    bool repeat_enabled() const noexcept;

    /// Set repeat period.
    /// @param delay_ms period in milliseconds before first repeat.
    /// @param interval_ms period in milliseconds starting second repeat. If 0, delay_ms used.
    void set_repeat_delay(unsigned delay_ms, unsigned interval_ms=0);

    /// Get repeat delay.
    unsigned repeat_delay() const noexcept;

    /// Get repeat interval.
    unsigned repeat_interval() const noexcept;

    /// Set Action.
    /// @since 0.7.0
    void set_action(Action & action, Action::Flags items=Action::ALL);

    /// Set Action.
    /// @since 0.7.0
    void set_action(Action & action, int icon_size, Action::Flags items=Action::ALL);

    /// Select Action.
    /// @since 0.7.0
    void select(Action & action);

    /// @}
    /// @name Execution
    /// @{

    /// Click button.
    /// Performs action same when button pressed by mouse click or keyboard input.
    /// @since 0.6.0
    void click();

    /// Connect slot to internal signal.
    /// @since 0.6.0
    connection connect(const slot<void()> & slot, bool prepend=false);

    /// Connect slot to internal signal.
    /// @since 0.6.0
    connection connect(slot<void()> && slot, bool prepend=false);

    /// @}
};

/// %Toggle button.
///
/// @note This class is a wrapper around its implementation shared pointer.
///
/// @ingroup widget_group
class __TAUEXPORT__ Toggle: public Button_base {
public:

    /// @name Constructors & Operators
    /// @{

    /// Default constructor.
    /// Constructs basic %Toggle.
    Toggle();

    /// Constructor with border radius.
    /// @param radius           border radius, in pixels.
    /// @since 0.7.0
    explicit Toggle(unsigned radius);

    /// Constructor with border radius.
    /// @param rtop_left        top left border radius, in pixels.
    /// @param rtop_right       top right border radius, in pixels.
    /// @param rbottom_right    bottom right border radius, in pixels.
    /// @param rbottom_left     bottom left border radius, in pixels.
    /// @since 0.7.0
    explicit Toggle(unsigned rtop_left, unsigned rtop_right, unsigned rbottom_right, unsigned rbottom_left);

    /// Constructor with label.
    /// @param label            the label.
    explicit Toggle(const ustring & label);

    /// Constructor with label and border radius.
    /// @param label            the label.
    /// @param radius           border radius, in pixels.
    /// @since 0.7.0
    explicit Toggle(const ustring & label, unsigned radius);

    /// Constructor with label and border radius.
    /// @param label            the label.
    /// @param rtop_left        top left border radius, in pixels.
    /// @param rtop_right       top right border radius, in pixels.
    /// @param rbottom_right    bottom right border radius, in pixels.
    /// @param rbottom_left     bottom left border radius, in pixels.
    /// @since 0.7.0
    explicit Toggle(const ustring & label, unsigned rtop_left, unsigned rtop_right, unsigned rbottom_right, unsigned rbottom_left);

    /// Constructor with image.
    /// @param img              an image widget.
    explicit Toggle(Widget & img);

    /// Constructor with image and border radius.
    /// @param img              an image widget.
    /// @param radius           border radius, in pixels.
    /// @since 0.7.0
    explicit Toggle(Widget & img, unsigned radius);

    /// Constructor with image and border radius.
    /// @param img              an image widget.
    /// @param rtop_left        top left border radius, in pixels.
    /// @param rtop_right       top right border radius, in pixels.
    /// @param rbottom_right    bottom right border radius, in pixels.
    /// @param rbottom_left     bottom left border radius, in pixels.
    /// @since 0.7.0
    explicit Toggle(Widget & img, unsigned rtop_left, unsigned rtop_right, unsigned rbottom_right, unsigned rbottom_left);

    /// Constructor with image and label.
    /// @param img              an image widget.
    /// @param label            the label.
    explicit Toggle(Widget & img, const ustring & label);

    /// Constructor with image, label and border radius.
    /// @param img              an image widget.
    /// @param label            the label.
    /// @param radius           border radius, in pixels.
    /// @since 0.7.0
    explicit Toggle(Widget & img, const ustring & label, unsigned radius);

    /// Constructor with image, label and border radius.
    /// @param img              an image widget.
    /// @param label            the label.
    /// @param rtop_left        top left border radius, in pixels.
    /// @param rtop_right       top right border radius, in pixels.
    /// @param rbottom_right    bottom right border radius, in pixels.
    /// @param rbottom_left     bottom left border radius, in pixels.
    /// @since 0.7.0
    explicit Toggle(Widget & img, const ustring & label, unsigned rtop_left, unsigned rtop_right, unsigned rbottom_right, unsigned rbottom_left);

    /// Constructor with icon.
    /// @param icon_size        the icon size in pixels or one of Icon class enumerator.
    /// @param icon_name        the icon name.
    explicit Toggle(int icon_size, const ustring & icon_name);

    /// Constructor with icon and border radius.
    /// @param icon_size        the icon size in pixels or one of Icon class enumerator.
    /// @param icon_name        the icon name.
    /// @param radius           border radius, in pixels.
    /// @since 0.7.0
    explicit Toggle(int icon_size, const ustring & icon_name, unsigned radius);

    /// Constructor with icon and border radius.
    /// @param icon_size        the icon size in pixels or one of Icon class enumerator.
    /// @param icon_name        the icon name.
    /// @param rtop_left        top left border radius, in pixels.
    /// @param rtop_right       top right border radius, in pixels.
    /// @param rbottom_right    bottom right border radius, in pixels.
    /// @param rbottom_left     bottom left border radius, in pixels.
    /// @since 0.7.0
    explicit Toggle(int icon_size, const ustring & icon_name, unsigned rtop_left, unsigned rtop_right, unsigned rbottom_right, unsigned rbottom_left);

    /// Constructor with label and icon.
    /// @param label            the label.
    /// @param icon_size        the icon size in pixels or one of Icon class enumerator.
    /// @param icon_name        the icon name.
    explicit Toggle(const ustring & label, int icon_size, const ustring & icon_name);

    /// Constructor with label, icon and border radius.
    /// @param label            the label.
    /// @param icon_size        the icon size in pixels or one of Icon class enumerator.
    /// @param icon_name        the icon name.
    /// @param radius           border radius, in pixels.
    /// @since 0.7.0
    explicit Toggle(const ustring & label, int icon_size, const ustring & icon_name, unsigned radius);

    /// Constructor with label, icon and border radius.
    /// @param label            the label.
    /// @param icon_size        the icon size in pixels or one of Icon class enumerator.
    /// @param icon_name        the icon name.
    /// @param rtop_left        top left border radius, in pixels.
    /// @param rtop_right       top right border radius, in pixels.
    /// @param rbottom_right    bottom right border radius, in pixels.
    /// @param rbottom_left     bottom left border radius, in pixels.
    /// @since 0.7.0
    explicit Toggle(const ustring & label, int icon_size, const ustring & icon_name, unsigned rtop_left, unsigned rtop_right, unsigned rbottom_right, unsigned rbottom_left);

    /// Constructor with Toggle_action.
    /// Suitable for tool buttons. Uses @b Icon::DEFAULT size.
    /// @param action           the action to be used.
    /// @param items            action items to be used, see @ref action_flags_group.
    explicit Toggle(Toggle_action & action, Action::Flags items=Action::ALL);

    /// Constructor with Toggle_action and border radius.
    /// Suitable for tool buttons. Uses @b Icon::DEFAULT size.
    /// @param action           the action to be used.
    /// @param radius           border radius, in pixels.
    /// @param items            action items to be used, see @ref action_flags_group.
    /// @since 0.7.0
    explicit Toggle(Toggle_action & action, unsigned radius, Action::Flags items=Action::ALL);

    /// Constructor with Toggle_action and border radius.
    /// Suitable for tool buttons. Uses @b Icon::DEFAULT size.
    /// @param action           the action to be used.
    /// @param rtop_left        top left border radius, in pixels.
    /// @param rtop_right       top right border radius, in pixels.
    /// @param rbottom_right    bottom right border radius, in pixels.
    /// @param rbottom_left     bottom left border radius, in pixels.
    /// @param items            action items to be used, see @ref action_flags_group.
    /// @since 0.7.0
    explicit Toggle(Toggle_action & action, unsigned rtop_left, unsigned rtop_right, unsigned rbottom_right, unsigned rbottom_left, Action::Flags items=Action::ALL);

    /// Constructor with Toggle_action and icon size.
    /// @param icon_size        the icon size in pixels or one of Icon class enumerator.
    /// @param action           the action to be used.
    /// @param items            action items to be used, see @ref action_flags_group.
    explicit Toggle(Toggle_action & action, int icon_size, Action::Flags items=Action::ALL);

    /// Constructor with Toggle_action, icon size and border radius.
    /// @param icon_size        the icon size in pixels or one of Icon class enumerator.
    /// @param action           the action to be used.
    /// @param radius           border radius, in pixels.
    /// @param items            action items to be used, see @ref action_flags_group.
    /// @since 0.7.0
    explicit Toggle(Toggle_action & action, int icon_size, unsigned radius, Action::Flags items=Action::ALL);

    /// Constructor with Toggle_action, icon size and border radius.
    /// @param icon_size        the icon size in pixels or one of Icon class enumerator.
    /// @param action           the action to be used.
    /// @param rtop_left        top left border radius, in pixels.
    /// @param rtop_right       top right border radius, in pixels.
    /// @param rbottom_right    bottom right border radius, in pixels.
    /// @param rbottom_left     bottom left border radius, in pixels.
    /// @param items            action items to be used, see @ref action_flags_group.
    /// @since 0.7.0
    explicit Toggle(Toggle_action & action, int icon_size, unsigned rtop_left, unsigned rtop_right, unsigned rbottom_right, unsigned rbottom_left, Action::Flags items=Action::ALL);

    /// Constructor with Master_action.
    /// Suitable for tool buttons. Uses @b Icon::DEFAULT size.
    /// @param action           the action to be used.
    /// @param items            action items to be used, see @ref action_flags_group.
    /// @since 0.7.0
    explicit Toggle(Master_action & action, Action::Flags items=Action::ALL);

    /// Constructor with Master_action and border radius.
    /// Suitable for tool buttons. Uses @b Icon::DEFAULT size.
    /// @param action           the action to be used.
    /// @param radius           border radius, in pixels.
    /// @param items            action items to be used, see @ref action_flags_group.
    /// @since 0.7.0
    explicit Toggle(Master_action & action, unsigned radius, Action::Flags items=Action::ALL);

    /// Constructor with Master_action and border radius.
    /// Suitable for tool buttons. Uses @b Icon::DEFAULT size.
    /// @param action           the action to be used.
    /// @param rtop_left        top left border radius, in pixels.
    /// @param rtop_right       top right border radius, in pixels.
    /// @param rbottom_right    bottom right border radius, in pixels.
    /// @param rbottom_left     bottom left border radius, in pixels.
    /// @param items            action items to be used, see @ref action_flags_group.
    /// @since 0.7.0
    explicit Toggle(Master_action & action, unsigned rtop_left, unsigned rtop_right, unsigned rbottom_right, unsigned rbottom_left, Action::Flags items=Action::ALL);

    /// Constructor with Action and icon size.
    /// @param icon_size        the icon size in pixels or one of Icon class enumerator.
    /// @param action           the action to be used.
    /// @param items            action items to be used, see @ref action_flags_group.
    /// @since 0.7.0
    explicit Toggle(Master_action & action, int icon_size, Action::Flags items=Action::ALL);

    /// Constructor with Master_action, icon size and border radius.
    /// @param icon_size        the icon size in pixels or one of Icon class enumerator.
    /// @param action           the action to be used.
    /// @param radius           border radius, in pixels.
    /// @param items            action items to be used, see @ref action_flags_group.
    /// @since 0.7.0
    explicit Toggle(Master_action & action, int icon_size, unsigned radius, Action::Flags items=Action::ALL);

    /// Constructor with Master_action, icon size and border radius.
    /// @param icon_size        the icon size in pixels or one of Icon class enumerator.
    /// @param action           the action to be used.
    /// @param rtop_left        top left border radius, in pixels.
    /// @param rtop_right       top right border radius, in pixels.
    /// @param rbottom_right    bottom right border radius, in pixels.
    /// @param rbottom_left     bottom left border radius, in pixels.
    /// @param items            action items to be used, see @ref action_flags_group.
    /// @since 0.7.0
    explicit Toggle(Master_action & action, int icon_size, unsigned rtop_left, unsigned rtop_right, unsigned rbottom_right, unsigned rbottom_left, Action::Flags items=Action::ALL);

    /// Copy constructor.
    /// @note This class is a wrapper around its implementation shared pointer,
    /// so copying it just increasing implementation pointer use count, but isn't
    /// really copies the object. The underlying implementation is not copyable.
    Toggle(const Toggle & other);

    /// Copy operator.
    /// @note This class is a wrapper around its implementation shared pointer,
    /// so copying it just increasing implementation pointer use count, but isn't
    /// really copies the object. The underlying implementation is not copyable.
    Toggle & operator=(const Toggle & other);

    /// Move constructor.
    /// @note This class is a wrapper around its implementation shared pointer,
    /// so copying it just increasing implementation pointer use count, but isn't
    /// really copies the object. The underlying implementation is not copyable.
    /// @note This constructor does not reset @c other implementation pointer.
    Toggle(Toggle && other);

    /// Move operator.
    /// @note This class is a wrapper around its implementation shared pointer,
    /// so copying it just increasing implementation pointer use count, but isn't
    /// really copies the object. The underlying implementation is not copyable.
    /// @note This operator does not reset @c other implementation pointer.
    Toggle & operator=(Toggle && other);

    /// Constructor with implementation pointer.
    /// @warning Unlike some other classes (Painter as an example), the whole
    /// @ref widget_stack "widget stack" is unable to run with pure implementation
    /// pointer, so attempting to construct widget from a pure (@b nullptr) pointer
    /// will cause throwing an user_error exception!
    /// That exception also will be thrown if user tries to construct the object
    /// from incompatible implementation shared pointer.
    ///
    /// @throw user_error in case of pure implementation pointer or incompatible
    /// implementation pointer class.
    /// @since 0.4.0
    Toggle(Widget_ptr wp);

    /// Assign implementation.
    ///
    /// @throw user_error in case of pure implementation pointer or incompatible
    /// implementation pointer class.
    /// @since 0.4.0
    Toggle & operator=(Widget_ptr wp);

    /// @}
    /// @name Controls
    /// @{

    /// Set Toggle_action.
    /// @since 0.7.0
    void set_action(Toggle_action & action, Action::Flags items=Action::ALL);

    /// Select Toggle_action.
    /// @since 0.7.0
    void select(Toggle_action & action);

    /// @}
    /// @name Execute
    /// @{

    /// Toggle button.
    void toggle();

    /// Get state.
    /// @sa signal_toggle()
    bool get() const noexcept;

    /// Set state without execution.
    /// @since 0.5.0
    void setup(bool state);

    /// Set state with execution.
    /// @since 0.5.0
    void set(bool state);

    /// Connect slot to internal signal.
    /// @since 0.6.0
    connection connect(const slot<void(bool)> & slot, bool prepend=false);

    /// Connect slot to internal signal.
    /// @since 0.6.0
    connection connect(slot<void(bool)> && slot, bool prepend=false);

    /// @}
};

} // namespace tau

#endif // __TAU_BUTTON_HH__

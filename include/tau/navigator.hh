// ----------------------------------------------------------------------------
// SPDX-License-Identifier: BSD-2-Clause
// Copyright © 2014-2024 Konstantin Shmelkov <mrcashe@gmail.com>.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
// THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
// PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
// CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
// EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
// PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
// OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
// WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
// OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
// EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// ----------------------------------------------------------------------------

#ifndef __TAU_NAVIGATOR_HH__
#define __TAU_NAVIGATOR_HH__

/// @file navigator.hh The %Navigator (file system navigation widget) class.

#include <tau/ustring.hh>
#include <tau/widget.hh>

namespace tau {

/// File system navigation widget.
///
/// @note This class is a wrapper around its implementation shared pointer.
///
/// @ingroup file_group
/// @ingroup widget_group
class __TAUEXPORT__ Navigator: public Widget {
public:

    /// @name Constructors & Operators
    /// @{

    /// Constructor with path.
    Navigator(const ustring & uri=ustring());

    /// Copy constructor.
    ///
    /// @note This class is a wrapper around its implementation shared pointer,
    /// so copying it just increasing implementation pointer use count, but isn't
    /// really copies the object. The underlying implementation is not copyable.
    Navigator(const Navigator & other);

    /// Copy operator.
    ///
    /// @note This class is a wrapper around its implementation shared pointer,
    /// so copying it just increasing implementation pointer use count, but isn't
    /// really copies the object. The underlying implementation is not copyable.
    Navigator & operator=(const Navigator & other);

    /// Move constructor.
    ///
    /// @note This class is a wrapper around its implementation shared pointer,
    /// so copying it just increasing implementation pointer use count, but isn't
    /// really copies the object. The underlying implementation is not copyable.
    /// @note This constructor does not reset @c other implementation pointer.
    Navigator(Navigator && other);

    /// Move operator.
    ///
    /// @note This class is a wrapper around its implementation shared pointer,
    /// so copying it just increasing implementation pointer use count, but isn't
    /// really copies the object. The underlying implementation is not copyable.
    /// @note This operator does not reset @c other implementation pointer.
    Navigator & operator=(Navigator && other);

    /// Constructor with implementation pointer.
    ///
    /// @warning Unlike some other classes (Painter as an example), the whole
    /// @ref widget_stack "widget stack" is unable to run with pure implementation
    /// pointer, so attempting to construct widget from a pure (@b nullptr) pointer
    /// will cause throwing an user_error exception!
    /// That exception also will be thrown if user tries to construct the object
    /// from incompatible implementation shared pointer.
    ///
    /// @throw user_error in case of pure implementation pointer or incompatible
    /// implementation pointer class.
    /// @since 0.4.0
    Navigator(Widget_ptr wp);

    /// Assign implementation.
    ///
    /// @throw user_error in case of pure implementation pointer or incompatible
    /// implementation pointer class.
    /// @since 0.4.0
    Navigator & operator=(Widget_ptr wp);

    /// @}
    /// Set current URI.
    void set_uri(const ustring & uri);

    /// Get current URI.
    ustring uri() const;

    /// Reload current directory.
    void refresh();

    /// Set sort column name.
    void sort_by(const ustring & col);

    /// Get sort column name.
    ustring sorted_by() const;

    /// Show info items.
    /// @param opt option name, see @ref navigator_info for meaning.
    /// @sa reset_option() has_option() options()
    void set_option(std::string_view opt);

    /// Hide info items.
    /// @param opt option name, see @ref navigator_info for meaning.
    /// @sa set_option() has_option() options()
    void reset_option(std::string_view opt);

    /// Test if info item visible.
    /// @param opt option name, see @ref navigator_info for meaning.
    /// @sa set_option() reset_option() options()
    bool has_option(std::string_view opt) const;

    /// %List visible info items.
    /// @param sep list separator
    /// @return option list, see @ref navigator_info for meaning.
    /// @sa set_option() reset_option() has_option()
    std::string options(char32_t sep=U':') const;

    /// Set filter.
    void set_filter(const ustring & patterns);

    /// Get current filter.
    ustring filter() const;

    /// Signal emitted when file selected.
    /// The bound string represents a name of the currently selected file or directory.
    signal<void(const ustring &)> & signal_file_select();

    /// Signal emitted when file unselected.
    /// The bound string represents a name of the unselected file or directory.
    signal<void(const ustring &)> & signal_file_unselect();

    /// Signal emitted when directory had changed.
    /// The bound string represents a full path to the current directory.
    signal<void(const ustring &)> & signal_dir_changed();

    /// Signal emitted when:
    /// 1. The user presses "ENTER" on file.
    /// 2. The user double clicks on file.
    /// The bound string represents a full path to the clicked or entered file or directory.
    signal<void(const ustring &)> & signal_file_activate();
};

} // namespace tau

#endif //__TAU_NAVIGATOR_HH__

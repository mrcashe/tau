// ----------------------------------------------------------------------------
// SPDX-License-Identifier: BSD-2-Clause
// Copyright © 2014-2024 Konstantin Shmelkov <mrcashe@gmail.com>.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
// THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
// PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
// CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
// EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
// PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
// OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
// WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
// OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
// EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// ----------------------------------------------------------------------------

#ifndef __TAU_ROLLER_HH__
#define __TAU_ROLLER_HH__

/// @file roller.hh The Roller class.

#include <tau/enums.hh>
#include <tau/container.hh>

namespace tau {

/// Single/Double dimension scrolling container with automatically hiding scrolling arrows/sliders.
/// The dimension depends on constructor used for creation. When contructor with Orientation used,
/// we have 1D %Roller with two buttons for scrolling up/down or left/right. When constructor
/// without Orientation used, we have 2D %Roller with two sliders.
///
/// @note This class is a wrapper around its implementation shared pointer.
/// @ingroup compound_container_group
class __TAUEXPORT__ Roller: public Container {
public:

    /// @name Constructors & Operators
    /// @{

    /// 2D constructor.
    /// @param autohide Automatically show/hide sliders.
    Roller(bool autohide=true);

    /// 1D constructor.
    /// @param orient Orientation.
    /// @param autohide Automatically show/hide scroll buttons.
    Roller(Orientation orient, bool autohide=true);

    /// Copy constructor.
    /// @note This class is a wrapper around its implementation shared pointer,
    /// so copying it just increasing implementation pointer use count, but isn't
    /// really copies the object. The underlying implementation is not copyable.
    Roller(const Roller & other);

    /// Copy operator.
    /// @note This class is a wrapper around its implementation shared pointer,
    /// so copying it just increasing implementation pointer use count, but isn't
    /// really copies the object. The underlying implementation is not copyable.
    Roller & operator=(const Roller & other);

    /// Move constructor.
    /// @note This class is a wrapper around its implementation shared pointer,
    /// so copying it just increasing implementation pointer use count, but isn't
    /// really copies the object. The underlying implementation is not copyable.
    /// @note This constructor does not reset @c other implementation pointer.
    Roller(Roller && other);

    /// Move operator.
    /// @note This class is a wrapper around its implementation shared pointer,
    /// so copying it just increasing implementation pointer use count, but isn't
    /// really copies the object. The underlying implementation is not copyable.
    /// @note This operator does not reset @c other implementation pointer.
    Roller & operator=(Roller && other);

    /// Constructor with implementation pointer.
    /// @warning Unlike some other classes (Painter as an example), the whole
    /// @ref widget_stack "widget stack" is unable to run with pure implementation
    /// pointer, so attempting to construct widget from a pure (@b nullptr) pointer
    /// will cause throwing an user_error exception!
    /// That exception also will be thrown if user tries to construct the object
    /// from incompatible implementation shared pointer.
    /// @throw user_error in case of pure implementation pointer or incompatible
    /// implementation pointer class.
    /// @since 0.4.0
    Roller(Widget_ptr wp);

    /// Assign implementation.
    /// @throw user_error in case of pure implementation pointer or incompatible
    /// implementation pointer class.
    /// @since 0.4.0
    Roller & operator=(Widget_ptr wp);

    /// @}
    /// @name Widget Insertion/Removal
    /// @{

    /// Insert child.
    /// @throw user_error if widget already inserted into another container.
    void insert(Widget & w);

    /// Remove child.
    void clear();

    /// Test if empty.
    /// @since 0.6.0
    bool empty() const noexcept;

    /// @}
    /// @name Controls
    /// @{

    /// Get managed Scroller.
    /// @since 0.6.0
    Scroller scroller();

    /// Get managed Scroller.
    /// @since 0.6.0
    const Scroller scroller() const;

    /// Pan to specified position.
    /// @note Effective only for 1D mode.
    void pan(int pos);

    /// Get current offset.
    /// @note Effective only for 1D mode.
    int pan() const noexcept;

    /// Set position increment step.
    /// @note Effective only for 1D mode.
    void set_step(int step);

    /// Get position increment step.
    /// @note Effective only for 1D mode.
    int step() const noexcept;

    /// Automatically show or hide scroll buttons/sliders.
    void allow_autohide() noexcept;

    /// Show scroll buttons/sliders, do not hide automatically.
    void disallow_autohide() noexcept;

    /// Test if autohide option is set.
    bool autohide_allowed() const noexcept;

    /// @}
};

} // namespace tau

#endif // __TAU_ROLLER_HH__

// ----------------------------------------------------------------------------
// SPDX-License-Identifier: BSD-2-Clause
// Copyright © 2014-2024 Konstantin Shmelkov <mrcashe@gmail.com>.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
// THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
// PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
// CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
// EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
// PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
// OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
// WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
// OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
// EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// ----------------------------------------------------------------------------

/// @file check.hh The Check class.

#ifndef __TAU_CHECK_HH__
#define __TAU_CHECK_HH__

#include <tau/enums.hh>
#include <tau/widget.hh>

namespace tau {

/// %Widget that can be checked or not.
///
/// @note This class is a wrapper around its implementation shared pointer.
///
/// @ingroup widget_group
class __TAUEXPORT__ Check: public Widget {
public:

    /// Check style.
    /// @ingroup check_style_group
    enum Style {
        /// V-style rectangular check.
        VSTYLE,

        /// X-style rectangular check.
        XSTYLE,

        /// Circular (Radio-like) check.
        RSTYLE,

        /// Square mark rectangular check.
        /// @since 0.6.0
        QSTYLE
    };

    /// @name Constructors
    /// @{

    /// Default constructor.
    /// Constructs basic %Check.
    /// @param checked the initial state.
    Check(bool checked=false);

    /// Constructor with check style.
    /// @param check_style the check style.
    /// @param checked the initial state.
    Check(Style check_style, bool checked=false);

    /// Constructor with border style.
    /// @param border_style the border style.
    /// @param checked the initial state.
    Check(Border border_style, bool checked=false);

    /// Constructor with check and border style.
    /// @param check_style the check style.
    /// @param border_style the border style.
    /// @param checked the initial state.
    Check(Style check_style, Border border_style, bool checked=false);

    /// Copy constructor.
    ///
    /// @note This class is a wrapper around its implementation shared pointer,
    /// so copying it just increasing implementation pointer use count, but isn't
    /// really copies the object. The underlying implementation is not copyable.
    Check(const Check & other);

    /// Copy operator.
    ///
    /// @note This class is a wrapper around its implementation shared pointer,
    /// so copying it just increasing implementation pointer use count, but isn't
    /// really copies the object. The underlying implementation is not copyable.
    Check & operator=(const Check & other);

    /// Move constructor.
    ///
    /// @note This class is a wrapper around its implementation shared pointer,
    /// so copying it just increasing implementation pointer use count, but isn't
    /// really copies the object. The underlying implementation is not copyable.
    /// @note This constructor does not reset @c other implementation pointer.
    Check(Check && other);

    /// Move operator.
    ///
    /// @note This class is a wrapper around its implementation shared pointer,
    /// so copying it just increasing implementation pointer use count, but isn't
    /// really copies the object. The underlying implementation is not copyable.
    /// @note This operator does not reset @c other implementation pointer.
    Check & operator=(Check && other);

    /// Constructor with implementation pointer.
    ///
    /// @warning Unlike some other classes (Painter as an example), the whole
    /// @ref widget_stack "widget stack" is unable to run with pure implementation
    /// pointer, so attempting to construct widget from a pure (@b nullptr) pointer
    /// will cause throwing an user_error exception!
    /// That exception also will be thrown if user tries to construct the object
    /// from incompatible implementation shared pointer.
    ///
    /// @throw user_error in case of pure implementation pointer or incompatible
    /// implementation pointer class.
    /// @since 0.4.0
    Check(Widget_ptr wp);

    /// Assign implementation.
    ///
    /// @throw user_error in case of pure implementation pointer or incompatible
    /// implementation pointer class.
    /// @since 0.4.0
    Check & operator=(Widget_ptr wp);

    /// @}
    /// @name State Management
    /// @{

    /// Set checked state.
    /// signal_check() emitted if %Check was not checked prior this call.
    void check();

    /// Reset checked state.
    /// If %Check joined radio group, this call has no effect.
    /// signal_uncheck() emitted if %Check was checked prior this call.
    void uncheck();

    /// Set %Check state accordingly given parameter with signals emission.
    /// @param state calls check() if @b true and uncheck() otherwise.
    /// If %Check joined radio group and @a state is @b false, this call has no effect.
    /// One of signal_check() and signal_uncheck() emitted during this call.
    /// @since 0.5.0
    void set(bool state);

    /// Set %Check state accordingly given parameter without signals emission.
    /// @param state new %Check state.
    /// If %Check joined radio group and @a state is @b false, this call has no effect.
    /// No signals emission performed during this call even if %Check joined radio group.
    /// @since 0.5.0
    void setup(bool state);

    /// Toggle check.
    /// One of signal_check() and signal_uncheck() emitted during this call.
    void toggle();

    /// Get current check state.
    bool checked() const noexcept;

    /// Allow state change.
    /// @remark Allowed by default.
    /// @since 0.6.0
    void allow_edit();

    /// Disallow state change.
    /// @remark Allowed by default.
    /// @since 0.6.0
    void disallow_edit();

    /// Test if state change allowed.
    /// @remark Allowed by default.
    /// @since 0.6.0
    bool edit_allowed() const noexcept;

    /// @}
    /// @name Border Management
    /// @{

    /// Set border style.
    void set_border_style(Border bs);

    /// Get border style.
    Border border_style() const noexcept;

    /// Set border width.
    void set_border_width(unsigned npx);

    /// Get border width.
    unsigned border_width() const noexcept;

    /// @}
    /// @name Controls
    /// @{

    /// Set check style.
    /// @param cs the check style.
    void set_check_style(Style cs);

    /// Get check style.
    Style check_style() const noexcept;

    /// Join same radio group which other check belongs.
    /// @note There is no way to leave radio group!
    void join(Check & other);

    /// Test if joined radio group.
    bool joined() const noexcept;

    /// @}
    /// @name Signals
    /// @{

    /// Signal emitted when check state changed to true.
    ///
    /// Slot prototype:
    /// ~~~~~~~~~~~~~~
    /// void on_check();
    /// ~~~~~~~~~~~~~~
    signal<void()> & signal_check();

    /// Signal emitted when check state changed to false.
    ///
    /// Slot prototype:
    /// ~~~~~~~~~~~~~~
    /// void on_uncheck();
    /// ~~~~~~~~~~~~~~
    signal<void()> & signal_uncheck();

    /// @}
};

} // namespace tau

#endif // __TAU_CHECK_HH__

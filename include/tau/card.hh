// ----------------------------------------------------------------------------
// SPDX-License-Identifier: BSD-2-Clause
// Copyright © 2014-2024 Konstantin Shmelkov <mrcashe@gmail.com>.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
// THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
// PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
// CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
// EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
// PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
// OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
// WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
// OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
// EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// ----------------------------------------------------------------------------

/// @file card.hh The Card class.

#ifndef __TAU_CARD_HH__
#define __TAU_CARD_HH__

#include <tau/container.hh>

namespace tau {

/// %Container that can display only one its child at a time.
///
/// @note This class is a wrapper around its implementation shared pointer.
///
/// To show or hide particular child, simply call Widget::show() or
/// Widget::hide() on that child.
///
/// You also can cycle over children using Card::show_next() and
/// Card::show_previous() methods.
///
/// @ingroup flat_container_group
class __TAUEXPORT__ Card: public Container {
public:

    /// @name Constructors
    /// @{

    /// Default constructor.
    /// Constructs basic %Card @ref container_group "container".
    Card();

    /// Copy constructor.
    ///
    /// @note This class is a wrapper around its implementation shared pointer,
    /// so copying it just increasing implementation pointer use count, but isn't
    /// really copies the object. The underlying implementation is not copyable.
    Card(const Card & other);

    /// Copy operator.
    ///
    /// @note This class is a wrapper around its implementation shared pointer,
    /// so copying it just increasing implementation pointer use count, but isn't
    /// really copies the object. The underlying implementation is not copyable.
    Card & operator=(const Card & other);

    /// Move constructor.
    ///
    /// @note This class is a wrapper around its implementation shared pointer,
    /// so copying it just increasing implementation pointer use count, but isn't
    /// really copies the object. The underlying implementation is not copyable.
    /// @note This constructor does not reset @c other implementation pointer.
    Card(Card && other);

    /// Move operator.
    ///
    /// @note This class is a wrapper around its implementation shared pointer,
    /// so copying it just increasing implementation pointer use count, but isn't
    /// really copies the object. The underlying implementation is not copyable.
    /// @note This operator does not reset @c other implementation pointer.
    Card & operator=(Card && other);

    /// Constructor with implementation pointer.
    ///
    /// @warning Unlike some other classes (Painter as an example), the whole
    /// @ref widget_stack "widget stack" is unable to run with pure implementation
    /// pointer, so attempting to construct widget from a pure (@b nullptr) pointer
    /// will cause throwing an user_error exception!
    /// That exception also will be thrown if user tries to construct the object
    /// from incompatible implementation shared pointer.
    ///
    /// @throw user_error in case of pure implementation pointer or incompatible
    /// implementation pointer class.
    /// @since 0.4.0
    Card(Widget_ptr wp);

    /// Assign implementation.
    ///
    /// @throw user_error in case of pure implementation pointer or incompatible
    /// implementation pointer class.
    /// @since 0.4.0
    Card & operator=(Widget_ptr wp);

    /// @}
    /// @name Widget insertion & removal
    /// @{

    /// Add child into container.
    /// @param w            Widget to be inserted
    /// @param take_focus   if @b true, show widget and invite it to grab focus.
    /// @throw user_error if widget already inserted into another container.
    /// @throw internal_error if w has pure implementation pointer.
    /// @sa remove()
    /// @sa remove_current()
    /// @sa remove_others()
    /// @sa clear()
    void insert(Widget & w, bool take_focus=false);

    /// Remove child from the container.
    ///
    /// @sa insert()
    /// @sa remove_current()
    /// @sa remove_others()
    /// @sa clear()
    /// @return count of removed widgets.
    std::size_t remove(Widget & w);

    /// Remove current child.
    ///
    /// @sa insert()
    /// @sa remove()
    /// @sa remove_others()
    /// @sa clear()
    /// @return count of removed widgets.
    std::size_t remove_current();

    /// Remove all but the current child.
    ///
    /// @sa insert()
    /// @sa remove()
    /// @sa remove_current()
    /// @sa clear()
    /// @return count of removed widgets.
    /// @since 0.5.0
    std::size_t remove_others();

    /// Remove all children.
    ///
    /// @sa insert()
    /// @sa remove()
    /// @sa remove_current()
    /// @sa remove_others()
    void clear();

    /// @}
    /// @name Controls
    /// @{

    /// Get children count.
    /// @since 0.5.0
    std::size_t count() const noexcept;

    /// Test if empty.
    bool empty() const noexcept;

    /// Get current child.
    /// @since 0.5.0
    Widget_ptr current();

    /// Get current child.
    /// @since 0.5.0
    Widget_cptr current() const;

    /// Show next child.
    void show_next();

    /// Show previous child.
    void show_previous();

    /// @}
};

} // namespace tau

#endif // __TAU_CARD_HH__

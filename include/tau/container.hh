// ----------------------------------------------------------------------------
// SPDX-License-Identifier: BSD-2-Clause
// Copyright © 2014-2024 Konstantin Shmelkov <mrcashe@gmail.com>.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
// THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
// PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
// CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
// EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
// PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
// OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
// WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
// OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
// EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// ----------------------------------------------------------------------------

/// @file container.hh The Container class.

#ifndef __TAU_CONTAINER_HH__
#define __TAU_CONTAINER_HH__

#include <tau/widget.hh>

namespace tau {

/// An abstract widget @ref container_group "container" base.
///
/// @note This class is a wrapper around its implementation shared pointer.
///
/// @ingroup container_group
class __TAUEXPORT__ Container: public Widget {
public:

    /// @name Controls
    /// @{

    /// Retrieve child implementation smart pointer by its raw pointer.
    /// @since 0.6.0
    Widget_ptr chptr(Widget_impl * wp) noexcept;

    /// Retrieve child implementation smart pointer by its raw pointer.
    /// @since 0.6.0
    Widget_cptr chptr(const Widget_impl * wp) const noexcept;

    /// Get focused child.
    /// @sa focus_endpoint()
    /// @since 0.6.0
    Widget_ptr focused_child();

    /// Get focused child.
    /// @sa focus_endpoint() const
    /// @since 0.6.0
    Widget_cptr focused_child() const;

    /// Get focus owner.
    /// In contrast to focused_child() performs deep container introspection
    /// and find final focus receiver within container.
    /// @sa focused_child()
    Widget_ptr focus_endpoint();

    /// Get focus owner.
    /// In contrast to focused_child() performs deep container introspection
    /// and find final focus receiver within container.
    /// @sa focused_child() const
    Widget_cptr focus_endpoint() const;

    /// Get children.
    std::list<Widget_ptr> children() const;

    /// Manage widget.
    /// @since 0.6.0
    Widget & manage(Widget * w);

    /// @}
    /// @name Focus Chain Control
    /// @{

    /// Set focus order for specified widget.
    /// @param w the widget to be focused
    /// @param after_this after this widget
    /// @since 0.6.0
    /// @sa action_focus_next()
    void focus_after(Widget & w, Widget & after_this);

    /// Set focus order for specified widget.
    /// @param w the widget to be focused
    /// @param before_this before this widget
    /// @since 0.6.0
    /// @sa action_focus_previous()
    void focus_before(Widget & w, Widget & before_this);

    /// Cancel assignment done by focus_before() or focus_next().
    /// @param w the widget to be removed from the focus chain.
    /// @since 0.6.0
    /// @sa focus_after()
    /// @sa focus_before()
    /// @sa action_focus_previous()
    /// @sa action_focus_next()
    void unchain_focus(Widget & w);

    /// @}
    /// @name Object Related Stuff
    /// @{

    /// Link widget with a given name.
    /// @relates Object::link()
    /// @since 0.6.0
    void link(Widget & w, const ustring & name);

    /// Lookup widget by its effective path.
    /// @relates Object::lookup()
    /// @since 0.6.0
    Widget_ptr lookup(const ustring & ep) const;

    /// Get widget name.
    /// Gets widget name if previously assigned by calling link().
    /// @relates Object::name()
    /// @since 0.6.0
    ustring name(const Widget & w) const;

    /// Get all names.
    /// Gets all widget names registered within this container using link() method.
    /// @relates Object::names()
    /// @since 0.6.0
    std::vector<ustring> names() const;

    /// @}
    /// @name Actions and Signals
    /// @{

    /// Get "focus next" action.
    /// @since 0.6.0
    /// @sa focus_after()
    Action & action_focus_next();

    /// Get "focus previous" action.
    /// @since 0.6.0
    /// @sa focus_before()
    Action & action_focus_previous();

    /// Signal emitted when some child (or children) added to or removed from container.
    ///
    /// Slot prototype:
    /// ~~~~~~~~~~~~~~~
    /// void on_children_changed();
    /// ~~~~~~~~~~~~~~~
    signal<void()> & signal_children_changed();

    /// @}

protected:

    /// @name Methods for custom container realization.
    /// @{

    /// @throw user_error if widget already inserted into another container.
    void make_child(Widget & w);

    void unparent_child(Widget & w);

    bool update_child_bounds(Widget & w, const Rect & bounds);
    bool update_child_bounds(Widget & w, const Point & origin, const Size & sz=Size());
    bool update_child_bounds(Widget & w, int x, int y, const Size & sz=Size());
    bool update_child_bounds(Widget & w, int x, int y, unsigned width, unsigned height);

    /// Force children arrange.
    void queue_arrange();

    signal<void()> & signal_arrange();

    /// @}

    /// @private
    Container(Widget_ptr wp);

    /// @private
    Container & operator=(Widget_ptr wp);
};

} // namespace tau

#endif // __TAU_CONTAINER_HH__

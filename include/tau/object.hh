// ----------------------------------------------------------------------------
// SPDX-License-Identifier: BSD-2-Clause
// Copyright © 2014-2024 Konstantin Shmelkov <mrcashe@gmail.com>.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
// THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
// PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
// CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
// EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
// PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
// OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
// WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
// OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
// EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// ----------------------------------------------------------------------------

/// @file widget.hh The Object class.

#ifndef __TAU_OBJECT_HH__
#define __TAU_OBJECT_HH__

#include <tau/signal.hh>
#include <tau/defs.hh>
#include <tau/ustring.hh>

namespace tau {

/// @brief The base class for widget implementation classes and some other classes.
/// %Object allows to organize objects into hierarchical tree-like structure.
/// The navigation within that structure is done using Unix-like filesystem paths.
/// Also %Object provides simple optional storage for single textual value.
/// For more information about using objects, see @ref object_guide.
/// @ingroup object_group
class __TAUEXPORT__ Object: public trackable {
public:

    /// @name Constructors, Destructor and Operators.
    /// @{

    /// Default constructor.
    /// Creates %Object with empty stored value.
    Object();

    /// Constructor with string as stored value.
    Object(const ustring & s);

    /// Constructor with boolean as stored value.
    Object(bool yes);

    /// Constructor with integer as stored value.
    Object(intmax_t i);

    /// Constructor with double as stored value.
    Object(double d);

    /// Polymorphic class.
    virtual ~Object();

    /// Non-copyable.
    Object(const Object & other) = delete;

    /// Non-copyable.
    Object & operator=(const Object & other) = delete;

    /// Non-copyable.
    Object(Object && other) = delete;

    /// Non-copyable.
    Object & operator=(Object && other) = delete;

    /// @}
    /// @name Object Insertion and Removal
    /// @{

protected:

    /// Destroy.
    void destroy();

    /// Shutdown.
    void shutdown(bool yes);

public:

    /// @}
    /// @name Controls
    /// @{

    /// @brief Add not-owning object.
    /// The specified object may be of these knids:
    /// - Object having no parent (o->parent() == nullptr)
    /// - Object already inserted into this object (only renaming performed).
    /// - Object having parent different from this (i.e. foreign object), link created.
    /// @throw user_error in case of:
    ///     - Invalid characters in name.
    ///     - Name already registered and belongs to the another object.
    /// @sa link(Object_ptr, const ustring &)
    /// @sa unlink()
    /// @sa unlink_all()
    /// @sa signal_link()
    /// @sa signal_name()
    void link(Object * o, const ustring & name=ustring());

    /// @brief Add owning object.
    /// @note   When specified object has parent which is not @b nullptr and does not equal
    ///         to this object, the object threated as foreign and owning will not happen.
    /// @throw user_error in case of:
    ///     - Invalid characters in name.
    ///     - Name already registered and belongs to the another object.
    /// @sa link(Object *, const ustring &)
    /// @sa unlink()
    /// @sa unlink_all()
    /// @sa signal_link()
    /// @sa signal_name()
    void link(Object_ptr op, const ustring & name=ustring());

    /// @brief Remove object.
    /// @sa link(Object *, const ustring &)
    /// @sa link(Object_ptr, const ustring &)
    /// @sa unlink_all()
    /// @sa signal_link()
    void unlink(Object * o);

    /// @brief Remove all objects.
    /// @sa link(Object *, const ustring &)
    /// @sa link(Object_ptr, const ustring &)
    /// @sa unlink()
    /// @sa signal_link()
    void unlink_all();

    /// @brief Gets managed objects count.
    /// Counts all objects including owning, not-owning and foreign.
    std::size_t count_objects() const noexcept;

    /// @brief Gets all objects.
    /// Returns all objects including owning, not-owning and foreign.
    /// To determine which object is foreign (a link), simply compare its parent() against pointer to this object.
    std::vector<Object *> objects() const;

    /// @brief Gets pointer to the parent object.
    Object * parent() noexcept;

    /// @brief Gets pointer to the parent object.
    const Object * parent() const noexcept;

    /// @brief Test if object is in "shutdown" state.
    /// The "shutdown" state occurs when object is destroying
    /// or removing from parent container is going on. This is a critical state
    /// in the object's life time and some operations must be limited.
    /// @since 0.5.0
    bool in_shutdown() const noexcept;

    /// @brief Test if object is in "destroy" state.
    /// The "destroy" state occurs when object or any of its parent is destroying.
    /// This is a critical state in the object's life time and some operations must be limited.
    /// @since 0.6.0
    bool in_destroy() const noexcept;

    /// @brief Get object name by pointer.
    /// @return the object name, if not assigned, returns text representation of
    ///         the object pointer, for exaple: 0x876543210.
    /// @sa signal_name()
    ustring name(const Object * o) const;

    /// @brief Get object names.
    /// Gets all names registered within this object.
    /// @sa name()
    std::vector<ustring> names() const;

    /// @brief Get object effective path.
    ustring epath() const;

    /// @brief Lookup object by effective path.
    Object * lookup(const ustring & ep);

    /// @brief Lookup object by effective path.
    const Object * lookup(const ustring & ep) const;

    /// @brief Gets smart pointer of the owning object.
    /// This call returns not-pure shared pointer in case given object owned by this object.
    /// In case it was linked in using link(Object *, const ustring &) method or was not linked at all,
    /// the result will be a pure pointer.
    Object_ptr ptr(Object * o);

    /// @brief Gets smart pointer of the owning object.
    /// This call returns not-pure shared pointer in case given object owned by this object.
    /// In case it was linked in using link(Object *, const ustring &) method or was not linked at all,
    /// the result will be a pure pointer.
    Object_cptr ptr(const Object * o) const;

    /// @}
    /// @name Value Storage
    /// @{

    /// Store value as string.
    void set_string(const ustring & s);

    /// Get value as string.
    ustring get_string() const;

    /// Store value as boolean.
    void set_boolean(bool yes);

    /// Get value as boolean.
    bool get_boolean() const;

    /// Store value as integer.
    void set_integer(intmax_t i);

    /// Get value as integer.
    intmax_t get_integer() const;

    /// Store value as double.
    void set_real(double d);

    /// Get value as double.
    double get_real() const;

    /// @}
    /// @name Signals
    /// @{

    /// @brief Get signal "parent".
    /// Signal is emitted when:
    /// - %Object inserted into up-level object (parameter is not @b nullptr);
    /// - %Object removed from the up-level object (parameter is @b nullptr).
    /// Slot prototype:
    /// ~~~~~~~~~~~~~~~
    /// void on_parent(Object * parent);
    /// ~~~~~~~~~~~~~~~
    ///
    /// @note When connecting to the signal, it is better choice to append handler
    /// rather then prepend it. This is because the object's implementation installs
    /// its own handler here to store pointer to the parent object. Hence it is
    /// possible situation when object don't know about the fact of insertion/removal
    /// into/from hierarchy while derived class handler already know about it if
    /// those handlers works before default one.
    signal<void(Object *)> & signal_parent();

    /// @brief Get signal "unparent".
    /// Signal is emitted when object removed from it's parent.
    /// Slot prototype:
    /// ~~~~~~~~~~~~
    /// void on_unparent();
    /// ~~~~~~~~~~~~
    signal<void()> & signal_unparent();

    /// @brief Get signal "link".
    /// Signal emitted when another object linked/unlinked into/from this object.
    /// The signal emitted within following conditions:
    /// 1. When not-owning widget linked for a first time using link(Object *, const ustring &) method.
    /// 2. When owning widget linked for a first time using link(Object_ptr, const ustring &) method.
    /// 3. When foreign object linked in this object using link(Object *, const ustring &) method.
    /// 4. When any of owned, not-owned or foreign object unlinked from this object using unlink() method.
    /// 5. When object name changed somehow using any of link() methods.
    ///
    /// Slot prototype:
    /// ~~~~~~~~~~~~
    /// void on_link(Object * current, Object * previous, const ustring & name);
    /// ~~~~~~~~~~~~
    /// Here is:
    /// - current:  The pointer to the object.
    ///     - In case pointing object linked in or renamed, this parameter is not @b nullptr.
    ///     - When pointing object has been linked out, this parameter is @b nullptr and @c previous is not @b nullptr.
    /// - previous: The pointer to the linked object.
    ///     - In case pointing object linked in for a first time, this parameter is @b nullptr.
    ///     - When object has been unlinked by calling unlink() method, this parameter holds pointer to the unlinked object.
    ///     - When object has been renamed using any of link() methods, this parameter is equal to @c current.
    /// - name: The object's name, if assigned.
    signal<void(Object *, Object *, const ustring &)> & signal_link();

    /// @brief Get signal "name".
    /// Signal intended for automatic object naming. When user links object with
    /// empty name, this signal emitted. Returning non-empty valid string you can
    /// assign an object name.
    ///
    /// Slot prototype:
    /// ~~~~~~~~~~~~
    /// ustring on_name(const Object * o);
    /// ~~~~~~~~~~~~
    signal<const char *(const Object *)> & signal_name();

    /// @brief Get signal "destroy".
    /// Signal is emitted when object destroyed.
    signal<void()> & signal_destroy();

    /// @}

private:

    struct Impl;
    Impl * impl;
};

} // namespace tau

#endif // __TAU_OBJECT_HH__

// ----------------------------------------------------------------------------
// SPDX-License-Identifier: BSD-2-Clause
// Copyright © 2014-2024 Konstantin Shmelkov <mrcashe@gmail.com>.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
// THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
// PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
// CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
// EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
// PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
// OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
// WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
// OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
// EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// ----------------------------------------------------------------------------

/// @file icon.hh The Icon class.
//  TODO Documentation: Windows ICON resource linking!

#ifndef __TAU_ICON_HH__
#define __TAU_ICON_HH__

#include <tau/action.hh>
#include <tau/widget.hh>

namespace tau {

/// An icon (image) displaying widget.
/// @note This class is a wrapper around its implementation shared pointer.
/// @ingroup paint_group
/// @ingroup widget_group
class __TAUEXPORT__ Icon: public Widget {
public:

    /// @brief %Icon sizes, in pixels, used when working with icons.
    /// These enumerators used as integers, so values in the range
    /// SMALLEST...LARGEST treats as indice in array placed in the
    /// Theme class. Values greater than LARGEST treats as number of pixels.
    /// @ingroup icon_size_group
    enum {
        /// Default icon size.
        DEFAULT    = 0,

        /// Default icon size is  8 pixels.
        NANO       = 1,

        /// Smallest icon.
        SMALLEST   = NANO,

        /// Default icon size is 12 pixels.
        TINY       = 2,

        /// Default icon size is 16 pixels.
        SMALL      = 3,

        /// Default icon size is 22 pixels.
        MEDIUM     = 4,

        /// Default icon size is 32 pixels.
        LARGE      = 5,

        /// Default icon size is 48 pixels.
        HEAVY      = 6,

        /// Largest icon.
        LARGEST    = HEAVY
    };

    /// @name Constructors & Operators
    /// @{

    /// Default constructor.
    Icon();

    /// Constructor with icon size.
    /// @param icon_size the icon size in pixels, see @ref icon_size_group.
    Icon(int icon_size);

    /// Constructor with icon name and icon size.
    /// @param icon_name the icon name.
    /// @param icon_size the icon size in pixels, see @ref icon_size_group.
    Icon(const ustring & icon_name, int icon_size);

    /// Constructor with Pixmap.
    /// TODO 0.8 add default icon_size.
    Icon(const Pixmap pix);

    /// @param icon_size the icon size in pixels, see @ref icon_size_group.
    /// @param action the action to be used.
    /// @param items action items to be used, see @ref action_flags_group.
    Icon(Action_base & action, int icon_size, Action::Flags items=Action::ALL);

    /// Copy constructor.
    /// @note This class is a wrapper around its implementation shared pointer,
    /// so copying it just increasing implementation pointer use count, but isn't
    /// really copies the object. The underlying implementation is not copyable.
    Icon(const Icon & other);

    /// Copy operator.
    /// @note This class is a wrapper around its implementation shared pointer,
    /// so copying it just increasing implementation pointer use count, but isn't
    /// really copies the object. The underlying implementation is not copyable.
    Icon & operator=(const Icon & other);

    /// Move constructor.
    /// @note This class is a wrapper around its implementation shared pointer,
    /// so copying it just increasing implementation pointer use count, but isn't
    /// really copies the object. The underlying implementation is not copyable.
    /// @note This constructor does not reset @c other implementation pointer.
    Icon(Icon && other);

    /// Move operator.
    /// @note This class is a wrapper around its implementation shared pointer,
    /// so copying it just increasing implementation pointer use count, but isn't
    /// really copies the object. The underlying implementation is not copyable.
    /// @note This operator does not reset @c other implementation pointer.
    Icon & operator=(Icon && other);

    /// Constructor with implementation pointer.
    /// @warning Unlike some other classes (Painter as an example), the whole
    /// @ref widget_stack "widget stack" is unable to run with pure implementation
    /// pointer, so attempting to construct widget from a pure (@b nullptr) pointer
    /// will cause throwing an user_error exception!
    /// That exception also will be thrown if user tries to construct the object
    /// from incompatible implementation shared pointer.
    ///
    /// @throw user_error in case of pure implementation pointer or incompatible
    /// implementation pointer class.
    /// @since 0.4.0
    Icon(Widget_ptr wp);

    /// Assign implementation.
    /// @throw user_error in case of pure implementation pointer or incompatible
    /// implementation pointer class.
    /// @since 0.4.0
    Icon & operator=(Widget_ptr wp);

    /// @}
    /// @name Controls
    /// @{

    /// Set icon name and icon size.
    /// @param icon_name the icon name.
    /// @param icon_size the icon size in pixels, see @ref icon_size_group.
    void assign(const ustring & icon_name, int icon_size);

    /// Set icon name.
    /// @overload
    /// @param icon_name the icon name.
    void assign(const ustring & icon_name);

    /// Set icon as Pixmap.
    /// @overload
    /// @since 0.5.0
    void assign(const Pixmap pix);

    /// Get icon name.
    ustring icon_name() const noexcept;

    /// Set icon size.
    /// @param icon_size the icon size in pixels, see @ref icon_size_group.
    void resize(int icon_size);

    /// Get icon size.
    /// @remark Returned value tells nothing about actual picture size. That
    /// value defined by the user in class constructor or by using resize() method.
    /// To obtain real picture size, use pixmap() methods.
    /// @return the icon size in pixels, see @ref icon_size_group.
    int icon_size() const noexcept;

    /// Test if empty.
    /// Empty icon does not have loaded pixmap.
    /// @since 0.5.0
    bool empty() const noexcept;

    /// Clear contents.
    /// @since 0.6.0
    void clear();

    /// Get Pixmap.
    /// @since 0.6.0
    Pixmap pixmap();

    /// Get Pixmap.
    /// @since 0.6.0
    const Pixmap pixmap() const;

    /// @}
    /// @name Signals
    /// @{

    /// Signal emitted when user clicks on the icon by left mouse button.
    /// @since 0.6.0
    signal<void()> & signal_click();

    /// @}
};

} // namespace tau

#endif // __TAU_ICON_HH__

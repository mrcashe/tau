// ----------------------------------------------------------------------------
// SPDX-License-Identifier: BSD-2-Clause
// Copyright © 2014-2024 Konstantin Shmelkov <mrcashe@gmail.com>.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
// THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
// PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
// CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
// EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
// PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
// OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
// WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
// OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
// EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// ----------------------------------------------------------------------------

/// @file enums.hh Enums.

#ifndef __TAU_ENUMS_HH__
#define __TAU_ENUMS_HH__

namespace tau {

/// The alignment of widget or text.
/// Used by multiple widgets.
/// @ingroup enum_group
enum class Align {
    /// Top or left alignment.
    START,

    /// Center alignment.
    CENTER,

    /// Bottom or right alignment.
    END,

    /// Fill alignment.
    FILL,

    /// Synonymous START.
    LEFT      = START,

    /// Synonymous START.
    TOP       = START,

    /// Synonymous CENTER.
    MIDDLE    = CENTER,

    /// Synonymous END.
    RIGHT     = END,

    /// Synonymous END.
    BOTTOM    = END
};

/// %Side used by Frame and Notebook.
/// @ingroup enum_group
enum class Side {

    /// Top side.
    TOP,

    /// Bottom side.
    BOTTOM,

    /// Left side.
    LEFT,

    /// Right side.
    RIGHT
};

/// The border around some widgets.
/// @ingroup enum_group
enum class Border {
    /// No border.
    NONE,

    /// Solid border.
    SOLID,

    /// Double line border.
    DOUBLE,

    /// Dotted line border.
    DOTTED,

    /// Dashed line border.
    DASHED,

    /// Pushed in border.
    INSET,

    /// Poped out border.
    OUTSET,

    /// Etched in border.
    GROOVE,

    /// Etched out border.
    RIDGE
};

/// The orientation.
/// Used by multiple widgets.
/// @ingroup enum_group
enum class Orientation {
    /// Down-to-Up orientation.
    UP,

    /// Up-to-Down orientation.
    DOWN,

    /// Right-to-Left orientation.
    LEFT,

    /// Left-to-Right orientation.
    RIGHT,

    /// Synonymous UP.
    NORTH       = UP,

    /// Synonymous DOWN.
    SOUTH       = DOWN,

    /// Synonymous LEFT.
    WEST        = LEFT,

    /// Synonymous RIGHT.
    EAST        = RIGHT,
};

/// The gravity.
/// @ingroup enum_group
enum class Gravity {
    /// Top left corner.
    TOP_LEFT,

    /// Top right corner.
    TOP_RIGHT,

    /// Bottom left corner.
    BOTTOM_LEFT,

    /// Bottom right corner.
    BOTTOM_RIGHT,

    /// Left gravity.
    LEFT,

    /// Right gravity.
    RIGHT,

    /// Top gravity.
    TOP,

    /// Bottom gravity.
    BOTTOM,

    /// Center.
    CENTER,

    /// Synonymous TOP_LEFT.
    NONE            = TOP_LEFT,

    /// Synonymous TOP_LEFT.
    LEFT_TOP        = TOP_LEFT,

    /// Synonymous TOP_RIGHT.
    RIGHT_TOP       = TOP_RIGHT,

    /// Synonymous BOTTOM_LEFT.
    LEFT_BOTTOM     = BOTTOM_LEFT,

    /// Synonymous BOTTOM_RIGHT.
    RIGHT_BOTTOM    = BOTTOM_RIGHT,

    /// Synonymous TOP_LEFT.
    NORTH_WEST      = TOP_LEFT,

    /// Synonymous TOP_RIGHT.
    NORTH_EAST      = TOP_RIGHT,

    /// Synonymous BOTTOM_LEFT.
    SOUTH_WEST      = BOTTOM_LEFT,

    /// Synonymous BOTTOM_RIGHT.
    SOUTH_EAST      = BOTTOM_RIGHT,

    /// Synonymous LEFT.
    WEST            = LEFT,

    /// Synonymous RIGHT.
    EAST            = RIGHT,

    /// Synonymous TOP.
    NORTH           = TOP,

    /// Synonymous BOTTOM.
    SOUTH           = BOTTOM
};

/// Line cap styles.
/// @ingroup enum_group
enum class Cap {
    SQUARE,
    FLAT,
    ROUND
};

/// Line join styles.
/// @ingroup enum_group
enum class Join {
    BEVEL,
    MITER,
    ROUND
};

/// Line styles.
/// @ingroup enum_group
enum class Line {
    /// Solid line.
    SOLID,

    /// Dashed line.
    DASH,

    /// Dotted line.
    /// @remark Currently not working with GDI if line width > 1, replaced by DASH_DOT.
    DOT,

    /// Dash-dotted line.
    DASH_DOT,

    /// Dash-dot-dotted line.
    DASH_DOT_DOT
};

/// Composition operators used while drawing.
/// @ingroup enum_group
enum class Oper {
    COPY,
    CLEAR,
    SOURCE,
    NOT,
    XOR,
    SET
};

/// Text word wrap modes.
/// @ingroup enum_group
enum class Wrap {
    /// Word wrap set off.
    NONE,

    /// Insert ellipsis at the start of line.
    ELLIPSIZE_START,

    /// Synonymous ELLIPSIZE_START.
    ELLIPSIZE_LEFT      = ELLIPSIZE_START,

    /// Insert ellipsis at the center of line.
    ELLIPSIZE_CENTER,

    // Synonymous ELLIPSIZE_CENTER.
    ELLIPSIZE_MIDDLE    = ELLIPSIZE_CENTER,

    /// Insert ellipsis at the end of line.
    ELLIPSIZE_END,

    /// Synonymous ELLIPSIZE_END.
    ELLIPSIZE_RIGHT     = ELLIPSIZE_END
};

/// Hints.
/// Used in conduction with Widget::signal_hints_changed().
/// @since 0.7.0
enum class Hints {
    HIDE        = -1,
    SIZE        = 0,
    SHOW        = 1
};

/// Metrics.
/// Metric's index used by metric() function.
/// @since 0.7.0
enum class Metric {
    /// Timer count.
    TIMERS,

    /// Widget count.
    WIDGETS,

    /// Signal count.
    SIGNALS,

    /// Slot count.
    SLOTS,

    /// Trackable count.
    TRACKABLES,

    /// Track count.
    TRACKS
};

} // namespace tau

#endif // __TAU_ENUMS_HH__

// ----------------------------------------------------------------------------
// SPDX-License-Identifier: BSD-2-Clause
// Copyright © 2014-2024 Konstantin Shmelkov <mrcashe@gmail.com>.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
// THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
// PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
// CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
// EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
// PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
// OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
// WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
// OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
// EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// ----------------------------------------------------------------------------

/// @file conf.hh The Conf class.

#ifndef __TAU_CONF_HH__
#define __TAU_CONF_HH__

#include <tau/color.hh>
#include <tau/signal.hh>
#include <tau/defs.hh>

namespace tau {

/// Basic style item.
/// @ingroup conf_group
class __TAUEXPORT__ Conf_item {
public:

    /// @private
    Conf_item(Conf_impl * cnf=nullptr);

    /// Copy constructor.
    Conf_item(const Conf_item & other) = default;

    /// Copy operator.
    Conf_item & operator=(const Conf_item & other) = default;

    /// Get value.
    ustring str() const;

    /// Set value or format.
    void set(const ustring & val=ustring());

    /// Test if value set.
    bool is_set() const noexcept;

    /// Unset value.
    void unset();

    /// Get "signal_changed".
    signal<void()> & signal_changed();

private:

    friend Conf_impl;
    friend Font_conf;

    Conf_impl * cnf_ = nullptr;

private:

    ustring & format();
};

/// Font item.
/// @ingroup conf_group
class __TAUEXPORT__ Font_conf {
public:

    /// Constructor.
    Font_conf(Conf_item & si);

    /// Get font specification string.
    ustring spec() const;

    /// Set font specification.
    void set(const ustring & spec);

    /// Get font specification string.
    operator ustring() const;

    /// Set font specification.
    void operator=(const ustring & s);

    /// Get font size in points.
    double size() const;

    /// Set font size.
    /// @param pts font size in points.
    /// @return previous value.
    double resize(double pts);

    /// Enlarge font size.
    /// @param pts font size in points.
    /// @return previous value.
    double enlarge(double pts);

    /// Grow font size.
    /// @param pts font size in points.
    /// @return previous value.
    double grow(double pts);

    /// Add face elements to the font.
    /// @param face_elements element to be added to font face specification.
    /// If current font specification is "Arial Bold" and the face_elements is
    /// "Italic", the result will be "Arial Bold Italic".
    void add_face(const ustring & face_elements);

    /// Set face.
    /// @param face face specification.
    /// If current font specification is "Arial Bold" and the face is
    /// "Italic", the result will be "Arial Italic".
    void set_face(const ustring & face);

    /// Make bold.
    /// @since 0.6.0
    void make_bold();

    /// Make italic.
    /// @since 0.6.0
    void make_italic();

private:

    Conf_item & cnf_;

private:

    void clear_size();
};

/// Color item.
/// @ingroup conf_group
class __TAUEXPORT__ Color_conf {
public:

    /// Constructor.
    Color_conf(Conf_item & si);

    /// Get color value.
    Color value() const noexcept;

    /// Set new color value.
    /// @param color the color.
    void set(const Color & color);

    /// Get color value.
    operator Color() const noexcept;

    /// Set new color value.
    Color_conf & operator=(const Color & color);

private:

    Conf_item & cnf_;
};

/// Brush item.
/// @ingroup conf_group
/// @since 0.6.0
class __TAUEXPORT__ Brush_conf {
public:

    /// Constructor.
    Brush_conf(Conf_item & si);

    /// Get value.
    Brush value() const noexcept;

    /// Set new value.
    /// @param brush the brush.
    void set(const Brush & brush);

    /// Get value.
    operator Brush() const noexcept;

    /// Set new value.
    Brush_conf & operator=(const Brush & brush);

private:

    Conf_item & cnf_;
};

/// Boolean item.
/// @ingroup conf_group
/// @since 0.7.0
class __TAUEXPORT__ Bool_conf {
public:

    /// Constructor.
    Bool_conf(Conf_item & si);

    /// Get value.
    /// @param fallback value to be returned in case item contains non-integer data.
    bool value(bool fallback=false) const noexcept;

    /// Set new value.
    void set(bool value);

    /// Get value.
    operator bool() const noexcept;

    /// Set new value.
    Bool_conf & operator=(bool value);

private:

    Conf_item & cnf_;
};

/// Integer item.
/// @ingroup conf_group
/// @since 0.7.0
class __TAUEXPORT__ Int_conf {
public:

    /// Constructor.
    Int_conf(Conf_item & si);

    /// Get value.
    /// @param fallback value to be returned in case item contains non-integer data.
    intmax_t value(intmax_t fallback=0) const noexcept;

    /// Set new value.
    void set(intmax_t value);

    /// Get value.
    operator intmax_t() const noexcept;

    /// Set new value.
    Int_conf & operator=(intmax_t value);

private:

    Conf_item & cnf_;
};

/// CSS-like configuration.
/// @ingroup conf_group
/// @sa @ref conf_sect
//  TODO parental items
//  TODO custom items
//  TODO item index to string
class __TAUEXPORT__ Conf {
public:

    /// @brief Enumerated standard style names.
    /// These values can be used instead of real item names in
    /// overloaded functions using integer item indice.
    /// @ingroup conf_items_group
    enum Item {
        /// Font style.
        /// The type is Font_conf.
        /// The actual name is "font".
        FONT,

        /// Monospace font.
        /// The type is Font_conf.
        /// The actual name is "mono_font".
        MONOSPACE_FONT,

        /// Font to be used by text editing widgets.
        /// The actual name is "edit_font".
        /// Normally initialized same as FONT.
        /// @since 0.6.0
        EDIT_FONT,

        /// Font to be used by tooltips.
        /// The type is Font_conf.
        /// The actual name is "tooltip_font".
        /// Normally initialized as FONT enlarged by -2 pts.
        /// @since 0.6.0
        TOOLTIP_FONT,

        /// Font to be used by menus.
        /// The type is Font_conf.
        /// The actual name is "menu_font".
        /// Normally initialized as FONT enlarged by -1 pts.
        /// @since 0.6.0
        MENU_FONT,

        /// Foreground.
        /// The type is Color_conf.
        /// The actual name is "foreground".
        FOREGROUND,

        /// Alternative foreground color.
        /// Usually contrast to normal foreground.
        /// The type is Color_conf.
        /// The actual name is "alt_foreground".
        /// @since 0.7.0
        ALT_FOREGROUND,

        /// Weak foreground color.
        /// Used to show inactive elements.
        /// The type is Color_conf.
        /// The actual name is "weak_foreground".
        /// @since 0.7.0
        WEAK_FOREGROUND,

        /// Menu foreground.
        /// The type is Color_conf.
        /// The actual name is "menu_foreground".
        MENU_FOREGROUND,

        /// Slider foreground.
        /// The type is Color_conf.
        /// The actual name is "slider_foreground".
        SLIDER_FOREGROUND,

        /// Progress foreground.
        /// The type is Color_conf.
        /// The actual name is "progress_foreground".
        PROGRESS_FOREGROUND,

        /// Accel foreground.
        /// The type is Color_conf.
        /// The actual name is "accel_foreground".
        ACCEL_FOREGROUND,

        /// Background.
        /// The type is Brush_conf.
        /// The actual name is "background".
        BACKGROUND,

        /// Alternative background brush.
        /// Usually contrast to normal background.
        /// The type is Brush_conf.
        /// The actual name is "alt_background".
        /// @since 0.7.0
        ALT_BACKGROUND,

        /// Progress background.
        /// The type is Brush_conf.
        /// The actual name is "progress_background".
        PROGRESS_BACKGROUND,

        /// Whitespace background.
        /// The type is Brush_conf.
        /// The actual name is "whitespace_background".
        WHITESPACE_BACKGROUND,

        /// Menu background.
        /// The type is Brush_conf.
        /// The actual name is "menu_background".
        MENU_BACKGROUND,

        /// Selection background.
        /// The type is Color_conf.
        /// The actual name is "select_background".
        SELECT_BACKGROUND,

        /// Button background.
        /// The type is Brush_conf.
        /// The actual name is "button_background".
        BUTTON_BACKGROUND,

        /// Slider background.
        /// The type is Brush_conf.
        /// The actual name is "slider_background".
        SLIDER_BACKGROUND,

        /// Mark background.
        /// The type is Brush_conf.
        /// The actual name is "info_background".
        INFO_BACKGROUND,

        /// Mark background.
        /// The type is Brush_conf.
        /// The actual name is "help_background".
        HELP_BACKGROUND,

        /// Mark background.
        /// The type is Brush_conf.
        /// The actual name is "warning_background".
        WARNING_BACKGROUND,

        /// Mark background.
        /// The type is Brush_conf.
        /// The actual name is "error_background".
        ERROR_BACKGROUND,

        /// Foreground to be used by tooltips.
        /// The type is Color_conf.
        /// The actual name is "tooltip_foreground".
        /// @since 0.6.0
        TOOLTIP_FOREGROUND,

        /// Background used for selection highlight.
        /// The type is Brush_conf.
        /// The actual name is "highlight_background".
        /// @since 0.6.0
        HIGHLIGHT_BACKGROUND,

        /// Radius used for some GUI elements in Progress, Button etc.
        /// The type is Int_conf.
        /// The actual name is "radius".
        /// @since 0.7.0
        RADIUS,

        /// Default Icon size, in pixels.
        /// The type is Int_conf.
        /// The actual name is "icon_size".
        /// @since 0.7.0
        ICON_SIZE,

        /// Show label on buttons.
        /// The type is Boolean_conf.
        /// The actual name is "button_label".
        /// @since 0.7.0
        BUTTON_LABEL

    };

    /// @name Contstructors & Operators
    /// @{

    /// Default constructor.
    Conf();

    /// Destructor.
   ~Conf();

    /// Copy constructor.
    Conf(const Conf & other);

    /// Copy operator.
    Conf & operator=(const Conf & other);

    /// @}
    /// @name Item Management
    /// @{

    /// Assign or reassign a value to the item.
    Conf_item & set(std::string_view name, const ustring & value=ustring());

    /// Assign or reassign a value to the standard item.
    /// @param ename standard item enumerated name index, see anonymous enum.
    /// @param value the value.
    /// @since 0.6.0
    Conf_item & set(Item ename, const ustring & value=ustring());

    /// Assign or reassign a value to the item and keep previous value onto stack.
    /// @since 0.7.0
    Conf_item & push(std::string_view name, const ustring & value=ustring());

    /// Assign or reassign a value to the item and keep previous value onto stack.
    /// @param ename standard item enumerated name index, see anonymous enum.
    /// @param value the value.
    /// @since 0.7.0
    Conf_item & push(Item ename, const ustring & value=ustring());

    /// Assign or reassign a value to the item from other item.
    /// @param dest     destination item
    /// @param src      source item
    /// @param push     push old value
    /// @since 0.5.0
    Conf_item & set_from(std::string_view dest, std::string_view src, bool push=false);

    /// Assign or reassign a value to the item from other item.
    /// @param edest    destination item
    /// @param src      source item
    /// @param push     push old value
    /// @since 0.6.0
    Conf_item & set_from(Item edest, std::string_view src, bool push=false);

    /// Assign or reassign a value to the item from other item.
    /// @param dest     destination item
    /// @param esrc     source item
    /// @param push     push old value
    /// @since 0.6.0
    Conf_item & set_from(std::string_view dest, Item esrc, bool push=false);

    /// Assign or reassign a value to the item from other item.
    /// @param edest    destination item
    /// @param esrc     source item
    /// @param push     push old value
    /// @since 0.6.0
    Conf_item & set_from(Item edest, Item esrc, bool push=false);

    /// Test if value set for specified item.
    /// @see Conf_item::is_set().
    /// @since 0.6.0
    bool is_set(std::string_view name) const noexcept;

    /// Test if value set for specified item.
    /// @see Conf_item::is_set().
    /// @since 0.6.0
    bool is_set(Item ename) const noexcept;

    /// Unset specified item.
    void unset(std::string_view name);

    /// Unset specified item.
    void unset(Item ename);

    /// Test if certain item available.
    /// Determines whether item available within this %Conf or parents.
    /// @since 0.6.0
    bool avail(std::string_view name) const noexcept;

    /// Test if certain item available.
    /// Determines whether item available within this %Conf or parents.
    /// @since 0.6.0
    bool avail(Item ename) const noexcept;

    /// Test if %Conf provides certain item.
    /// Determines whether item provided by this %Conf but not parents.
    /// @since 0.6.0
    bool provides(std::string_view name) const noexcept;

    /// Test if %Conf provides certain item.
    /// Determines whether item provided by this %Conf but not parents.
    /// @since 0.6.0
    bool provides(Item ename) const noexcept;

    /// Get item by name.
    Conf_item & item(std::string_view name);

    /// Get item by enumerated index.
    Conf_item & item(Item ename);

    /// Get constant item by name.
    const Conf_item & item(std::string_view name) const;

    /// Get constant item by enumerated index.
    const Conf_item & item(Item ename) const;

    /// Get item value as UTF-8 string by name.
    /// @see Conf_item::str().
    /// @since 0.6.0
    ustring str(std::string_view name) const;

    /// Get item value as UTF-8 string by enumerated index.
    /// @see Conf_item::str().
    /// @since 0.6.0
    ustring str(Item name) const;

    /// Get wrapped Conf_item into Font_conf by name.
    Font_conf font(std::string_view name="font");

    /// Get wrapped Conf_item into Font_conf by enumerated index.
    Font_conf font(Item ename);

    /// Get wrapped Conf_item into Font_conf by name.
    Font_conf font(std::string_view name="font") const;

    /// Get wrapped Conf_item into Font_conf by enumerated index.
    Font_conf font(Item ename) const;

    /// Get wrapped Conf_item into Color_conf by name.
    Color_conf color(std::string_view name);

    /// Get wrapped Conf_item into Color_conf by enumerated index.
    Color_conf color(Item ename);

    /// Get wrapped Conf_item into Color_conf by name.
    Color_conf color(std::string_view name) const;

    /// Get wrapped Conf_item into Color_conf by enumerated index.
    Color_conf color(Item ename) const;

    /// Get wrapped Conf_item into Brush_conf by name.
    /// @since 0.6.0
    Brush_conf brush(std::string_view name);

    /// Get wrapped Conf_item into Brush_conf by enumerated index.
    /// @since 0.6.0
    Brush_conf brush(Item ename);

    /// Get wrapped Conf_item into Brush_conf by name.
    /// @since 0.6.0
    Brush_conf brush(std::string_view name) const;

    /// Get wrapped Conf_item into Brush_conf by enumerated index.
    /// @since 0.6.0
    Brush_conf brush(Item ename) const;

    /// Get wrapped Conf_item into Bool_conf by name.
    /// @since 0.7.0
    Bool_conf boolean(std::string_view name);

    /// Get wrapped Conf_item into Bool_conf by enumerated index.
    /// @since 0.7.0
    Bool_conf boolean(Item ename);

    /// Get wrapped Conf_item into Bool_conf by name.
    /// @since 0.7.0
    Bool_conf boolean(std::string_view name) const;

    /// Get wrapped Conf_item into Bool_conf by enumerated index.
    /// @since 0.7.0
    Bool_conf boolean(Item ename) const;

    /// Get wrapped Conf_item into Int_conf by name.
    /// @since 0.7.0
    Int_conf integer(std::string_view name);

    /// Get wrapped Conf_item into Int_conf by enumerated index.
    /// @since 0.7.0
    Int_conf integer(Item ename);

    /// Get wrapped Conf_item into Int_conf by name.
    /// @since 0.7.0
    Int_conf integer(std::string_view name) const;

    /// Get wrapped Conf_item into Int_conf by enumerated index.
    /// @since 0.7.0
    Int_conf integer(Item ename) const;

    /// List all provided items.
    /// @since 0.6.0
    std::vector<std::string> items() const;

    /// @}
    /// @name Item Redirection
    /// @{

    /// Redirect item.
    /// @param dest item that will be replaced
    /// @param src item that will replace @a dest
    /// @sa unredirect()
    void redirect(std::string_view dest, std::string_view src);

    /// Redirect item.
    /// @param edest item that will be replaced
    /// @param src item that will replace @a dest
    /// @sa unredirect()
    /// @since 0.6.0
    void redirect(Item edest, std::string_view src);

    /// Redirect item.
    /// @param dest item that will be replaced
    /// @param esrc item that will replace @a dest
    /// @sa unredirect()
    /// @since 0.6.0
    void redirect(std::string_view dest, Item esrc);

    /// Redirect item.
    /// @param edest item that will be replaced
    /// @param esrc item that will replace @a dest
    /// @sa unredirect()
    /// @since 0.6.0
    void redirect(Item edest, Item esrc);

    /// Unredirect item.
    /// @param dest item that will be replaced
    /// @sa redirect()
    /// @since 0.4.0
    void unredirect(std::string_view dest);

    /// Unredirect item.
    /// @param edest item that will be replaced
    /// @sa redirect()
    /// @since 0.6.0
    void unredirect(Item edest);

    /// Test if item redirected.
    /// @since 0.6.0
    bool redirected(std::string_view name) const noexcept;

    /// Test if item redirected.
    /// @since 0.6.0
    bool redirected(Item ename) const noexcept;

    /// Get redirection source for item.
    /// @since 0.6.0
    std::string redirection(std::string_view name) const;

    /// Get redirection source for item.
    /// @since 0.6.0
    std::string redirection(Item ename) const;

    /// @}
    /// @name Controls
    /// @{

    /// Set parent style.
    void set_parent(Conf & parent);

    /// Unset parent style.
    void unparent();

    /// @}
    /// @name Signals
    /// @{

    /// Get "signal_changed" for specified item name.
    /// @throw user_error in case specified item not found.
    /// @since 0.4.0
    signal<void()> & signal_changed(std::string_view name);

    /// Get "signal_changed" for specified item name.
    /// @throw user_error in case specified item not found.
    /// @since 0.6.0
    signal<void()> & signal_changed(Item ename);

    /// Get "signal_set".
    /// Signal emitted when some item value changed.
    /// Slot prototype:
    /// ~~~~~~~~~~~~~~~
    /// void on_set(std::string_view name, std::string_view value);
    /// ~~~~~~~~~~~~~~~
    signal<void(std::string_view, std::string_view)> & signal_set();

    /// @}

private:

    Conf_ptr impl;
};

} // namespace tau

#endif // __TAU_CONF_HH__

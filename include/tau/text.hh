// ----------------------------------------------------------------------------
// SPDX-License-Identifier: BSD-2-Clause
// Copyright © 2014-2024 Konstantin Shmelkov <mrcashe@gmail.com>.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
// THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
// PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
// CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
// EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
// PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
// OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
// WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
// OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
// EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// ----------------------------------------------------------------------------

/// @file text.hh The Text class.

#ifndef __TAU_TEXT_HH__
#define __TAU_TEXT_HH__

#include <tau/label.hh>

namespace tau {

/// %Text displaying widget.
/// @note This class is a wrapper around its implementation shared pointer.
/// @note This class installs Widget::signal_mouse_down(), Widget::signal_mouse_up(), Widget::signal_mouse_motion(), Widget::signal_mouse_leave() handlers.
/// @ingroup widget_group
/// @ingroup text_group
class __TAUEXPORT__ Text: public Label {
public:

    /// @name General Constructors
    /// @{

    /// Default constructor.
    Text();

    /// Constructor with text alignment.
    explicit Text(Align halign, Align valign=Align::CENTER);

    /// Constructor with text and text alignment.
    explicit Text(const ustring & s, Align halign=Align::CENTER, Align valign=Align::CENTER);

    /// Constructor with text and text alignment.
    /// @since 0.5.0
    explicit Text(const std::u32string & ws, Align halign=Align::CENTER, Align valign=Align::CENTER);

    /// Constructor with buffer and text alignment.
    explicit Text(Buffer buf, Align halign=Align::CENTER, Align valign=Align::CENTER);

    /// @}
    /// @name Special Constructors and Operators
    /// @{

    /// Copy constructor.
    /// @note This class is a wrapper around its implementation shared pointer,
    /// so copying it just increasing implementation pointer use count, but isn't
    /// really copies the object. The underlying implementation is not copyable.
    Text(const Text & other);

    /// Copy operator.
    /// @note This class is a wrapper around its implementation shared pointer,
    /// so copying it just increasing implementation pointer use count, but isn't
    /// really copies the object. The underlying implementation is not copyable.
    Text & operator=(const Text & other);

    /// Move constructor.
    /// @note This class is a wrapper around its implementation shared pointer,
    /// so copying it just increasing implementation pointer use count, but isn't
    /// really copies the object. The underlying implementation is not copyable.
    /// @note This constructor does not reset @c other implementation pointer.
    Text(Text && other);

    /// Move operator.
    /// @note This class is a wrapper around its implementation shared pointer,
    /// so copying it just increasing implementation pointer use count, but isn't
    /// really copies the object. The underlying implementation is not copyable.
    /// @note This operator does not reset @c other implementation pointer.
    Text & operator=(Text && other);

    /// Constructor with implementation pointer.
    /// @warning Unlike some other classes (Painter as an example), the whole
    /// @ref widget_stack "widget stack" is unable to run with pure implementation
    /// pointer, so attempting to construct widget from a pure (@b nullptr) pointer
    /// will cause throwing an user_error exception!
    /// That exception also will be thrown if user tries to construct the object
    /// from incompatible implementation shared pointer.
    ///
    /// @throw user_error in case of pure implementation pointer or incompatible
    /// implementation pointer class.
    /// @since 0.4.0
    Text(Widget_ptr wp);

    /// Assign implementation.
    /// @throw user_error in case of pure implementation pointer or incompatible
    /// implementation pointer class.
    /// @since 0.4.0
    Text & operator=(Widget_ptr wp);

    /// @}
    /// @name Selection Control.
    /// @{

    /// Allow select.
    /// If enabled, the selection of the text can be done using the keyboard or
    /// by moving the mouse pointer while holding down the left button.
    /// It is not possible to select text with the keys if the cursor is disabled by a call to disable_caret().
    /// @note disabled by default in Text, enabled in Edit.
    /// @sa disallow_select()
    /// @sa select_allowed()
    void allow_select();

    /// Disallow select.
    /// If disabled, шt is impossible to select text in any way.
    /// @sa allow_select.
    /// @sa select_allowed()
    void disallow_select();

    /// Determines if a text selection allowed.
    /// @sa allow_select()
    /// @sa disallow_select()
    bool select_allowed() const noexcept;

    /// Select text between begin and end.
    void select(Buffer_citer begin, Buffer_citer end);

    /// Select text between row1:col1 and row2:col2.
    /// @since 0.6.0
    void select(std::size_t row1, std::size_t col1, std::size_t row2, std::size_t col2);

    /// Select all.
    void select_all();

    /// Test has selection.
    /// @return true if has some selection.
    bool has_selection() const noexcept;

    /// Get selected text as UTF-8.
    ustring selection() const;

    /// Get selected text as UTF-32.
    /// @since 0.6.0
    std::u32string wselection() const;

    /// Get selection as range.
    /// @return a std::pair where .first is selection start and .second is selection end.
    /// @since 0.6.0
    std::pair<Buffer_citer, Buffer_citer> current() const;

    /// Cancel selection.
    void unselect();

    /// @}
    /// @name Caret and iterators
    /// @{

    /// Set caret position.
    void move_to(const Buffer_citer pos);

    /// Set caret position.
    void move_to(std::size_t row, std::size_t col);

    /// Get iterator for current caret position.
    Buffer_citer caret() const;

    /// Enable caret.
    void enable_caret();

    /// Disable caret.
    void disable_caret();

    /// Determines whether caret is enabled.
    bool caret_enabled() const noexcept;

    /// @}
    /// @name Access to established actions.
    /// @{

    /// Gets "Previous Character" action.
    Action & action_previous();

    /// Gets "Select Previous Character" action.
    Action & action_select_previous();

    /// Gets "Next Character" action.
    Action & action_next();

    /// Gets "Select Next Character" action.
    Action & action_select_next();

    /// Gets "Previous Line" action.
    Action & action_previous_line();

    /// Gets "Select Previous Line" action.
    Action & action_select_previous_line();

    /// Gets "Next Line" action.
    Action & action_next_line();

    /// Gets "Select Next Line" action.
    Action & action_select_next_line();

    /// Gets "Previous Word" action.
    Action & action_previous_word();

    /// Gets "Select Previous Word" action.
    Action & action_select_previous_word();

    /// Gets "Next Word" action.
    Action & action_next_word();

    /// Gets "Select Next Word" action.
    Action & action_select_next_word();

    /// Gets "Move Home" action.
    Action & action_home();

    /// Gets "Select Home" action.
    Action & action_select_home();

    /// Gets "Move To End of Line" action.
    Action & action_end();

    /// Gets "Select To End of Line" action.
    Action & action_select_to_eol();

    /// Gets "Move To Start of File" action.
    Action & action_sof();

    /// Gets "Select To Start of File" action.
    Action & action_select_to_sof();

    /// Gets "Move To End of File" action.
    Action & action_eof();

    /// Gets "Select To End of File" action.
    Action & action_select_to_eof();

    /// Gets "Move Page Up" action.
    Action & action_previous_page();

    /// Gets "Move Page Down" action.
    Action & action_next_page();

    /// Gets "Select Previous Page" action.
    Action & action_select_previous_page();

    /// Gets "Select Next Page" action.
    Action & action_select_next_page();

    /// Gets "Select All" action.
    Action & action_select_all();

    /// Gets "Copy" action.
    Action & action_copy();

    /// Gets "Cancel" action.
    Action & action_cancel();

    /// @}
    /// @name Access to established signals.
    /// @{

    /// Signal emitted when selection changes.
    /// Slot prototype:
    /// ~~~~~~~~~~~~~~~
    /// void on_selection_changed();
    /// ~~~~~~~~~~~~~~~
    signal<void()> & signal_selection_changed();

    /// Signal emitted when caret moves.
    /// Slot prototype:
    /// ~~~~~~~~~~~~~~~
    /// void on_caret_motion();
    /// ~~~~~~~~~~~~~~~
    signal<void()> & signal_caret_motion();

    /// Signal emitted when user clicks on the text.
    /// Slot prototype:
    /// ~~~~~~~~~~~~~~~
    /// void on_click();
    /// ~~~~~~~~~~~~~~~
    signal<void()> & signal_click();

    /// @}
};

} // namespace tau

#endif // __TAU_TEXT_HH__

// ----------------------------------------------------------------------------
// SPDX-License-Identifier: BSD-2-Clause
// Copyright © 2014-2024 Konstantin Shmelkov <mrcashe@gmail.com>.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
// THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
// PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
// CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
// EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
// PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
// OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
// WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
// OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
// EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// ----------------------------------------------------------------------------

/// @file display.hh The Display class.

#ifndef __TAU_DISPLAY_HH__
#define __TAU_DISPLAY_HH__

#include <tau/signal.hh>
#include <tau/defs.hh>
#include <tau/ustring.hh>

namespace tau {

/// A display.
///
/// @note This class is a wrapper around its implementation shared pointer.
///
/// @ingroup sys_group
class __TAUEXPORT__ Display: public trackable {
public:

    /// @name Constructors & Operators
    /// @{

    /// Default constructor.
    Display();

    /// Copy constructor.
    ///
    /// @note This class is a wrapper around its implementation shared pointer,
    /// so copying it just increasing implementation pointer use count, but isn't
    /// really copies the object. The underlying implementation is not copyable.
    Display(const Display & other) = default;

    /// Copy operator.
    ///
    /// @note This class is a wrapper around its implementation shared pointer,
    /// so copying it just increasing implementation pointer use count, but isn't
    /// really copies the object. The underlying implementation is not copyable.
    Display & operator=(const Display & other) = default;

    /// Constructor with implementation pointer.
    Display(Display_ptr dp);

    /// Open display with optional arguments.
    static Display open(const ustring & args=ustring());

    /// @}
    /// @name Controls
    /// @{

    /// Get implementation pointer.
    /// @since 0.6.0
    Display_ptr ptr();

    /// Get implementation pointer.
    /// @since 0.6.0
    Display_cptr ptr() const;

    /// Get owninig loop.
    /// @since 0.6.0
    Loop loop();

    /// Get owninig loop.
    /// @since 0.6.0
    const Loop loop() const;

    /// Get unique id.
    int id() const noexcept;

    /// Get size in pixels.
    Size size_px() const noexcept;

    /// Get size in millimeters.
    Size size_mm() const noexcept;

    /// Get dots per inch ratio.
    int dpi() const noexcept;

    /// Get number of bits per pixel.
    int depth() const noexcept;

    /// Get focus end point.
    /// @since 0.6.0
    Widget_ptr focus_endpoint() noexcept;

    /// Get focus end point.
    /// @since 0.6.0
    Widget_cptr focus_endpoint() const noexcept;

    bool can_paste_text() const noexcept;

    void paste_text();

    void copy_text(const ustring & str);

    void allow_screensaver();
    void disallow_screensaver();
    bool screensaver_allowed() const noexcept;

    /// @}
    /// @name Object Stuff
    /// @{

    /// Lookup window by effective path.
    /// @since 0.7.0
    Widget_ptr lookup(const ustring & ep);

    /// Lookup window by effective path.
    /// @since 0.7.0
    Widget_cptr lookup(const ustring & ep) const;

    /// @}
    /// @name Signals
    /// @{

    /// Signal emitted when clipboard state changed.
    /// Slot prototype:
    /// ~~~~~~~~~~~~~~~
    /// void on_clipboard_changed().
    /// ~~~~~~~~~~~~~~~
    signal<void()> & signal_clipboard_changed();

    /// @}

private:

    Display_ptr impl;
};

} // namespace tau

#endif // __TAU_DISPLAY_HH__

// ----------------------------------------------------------------------------
// SPDX-License-Identifier: BSD-2-Clause
// Copyright © 2014-2024 Konstantin Shmelkov <mrcashe@gmail.com>.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
// THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
// PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
// CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
// EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
// PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
// OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
// WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
// OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
// EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// ----------------------------------------------------------------------------

/// @file fontsel.hh The Fontsel class.

#ifndef __TAU_FONTSEL_HH__
#define __TAU_FONTSEL_HH__

#include <tau/ustring.hh>
#include <tau/widget.hh>

namespace tau {

/// %Font selection widget.
///
/// @note This class is a wrapper around its implementation shared pointer.
///
/// @ingroup widget_group
class __TAUEXPORT__ Fontsel: public Widget {
public:

    /// @name Constructors & Operators
    /// @{

    /// Default constructor.
    Fontsel();

    /// Constructor with font specification.
    /// @param spec the font specification, e.g. "Arial Bold 10".
    /// @param sample the sample text to be displayed. If empty, the default one will be used.
    Fontsel(std::string_view spec, const ustring & sample=ustring());

    /// Copy constructor.
    ///
    /// @note This class is a wrapper around its implementation shared pointer,
    /// so copying it just increasing implementation pointer use count, but isn't
    /// really copies the object. The underlying implementation is not copyable.
    Fontsel(const Fontsel & other);

    /// Copy operator.
    ///
    /// @note This class is a wrapper around its implementation shared pointer,
    /// so copying it just increasing implementation pointer use count, but isn't
    /// really copies the object. The underlying implementation is not copyable.
    Fontsel & operator=(const Fontsel & other);

    /// Move constructor.
    ///
    /// @note This class is a wrapper around its implementation shared pointer,
    /// so copying it just increasing implementation pointer use count, but isn't
    /// really copies the object. The underlying implementation is not copyable.
    /// @note This constructor does not reset @c other implementation pointer.
    Fontsel(Fontsel && other);

    /// Move operator.
    ///
    /// @note This class is a wrapper around its implementation shared pointer,
    /// so copying it just increasing implementation pointer use count, but isn't
    /// really copies the object. The underlying implementation is not copyable.
    /// @note This operator does not reset @c other implementation pointer.
    Fontsel & operator=(Fontsel && other);

    /// Constructor with implementation pointer.
    ///
    /// @warning Unlike some other classes (Painter as an example), the whole
    /// @ref widget_stack "widget stack" is unable to run with pure implementation
    /// pointer, so attempting to construct widget from a pure (@b nullptr) pointer
    /// will cause throwing an user_error exception!
    /// That exception also will be thrown if user tries to construct the object
    /// from incompatible implementation shared pointer.
    ///
    /// @throw user_error in case of pure implementation pointer or incompatible
    /// implementation pointer class.
    /// @since 0.4.0
    Fontsel(Widget_ptr wp);

    /// Assign implementation.
    ///
    /// @throw user_error in case of pure implementation pointer or incompatible
    /// implementation pointer class.
    /// @since 0.4.0
    Fontsel & operator=(Widget_ptr wp);

    /// @}
    /// @name Controls
    /// @{

    /// Select specified font.
    void select(std::string_view spec);

    /// Get selected font specification.
    ustring spec() const;

    /// Show only monospace fonts.
    /// @param yes show only monospace fonts if true, otherwise show all fonts.
    /// @see monospace_only_visible()
    void show_monospace_only(bool yes);

    /// Test if only monospace fonts are visible at the moment.
    /// @see set_show_monospace_only()
    bool monospace_only_visible() const noexcept;

    /// Set sample text.
    /// @param sample the sample text to be displayed. If empty, the default one will be used.
    void set_sample(const ustring & sample);

    /// Get sample text.
    ustring sample() const;

    /// @}
    /// @name Actions & Signals
    /// @{

    /// Get "Apply" action.
    Action & action_apply();

    /// Get "Cancel" action.
    Action & action_cancel();

    /// Get signal "selection changed".
    signal<void(std::string_view)> & signal_selection_changed();

    /// Get signal "font activated".
    signal<void(std::string_view)> & signal_font_activated();

    /// @}

};

} // namespace tau

#endif // __TAU_FONTSEL_HH__

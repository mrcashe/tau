// ----------------------------------------------------------------------------
// SPDX-License-Identifier: BSD-2-Clause
// Copyright © 2014-2024 Konstantin Shmelkov <mrcashe@gmail.com>.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
// THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
// PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
// CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
// EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
// PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
// OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
// WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
// OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
// EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// ----------------------------------------------------------------------------

/// @file date.hh The Date class declaration.

#ifndef __TAU_DATE_HH__
#define __TAU_DATE_HH__

#include <tau/defs.hh>
#include <tau/ustring.hh>

namespace tau {

/// Date (year, month, day) representation.
/// Currently only gregorian dates supported.
/// @ingroup time_group
class __TAUEXPORT__ Date {
public:

    /// @name Constructor, operators and control
    /// @{

    /// Default constructor.
    /// Creates pure date.
    Date() noexcept;

    /// Constructor with components.
    Date(int year, int month=0, int day=0) noexcept;

    /// Constructor with string.
    Date(const ustring & s);

    /// Test if pure.
    /// Pure date has all components pure.
    /// @remark Equivalent to has_year().
    operator bool() const noexcept;

    /// Reset to pure state.
    void reset() noexcept;

    /// @}
    /// @name Accessors
    /// @{

    /// Test if year defined.
    bool has_year() const noexcept;

    /// Test if month defined.
    bool has_month() const noexcept;

    /// Test if day defined.
    bool has_day() const noexcept;

    /// Get year as number.
    /// @return if not defined, 0 returned.
    unsigned year() const noexcept;

    /// Get month as number.
    /// @return if not defined, 0 returned.
    unsigned month() const noexcept;

    /// Get day as number.
    /// @return if not defined, 0 returned.
    unsigned day() const noexcept;

    /// Test if certain.
    /// Certain date has all components set.
    bool certain() const noexcept;

    /// Test if leap year stored.
    bool leap() const noexcept;

    /// Get number of days per month.
    /// @return 0 if pure or 28-31 for Gregorian.
    unsigned month_days() const;

    /// Get number of days per year.
    /// @return 0 if pure or 365/366 for Gregorian.
    unsigned year_days() const noexcept;

    /// Get textual representation for current locale.
    /// @return if date is pure, an empty string returned.
    ustring str() const;

    /// Get textual representation for specified locale.
    /// @return if date is pure, an empty string returned.
    ustring str(const Locale & lc) const;

    /// @}
    /// @name Modifiers
    /// @{

    /// Assignment from components.
    /// @note in case parameters contain invalid data, %Date becomes pure.
    void assign(int year, int month=0, int day=0) noexcept;


    /// Assignment from the string.
    /// @note in case @c s contains invalid data, %Date becomes pure.
    void assign(const ustring & s);

    /// Assign year.
    /// @note in case @c y contains invalid data, %Date becomes pure.
    void year(int y) noexcept;

    /// Assign month.
    /// @note in case @c m contains invalid data, %Date becomes pure.
    void month(int m) noexcept;

    /// Assign day.
    /// @note in case @c d contains invalid data, %Date becomes pure.
    void day(int d) noexcept;

    /// @}
    /// @name Helpers
    /// @{

    /// Create Date for today.
    static Date today();

    /// @}
    /// @name Validators
    /// @{

    /// General validator (static).
    static bool validate(const std::u32string & ws);

private:

    unsigned    year_   = 0;
    unsigned    mon_    = 0;
    unsigned    day_    = 0;
};

/// Compare dates.
/// @relates Date
bool operator==(const Date & d1, const Date & d2) noexcept;

/// Compare dates.
/// @relates Date
bool operator!=(const Date & d1, const Date & d2) noexcept;

} // namespace tau

#endif // __TAU_DATE_HH__

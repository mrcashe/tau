// ----------------------------------------------------------------------------
// SPDX-License-Identifier: BSD-2-Clause
// Copyright © 2014-2024 Konstantin Shmelkov <mrcashe@gmail.com>.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
// THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
// PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
// CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
// EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
// PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
// OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
// WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
// OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
// EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// ----------------------------------------------------------------------------

/// @file buffer.hh The Buffer class.

#ifndef __TAU_BUFFER_HH__
#define __TAU_BUFFER_HH__

#include <tau/buffer-iter.hh>
#include <tau/signal.hh>

namespace tau {

/// The text buffer.
///
/// @note This class is a wrapper around its implementation shared pointer.
///
/// @ingroup text_group
class __TAUEXPORT__ Buffer {
public:

    /// @name Constructors, destructor and operators
    /// @{

    /// Constructs an empty buffer.
    Buffer();

    /// Copy constructor.
    ///
    /// @note This class is a wrapper around its implementation shared pointer,
    /// so copying it just increasing implementation pointer use count, but isn't
    /// really copies the object. The underlying implementation is not copyable.
    Buffer(const Buffer & other) = default;

    /// Constructor with UTF-8 text.
    Buffer(const ustring & str);

    /// Constructor with UTF-32 text.
    Buffer(const std::u32string & str);

    /// Constructor with stream.
    Buffer(std::istream & is);

    /// Destructor.
   ~Buffer();

    /// Copy operator.
    ///
    /// @note This class is a wrapper around its implementation shared pointer,
    /// so copying it just increasing implementation pointer use count, but isn't
    /// really copies the object. The underlying implementation is not copyable.
    Buffer & operator=(const Buffer & other) = default;

    /// @}
    /// @name Input/Output
    /// @{

    /// Load from file.
    /// @param path an UTF-8 encoded path to the file.
    static Buffer load_from_file(const ustring & path);

    /// Save to stream.
    void save(std::ostream & os) const;

    /// Save to file.
    void save_to_file(const ustring & path);

    /// Flush contents.
    /// If buffer was created using load_from_file() method, this call will
    /// save it back. If buffer was not loaded from file that way, only
    /// "changed" flag cleared.
    void flush();

    /// @}
    /// @name Change contents
    /// @{

    /// Replace buffer contents from an UTF-8 string.
    void assign(const ustring & str);

    /// Replace buffer contents from an UTF-32 string.
    /// @overload
    void assign(const std::u32string & str);

    /// Replace buffer contents from other buffer.
    /// @overload
    void assign(const Buffer buf);

    /// Insert an UTF-32 characters.
    /// @param pos the position in which insertion will be done.
    /// @param uc the character to be inserted.
    /// @param count repeat count.
    /// @return position at the end of inserted text.
    Buffer_citer insert(Buffer_citer pos, char32_t uc, std::size_t count=1);

    /// Insert text from an UTF-8 string.
    /// @overload
    /// @return position at the end of inserted text.
    Buffer_citer insert(Buffer_citer pos, const ustring & s);

    /// Insert text from an UTF-32 string.
    /// @overload
    /// @return position at the end of inserted text.
    Buffer_citer insert(Buffer_citer pos, const std::u32string & s);

    /// Insert text from STL stream.
    /// @overload
    /// @return position at the end of inserted text.
    Buffer_citer insert(Buffer_citer pos, std::istream & is);

    /// Replace text.
    /// @return position at the end of replaced text.
    Buffer_citer replace(Buffer_citer i, const ustring & s);

    /// Replace text.
    /// @overload
    /// @return position at the end of replaced text.
    Buffer_citer replace(Buffer_citer i, const std::u32string & s);

    /// Replace character.
    /// @overload
    /// @return position at the end of replaced text.
    /// @since 0.6.0
    Buffer_citer replace(Buffer_citer i, char32_t uc, std::size_t count=1);

    /// Erase range.
    /// @return iterator to the actual end position.
    Buffer_citer erase(Buffer_citer begin, Buffer_citer end);

    /// Clears buffer.
    void clear();

    /// @}
    /// @name Get contents
    /// @{

    /// Get text as UTF-8.
    ustring str() const;

    /// Get text as UTF-32.
    std::u32string wstr() const;

    /// @}
    /// @name Controls
    /// @{

    /// Get size in unicode characters.
    std::size_t size() const noexcept;

    /// Get row count.
    std::size_t rows() const noexcept;

    /// Test if empty.
    bool empty() const noexcept;

    /// Test if changed.
    /// @sa signal_changed()
    bool changed() const noexcept;

    /// Get text encoding.
    Encoding encoding() const;

    /// Change encoding.
    void change_encoding(const Encoding & enc);

    /// Enables byte order mark (BOM) generation on output.
    /// This flag is also can be set on input in case the file
    /// or stream has BOM.
    /// Disabled by default.
    /// @sa disable_bom()
    /// @sa bom_enabled()
    /// @sa signal_bom_changed()
    void enable_bom();

    /// Disables byte order mark (BOM) generation on output.
    /// Disabled by default.
    /// @sa enable_bom()
    /// @sa bom_enabled()
    /// @sa signal_bom_changed()
    void disable_bom();

    /// Determines if BOM generation enabled.
    /// Also indicates BOM presence in input file/stream after load.
    /// @sa enable_bom()
    /// @sa disable_bom()
    /// @sa signal_bom_changed()
    bool bom_enabled() const noexcept;

    /// Returns Buffer_citer pointing to specified position.
    Buffer_citer citer(std::size_t row, std::size_t col) const;

    /// Returns Buffer_citer pointing to begin.
    Buffer_citer cbegin() const;

    /// Returns Buffer_citer pointing to end.
    Buffer_citer cend() const;

    /// Lock buffer.
    /// Disables buffer modifying.
    void lock();

    /// Test if locked.
    bool locked() const noexcept;

    /// Unlock buffer.
    /// Enables buffer modifying.
    void unlock();

    /// @}
    /// @name Signals
    /// @{

    /// Signal emitted when text erased.
    /// Slot prototype:
    /// ~~~~~~~~~~~~~~~
    /// void on_buffer_erase(Buffer_citer begin, Buffer_citer end, const std::u32string & erased_text);
    /// ~~~~~~~~~~~~~~~
    signal<void(Buffer_citer, Buffer_citer, const std::u32string &)> & signal_erase();

    /// Signal emitted when text inserted.
    /// Slot prototype:
    /// ~~~~~~~~~~~~~~~
    /// void on_buffer_insert(Buffer_citer begin, Buffer_citer end);
    /// ~~~~~~~~~~~~~~~
    signal<void(Buffer_citer, Buffer_citer)> & signal_insert();

    /// Signal emitted when text replaced.
    /// Slot prototype:
    /// ~~~~~~~~~~~~~~~
    /// void on_buffer_replace(Buffer_citer begin, Buffer_citer end, const std::u32string & replaced_text);
    /// ~~~~~~~~~~~~~~~
    signal<void(Buffer_citer, Buffer_citer, const std::u32string &)> & signal_replace();

    /// Signal emitted when buffer changes somehow.
    /// Slot prototype:
    /// ~~~~~~~~~~~~~~~
    /// void on_buffer_changed();
    /// ~~~~~~~~~~~~~~~
    signal<void()> & signal_changed();

    /// Signal emitted when buffer flushed to disk or elsewhere using save* methods.
    /// Slot prototype:
    /// ~~~~~~~~~~~~~~~
    /// void on_buffer_flush();
    /// ~~~~~~~~~~~~~~~
    signal<void()> & signal_flush();

    /// Signal emitted when buffer locked.
    /// Slot prototype:
    /// ~~~~~~~~~~~~~~~
    /// void on_buffer_lock();
    /// ~~~~~~~~~~~~~~~
    signal<void()> & signal_lock();

    /// Signal emitted when buffer unlocked.
    /// Slot prototype:
    /// ~~~~~~~~~~~~~~~
    /// void on_buffer_unlock();
    /// ~~~~~~~~~~~~~~~
    signal<void()> & signal_unlock();

    /// Signal emitted when encoding changed.
    /// Slot prototype:
    /// ~~~~~~~~~~~~~~~
    /// void on_buffer_encoding_changed(const Encoding & enc);
    /// ~~~~~~~~~~~~~~~
    signal<void(const Encoding &)> & signal_encoding_changed();

    /// Signal emitted when Byte Order Mark changed.
    /// Slot prototype:
    /// ~~~~~~~~~~~~~~~
    /// void on_buffer_bom_changed();
    /// ~~~~~~~~~~~~~~~
    /// @sa enable_bom()
    /// @sa disable_bom()
    /// @sa bom_enabled()
    signal<void()> & signal_bom_changed();

    /// @}

private:

    Buffer_ptr impl;
};

/// Write contents to the stream.
/// @relates Buffer
/// @since 0.7.0
std::ostream & operator<<(std::ostream & os, const Buffer buf);


} // namespace tau

#endif // __TAU_BUFFER_HH__

// ----------------------------------------------------------------------------
// SPDX-License-Identifier: BSD-2-Clause
// Copyright © 2014-2024 Konstantin Shmelkov <mrcashe@gmail.com>.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
// THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
// PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
// CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
// EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
// PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
// OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
// WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
// OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
// EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// ----------------------------------------------------------------------------

/// @file action.hh Master_action, Action_base, Action and Toggle_action classes.

#ifndef __TAU_ACTION_HH__
#define __TAU_ACTION_HH__

#include <tau/defs.hh>
#include <tau/accel.hh>
#include <tau/signal.hh>
#include <tau/ustring.hh>

namespace tau {

/// An action that holds information for other actions.
/// @ingroup action_group
/// @ingroup input_group
/// @sa @ref action_sect
/// @sa @ref kbd_sect
class __TAUEXPORT__ Master_action: public trackable {
public:

    /// @name Constructors, operators
    /// @{

    /// Default constructor.
    Master_action();

    /// Destructor.
   ~Master_action();

    /// Copy constructor.
    Master_action(const Master_action & other);

    /// Copy operator.
    Master_action & operator=(const Master_action & other);

    /// Move constructor.
    Master_action(Master_action && other);

    /// Move operator.
    Master_action & operator=(Master_action && other);

    /// Constructor with accels.
    /// @param accels space separated list of accelerator specifications.
    Master_action(const ustring & accels);

    /// Constructor with accel.
    /// @param kc the key code, see #Key_codes enum.
    /// @param km the key modifier, see #Key_modifiers enum.
    Master_action(char32_t kc, int km);

    /// Constructor with accels and label.
    /// @param accels space separated list of accelerator specifications.
    /// @param label the label.
    Master_action(const ustring & accels, const ustring & label);

    /// Constructor with accel and label.
    /// @param kc the key code, see #Key_codes enum.
    /// @param km the key modifier, see #Key_modifiers enum.
    /// @param label the label.
    Master_action(char32_t kc, int km, const ustring & label);

    /// Constructor with accels, label and icon name.
    /// @param accels space separated list of accelerator specifications.
    /// @param label the label.
    /// @param icon_name the icon name.
    Master_action(const ustring & accels, const ustring & label, const ustring & icon_name);

    /// Constructor with accel, label and icon name.
    /// @param kc the key code, see #Key_codes enum.
    /// @param km the key modifier, see #Key_modifiers enum.
    /// @param label the label.
    /// @param icon_name the icon name.
    Master_action(char32_t kc, int km, const ustring & label, const ustring & icon_name);

    /// Constructor with accels, label, icon name and tooltip.
    /// @param accels space separated list of accelerator specifications.
    /// @param label the label.
    /// @param icon_name the icon name.
    /// @param tooltip the tooltip.
    Master_action(const ustring & accels, const ustring & label, const ustring & icon_name, const ustring & tooltip);

    /// Constructor with accel, label, icon name and tooltip.
    /// @param kc the key code, see #Key_codes enum.
    /// @param km the key modifier, see #Key_modifiers enum.
    /// @param label the label.
    /// @param icon_name the icon name.
    /// @param tooltip the tool tip.
    Master_action(char32_t kc, int km, const ustring & label, const ustring & icon_name, const ustring & tooltip);

    /// @}
    /// @name Sensitivity
    /// @{

    /// Enable action.
    /// %Master_action is enabled (sensitive) by default.
    void enable();

    /// Disable action.
    /// %Master_action is enabled (sensitive) by default.
    void disable();

    /// Parametric form of functions enable() and disable().
    /// @param yes enable or disable.
    /// Calls enable() if @a yes is @b true and disable() otherwise.
    /// @since 0.5.0
    void par_enable(bool yes);

    /// Determines whether action enabled or not.
    /// %Master_action is enabled (sensitive) by default.
    bool enabled() const noexcept;

    /// @}
    /// @name Visibility
    /// @{

    /// Show action.
    /// %Master_action is shown by default.
    void show();

    /// Hide action.
    /// %Master_action is shown by default.
    void hide();

    /// Determines whether action visible or not.
    /// %Master_action is visible by default.
    bool visible() const noexcept;

    /// @}
    /// @name Controls
    /// @{

    /// Get label.
    ustring label() const;

    /// Set label.
    void set_label(const ustring & label);

    /// Assign an icon name.
    void set_icon(const ustring & icon_name);

    /// Get icon name.
    ustring icon_name() const;

    /// Set tooltip.
    void set_tooltip(const ustring & tooltip);

    /// Unset tooltip.
    void unset_tooltip();

    /// Get tooltip.
    ustring tooltip() const;

    /// Add accelerator.
    void add_accel(char32_t kc, int km=KM_NONE);

    /// Add accelerators.
    /// @param key_specs is a space delimited list of key specifications.
    void add_accels(const ustring & key_specs);

    /// Remove accelerator.
    void remove_accel(char32_t kc, int km=KM_NONE);

    /// Remove accelerators.
    /// @param key_specs is a space delimited list of key specifications.
    void remove_accels(const ustring & key_specs);

    /// Remove all accelerators.
    void clear_accels();

    /// %List accelerators.
    std::vector<Accel *> accels();

    /// %List accelerators.
    /// @since 0.7.0
    std::vector<const Accel *> accels() const;

    /// @}
    /// @name Signals
    /// @{

    /// Signal emitted when action becomes disabled.
    signal<void()> & signal_disable();

    /// Signal emitted when action becomes enabled.
    signal<void()> & signal_enable();

    /// Signal emitted when action becomes hidden.
    signal<void()> & signal_hide();

    /// Signal emitted when action becomes visible using show() method.
    signal<void()> & signal_show();

    /// Signal emitted when new accelerator added using add_accel() method.
    signal<void(const Accel & accel)> & signal_accel_added();

    /// Signal emitted when accelerator removed using remove_accel() method.
    signal<void(const Accel & accel)> & signal_accel_removed();

    /// Signal emitted when label changed using set_label() method.
    signal<void(const ustring &)> & signal_label_changed();

    /// Signal emitted when label changed using set_icon() method.
    signal<void(const ustring &)> & signal_icon_changed();

    /// Signal emitted when tooltip changed using one of set_tooltip() methods.
    signal<void(const ustring &)> & signal_tooltip_changed();

    /// @}

protected:

    /// @private
    struct  Data;

    /// @private
    Data *  data = nullptr;
};

/// An action which can be activated in some way by keyboard accelerator,
/// menu item or tool button.
/// %Action provides some information how it should be presented, such as:
/// - %Icon.
/// - Label for menu item.
/// - Tool tip to be shown on tool button.
/// - Set of keyboard accelerators associated with an action.
///
/// %Action can be:
/// - Hidden or shown.
/// - Sensitive or insensitive.
/// - Trigger or toggle.
///
/// %Action also provides set of signals to inform user about happening events.
/// @ingroup action_group
/// @ingroup input_group
/// @sa @ref action_sect
/// @sa @ref kbd_sect
class __TAUEXPORT__ Action_base: public trackable {
public:

    /// @name Sensitivity
    /// @{

    /// Enable action.
    /// %Action is enabled (sensitive) by default.
    void enable();

    /// Disable action.
    /// %Action is enabled (sensitive) by default.
    void disable();

    /// Parametric form of functions enable() and disable().
    /// @param yes enable or disable.
    /// Calls enable() if @a yes is @b true and disable() otherwise.
    /// @since 0.5.0
    void par_enable(bool yes);

    /// Determines whether action enabled or not.
    /// %Action is enabled (sensitive) by default.
    bool enabled() const noexcept;

    /// @}
    /// @name Visibility
    /// @{

    /// Show action.
    /// %Action is shown by default.
    void show();

    /// Hide action.
    /// %Action is shown by default.
    void hide();

    /// Parametric form of functions show() and hide().
    /// @param yes enable or disable.
    /// Calls show() if @a yes is @b true and hide() otherwise.
    /// @since 0.5.0
    void par_show(bool yes);

    /// Determines whether action visible or not.
    /// %Action is visible by default.
    bool visible() const noexcept;

    /// @}
    /// @name Controls
    /// @{

    /// Set master action.
    void set_master_action(Master_action & master_action);

    /// Set master action with checking pointer to @b nullptr.
    void set_master_action(Master_action * master_action);

    /// Set master action by name.
    void set_master_action(const ustring & name);

    /// Lookup accelerators.
    /// @param kc the key code, see #Key_codes enum.
    /// @param km the key modifiers, see #Key_modifiers enum.
    /// @return @b this pointer if action contains such accelerator or @b nullptr if not found.
    Action_base * lookup(char32_t kc, int km);

    /// Get label.
    ustring label() const;

    /// Set label.
    void set_label(const ustring & label);

    /// Assign an icon name.
    void set_icon(const ustring & icon_name);

    /// Get icon name.
    ustring icon_name() const;

    /// Set tool tip.
    void set_tooltip(const ustring & tooltip_text);

    /// Unset tool tip.
    void unset_tooltip();

    /// Get tool tip.
    ustring tooltip() const;

    /// Add accelerator.
    void add_accel(char32_t kc, int km=KM_NONE);

    /// Add accelerators.
    /// @param key_specs is a space delimited list of key specifications.
    void add_accels(const ustring & key_specs);

    /// Remove accelerator.
    void remove_accel(char32_t kc, int km=KM_NONE);

    /// Remove accelerators.
    /// @param key_specs is a space delimited list of key specifications.
    void remove_accels(const ustring & key_specs);

    /// Remove all accelerators.
    void clear_accels();

    /// %List accelerators.
    std::vector<Accel *> accels();

    /// %List accelerators.
    /// @since 0.7.0
    std::vector<const Accel *> accels() const;

    /// Test if owning widget selected.
    /// @since 0.6.0
    bool selected() const noexcept;

    /// @}
    /// @name Signals
    /// @{

    /// Signal emitted when action becomes disabled.
    signal<void()> & signal_disable();

    /// Signal emitted when action becomes enabled.
    signal<void()> & signal_enable();

    /// Signal emitted when action becomes hidden.
    signal<void()> & signal_hide();

    /// Signal emitted when action becomes visible using show() method.
    signal<void()> & signal_show();

    /// Signal emitted when new accelerator added using add_accel() method.
    signal<void(const Accel &)> & signal_accel_added();

    /// Signal emitted when accelerator changed.
    /// Slot prototype:
    /// ~~~~~~~~~~~~~~~
    /// void on_accel_changed(const tau::Accel & new_accel, const tau::Accel & old_accel);
    /// ~~~~~~~~~~~~~~~
    /// @since 0.5.0
    signal<void(const Accel &, const Accel &)> & signal_accel_changed();

    /// Signal emitted when accelerator removed using remove_accel() method.
    /// Slot prototype:
    /// ~~~~~~~~~~~~~~~
    /// void on_accel_removed(const tau::Accel & acc);
    /// ~~~~~~~~~~~~~~~
    signal<void(const Accel &)> & signal_accel_removed();

    /// Signal emitted when label changed using set_label() method.
    signal<void(const ustring &)> & signal_label_changed();

    /// Signal emitted when label changed using set_icon() method.
    signal<void(const ustring &)> & signal_icon_changed();

    /// Signal emitted when tooltip changed using one of set_tooltip() methods.
    signal<void(const ustring &)> & signal_tooltip_changed();

    /// Signal emitted when owning widget selected.
    /// @since 0.6.0
    signal<void()> & signal_select();

    /// Signal emitted when owning widget unselected.
    /// @since 0.6.0
    signal<void()> & signal_unselect();

    /// Signal emitted when action destroyed.
    signal<void()> & signal_destroy();

    /// @}

protected:

    /// @private
    struct  Data;

    /// @private
    Data *  data;

protected:

    virtual ~Action_base();
};

/// An action.
/// To use %Action, you have to connect main %slot to its internal %signal using Action::connect() method.
/// The type of slot is slot< void()>. Additionally, you can connect slots of type slot< bool()>
/// before main slot using Action::connect_before() method and after main slot using Action::connect_after() method.
/// When some of connected before slots returned @b false, nor later connected before slots, nor main, nor after slots will be called and
/// action will not be executed. That slots suitable for validation purposes.
/// If all before connected slots returned @b true or there are no connected before slots, main slot will be called and than after slots
/// will be called, if some. Emission of after slots stops after some slot returned @b false.
/// @ingroup action_group
/// @ingroup input_group
/// @sa @ref action_sect
/// @sa @ref kbd_sect
class __TAUEXPORT__ Action: public Action_base {
public:

    /// Action items to be used.
    /// Flags used to determine which Action_base items will be used
    /// to construct objects like Button, Toggle, Icon from different
    /// kinds of actions.
    /// @ingroup action_flags_group
    enum Flags {

        /// Do not use anything.
        NOTHING     = 0,

        /// Use accelerators provided by action.
        ACCEL       = 1 << 0,

        /// Use tool tip provided by action.
        TOOLTIP     = 1 << 1,

        /// Use icon provided by action.
        ICON        = 1 << 2,

        /// Use everything provided by action.
        ALL         = (ACCEL|TOOLTIP|ICON),

        /// Don't use accelerator(s).
        NO_ACCEL    = (ALL & ~ACCEL),

        /// Don't use tool tip.
        NO_TOOLTIP  = (ALL & ~TOOLTIP),

        /// Don't use icon.
        NO_ICON     = (ALL & ~ICON)
    };

    /// @name Constructors, operators
    /// @{

    /// Default constructor.
    Action();

    /// Constructor with slot.
    /// @param slot_activate slot to be connected using connect() method.
    Action(slot<void()> slot_activate);

    /// Constructor with accelerators and optional slot.
    /// @param accels space separated list of accelerator specifications.
    /// @param slot_activate [optional] slot to be connected using connect() method.
    Action(const ustring & accels, slot<void()> slot_activate=slot<void()>());

    /// Constructor with accelerator and optional slot.
    /// @param kc the key code, see #Key_codes enum.
    /// @param km the key modifier, see #Key_modifiers enum.
    /// @param slot_activate [optional] slot to be connected using connect() method.
    Action(char32_t kc, int km, slot<void()> slot_activate=slot<void()>());

    /// Constructor with accelerators, label and optional slot.
    /// @param accels space separated list of accelerator specifications.
    /// @param label the label.
    /// @param slot_activate [optional] slot to be connected using connect() method.
    Action(const ustring & accels, const ustring & label, slot<void()> slot_activate=slot<void()>());

    /// Constructor with accelerator, label and optional slot.
    /// @param kc the key code, see #Key_codes enum.
    /// @param km the key modifier, see #Key_modifiers enum.
    /// @param label the label.
    /// @param slot_activate [optional] slot to be connected using connect() method.
    Action(char32_t kc, int km, const ustring & label, slot<void()> slot_activate=slot<void()>());

    /// Constructor with accelerators, label, icon name and optional slot.
    /// @param accels space separated list of accelerator specifications.
    /// @param label the label.
    /// @param icon_name the icon name.
    /// @param slot_activate [optional] slot to be connected using connect() method.
    Action(const ustring & accels, const ustring & label, const ustring & icon_name, slot<void()> slot_activate=slot<void()>());

    /// Constructor with accel, label, icon name and optional slot.
    /// @param kc the key code, see #Key_codes enum.
    /// @param km the key modifier, see #Key_modifiers enum.
    /// @param label the label.
    /// @param icon_name the icon name.
    /// @param slot_activate [optional] slot to be connected using connect() method.
    Action(char32_t kc, int km, const ustring & label, const ustring & icon_name, slot<void()> slot_activate=slot<void()>());

    /// Constructor with accelerators, label, icon name, tool tip and optional slot.
    /// @param accels space separated list of accelerator specifications.
    /// @param label the label.
    /// @param icon_name the icon name.
    /// @param tooltip the tool tip.
    /// @param slot_activate [optional] slot to be connected using connect() method.
    Action(const ustring & accels, const ustring & label, const ustring & icon_name, const ustring & tooltip, slot<void()> slot_activate=slot<void()>());

    /// Constructor with accelerator, label, icon name, tool tip and optional slot.
    /// @param kc the key code, see #Key_codes enum.
    /// @param km the key modifier, see #Key_modifiers enum.
    /// @param label the label.
    /// @param icon_name the icon name.
    /// @param tooltip the tool tip.
    /// @param slot_activate [optional] slot to be connected using connect() method.
    Action(char32_t kc, int km, const ustring & label, const ustring & icon_name, const ustring & tooltip, slot<void()> slot_activate=slot<void()>());

    /// Constructor with master action and optional slot.
    /// @param master_action the Master_action.
    /// @param slot_activate [optional] slot to be connected using connect() method.
    Action(Master_action & master_action, slot<void()> slot_activate=slot<void()>());

    /// Constructor with pointer to master action and optional slot.
    /// @param master_action pointer to the Master_action.
    /// @param slot_activate [optional] slot to be connected using connect() method.
    /// @since 0.6.0
    Action(Master_action * master_action, slot<void()> slot_activate=slot<void()>());

    /// Copy constructor.
    Action(const Action & other);

    /// Move constructor.
    Action(Action && other);

    /// Destructor.
    ~Action();

    /// Copy operator.
    Action & operator=(const Action & other);

    /// Move operator.
    Action & operator=(Action && other);

    /// @}
    /// @name Execute
    //  TODO Documentation
    /// @{

    /// Execute the action.
    void exec() const;

    /// Execute the action.
    /// @since 0.6.0
    void operator()() const;

    /// Connect slot before main.
    /// @since 0.6.0
    connection connect_before(slot<bool()> slot, bool prepend=false);

    /// Connect main slot.
    connection connect(slot<void()> slot, bool prepend=false);

    /// Connect slot after main.
    /// @since 0.6.0
    connection connect_after(slot<bool()> slot, bool prepend=false);

    /// @}

private:

    struct Action_data;
};

/// A toggle action.
/// To use %Toggle_action, you have to connect main %slot to its internal %signal using Toggle_action::connect() method.
/// The type of slot is slot< void(bool)>. Additionally, you can connect slots of type slot< bool(bool)>
/// before main slot using Toggle_action::connect_before() method and after main slot using Toggle_action::connect_after() method.
/// When some of connected before slots returned @b false, nor later connected before slots, nor main, nor after slots will be called and
/// action state will not be changed. That slots suitable for validation purposes.
/// If all before connected slots returned @b true or there are no connected before slots, main slot will be called and than after slots
/// will be called, if some. Emission of after slots stops after some slot returned @b false.
/// @ingroup action_group
/// @ingroup input_group
/// @sa @ref action_sect
/// @sa @ref kbd_sect
class __TAUEXPORT__ Toggle_action: public Action_base {
public:

    /// @name Constructors, operators
    /// @{

    /// Default constructor.
    Toggle_action();

    /// Constructor with slot.
    /// @param slot_toggle slot to be connected using connect() method.
    Toggle_action(slot<void(bool)> slot_toggle);

    /// Constructor with accelerators and optional slot.
    Toggle_action(const ustring & accels, slot<void(bool)> slot_toggle=slot<void(bool)>());

    /// Constructor with accelerator and optional slot.
    /// @param kc the key code, see #Key_codes enum.
    /// @param km the key modifier, see #Key_modifiers enum.
    /// @param slot_toggle [optional] slot to be connected using connect() method.
    Toggle_action(char32_t kc, int km, slot<void(bool)> slot_toggle=slot<void(bool)>());

    /// Constructor with accelerators and label and optional slot.
    Toggle_action(const ustring & accels, const ustring & label, slot<void(bool)> slot_toggle=slot<void(bool)>());

    /// Constructor with accel, label and optional slot.
    /// @param kc the key code, see #Key_codes enum.
    /// @param km the key modifier, see #Key_modifiers enum.
    /// @param label the label.
    /// @param slot_toggle [optional] slot to be connected using connect() method.
    Toggle_action(char32_t kc, int km, const ustring & label, slot<void(bool)> slot_toggle=slot<void(bool)>());

    /// Constructor with accelerators, label and icon name and optional slot.
    Toggle_action(const ustring & accels, const ustring & label, const ustring & icon_name, slot<void(bool)> slot_toggle=slot<void(bool)>());

    /// Constructor with accel, label and icon name and optional slot.
    /// @param kc the key code, see #Key_codes enum.
    /// @param km the key modifier, see #Key_modifiers enum.
    /// @param label the label.
    /// @param icon_name the icon name.
    /// @param slot_toggle [optional] slot to be connected using connect() method.
    Toggle_action(char32_t kc, int km, const ustring & label, const ustring & icon_name, slot<void(bool)> slot_toggle=slot<void(bool)>());

    /// Constructor with accelerators, label, icon name,tool tip and optional slot.
    /// @param accels space separated list of accelerator specifications.
    /// @param label the label.
    /// @param icon_name the icon name.
    /// @param tooltip the tool tip.
    /// @param slot_toggle [optional] slot to be connected using connect() method.
    Toggle_action(const ustring & accels, const ustring & label, const ustring & icon_name, const ustring & tooltip, slot<void(bool)> slot_toggle=slot<void(bool)>());

    /// Constructor with accelerator, label, icon name and tool tip and optional slot.
    /// @param kc the key code, see #Key_codes enum.
    /// @param km the key modifier, see #Key_modifiers enum.
    /// @param label the label.
    /// @param icon_name the icon name.
    /// @param tooltip the tool tip.
    /// @param slot_toggle [optional] slot to be connected using connect() method.
    Toggle_action(char32_t kc, int km, const ustring & label, const ustring & icon_name, const ustring & tooltip, slot<void(bool)> slot_toggle=slot<void(bool)>());

    /// Constructor with master action and optional slot.
    /// @param master_action the Master_action.
    /// @param slot_toggle [optional] slot to be connected using connect() method.
    Toggle_action(Master_action & master_action, slot<void(bool)> slot_toggle=slot<void(bool)>());

    /// Constructor with pointer to master action and optional slot.
    /// @param master_action pointer to the Master_action.
    /// @param slot_toggle [optional] slot to be connected using connect() method.
    /// @since 0.6.0
    Toggle_action(Master_action * master_action, slot<void(bool)> slot_toggle=slot<void(bool)>());

    /// Copy constructor.
    Toggle_action(const Toggle_action & other);

    /// Move constructor.
    Toggle_action(Toggle_action && other);

    /// Destructor.
   ~Toggle_action();

    /// Copy operator.
    Toggle_action & operator=(const Toggle_action & other);

    /// Move operator.
    Toggle_action & operator=(Toggle_action && other);

    /// @}
    /// @name Controls
    /// @{

    /// %Toggle the action.
    void toggle();

    /// %Toggle the action.
    /// @since 0.6.0
    void operator()();

    /// Test if toggled (get state).
    bool get() const noexcept;

    /// Set state without emission.
    /// @since 0.5.0
    void setup(bool state);

    /// Set state with emission.
    void set(bool state);

    /// Set state with emission.
    /// @since 0.6.0
    void operator()(bool state);

    /// Connect slot before main slot.
    /// @since 0.7.0
    connection connect_before(slot<bool(bool)> slot, bool prepend);

    /// Connect main slot.
    connection connect(slot<void(bool)> slot_toggle, bool prepend=false);

    /// Connect slot after main slot.
    /// @since 0.7.0
    connection connect_after(slot<bool(bool)> slot, bool prepend);

    /// @}
    /// @name Signals
    /// @{

    /// Signal emitted when setup() method called.
    /// Slot prototype:
    /// ~~~~~~~~~~~~~~~
    /// void on_setup(bool state);
    /// ~~~~~~~~~~~~~~~
    /// @since 0.5.0
    signal<void(bool)> & signal_setup();

    /// @}

private:

    struct Toggle_data;
};

} // namespace tau

#endif // __TAU_ACTION_HH__
